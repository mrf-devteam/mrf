/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/
#ifdef IRAY_INSTALL_DIR

#  include <iostream>

#  include <mrf/iray/iray_util.hpp>
#  include <mrf/iray/iray_light_handler.hpp>

#  include <mrf/geometry/quad.hpp>
#  include <mrf/geometry/direction.hpp>

#  include <mrf/materials/lambert.hpp>
#  include <mrf/materials/diffuse_emittance.hpp>
#  include <mrf/materials/uniform_emittance.hpp>
#  include <mrf/materials/conical_emittance.hpp>
#  include <mrf/materials/dirac_emittance.hpp>
#  include <mrf/materials/l_phong.hpp>
#  include <mrf/materials/perfect_mirror.hpp>
#  include <mrf/materials/ggx.hpp>
#  include <mrf/materials/walter_bsdf.hpp>
#  include <mrf/materials/aniso_ggx.hpp>


namespace mrf
{
namespace iray
{
IrayLightHandler::IrayLightHandler(
    std::vector<mrf::lighting::Light *> const &mrf_lights,
    mrf::gui::fb::Loger const &                loger)
  : _mrf_lights(mrf_lights)
  , _loger(loger)
{}

void IrayLightHandler::createLights(
    mi::base::Handle<mi::neuraylib::IGroup> &      root_group,
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    bool                                           lights_visible_in_scene)
{
  for (uint light_num = 0; light_num < _mrf_lights.size(); light_num++)
  {
    mrf::lighting::Light *mrf_light = (_mrf_lights[light_num]);
    if (mrf_light && mrf_light->type() == mrf::lighting::Light::POINT_LIGHT)
    {
      createPointLight(root_group, transaction, mrf_light, light_num, lights_visible_in_scene);
    }
    else if (mrf_light && mrf_light->type() == mrf::lighting::Light::SURFACIC_LIGHT)
    {
      //Both method works
      createSurfacicLight(root_group, transaction, mrf_light, light_num, lights_visible_in_scene);
      //createMeshLight(root_group,  transaction, mrf_light, light_num, true);
    }
    else if (mrf_light && mrf_light->type() == mrf::lighting::Light::DIR_LIGHT)
    {
      createDirectionnalLight(root_group, transaction, mrf_light, light_num, lights_visible_in_scene);
    }
  }
}

void IrayLightHandler::createPointLight(
    mi::base::Handle<mi::neuraylib::IGroup> &      root_group,
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    mrf::lighting::Light *                         mrf_light,
    uint                                           light_num,
    bool                                           visible)
{
  auto            pos = mrf_light->center();
  mi::Float64_4_4 light_matrix(1.0f);
  light_matrix.translate(-pos.x(), -pos.y(), -pos.z());


  mrf::materials::UMat *umat = mrf_light->umat_pointer();

  bool emittance_type_found = false;

  auto uniform_emitt = dynamic_cast<mrf::materials::UniformEmittance *>(umat);
  if (uniform_emitt)
  {
    emittance_type_found = true;
  }
  if (!emittance_type_found)
  {
    auto conic_emitt = dynamic_cast<mrf::materials::ConicalEmittance *>(umat);
    if (conic_emitt)
    {
      auto light_direction = conic_emitt->direction();

      mi::Float64_4_4 light_rotation_matrix(1.0);
      mrfLightDirectionToIrayMatrix(light_direction, mrf::math::Vec3f(0, 0, 1), light_rotation_matrix);

      light_matrix = light_matrix * light_rotation_matrix;

      emittance_type_found = true;
    }
  }
  if (!emittance_type_found)
  {
    _loger.fatal("Can't retrieve emittance in convert Mrf light while converting a point light");
    return;
  }
  else
  {
    // Create the light "light"
    mi::base::Handle<mi::neuraylib::ILight> light(transaction->create<mi::neuraylib::ILight>("Light"));

    createFlag(light.get(), "shadow_cast", true);
    createFlag(light.get(), "visible", visible);

    std::string light_name = "point_light_" + std::to_string(light_num);
    transaction->store(light.get(), light_name.c_str());

    // Create the instance "light_instance" referencing "light"
    mi::base::Handle<mi::neuraylib::IInstance> instance(transaction->create<mi::neuraylib::IInstance>("Instance"));
    check_success(instance->attach(light_name.c_str()) == 0);

    instance->set_matrix(light_matrix);

    std::string light_instance_name = "light_" + std::to_string(light_num) + "_instance";

    transaction->store(instance.get(), light_instance_name.c_str());

    check_success(root_group->attach(light_instance_name.c_str()) == 0);
  }
}

void IrayLightHandler::createSurfacicLight(
    mi::base::Handle<mi::neuraylib::IGroup> &      root_group,
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    mrf::lighting::Light *                         light,
    uint                                           num_light,
    bool                                           visible)
{
  auto pos = light->center();

  mrf::materials::UMat *umat = light->umat_pointer();

  auto emittance = dynamic_cast<mrf::materials::DiffuseEmittance *>(umat);
  if (emittance)
  {
    mrf::geom::RQuad *rquad = dynamic_cast<mrf::geom::RQuad *>(light->geometry());
    if (!rquad)
    {
      _loger.fatal("Unsupported geometry in create surfacic light Iray Backend");
      return;
    }

    // Create the light "light"
    mi::base::Handle<mi::neuraylib::ILight> light(transaction->create<mi::neuraylib::ILight>("Light"));
    createFlag(light.get(), "shadow_cast", true);
    createFlag(light.get(), "visible", visible);

    light.get()->set_type(mi::neuraylib::LIGHT_POINT);
    light.get()->set_area_shape(mi::neuraylib::AREA_RECTANGLE);
    light.get()->set_area_size_x(rquad->length());
    light.get()->set_area_size_y(rquad->height());


    std::string light_name = "quad_light_" + std::to_string(num_light);
    transaction->store(light.get(), light_name.c_str());

    // Create the instance "light_instance" referencing "light"
    mi::base::Handle<mi::neuraylib::IInstance> instance(transaction->create<mi::neuraylib::IInstance>("Instance"));
    check_success(instance->attach(light_name.c_str()) == 0);
    mi::Float64_4_4 matrix(1.0f);
    //matrix.translate(-pos.x(),-pos.y(),-pos.z());
    //matrix.rotate(0, 3.14, 0);

    //retrieve mrf matrix
    auto reff       = rquad->localRef();
    auto mrf_matrix = reff.getMatrixTo();

    mrfMatrixToIrayMatrix(mrf_matrix, matrix);

    //BEFORE IRAY Area light lokk at (0,0,-1) whereas MRF area light look at (0,0,1) by default
    //NOO NEED TO DO THIS ANYMORE, MRF area light now looks at (0,0,-1)
    //need to rotate here
    //mi::Float64_4_4 matrix2(1.0f);
    //matrix2.rotate(0, 3.14159265359, 0);
    //matrix2.set_rotation(0, 3.14159265359, 0);
    //matrix = matrix * matrix2;

    instance->set_matrix(matrix);

    std::string light_instance_name = "light_" + std::to_string(num_light) + "_instance";

    transaction->store(instance.get(), light_instance_name.c_str());
    check_success(root_group->attach(light_instance_name.c_str()) == 0);
  }
  else
  {
    _loger.fatal("Unsupported emittance type in create surfacic light Iray Backend");
  }
}


/*
auto light_direction = conic_emitt->direction();

mi::Float64_4_4 light_rotation_matrix(1.0);
mrfLightDirectionToIrayMatrix(light_direction, mrf::math::Vec3f(0, 0, 1), light_rotation_matrix);

light_matrix = light_matrix * light_rotation_matrix;

mi = md_spot_light->create_material_instance(arguments.get());
*/

void IrayLightHandler::createDirectionnalLight(
    mi::base::Handle<mi::neuraylib::IGroup> &      root_group,
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    mrf::lighting::Light *                         mrf_light,
    uint                                           light_num,
    bool                                           visible)
{
  mrf::materials::UMat *umat = mrf_light->umat_pointer();

  auto emittance = dynamic_cast<mrf::materials::DiracEmittance *>(umat);
  if (emittance)
  {
    mrf::geom::Direction *direction = dynamic_cast<mrf::geom::Direction *>(mrf_light->geometry());
    if (!direction)
    {
      _loger.fatal("Unsupported geometry in create directionnal light Iray Backend");
      return;
    }
    // Create the light "light"
    mi::base::Handle<mi::neuraylib::ILight> light(transaction->create<mi::neuraylib::ILight>("Light"));
    createFlag(light.get(), "shadow_cast", true);
    createFlag(light.get(), "visible", visible);

    light.get()->set_type(mi::neuraylib::LIGHT_INFINITE);

    std::string light_name = "dir_light_" + std::to_string(light_num);
    transaction->store(light.get(), light_name.c_str());

    // Create the instance "light_instance" referencing "light"
    mi::base::Handle<mi::neuraylib::IInstance> instance(transaction->create<mi::neuraylib::IInstance>("Instance"));
    check_success(instance->attach(light_name.c_str()) == 0);


    //mi::Float64_4_4 matrix(1.0f);
    auto light_direction = direction->value();

    mi::Float64_4_4 light_rotation_matrix(1.0);
    mrfLightDirectionToIrayMatrix(light_direction, mrf::math::Vec3f(0, 0, -1), light_rotation_matrix);

    instance->set_matrix(light_rotation_matrix);

    //light_matrix = light_matrix * light_rotation_matrix;


    /*
    mrf::math::Vec3f a = direction->value();

    mrf::math::Mat3f mrf_matrix;

    if (a.dot(mrf::math::Vec3f(0, 0, 1)) >= 0.99999f)
    {
    mrf_matrix = mrf::math::Mat3f::identity();


    //need to rotate here
    mi::Float64_4_4 matrix2(1.0f);
    matrix2.rotate(0, 3.14159265359, 0);

    instance->set_matrix(matrix2);
    }
    else if (a.dot(mrf::math::Vec3f(0, 0, -1)) >= 0.99999f)
    {

    mrf_matrix = mrf::math::Mat3f::identity();


    mrfMatrixToIrayMatrix(mrf_matrix, matrix);
    instance->set_matrix(matrix);
    }
    else
    {

    mrf::math::Vec3f b(0, 0, -1);
    mrf::math::Vec3f v = a.cross(b);
    float s = v.norm();
    float c = a.dot(b);

    mrf_matrix = mrf::math::Mat3f::identity();
    mrf::math::Mat3f skew = mrf::math::Mat3f(0, -v[2], v[1], v[2], 0, -v[0], -v[1], v[0], 0);
    mrf_matrix = mrf_matrix + skew + skew*skew*(1.f / (1.f + c));


    mrfMatrixToIrayMatrix(mrf_matrix, matrix);
    instance->set_matrix(matrix);

    }
    */



    //IRAY Area light lokk at (0,0,-1) whereas MRF area light look at (0,0,1) by default
    //need to rotate here
    //mi::Float64_4_4 matrix2(1.0f);
    //matrix2.rotate(0, 3.14159265359, 0);
    //matrix = matrix * matrix2;

    //instance->set_matrix(matrix2);

    std::string light_instance_name = "light_" + std::to_string(light_num) + "_instance";

    transaction->store(instance.get(), light_instance_name.c_str());
    check_success(root_group->attach(light_instance_name.c_str()) == 0);
  }
  else
  {
    _loger.fatal("Unsupported emittance type in create directionnal light Iray Backend");
  }
}

void IrayLightHandler::createMeshLight(
    mi::base::Handle<mi::neuraylib::IGroup> &      root_group,
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    mrf::lighting::Light *                         light,
    uint                                           num_light,
    bool                                           visible)
{
  auto pos = light->center();

  mrf::materials::UMat *umat = light->umat_pointer();

  auto emittance = dynamic_cast<mrf::materials::DiffuseEmittance *>(umat);
  if (emittance)
  {
    mrf::geom::RQuad *rquad = dynamic_cast<mrf::geom::RQuad *>(light->geometry());
    if (!rquad)
    {
      _loger.fatal("Unsupported geometry in create surfacic light Iray Backend");
      return;
    }

    // Create a quad mesh
    mi::base::Handle<mi::neuraylib::IPolygon_mesh> iray_mesh(
        transaction->create<mi::neuraylib::IPolygon_mesh>("Polygon_mesh"));

    createFlag(iray_mesh.get(), "visible", visible);
    //createFlag(iray_mesh.get(), "reflection_cast", true);
    //createFlag(iray_mesh.get(), "reflection_recv", true);
    //createFlag(iray_mesh.get(), "refraction_cast", true);
    //createFlag(iray_mesh.get(), "refraction_recv", true);
    createFlag(iray_mesh.get(), "shadow_cast", true);
    //createFlag(iray_mesh.get(), "shadow_recv", true);
    //createFlag(iray_mesh.get(), "important", true);

    //Create Vertices
    auto vertices = rquad->getVertices();
    if (vertices.size() != 4)
    {
      _loger.fatal("Unsupported geometry in create surfacic light Iray Backend: vertices.size() != 4");
      return;
    }

    iray_mesh->reserve_points(static_cast<mi::Uint32>(vertices.size()));
    for (mi::Uint32 i = 0; i < vertices.size(); ++i)
    {
      _loger.debug("IRAY create mesh light vertex:", vertices[i]);
      iray_mesh->append_point(mi::Float32_3(vertices[i].x(), vertices[i].y(), vertices[i].z()));
    }

    // The normal of the quad
    auto          quad_normal = rquad->normal();
    mi::Float32_3 normal(quad_normal.x(), quad_normal.y(), quad_normal.z());
    {
      _loger.debug("IRAY create mesh light normal:", quad_normal);
    }
    // The texture coordinates for the quad
    mi::Float32_2 tex_coord[4];
    tex_coord[0] = mi::Float32_2(0.0f, 0.0f);
    tex_coord[0] = mi::Float32_2(1.0f, 0.0f);
    tex_coord[0] = mi::Float32_2(1.0f, 1.0f);
    tex_coord[0] = mi::Float32_2(0.0f, 1.0f);

    // Create the quad and add the mapping of its vertices to points via the mesh connectivity
    mi::neuraylib::Polygon_handle                          polygon_handle(iray_mesh->add_polygon(4));
    mi::base::Handle<mi::neuraylib::IPolygon_connectivity> mesh_connectivity(iray_mesh->edit_mesh_connectivity());
    mesh_connectivity->set_polygon_indices(polygon_handle, 0, 1, 2, 3);

    // Create an attribute vector for 2-dimensional texture coordinates
    mi::base::Handle<mi::neuraylib::IAttribute_vector> texture_coordinates(
        mesh_connectivity->create_attribute_vector(mi::neuraylib::ATTR_TEXTURE, 2));
    for (mi::Uint32 i = 0; i < 4; ++i)
      texture_coordinates->append_float32(&tex_coord[i].x, 2);
    mesh_connectivity->attach_attribute_vector(texture_coordinates.get());
    // Attach the mesh connectivity back to mesh
    iray_mesh->attach_mesh_connectivity(mesh_connectivity.get());

    // Create a custom connectivity for normals to share the same normal for all vertices
    mi::base::Handle<mi::neuraylib::IPolygon_connectivity> normal_connectivity(
        iray_mesh->create_connectivity(mi::neuraylib::CONNECTIVITY_MAP_GENERIC));
    normal_connectivity->set_polygon_indices(polygon_handle, 0, 0, 0, 0);
    // Create an attribute vector for normals
    mi::base::Handle<mi::neuraylib::IAttribute_vector> normals(
        normal_connectivity->create_attribute_vector(mi::neuraylib::ATTR_NORMAL, 1));
    normals->append_vector3(normal);
    normal_connectivity->attach_attribute_vector(normals.get());
    // Attach the custom connectivity to the mesh
    check_success(iray_mesh->attach_connectivity(normal_connectivity.get()) == 0);

    std::string mesh_name     = "surfacic_light_mesh_" + std::to_string(num_light);
    std::string instance_name = "light_" + std::to_string(num_light) + "_instance";
    transaction->store(iray_mesh.get(), mesh_name.c_str());


    /*
    // The points of the triangle
    mi::Float32_3 point[3];
    point[0] = mi::Float32_3(0.0f, 0.0f, 0.0f);
    point[1] = mi::Float32_3(1.0f, 0.0f, 0.0f);
    point[2] = mi::Float32_3(1.0f, 1.0f, 0.0f);
    // The normal of the triangle
    mi::Float32_3 normal(0.0f, 0.0f, 1.0f);
    // The texture coordinates for the triangle
    mi::Float32_2 tex_coord[3];
    tex_coord[0] = mi::Float32_2(0.0f, 0.0f);
    tex_coord[1] = mi::Float32_2(1.0f, 0.0f);
    tex_coord[2] = mi::Float32_2(1.0f, 1.0f);
    mi::base::Handle<mi::neuraylib::ITriangle_mesh> iray_mesh(
    _transaction->create<mi::neuraylib::ITriangle_mesh>("Triangle_mesh"));
    // Create the points of the mesh
    for (mi::Uint32 i = 0; i < 3; ++i)
    iray_mesh->append_point(point[i]);
    // Create the triangle and specify the mapping of its vertices to points
    mi::neuraylib::Triangle_handle triangle_handle(
    iray_mesh->append_triangle(mi::neuraylib::Triangle_point_indices(0, 1, 2)));
    // Access the mesh connectivity
    mi::base::Handle<mi::neuraylib::ITriangle_connectivity> mesh_connectivity(
    iray_mesh->edit_mesh_connectivity());
    // Create an attribute vector for 2-dimensional texture coordinates
    mi::base::Handle<mi::neuraylib::IAttribute_vector> texture_coordinates(
    mesh_connectivity->create_attribute_vector(mi::neuraylib::ATTR_TEXTURE, 2));
    for (mi::Uint32 i = 0; i < 3; ++i)
    texture_coordinates->append_float32(&tex_coord[i].x, 2);
    mesh_connectivity->attach_attribute_vector(texture_coordinates.get());
    // Attach the mesh connectivity back to mesh
    iray_mesh->attach_mesh_connectivity(mesh_connectivity.get());
    // Create a custom connectivity for normals to share the same normal for all vertices
    mi::base::Handle<mi::neuraylib::ITriangle_connectivity> normal_connectivity(
    iray_mesh->create_connectivity(mi::neuraylib::CONNECTIVITY_MAP_GENERIC));
    normal_connectivity->set_triangle_indices(triangle_handle, 0, 0, 0);
    // Create an attribute vector for normals
    mi::base::Handle<mi::neuraylib::IAttribute_vector> normals(
    normal_connectivity->create_attribute_vector(mi::neuraylib::ATTR_NORMAL, 1));
    normals->append_vector3(normal);
    normal_connectivity->attach_attribute_vector(normals.get());
    // Attach the custom connectivity to the mesh
    iray_mesh->attach_connectivity(normal_connectivity.get());


    createFlag(iray_mesh.get(), "visible", true);
    createFlag(iray_mesh.get(), "reflection_cast", true);
    createFlag(iray_mesh.get(), "reflection_recv", true);
    createFlag(iray_mesh.get(), "refraction_cast", true);
    createFlag(iray_mesh.get(), "refraction_recv", true);
    createFlag(iray_mesh.get(), "shadow_cast", true);
    createFlag(iray_mesh.get(), "shadow_recv", true);
    createFlag(iray_mesh.get(), "important", true);



    std::string mesh_name = "surfacic_light_mesh_" + std::to_string(light_num);
    std::string instance_name = "light_" + std::to_string(light_num) + "_instance";
    _transaction->store(iray_mesh.get(), mesh_name.c_str());
    */


    // Create the instance of the mesh
    mi::base::Handle<mi::neuraylib::IInstance> instance(transaction->create<mi::neuraylib::IInstance>("Instance"));
    check_success(instance->attach(mesh_name.c_str()) == 0);
    mi::Float64_4_4 matrix(1.0f);
    //no need set at transfo here, vertices of the mesh light are already transformed
    instance->set_matrix(matrix);

    /*
    mi::base::Handle<mi::IRef> iray_material(
      instance->create_attribute<mi::IRef>("material", "Ref"));
    check_success(iray_material->set_reference(light_material_name.c_str()) == 0);

    _transaction->store(instance.get(), instance_name.c_str());
    check_success(root_group->attach(instance_name.c_str()) == 0);
    */
  }
  else
  {
    _loger.fatal("Unsupported emittance type in create surfacic light Iray Backend");
  }
}


}   // namespace iray
}   // namespace mrf

#endif
