/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * backend iray renderer
 *
 **/
#pragma once

#ifdef IRAY_INSTALL_DIR

#  include <mrf_core/mrf_dll.hpp>
#  include <mrf_core/mrf_types.hpp>


#  include "gui/feedback/loger.hpp"
#  include "rendering/renderer.hpp"
#  include "rendering/iray_render_target.hpp"
#  include "iray/iray_material_handler.hpp"
#  include "iray/iray_light_handler.hpp"

#  include <cstdio>
#  include <cstdlib>
#  include <vector>

#  include <mi/neuraylib.h>

#  ifdef MI_PLATFORM_WINDOWS
#    include <mi/base/miwindows.h>
#  else
#    include <dlfcn.h>
#    include <unistd.h>
#  endif

#  include <authentication.h>

#  include "rendering/scene.hpp"
#  include "rendering/camera.hpp"

namespace mrf
{
namespace rendering
{
class MRF_CORE_EXPORT IRayRenderer: Renderer
{
public:
  IRayRenderer(mrf::rendering::Scene *mrf_scene, mrf::gui::fb::Loger const &loger);
  ~IRayRenderer();
  void              init();
  void              createTransaction();
  bool              transactionIsOpen();
  void              importMdlModule(std::string const &filepath);
  void              shutdown();
  void              addMdlPath(std::string const &mdl_path);
  void              addIncludePath(std::string const &inc_path);
  void              addResourcePath(std::string const &res_path);
  void              createRenderContext();
  void              createRenderTarget(uint width, uint height);
  void              openMentalImageScene(std::string const &mdl_path);
  void              importMrfScene();
  void              convertMrfShapes();
  void              createAndStoreMesh(std::string const &mesh_name, mrf::geom::Mesh *mesh, bool visible = true);
  void              addBackgroundFromMrfScene();
  void              setCamera(mrf::rendering::Camera *camera, uint num_cam);
  void              render();
  void              saveImage(std::string const &file_path);
  void              commitTransaction();
  mrf::color::Color getPixel(uint col, uint row);
  void              setRenderingQualityCriterion(bool value);
  void              setBatchUpdateInterval(float value);
  void              setMinSamplesPerUpdate(uint value);
  void              setRenderingMinSamples(uint value);
  void              setRenderingMaxSamples(int value);
  void              setMaxRenderingTime(int value);
  void              setMaxPathLength(uint value);
  void              setFireflyFilter(bool value);
  void              setCausticSampler(bool value);
  void              setInstancing(bool value);
  void              printSceneOptions();
  std::string       getSceneOptions();
  void              printRenderContextOptions();
  void addPanoramicEnvmap(std::string &file, float envmap_intensity, mrf::math::Mat3f const &rotation_matrix);
  void activateSunAndSky();
  void setNumPass(uint num);
  void applyMaterials();
  void updateMaterials();
  void setSpectralWavelength(std::vector<uint> const &spectral_wavelengths);

  void exportScene(std::string const &filename);

private:
  mi::base::Handle<mi::neuraylib::INeuray>         _neuray;
  mi::base::Handle<mi::neuraylib::IDatabase>       _database;
  mi::base::Handle<mi::neuraylib::ITransaction>    _transaction;
  mi::base::Handle<mi::neuraylib::IRender_target>  _render_target;
  mi::base::Handle<mi::neuraylib::IRender_context> _render_context;
  //mi::base::Handle<mi::neuraylib::IScene> _iray_scene;
  //mi::base::Handle<mi::neuraylib::IGroup> _root_group;
  //Store an array of array of materials
  //first array size correspond of the number of pass the renderer need to render to sample of wavelength
  //in case of spectral rendering
  //each sub array stores the materials of the objects present in the scene for the pass
  //in the sub array the index of the material correspond to the index in the array return by mrf_scene.getAllMaterials()
  //std::vector<std::vector<std::string>> _iray_material_names;
  //pass number for the renderer, used for spectral rendering where wavelength are sampled 3 by 3.
  uint _num_pass;
  //Pointer to the mrf scene
  mrf::rendering::Scene *_mrf_scene;
  std::vector<uint>      _spectral_wavelengths;
  bool                   _instanciate_meshes;

  static constexpr char const *const IRAY_BACKEND_SCENE_OPTIONS_DB_NAME = "SCENE_OPTIONS";

  mrf::gui::fb::Loger const &_loger;

  mrf::iray::IrayMaterialHandler _material_handler;
  mrf::iray::IrayLightHandler    _light_handler;
};
};   // namespace rendering
};   // namespace mrf
#endif