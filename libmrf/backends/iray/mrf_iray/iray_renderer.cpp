/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/

#ifdef IRAY_INSTALL_DIR

#  include "iray/iray_util.hpp"
#  include "iray/iray_scene_options.hpp"
#  include "rendering/iray_renderer.hpp"
#  include "materials/brdf.hpp"
#  include "materials/l_phong.hpp"
#  include "materials/perfect_mirror.hpp"
#  include "materials/ggx.hpp"
#  include "materials/lambert.hpp"
#  include "materials/anisoGGX.hpp"
#  include "materials/walter_bsdf.hpp"
#  include "rendering/color_background_manager.hpp"
#  include "lighting/panoramic_envmap.hpp"
#  include "lighting/spherical_envmap.hpp"
#  include "lighting/light.hpp"
#  include "util/string_parsing.hpp"
#  include "materials/uniform_emittance.hpp"
#  include "materials/diffuse_emittance.hpp"
#  include "materials/conical_emittance.hpp"
#  include "materials/dirac_emittance.hpp"

#  include <mrf_core/geometry/quad.hpp>
#  include <mrf_core/math/math.hpp>
#  include <mrf_core/rendering/thin_lens_camera.hpp>

using namespace mrf::iray;

namespace mrf
{
namespace rendering
{
IRayRenderer::IRayRenderer(mrf::rendering::Scene *mrf_scene, mrf::gui::fb::Loger const &loger)
  : _neuray(nullptr)
  , _num_pass(0)
  , _mrf_scene(mrf_scene)
  , _loger(loger)
  , _material_handler(mrf_scene->getAllMaterials(), loger)
  , _light_handler(mrf_scene->lights(), loger)
{
  _instanciate_meshes = true;
}

IRayRenderer::~IRayRenderer() {}

void IRayRenderer::init()
{
  // Access the neuray library
  _neuray = load_and_get_ineuray();

  //TODO remove thistest linux unload reload
  //  check_success(unload());
  //  _neuray = load_and_get_ineuray();

  check_success(_neuray.is_valid_interface());

  // Configure the neuray library
  // Load the DDS, FreeImage, Iray Photoreal, and .mi importer plugins.
  mi::base::Handle<mi::neuraylib::IPlugin_configuration> plugin_configuration(
      _neuray->get_api_component<mi::neuraylib::IPlugin_configuration>());
#  ifndef MI_PLATFORM_WINDOWS
  check_success(plugin_configuration->load_plugin_library("nv_freeimage.so") == 0);
  check_success(plugin_configuration->load_plugin_library("libiray.so") == 0);
  check_success(plugin_configuration->load_plugin_library("mi_importer.so") == 0);
  check_success(plugin_configuration->load_plugin_library("mi_exporter.so") == 0);
#  else
  check_success(plugin_configuration->load_plugin_library("nv_freeimage.dll") == 0);
  check_success(plugin_configuration->load_plugin_library("libiray.dll") == 0);
  //check_success(plugin_configuration->load_plugin_library("libirt.dll") == 0);
  check_success(plugin_configuration->load_plugin_library("mi_importer.dll") == 0);
  check_success(plugin_configuration->load_plugin_library("mi_exporter.dll") == 0);
#  endif

  // Start the neuray library
  mi::Sint32 result = _neuray->start();
  check_start_success(result);

  // Get the database, the global scope of the database, and create a transaction in the global
  // scope for importing the scene file and storing the scene.
  _database = (_neuray->get_api_component<mi::neuraylib::IDatabase>());
  check_success(_database.is_valid_interface());



  createTransaction();
}

void IRayRenderer::createTransaction()
{
  _loger.trace("Iray Renderer create a new transaction");
  mi::base::Handle<mi::neuraylib::IScope> scope(_database->get_global_scope());

  _transaction = scope->create_transaction();
  check_success(_transaction.is_valid_interface());
}

bool IRayRenderer::transactionIsOpen()
{
  if (_transaction && _transaction.is_valid_interface()) return _transaction.get()->is_open();
  return false;
}

void IRayRenderer::importMdlModule(std::string const &filepath)
{
  // Import the MDL module "main"
  mi::base::Handle<mi::neuraylib::IImport_api> import_api(_neuray->get_api_component<mi::neuraylib::IImport_api>());
  check_success(import_api.is_valid_interface());

  mi::base::Handle<const mi::neuraylib::IImport_result> import_result(
      import_api->import_elements(_transaction.get(), filepath.c_str()));

  if (import_result->get_error_number() != 0)
  {
    std::cerr << "Error in IrayRenderer::importMdlModule, error code = " << import_result->get_error_number()
              << std::endl;
    std::cerr << "error message = " << import_result->get_error_message() << std::endl;
    //exit(-1);
  }
}

void IRayRenderer::shutdown()
{
  // Shut down the neuray library
  check_success(_neuray->shutdown() == 0);
  _neuray = nullptr;

  // Unload the neuray library
  check_success(unload());
}

void IRayRenderer::addMdlPath(std::string const &mdl_path)
{
  // Configure the neuray library. Here we set the search path for .mdl files.
  mi::base::Handle<mi::neuraylib::IRendering_configuration> rendering_configuration(
      _neuray->get_api_component<mi::neuraylib::IRendering_configuration>());
  check_success(rendering_configuration->add_mdl_path(mdl_path.c_str()) == 0);
}

void IRayRenderer::addIncludePath(std::string const &inc_path)
{
  // Configure the neuray library. Here we set the search path for .mdl files.
  mi::base::Handle<mi::neuraylib::IRendering_configuration> rendering_configuration(
      _neuray->get_api_component<mi::neuraylib::IRendering_configuration>());
  check_success(rendering_configuration->add_include_path(inc_path.c_str()) == 0);
}

void IRayRenderer::addResourcePath(std::string const &res_path)
{
  // Configure the neuray library. Here we set the search path for .mdl files.
  mi::base::Handle<mi::neuraylib::IRendering_configuration> rendering_configuration(
      _neuray->get_api_component<mi::neuraylib::IRendering_configuration>());
  if (!(rendering_configuration->add_resource_path(res_path.c_str()) == 0))
  {
    _loger.warn("Cannot add a ressource path in iray renderer: ", res_path);
  }
}

void IRayRenderer::openMentalImageScene(std::string const &scene_path)
{
  // Import the scene file
  mi::base::Handle<mi::neuraylib::IImport_api> import_api(_neuray->get_api_component<mi::neuraylib::IImport_api>());
  check_success(import_api.is_valid_interface());
  mi::base::Handle<const mi::IString>                   uri(import_api->convert_filename_to_uri(scene_path.c_str()));
  mi::base::Handle<const mi::neuraylib::IImport_result> import_result(
      import_api->import_elements(_transaction.get(), uri->get_c_str()));
  check_success(import_result->get_error_number() == 0);

  // Create the scene object
  mi::base::Handle<mi::neuraylib::IScene> iray_scene;
  iray_scene = _transaction->create<mi::neuraylib::IScene>("the_scene");
  iray_scene->set_rootgroup(import_result->get_rootgroup());
  iray_scene->set_options(import_result->get_options());
  iray_scene->set_camera_instance(import_result->get_camera_inst());

  _transaction->store(iray_scene.get(), "the_scene");
  iray_scene = 0;
}


//The root group, the options, and the camera instance must be valid
void IRayRenderer::createRenderContext()
{
  // Create the render context using the Iray Photoreal render mode
  mi::base::Handle<mi::neuraylib::IScene> iray_scene;
  iray_scene = _transaction->edit<mi::neuraylib::IScene>("the_scene");
  if (!iray_scene)
  {
    std::cerr << "Can't retrieve the_scene in IRayRenderer::createRenderContext()" << std::endl;
    exit(1);
  }

  _render_context = iray_scene->create_render_context(_transaction.get(), "iray");
  //calling updateMaterials after this won't work
  //updateMaterials();

  check_success(_render_context.is_valid_interface());
  mi::base::Handle<mi::IString> scheduler_mode(_transaction->create<mi::IString>());
  scheduler_mode->set_c_str("batch");
  //scheduler_mode->set_c_str("interactive");

  auto err = _render_context->set_option("scheduler_mode", scheduler_mode.get());
  check_success(err == 0);
  iray_scene = 0;
}

void IRayRenderer::createRenderTarget(uint width, uint height)
{
  // Create the render target
  mi::base::Handle<mi::neuraylib::IImage_api> image_api(_neuray->get_api_component<mi::neuraylib::IImage_api>());

  _render_target.reset();
  _render_target = new IRayRenderTarget(image_api.get(), "Color", width, height);

  //mi::nrx::Render_target* render_target = new mi::nrx::Render_target(image_api.get(), "result","Rgba", 640, 480);
}

void IRayRenderer::importMrfScene()
{
  // Create the group "rootgroup" containing all instances
  mi::base::Handle<mi::neuraylib::IGroup> root_group(_transaction->create<mi::neuraylib::IGroup>("Group"));

  _transaction->store(root_group.get(), "rootgroup");

  _material_handler.createMaterials(_neuray, _transaction);
  convertMrfShapes();

  root_group = _transaction->edit<mi::neuraylib::IGroup>("rootgroup");
  _light_handler.createLights(root_group, _transaction, true);

  //apply materials on lights and meshes
  applyMaterials();



  // Create the options "options"
  mi::neuraylib::IOptions *options
      = _transaction->create<mi::neuraylib::IOptions>("Options");   //  IRAY_BACKEND_SCENE_OPTIONS_DB_NAME);
  mi::base::Handle<mi::neuraylib::IOptions> handle_options(options);
  //mi::base::Handle<mi::neuraylib::IOptions> options(
  //  _transaction->create<mi::neuraylib::IOptions>("Options"));
  _transaction->store(handle_options.get(), IRAY_BACKEND_SCENE_OPTIONS_DB_NAME);
  options        = 0;
  handle_options = 0;

  // Create the scene object itself and return it.
  //mi::base::Handle<mi::neuraylib::IScene> iray_scene;
  //iray_scene = _transaction->create<mi::neuraylib::IScene>("the_scene");
  mi::neuraylib::IScene *iray_scene = _transaction->create<mi::neuraylib::IScene>("Scene");
  iray_scene->set_rootgroup("rootgroup");

  iray_scene->set_options(IRAY_BACKEND_SCENE_OPTIONS_DB_NAME);
  mi::base::Handle<mi::neuraylib::IScene> handle_iray_scene(iray_scene);
  _transaction->store(handle_iray_scene.get(), "the_scene");

  handle_iray_scene = 0;
  iray_scene        = 0;

  //Once options have been created we can add an envmap
  addBackgroundFromMrfScene();
}


void IRayRenderer::convertMrfShapes()
{
  mi::base::Handle<mi::neuraylib::IGroup> root_group;
  root_group = _transaction->edit<mi::neuraylib::IGroup>("rootgroup");

  //std::map<std::string, mrf::geom::Intersectable*>& meshes = _mrf_scene->geometryObjects();
  //uint mesh_num = 0;
  //for (auto mesh_it = meshes.begin(); mesh_it != meshes.end(); mesh_it++, mesh_num++)
  //{

  std::vector<mrf::geom::Shape *> &shapes    = _mrf_scene->shapes();
  uint                             mesh_num  = 0;
  uint                             shape_num = 0;
  for (auto shape_it = shapes.begin(); shape_it != shapes.end(); shape_it++, mesh_num++, shape_num++)
  {
    auto mesh = dynamic_cast<mrf::geom::Mesh *>((*shape_it)->mesh());

    if (mesh)
    {
      std::string mesh_name     = (*shape_it)->meshName();   // = "mesh_" + std::to_string(mesh_num);
      std::string instance_name = "mesh_" + std::to_string(mesh_num) + "_instance";

      if (!_instanciate_meshes)
      {
        if (_transaction->access(mesh_name.c_str()))
        {
          mesh_name += "_" + std::to_string(mesh_num);
        }
        createAndStoreMesh(mesh_name, mesh);
      }
      else
      {
        if (_transaction->access(mesh_name.c_str()))
        {
        }
        else
        {
          createAndStoreMesh(mesh_name, mesh);
        }
      }



      // Create the instance of the mesh
      mi::base::Handle<mi::neuraylib::IInstance> instance(_transaction->create<mi::neuraylib::IInstance>("Instance"));
      check_success(instance->attach(mesh_name.c_str()) == 0);
      mi::Float64_4_4 matrix(1.0f);

      mrf::math::Mat4f mrf_matrix = (*shape_it)->worldToObjectTransform();
      mrfMatrixToIrayMatrix(mrf_matrix, matrix);

      instance->set_matrix(matrix);

      //add instance
      _transaction->store(instance.get(), instance_name.c_str());
      check_success(root_group->attach(instance_name.c_str()) == 0);
    }
  }
}

void IRayRenderer::createAndStoreMesh(std::string const &mesh_name, mrf::geom::Mesh *mesh, bool visible)
{
  // Create a triangle mesh
  mi::base::Handle<mi::neuraylib::ITriangle_mesh> iray_mesh(
      _transaction->create<mi::neuraylib::ITriangle_mesh>("Triangle_mesh"));
  createFlag(iray_mesh.get(), "visible", visible);
  createFlag(iray_mesh.get(), "reflection_cast", true);
  createFlag(iray_mesh.get(), "reflection_recv", true);
  createFlag(iray_mesh.get(), "refraction_cast", true);
  createFlag(iray_mesh.get(), "refraction_recv", true);
  createFlag(iray_mesh.get(), "shadow_cast", true);
  createFlag(iray_mesh.get(), "shadow_recv", true);

  //Create Vertices
  auto vertices = mesh->vertices();
  iray_mesh->reserve_points(static_cast<mi::Uint32>(vertices.size()));
  for (mi::Uint32 i = 0; i < vertices.size(); ++i)
  {
    iray_mesh->append_point(mi::Float32_3(vertices[i].x(), vertices[i].y(), vertices[i].z()));
  }

  //Create Faces
  auto faces = mesh->faces();
  iray_mesh->reserve_triangles(static_cast<mi::Uint32>(faces.size()));
  for (mi::Uint32 i = 0; i < faces.size(); ++i)
    iray_mesh->append_triangle(
        mi::neuraylib::Triangle_point_indices(std::get<0>(faces[i]), std::get<1>(faces[i]), std::get<2>(faces[i])));

  // Set normal data and normal connectivity
  auto normals = mesh->normals();
  //TODO
  //NORMAL PER FACE CHANGE THIS
  mi::base::Handle<mi::neuraylib::ITriangle_connectivity> normal_connectivity(iray_mesh->create_connectivity());
  for (mi::Uint32 i = 0; i < faces.size(); ++i)
    normal_connectivity->set_triangle_indices(
        mi::neuraylib::Triangle_handle(i),
        mi::neuraylib::Triangle_point_indices(std::get<0>(faces[i]), std::get<1>(faces[i]), std::get<2>(faces[i])));
  mi::base::Handle<mi::neuraylib::IAttribute_vector> iray_normals(
      normal_connectivity->create_attribute_vector(mi::neuraylib::ATTR_NORMAL));

  for (mi::Uint32 i = 0; i < normals.size(); ++i)
    iray_normals->append_vector3(mi::Float32_3(normals[i].x(), normals[i].y(), normals[i].z()));

  check_success(normal_connectivity->attach_attribute_vector(iray_normals.get()) == 0);
  check_success(iray_mesh->attach_connectivity(normal_connectivity.get()) == 0);

  // Set uv data and uv connectivity
  mi::base::Handle<mi::neuraylib::ITriangle_connectivity> uv_connectivity(iray_mesh->create_connectivity());
  for (mi::Uint32 i = 0; i < faces.size(); ++i)
    uv_connectivity->set_triangle_indices(
        mi::neuraylib::Triangle_handle(i),
        mi::neuraylib::Triangle_point_indices(std::get<0>(faces[i]), std::get<1>(faces[i]), std::get<2>(faces[i])));
  mi::base::Handle<mi::neuraylib::IAttribute_vector> iray_uvs(
      uv_connectivity->create_attribute_vector(mi::neuraylib::ATTR_TEXTURE, 2));

  auto uvs = mesh->uvs();

  for (mi::Uint32 i = 0; i < uvs.size(); ++i)
  {
    iray_uvs->append_float32(uvs[i].getArray(), 2);
  }

  check_success(uv_connectivity->attach_attribute_vector(iray_uvs.get()) == 0);
  check_success(iray_mesh->attach_connectivity(uv_connectivity.get()) == 0);

  _transaction->store(iray_mesh.get(), mesh_name.c_str());
}

void IRayRenderer::addBackgroundFromMrfScene()
{
  if (!_mrf_scene->hasBackground()) return;


  ColorBckgManager *color_bckg = dynamic_cast<ColorBckgManager *>(_mrf_scene->background());
  if (color_bckg)
  {
    _loger.warn("Color background not yet supported in IRAY BACKEND");
    return;
  }

  mrf::lighting::SphericalEnvMap *spherical_envmap
      = dynamic_cast<mrf::lighting::SphericalEnvMap *>(_mrf_scene->background());
  if (spherical_envmap)
  {
    _loger.warn("Spherical envmap not yet supported in IRAY BACKEND");
    return;
  }

  mrf::lighting::PanoramicEnvMap *panoramic_envmap
      = dynamic_cast<mrf::lighting::PanoramicEnvMap *>(_mrf_scene->background());
  if (panoramic_envmap)
  {
    //add the envmap path as a mdl resource path first
    std::string full_path = panoramic_envmap->filePath();
    std::string directory = "";
    std::string filename  = "";
    mrf::util::StringParsing::getDirectory(full_path.c_str(), directory);
    mrf::util::StringParsing::fileNameFromAbsPath(full_path.c_str(), filename);
    //iray requires absolute mdl filepath
    filename = "/" + filename;
    addResourcePath(directory);

    mrf::math::Mat3f rot_matrix = panoramic_envmap->rotationMatrix();

    //then load the texture and set the scene options for envmap lighting
    addPanoramicEnvmap(filename, panoramic_envmap->powerScalingFactor(), rot_matrix);
    _loger.info("Add a Panoramic envmap to the scene");
    return;
  }
}

void IRayRenderer::setCamera(mrf::rendering::Camera *camera, uint num_cam)
{
  //float fovy = camera->fovy();

  auto thin_lens = dynamic_cast<mrf::rendering::ThinLens *>(camera);

  float focal_length = 35.f;
  float sensor_width = float(camera->sensor().size().x());

  if (thin_lens)
  {
    //iray needs milimeters
    focal_length = 1000.f * static_cast<float>(thin_lens->focalLength());
    _loger.trace("IRAY set camera THIN LENS, focal length = ", focal_length);
  }

  mi::base::Handle<mi::neuraylib::ICamera> iray_camera;

  if (num_cam > 0)
  {
    iray_camera = _transaction->edit<mi::neuraylib::ICamera>("camera");
  }
  else
  {
    // Create the camera "camera"
    iray_camera = _transaction->create<mi::neuraylib::ICamera>("Camera");
  }

  //iray_camera->set_focal(15.885967f);
  iray_camera->set_focal(focal_length);
  //iray_camera->set_orthographic(true);
  iray_camera->set_aperture(sensor_width);
  iray_camera->set_aspect(camera->aspectRatio());
  iray_camera->set_resolution_x(camera->sensor().resolution().x());
  iray_camera->set_resolution_y(camera->sensor().resolution().y());
  iray_camera->set_clip_min(camera->zNear());
  iray_camera->set_clip_max(camera->zFar());

  mi::Float64_4_4 camera_matrix;
  mrfMatrixToIrayMatrix(camera->viewMat(), camera_matrix);

  mi::base::Handle<mi::neuraylib::IInstance> instance;

  if (num_cam == 0)
  {
    _transaction->store(iray_camera.get(), "camera");

    // Create the instance "camera_instance" referencing "camera"
    instance = _transaction->create<mi::neuraylib::IInstance>("Instance");

    check_success(instance->attach("camera") == 0);
  }
  else
  {
    instance = _transaction->edit<mi::neuraylib::IInstance>("camera_instance");
  }

  instance->set_matrix(camera_matrix);

  if (num_cam == 0)
  {
    _transaction->store(instance.get(), "camera_instance");


    mi::base::Handle<mi::neuraylib::IGroup> root_group;
    root_group = _transaction->edit<mi::neuraylib::IGroup>("rootgroup");

    if (!root_group)
    {
      std::cerr << "Can't retrieve rootgroup in setCamera(...)" << std::endl;
      exit(1);
    }
    check_success(root_group->attach("camera_instance") == 0);
    mi::base::Handle<mi::neuraylib::IScene> iray_scene;
    iray_scene = _transaction->edit<mi::neuraylib::IScene>("the_scene");
    if (!iray_scene)
    {
      std::cerr << "Can't retrieve the_scene in setCamera(...)" << std::endl;
      exit(1);
    }
    iray_scene->set_camera_instance("camera_instance");
    iray_scene = 0;
  }
}

void IRayRenderer::render()
{
  _loger.debug("IRAY call render()");
  while (_render_context->render(_transaction.get(), _render_target.get(), 0) == 0)
  {
    _loger.debug("IRAY call render()");
  }
  check_success(_render_context->render(_transaction.get(), _render_target.get(), 0) >= 0);
}

void IRayRenderer::saveImage(std::string const &file_path)
{
  _loger.info("IRAY saveImage() in", file_path);

  // Write the image to disk
  mi::base::Handle<mi::neuraylib::IExport_api> export_api(_neuray->get_api_component<mi::neuraylib::IExport_api>());
  check_success(export_api.is_valid_interface());
  mi::base::Handle<mi::neuraylib::ICanvas> canvas(_render_target->get_canvas(0));
  export_api->export_canvas(file_path.c_str(), canvas.get());
}

void IRayRenderer::commitTransaction()
{
  // All transactions need to get committed or aborted.
  _transaction->commit();
}

mrf::color::Color IRayRenderer::getPixel(uint col, uint row)
{
  mi::base::Handle<mi::neuraylib::ICanvas> canvas(_render_target->get_canvas(0));
  uint                                     tile_res_x = canvas->get_tile_resolution_x();
  uint                                     tile_res_y = canvas->get_tile_resolution_y();
  uint                                     nb_tiles_x = canvas->get_tiles_size_x();
  uint                                     nb_tiles_y = canvas->get_tiles_size_y();
  auto                                     tile       = canvas->get_tile(col, row);

  mi::Float32 temp[4];
  tile->get_pixel(col, row, temp);

  return mrf::color::Color(temp[0], temp[1], temp[2]);
}

void IRayRenderer::setRenderingQualityCriterion(bool value)
{
  IraySceneOptions::setRenderingQualityCriterion(_transaction, IRAY_BACKEND_SCENE_OPTIONS_DB_NAME, value);
}

void IRayRenderer::setBatchUpdateInterval(float value)
{
  mi::base::Handle<mi::IFloat32> attr(_transaction->create<mi::IFloat32>());
  attr->set_value(value);
  _render_context->set_option("batch_update_interval", attr.get());
}

void IRayRenderer::setMinSamplesPerUpdate(uint value)
{
  mi::base::Handle<mi::IUint32> attr(_transaction->create<mi::IUint32>());
  attr->set_value(value);
  auto err = _render_context->set_option("min_samples_per_update", attr.get());
  check_success(err == 0);
}

void IRayRenderer::setRenderingMinSamples(uint value)
{
  IraySceneOptions::setRenderingMinSamples(_transaction, IRAY_BACKEND_SCENE_OPTIONS_DB_NAME, value);
}

void IRayRenderer::setRenderingMaxSamples(int value)
{
  IraySceneOptions::setRenderingMaxSamples(_transaction, IRAY_BACKEND_SCENE_OPTIONS_DB_NAME, value);
}

void IRayRenderer::setMaxRenderingTime(int value)
{
  IraySceneOptions::setMaxRenderingTime(_transaction, IRAY_BACKEND_SCENE_OPTIONS_DB_NAME, value);
}

void IRayRenderer::setMaxPathLength(uint value)
{
  IraySceneOptions::setMaxPathLength(_transaction, IRAY_BACKEND_SCENE_OPTIONS_DB_NAME, value);
}

void IRayRenderer::setFireflyFilter(bool value)
{
  IraySceneOptions::setFireflyFilter(_transaction, IRAY_BACKEND_SCENE_OPTIONS_DB_NAME, value);
}

void IRayRenderer::setCausticSampler(bool value)
{
  IraySceneOptions::setCausticSampler(_transaction, IRAY_BACKEND_SCENE_OPTIONS_DB_NAME, value);
}

void IRayRenderer::setInstancing(bool value)
{
  IraySceneOptions::setInstancing(_transaction, IRAY_BACKEND_SCENE_OPTIONS_DB_NAME, value);
}



void IRayRenderer::printSceneOptions()
{
  IraySceneOptions::printSceneOptions(_transaction, IRAY_BACKEND_SCENE_OPTIONS_DB_NAME);
}

std::string IRayRenderer::getSceneOptions()
{
  bool need_to_commit = false;
  if (!transactionIsOpen())
  {
    createTransaction();
    need_to_commit = true;
  }

  auto res = IraySceneOptions::getSceneOptions(_transaction, IRAY_BACKEND_SCENE_OPTIONS_DB_NAME);

  if (need_to_commit) _transaction->commit();

  return res;
}

void IRayRenderer::printRenderContextOptions()
{
  mi::Size options_length = _render_context->get_options_length();

  for (auto i = 0; i < options_length; i++)
  {
    const char *name = _render_context->get_option_name(i);
    const char *type = _render_context->get_option_type(i);

    _loger.info("Option ", i);
    _loger.info(name);
    _loger.info(type);
    //<< render_context->get_option(render_context->get_option_name(i)) << std::endl;

    if (strcmp(type, "String") == 0)
    {
      _loger.info(" = ", _render_context->get_option<mi::IString>(name)->get_c_str());
    }
    else if (strcmp(type, "Uint32") == 0)
    {
      mi::Uint32 val;
      _render_context->get_option<mi::IUint32>(name)->get_value(val);
      _loger.info(" = ", val);
    }
    else if (strcmp(type, "Float32") == 0)
    {
      mi::Float32 val;
      _render_context->get_option<mi::IFloat32>(name)->get_value(val);
      _loger.info(" = ", val);
    }
    else if (strcmp(type, "Boolean") == 0)
    {
      bool val;
      _render_context->get_option<mi::IBoolean>(name)->get_value(val);
      _loger.info(" = ", val);
    }
    else
    {}
  }
}

void IRayRenderer::addPanoramicEnvmap(
    std::string &           file,
    float                   envmap_intensity,
    mrf::math::Mat3f const &rotation_matrix)
{
  mi::base::Handle<mi::neuraylib::IScene> iray_scene;
  iray_scene = _transaction->edit<mi::neuraylib::IScene>("the_scene");
  if (!iray_scene)
  {
    std::cerr << "Can't retrieve the_scene in addPanoramicEnvmap(...)" << std::endl;
    exit(1);
  }

  mi::base::Handle<mi::neuraylib::IOptions> scene_options;
  scene_options = _transaction->edit<mi::neuraylib::IOptions>((iray_scene->get_options())->get_c_str());

  mi::base::Handle<mi::IRef> target_attr;

  if (!scene_options->is_attribute("environment_function"))
  {
    target_attr = scene_options->create_attribute<mi::IRef>("environment_function");
  }
  else
  {
    target_attr = scene_options->edit_attribute<mi::IRef>("environment_function");
  }

  mi::base::Handle<mi::neuraylib::IMdl_factory> mdl_factory(_neuray->get_api_component<mi::neuraylib::IMdl_factory>());
  mi::base::Handle<mi::neuraylib::IExpression_factory> expression_factory(
      mdl_factory->create_expression_factory(_transaction.get()));
  mi::base::Handle<mi::neuraylib::IValue_factory> value_factory(mdl_factory->create_value_factory(_transaction.get()));

  mi::Sint32 error;

  //CREATING THE HDR TEXTURE FROM FILE
  auto env_tex = mdl_factory->create_texture(
      _transaction.get(),
      file.c_str(),
      mi::neuraylib::IType_texture::Shape::TS_2D,
      0.0,
      true,
      &error);

  if (!env_tex)
  {
    _loger.fatal("Unable to load texture in IRAY BACKEND addPanoramicEnvmap");
    _loger.fatal("error code = ");
    switch (error)
    {
      case -1:
        _loger.fatal("Invalid parameters");
        break;
      case -2:
        _loger.fatal("The file path is not an absolute MDL file path");
        break;
      case -3:
        _loger.fatal("Failed to resolve the given file path, or no suitable image plugin available");
        break;
    }
  }

  mi::base::Handle<const mi::neuraylib::IFunction_definition> fd(
      _transaction->access<mi::neuraylib::IFunction_definition>("mdl::base::environment_spherical(texture_2d)"));

  mi::base::Handle<mi::neuraylib::IExpression_list> arguments(expression_factory->create_expression_list());

  mi::base::Handle<mi::neuraylib::IExpression> texture_exp(expression_factory->create_constant(env_tex));

  error = arguments->add_expression("texture", texture_exp.get());

  mi::base::Handle<mi::neuraylib::IFunction_call> func_call(fd->create_function_call(arguments.get(), &error));
  check_success(func_call.get());

  _transaction->store(func_call.get(), "envmap_texture_call");
  error = target_attr->set_reference("envmap_texture_call");


  //Set the intensity of the envmap
  mi::base::Handle<mi::IFloat32> target_attr_intensity;

  if (!scene_options->is_attribute("environment_function_intensity"))
  {
    target_attr_intensity = scene_options->create_attribute<mi::IFloat32>("environment_function_intensity");
  }
  else
  {
    target_attr_intensity = scene_options->edit_attribute<mi::IFloat32>("environment_function_intensity");
  }
  target_attr_intensity->set_value(envmap_intensity);

  //Rotate the envmap
  mi::base::Handle<mi::IFloat32_3_3> target_attr_orientation;

  if (!scene_options->is_attribute("environment_function_orientation"))
  {
    target_attr_orientation = scene_options->create_attribute<mi::IFloat32_3_3>("environment_function_orientation");
  }
  else
  {
    target_attr_orientation = scene_options->edit_attribute<mi::IFloat32_3_3>("environment_function_orientation");
  }
  //TODO use also theta angle to rotate envmap
  /*float cosPhi = cosf(phi*2.f*mrf::math::Math::PI / 180.f + mrf::math::Math::PI_2);
  float sinPhi = sinf(phi*2.f*mrf::math::Math::PI / 180.f + mrf::math::Math::PI_2);
  mi::Float32 matrix[] = { cosPhi, 0, sinPhi,  0,1,0,  -sinPhi,0,cosPhi };*/
  mi::Float32 matrix[]
      = {rotation_matrix(0, 0),
         rotation_matrix(0, 1),
         rotation_matrix(0, 2),
         rotation_matrix(1, 0),
         rotation_matrix(1, 1),
         rotation_matrix(1, 2),
         rotation_matrix(2, 0),
         rotation_matrix(2, 1),
         rotation_matrix(2, 2)};
  target_attr_orientation->set_values(matrix);

  iray_scene    = 0;
  scene_options = 0;
}

void IRayRenderer::activateSunAndSky()
{
  mi::base::Handle<mi::neuraylib::IScene> iray_scene;
  iray_scene = _transaction->edit<mi::neuraylib::IScene>("the_scene");
  if (!iray_scene)
  {
    std::cerr << "Can't retrieve the_scene in activateSunAndSky(...)" << std::endl;
    exit(1);
  }

  mi::base::Handle<mi::neuraylib::IOptions> scene_options;
  scene_options = _transaction->edit<mi::neuraylib::IOptions>((iray_scene->get_options())->get_c_str());

  mi::base::Handle<mi::IRef> target_attr;

  if (!scene_options->is_attribute("environment_function"))
  {
    target_attr = scene_options->create_attribute<mi::IRef>("environment_function");
  }
  else
  {
    target_attr = scene_options->edit_attribute<mi::IRef>("environment_function");
  }



  mi::base::Handle<mi::neuraylib::IMdl_factory> mdl_factory(_neuray->get_api_component<mi::neuraylib::IMdl_factory>());
  mi::base::Handle<mi::neuraylib::IExpression_factory> expression_factory(
      mdl_factory->create_expression_factory(_transaction.get()));
  mi::base::Handle<mi::neuraylib::IValue_factory> value_factory(mdl_factory->create_value_factory(_transaction.get()));


  /*show functions in module mdl::base*/
  /*
  mi::base::Handle<const mi::neuraylib::IModule> module(
    _transaction->access<mi::neuraylib::IModule>(
      "mdl::base"));
  _loger.debug(module->get_mdl_name() );

  auto num_function = module->get_function_count();
  for (auto i = 0; i < num_function; i++)
  {
    _loger.debug( module->get_function(i));
  }
  */


  mi::Sint32 error;


  //CREATING THE HDR TEXTURE FROM FILE
  auto env_tex = mdl_factory->create_texture(
      _transaction.get(),
      "/wells6_hd.hdr",
      mi::neuraylib::IType_texture::Shape::TS_2D,
      0.0,
      true,
      &error);
  //USING DEFAULT MDL ENVIRONMENT SPHERICAL BASE FUNCTION
  /*mi::neuraylib::Mdl env_shader(trans, neuray.factory(),
  mi::nrx::Mdl::get_base_function(trans,
  "environment_spherical").c_str(),
  "mat_env");
  env_shader->set_value("texture", env_tex.c_str());*/




  mi::base::Handle<const mi::neuraylib::IFunction_definition> fd(
      _transaction->access<mi::neuraylib::IFunction_definition>("mdl::base::environment_spherical(texture_2d)"));

  mi::base::Handle<mi::neuraylib::IExpression_list> arguments(expression_factory->create_expression_list());


  mi::base::Handle<mi::neuraylib::IExpression> texture_exp(expression_factory->create_constant(env_tex));

  error = arguments->add_expression("texture", texture_exp.get());

  mi::base::Handle<mi::neuraylib::IFunction_call> func_call(fd->create_function_call(arguments.get(), &error));
  check_success(func_call.get());

  _transaction->store(func_call.get(), "envmap_texture_call");


  //REFERENCING THE ENVIRONMENT SHADER FROM THE SCENE OPTIONS
  //mi::nrx::Attribute opt(trans, "options");
  //opt.create_attribute("environment_function", "Ref");
  //opt.set_value("environment_function", "mat_env");
  //error = target_attr->set_reference("mat_env");
  //error = target_attr->set_reference("mdl::main::envmap");
  error = target_attr->set_reference("envmap_texture_call");

  _loger.debug("activate sun and sky ", error);


  mi::base::Handle<mi::IBoolean> attr_bool;

  if (!scene_options->is_attribute("environment_dome_visualize"))
  {
    attr_bool = scene_options->create_attribute<mi::IBoolean>("environment_dome_visualize");
  }
  else
  {
    attr_bool = scene_options->edit_attribute<mi::IBoolean>("environment_dome_visualize");
  }

  attr_bool->set_value(true);



  mi::base::Handle<mi::neuraylib::ICamera> iray_camera(_transaction->edit<mi::neuraylib::ICamera>("camera"));

  //background_color

  //error = iray_camera->set_backplate_function("envmap_texture_call");
  iray_camera->set_backplate_background_color(mi::Color(1.0, 0.0, 0.0));
  _loger.debug("activate sun and sky ", error);
  //iray_camera->set_orthographic(true);

  iray_scene    = 0;
  scene_options = 0;

  /*
    attribute scalar "environment_dome_depth" 200
    attribute scalar "environment_dome_ground_shadow_intensity" 1
    attribute scalar "environment_function_intensity" 0.600000024
    attribute scalar "environment_dome_height" 200
    attribute string "environment_function" "env_shd_1"
    attribute boolean "environment_dome_ground" false
    attribute matrix3x3 "environment_function_orientation" 1 0 0  0 1 0  0 0 1
    attribute scalar "environment_dome_width" 200
    attribute string "environment_dome_mode" "infinite"
    attribute scalar "environment_dome_ground_texturescale" 1
    attribute scalar "environment_dome_rotation_angle" 0
    attribute vector "environment_dome_position" 0 0 0
    attribute boolean "environment_lighting_blur" false
    attribute scalar "environment_dome_radius" 100
    attribute vector "environment_dome_ground_position" 0 0 0
    attribute integer "environment_lighting_resolution" 512

    */

  /*
  mi::base::Handle<mi::neuraylib::IFun> mi(
    md->create_material_instance(arguments.get()));
  check_success(mi.get());
  _transaction->store(mi.get(), "test_sun");
  */


  //target_attr->set_reference("base::environment_spherical");
  //auto err = target_attr->set_reference("mdl::base::sun_and_sky");
  //_loger.debug("activate sun and sky ", err);

  /*
  mi::base::Handle<mi::IRef> iray_material(
    instance->create_attribute<mi::IRef>("material", "Ref"));

  environment_function
    base::sun_and_sky
    */

  /*
  mi::base::Handle<const mi::neuraylib::IMaterial_definition> material_definition(
    _transaction->access<mi::neuraylib::IMaterial_definition>("mdl::base::environment_spherical"));


  mi::base::Handle<const mi::neuraylib::IMaterial_definition> material_definition_sun(
    _transaction->access<mi::neuraylib::IMaterial_definition>("mdl::base::sun_and_sky"));

  if (material_definition)
  {
    _loger.info("Create environment spherical");
  }

  if (material_definition_sun)
  {
    _loger.info("Create sun and sky");
  }
  */
  //create_attribute

  // Create a DB element for the image and the texture referencing it.
  /*mi::base::Handle<mi::neuraylib::IImage> image(
    transaction->create<mi::neuraylib::IImage>("Image"));
  check_success(image->reset_file("nvidia_logo.png") == 0);
  transaction->store(image.get(), "nvidia_image");
  mi::base::Handle<mi::neuraylib::ITexture> texture(
    transaction->create<mi::neuraylib::ITexture>("Texture"));
  texture->set_image("nvidia_image");
  transaction->store(texture.get(), "nvidia_texture");*/
}

void IRayRenderer::setNumPass(uint num)
{
  bool transaction_was_open_before = transactionIsOpen();

  if (!transaction_was_open_before) createTransaction();

  _num_pass = num;
  _loger.trace("SET NUM PASS = ", num);
  updateMaterials();
  commitTransaction();

  if (transaction_was_open_before) createTransaction();
}

void IRayRenderer::applyMaterials()
{
  mi::base::Handle<mi::neuraylib::IMdl_factory> mdl_factory(_neuray->get_api_component<mi::neuraylib::IMdl_factory>());

  std::vector<mrf::geom::Shape *> &shapes    = _mrf_scene->shapes();
  uint                             mesh_num  = 0;
  uint                             shape_num = 0;
  for (auto shape_it = shapes.begin(); shape_it != shapes.end(); shape_it++, mesh_num++, shape_num++)
  {
    auto mesh = dynamic_cast<mrf::geom::Mesh *>((*shape_it)->mesh());

    /*
    std::map<std::string, mrf::geom::Intersectable*>& meshes = _mrf_scene->geometryObjects();
    uint mesh_num = 0;
    for (auto mesh_it = meshes.begin(); mesh_it != meshes.end(); mesh_it++, mesh_num++)
    {
    auto mesh = dynamic_cast<mrf::geom::Mesh*>(mesh_it->second);
    */
    if (mesh)
    {
      std::string instance_name = "mesh_" + std::to_string(mesh_num) + "_instance";

      mi::base::Handle<mi::neuraylib::IInstance> mesh_instance;
      mesh_instance = _transaction->edit<mi::neuraylib::IInstance>(instance_name.c_str());

      if (mesh_instance)
      {
        //use shape material
        //TODO support material per face
        //uint material_id = mesh->materialPerFaces()[0];
        uint material_id = (*shape_it)->materialIndex();

        _material_handler.applyMaterial(mesh_instance, material_id);
      }
    }
  }

  auto lights = _mrf_scene->lights();
  for (uint light_num = 0; light_num < lights.size(); light_num++)
  {
    mrf::lighting::Light *mrf_light = (lights[light_num]);

    std::string instance_name = "light_" + std::to_string(light_num) + "_instance";

    mi::base::Handle<mi::neuraylib::IInstance> light_instance;
    light_instance = _transaction->edit<mi::neuraylib::IInstance>(instance_name.c_str());

    if (light_instance)
    {
      uint material_id = mrf_light->materialIndex();
      _material_handler.applyMaterial(light_instance, material_id);
    }
  }

  //_material_handler.updateMaterials(mdl_factory, _transaction, _num_pass, _spectral_wavelengths);
}

void IRayRenderer::updateMaterials()
{
  _loger.trace("IRAY RENDERER UPDATE MATERIALS NUM_PASS = ", _num_pass);

  mi::base::Handle<mi::neuraylib::IMdl_factory> mdl_factory(_neuray->get_api_component<mi::neuraylib::IMdl_factory>());

  _material_handler.updateMaterials(mdl_factory, _transaction, _num_pass, _spectral_wavelengths);
}

void IRayRenderer::setSpectralWavelength(std::vector<uint> const &spectral_wavelengths)
{
  _spectral_wavelengths = spectral_wavelengths;
}

void IRayRenderer::exportScene(std::string const &filename)
{
  // Export the scene
  // Get access to the export functionality
  mi::base::Handle<mi::neuraylib::IExport_api> export_api(_neuray->get_api_component<mi::neuraylib::IExport_api>());
  mi::base::Handle<const mi::IString>          uri(export_api->convert_filename_to_uri(filename.c_str()));

  // Options

  mi::base::Handle<mi::IMap> exportOptions(_transaction->create<mi::IMap>("Map<Interface>"));
  //if (mApp->settings()->value("save_binary_files", true).toBool())
  {
    mi::base::Handle<mi::IUint32> vlimit(_transaction->create<mi::IUint32>("Uint32"));
    vlimit->set_value(1);
    exportOptions->insert("mi_write_binary_vectors_limit", vlimit.get());
  }

  //mi::base::Handle<mi::neuraylib::IGroup> root_group;
  //root_group = _transaction->edit<mi::neuraylib::IGroup>("rootgroup");

  //mi::base::Handle<mi::neuraylib::IScene> iray_scene;
  //iray_scene = _transaction->edit<mi::neuraylib::IScene>("the_scene");


  // Exporting the scene
  const mi::neuraylib::IExport_result *export_result = export_api->export_scene(
      _transaction.get(),
      uri->get_c_str(),
      "rootgroup",
      "camera",
      IRAY_BACKEND_SCENE_OPTIONS_DB_NAME,
      exportOptions.get());

  // Check for errors
  if (export_result && export_result->get_error_number() != 0)
  {
    //QString err(tr("Error while exporting the scene: %1").arg(m_filename));
    std::string err;
    if (export_result->get_error_number() == 2)
    {
      err.append(" (e.g., because the directory does not exist, or the user has insufficient permissions)");
    }
    //nrxLogger.log_message_error(err.toStdString());
    _loger.fatal("IRAY error in exportScene() = ", err);

    for (mi::Size s = 0; s < export_result->get_messages_length(); s++)
    {
      const char *err_msg = export_result->get_message(s);
      if (err_msg) _loger.fatal("error message = ", err_msg);
      //nrxLogger.log_message_error(err_msg);
    }
  }

  // Print statistics
  /*
  QString msg;
  msg = QString("Saving scene in %2sec").arg(QString::number((double)timer.elapsed() / 1000, 'f', 2));
  nrxLogger.log_message_info(msg.toStdString());

  // Saving is done
  emit finished();
  */
}

}   // namespace rendering
}   // namespace mrf

#endif
