/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * backend iray renderer
 * scene options management class
 *
 **/
#pragma once

#include <mrf/mrf_dll.hpp>
#include <mrf/mrf_types.hpp>

#ifdef IRAY_INSTALL_DIR

#  include <mi/neuraylib.h>

#  ifdef MI_PLATFORM_WINDOWS
#    include <mi/base/miwindows.h>
#  else
#    include <dlfcn.h>
#    include <unistd.h>
#  endif

namespace mrf
{
namespace iray
{
MRF_EXPORT class IraySceneOptions
{
public:
  static void setMaxRenderingTime(
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      const char *                                   options_db_name,
      int                                            value);
  static void
  setMaxPathLength(mi::base::Handle<mi::neuraylib::ITransaction> &transaction, const char *options_db_name, uint value);
  static void
              setFireflyFilter(mi::base::Handle<mi::neuraylib::ITransaction> &transaction, const char *options_db_name, bool value);
  static void setCausticSampler(
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      const char *                                   options_db_name,
      bool                                           value);
  static void setRenderingQualityCriterion(
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      const char *                                   options_db_name,
      bool                                           value);
  static void setRenderingMinSamples(
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      const char *                                   options_db_name,
      uint                                           value);
  static void setRenderingMaxSamples(
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      const char *                                   options_db_name,
      int                                            value);
  static void
  setInstancing(mi::base::Handle<mi::neuraylib::ITransaction> &transaction, const char *options_db_name, bool value);

  static void
  printSceneOptions(mi::base::Handle<mi::neuraylib::ITransaction> &transaction, const char *options_db_name);

  static std::string
  getSceneOptions(mi::base::Handle<mi::neuraylib::ITransaction> &transaction, const char *options_db_name);

private:
  template<class T1>
  static mi::base::Handle<T1> getSceneOption(
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      const char *                                   options_db_name,
      const char *                                   option_name);
};


template<class T1>
mi::base::Handle<T1> IraySceneOptions::getSceneOption(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name,
    const char *                                   option_name)
{
  mi::base::Handle<mi::neuraylib::IOptions> scene_options;
  scene_options = transaction->edit<mi::neuraylib::IOptions>(options_db_name);

  mi::base::Handle<T1> target_attr;

  if (!scene_options->is_attribute(option_name))
  {
    target_attr = scene_options->create_attribute<T1>(option_name);
  }
  else
  {
    target_attr = scene_options->edit_attribute<T1>(option_name);
  }
  return target_attr;
}




}   // namespace iray
}   // namespace mrf

#endif