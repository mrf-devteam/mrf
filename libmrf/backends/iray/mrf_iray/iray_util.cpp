/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/
#ifdef IRAY_INSTALL_DIR
#  include "iray/iray_util.hpp"
#  include <iostream>

namespace mrf
{
namespace iray
{
// Pointer to the DSO handle. Cached here for unload().
void *g_dso_handle = 0;

// Helper function similar to check_success(), but specifically for the result of
// #mi::neuraylib::INeuray::start().
void check_start_success(mi::Sint32 result)
{
  if (result == 0) return;
  fprintf(stderr, "mi::neuraylib::INeuray::start() failed with return code %d.\n", result);
  fprintf(stderr, "Typical failure reasons are related to authentication, see the\n");
  fprintf(stderr, "documentation of this method for details.\n");
  exit(EXIT_FAILURE);
}

// printf() format specifier for arguments of type LPTSTR (Windows only).
#  ifdef MI_PLATFORM_WINDOWS
#    ifdef UNICODE
#      define FMT_LPTSTR "%ls"
#    else   // UNICODE
#      define FMT_LPTSTR "%s"
#    endif   // UNICODE
#  endif     // MI_PLATFORM_WINDOWS

// Loads the neuray library and calls the main factory function.
//
// This convenience function loads the neuray DSO, locates and calls the #mi_neuray_factory()
// function. It returns an instance of the main #mi::neuraylib::INeuray interface. It also
// supplies a authentication key (only needed by some variants of the neuray library).
// The function may be called only once.
//
// \param filename    The file name of the DSO. It is feasible to pass \c NULL, which uses a
//                    built-in default value.
// \return            A pointer to an instance of the main #mi::neuraylib::INeuray interface
mi::neuraylib::INeuray *load_and_get_ineuray(const char *filename)
{
#  ifdef MI_PLATFORM_WINDOWS
  if (!filename) filename = "libneuray.dll";
  void *handle = LoadLibraryA((LPSTR)filename);
  if (!handle)
  {
    LPTSTR buffer     = 0;
    LPTSTR message    = TEXT("unknown failure");
    DWORD  error_code = GetLastError();
    if (FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
            0,
            error_code,
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPTSTR)&buffer,
            0,
            0))
      message = buffer;
    fprintf(stderr, "Failed to load library (%u): " FMT_LPTSTR, error_code, message);
    if (buffer) LocalFree(buffer);
    return 0;
  }
  void *symbol = GetProcAddress((HMODULE)handle, "mi_neuray_factory");
  if (!symbol)
  {
    LPTSTR buffer     = 0;
    LPTSTR message    = TEXT("unknown failure");
    DWORD  error_code = GetLastError();
    if (FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
            0,
            error_code,
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPTSTR)&buffer,
            0,
            0))
      message = buffer;
    fprintf(stderr, "GetProcAddress error (%u): " FMT_LPTSTR, error_code, message);
    if (buffer) LocalFree(buffer);
    return 0;
  }
#  else    // MI_PLATFORM_WINDOWS
  std::string temp;
  if (!filename)
  {
    temp = "/linux-x86-64/lib/libneuray.so";
    temp = IRAY_INSTALL_DIR + temp;
    //    filename = "libneuray.so";
    //filename = temp.c_str();
  }
  else
  {
    temp = filename;
  }
  //fprintf(stderr, "try open %s\n", temp.c_str());
  void *handle = dlopen(temp.c_str(), RTLD_LAZY);
  if (!handle)
  {
    //fprintf(stderr, "can't open %s\n", temp.c_str());
    fprintf(stderr, "%s\n", dlerror());
    std::cout << "CAN'T LOAD LIBNEURAY.SO " << dlerror() << std::endl;
    return 0;
  }
  else
  {
    std::cout << "LOAD LIBNEURAY.SO OK " << std::endl;
  }
  void *symbol = dlsym(handle, "mi_neuray_factory");
  if (!symbol)
  {
    fprintf(stderr, "%s\n", dlerror());
    return 0;
  }
#  endif   // MI_PLATFORM_WINDOWS
  g_dso_handle = handle;

  typedef mi::neuraylib::INeuray *(INeuray_factory)(mi::neuraylib::IAllocator *, mi::Uint32);
  INeuray_factory *       factory = reinterpret_cast<INeuray_factory *>(symbol);
  mi::neuraylib::INeuray *neuray  = factory(0, MI_NEURAYLIB_API_VERSION);
  if (neuray) check_success(authenticate(neuray) == 0);
  return neuray;
}

// Unloads the neuray library.
bool unload()
{
#  ifdef MI_PLATFORM_WINDOWS
  int result = FreeLibrary((HMODULE)g_dso_handle);
  if (result == 0)
  {
    LPTSTR buffer     = 0;
    LPTSTR message    = TEXT("unknown failure");
    DWORD  error_code = GetLastError();
    if (FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
            0,
            error_code,
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPTSTR)&buffer,
            0,
            0))
      message = buffer;
    fprintf(stderr, "Failed to unload library (%u): " FMT_LPTSTR, error_code, message);
    if (buffer) LocalFree(buffer);
    return false;
  }
  return true;
#  else
  int result = dlclose(g_dso_handle);
  if (result != 0)
  {
    //fprintf(stderr, "%s\n", dlerror());
    std::cout << "CAN'T UNLOAD LIBNEURAY.SO " << dlerror() << std::endl;
    return false;
  }
  else
  {
    std::cout << "UNLOAD LIBNEURAY.SO OK" << std::endl;
  }
  return true;
#  endif
}

void mrfMatrixToIrayMatrix(mrf::math::Mat4f const &mat_in, mi::Float64_4_4 &mat_out)
{
  for (int i_mat = 0; i_mat < 4; i_mat++)
  {
    for (int j_mat = 0; j_mat < 4; j_mat++)
    {
      mat_out(j_mat, i_mat) = mat_in(i_mat, j_mat);
    }
  }
}

void mrfLightDirectionToIrayMatrix(
    mrf::math::Vec3f const &mrf_light_direction,
    mrf::math::Vec3f const &iray_light_direction,
    mi::Float64_4_4 &       mat_out)
{
  //iray default light orientation is towards positive Z: (0,0,-1)
  //mrf::math::Vec3f iray_light_dir(0, 0, 1);
  auto  cross_vec = iray_light_direction.cross(mrf_light_direction);
  float dot_      = iray_light_direction.dot(mrf_light_direction);

  //auto cross_vec = light_direction.cross(iray_light_dir);
  //float dot_ = light_direction.dot(iray_light_dir);


  std::cout << "dot=" << dot_ << " cross=" << cross_vec.sqrLength() << std::endl;

  //vectors are aligned
  if (cross_vec.sqrLength() < 0.0001)
  {
    //no rotation needed
    if (dot_ > 0)
    {
      std::cout << "aligned light" << std::endl;
      mat_out = mi::Float64_4_4(1.0);
      //mat_out(3, 3) = 1.0;
    }
    else   //need to set light to look on Z (0,0,1)
    {
      std::cout << "aligned light rotation PI" << std::endl;
      mat_out       = mi::Float64_4_4(0.0);
      mat_out(3, 3) = 1.0;
      mat_out.set_rotation(mrf::math::Math::PI, 0, 0);
    }
  }
  else   //build a 3x3 rotation matrix
  {
    //build a rotation matrix to align light direction with emittance direction

    mi::Float64_4_4 v_matrix(1.0f);


    v_matrix(0, 0) = 0;
    v_matrix(1, 0) = cross_vec.z();
    v_matrix(2, 0) = -cross_vec.y();
    v_matrix(0, 1) = -cross_vec.z();
    v_matrix(1, 1) = 0;
    v_matrix(2, 1) = cross_vec.x();
    v_matrix(0, 2) = cross_vec.y();
    v_matrix(1, 2) = -cross_vec.x();
    v_matrix(2, 2) = 0;

    mat_out = mi::Float64_4_4(1.0);
    mat_out = mat_out + v_matrix + v_matrix * v_matrix * mi::Float64(1.f / (1 + dot_));

    mat_out(3, 0) = 0;
    mat_out(3, 1) = 0;
    mat_out(3, 2) = 0;
    mat_out(3, 3) = 1;
    mat_out(0, 3) = 0;
    mat_out(1, 3) = 0;
    mat_out(2, 3) = 0;

    // mat_out = mi::Float64_4_4(1.0);

    /*
    mrf::math::Mat3f mrf_matrix = mrf::math::Mat3f::identity();
    mrf::math::Mat3f skew = mrf::math::Mat3f(0, -cross_vec[2], cross_vec[1], cross_vec[2], 0, -cross_vec[0], -cross_vec[1], cross_vec[0], 0);
    mrf_matrix = mrf_matrix + skew + skew*skew*(1.f / (1.f + dot_));


    mrfMatrixToIrayMatrix(mrf_matrix, mat_out);
    */
  }
}

void createFlag(mi::neuraylib::IAttribute_set *attribute_set, const char *name, bool value)
{
  mi::base::Handle<mi::IBoolean> attribute(attribute_set->create_attribute<mi::IBoolean>(name, "Boolean"));
  attribute->set_value(value);
}

}   // namespace iray
}   // namespace mrf
#endif
