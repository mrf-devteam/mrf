/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/
#ifdef IRAY_INSTALL_DIR



#  include "iray/iray_util.hpp"
#  include "iray/iray_material_handler.hpp"
#  include <iostream>

#  include <materials/lambert.hpp>
#  include <materials/diffuse_emittance.hpp>
#  include <materials/uniform_emittance.hpp>
#  include <materials/conical_emittance.hpp>
#  include <materials/dirac_emittance.hpp>
#  include <materials/l_phong.hpp>
#  include <materials/perfect_mirror.hpp>
#  include <materials/ggx.hpp>
#  include <materials/walter_bsdf.hpp>
#  include <materials/aniso_ggx.hpp>
#  include <materials/measured_isotropic.hpp>
#  include <materials/checkerboard.hpp>

#  include <mrf/color/spectrum_converter.hpp>

namespace mrf
{
namespace iray
{
IrayMaterialHandler::IrayMaterialHandler(
    std::vector<mrf::materials::UMat *> const &mrf_materials,
    mrf::gui::fb::Loger const &                loger)
  : _mrf_materials(mrf_materials)
  , _loger(loger)
{}

void IrayMaterialHandler::createMaterials(
    mi::base::Handle<mi::neuraylib::INeuray> &     neuray,
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction)
{
  mi::base::Handle<mi::neuraylib::IMdl_factory> mdl_factory(neuray->get_api_component<mi::neuraylib::IMdl_factory>());
  mi::base::Handle<mi::neuraylib::IExpression_factory> expression_factory(
      mdl_factory->create_expression_factory(transaction.get()));


  mi::base::Handle<mi::neuraylib::IValue_factory> value_factory(mdl_factory->create_value_factory(transaction.get()));


  // Create the default MDL material instance "grey_material"
  mi::base::Handle<mi::neuraylib::IValue>           value(value_factory->create_color(0.6f, 0.6f, 0.6f));
  mi::base::Handle<mi::neuraylib::IExpression>      expression(expression_factory->create_constant(value.get()));
  mi::base::Handle<mi::neuraylib::IExpression_list> arguments(expression_factory->create_expression_list());
  arguments->add_expression("tint", expression.get());
  mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
      transaction->access<mi::neuraylib::IMaterial_definition>("mdl::main::lambert"));
  mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
  check_success(mi.get());
  transaction->store(mi.get(), "grey_material");



  for (uint material_id = 0; material_id < _mrf_materials.size(); material_id++)
  {
    auto material = _mrf_materials[material_id];

    std::string material_name = "material_" + std::to_string(material_id);
    _iray_material_names.push_back(material_name);

    mi::base::Handle<mi::neuraylib::IExpression_list> arguments(expression_factory->create_expression_list());

    auto lambert = dynamic_cast<mrf::materials::Lambert *>(material);
    if (lambert)
    {
      // Create the MDL material
      mi::base::Handle<mi::neuraylib::IValue> tint_value;

      tint_value = (value_factory->create_color(1, 1, 1));

      _loger.trace("Create Iray LAMBERT material_id=", material_id);

      mi::base::Handle<mi::neuraylib::IExpression> tint_expression(
          expression_factory->create_constant(tint_value.get()));

      arguments->add_expression("tint", tint_expression.get());

      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::main::lambert"));
      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }
    auto checkerboard = dynamic_cast<mrf::materials::CheckerBoard *>(material);
    if (checkerboard)
    {
      // Create the MDL material
      _loger.trace("Create Iray CHECKERBOARD material_id=", material_id);

      mi::base::Handle<mi::neuraylib::IValue> light_value;
      light_value = (value_factory->create_color(1, 1, 1));
      mi::base::Handle<mi::neuraylib::IExpression> light_expression(
          expression_factory->create_constant(light_value.get()));
      arguments->add_expression("light", light_expression.get());

      mi::base::Handle<mi::neuraylib::IValue> dark_value;
      dark_value = (value_factory->create_color(1, 1, 1));
      mi::base::Handle<mi::neuraylib::IExpression> dark_expression(
          expression_factory->create_constant(dark_value.get()));
      arguments->add_expression("dark", dark_expression.get());

      mi::base::Handle<mi::neuraylib::IValue> repeat_value(value_factory->create_int(checkerboard->getRepetition()));
      mi::base::Handle<mi::neuraylib::IExpression> repeat_exp(expression_factory->create_constant(repeat_value.get()));
      arguments->add_expression("repeat", repeat_exp.get());



      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::main::checkerboard"));
      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }
    auto measured_iso = dynamic_cast<mrf::materials::MeasuredIsotropic *>(material);
    if (measured_iso)
    {
      _loger.trace("Create Iray MEASURED material_id=", material_id);
      mi::Sint32 result;

      uint alta_d_phi_resolution = 360;
      uint resolution_theta      = 180;   //  std::stoi(alta_header.header()["#GRID"].front());
      uint resolution_phi        = 180;   //alta_d_phi_resolution / 2;

      mi::neuraylib::Bsdf_isotropic_data *bsdfData
          = new mi::neuraylib::Bsdf_isotropic_data(resolution_theta, resolution_phi, mi::neuraylib::BSDF_RGB);
      mi::base::Handle<mi::neuraylib::Bsdf_buffer> bsdfBuffer(bsdfData->get_bsdf_buffer());
      check_success(bsdfBuffer.is_valid_interface());

      // Get a handle on the data buffer and populate (in this case red everywhere)
      mi::Float32 *dataBuffer = bsdfBuffer->get_data();

#  ifdef MRF_RENDERING_MODE_SPECTRAL
      memset(dataBuffer, 0, sizeof(mi::Float32) * 3 * resolution_theta * resolution_theta * resolution_phi);

      const auto &alta_data = measured_iso->getColor();



      for (mi::Uint32 theta_v = 0; theta_v < resolution_theta; theta_v++)
      {
        for (mi::Uint32 theta_l = 0; theta_l < resolution_theta; theta_l++)
        {
          for (mi::Uint32 d_phi = 0; d_phi < resolution_phi; d_phi++)
          {
            //  The index of the(first) element for a particular triple(index_theta_in, index_theta_out, index_phi_in) is given by
            //   factor * (index_theta_in * (res_phi * res_theta) + index_theta_out * res_phi + index_phi)
            //rgb --> factor == 3
            uint idx = 3 * (theta_l * (resolution_phi * resolution_theta) + theta_v * resolution_phi + d_phi);

            uint id_alta = (d_phi * (resolution_theta * resolution_theta) + theta_l * resolution_theta + theta_v);

            const auto &spectrum = alta_data[id_alta];

            //set only the first three wavelengths
            if (spectrum.size() >= 3)
            {
              dataBuffer[idx]     = spectrum[0];
              dataBuffer[idx + 1] = spectrum[1];
              dataBuffer[idx + 2] = spectrum[2];
            }
          }
        }
      }


#  else

      float *alta_data = measured_iso->getColor();
      uint   id_alta   = 0;

      for (mi::Uint32 theta_v = 0; theta_v < resolution_theta; theta_v++)
      {
        for (mi::Uint32 theta_l = 0; theta_l < resolution_theta; theta_l++)
        {
          for (mi::Uint32 d_phi = 0; d_phi < resolution_phi; d_phi++)
          {
            //  The index of the(first) element for a particular triple(index_theta_in, index_theta_out, index_phi_in) is given by
            //   factor * (index_theta_in * (res_phi * res_theta) + index_theta_out * res_phi + index_phi)
            //rgb --> factor == 3
            uint idx = 3 * (theta_l * (resolution_phi * resolution_theta) + theta_v * resolution_phi + d_phi);


            id_alta = 3 * (d_phi * (resolution_theta * resolution_theta) + theta_l * resolution_theta + theta_v);

            dataBuffer[idx]     = alta_data[id_alta];
            dataBuffer[idx + 1] = alta_data[id_alta + 1];
            dataBuffer[idx + 2] = alta_data[id_alta + 2];
          }
        }
      }
#  endif

      // Create an instance of an IBsdf_measurement, assign the Bsdf_isotropic data as reflection,
      //  and store in the database
      mi::base::Handle<mi::neuraylib::IBsdf_measurement> bsdfMeasurement(
          transaction->create<mi::neuraylib::IBsdf_measurement>("Bsdf_measurement"));
      check_success(bsdfMeasurement.is_valid_interface());

      mi::Sint32 err = bsdfMeasurement->set_reflection(bsdfData);
      check_success(err == 0);

      std::string bsdf_measurement_name = material_name + "_measurement";

      err = transaction->store(
          bsdfMeasurement.get(),
          bsdf_measurement_name.c_str(),
          mi::neuraylib::ITransaction::LOCAL_SCOPE);
      check_success(err == 0);

      // Prepare the arguments for the new material instance: set the "customBSDF" argument to "bsdf_red".
      mi::base::Handle<mi::neuraylib::IValue_bsdf_measurement> bsdfValue(
          value_factory->create_bsdf_measurement(bsdf_measurement_name.c_str()));
      mi::base::Handle<mi::neuraylib::IExpression_constant> bsdfExpr(
          expression_factory->create_constant(bsdfValue.get()));
      mi::base::Handle<mi::neuraylib::IExpression_list> arguments(expression_factory->create_expression_list());


      arguments->add_expression("customBSDF", bsdfExpr.get());


      /*
            if (measured_iso->getDiracSize())
            {
              // Prepare the arguments for the new material instance dirac
              std::string dirac_array_name = material_name + "_dirac";

              std::string array_name_with_size = "color[" + std::to_string(measured_iso->getDiracSize()) + "]";

              mi::base::Handle<mi::IArray> mdl_array(
                transaction->create<mi::IArray>(array_name_with_size.c_str()));


              check_success(mdl_array.is_valid_interface());

              for (int array_idx = 0; array_idx < measured_iso->getDiracSize(); array_idx++)
              {
                mi::math::Color mi_color;
                auto color = measured_iso->getDiracColor(array_idx);
                mi_color.r = color.r();
                mi_color.g = color.g();
                mi_color.b = color.b();

                mi::base::Handle<mi::IColor> element(
                  mdl_array->get_element<mi::IColor>(static_cast<mi::Size>(array_idx)));
                element->set_value(mi::Color_struct(mi_color));

              }

              err = transaction->store(mdl_array.get(), dirac_array_name.c_str(), mi::neuraylib::ITransaction::LOCAL_SCOPE);
              check_success(err == 0);


              mi::base::Handle<mi::neuraylib::IValue_array> mdl_value_array(value_factory->create_array(type_factory->create_immediate_sized_array();

            }
            */




      // Create a material instance of the material definition "mdl::MeasuredBase::MeasuredBase" with
      // the just prepared arguments.
      mi::base::Handle<const mi::neuraylib::IMaterial_definition> material_definition(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::main::measured_with_dirac_material"));

      mi::base::Handle<mi::neuraylib::IMaterial_instance> material_instance(
          material_definition->create_material_instance(arguments.get(), &result));
      check_success(result == 0);
      transaction->store(material_instance.get(), material_name.c_str());



      if (measured_iso->getDiracSize())
      {
        mi::neuraylib::Argument_editor mdlMaterial(transaction.get(), material_name.c_str(), mdl_factory.get());
        mi::Size                       dirac_array_size;
        mdlMaterial.get_array_length("dirac_measurement", dirac_array_size);

        if (dirac_array_size != measured_iso->getDiracSize())
        {
          _loger.fatal("Cannot set Dirac values in create MEASURED ISOTROPIC material, wrong array size");
          continue;
        }

        for (int array_idx = 0; array_idx < dirac_array_size; array_idx++)
        {
          mi::math::Color mi_color;
          auto            color = measured_iso->getDiracColor(array_idx);
#  ifdef MRF_RENDERING_MODE_SPECTRAL

          if (color.size() >= 0)
          {
            mi_color.r = 2.f * color[0];
            mi_color.g = 2.f * color[1];
            mi_color.b = 2.f * color[2];
          }



          mdlMaterial.set_value("dirac_measurement", array_idx, mi_color);
#  else
          mi_color.r = 2.f * color.r();
          mi_color.g = 2.f * color.g();
          mi_color.b = 2.f * color.b();


#  endif
          mdlMaterial.set_value("dirac_measurement", array_idx, mi_color);
        }
      }


      continue;
    }
    auto phong = dynamic_cast<mrf::materials::LPhysicalPhong *>(material);
    if (phong)
    {
      _loger.trace("Create Iray L_PHONG material_id=", material_id);

      //PHONG
      mi::base::Handle<mi::neuraylib::IValue> specular_value(value_factory->create_color(1, 1, 1));

      mi::base::Handle<mi::neuraylib::IValue> diffuse_value(value_factory->create_color(1, 1, 1));

      mi::base::Handle<mi::neuraylib::IValue> roughness(value_factory->create_float(1.f));

      mi::base::Handle<mi::neuraylib::IExpression> specular_exp(
          expression_factory->create_constant(specular_value.get()));

      mi::base::Handle<mi::neuraylib::IExpression> diffuse_exp(
          expression_factory->create_constant(diffuse_value.get()));

      mi::base::Handle<mi::neuraylib::IExpression> roughness_exp(expression_factory->create_constant(roughness.get()));

      arguments->add_expression("diffuse_color", diffuse_exp.get());
      arguments->add_expression("specular_color", specular_exp.get());
      arguments->add_expression("roughness", roughness_exp.get());

      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::main::phong"));
      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));

      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }

    auto mirror = dynamic_cast<mrf::materials::PerfectMirror *>(material);
    if (mirror)
    {
      _loger.trace("Create Iray MIRROR material_id=", material_id);

      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::main::perfect_mirror"));

      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }

    auto ggx = dynamic_cast<mrf::materials::GGX *>(material);
    if (ggx)
    {
      _loger.trace("Create Iray GGX material_id=", material_id);

      mi::base::Handle<mi::neuraylib::IValue> ior_value(value_factory->create_color(1, 1, 1));

      mi::base::Handle<mi::neuraylib::IExpression> ior_exp(expression_factory->create_constant(ior_value.get()));

      mi::base::Handle<mi::neuraylib::IValue> roughness_u_value(value_factory->create_float(1.f));

      mi::base::Handle<mi::neuraylib::IExpression> roughness_u_exp(
          expression_factory->create_constant(roughness_u_value.get()));

      mi::base::Handle<mi::neuraylib::IValue> roughness_v_value(value_factory->create_float(1.f));

      mi::base::Handle<mi::neuraylib::IExpression> roughness_v_exp(
          expression_factory->create_constant(roughness_v_value.get()));

      arguments->add_expression("roughness_u", roughness_u_exp.get());
      arguments->add_expression("roughness_v", roughness_v_exp.get());
      arguments->add_expression("ior", ior_exp.get());

      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::main::ggx_conductor"));
      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }

    auto aniso_ggx = dynamic_cast<mrf::materials::AnisoGGX *>(material);
    if (aniso_ggx)
    {
      _loger.trace("Create Iray AnisoGGX material_id=", material_id);

      mi::base::Handle<mi::neuraylib::IValue> ior_value(value_factory->create_color(1, 1, 1));

      mi::base::Handle<mi::neuraylib::IExpression> ior_exp(expression_factory->create_constant(ior_value.get()));

      mi::base::Handle<mi::neuraylib::IValue> roughness_u_value(value_factory->create_float(1.f));

      mi::base::Handle<mi::neuraylib::IExpression> roughness_u_exp(
          expression_factory->create_constant(roughness_u_value.get()));

      mi::base::Handle<mi::neuraylib::IValue> roughness_v_value(value_factory->create_float(1.f));

      mi::base::Handle<mi::neuraylib::IExpression> roughness_v_exp(
          expression_factory->create_constant(roughness_v_value.get()));

      arguments->add_expression("roughness_u", roughness_u_exp.get());
      arguments->add_expression("roughness_v", roughness_v_exp.get());
      arguments->add_expression("ior", ior_exp.get());

      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::main::ggx_conductor"));
      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }

    auto walter_bsdf = dynamic_cast<mrf::materials::_WalterBSDF<mrf::materials::GGX> *>(material);
    if (walter_bsdf)
    {
      _loger.trace("Create Iray Walter material_id=", material_id);

      mi::base::Handle<mi::neuraylib::IValue> ior_value(value_factory->create_color(1, 1, 1));

      mi::base::Handle<mi::neuraylib::IExpression> ior_exp(expression_factory->create_constant(ior_value.get()));

      mi::base::Handle<mi::neuraylib::IValue> roughness_u_value(value_factory->create_float(1.f));

      mi::base::Handle<mi::neuraylib::IExpression> roughness_u_exp(
          expression_factory->create_constant(roughness_u_value.get()));

      mi::base::Handle<mi::neuraylib::IValue> roughness_v_value(value_factory->create_float(1.f));

      mi::base::Handle<mi::neuraylib::IExpression> roughness_v_exp(
          expression_factory->create_constant(roughness_v_value.get()));

      arguments->add_expression("roughness_u", roughness_u_exp.get());
      arguments->add_expression("roughness_v", roughness_v_exp.get());
      arguments->add_expression("ior", ior_exp.get());

      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::main::walter_ggx"));
      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }

    auto uniform_light = dynamic_cast<mrf::materials::UniformEmittance *>(material);
    if (uniform_light)
    {
      _loger.trace("Create Iray UNIFORM EMITTANCE material_id=", material_id);

      mi::base::Handle<mi::neuraylib::IValue> tint_value;
      tint_value = value_factory->create_color(1, 1, 1);

      mi::base::Handle<mi::neuraylib::IExpression> tint_expression(
          expression_factory->create_constant(tint_value.get()));

      arguments->add_expression("tint", tint_expression.get());

      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::mrf_lights::diffuse_light"));
      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }

    auto diffuse_light = dynamic_cast<mrf::materials::DiffuseEmittance *>(material);
    if (diffuse_light)
    {
      mi::base::Handle<mi::neuraylib::IValue> tint_value;
      tint_value = value_factory->create_color(1, 1, 1);

      _loger.trace("Create Iray DIFFUSE EMITTANCE material_id=", material_id);

      mi::base::Handle<mi::neuraylib::IExpression> tint_expression(
          expression_factory->create_constant(tint_value.get()));

      arguments->add_expression("tint", tint_expression.get());

      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::mrf_lights::diffuse_light"));
      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }

    auto conic_emitt = dynamic_cast<mrf::materials::ConicalEmittance *>(material);
    if (conic_emitt)
    {
      /*
      color = conic_emitt->getColor() * conic_emitt->getRadiance();

      #ifdef MRF_RENDERING_MODE_SPECTRAL
      mi::base::Handle<mi::neuraylib::IValue> tint_value(
        value_factory->create_color(0, 0, 0));
      #else
      mi::base::Handle<mi::neuraylib::IValue> tint_value(
        value_factory->create_color(color.r(), color.g(), color.b()));
      #endif

      _loger.trace("Create Iray CONICAL EMITTANCE RGB ", color);

      mi::base::Handle<mi::neuraylib::IExpression> tint_expression(
        expression_factory->create_constant(tint_value.get()));

      arguments->add_expression("tint", tint_expression.get());


      mi::base::Handle<mi::neuraylib::IValue> exponent_value(
        value_factory->create_float(1.f));

      mi::base::Handle<mi::neuraylib::IExpression> exponent_expression(
        expression_factory->create_constant(exponent_value.get()));

      arguments->add_expression("exponent", exponent_expression.get());


      mi::base::Handle<mi::neuraylib::IValue> spread_value(
        value_factory->create_float(conic_emitt->apertureRad()));

      mi::base::Handle<mi::neuraylib::IExpression> spread_expression(
        expression_factory->create_constant(spread_value.get()));

      arguments->add_expression("spread", spread_expression.get());


      auto light_direction = conic_emitt->direction();

      mi::Float64_4_4 light_rotation_matrix(1.0);
      mrfLightDirectionToIrayMatrix(light_direction, mrf::math::Vec3f(0, 0, 1), light_rotation_matrix);

      light_matrix = light_matrix * light_rotation_matrix;

      mi = md_spot_light->create_material_instance(arguments.get());
      emittance_type_found = true;
      */

      mi::base::Handle<mi::neuraylib::IValue> tint_value;
      tint_value = value_factory->create_color(1, 1, 1);

      _loger.trace("Create Iray CONIC EMITTANCE material_id=", material_id);

      mi::base::Handle<mi::neuraylib::IExpression> tint_expression(
          expression_factory->create_constant(tint_value.get()));

      arguments->add_expression("tint", tint_expression.get());

      mi::base::Handle<mi::neuraylib::IValue> exponent_value(value_factory->create_float(1.f));

      mi::base::Handle<mi::neuraylib::IExpression> exponent_expression(
          expression_factory->create_constant(exponent_value.get()));

      arguments->add_expression("exponent", exponent_expression.get());


      mi::base::Handle<mi::neuraylib::IValue> spread_value(value_factory->create_float(conic_emitt->apertureRad()));

      mi::base::Handle<mi::neuraylib::IExpression> spread_expression(
          expression_factory->create_constant(spread_value.get()));

      arguments->add_expression("spread", spread_expression.get());

      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::mrf_lights::spot_light"));

      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }

    auto dirac_emitt = dynamic_cast<mrf::materials::DiracEmittance *>(material);
    if (dirac_emitt)
    {
      //auto color = dirac_emitt->getColor() * dirac_emitt->getRadiance();
      float power = dirac_emitt->getRadiance();

      mi::base::Handle<mi::neuraylib::IValue> tint_value;
      tint_value = value_factory->create_color(power, power, power);

      _loger.trace("Create Iray DIRAC EMITTANCE material_id = ", material_id);
      _loger.trace("     power = ", power);

      mi::base::Handle<mi::neuraylib::IExpression> tint_expression(
          expression_factory->create_constant(tint_value.get()));

      arguments->add_expression("tint", tint_expression.get());

      mi::base::Handle<const mi::neuraylib::IMaterial_definition> md(
          transaction->access<mi::neuraylib::IMaterial_definition>("mdl::mrf_lights::diffuse_light"));
      mi::base::Handle<mi::neuraylib::IMaterial_instance> mi(md->create_material_instance(arguments.get()));
      check_success(mi.get());
      transaction->store(mi.get(), material_name.c_str());

      continue;
    }
  }
}

std::string get_mdl_type_name(const mi::neuraylib::IType *t)
{
  switch (t->get_kind())
  {
    case mi::neuraylib::IType::Kind::TK_ALIAS:
    {
      mi::base::Handle<const mi::neuraylib::IType_alias> alias_type(
          t->get_interface<const mi::neuraylib::IType_alias>());
      if (alias_type->get_symbol())
        return std::string(alias_type->get_symbol());
      else
        return get_mdl_type_name(alias_type->get_aliased_type());
    }
    case mi::neuraylib::IType::Kind::TK_ARRAY:
    {
      mi::base::Handle<const mi::neuraylib::IType_array> arr_type(t->get_interface<const mi::neuraylib::IType_array>());
      std::string                                        r = get_mdl_type_name(arr_type->get_element_type());
      /*
      if (arr_type->is_immediate_sized())
        r += "[" + to_string(arr_type->get_size()) + "]";
      else
        r += "[]";*/
      return r;
    }
    case mi::neuraylib::IType::Kind::TK_BOOL:
      return "bool";
    case mi::neuraylib::IType::Kind::TK_BSDF:
      return "bsdf";
    case mi::neuraylib::IType::Kind::TK_BSDF_MEASUREMENT:
      return "bsdf_measurement";
    case mi::neuraylib::IType::Kind::TK_COLOR:
      return "color";
    case mi::neuraylib::IType::Kind::TK_DOUBLE:
      return "double";
    case mi::neuraylib::IType::Kind::TK_EDF:
      return "edf";
    case mi::neuraylib::IType::Kind::TK_ENUM:
    {
      mi::base::Handle<const mi::neuraylib::IType_enum> enum_type(t->get_interface<const mi::neuraylib::IType_enum>());
      return enum_type->get_symbol();
    }
    case mi::neuraylib::IType::Kind::TK_FLOAT:
      return "float";
    case mi::neuraylib::IType::Kind::TK_INT:
      return "int";
    case mi::neuraylib::IType::Kind::TK_LIGHT_PROFILE:
      return "light_profile";
    case mi::neuraylib::IType::Kind::TK_MATRIX:
    {
      mi::base::Handle<const mi::neuraylib::IType_matrix> m_type(t->get_interface<const mi::neuraylib::IType_matrix>());

      mi::base::Handle<const mi::neuraylib::IType_vector> v_type(m_type->get_element_type());

      const mi::Size n_col = m_type->get_size();
      const mi::Size n_row = v_type->get_size();
      /*
      std::string r = to_string(n_col) + "x" + to_string(n_row);
      r = get_mdl_type_name(v_type->get_element_type()) + r;
      return r;
      */
      return "matrix";
    }
    case mi::neuraylib::IType::Kind::TK_STRING:
      return "string";
    case mi::neuraylib::IType::Kind::TK_STRUCT:
    {
      mi::base::Handle<const mi::neuraylib::IType_struct> struct_type(
          t->get_interface<const mi::neuraylib::IType_struct>());
      return struct_type->get_symbol();
    }
    case mi::neuraylib::IType::Kind::TK_TEXTURE:
      return "texture";
    case mi::neuraylib::IType::Kind::TK_VDF:
      return "vdf";
    case mi::neuraylib::IType::Kind::TK_VECTOR:
    {
      /*
       mi::base::Handle<const mi::neuraylib::IType_vector> v_type(
         t->get_interface<const mi::neuraylib::IType_vector>());
       std::string r = get_mdl_type_name(v_type->get_element_type()) + to_string(v_type->get_size());
       return r;*/
      return "vector";
    }
    default:
      break;
  }
  return "";
}

bool IrayMaterialHandler::updateMaterials(
    mi::base::Handle<mi::neuraylib::IMdl_factory> &mdl_factory,
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    uint                                           num_rendering_pass,
    std::vector<uint> const &                      spectral_wavelengths)
{
  bool an_error_occured = false;

  for (uint material_id = 0; material_id < _mrf_materials.size(); material_id++)
  {
    //create mdl material editor to edit material values
    mi::neuraylib::Argument_editor mdlMaterial(
        transaction.get(),
        _iray_material_names[material_id].c_str(),
        mdl_factory.get());


    auto lambert = dynamic_cast<mrf::materials::Lambert *>(_mrf_materials[material_id]);
    if (lambert)
    {
      float albedo = lambert->diffuseAlbedo();

      an_error_occured = updateColor(
          mdlMaterial,
          albedo,
          lambert->getColor(),
          num_rendering_pass,
          spectral_wavelengths,
          "tint",
          "LAMBERT");
      continue;
    }

    auto measured = dynamic_cast<mrf::materials::MeasuredIsotropic *>(_mrf_materials[material_id]);
    if (measured)
    {
      /*
      _loger.trace("Update Iray MEASURED material_id=", material_id);
      mi::Sint32 result;

      uint alta_d_phi_resolution = 360;
      uint resolution_theta = 180;//  std::stoi(alta_header.header()["#GRID"].front());
      uint resolution_phi = 180;//alta_d_phi_resolution / 2;

      std::string bsdf_measurement_name = _iray_material_names[material_id] + "_measurement";

      mi::base::Handle<mi::neuraylib::IBsdf_measurement> bsdf_measurement = mi::base::Handle<mi::neuraylib::IBsdf_measurement>(
        transaction->edit<mi::neuraylib::IBsdf_measurement>(bsdf_measurement_name.c_str()) );

      mi::base::Handle<mi::neuraylib::Bsdf_isotropic_data> bsdfData = mi::base::Handle<mi::neuraylib::Bsdf_isotropic_data>(
        new mi::neuraylib::Bsdf_isotropic_data(resolution_theta, resolution_phi, mi::neuraylib::BSDF_RGB) );

      mi::base::Handle<mi::neuraylib::Bsdf_buffer> bsdfBuffer(bsdfData->get_bsdf_buffer());
      check_success(bsdfBuffer.is_valid_interface());

      // Get a handle on the data buffer and populate (in this case red everywhere)
      mi::Float32* dataBuffer = bsdfBuffer->get_data();

      memset(dataBuffer, 1, sizeof(mi::Float32) * 3 * resolution_theta*resolution_theta*resolution_phi);

      mi::Sint32 err = bsdf_measurement->set_reflection(bsdfData.get());
      check_success(err == 0);

      //need to find a way to release memory here


      //delete bsdfData;
      */

      //auto bsdf_measurement = transaction->edit(bsdf_measurement_name.c_str());
      continue;
    }


    auto checkerboard = dynamic_cast<mrf::materials::CheckerBoard *>(_mrf_materials[material_id]);
    if (checkerboard)
    {
      // Create the MDL material
      _loger.trace("Create Iray CHECKERBOARD material_id=", material_id);

      float albedo = 1.0;

      an_error_occured = updateColor(
          mdlMaterial,
          albedo,
          checkerboard->getColor1(),
          num_rendering_pass,
          spectral_wavelengths,
          "dark",
          "CheckerBoard");
      an_error_occured = updateColor(
          mdlMaterial,
          albedo,
          checkerboard->getColor2(),
          num_rendering_pass,
          spectral_wavelengths,
          "light",
          "CheckerBoard");
      continue;
    }

    auto mirror = dynamic_cast<mrf::materials::PerfectMirror *>(_mrf_materials[material_id]);
    if (mirror)
    {
      continue;
    }

    auto phong = dynamic_cast<mrf::materials::LPhysicalPhong *>(_mrf_materials[material_id]);
    if (phong)
    {
      _loger.trace("Update Iray Phong material_id=", material_id);

      an_error_occured = updateColor(
          mdlMaterial,
          phong->diffuseAlbedo(),
          phong->getDiffuseColor(),
          num_rendering_pass,
          spectral_wavelengths,
          "diffuse_color",
          "LPHONG");
      an_error_occured = updateColor(
          mdlMaterial,
          phong->specularAlbedo(),
          phong->getSpecularColor(),
          num_rendering_pass,
          spectral_wavelengths,
          "specular_color",
          "LPHONG");

      float phong_roughness = std::sqrt(2.f / phong->getExponent());

      if (mdlMaterial.set_value("roughness", mi::Float32(phong_roughness)) != 0)
      {
        _loger.fatal("CANNOT UPDATE Iray LPHONG");
        an_error_occured = true;
        continue;
      }
      else
      {
        _loger.trace("UPDATE Iray Phong, roughness = ", phong_roughness);
      }

      continue;
    }

    //GGX
    auto ggx = dynamic_cast<mrf::materials::GGX *>(_mrf_materials[material_id]);
    if (ggx)
    {
      updateIor(mdlMaterial, ggx->eta(), ggx->k(), num_rendering_pass, spectral_wavelengths, "ior", "GGX");

      if (mdlMaterial.set_value("roughness_u", ggx->alpha_g()) != 0)
      {
        _loger.fatal("CANNOT UPDATE Iray GGX RGB");
        an_error_occured = true;
        continue;
      }
      _loger.trace("UPDATE Iray GGX RGB roughness_u=", ggx->alpha_g());
      if (mdlMaterial.set_value("roughness_v", ggx->alpha_g()) != 0)
      {
        _loger.fatal("CANNOT UPDATE Iray GGX RGB");
        an_error_occured = true;
        continue;
      }
      _loger.trace("UPDATE Iray GGX RGB roughness_v=", ggx->alpha_g());
    }

    auto aniso_ggx = dynamic_cast<mrf::materials::AnisoGGX *>(_mrf_materials[material_id]);
    if (aniso_ggx)
    {
#  ifdef MRF_RENDERING_MODE_SPECTRAL
      float spectrum_1, spectrum_2, spectrum_3;

      if ((3 * (num_rendering_pass - 1)) < spectral_wavelengths.size())
        spectrum_1 = aniso_ggx->eta().findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1)]);
      else
        spectrum_1 = 0.f;

      if ((3 * (num_rendering_pass - 1) + 1) < spectral_wavelengths.size())
        spectrum_2
            = aniso_ggx->eta().findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1) + 1]);
      else
        spectrum_2 = 0.f;

      if ((3 * (num_rendering_pass - 1) + 2) < spectral_wavelengths.size())
        spectrum_3
            = aniso_ggx->eta().findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1) + 2]);
      else
        spectrum_3 = 0.f;


      mrf::color::Color eta(spectrum_1, spectrum_2, spectrum_3);

      if ((3 * (num_rendering_pass - 1)) < spectral_wavelengths.size())
        spectrum_1 = aniso_ggx->k().findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1)]);
      else
        spectrum_1 = 0.f;

      if ((3 * (num_rendering_pass - 1) + 1) < spectral_wavelengths.size())
        spectrum_2
            = aniso_ggx->k().findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1) + 1]);
      else
        spectrum_2 = 0.f;

      if ((3 * (num_rendering_pass - 1) + 2) < spectral_wavelengths.size())
        spectrum_3
            = aniso_ggx->k().findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1) + 2]);
      else
        spectrum_3 = 0.f;

      mrf::color::Color kappa(spectrum_1, spectrum_2, spectrum_3);
#  else
      mrf::color::Color eta   = aniso_ggx->eta();
      mrf::color::Color kappa = aniso_ggx->k();
#  endif

      mrf::color::Color one(1.0);

      mrf::color::Color temp_r
          = ((eta - one) * (eta - one) + kappa * kappa) / ((eta + one) * (eta + one) + kappa * kappa);
      mrf::color::Color sqrt_r(sqrtf(temp_r.r()), sqrtf(temp_r.g()), sqrtf(temp_r.b()));
      mrf::color::Color eta_0 = (one + sqrt_r) / (one - sqrt_r);

      if (mdlMaterial.set_value("ior", mi::Color(eta_0.r(), eta_0.g(), eta_0.b())) != 0)
      {
        _loger.fatal("CANNOT UPDATE Iray AnisoGGX RGB");
        an_error_occured = true;
        continue;
      }
      _loger.trace("UPDATE Iray AnisoGGX RGB ior=", eta_0);

      if (mdlMaterial.set_value("roughness_u", aniso_ggx->alpha_x()) != 0)
      {
        _loger.fatal("CANNOT UPDATE Iray AnisoGGX RGB");
        an_error_occured = true;
        continue;
      }
      _loger.trace("UPDATE Iray AnisoGGX RGB roughness_u=", aniso_ggx->alpha_x());
      if (mdlMaterial.set_value("roughness_v", aniso_ggx->alpha_y()) != 0)
      {
        _loger.fatal("CANNOT UPDATE Iray AnisoGGX RGB");
        an_error_occured = true;
        continue;
      }
      _loger.trace("UPDATE Iray AnisoGGX RGB roughness_v=", aniso_ggx->alpha_y());
    }

    auto walter_bsdf = dynamic_cast<mrf::materials::_WalterBSDF<mrf::materials::GGX> *>(_mrf_materials[material_id]);
    if (walter_bsdf)
    {
      updateIor(
          mdlMaterial,
          walter_bsdf->eta(),
          walter_bsdf->k(),
          num_rendering_pass,
          spectral_wavelengths,
          "ior",
          "WALTER");

      if (mdlMaterial.set_value("roughness_u", walter_bsdf->alpha_g()) != 0)
      {
        _loger.fatal("CANNOT UPDATE Iray WALTER RGB");
        an_error_occured = true;
        continue;
      }
      _loger.trace("UPDATE Iray WALTER RGB roughness_u=", walter_bsdf->alpha_g());
      if (mdlMaterial.set_value("roughness_v", walter_bsdf->alpha_g()) != 0)
      {
        _loger.fatal("CANNOT UPDATE Iray WALTER RGB");
        an_error_occured = true;
        continue;
      }
      _loger.trace("UPDATE Iray WALTER RGB roughness_v=", walter_bsdf->alpha_g());
    }


    auto uniform_light = dynamic_cast<mrf::materials::UniformEmittance *>(_mrf_materials[material_id]);
    if (uniform_light)
    {
      float radiance   = uniform_light->getRadiance();
      an_error_occured = updateColor(
          mdlMaterial,
          radiance,
          uniform_light->getColor(),
          num_rendering_pass,
          spectral_wavelengths,
          "tint",
          "UNIFORM EMITTANCE");
      continue;
    }

    auto diffuse_light = dynamic_cast<mrf::materials::DiffuseEmittance *>(_mrf_materials[material_id]);
    if (diffuse_light)
    {
      float radiance   = diffuse_light->getRadiance();
      an_error_occured = updateColor(
          mdlMaterial,
          radiance,
          diffuse_light->getColor(),
          num_rendering_pass,
          spectral_wavelengths,
          "tint",
          "DIFFUSE EMITTANCE");
      continue;
    }

    auto spot_light = dynamic_cast<mrf::materials::ConicalEmittance *>(_mrf_materials[material_id]);
    if (spot_light)
    {
      float radiance   = spot_light->getRadiance();
      an_error_occured = updateColor(
          mdlMaterial,
          radiance,
          spot_light->getColor(),
          num_rendering_pass,
          spectral_wavelengths,
          "tint",
          "CONICAL EMITTANCE");
      continue;
    }

    auto directionnal_light = dynamic_cast<mrf::materials::DiracEmittance *>(_mrf_materials[material_id]);
    if (directionnal_light)
    {
      float radiance   = directionnal_light->getRadiance();
      an_error_occured = updateColor(
          mdlMaterial,
          radiance,
          directionnal_light->getColor(),
          num_rendering_pass,
          spectral_wavelengths,
          "tint",
          "DIRAC EMITTANCE");
      continue;
    }
  }
  return an_error_occured;
}

bool IrayMaterialHandler::updateColor(
    mi::neuraylib::Argument_editor &mdl_material,
    float const &                   factor,
    mrf::materials::COLOR const &   color,
    mrf::uint const &               num_rendering_pass,
    std::vector<uint> const &       spectral_wavelengths,
    std::string const &             iray_material_parameter,
    std::string const &             material_type)
{
  if (num_rendering_pass == 0)
  {
#  ifndef MRF_RENDERING_MODE_SPECTRAL
    if (mdl_material.set_value(
            iray_material_parameter.c_str(),
            mi::Color(factor * color.r(), factor * color.g(), factor * color.b()))
        != 0)
    {
      std::string info = "CANNOT UPDATE Iray " + material_type + " RGB";
      _loger.fatal(info);
      return false;
    }
    else
    {
      std::string info = "UPDATE Iray " + material_type + " RGB ";
      _loger.trace(info, mrf::color::Color(color * factor));
      return true;
    }
#  else
    return false;
#  endif
  }
  else
  {
#  ifdef MRF_RENDERING_MODE_SPECTRAL
    float spectrum_1, spectrum_2, spectrum_3;

    if ((3 * (num_rendering_pass - 1)) < spectral_wavelengths.size())
      spectrum_1 = factor * color.findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1)]);
    else
      spectrum_1 = 0.f;

    if ((3 * (num_rendering_pass - 1) + 1) < spectral_wavelengths.size())
      spectrum_2 = factor * color.findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1) + 1]);
    else
      spectrum_2 = 0.f;

    if ((3 * (num_rendering_pass - 1) + 2) < spectral_wavelengths.size())
      spectrum_3 = factor * color.findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1) + 2]);
    else
      spectrum_3 = 0.f;

    if (mdl_material.set_value(iray_material_parameter.c_str(), mi::Color(spectrum_1, spectrum_2, spectrum_2)) != 0)
    {
      std::string info = "CANNOT UPDATE Iray " + material_type + " SPECTRAL";
      _loger.fatal(info);
      return false;
    }
    else
    {
      std::string info = "UPDATE Iray " + material_type + " SPECTRAL:";
      _loger.trace(info, mrf::color::Color(spectrum_1, spectrum_2, spectrum_3));
      return true;
    }
#  else
    std::string info = "UPDATE Iray " + material_type + "  Spectral NOT SUPPORTED ";
    _loger.fatal(info);
    return false;
#  endif
  }
}

bool IrayMaterialHandler::updateIor(
    mi::neuraylib::Argument_editor &mdl_material,
    mrf::materials::COLOR const &   eta,
    mrf::materials::COLOR const &   kappa,
    mrf::uint const &               num_rendering_pass,
    std::vector<uint> const &       spectral_wavelengths,
    std::string const &             iray_material_parameter,
    std::string const &             material_type)
{
  mrf::color::Color e;
  mrf::color::Color k;

  if (num_rendering_pass == 0)
  {
#  ifndef MRF_RENDERING_MODE_SPECTRAL
    e = eta;
    k = kappa;
#  else
    return false;
#  endif
  }
  else
  {
#  ifdef MRF_RENDERING_MODE_SPECTRAL
    float spectrum_1, spectrum_2, spectrum_3;

    if ((3 * (num_rendering_pass - 1)) < spectral_wavelengths.size())
      spectrum_1 = eta.findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1)]);
    else
      spectrum_1 = 0.f;

    if ((3 * (num_rendering_pass - 1) + 1) < spectral_wavelengths.size())
      spectrum_2 = eta.findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1) + 1]);
    else
      spectrum_2 = 0.f;

    if ((3 * (num_rendering_pass - 1) + 2) < spectral_wavelengths.size())
      spectrum_3 = eta.findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1) + 2]);
    else
      spectrum_3 = 0.f;


    e = mrf::color::Color(spectrum_1, spectrum_2, spectrum_3);


    if ((3 * (num_rendering_pass - 1)) < spectral_wavelengths.size())
      spectrum_1 = kappa.findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1)]);
    else
      spectrum_1 = 0.f;

    if ((3 * (num_rendering_pass - 1) + 1) < spectral_wavelengths.size())
      spectrum_2 = kappa.findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1) + 1]);
    else
      spectrum_2 = 0.f;

    if ((3 * (num_rendering_pass - 1) + 2) < spectral_wavelengths.size())
      spectrum_3 = kappa.findLinearyInterpolatedValue(spectral_wavelengths[3 * (num_rendering_pass - 1) + 2]);
    else
      spectrum_3 = 0.f;

    k = mrf::color::Color(spectrum_1, spectrum_2, spectrum_3);




#  else
    std::string info = "UPDATE Iray " + material_type + "  Spectral NOT SUPPORTED ";
    _loger.fatal(info);
    return false;
#  endif
  }

  _loger.info("ETTAAAAA", e);
  _loger.info("KAAPPPAA", k);

  mrf::color::Color one(1.0);

  mrf::color::Color temp_r = ((e - one) * (e - one) + k * k) / ((e + one) * (e + one) + k * k);
  mrf::color::Color sqrt_r(sqrtf(temp_r.r()), sqrtf(temp_r.g()), sqrtf(temp_r.b()));
  mrf::color::Color e_0 = (one + sqrt_r) / (one - sqrt_r);

  if (std::isnan(e_0.r()) || std::isnan(e_0.g()) || std::isnan(e_0.b()))
  {
    std::string info = "UPDATE Iray " + material_type + "has some nan value -> set to 0";
    _loger.fatal(info);
    e_0 = mrf::color::Color(0.f);
  }

  if (mdl_material.set_value(iray_material_parameter.c_str(), mi::Color(e_0.r(), e_0.g(), e_0.b())) != 0)
  {
    std::string info = "CANNOT UPDATE Iray " + material_type;
    _loger.fatal(info);
    return false;
  }
  else
  {
    std::string info = "UPDATE Iray " + material_type + " :";
    _loger.trace(info, e_0);
    return true;
  }
}


bool IrayMaterialHandler::applyMaterial(mi::base::Handle<mi::neuraylib::IInstance> &mesh_instance, uint material_id)
{
  mi::base::Handle<mi::IRef> iray_material;

  if (!mesh_instance->is_attribute("material"))
  {
    iray_material = mesh_instance->create_attribute<mi::IRef>("material", "Ref");
  }
  else
  {
    iray_material = mesh_instance->edit_attribute<mi::IRef>("material");
  }

  //check if at least one index of material exist
  //if (mesh->materialPerFaces().size() > 0)

  //use shape material
  //TODO support material per face
  //uint material_id = mesh->materialPerFaces()[0];

  if (material_id < _mrf_materials.size())
  {
    //set the mesh material reference
    check_success(iray_material->set_reference(_iray_material_names[material_id].c_str()) == 0);
  }
  else
  {
    return false;
  }
  return true;
}

}   // namespace iray
}   // namespace mrf

#endif
