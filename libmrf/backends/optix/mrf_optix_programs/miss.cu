#include "include/path_tracer_definitions.h"
#include "include/lighting/envmap_definitions.h"

rtDeclareVariable(PerRayData_pathtrace, current_prd, rtPayload, );
rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, );
rtDeclareVariable(float3, shading_normal, attribute shading_normal, );

rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );


#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> bg_color;
#  else
rtDeclareVariable(float, bg_color, , );
#  endif
#else
rtDeclareVariable(float3, bg_color, , );
#endif


//TODO: place in material eval ? Or in a "miss.cu" ?

using namespace optix;
using namespace mrf;


//-----------------------------------------------------------------------------
//
//  Miss programs
//
//-----------------------------------------------------------------------------

RT_PROGRAM void miss()
{
  current_prd.done = true;
  if (current_prd.shadow_catcher > 0)
  {
    current_prd.result += current_prd.attenuation;
    return;
  }

  if (use_envmap == 1)
  {
    if (current_prd.countEmitted)
    {
#if MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
      COLOR   envmap_color;
      COLOR_B tmp_env = envmap_lookup(ray.direction);
      for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
      {
        envmap_color.value[i] = lerp(tmp_env.value[i], tmp_env.value[i + 1], current_prd.wavelength_offset);
      }

#else
      COLOR envmap_color = envmap_lookup(ray.direction);
#endif

      current_prd.result += envmap_color * current_prd.attenuation;
    }
  }
  else
  {
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    COLOR temp_bg_color;
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    {
      temp_bg_color.value[i] = lerp(bg_color[i], bg_color[i + 1], current_prd.wavelength_offset);
    }
#else
    COLOR temp_bg_color = bg_color;
#endif
    current_prd.result += temp_bg_color * current_prd.attenuation;
  }
}
