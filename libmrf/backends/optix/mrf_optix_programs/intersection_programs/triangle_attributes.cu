/*
 * Copyright (c) 2018 NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Adapted for MRF by
 * david murray @ institutoptique fr
 *
 *Copyright (c) 2020 CNRS
 */

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include <optixu/optixu_aabb_namespace.h>
#include "intersection_refinement.h"

using namespace optix;

// This is to be plugged into an RTgeometryTriangle object to compute attributes.

rtBuffer<float3> vertex_buffer;
rtBuffer<float3> normal_buffer;
rtBuffer<float2> texcoord_buffer;
rtBuffer<float3> tangent_buffer;
rtBuffer<int3>   index_buffer;
rtBuffer<uint>   mat_index_buffer;

rtDeclareVariable(float3, tangent, attribute tangent, );
rtDeclareVariable(float3, bitangent, attribute bitangent, );
rtDeclareVariable(float3, texcoord, attribute texcoord, );
rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, );
rtDeclareVariable(float3, shading_normal, attribute shading_normal, );

rtDeclareVariable(float3, back_hit_point, attribute back_hit_point, );
rtDeclareVariable(float3, front_hit_point, attribute front_hit_point, );

// rtDeclareVariable(uint, mat_index, attribute mat_index, );

rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );

// template<bool DO_REFINE>
RT_PROGRAM void triangle_attributes()
{
  // ray in object space
  optix::Ray os_ray = ray;
  os_ray.origin     = rtTransformPoint(RT_WORLD_TO_OBJECT, os_ray.origin);
  os_ray.direction  = rtTransformVector(RT_WORLD_TO_OBJECT, os_ray.direction);

  const float2 barycentrics = rtGetTriangleBarycentrics();
  float        beta         = barycentrics.x;
  float        gamma        = barycentrics.y;

  const int3   v_idx  = index_buffer[rtGetPrimitiveIndex()];
  const float3 v0     = vertex_buffer[v_idx.x];
  const float3 v1     = vertex_buffer[v_idx.y];
  const float3 v2     = vertex_buffer[v_idx.z];
  const float3 impact = v1 * beta + v2 * gamma + v0 * (1.0f - beta - gamma);
  float        t      = optix::length(impact - os_ray.origin);

  const float3 Ng  = optix::cross(v1 - v0, v2 - v0);
  geometric_normal = optix::normalize(Ng);

  if (normal_buffer.size() == 0)
  {
    shading_normal = geometric_normal;
  }
  else
  {
    float3 n0      = normal_buffer[v_idx.x];
    float3 n1      = normal_buffer[v_idx.y];
    float3 n2      = normal_buffer[v_idx.z];
    shading_normal = normalize(n1 * beta + n2 * gamma + n0 * (1.0f - beta - gamma));
  }

  // if (mat_index_buffer.size() == 0)
  // {
  // mat_index = 0;
  // }
  // else
  // {
  // mat_index = mat_index_buffer[rtGetPrimitiveIndex()];
  // }

  if (texcoord_buffer.size() == 0)
  {
    texcoord = make_float3(0.0f, 0.0f, 0.0f);
  }
  else
  {
    float2 t0 = texcoord_buffer[v_idx.x];
    float2 t1 = texcoord_buffer[v_idx.y];
    float2 t2 = texcoord_buffer[v_idx.z];
    texcoord  = make_float3(t1 * beta + t2 * gamma + t0 * (1.0f - beta - gamma));
  }

  if (tangent_buffer.size() == 0)
  {
    const float3 n = geometric_normal;
    if (n.z < -0.9999f)   //Singularity
    {
      tangent = make_float3(0.f, 1.f, 0.f);
    }
    else
    {
      tangent = normalize(cross(make_float3(0.f, 1.f, 0.f), n));
    }
    bitangent = normalize(cross(n, tangent));
  }
  else
  {
    float3 t0 = tangent_buffer[v_idx.x];
    float3 t1 = tangent_buffer[v_idx.y];
    float3 t2 = tangent_buffer[v_idx.z];
    tangent   = normalize(t1 * beta + t2 * gamma + t0 * (1.0f - beta - gamma));
    bitangent = cross(geometric_normal, tangent);
  }

  float3 front, back;
  refine_and_offset_hitpoint(
      os_ray.origin + t * os_ray.direction,
      os_ray.direction,
      geometric_normal,
      impact,
      back,
      front);
  back_hit_point  = rtTransformPoint(RT_OBJECT_TO_WORLD, impact);
  front_hit_point = rtTransformPoint(RT_OBJECT_TO_WORLD, impact);
}

RT_PROGRAM void triangle_attributes_refine()
{
  const float2 barycentrics = rtGetTriangleBarycentrics();
  float        beta         = barycentrics.x;
  float        gamma        = barycentrics.y;

  const int3   v_idx  = index_buffer[rtGetPrimitiveIndex()];
  const float3 v0     = vertex_buffer[v_idx.x];
  const float3 v1     = vertex_buffer[v_idx.y];
  const float3 v2     = vertex_buffer[v_idx.z];
  const float3 impact = v1 * beta + v2 * gamma + v0 * (1.0f - beta - gamma);
  float        t      = optix::length(impact - ray.origin);

  const float3 Ng  = optix::cross(v1 - v0, v2 - v0);
  geometric_normal = optix::normalize(Ng);

  if (normal_buffer.size() == 0)
  {
    shading_normal = geometric_normal;
  }
  else
  {
    float3 n0      = normal_buffer[v_idx.x];
    float3 n1      = normal_buffer[v_idx.y];
    float3 n2      = normal_buffer[v_idx.z];
    shading_normal = normalize(n1 * beta + n2 * gamma + n0 * (1.0f - beta - gamma));
  }

  if (texcoord_buffer.size() == 0)
  {
    texcoord = make_float3(0.0f, 0.0f, 0.0f);
  }
  else
  {
    float2 t0 = texcoord_buffer[v_idx.x];
    float2 t1 = texcoord_buffer[v_idx.y];
    float2 t2 = texcoord_buffer[v_idx.z];
    texcoord  = make_float3(t1 * beta + t2 * gamma + t0 * (1.0f - beta - gamma));
  }

  if (tangent_buffer.size() == 0)
  {
    const float3 n = shading_normal;
    if (n.z < -0.9999f)   //Singularity
    {
      tangent = make_float3(0.f, 1.f, 0.f);
    }
    else
    {
      tangent = normalize(cross(make_float3(0.f, 1.f, 0.f), n));
    }
    bitangent = normalize(cross(n, tangent));
  }
  else
  {
    float3 t0 = tangent_buffer[v_idx.x];
    float3 t1 = tangent_buffer[v_idx.y];
    float3 t2 = tangent_buffer[v_idx.z];
    tangent   = normalize(t1 * beta + t2 * gamma + t0 * (1.0f - beta - gamma));
    bitangent = normalize(cross(shading_normal, tangent));
  }


  float3 front, back;
  refine_and_offset_hitpoint(ray.origin + t * ray.direction, ray.direction, geometric_normal, impact, back, front);
  back_hit_point  = back;
  front_hit_point = front;
}
