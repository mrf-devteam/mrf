#include <optixu/optixu_math_namespace.h>
#include "include/path_tracer_definitions.h"

using namespace optix;

rtDeclareVariable(PerRayData_pathtrace, current_prd, rtPayload, );
rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, );
rtDeclareVariable(float3, shading_normal, attribute shading_normal, );
rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );

rtDeclareVariable(int, registered_as_light, , );

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> emission_color;
#  else
rtDeclareVariable(float, emission_color, , );
#  endif

#else
rtDeclareVariable(float3, emission_color, , );
#endif

RT_PROGRAM void diffuseEmitter()
{
  float3 world_shading_normal = normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, shading_normal));
  float  dot_ray              = dot(-ray.direction, world_shading_normal);

  //Primary rays get through emitters
  /* if(current_prd.depth==0)
   {
       float3 hitpoint = ray.origin + t_hit * ray.direction;
       current_prd.origin = hitpoint;
       current_prd.backface = true;
       return;
   }
  */
  bool count = true;
  if (registered_as_light > 0 && current_prd.countEmitted == false) count = false;

  if (dot_ray > 0 && count)
  {
    //current_prd.pathCounter++;

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    {
      const float emission_at_wl = lerp(emission_color[i], emission_color[i + 1], current_prd.wavelength_offset);
      current_prd.result.value[i] += emission_at_wl * current_prd.attenuation.value[i];
    }
#else
    current_prd.result += current_prd.attenuation * emission_color;
#endif
  }
  else
  {}

  //Stop path when hiting an emissive surface
  current_prd.done = true;
}