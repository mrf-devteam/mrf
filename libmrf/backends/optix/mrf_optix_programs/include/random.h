#pragma once

/*
 * Copyright (c) 2018 NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <optixu/optixu_math_namespace.h>

#ifdef OPTIX_RNG

#  define PRNG_FILTER_U 0
#  define PRNG_FILTER_V 0
//#define PRNG_LENS_U = 2
//#define PRNG_LENS_V = 3
//#define PRNG_BASE_NUM = 4

#  define PRNG_BSDF_U  0
#  define PRNG_BSDF_V  0
#  define PRNG_BSDF    0
#  define PRNG_LIGHT   0
#  define PRNG_LIGHT_U 0
#  define PRNG_LIGHT_V 0
//#define PRNG_LIGHT_F = 6
#  define PRNG_TERMINATE  0
#  define PRNG_BOUNCE_NUM 0

#endif
#ifdef SOBOL_RNG

#  define SOBOL_BITS           32
#  define SOBOL_MAX_DIMENSIONS 21201


//WARNING IF YOU CHANGE ANY OF THESE VALUES
//CHANGE ALSO THE ONES IN void OptixRenderer::updateSobolRandomNumbers()
#  define PRNG_FILTER_U 0
#  define PRNG_FILTER_V 1
#  define PRNG_LENS_U   = 2
#  define PRNG_LENS_V   = 3
#  define PRNG_BASE_NUM = 4

#  define PRNG_BSDF_U           0
#  define PRNG_BSDF_V           1
#  define PRNG_BSDF             4
#  define PRNG_LIGHT            5
#  define PRNG_LIGHT_U          2
#  define PRNG_LIGHT_V          3
#  define PRNG_RUSSIAN_ROULETTE 7
#  define PRNG_TERMINATE        6
#  define PRNG_BOUNCE_NUM       8
/*
  PRNG_FILTER_U = 0,
  PRNG_FILTER_V = 1,
  PRNG_LENS_U = 2,
  PRNG_LENS_V = 3,
  PRNG_TIME = 4,
  PRNG_UNUSED_0 = 5,*/
//PRNG_UNUSED_1 = 6,  /* for some reason (6, 7) is a bad sobol pattern */
//PRNG_UNUSED_2 = 7,  /* with a low number of samples (< 64) */
//PRNG_BASE_NUM = 10,
/*
  PRNG_BSDF_U = 0,
  PRNG_BSDF_V = 1,
  PRNG_LIGHT_U = 2,
  PRNG_LIGHT_V = 3,
  PRNG_LIGHT_TERMINATE = 4,
  PRNG_TERMINATE = 5,
  PRNG_PHASE_CHANNEL = 6,
  PRNG_SCATTER_DISTANCE = 7,
  PRNG_BOUNCE_NUM = 8,*/

rtBuffer<unsigned int, 1> sobol_buffer;
rtBuffer<float, 1>        cranley_patterson_buffer;

#endif
#ifdef HALTON_RNG

#  define PRNG_FILTER_U 0
#  define PRNG_FILTER_V 1
//#define PRNG_LENS_U = 2
//#define PRNG_LENS_V = 3
//#define PRNG_BASE_NUM = 4

#  define PRNG_BSDF_U  0
#  define PRNG_BSDF_V  1
#  define PRNG_BSDF    2
#  define PRNG_LIGHT   3
#  define PRNG_LIGHT_U 0
#  define PRNG_LIGHT_V 1
//#define PRNG_LIGHT_F = 6
#  define PRNG_TERMINATE  3
#  define PRNG_BOUNCE_NUM 4

rtBuffer<float, 1> cranley_patterson_buffer;


#endif


template<unsigned int N>
static __host__ __device__ __inline__ unsigned int tea(unsigned int val0, unsigned int val1)
{
  unsigned int v0 = val0;
  unsigned int v1 = val1;
  unsigned int s0 = 0;

  for (unsigned int n = 0; n < N; n++)
  {
    s0 += 0x9e3779b9;
    v0 += ((v1 << 4) + 0xa341316c) ^ (v1 + s0) ^ ((v1 >> 5) + 0xc8013ea4);
    v1 += ((v0 << 4) + 0xad90777d) ^ (v0 + s0) ^ ((v0 >> 5) + 0x7e95761e);
  }

  return v0;
}




// Generate random unsigned int in [0, 2^24)
static __host__ __device__ __inline__ unsigned int lcg(unsigned int &prev)
{
  const unsigned int LCG_A = 1664525u;
  const unsigned int LCG_C = 1013904223u;
  prev                     = (LCG_A * prev + LCG_C);
  return prev & 0x00FFFFFF;
}

static __host__ __device__ __inline__ unsigned int lcg2(unsigned int &prev)
{
  prev = (prev * 8121 + 28411) % 134456;
  return prev;
}



static __host__ __device__ __inline__ unsigned int rot_seed(unsigned int seed, unsigned int frame)
{
  return seed ^ frame;
}




// Generate random float in [0, 1)
static __host__ __device__ __inline__ float rnd(unsigned int &prev)
{
  return ((float)lcg(prev) / (float)0x01000000);
}


#ifdef OPTIX_RNG

static __host__ __device__ __inline__ float
sampling1D(int numSample, int numDimension, float offsetSampling, unsigned int &prev)
{
  return rnd(prev);
}

static __host__ __device__ __inline__ float2
sampling2D(int numSample, int numStartDimension, float2 offsetSampling, unsigned int &prev)
{
  return make_float2(rnd(prev), rnd(prev));
}

static __host__ __device__ __inline__ float2 getOffsetSampling(int pixelNum)
{
  return make_float2(0.f);
}
#endif




#ifdef HALTON_RNG

/*
vec2 hash2( const float n ) {
  return fract(sin(vec2(n,n+1.))*vec2(43758.5453123));
}

float hash( const float n ) {
  return fract(sin(n)*43758.5453123);
}
*/
static __host__ __device__ __inline__ float Halton(int index, int base)
{
  float res = 0.0;
  float f   = 1.0 / float(base);
  int   i   = index;
  while (i > 0)
  {
    res = res + f * (i % base);
    i   = i / base;   //int(floor(i/int(base);
    f   = f / float(base);
  }
  return res;
}
/*
floatHaltonCranley(in int index, in int base, in int offsetCranley)
{
  float res = 0.0;
  float f = 1.0 / float(base);
  int i = index;
  while(i>0)
  {
    res = res + f * (i % base);
    i = i/base;
    f = f / float(base);
  }
  res += cranleyPatterson_val[offsetCranley];
  return res - floor(res) ;//modulo 1.0
}
*/

const int prime_numbers[40]
    = {2,  3,  5,  7,  11, 13,  17,  19,  23,  29,  31,  37,  41,  43,  47,  53,  59,  61,  67,  71,
       73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 179};

static __host__ __device__ __inline__ float
sampling1D(int numSample, int numDimension, float offsetSampling, unsigned int &prev)
{
  float rand = Halton(numSample, prime_numbers[numDimension]);
  rand += offsetSampling;
  return rand - floor(rand);
}

static __host__ __device__ __inline__ float2
sampling2D(int numSample, int numStartDimension, float2 offsetSampling, unsigned int &prev)
{
  float2 rand = make_float2(
      Halton(numSample, prime_numbers[numStartDimension]),
      Halton(numSample, prime_numbers[numStartDimension + 1]));
  rand += offsetSampling;
  return rand - floor(rand);
}



static __host__ __device__ __inline__ float2 getOffsetSampling(int pixelNum)
{
  /*
   if(useJitter)
     return vec2(2.0*cranleyPatterson_val[2*pixelNum]-1.0,2.0*cranleyPatterson_val[2*pixelNum+1]-1.0) * vec2(jitterRadius,jitterRadius);
   else
   */
  return make_float2(cranley_patterson_buffer[2 * pixelNum], cranley_patterson_buffer[2 * pixelNum + 1]);
}


#endif


#ifdef SOBOL_RNG

//FROM BLENDER CYCLES
/* High Dimensional Sobol.
 *
 * Multidimensional sobol with generator matrices. Dimension 0 and 1 are equal
 * to classic Van der Corput and Sobol sequences. */

/* Skip initial numbers that for some dimensions have clear patterns that
 * don't cover the entire sample space. Ideally we would have a better
 * progressive pattern that doesn't suffer from this problem, because even
 * with this offset some dimensions are quite poor.
 */

#  define SOBOL_SKIP 64
static __host__ __device__ __inline__ unsigned int sobol_dimension(int index, int dimension)
//ccl_device uint sobol_dimension(KernelGlobals *kg, int index, int dimension)
{
  unsigned int result = 0;
  unsigned int i      = index + SOBOL_SKIP;
  for (unsigned int j = 0; i; i >>= 1, j++)
  {
    if (i & 1)
    {
      //result ^= kernel_tex_fetch(__sobol_directions, 32*dimension + j);
      //#define kernel_tex_fetch(tex, index) (kg->tex.fetch(index))
      result ^= sobol_buffer[32 * dimension + j];
    }
  }
  return result;
}

static __host__ __device__ __inline__ float
sampling1D(int numSample, int numDimension, float offsetSampling, unsigned int &prev)
/*ccl_device_forceinline float path_rng_1D(KernelGlobals *kg,
                                         uint rng_hash,
                                         int sample, int num_samples,
                                         int dimension)
                                         */
{
  /* Sobol sequence value using direction vectors. */
  unsigned int result = sobol_dimension(numSample, numDimension);
  float        r      = (float)result * (1.0f / (float)0xFFFFFFFF);


  /* Cranly-Patterson rotation using rng seed */
  // float shift;

  /* Hash rng with dimension to solve correlation issues.
   * See T38710, T50116.
   */
  //uint tmp_rng = cmj_hash_simple(dimension, rng_hash);
  //shift = tmp_rng * (1.0f/(float)0xFFFFFFFF);

  //return r + shift - floorf(r + shift);
  r += offsetSampling;
  return r - floor(r);
}

static __host__ __device__ __inline__ float2
sampling2D(int numSample, int numStartDimension, float2 offsetSampling, unsigned int &prev)
/*ccl_device_forceinline void path_rng_2D(KernelGlobals *kg,
                                        uint rng_hash,
                                        int sample, int num_samples,
                                        int dimension,
                                        float *fx, float *fy)*/
{
  /* Sobol. */
  //  *fx = path_rng_1D(kg, rng_hash, sample, num_samples, dimension);
  //  *fy = path_rng_1D(kg, rng_hash, sample, num_samples, dimension + 1);

  return make_float2(
      sampling1D(numSample, numStartDimension, offsetSampling.x, prev),
      sampling1D(numSample, numStartDimension + 1, offsetSampling.y, prev));
}

static __host__ __device__ __inline__ float2 getOffsetSampling(int pixelNum)
{
  /*
   if(useJitter)
     return vec2(2.0*cranleyPatterson_val[2*pixelNum]-1.0,2.0*cranleyPatterson_val[2*pixelNum+1]-1.0) * vec2(jitterRadius,jitterRadius);
   else
   */
  return make_float2(cranley_patterson_buffer[2 * pixelNum], cranley_patterson_buffer[2 * pixelNum + 1]);
  //return make_float2(0.f);
}

/*
ccl_device_inline void path_rng_init(KernelGlobals *kg,
                                     int sample, int num_samples,
                                     uint *rng_hash,
                                     int x, int y,
                                     float *fx, float *fy)
{

  *rng_hash = hash_int_2d(x, y);
  *rng_hash ^= kernel_data.integrator.seed;

#ifdef __DEBUG_CORRELATION__
  srand48(*rng_hash + sample);
#endif

  if(sample == 0) {
    *fx = 0.5f;
    *fy = 0.5f;
  }
  else {
    path_rng_2D(kg, *rng_hash, sample, num_samples, PRNG_FILTER_U, fx, fy);
  }
}
*/


#endif
