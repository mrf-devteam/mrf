
#pragma once

namespace mrf
{
namespace math
{
__host__ __device__ class Complex
{
public:
  float _a;
  float _b;
  Complex()
  {
    _a = 0.f;
    _b = 0.f;
  }
  Complex(float a, float b)
  {
    _a = a;
    _b = b;
  }
  Complex &operator=(Complex const &c)
  {
    _a = c._a;
    _b = c._b;
    return *this;
  }
  float real() const { return _a; }

  float img() const { return _b; }
};

// This is not the norm of a complex but its squared norm
__host__ __device__ float norm(mrf::math::Complex const &c)
{
  return c._a * c._a + c._b * c._b;
}
__host__ __device__ float abs(mrf::math::Complex const &c)
{
  float s = fmaxf(fabsf(c.real()), fabsf(c.img()));
  if (s == 0.f)
  {
    return s;
  }
  float x = c._a / s;
  float y = c._b / s;
  return s * sqrt(x * x + y * y);
}
__host__ __device__ Complex operator+(Complex const &c1, float const &f)
{
  Complex res;
  res._a = c1._a + f;
  res._b = c1._b;
  return res;
}
__host__ __device__ Complex operator+(Complex const &c1, Complex const &c2)
{
  Complex res;
  res._a = c1._a + c2._a;
  res._b = c1._b + c2._b;
  return res;
}
__host__ __device__ Complex operator-(Complex const &c1, Complex const &c2)
{
  Complex res;
  res._a = c1._a - c2._a;
  res._b = c1._b - c2._b;
  return res;
}
__host__ __device__ Complex operator-(float const &f, Complex const &c2)
{
  Complex res;
  res._a = f - c2._a;
  res._b = -c2._b;
  return res;
}
__host__ __device__ Complex operator*(Complex const &c1, Complex const &c2)
{
  Complex res;
  res._a = c1._a * c2._a - c1._b * c2._b;
  res._b = c1._a * c2._b + c1._b * c2._a;
  return res;
}
__host__ __device__ Complex operator*(Complex const &c1, float const &f)
{
  Complex res;
  res._a = c1._a * f;
  res._b = c1._b * f;
  return res;
}
__host__ __device__ Complex operator/(Complex const &c1, float const &f)
{
  Complex res;
  res._a = c1._a / f;
  res._b = c1._b / f;
  return res;
}
__host__ __device__ Complex operator/(Complex const &c1, Complex const &c2)
{
  Complex res;
  res._a = c1._a * c2._a + c1._b * c2._b;
  res._b = c1._b * c2._a - c1._a * c2._b;
  return res / norm(c2);
}

__host__ __device__ mrf::math::Complex sqrt(mrf::math::Complex const &c)
{
  mrf::math::Complex res;
  float              x = c.real();
  float              y = c.img();
  if (x == 0.f)
  {
    float t = sqrtf(fabsf(y) / 2.f);
    res._a  = t;
    if (y < 0.f)
      res._b = -t;
    else
      res._b = t;
  }
  else
  {
    float t = sqrtf(2.f * (abs(c) + fabsf(x)));
    float u = t / 2.f;
    if (x > 0.f)
    {
      res._a = u;
      res._b = y / t;
    }
    else
    {
      res._a = fabsf(y) / t;
      if (y < 0.f)
        res._b = -u;
      else
        res._b = u;
    }
  }
  return res;
}


}   // namespace math
}   // namespace mrf