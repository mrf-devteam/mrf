float balance_heuristic(int nf, float fPdf, int ng, float gPdf)
{
  return (nf * fPdf) / (nf * fPdf + ng * gPdf);
}

float power_heuristic(int nf, float fPdf, int ng, float gPdf)
{
  float f = nf * fPdf;
  float g = ng * gPdf;
  return (f * f) / (f * f + g * g);
}

float mis_heuristic(int nf, float fPdf, int ng, float gPdf)
{
  //return balance_heuristic(nf, fPdf, ng, gPdf);
  return power_heuristic(nf, fPdf, ng, gPdf);
}

COLOR eval_direct_light(
    float3 hitpoint,
    float3 n,
    float3 eye,
    float3 l,
    int    sampled_lobe,
    float  light_pdf,
    float  Ldist,
    bool   use_mis)
{
  //If light pdf > 0 for the light sampled direction, light is potentially reachable, so test and add contribution.
  if (light_pdf > 0.f)
  {
    PerRayData_pathtrace_shadow shadow_prd;
    shadow_prd.inShadow = false;
    // Note: bias both ends of the shadow ray, in case the light is also present as geometry in the scene.
    Ray shadow_ray = make_Ray(hitpoint, l, pathtrace_shadow_ray_type, scene_epsilon, Ldist - scene_epsilon);
    rtTrace(top_object, shadow_ray, shadow_prd);

    //Not in shadow, evaluate BRDF for light direction.
    if (!shadow_prd.inShadow)
    {
      COLOR result = bsdf_eval(n, eye, l, ANY_REFL);
      result *= abs(dot(n, l)) / light_pdf;
      result *= current_prd.attenuation;

      if (use_mis)
      {
        float brdf_pdf_value = bsdf_pdf(n, eye, l, ANY_REFL);
        if (brdf_pdf_value <= 0.f) return COLOR(0.f);
        float mis_weight = mis_heuristic(1, light_pdf, 1, brdf_pdf_value);
        result *= mis_weight;
      }

      // current_prd.countEmitted = false;   //Light has contributed -> do not count it on impact
      return result;
    }
  }

  return COLOR(0.f);
}

COLOR eval_direct_bsdf(
    float3 hitpoint,
    float3 n,
    float3 eye,
    float3 l,
    int    sampled_lobe,
    float  light_pdf,
    float  Ldist,
    COLOR  bsdf_optim)
{
  //If light pdf > 0 for the BRDF sampled direction, light is potentially reachable, so test and add contribution.
  if (light_pdf > 0.f)
  {
    PerRayData_pathtrace_shadow shadow_prd;
    shadow_prd.inShadow = false;
    // Note: bias both ends of the shadow ray, in case the light is also present as geometry in the scene.
    Ray shadow_ray = make_Ray(hitpoint, l, pathtrace_shadow_ray_type, scene_epsilon, Ldist - scene_epsilon);
    rtTrace(top_object, shadow_ray, shadow_prd);

    //Not in shadow, evaluate BRDF for BRDF sampled direction.
    if (!shadow_prd.inShadow)
    {
      float brdf_pdf_value = bsdf_pdf(n, eye, l, sampled_lobe);
      if (brdf_pdf_value <= 0.f) return COLOR(0.f);
      float mis_weight = mis_heuristic(1, brdf_pdf_value, 1, light_pdf);

      // COLOR result = bsdf_optim; // = bsdf * ndotl / pdf;
      COLOR result = bsdf_eval(n, eye, l, sampled_lobe) * abs(dot(l, n)) / brdf_pdf_value;
      result *= mis_weight;
      result *= current_prd.attenuation;

      // current_prd.countEmitted = false;   //Light has contributed -> do not count it on impact
      return result;
    }
  }

  return COLOR(0.f);
}