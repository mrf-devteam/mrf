#pragma once

#include <optixu/optixu_math_namespace.h>
#include <optix_host.h>

//to retrieve MRF_INTERACTIVE
#include <optix_backend_config.h>

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
// FOR MULTIPLEXING WE NEED A spectral distribution (SPD) per light source
rtBuffer<float, 1> emi_spd_quad_lights;     // (NB_MULTIPLEXED + 1) * nb_quad_lights
rtBuffer<float, 1> emi_spd_sphere_lights;   // (NB_MULTIPLEXED + 1) * nb_sphere_ights
rtBuffer<float, 1> emi_spd_dir_lights;      // (NB_MULTIPLEXED + 1) * nb_dir_lights
#endif

struct BaseLight
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  float emission;
#else
  optix::float3 emission;
#endif
};

struct QuadLight: public BaseLight
{
  optix::float3 corner;
  optix::float3 v1, v2;
  optix::float3 normal;
  //#ifdef MRF_RENDERING_MODE_SPECTRAL
  //  // #ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  //  // //  optix::Handle<optix::BufferObj> emission; // Contains a subset or all Spectral Disbribution of the light source
  //  //   //rtBuffer<float, 1> emission;
  //  //   float emission[NB_MAX_WAVES_MULTIPLEXED];
  //  // #else
  //  float emission;
  ////#endif
  //#else   // RGB MODE
  //  optix::float3 emission;
  //#endif
};

struct DirectionnalLight: public BaseLight
{
  optix::float3 direction;
};

struct SphereLight: public BaseLight
{
  optix::float3 position;
  float         radius;
};
