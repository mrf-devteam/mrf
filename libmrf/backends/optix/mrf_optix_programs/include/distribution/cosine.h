float cosine_distribution(float3 n, float3 in_dir, float3 out_dir)
{
  float cosTheta = dot(n, in_dir);
  if (cosTheta >= 0) return cosTheta;
  return 0;
}

float cosine_pdf(float3 n, float3 eye, float3 light)
{
  return dot(n, light) / M_PIf;
}

float3 cosine_sample(float z1, float z2, float3 in_dir, float3 normal)
{
  float3 p;
  cosine_sample_hemisphere(z1, z2, p);
  optix::Onb onb(normal);
  onb.inverse_transform(p);
  return p;
}