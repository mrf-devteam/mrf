namespace mrf
{
namespace spectrum
{
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED

// This structure is used for interpolating between two values in multiplexing.
// This contains a Spectrum defined for each boundaries of the multiplexed wavelengths.
// So, NB_MAX_WAVES_MULTIPLEXED + 1.
struct COLOR_B
{
  float value[NB_MAX_WAVES_MULTIPLEXED + 1];
  COLOR_B() {}

  COLOR_B(float f)
  {
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
    {
      value[i] = f;
    }
  }

  void operator=(float f);

  COLOR_B operator+(float f);
  COLOR_B operator+(COLOR_B const &color_2);
  void    operator+=(float f);
  void    operator+=(COLOR_B const &color_2);

  COLOR_B operator-(float f);
  COLOR_B operator-(COLOR_B const &color_2);
  void    operator-=(float f);
  void    operator-=(COLOR_B const &color_2);

  COLOR_B operator*(float f);
  COLOR_B operator*(COLOR_B const &color_2);
  void    operator*=(float f);
  void    operator*=(COLOR_B const &color_2);

  COLOR_B operator/(float f);
  COLOR_B operator/(COLOR_B const &color_2);
  void    operator/=(float f);
  void    operator/=(COLOR_B const &color_2);

  float &operator[](int index);
};

__forceinline__ __device__ void COLOR_B::operator=(float f)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    value[i] = f;
  }
}

__forceinline__ __device__ COLOR_B COLOR_B::operator+(float f)
{
  COLOR_B tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    tmp.value[i] = value[i] + f;
  }
  return tmp;
}

__forceinline__ __device__ COLOR_B COLOR_B::operator+(COLOR_B const &color_2)
{
  COLOR_B tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    tmp.value[i] = value[i] + color_2.value[i];
  }
  return tmp;
}

__forceinline__ __device__ void COLOR_B::operator+=(float f)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    value[i] += f;
  }
}

__forceinline__ __device__ void COLOR_B::operator+=(COLOR_B const &color_2)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    value[i] += color_2.value[i];
  }
}

__forceinline__ __device__ COLOR_B COLOR_B::operator-(float f)
{
  COLOR_B tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    tmp.value[i] = value[i] - f;
  }
  return tmp;
}

__forceinline__ __device__ COLOR_B COLOR_B::operator-(COLOR_B const &color_2)
{
  COLOR_B tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    tmp.value[i] = value[i] - color_2.value[i];
  }
  return tmp;
}

__forceinline__ __device__ void COLOR_B::operator-=(float f)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    value[i] -= f;
  }
}

__forceinline__ __device__ void COLOR_B::operator-=(COLOR_B const &color_2)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    value[i] -= color_2.value[i];
  }
}

__forceinline__ __device__ COLOR_B COLOR_B::operator*(float f)
{
  COLOR_B tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    tmp.value[i] = value[i] * f;
  }
  return tmp;
}

__forceinline__ __device__ COLOR_B COLOR_B::operator*(COLOR_B const &color_2)
{
  COLOR_B tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    tmp.value[i] = value[i] * color_2.value[i];
  }
  return tmp;
}

__forceinline__ __device__ void COLOR_B::operator*=(float f)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    value[i] *= f;
  }
}

__forceinline__ __device__ void COLOR_B::operator*=(COLOR_B const &color_2)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    value[i] *= color_2.value[i];
  }
}

__forceinline__ __device__ COLOR_B COLOR_B::operator/(float f)
{
  COLOR_B tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    tmp.value[i] = value[i] / f;
  }
  return tmp;
}

__forceinline__ __device__ COLOR_B COLOR_B::operator/(COLOR_B const &color_2)
{
  COLOR_B tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    tmp.value[i] = value[i] / color_2.value[i];
  }
  return tmp;
}

__forceinline__ __device__ void COLOR_B::operator/=(float f)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    value[i] /= f;
  }
}

__forceinline__ __device__ void COLOR_B::operator/=(COLOR_B const &color_2)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    value[i] /= color_2.value[i];
  }
}

__forceinline__ __device__ float &COLOR_B::operator[](int index)
{
  return value[index];
}

void clamp_negative_color(COLOR_B &color)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    color.value[i] = optix::clamp(color.value[i], 0.f, color.value[i]);
  }
}

COLOR_B sqrt(COLOR_B color)
{
  COLOR_B tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    tmp[i] = sqrtf(color[i]);
  }
  return tmp;
}

COLOR_B lerp(COLOR_B color_1, COLOR_B color_2, float a)
{
  COLOR_B tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
  {
    tmp[i] = optix::lerp(color_1[i], color_2[i], a);
  }
  return tmp;
}

// This contains a Spectrum defined for each wavelength multiplexed.
// So, NB_MAX_WAVES_MULTIPLEXED.
struct COLOR
{
  float value[NB_MAX_WAVES_MULTIPLEXED];
  COLOR() {}

  COLOR(float f)
  {
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    {
      value[i] = f;
    }
  }

  COLOR(const COLOR_B &c, float f)
  {
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    {
      value[i] = optix::lerp(c.value[i], c.value[i + 1], f);
    }
  }

  COLOR &operator=(float f);
  COLOR &operator=(COLOR const &color_2);

  COLOR operator+(float f);
  COLOR operator+(COLOR const &color_2);
  void  operator+=(float f);
  void  operator+=(COLOR const &color_2);

  COLOR operator-(float f);
  COLOR operator-(COLOR const &color_2);
  void  operator-=(float f);
  void  operator-=(COLOR const &color_2);

  COLOR operator*(float f);
  COLOR operator*(COLOR const &color_2);
  void  operator*=(float f);
  void  operator*=(COLOR const &color_2);

  COLOR operator/(float f);
  COLOR operator/(COLOR const &color_2);
  void  operator/=(float f);
  void  operator/=(COLOR const &color_2);

  float &operator[](int index);

  float avg();
};

__forceinline__ __device__ COLOR &COLOR::operator=(float f)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    value[i] = f;
  return *this;
}

__forceinline__ __device__ COLOR &COLOR::operator=(COLOR const &color_2)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    value[i] = color_2.value[i];
  return *this;
}

__forceinline__ __device__ COLOR COLOR::operator+(float f)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    tmp.value[i] = value[i] + f;
  }
  return tmp;
}

__forceinline__ __device__ COLOR COLOR::operator+(COLOR const &color_2)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    tmp.value[i] = value[i] + color_2.value[i];
  }
  return tmp;
}

__forceinline__ __device__ void COLOR::operator+=(float f)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    value[i] += f;
  }
}

__forceinline__ __device__ void COLOR::operator+=(COLOR const &color_2)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    value[i] += color_2.value[i];
  }
}

__forceinline__ __device__ COLOR COLOR::operator-(float f)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    tmp.value[i] = value[i] - f;
  }
  return tmp;
}

__forceinline__ __device__ COLOR COLOR::operator-(COLOR const &color_2)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    tmp.value[i] = value[i] - color_2.value[i];
  }
  return tmp;
}

__forceinline__ __device__ void COLOR::operator-=(float f)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    value[i] -= f;
  }
}

__forceinline__ __device__ void COLOR::operator-=(COLOR const &color_2)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    value[i] -= color_2.value[i];
  }
}

__forceinline__ __device__ COLOR COLOR::operator*(float f)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    tmp.value[i] = value[i] * f;
  }
  return tmp;
}

__forceinline__ __device__ COLOR COLOR::operator*(COLOR const &color_2)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    tmp.value[i] = value[i] * color_2.value[i];
  }
  return tmp;
}

__forceinline__ __device__ void COLOR::operator*=(float f)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    value[i] *= f;
  }
}

__forceinline__ __device__ void COLOR::operator*=(COLOR const &color_2)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    value[i] *= color_2.value[i];
  }
}

__forceinline__ __device__ COLOR COLOR::operator/(float f)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    tmp.value[i] = value[i] / f;
  }
  return tmp;
}

__forceinline__ __device__ COLOR COLOR::operator/(COLOR const &color_2)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    tmp.value[i] = value[i] / color_2.value[i];
  }
  return tmp;
}

__forceinline__ __device__ void COLOR::operator/=(float f)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    value[i] /= f;
  }
}

__forceinline__ __device__ void COLOR::operator/=(COLOR const &color_2)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    value[i] /= color_2.value[i];
  }
}

__forceinline__ __device__ float &COLOR::operator[](int index)
{
  return value[index];
}

// __forceinline__ __device__ COLOR COLOR::operator+(float f, COLOR const&color)
// {
// return color + f;
// }

// __forceinline__ __device__ COLOR COLOR::operator-(float f, COLOR const& color)
// {
// COLOR tmp;
// for(int i=0; i<NB_MAX_WAVES_MULTIPLEXED; i++)
// tmp.value[i] = f - color.value[i];
// return
// }


void clamp_negative_color(COLOR &color)
{
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    color.value[i] = optix::clamp(color.value[i], 0.f, color.value[i]);
  }
}

COLOR sqrt(COLOR color)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
  {
    tmp.value[i] = sqrtf(color.value[i]);
  }
  return tmp;
}

float avg(COLOR value)
{
  float tmp = 0.f;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    tmp += value[i];
  return tmp / NB_MAX_WAVES_MULTIPLEXED;
}

COLOR lerp(COLOR a, COLOR b, float x)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    tmp[i] = optix::lerp(a[i], b[i], x);
  return tmp;
}

COLOR expf(COLOR color)
{
  COLOR tmp;
  for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    tmp[i] = optix::expf(color[i]);
  return tmp;
}


COLOR colorFromAsset(COLOR_B &color, float offset, int index)
{
  COLOR res(0.f);

  if (index == -1)
  {
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    {
      res[i] = optix::lerp(color[i], color[i + 1], offset);
    }
  }
  else
  {
    res[index] = optix::lerp(color[index], color[index + 1], offset);
  }

  return res;
}

COLOR colorFromAsset(optix::buffer<float, 1> &color, float offset, int index)
{
  COLOR res(0.f);

  if (index == -1)
  {
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    {
      res[i] = optix::lerp(color[i], color[i + 1], offset);
    }
  }
  else
  {
    res[index] = optix::lerp(color[index], color[index + 1], offset);
  }

  return res;
}



#else

typedef float COLOR;
typedef float COLOR_B;

void clamp_negative_color(COLOR &color)
{
  color = optix::clamp(color, 0.f, color);
}

// COLOR sqrtf(COLOR color)
// {
// return sqrtf(color);
// }

float avg(COLOR value)
{
  return value;
}

COLOR lerp(COLOR color_1, COLOR color_2, float a)
{
  //Need explicitly optix namespace here.
  return optix::lerp(color_1, color_2, a);
}

COLOR expf(COLOR color)
{
  //Need explicitly optix namespace here.
  return optix::expf(color);
}

#endif
COLOR_B bilerp(COLOR_B c_00, COLOR_B c_01, COLOR_B c_10, COLOR_B c_11, float dx, float dy)
{
  return lerp(lerp(c_00, c_01, dy), lerp(c_10, c_11, dx), dx);
}

COLOR_B trilerp(
    COLOR_B c_000,
    COLOR_B c_001,
    COLOR_B c_010,
    COLOR_B c_011,
    COLOR_B c_100,
    COLOR_B c_101,
    COLOR_B c_110,
    COLOR_B c_111,
    float   dx,
    float   dy,
    float   dz)
{
  return lerp(bilerp(c_000, c_001, c_010, c_011, dy, dz), bilerp(c_100, c_101, c_110, c_111, dy, dz), dx);
}

}   // namespace spectrum
}   // namespace mrf