#pragma once

#include <optixu/optixu_math_namespace.h>

//to retrieve MRF_INTERACTIVE
#include "optix_backend_config.h"


#define INV_PIf 0.31830988618379067154f
#define PIf     3.14159265358979323846f

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  include "spectrum_color.h"
#else
#  include "rgb_color.h"
#endif

struct PerRayData_pathtrace
{
  mrf::spectrum::COLOR attenuation;
  mrf::spectrum::COLOR result;

  optix::float3 origin;
  optix::float3 direction;

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  float wavelength_offset;
#endif

  mrf::spectrum::COLOR_B ior;

  int id_wavelength;
  int backface;

  unsigned int  seed;
  unsigned int  num_sample;
  optix::float2 offset_sampling;   //cranley patterson

  int depth;

  bool countEmitted;
  bool done;
  bool is_inside;

  int shadow_catcher;
};

struct PerRayData_pathtrace_shadow
{
  bool inShadow;
};

/*
rtDeclareVariable(float,         scene_epsilon,, );
rtDeclareVariable(rtObject,      top_object,, );
rtDeclareVariable(unsigned int,  pathtrace_shadow_ray_type,, );

rtBuffer<DirectionnalLight>      directionnal_lights;
*/
