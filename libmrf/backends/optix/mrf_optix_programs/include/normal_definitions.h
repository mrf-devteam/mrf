
//TODO find if already in cuda
optix::float3 halfVector(optix::float3 view_dir, optix::float3 out_dir, optix::float3 normal)
{
  float3 h = view_dir + out_dir;

  float h_sqr_length = optix::dot(h, h);

  if (h_sqr_length < 0.0001f)
  {
    h = normal;
  }
  else
  {
    h = h * 1.f / sqrtf(h_sqr_length);
  }

  return h;
}

inline optix::float3 generateOrthogonal(optix::float3 n)
{
  if ((n.x < n.y) && (n.x < n.z))
  {
    return optix::make_float3(0.f, n.z, -n.y);
  }
  else if (n.y < n.z)
  {
    return optix::make_float3(n.z, 0.f, -n.x);
  }
  else
  {
    return optix::make_float3(-n.y, n.x, 0.f);
  }
}
