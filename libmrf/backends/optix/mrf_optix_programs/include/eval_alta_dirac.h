COLOR evalDirac(float3 direction, float3 normal)
{
  if (brdf_data_dirac.size() <= 1)
  {
    return COLOR(0.f);
  }

  float nDotL       = dot(direction, normal);
  float theta_light = acos(nDotL);

  float grid_min = 0.f;
  float grid_max = M_PIf / 2.f - M_PIf / 360.f;

  theta_light = min(theta_light / (grid_max - grid_min), 1.0);

  float X = theta_light * (brdf_data_dirac.size() - 1);

  int idX = int(X);
  // #ifdef INTERPOLATE_DIRAC // maybe latter
  //COLOR_B brdf_dirac = brdf_data_dirac[id]+(brdf_data_dirac[id+1]-brdf_data_dirac[id])*((theta_light * brdf_data_dirac.size())-id);

  COLOR_B brdf_dirac = brdf_data_dirac[idX];

  if (_interpolation == 1)
  {
    int   idX2 = min(idX + 1, int(brdf_data_dirac.size() - 1));
    float dx   = X - idX;
    brdf_dirac = mrf::spectrum::lerp(brdf_dirac, brdf_data_dirac[idX2], dx);
  }

  //#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  return colorFromAsset(brdf_dirac, current_prd.wavelength_offset, current_prd.id_wavelength);
#else
  return brdf_dirac;
#endif
}
