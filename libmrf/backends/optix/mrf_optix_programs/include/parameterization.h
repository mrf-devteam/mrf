//Parameterization
#define UTIA_2D              0
#define UTIA_4D              1
#define UTIA_4D_H            2
#define UTIA_4D_H_NL         3
#define RUSINKIEWICZ_2D      4
#define RUSINKIEWICZ_2D_COS  5
#define RUSINKIEWICZ_3D      6
#define RUSINKIEWICZ_3D_COS  7
#define RUSINKIEWICZ_4D      8
#define HEMISPHERICAL_ALBEDO 9   //TODO

float3 toLocalFrame(float3 dir, float3 normal, float3 tangent, float3 bitangent)
{
  float3 res;
  res.x = clamp(dot(dir, tangent), -1.f, 1.f);
  res.y = clamp(dot(dir, bitangent), -1.f, 1.f);
  res.z = clamp(dot(dir, normal), -1.f, 1.f);

  return normalize(res);
}

//TODO: merge with direction to PHI
float azimuthAngle(float3 local_dir)
{
  //atan2f(y,x) -> [-pi, pi]
  float phi = atan2f(local_dir.y, local_dir.x);

  if (phi < 0.0)
  {
    phi += 2.f * M_PIf;
  }

  return phi;
}

float azimuthAngle2(float3 local_dir)
{
  //atan2f(y,x) -> [-pi, pi]
  return atan2f(local_dir.y, local_dir.x);
}

float directionToPhi(float3 normal, float3 direction)
{
  float3 local_dir = toLocalFrame(direction, normal, tangent, cross(normal, tangent));
  float  phi       = atan2f(local_dir.y, local_dir.x) + M_PIf;

  return phi;
}

float directionToTheta(float3 normal, float3 direction)
{
  return acos(dot(normal, direction));
}

//Normalized theta/phi for a direction
float2 directionToSpherical(float3 normal, float3 direction)
{
  float theta = acos(dot(normal, direction));
  //Map theta from 0;pi/2 to 0;1
  theta *= 2.f / M_PIf;

  float phi = directionToPhi(normal, direction);
  //Map phi from 0;2pi to 0;1
  phi *= 0.5f / M_PIf;

  return make_float2(theta, phi);
}
