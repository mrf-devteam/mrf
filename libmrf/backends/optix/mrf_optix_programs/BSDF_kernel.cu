#include "random.h"
#include <optixu/optixu_math_namespace.h>

#include "include/path_tracer_definitions.h"

using namespace optix;
using mrf::spectrum::COLOR;
using mrf::spectrum::COLOR_B;

#include "include/normal_definitions.h"

rtDeclareVariable(float, scene_epsilon, , );
rtDeclareVariable(rtObject, top_object, , );
rtDeclareVariable(unsigned int, pathtrace_ray_type, , );
rtDeclareVariable(unsigned int, pathtrace_shadow_ray_type, , );

rtDeclareVariable(float3, tangent, attribute tangent, );
rtDeclareVariable(float3, bitangent, attribute bitangent, );
rtDeclareVariable(float3, texcoord, attribute texcoord, );
rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, );
rtDeclareVariable(float3, shading_normal, attribute shading_normal, );

rtDeclareVariable(float3, back_hit_point, attribute back_hit_point, );
rtDeclareVariable(float3, front_hit_point, attribute front_hit_point, );

rtDeclareVariable(PerRayData_pathtrace, current_prd, rtPayload, );
rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );
rtDeclareVariable(float, t_hit, rtIntersectionDistance, );


#include "include/lighting/light_definitions.h"

rtBuffer<QuadLight>         lights;
rtBuffer<SphereLight>       sphere_lights;
rtBuffer<DirectionnalLight> directionnal_lights;

rtDeclareVariable(unsigned int, next_event, , );   //0 = none, 1 = all

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> _wavelengths;
rtDeclareVariable(unsigned int, first_wave_index, , );
rtDeclareVariable(unsigned int, last_wave_index, , );
rtDeclareVariable(unsigned int, nb_wavelengths, , );
#  else
rtDeclareVariable(float, _wavelengths, , );
#  endif
#else
rtDeclareVariable(float3, _wavelengths, , );
#endif


rtDeclareVariable(float, _thickness, , );   //0 = none, 1 = all

//List of defined to be used as sampled lobe (int parameter) in sampling function.
//WARNING: some should not be used, but are placed for consistency with the CPU backend.
#define ABSORPTION    0
#define TIR           1 /* Total Internal Reflection */
#define INTERNAL_REFL 2 /* INTERNAL REFLECTION inside the medium */
#define DIFFUSE_REFL  3
#define SPECULAR_REFL 4
/* This means a reflection occured but it can be diffuse or specular */
#define ANY_REFL 5
/* Perfect Transmissive Case when the surface is not rough  */
#define TRANSMISSION 6
/* This means a transmission has occured but not necessarily the perfect one*/
#define TRANSMISSION_FRONT 7   // Transmission when view dir is front facing the geometry normal
#define TRANSMISSION_BACK  8   // Transmission when view dir is back facing the geometry normal
#define ANY_TRANSMISSION   9
#define CLEARCOAT_REFL     10   // For Principled brdf

//#PLUGIN#

#include "include/lighting/envmap_definitions.h"
#include "include/lighting/light_sampling.h"
#include "include/lighting/next_event.h"

float3 get_normal(float3 shading_normal, bool face_forward)
{
  //TODO: handle normal_map here
  float3 world_shading_normal = normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, shading_normal));
  if (face_forward)
  {
    float3 world_geometric_normal = normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, geometric_normal));
    return normalize(faceforward(world_shading_normal, -ray.direction, world_geometric_normal));
  }
  else
  {
    return world_shading_normal;
  }
}

RT_PROGRAM void material_eval()
{
  if (current_prd.shadow_catcher > 0)
  {
    current_prd.done = true;
    return;
  }


  float3 hitpoint = ray.origin + t_hit * ray.direction;
  float3 ffnormal = get_normal(shading_normal, true);

  //We need both -> faceforward for easier evaluation, real_normal for fresnel and refraction stuff.
  float3 real_normal = get_normal(shading_normal, false);

  //Sample a half-vector. For material without this definition -> generate_half_vector return ffnormal.
  float3 h = generate_half_vector(ffnormal, -ray.direction);

  //backface culling
  /*
  if(dot(ray.direction,world_geometric_normal)>0)
  {
      current_prd.backface = true;
      return;
  }
  current_prd.backface = false;
  */

  current_prd.origin       = hitpoint;
  current_prd.countEmitted = true;

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  //Store current id_wavelength. reflection_pdf sets the current_prd.id_wavelength as it must sample a wavelength -> ONLY if not already sampled.
  //If reflection, current_prd.id_wavelength is set back to previous value later.
  int tmp_id_wavelength = current_prd.id_wavelength;
#endif
  float2 reflection_temp      = reflection_pdf(h, -ray.direction);
  float  reflection_pdf_value = reflection_temp.x;
  current_prd.id_wavelength   = int(reflection_temp.y);

  //Convenience initialization for fully reflective (pdf == 1) /transmissive (pdf == 0).
  float z1 = 1.f - reflection_pdf_value;

  //If pdf == 1 (-> fully reflective), or pdf == 0 (fully transmissive) no need to randomly update z1
  if (reflection_pdf_value < 1.f && reflection_pdf_value > 0.f)
  {
    z1 = sampling1D(
        current_prd.num_sample,
        current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
        current_prd.offset_sampling.x,
        current_prd.seed);
  }

  COLOR bsdf_dot_nl_div_by_pdf(0.f);
  if (z1 <= reflection_pdf_value)   //Ray is reflected -> sample + apply next_event.
  {
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    //We are in reflection event, if no refraction has ever occurred, place back the default "-1" id_wavelength to continue to propagate all wavelengths.
    if (tmp_id_wavelength == -1) current_prd.id_wavelength = -1;
#endif

    //generate_reflective_sample returns a float4: sampled direction (xyz) and sampled lobe (w).
    float4 sample         = generate_reflective_sample(h, ray.direction);
    current_prd.direction = make_float3(sample);
    int sampled_lobe      = int(sample.w);   //-1 if BRDF has one lobe.

    //This can not happen for a reflection.
    if (dot(ffnormal, current_prd.direction) <= 0.f)
    {
      current_prd.done = true;
      return;
    }

    //brdf_eval_optim return the BRDF, already multiplied by n dot l and divided by sample pdf.
    //Warning, the division by the reflection pdf MUST be hidden in brdf_eval_optim.
    bsdf_dot_nl_div_by_pdf
        = bsdf_eval_optim(h, ffnormal, -ray.direction, current_prd.direction, sampled_lobe, reflection_pdf_value);

    current_prd.origin = front_hit_point;

    //TODO: try to sample only ONE directional light + stop ray if sampled ?
    current_prd.result += compute_directional_light(hitpoint, ffnormal, ray.direction);

    bool is_inside = false;
    if (rtIsTriangleHit())
    {
      is_inside = rtIsTriangleHitBackFace();
    }
    else   //Analytical sphere, no built-in check...
    {
      //If real and face-forward normal are opposite direction, we are exiting.
      is_inside = dot(real_normal, ffnormal) < 0.f;
    }

    //Is dirac ? no need to compute next event -> add contribution and return.
    if (bsdf_is_dirac())
    {
      current_prd.attenuation *= bsdf_dot_nl_div_by_pdf;
      return;
    }

    //
    // Next event estimation (== compute direct lighting).
    //
    if (next_event == true && !is_inside)   //Dont evaluate for glass TIR
    {
      current_prd.countEmitted = false;   //Avoid double contribution of the source

      COLOR   result(0.f);
      COLOR_B light_emission(0.f);

      float3 L         = make_float3(0.f);
      float  light_pdf = 0.f;
      float  Ldist     = 0.f;

      //TODO: use rtVariable(s) (possibly one "int3" ?), can be computed only once
      //+1 for 1 chance to choose to sample the envmap
      unsigned int num_lights
          = (use_envmap == 1) ? lights.size() + sphere_lights.size() + 1 : lights.size() + sphere_lights.size();
      unsigned int idx_light = sample_light(num_lights, hitpoint, ffnormal, light_emission, L, light_pdf, Ldist);

      //TODO: use rtVariable instead of hardcoded, to make it controllable ?
      bool use_mis = true;

      //
      // cast shadow rays and add MIS
      //
      // Add contribution from light sampling.
      if (dot(ffnormal, L) > 0.f)
      {
        COLOR light = eval_direct_light(hitpoint, ffnormal, -ray.direction, L, sampled_lobe, light_pdf, Ldist, use_mis);
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
        light *= colorFromAsset(light_emission, current_prd.wavelength_offset, current_prd.id_wavelength);
#else
        light *= light_emission;
#endif

        current_prd.result += light * num_lights / reflection_pdf_value;
      }

      //add contribution from brdf sampling if MIS
      if (use_mis)
      {
        //First compute light pdf and update Ldist for BRDF sampled direction
        float Ldist, light_pdf;
        if (idx_light < lights.size())
        {
          light_pdf = area_quad_pdf(idx_light, hitpoint, ffnormal, -ray.direction, current_prd.direction, Ldist);
        }
        else if (idx_light < lights.size() + sphere_lights.size())
        {
          light_pdf = sphere_light_pdf(
              idx_light - lights.size(),
              hitpoint,
              ffnormal,
              -ray.direction,
              current_prd.direction,
              Ldist);
        }
        else
        {
          envmap_eval_and_pdf(current_prd.direction, Ldist, light_emission, light_pdf);
        }

        COLOR bsdf = eval_direct_bsdf(
            hitpoint,
            ffnormal,
            -ray.direction,
            current_prd.direction,
            sampled_lobe,
            light_pdf,
            Ldist,
            bsdf_dot_nl_div_by_pdf);
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
        for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
        {
          bsdf.value[i] *= lerp(light_emission.value[i], light_emission.value[i + 1], current_prd.wavelength_offset);
        }
#else
        bsdf *= light_emission;
#endif

        current_prd.result += bsdf * num_lights / reflection_pdf_value;
      }   //endif use_mis
    }
    else   //TIR -> apply transmittance
    {
      const float thickness = t_hit;
      bsdf_dot_nl_div_by_pdf *= bsdf_transmittance(thickness);
    }
  }
  else   //Ray is transmitted -> sample transmission direction
  {
    //generate_transmissive_sample returns a float4: sampled direction (xyz) and sampled lobe (w).
    float4 sample         = generate_transmissive_sample(h, ffnormal, ray.direction);
    current_prd.direction = make_float3(sample);
    int sampled_lobe      = int(sample.w);   //-1 if BRDF has one lobe.

    //face-forward normal dot sampled direction > 0 -> ray is not refracted, abort.
    if (dot(ffnormal, current_prd.direction) >= 0.f)
    {
      current_prd.done = true;
      return;
    }

    //brdf_eval_optim returns the BTDF, already multiplied by n dot l and divided by sample pdf.
    //Warning, the division by the reflection pdf MUST also be included in brdf_eval_optim.
    //Otherwise, use bsdf_eval, bdsf_pdf and n dot l explicitly.
    bsdf_dot_nl_div_by_pdf
        = bsdf_eval_optim(h, ffnormal, -ray.direction, current_prd.direction, sampled_lobe, reflection_pdf_value);

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    //Upon transmission, only one wavelength can be maintained is transmission is wavelength-dependent.
    //First time ray is transmitted, decimate wavelength.
    if (tmp_id_wavelength == -1)
    {
      int   id_wavelength = current_prd.id_wavelength;
      float pdf           = inv_decimation_pdf(id_wavelength);
      if (pdf <= 0.f)
      {
        current_prd.done = true;
        return;
      }

      //turn all other wavelengths transported by ray to 0
      const float temp_att    = current_prd.attenuation[id_wavelength];
      current_prd.attenuation = COLOR(0.f);

      //Apply decimation PDF to decimated component (attenuation and result of accumulated next event estimation)
      current_prd.attenuation[id_wavelength] = temp_att * pdf;
    }
#endif
    bool  is_exiting;
    float thickness = 0.f;
    if (_thickness > 0.f)
    {
      //Material is a slab -> TODO handle internal reflection (+ absorption ?)
      bsdf_dot_nl_div_by_pdf
          *= bsdf_eval_optim(h, ffnormal, -make_float3(sample), ray.direction, sampled_lobe, reflection_pdf_value);

      float t_slab          = _thickness / abs(dot(make_float3(sample), ffnormal));
      current_prd.direction = ray.direction;
      current_prd.origin += t_slab * make_float3(sample);
      thickness  = _thickness;
      is_exiting = true;
    }
    else
    {
      current_prd.origin = back_hit_point;
      update_ior(real_normal, -ray.direction, current_prd.direction);

      thickness = t_hit;
      if (rtIsTriangleHit())
      {
        is_exiting = rtIsTriangleHitBackFace();
      }
      else   //Analytical sphere, no built-in check...
      {
        //If real and face-forward normal are opposite direction, we are exiting.
        is_exiting = dot(real_normal, ffnormal) < 0.f;
      }
    }

    //If true, we are dealing with out-transmission -> apply transmittance, directional lights and next_event
    if (is_exiting)
    {
      bsdf_dot_nl_div_by_pdf *= bsdf_transmittance(thickness);

      current_prd.result += compute_directional_light(hitpoint, ffnormal, ray.direction);

      if (bsdf_is_dirac())
      {
        current_prd.attenuation *= bsdf_dot_nl_div_by_pdf;
        return;
      }


      //TODO: TEST AD REACTIVATE LATER !!!
      // //if brdf is a dirac, next event has no sense.
      // if (next_event == true)
      // {
      // COLOR light_emission(0.f);

      // float3 L         = make_float3(0.f);
      // float  light_pdf = 0.f;
      // float  Ldist     = 0.f;

      // //TODO: use rtVariable(s) (possibly one "int3" ?), can be computed only once
      // //+1 for 1 chance to choose to sample the envmap
      // unsigned int num_lights
      // = (use_envmap == 1) ? lights.size() + sphere_lights.size() + 1 : lights.size() + sphere_lights.size();
      // unsigned int idx_light = sample_light(num_lights, hitpoint, real_normal, light_emission, L, light_pdf, Ldist);

      // //TODO: use rtVariable instead of hardcoded, to make it controllable ?
      // bool use_mis = true;

      // //
      // // cast shadow rays and add MIS
      // //

      // // Add contribution from light sampling.
      // {
      // COLOR light = eval_direct_light(hitpoint, ffnormal, -ray.direction, L, sampled_lobe, light_emission, light_pdf, Ldist, use_mis);

      // current_prd.result += light * light_emission * num_lights * bsdf_transmittance(t_hit) / (1.f - reflection_pdf_value);
      // }

      // //add contribution from brdf sampling if MIS
      // if (use_mis)
      // {
      // //First compute light pdf and update Ldist for BRDF sampled direction
      // float Ldist, light_pdf;
      // if (idx_light < lights.size())
      // light_pdf = area_quad_pdf(idx_light, hitpoint, real_normal, -ray.direction, current_prd.direction, Ldist);
      // else if (idx_light < lights.size() + sphere_lights.size())
      // light_pdf = sphere_light_pdf(idx_light - lights.size(), hitpoint, real_normal, -ray.direction, current_prd.direction, Ldist);
      // else
      // {
      // envmap_eval_and_pdf(current_prd.direction, Ldist, light_emission, light_pdf);
      // }

      // COLOR bsdf = eval_direct_bsdf(hitpoint, ffnormal, -ray.direction, current_prd.direction, sampled_lobe, light_emission, light_pdf, Ldist, bsdf_dot_nl_div_by_pdf);

      // current_prd.result += bsdf * light_emission * num_lights * bsdf_transmittance(t_hit) / (1.f - reflection_pdf_value);
      // }   //endif use_mis
      // }
    }
  }
  //update energy along the ray relative to brdf importance sampling
  current_prd.attenuation *= bsdf_dot_nl_div_by_pdf;
}


//-----------------------------------------------------------------------------
//
//  Shadow any-hit
//
//-----------------------------------------------------------------------------

rtDeclareVariable(PerRayData_pathtrace_shadow, current_prd_shadow, rtPayload, );

RT_PROGRAM void shadow()
{
  current_prd_shadow.inShadow = true;
  rtTerminateRay();
}
