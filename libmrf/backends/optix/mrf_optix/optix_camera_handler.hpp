/*
 *
 * author : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 **/

#pragma once
#include <mrf_core/rendering/pinhole_camera.hpp>
#include <mrf_core/rendering/thin_lens_camera.hpp>
#include <mrf_core/rendering/orthographic_camera.hpp>
#include <mrf_core/rendering/environment_camera.hpp>


#include <mrf_optix/optix_util.hpp>
#include <mrf_optix/optix_renderer.hpp>

#include <optixu/optixpp_namespace.h>

#include <iostream>

namespace mrf
{
namespace optix_backend
{
class MRF_OPTIX_EXPORT OptixCameraHandler
{
public:
  static std::vector<std::string> getCameraCompileOptions(mrf::rendering::Camera *camera)
  {
    std::vector<std::string> options;
    switch (camera->getType())
    {
      case mrf::rendering::CameraType::Pinhole:
        options.push_back("-DCAM_PINHOLE");
        break;
      case mrf::rendering::CameraType::ThinLens:
        options.push_back("-DCAM_THIN_LENS");
        break;
      case mrf::rendering::CameraType::Orthographic:
        options.push_back("-DCAM_ORTHOGRAPHIC");
        break;
      case mrf::rendering::CameraType::Gonio:
        options.push_back("-DCAM_GONIO");
        break;
      case mrf::rendering::CameraType::Environment:
        options.push_back("-DCAM_ENVIRONMENT");
        break;
      default:
        options.push_back("-DCAM_NOTYPE");
        break;
    }
    return options;
  }

  static void setCameraParameters(mrf::rendering::Camera *camera, optix::Context &context)
  {
    optix::float3 camera_u, camera_v, camera_w;
    optix::float3 camera_eye    = mrfToOptix(camera->position());
    optix::float3 camera_lookat = mrfToOptix(camera->lookAt());
    optix::float3 camera_up     = mrfToOptix(camera->up());

    ////////////////////////////////////////
    // Apply rotation matrix to parameters
    // TODO : move this section in the camera class
    ///////////////////////////////////////
    camera_w = normalize(camera_lookat - camera_eye);
    camera_u = normalize(cross(camera_w, camera_up));
    camera_v = normalize(cross(camera_u, camera_w));

    const optix::Matrix4x4 frame
        = optix::Matrix4x4::fromBasis(normalize(camera_u), normalize(camera_v), normalize(-camera_w), camera_lookat);
    const optix::Matrix4x4 frame_inv = frame.inverse();
    // Apply camera rotation twice to match old SDK behavior.

    optix::Matrix4x4 camera_rotate;
    mrf::math::Vec3f u, v, w;
    camera->rotMat().getColumn(0, u);
    camera->rotMat().getColumn(1, v);
    camera->rotMat().getColumn(2, w);

    camera_rotate.setCol(0, optix::make_float4(mrf::optix_backend::mrfToOptix(u), 0.f));
    camera_rotate.setCol(1, optix::make_float4(mrf::optix_backend::mrfToOptix(v), 0.f));
    camera_rotate.setCol(2, optix::make_float4(mrf::optix_backend::mrfToOptix(w), 0.f));
    camera_rotate.setCol(3, optix::make_float4(0.f, 0.f, 0.f, 1.f));

    const optix::Matrix4x4 trans = frame * camera_rotate * camera_rotate * frame_inv;

    camera_eye    = optix::make_float3(trans * make_float4(camera_eye, 1.0f));
    camera_lookat = optix::make_float3(trans * make_float4(camera_lookat, 1.0f));
    camera_up     = optix::make_float3(trans * make_float4(camera_up, 0.0f));

    camera->position(mrf::optix_backend::optixToMRF(camera_eye));
    camera->lookAt(mrf::optix_backend::optixToMRF(camera_lookat));
    camera->up(mrf::optix_backend::optixToMRF(camera_up));
    camera->rotMat(mrf::optix_backend::optixToMRF(optix::Matrix3x3::identity()));

    /////////////////////////////////////
    // END : Apply rotation matrix to parameters
    /////////////////////////////////

    camera_w = normalize(camera_lookat - camera_eye);
    camera_u = normalize(cross(camera_w, camera_up));
    camera_v = normalize(cross(camera_u, camera_w));

    switch (camera->getType())
    {
      case mrf::rendering::CameraType::Pinhole:
      {
        mrf::rendering::Pinhole *cam = dynamic_cast<mrf::rendering::Pinhole *>(camera);
        mrf::optix_backend::applyFov(camera_u, camera_v, cam->fovy(), camera->aspectRatio(), false);
        break;
      }
      case mrf::rendering::CameraType::ThinLens:
      {
        context["blur_dir_x"]->setFloat(camera_u);
        context["blur_dir_y"]->setFloat(camera_v);

        mrf::rendering::ThinLens *cam = dynamic_cast<mrf::rendering::ThinLens *>(camera);
        mrf::optix_backend::applyFov(camera_u, camera_v, cam->fovy(), camera->aspectRatio(), false);
        camera_u *= (float)cam->focalPlaneDistance();
        camera_v *= (float)cam->focalPlaneDistance();
        camera_w *= (float)cam->focalPlaneDistance();

        context["aperture"]->setFloat(cam->aperture());
        break;
      }
      case mrf::rendering::CameraType::Orthographic:
      {
        mrf::rendering::Orthographic *cam = dynamic_cast<mrf::rendering::Orthographic *>(camera);
        optix::float2                 sensor_size;
        sensor_size.x = (float)cam->sensor().size().x();
        sensor_size.y = (float)cam->sensor().size().y();
        context["sensor_size"]->setFloat(sensor_size);
        break;
      }
      case mrf::rendering::CameraType::Gonio:
      {
        break;
      }
      case mrf::rendering::CameraType::Environment:
      {
        mrf::rendering::Environment *cam = dynamic_cast<mrf::rendering::Environment *>(camera);
        context["theta"]->setFloat(cam->theta());
        context["phi"]->setFloat(cam->phi());
        break;
      }
      default:
      {
        break;
      }
    }

    context["eye"]->setFloat(camera_eye);
    context["U"]->setFloat(camera_u);
    context["V"]->setFloat(camera_v);
    context["W"]->setFloat(camera_w);
  }
};

}   // namespace optix_backend
}   // namespace mrf
    /*
     */