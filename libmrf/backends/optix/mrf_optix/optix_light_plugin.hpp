/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * backend optix
 * Optix light handling
 *
 **/
#pragma once

#include <mrf_optix_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/lighting/light.hpp>
#include <mrf_core/feedback/loger.hpp>


#include <mrf_optix/optix_material_handler.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <mrf_optix_programs/include/lighting/light_definitions.h>

#include <vector>

namespace mrf
{
namespace optix_backend
{
class MRF_OPTIX_EXPORT OptixBaseLight
{
public:
  OptixBaseLight(mrf::lighting::Light *light);
  virtual ~OptixBaseLight() {};

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateLight(std::vector<uint> const &wavelengths) = 0;
#else
  virtual void  updateLight() = 0;
#endif

  mrf::materials::Emittance *getEmitMat() { return _light->emit_pointer(); }

  virtual optix::GeometryInstance
  getGeometryInstance(optix::Context &context, std::vector<std::string> const &cuda_compile_options, PTXConfig ptx_cfg)
      = 0;

  optix::GeometryInstance &getGeometryInstance() { return _gi; }

  virtual BaseLight *getAsStruct() { return new BaseLight(); }

protected:
  optix::GeometryInstance _gi;
  mrf::lighting::Light *  _light;

  /**Optix buffers for the GPU Path tracer*/
  optix::Buffer _light_buffer;

  bool _spd_buffer_allocated;
  bool _has_geometry;

#ifdef MRF_RENDERING_MODE_SPECTRAL

#else
  optix::float3 _emission;
#endif
};

/// <summary>
/// PLUGIN
/// </summary>

class MRF_OPTIX_EXPORT OptixLightPlugin
{
public:
  OptixLightPlugin() {};
  virtual ~OptixLightPlugin() {};

  static OptixLightPlugin *create() { return nullptr; }

  virtual OptixBaseLight *createFromMRF(mrf::lighting::Light *light) = 0;
};

struct OptixLightPlugins
{
  std::map<int, OptixLightPlugin *> fcts;
};

template<typename O>
static void register_plugin(int id, std::map<int, OptixLightPlugin *> &parse_fct_map)
{
  parse_fct_map[id] = O::create();
}

}   // namespace optix_backend
}   // namespace mrf
