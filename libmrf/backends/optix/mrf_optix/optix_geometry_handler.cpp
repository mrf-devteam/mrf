/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * author : Romain Pacanowski @ institutoptique . fr
 * Copyright CNRS 2020 : Added Analytical Sphere
 *
 **/

#include <mrf_optix/optix_geometry_handler.hpp>

#include <mrf_optix/optix_util.hpp>
#include <mrf_optix/optix_kernel.hpp>

#include <optixu/optixu_matrix.h>
#include <optixu/optixu_math_stream_namespace.h>

namespace mrf
{
namespace optix_backend
{
OptixGeometryHandler::OptixGeometryHandler(optix::Context &context): _context(context)
{
  _pgram_intersection          = 0;
  _pgram_bounding_box          = 0;
  _pgram_attributes            = 0;
  _pgram_intersection_sphere   = 0;
  _pgram_bounding_box_sphere   = 0;
  _pgram_intersection_triangle = 0;
  _pgram_bounding_box_triangle = 0;
}

OptixGeometryHandler::~OptixGeometryHandler() {}

void OptixGeometryHandler::importMrfScene(
    mrf::rendering::Scene const &             scene,
    mrf::optix_backend::OptixMaterialHandler &material_handler,
    mrf::optix_backend::OptixLightHandler &   light_handler,
    std::vector<std::string> const &          cuda_compile_options,
    PTXConfig                                 ptx_config)
{
  std::vector<std::string> cuda_compile_options_temp;

  mrf::gui::fb::Loger::getInstance()->info("Importing scene in OptiX");

  // Starting to create the SceneGraph
  optix::Group top_group;
  top_group = _context->createGroup();
  top_group->setAcceleration(_context->createAcceleration(SPATIAL_ACCELERATION_DATA_STRUCTURE));

  // Set up parallelogram programs
  //const char *ptx     = getPtxString("parallelogram.cu", cuda_compile_options);
  //_pgram_bounding_box = _context->createProgramFromPTXString(ptx, "bounds");
  //_pgram_intersection = _context->createProgramFromPTXString(ptx, "intersect");
  OptixKernel prg_parallelogram("parallelogram.cu", cuda_compile_options, ptx_config);
  prg_parallelogram.compile(_context);
  _pgram_bounding_box = prg_parallelogram.createBBoxProgram(_context);
  _pgram_intersection = prg_parallelogram.createIntersectionProgram(_context);

  //Set up sphere programs
  //ptx                        = getPtxString("sphere.cu", cuda_compile_options);
  //_pgram_bounding_box_sphere = _context->createProgramFromPTXString(ptx, "bounds");
  //_pgram_intersection_sphere = _context->createProgramFromPTXString(ptx, "intersect");
  OptixKernel prg_sphere("sphere.cu", cuda_compile_options, ptx_config);
  prg_sphere.compile(_context);
  _pgram_bounding_box_sphere = prg_sphere.createBBoxProgram(_context);
  _pgram_intersection_sphere = prg_sphere.createIntersectionProgram(_context);

  //Create geometry group for parallelograms and spheres
  optix::GeometryGroup geometry_group = _context->createGeometryGroup();
  geometry_group->setAcceleration(_context->createAcceleration(SPATIAL_ACCELERATION_DATA_STRUCTURE));
  top_group->addChild(geometry_group);

  //Set up generic triangle mesh programs
  //ptx                 = getPtxString("triangle_mesh.cu", cuda_compile_options);
  //const char *tri_ptx = getPtxString("triangle_attributes.cu", cuda_compile_options);
  //ptx = getPtxString("triangle_attributes.cu", cuda_compile_options);
  OptixKernel prg_tri_mesh("triangle_mesh.cu", cuda_compile_options, ptx_config);
  prg_tri_mesh.compile(_context);
  OptixKernel prg_tri_attrib("triangle_attributes.cu", cuda_compile_options, ptx_config);
  prg_tri_attrib.compile(_context);


  std::vector<std::shared_ptr<mrf::geom::Intersectable>> meshes_vector = scene.meshes_vector();
  std::vector<std::vector<uint>>                         instances     = scene.meshes_instances();
  std::vector<mrf::geom::Shape *> const &                shapes        = scene.shapes();
  uint                                                   mesh_num      = 0;
  uint                                                   shape_num     = 0;
  int                                                    num_triangles = 0;
  //RP: When do we need this?
  optix::Aabb aabb;

  std::vector<OptiXMesh> optix_triangle_mesh_geoms;
  optix_triangle_mesh_geoms.resize(scene.meshes().size());

  // Generate geometry and acceleration structure that will be shared between instances
  for (int mesh_idx = 0; mesh_idx < meshes_vector.size(); mesh_idx++)
  {
    auto mrf_mesh = dynamic_cast<mrf::geom::Mesh *>(meshes_vector[mesh_idx].get());
    if (mrf_mesh)
    {
      // Make sure that instances are bind to this mesh
      assert(instances[mesh_idx].size());

      optix_triangle_mesh_geoms[mesh_idx].attributes = prg_tri_attrib.createProgram(_context, "triangle_attributes");
      optix_triangle_mesh_geoms[mesh_idx].context    = _context;

      // If there is only one instance bind to the mesh, apply the shape transform directly to the mesh on cpu side.
      if (instances[mesh_idx].size() == 1)
      {
        loadMeshFromMRF(
            *mrf_mesh,
            optix_triangle_mesh_geoms[mesh_idx],
            shapes[instances[mesh_idx][0]]->worldToObjectTransform().transpose().ptr());
      }
      else
      {
        loadMeshFromMRF(*mrf_mesh, optix_triangle_mesh_geoms[mesh_idx]);
      }

      optix_triangle_mesh_geoms[mesh_idx].accel = _context->createAcceleration(SPATIAL_ACCELERATION_DATA_STRUCTURE);

      aabb.include(optix_triangle_mesh_geoms[mesh_idx].bbox_min, optix_triangle_mesh_geoms[mesh_idx].bbox_max);

      mrf::gui::fb::Loger::getInstance()->trace(
          "Optix Renderer import mesh " + mrf_mesh->absoluteFilepath() + ", triangle count:",
          optix_triangle_mesh_geoms[mesh_idx].num_triangles);
      num_triangles += optix_triangle_mesh_geoms[mesh_idx].num_triangles;
    }
  }

  //FOR ALL SHAPES
  for (auto shape_it = shapes.begin(); shape_it != shapes.end(); shape_it++, mesh_num++, shape_num++)
  {
    auto a_sphere = dynamic_cast<mrf::geom::Sphere *>(*shape_it);
    if (a_sphere)
    {
      mrf::gui::fb::Loger::getInstance()->debug(" Found a Sphere as Shape !!! ");

      auto gi = createSphere(mrfToOptix(a_sphere->position()), a_sphere->radius());
      //Save it for later when applycing Material
      unsigned int mat_index = a_sphere->materialIndex();
      _optix_analytical_shapes.push_back(std::make_pair(gi, mat_index));

      gi->addMaterial(material_handler.getMaterialFromMRF(mat_index)[0]);

      //TODO UPDATE aabb ?

      // NOW ADD TO SCENE GRAPH
      geometry_group->addChild(gi);
    }
    else
    {
      auto mrf_mesh = dynamic_cast<mrf::geom::Mesh *>((*shape_it)->mesh().get());
      if (mrf_mesh)
      {
        std::string instance_name = "mesh_" + std::to_string(mesh_num) + "_instance";
        std::string file_name     = mrf_mesh->absoluteFilepath();
        if (file_name.size() > 0)
        {
          // Retrieve the instance's mesh index to get the shared geometry
          mrf::geom::Shape *shape    = (*shape_it);
          int               mesh_idx = 0;
          for (; mesh_idx < meshes_vector.size(); mesh_idx++)
          {
            if (meshes_vector[mesh_idx].get() == shape->mesh().get()) break;
          }


          // Retrieve the instance's material
          uint                         material_id     = (*shape_it)->materialIndex();
          std::vector<optix::Material> optix_materials = material_handler.getMaterialFromMRF(material_id);

          // Multi-materials not supported
          if (optix_materials.size() > 1)
          {
            throw std::runtime_error("optix_geometry_handler:  Multi-materials not supported");
          }

          // Retrieve shared geometry, shared acceleration structure
          optix::GeometryTriangles geometry_triangle = optix_triangle_mesh_geoms[mesh_idx].geom_tri;
          optix::Acceleration      accel             = optix_triangle_mesh_geoms[mesh_idx].accel;

          // Bind the geometry and the material
          optix::GeometryInstance inst
              = _context->createGeometryInstance(geometry_triangle, optix_materials.begin(), optix_materials.end());

          // Bind the instance to a groupe and set the shared acceleration structure
          optix::GeometryGroup tri_geometry_group = _context->createGeometryGroup();
          tri_geometry_group->addChild(inst);
          tri_geometry_group->setAcceleration(accel);

          if (instances[mesh_idx].size() == 1)   // Single instance : Transform already added to the mesh
          {
            top_group->addChild(tri_geometry_group);
          }
          else   // Multiple instance : Bind instance's transform
          {
            // Retrieve the instance's transform
            optix::Matrix4x4 transform = optix::Matrix4x4((*shape_it)->worldToObjectTransform().transpose().ptr());
            // Bind the group and the transform
            optix::Transform group_trans = _context->createTransform();
            group_trans->setMatrix(false, transform.getData(), transform.inverse().getData());
            group_trans->setChild(tri_geometry_group);

            // Bind the transformed group to the top group
            top_group->addChild(group_trans);
          }

          /*
          if (optix_materials.size() == 1)
          {
            if (material_handler.getMaterial(material_id)->needMeshRefine())
              //optix_mesh.attributes = _context->createProgramFromPTXString(tri_ptx, "triangle_attributes_refine");
              optix_mesh.attributes = prg_tri_attrib.createProgram(_context, "triangle_attributes_refine");
            else
              //optix_mesh.attributes = _context->createProgramFromPTXString(tri_ptx, "triangle_attributes");
              optix_mesh.attributes = prg_tri_attrib.createProgram(_context, "triangle_attributes");

            loadMeshFromMRF(*mrf_mesh, optix_mesh, transform, optix_materials);
            tri_geometry_group->addChild(optix_mesh.geom_instance);
            trans->setMatrix(false, transform.getData(), transform.inverse().getData());
            trans->setChild(tri_geometry_group);
            top_group->addChild(trans);
          }
          else
          {
            if (material_handler.getMaterial(material_id))
            {
              if (material_handler.getMaterial(material_id)->needMeshRefine())
                optix_mesh.intersection = prg_tri_mesh.createProgram(_context, "mesh_intersect_refine");
              //  optix_mesh.intersection = _context->createProgramFromPTXString(ptx, "mesh_intersect_refine");
            }
            else
              //optix_mesh.intersection = _context->createProgramFromPTXString(ptx, "mesh_intersect");
              optix_mesh.intersection = prg_tri_mesh.createProgram(_context, "mesh_intersect");

            //optix_mesh.bounds = _context->createProgramFromPTXString(ptx, "mesh_bounds");
            optix_mesh.bounds = prg_tri_mesh.createBBoxProgram(_context);

            //TODO !!! CHECK IF TEXTURED
            loadMeshFromMRF(*mrf_mesh, optix_mesh, transform, optix_materials);
            geometry_group->addChild(optix_mesh.geom_instance);
          }
          */

          //if (material_handler.needMeshRefine(material_id))
          //  optix_mesh.intersection = _context->createProgramFromPTXString(ptx, "mesh_intersect_refine");
          //else
          //  optix_mesh.intersection = _context->createProgramFromPTXString(ptx, "mesh_intersect");
          //optix_mesh.intersection = _context->createProgramFromPTXString(ptx, "mesh_intersect");
          //optix_mesh.bounds       = _context->createProgramFromPTXString(ptx, "mesh_bounds");

          //optix_mesh.has_texcoords = mesh.has_texcoords;


          _optix_geometry_instances.push_back(std::make_pair(inst, (*shape_it)));




        }   // end filename of the mesh is
      }     //end it is a mesh
    }       //end of else (it is not Sphere as shape)
  }         //end for loop for all SHAPES

  for (auto light : light_handler.getLights())
  {
    auto gi = light->getGeometryInstance(_context, cuda_compile_options, ptx_config);
    if (gi)
    {
      gi->addMaterial(material_handler.getMaterialFromUMat(light->getEmitMat()));
      geometry_group->addChild(gi);
    }
  }

  ////Create parallelogram light geometries
  //auto optix_lights = light_handler.optixQuadLights();
  //for (auto it = optix_lights.cbegin(); it != optix_lights.cend(); ++it)
  //{
  //  auto gi = createParallelogram(it->corner, it->v1, it->v2);

  //  //gi->addMaterial(material_handler.diffuseEmittance());

  //  int  light_index = std::distance(optix_lights.cbegin(), it);
  //  auto mat         = light_handler.getUMatPointerQuadLight(light_index);

  //  //gi->addMaterial(material_handler.diffuseEmittance());
  //  gi->addMaterial(material_handler.getMaterialFromUMat(mat));

  //  geometry_group->addChild(gi);
  //  light_handler.addQuadLight(gi);
  //}

  ////Create sphere light geometries
  //auto optix_sph_lights = light_handler.optixSphereLights();
  //for (auto it = optix_sph_lights.cbegin(); it != optix_sph_lights.cend(); ++it)
  //{
  //  auto gi = createSphere(it->position, it->radius);

  //  //gi->addMaterial(material_handler.diffuseEmittance());

  //  int  light_index = std::distance(optix_sph_lights.cbegin(), it);
  //  auto mat         = light_handler.getUMatPointerSpereLight(light_index);

  //  gi->addMaterial(material_handler.getMaterialFromUMat(mat));

  //  geometry_group->addChild(gi);
  //  light_handler.addSphereLight(gi);
  //}


  //_context["top_object"]->set(geometry_group);
  _context["top_object"]->set(top_group);



  mrf::gui::fb::Loger::getInstance()->info("Optix Renderer MRF scene imported, total triangle count:", num_triangles);
}


optix::GeometryInstance OptixGeometryHandler::createParallelogram(
    const optix::float3 &anchor,
    const optix::float3 &offset1,
    const optix::float3 &offset2)
{
  optix::Geometry parallelogram = _context->createGeometry();
  parallelogram->setPrimitiveCount(1u);
  parallelogram->setIntersectionProgram(_pgram_intersection);
  parallelogram->setBoundingBoxProgram(_pgram_bounding_box);

  optix::float3 normal = optix::normalize(optix::cross(offset1, offset2));
  float         d      = optix::dot(normal, anchor);
  optix::float4 plane  = make_float4(normal, d);

  optix::float3 v1 = offset1 / optix::dot(offset1, offset1);
  optix::float3 v2 = offset2 / optix::dot(offset2, offset2);

  parallelogram["plane"]->setFloat(plane);
  parallelogram["anchor"]->setFloat(anchor);
  parallelogram["v1"]->setFloat(v1);
  parallelogram["v2"]->setFloat(v2);

  optix::GeometryInstance gi = _context->createGeometryInstance();
  gi->setGeometry(parallelogram);
  return gi;
}

optix::GeometryInstance OptixGeometryHandler::createSphere(const optix::float3 &center, const float &radius)
{
  optix::Geometry sphere = _context->createGeometry();
  sphere->setPrimitiveCount(1u);
  sphere->setIntersectionProgram(_pgram_intersection_sphere);
  sphere->setBoundingBoxProgram(_pgram_bounding_box_sphere);

  sphere["center"]->setFloat(center);
  sphere["radius"]->setFloat(radius);

  optix::GeometryInstance gi = _context->createGeometryInstance();
  gi->setGeometry(sphere);
  return gi;
}

std::vector<std::pair<optix::GeometryInstance, mrf::geom::Shape *>> &OptixGeometryHandler::geometryInstances()
{
  return _optix_geometry_instances;
}

void OptixGeometryHandler::loadMeshFromMRF(
    mrf::geom::Mesh const &mrf_mesh,
    OptiXMesh &            optix_mesh,
    const float *          transform)
{
  if (!optix_mesh.context)
  {
    throw std::runtime_error("OptiXMesh: loadMesh() requires valid OptiX context");
  }

  optix::Context context = optix_mesh.context;

  Mesh             mesh;
  OptixMeshBuffers buffers;

  mesh.num_triangles = mrf_mesh.numSubObjects();
  mesh.num_vertices  = static_cast<int>(mrf_mesh.vertices().size());
  if (mrf_mesh.normals().size() == mrf_mesh.vertices().size()) mesh.has_normals = true;
  if (mrf_mesh.uvs().size() == mrf_mesh.vertices().size()) mesh.has_texcoords = true;
  if (mrf_mesh.tangents().size() == mrf_mesh.vertices().size()) mesh.has_tangents = true;

  setupMeshLoaderInputs(context, buffers, mesh);

  memcpy(mesh.positions, &mrf_mesh.vertices()[0], 3 * mrf_mesh.vertices().size() * sizeof(float));

  mesh.bbox_min[0] = mrf_mesh.bbox().getMin().x();
  mesh.bbox_min[1] = mrf_mesh.bbox().getMin().y();
  mesh.bbox_min[2] = mrf_mesh.bbox().getMin().z();

  mesh.bbox_max[0] = mrf_mesh.bbox().getMax().x();
  mesh.bbox_max[1] = mrf_mesh.bbox().getMax().y();
  mesh.bbox_max[2] = mrf_mesh.bbox().getMax().z();


  if (mesh.has_normals) memcpy(mesh.normals, &mrf_mesh.normals()[0], 3 * mrf_mesh.normals().size() * sizeof(float));

  if (mesh.has_texcoords)
  {
    //CANNOT COPY DIRECTLY NEED TO CONVERT FROM Vec3f to FLOAT2
    //memcpy(mesh.texcoords, &mrf_mesh.uvs()[0], 3 * mrf_mesh.uvs().size() * sizeof(float));
    for (uint64_t i = 0; i < mrf_mesh.uvs().size(); ++i)
    {
      mesh.texcoords[2 * i]     = mrf_mesh.uvs()[i].x();
      mesh.texcoords[2 * i + 1] = mrf_mesh.uvs()[i].y();
    }
  }

  if (mesh.has_tangents)
  {
    memcpy(mesh.tangents, &mrf_mesh.tangents()[0], 3 * mrf_mesh.tangents().size() * sizeof(float));
  }

  memcpy(mesh.tri_indices, &mrf_mesh.faces()[0], mrf_mesh.faces().size() * sizeof(unsigned int));

  if (transform) applyLoadXForm(mesh, transform);

  optix_mesh.bbox_min      = mrf::optix_backend::make_float3(mesh.bbox_min);
  optix_mesh.bbox_max      = mrf::optix_backend::make_float3(mesh.bbox_max);
  optix_mesh.num_triangles = mesh.num_triangles;

  // Rewrite all mat_indices to point to single override material
  memset(mesh.mat_indices, 0, mesh.num_triangles * sizeof(uint32_t));

  optix_mesh.geom_tri = optix_mesh.context->createGeometryTriangles();
  optix_mesh.geom_tri->setPrimitiveCount(mesh.num_triangles);
  optix_mesh.geom_tri->setVertices(
      static_cast<unsigned int>(mrf_mesh.vertices().size()),
      buffers.positions,
      RT_FORMAT_FLOAT3);
  optix_mesh.geom_tri->setTriangleIndices(buffers.tri_indices, RT_FORMAT_UNSIGNED_INT3);

  optix_mesh.geom_tri["vertex_buffer"]->setBuffer(buffers.positions);
  optix_mesh.geom_tri["normal_buffer"]->setBuffer(buffers.normals);
  optix_mesh.geom_tri["texcoord_buffer"]->setBuffer(buffers.texcoords);
  optix_mesh.geom_tri["tangent_buffer"]->setBuffer(buffers.tangents);
  optix_mesh.geom_tri["index_buffer"]->setBuffer(buffers.tri_indices);

  optix_mesh.geom_tri->setAttributeProgram(optix_mesh.attributes);

  /*
  if (optix_materials.size() == 1)
  {
    // Rewrite all mat_indices to point to single override material
    memset(mesh.mat_indices, 0, mesh.num_triangles * sizeof(uint32_t));

    optix::GeometryTriangles geometry = optix_mesh.context->createGeometryTriangles();
    geometry->setPrimitiveCount(mesh.num_triangles);
    geometry->setVertices(static_cast<unsigned int>(mrf_mesh.vertices().size()), buffers.positions, RT_FORMAT_FLOAT3);
    geometry->setTriangleIndices(buffers.tri_indices, RT_FORMAT_UNSIGNED_INT3);

    geometry["vertex_buffer"]->setBuffer(buffers.positions);
    geometry["normal_buffer"]->setBuffer(buffers.normals);
    geometry["texcoord_buffer"]->setBuffer(buffers.texcoords);
    geometry["tangent_buffer"]->setBuffer(buffers.tangents);
    geometry["index_buffer"]->setBuffer(buffers.tri_indices);

    geometry->setAttributeProgram(optix_mesh.attributes);

    //Materials: TODO: per face
    //geometry->setMaterialCount((unsigned int)optix_materials.size());
    //geometry->setMaterialIndices(buffers.mat_indices, 0, 0, RT_FORMAT_UNSIGNED_INT);

    optix_mesh.geom_instance
        = optix_mesh.context->createGeometryInstance(geometry, optix_materials.begin(), optix_materials.end());
  }
  else
  {
    assert(mrf_mesh.materialPerFaces().size() == mesh.num_triangles);
    memcpy(mesh.mat_indices, &mrf_mesh.materialPerFaces()[0], mrf_mesh.materialPerFaces().size() * sizeof(uint32_t));

    optix::Geometry geometry = optix_mesh.context->createGeometry();
    geometry->setPrimitiveCount(mesh.num_triangles);

    geometry["vertex_buffer"]->setBuffer(buffers.positions);
    geometry["normal_buffer"]->setBuffer(buffers.normals);
    geometry["texcoord_buffer"]->setBuffer(buffers.texcoords);
    geometry["tangent_buffer"]->setBuffer(buffers.tangents);
    geometry["index_buffer"]->setBuffer(buffers.tri_indices);
    geometry["material_buffer"]->setBuffer(buffers.mat_indices);

    geometry->setBoundingBoxProgram(optix_mesh.bounds);
    geometry->setIntersectionProgram(optix_mesh.intersection);

    optix_mesh.geom_instance
        = optix_mesh.context->createGeometryInstance(geometry, optix_materials.begin(), optix_materials.end());
  }
  */

  buffers.tri_indices->unmap();
  buffers.mat_indices->unmap();
  buffers.positions->unmap();
  if (mesh.has_normals) buffers.normals->unmap();
  if (mesh.has_texcoords) buffers.texcoords->unmap();
  if (mesh.has_tangents) buffers.tangents->unmap();

  mesh.tri_indices = 0;
  mesh.mat_indices = 0;
  mesh.positions   = 0;
  mesh.normals     = 0;
  mesh.texcoords   = 0;
  mesh.tangents    = 0;

  optix_mesh.has_texcoords = mesh.has_texcoords;
}

void OptixGeometryHandler::setupMeshLoaderInputs(optix::Context context, OptixMeshBuffers &buffers, Mesh &mesh)
{
  buffers.tri_indices = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_INT3, mesh.num_triangles);
  buffers.mat_indices = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_UNSIGNED_INT, mesh.num_triangles);
  buffers.positions   = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, mesh.num_vertices);
  buffers.normals = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, mesh.has_normals ? mesh.num_vertices : 0);
  buffers.texcoords
      = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT2, mesh.has_texcoords ? mesh.num_vertices : 0);
  buffers.tangents
      = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, mesh.has_tangents ? mesh.num_vertices : 0);

  mesh.tri_indices = reinterpret_cast<int32_t *>(buffers.tri_indices->map());
  mesh.mat_indices = reinterpret_cast<uint32_t *>(buffers.mat_indices->map());
  mesh.positions   = reinterpret_cast<float *>(buffers.positions->map());
  mesh.normals     = reinterpret_cast<float *>(mesh.has_normals ? buffers.normals->map() : 0);
  mesh.texcoords   = reinterpret_cast<float *>(mesh.has_texcoords ? buffers.texcoords->map() : 0);
  mesh.tangents    = reinterpret_cast<float *>(mesh.has_tangents ? buffers.tangents->map() : 0);

  //mesh.mat_params = new MaterialParams[mesh.num_materials];
}


void applyLoadXForm(mrf::optix_backend::Mesh &mesh, const float *load_xform)
{
  if (!load_xform) return;

  bool have_matrix = false;
  for (int32_t i = 0; i < 16; ++i)
    if (load_xform[i] != 0.0f) have_matrix = true;

  if (have_matrix)
  {
    mesh.bbox_min[0] = mesh.bbox_min[1] = mesh.bbox_min[2] = 1e16f;
    mesh.bbox_max[0] = mesh.bbox_max[1] = mesh.bbox_max[2] = -1e16f;

    optix::Matrix4x4 mat(load_xform);

    optix::float3 *positions = reinterpret_cast<optix::float3 *>(mesh.positions);
    #pragma omp parallel for
    for (int32_t i = 0; i < mesh.num_vertices; ++i)
    {
      const optix::float3 v = optix::make_float3(mat * optix::make_float4(positions[i], 1.0f));
      positions[i]          = v;
      mesh.bbox_min[0]      = std::min<float>(mesh.bbox_min[0], v.x);
      mesh.bbox_min[1]      = std::min<float>(mesh.bbox_min[1], v.y);
      mesh.bbox_min[2]      = std::min<float>(mesh.bbox_min[2], v.z);
      mesh.bbox_max[0]      = std::max<float>(mesh.bbox_max[0], v.x);
      mesh.bbox_max[1]      = std::max<float>(mesh.bbox_max[1], v.y);
      mesh.bbox_max[2]      = std::max<float>(mesh.bbox_max[2], v.z);
    }

    if (mesh.has_normals)
    {
      mat                    = mat.inverse().transpose();
      optix::float3 *normals = reinterpret_cast<optix::float3 *>(mesh.normals);

      #pragma omp parallel for
      for (int32_t i = 0; i < mesh.num_vertices; ++i)
        normals[i] = optix::make_float3(mat * optix::make_float4(normals[i], 1.0f));
    }
  }
}

}   // namespace optix_backend
}   // namespace mrf
