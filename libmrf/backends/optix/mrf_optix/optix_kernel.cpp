/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * author : Romain Pacanowski @ institutoptique DOT fr
 * Copyright CNRS 2020
 *
 *
 **/
#include <mrf_optix/optix_kernel.hpp>

#include <mrf_core/util/precision_timer.hpp>
#include <mrf_core/util/path.hpp>
#include <mrf_core/util/string_parsing.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_optix/optix_util.hpp>

#include <nvrtc.h>

#include <string>
#include <map>


#include <ghc/fs_std_fwd.hpp>

using optix::Exception;

namespace mrf
{
namespace optix_backend
{
OptixKernel::OptixKernel(): _config(PTXConfig()) {}
OptixKernel::OptixKernel(std::string filename): _prg_name(filename), _config(PTXConfig()) {}
OptixKernel::OptixKernel(std::string filename, std::vector<std::string> const &cuda_compile_options)
  : _prg_name(filename)
  , _prg_compile_options(cuda_compile_options)
  , _config(PTXConfig())
{}

OptixKernel::OptixKernel(std::string filename, std::vector<std::string> const &cuda_compile_options, PTXConfig ptx_cfg)
  : _prg_name(filename)
  , _prg_compile_options(cuda_compile_options)
  , _config(ptx_cfg)
{}

bool OptixKernel::readSourceFile(std::string &str, const std::string &filename)
{
  // Try to open file
  std::ifstream file(filename.c_str());
  if (file.good())
  {
    // Found usable source file
    std::stringstream source_buffer;
    source_buffer << file.rdbuf();
    str = source_buffer.str();
    return true;
  }
  return false;
}

#if CUDA_NVRTC_ENABLED

// This methods attemp to locate a kernel
void OptixKernel::getCuStringFromFile(std::string &cu, std::string &location, const char *filename)
{
  std::vector<std::string> source_locations;

  mrf::util::Path current_path = mrf::util::Path::getExecutionPath();
  std::string     base_dir     = current_path.name();

  // Potential source locations (in priority order)
  source_locations.push_back(base_dir + "/" + filename);

  std::string const mrf_cuda_dir = base_dir + "mrf_optix_programs";

  // Look inside the current execution dir/cuda/
  source_locations.push_back(mrf_cuda_dir + "/" + filename);
  source_locations.push_back(mrf_cuda_dir + "/include/" + filename);
  source_locations.push_back(mrf_cuda_dir + "/intersection_programs/" + filename);

  //add . as resource path for deployement
  source_locations.push_back(std::string("./mrf_optix_programs/") + std::string(filename));

  //absolute path
  source_locations.push_back(filename);

  source_locations.push_back(base_dir + "mrf_plugin_programs/" + filename);


  for (std::vector<std::string>::const_iterator it = source_locations.begin(); it != source_locations.end(); ++it)
  {
    // Try to get source code from file
    if (readSourceFile(cu, *it))
    {
      location = *it;
      return;
    }
  }

  // Wasn't able to find or open the requested file
  mrf::gui::fb::Loger::getInstance()->fatal("Couldn't open source file " + std::string(filename));
  //throw optix::Exception("Couldn't open source file " + std::string(filename));
}

static std::string g_nvrtcLog;

void OptixKernel::getPtxFromCuString(
    std::string &                   ptx,
    const char *                    cu_source,
    const char *                    name,
    std::vector<std::string> const &cuda_compile_options,
    const char **                   log_string)
{
#  ifdef MALIA_AND_MALIA_RGB_DEBUG
  mrf::gui::fb::Loger::getInstance()->debug(" CUDA Code source = " + std::string(cu_source));
  mrf::gui::fb::Loger::getInstance()->debug(" Cudea filename = " + std::string(name));
#  endif

  // Create program
  nvrtcProgram prog = 0;
  NVRTC_CHECK_ERROR(nvrtcCreateProgram(&prog, cu_source, name, 0, NULL, NULL));

  // Gather NVRTC options
  std::vector<const char *> options;

  // Collect include dirs
  std::vector<std::string> include_dirs;


  // std::cout << " WARNING mdl-sdk.include not deploye. THis is for the iRay Backend
  mrf::util::Path const current_path = mrf::util::Path::getExecutionPath();
  std::string const     cep          = current_path.name();
  std::string const     abs_dirs[]
      = {cep + "/optix_include/include/",
         cep + "/optix_include/include/optixu/",
         cep + "/cuda_sdk/include/",
         cep + "/cuda_sdk/",
         cep + "/mrf_optix_programs/",
         cep + "/mrf_optix_programs/include/",
         cep + "/mrf_optix_programs/intersection_programs/",
         cep + "/mrf_plugin_programs/",
         cep};

  const size_t n_abs_dirs = sizeof(abs_dirs) / sizeof(abs_dirs[0]);
  //std::cout << " COMPILING name = " << std::string(name) << std::endl;

  for (size_t i = 0; i < n_abs_dirs; i++)
  {
    // std::cout << __FILE__ << " " <<  __LINE__ << " including dir: " << abs_dirs[i] << std::endl;
    include_dirs.push_back(std::string("-I") + abs_dirs[i]);
  }

  for (std::vector<std::string>::const_iterator it = include_dirs.begin(); it != include_dirs.end(); ++it)
    options.push_back(it->c_str());

  // Collect NVRTC options
  const char * compiler_options[] = {CUDA_NVRTC_OPTIONS};
  const size_t n_compiler_options = sizeof(compiler_options) / sizeof(compiler_options[0]);
  for (size_t i = 0; i < n_compiler_options - 1; i++)
    options.push_back(compiler_options[i]);

  for (auto it_options = cuda_compile_options.cbegin(); it_options != cuda_compile_options.cend(); ++it_options)
  {
    options.push_back(it_options->c_str());
  }
#  ifdef MALIA_AND_MALIA_RGB_DEBUG
  mrf::gui::fb::Loger::getInstance()->debug("Cep = " + cep);
  mrf::gui::fb::Loger::getInstance()->debug(" Compilation Options passed to NVCC :");
  for (size_t i = 0; i < options.size(); i++)
  {
    std::cout << options[i] << " ";
  }
  std::cout << std::endl;
  int major;
  int minor;
  NVRTC_CHECK_ERROR(nvrtcVersion(&major, &minor));
  mrf::gui::fb::Loger::getInstance()->debug(
      "[INFO] From NVRTC Cuda Languages Versions (Major,Minor) = " + major + ", " + minor);
#  endif

  // JIT compile CU to PTX
  const nvrtcResult compileRes = nvrtcCompileProgram(prog, (int)options.size(), options.data());

  // Retrieve log output
  size_t log_size = 0;
  NVRTC_CHECK_ERROR(nvrtcGetProgramLogSize(prog, &log_size));
  g_nvrtcLog.resize(log_size);
  if (log_size > 1)
  {
    NVRTC_CHECK_ERROR(nvrtcGetProgramLog(prog, &g_nvrtcLog[0]));
    if (log_string) *log_string = g_nvrtcLog.c_str();
  }
  if (compileRes != NVRTC_SUCCESS)
  {
    mrf::gui::fb::Loger::getInstance()->fatal("NVRTC Compilation failed.\n" + g_nvrtcLog);
    //throw Exception("NVRTC Compilation failed.\n" + g_nvrtcLog);
  }

  // Retrieve PTX code
  size_t ptx_size = 0;
  NVRTC_CHECK_ERROR(nvrtcGetPTXSize(prog, &ptx_size));
  ptx.resize(ptx_size);
  NVRTC_CHECK_ERROR(nvrtcGetPTX(prog, &ptx[0]));

  // Cleanup
  NVRTC_CHECK_ERROR(nvrtcDestroyProgram(&prog));
}

#endif   // CUDA_NVRTC_ENABLED

std::string OptixKernel::getPTXpath(const char *filename)
{
  //std::string current_path, abs_filename;
  //mrf::util::StringParsing::getDirectory(_prg_path.c_str(), current_path);
  //mrf::util::StringParsing::getFileNameWithoutExtension(filename, abs_filename);

  std::string current_path = mrf::util::Path::getExecutionPath().name() + "precompiled_ptx/";
  std::string abs_filename;
  mrf::util::StringParsing::getFileNameWithoutExtension(filename, abs_filename);

  std::string suffix = "";

  if (_config.mode_sensitive)
  {
    bool is_spectral
        = std::find(_prg_compile_options.begin(), _prg_compile_options.end(), "-DMRF_RENDERING_MODE_SPECTRAL")
          != _prg_compile_options.end();
    if (is_spectral)
    {
      bool is_multi = std::find(
                          _prg_compile_options.begin(),
                          _prg_compile_options.end(),
                          "-DMRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED")
                      != _prg_compile_options.end();
      if (is_multi)
      {
        auto option_it = std::find(
            _prg_compile_options.begin(),
            _prg_compile_options.end(),
            "-DMRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED");
        int         wpp_option_idx = static_cast<int>(std::distance(_prg_compile_options.begin(), option_it));
        std::string wpp_str        = _prg_compile_options[wpp_option_idx + 1];
        std::size_t wpp_int_pos    = wpp_str.find("=");
        suffix                     = "wpp_" + wpp_str.substr(wpp_int_pos + 1);
      }
      else
        suffix = "single";
    }
    else
      suffix = "rgb";
  }

  if (suffix.size() > 0)
  {
    current_path += suffix + "/";
    fs::path dir(current_path);
    if (!fs::exists(dir))
    {
      fs::create_directory(dir);
    }
    suffix = "_" + suffix;
  }

  return current_path + abs_filename + suffix + ".ptx";
}

bool OptixKernel::getPtxStringFromFile(std::string &ptx, const char *filepath)
{
  std::string source_filename = getPTXpath(filepath);

  // Try to open source PTX file
  if (!readSourceFile(ptx, source_filename))
  {
    mrf::gui::fb::Loger::getInstance()->debug("Couldn't open source file " + source_filename);
    //throw Exception("Couldn't open source file " + source_filename);
    return false;
  }
  return true;
}

bool OptixKernel::exportPTX(const char *filepath)
{
  std::string source_filename = getPTXpath(filepath);

  std::ofstream ptx;
  ptx.open(source_filename);
  ptx.write(_prg_ptx.c_str(), _prg_ptx.size());
  ptx.close();

  return true;
}


struct PtxSourceCache
{
  std::map<std::string, std::string> map;
  //~PtxSourceCache()
  //{
  //  for (std::map<std::string, std::string>::const_iterator it = map.begin(); it != map.end(); ++it)
  //    delete it->second;
  //}
};

static PtxSourceCache g_ptxSourceCache;

optix::Program OptixKernel::createProgram(optix::Context &context, std::string fct_name)
{
  if (_prg_ptx.size() > 0)
    return context->createProgramFromPTXString(_prg_ptx, fct_name);
  else
    throw std::runtime_error("Trying to create a program without PTX ! Check that the kernel has been compiled.");
}

void OptixKernel::load(optix::Context &)
{
#if CUDA_NVRTC_ENABLED
  std::string key = _prg_name;

  for (auto it = _prg_compile_options.cbegin(); it != _prg_compile_options.cend(); ++it)
  {
    key += ";" + *it;
  }

  std::map<std::string, std::string>::iterator elem = g_ptxSourceCache.map.find(key);

  if (elem == g_ptxSourceCache.map.end())
  {
    getCuStringFromFile(_prg_cu_srcs, _prg_path, _prg_name.c_str());
  }
#endif
}

void OptixKernel::load(optix::Context &context, std::string filename)
{
#if CUDA_NVRTC_ENABLED
  _prg_name = filename;
  load(context);
#endif
}

void OptixKernel::compile(optix::Context &context, std::string ptx_name, const char **log)
{
  std::string key = _prg_name;

  if (_prg_cu_srcs.size() == 0) load(context);

  for (auto it = _prg_compile_options.cbegin(); it != _prg_compile_options.cend(); ++it)
  {
    key += ";" + *it;
  }

  std::map<std::string, std::string>::iterator elem = g_ptxSourceCache.map.find(key);

  if (elem == g_ptxSourceCache.map.end())
  {
#if CUDA_NVRTC_ENABLED
    if (!_config.runtime_compilation)
    {
      if (!getPtxStringFromFile(_prg_ptx, ptx_name.c_str()) || _config.overwrite_ptx)
      {
        mrf::gui::fb::Loger::getInstance()->info("Generating PTX for kernel: " + getPTXpath(ptx_name.c_str()));
        getPtxFromCuString(_prg_ptx, _prg_cu_srcs.c_str(), _prg_path.c_str(), _prg_compile_options, log);
        exportPTX(ptx_name.c_str());
      }
    }
    else
      getPtxFromCuString(_prg_ptx, _prg_cu_srcs.c_str(), _prg_path.c_str(), _prg_compile_options, log);

#else
    if (_use_precompiled_ptx)
    {
      getPtxStringFromFile(_prg_ptx, ptx_name.c_str())
    }
    else
    {
      throw std::runtime_error "No PTX for current program !";
    }
#endif
    g_ptxSourceCache.map[key] = _prg_ptx;
  }
  else
  {
    _prg_ptx = elem->second;
  }
}

void OptixKernel::compile(optix::Context &context, const char **log)
{
  compile(context, _prg_name, log);
}

void OptixKernel::compile(
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    std::string                     ptx_name,
    const char **                   log)
{
  _prg_compile_options = cuda_compile_options;
  compile(context, ptx_name, log);
}

bool OptixKernel::addInclude(std::string incl_path, std::string clue)
{
  if (_prg_cu_srcs.size() == 0) return false;

  std::string include_str = "\n #include \"" + incl_path + "\"\n";

  _prg_cu_srcs.insert(_prg_cu_srcs.find(clue) + clue.size(), include_str);

  return true;
}

}   // namespace optix_backend
}   // namespace mrf
