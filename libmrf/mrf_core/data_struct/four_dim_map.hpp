/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 *
 **/


#pragma once

#include <mrf_core/math/math.hpp>
#include <mrf_core/data_struct/three_dim_map.hpp>

#include <iostream>
#include <map>

namespace mrf
{
namespace util
{
template<typename T, class DATA_TYPE>
class _FourDimMap
{
protected:
  std::map<T, _ThreeDimMap<T, DATA_TYPE>> _map;

public:
  inline unsigned int size() const;

  inline typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::const_iterator begin() const;
  inline typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::const_iterator end() const;

  /**
   * Insert an element into the map
   **/
  inline bool insert(T key1, T key2, T key3, T key4, DATA_TYPE const &data);

  /**
   * Returns true if key1 exists in the map
   **/
  inline bool findkey(T key1) const;


  inline DATA_TYPE linear(T t_key1, T t_key2, T t_key3, T t_key_4) const;




  /**
   * Returns the nearest element
   **/
  DATA_TYPE const &operator()(T key1, T key2, T t_key3, T t_key4) const;
};



template<typename T, class DATA_TYPE>
inline unsigned int _FourDimMap<T, DATA_TYPE>::size() const
{
  unsigned int size = 0;

  typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::const_iterator it;

  for (it = begin(); it != end(); ++it)
  {
    size += it->second.size();
  }

  return size;
}



template<typename T, class DATA_TYPE>
inline typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::const_iterator _FourDimMap<T, DATA_TYPE>::begin() const
{
  return _map.begin();
}


template<typename T, class DATA_TYPE>
inline typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::const_iterator _FourDimMap<T, DATA_TYPE>::end() const
{
  return _map.end();
}



template<typename T, class DATA_TYPE>
inline bool _FourDimMap<T, DATA_TYPE>::insert(T key1, T key2, T key3, T key4, DATA_TYPE const &data)
{
  if (findkey(key1))
  {
    return _map[key1].insert(key2, key3, key4, data);
  }
  else
  {
    _ThreeDimMap<T, DATA_TYPE> a_map;
    a_map.insert(key2, key3, key4, data);
    std::pair<typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::iterator, bool> inserted;
    inserted = _map.insert(make_pair(key1, a_map));
    return inserted.second;
  }
}



template<typename T, class DATA_TYPE>
inline bool _FourDimMap<T, DATA_TYPE>::findkey(T key1) const
{
  typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::const_iterator a_cit;
  a_cit = _map.find(key1);
  return a_cit != _map.end();
}


template<typename T, class DATA_TYPE>
DATA_TYPE const &_FourDimMap<T, DATA_TYPE>::operator()(T key1, T key2, T key3, T key4) const
{
  using namespace mrf::math;

  typedef typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::const_iterator IT_TYPE;
  IT_TYPE                                                                  upper_it = _map.lower_bound(key1);
  IT_TYPE                                                                  lower_it = upper_it;
  lower_it--;

  IT_TYPE n_it;   //nearest iterator

  if (upper_it == _map.begin())   //  < min_map
  {
    n_it = upper_it;
  }
  else if (upper_it == _map.end() && lower_it != _map.end())   // > max_map
  {
    n_it = lower_it;
  }
  else   //between keys
  {
    T const diff_upper = _Math<T>::absVal(upper_it->first - key1);
    T const diff_lower = _Math<T>::absVal(lower_it->first - key1);

    if (diff_upper <= diff_lower)
    {
      n_it = upper_it;
    }
    else
    {
      n_it = lower_it;
    }
  }


  return (n_it->second)(key2, key3, key4);
}



template<typename T, class DATA_TYPE>
inline DATA_TYPE _FourDimMap<T, DATA_TYPE>::linear(T t_key1, T t_key2, T t_key3, T t_key4) const
{
  using namespace mrf::math;

  // element which is >= targeted_key
  typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::const_iterator upper_it = _map.lower_bound(t_key1);
  typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::const_iterator lower_it = upper_it;
  lower_it--;


  if (upper_it == _map.begin())   //  < min_map
  {
    return upper_it->second.linear(t_key2, t_key3, t_key4);
  }
  else if (upper_it == _map.end() && lower_it != _map.end())   // > max_map
  {
    return lower_it->second.linear(t_key2, t_key3, t_key4);
  }
  else if (upper_it != _map.end() && lower_it != _map.end())   // between two keys
  {
    //Bilinear interpolation !
    DATA_TYPE d1 = upper_it->second.linear(t_key2, t_key3, t_key4);
    DATA_TYPE d2 = lower_it->second.linear(t_key2, t_key3, t_key4);

    T const diff_upper = _Math<T>::absVal(upper_it->first - t_key1);
    T const diff_lower = _Math<T>::absVal(lower_it->first - t_key1);

    T const il = _Math<T>::absVal(upper_it->first - lower_it->first);

    T const alpha = T(1.0) - (diff_upper / il);
    T const beta  = T(1.0) - (diff_lower / il);

    return d1 * alpha + d2 * beta;
  }
  else   //Map empty???
  {
    assert(0);
  }
}

//-------------------------------------------------------------------------
// Extern Operator
//-------------------------------------------------------------------------
template<typename T, class DATA_TYPE>
inline std::ostream &operator<<(std::ostream &os, _FourDimMap<T, DATA_TYPE> const &four_dim_map)
{
  typename std::map<T, _ThreeDimMap<T, DATA_TYPE>>::const_iterator it;

  os << " [ ";

  for (it = four_dim_map.begin(); it != four_dim_map.end(); ++it)
  {
    os << " (" << it->first << " , " << it->second << ") " << std::endl;
  }

  os << " ] " << std::endl;

  return os;
}

}   // namespace util


}   // namespace mrf
