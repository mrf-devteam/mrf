/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016,2017
 *
 **/
#pragma once

#include <cassert>
#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#ifdef MRF_WITH_EIGEN_SUPPORT
#  include <mrf_core/data_struct/eigen_array2d.hpp>
#else
#  include <mrf_core/data_struct/mrf_array2d.hpp>
#endif

namespace mrf
{
namespace data_struct
{
//-------------------------------------------------------------------------
// Default Type Definitions
//
// No Matter the implementatio beneath the Array2D Class the following
// Default types are always defined
//-------------------------------------------------------------------------
typedef _Array2D<float> Array2D;

typedef _Array2D<float>       _Array2Df;
typedef _Array2D<double>      _Array2Dd;
typedef _Array2D<long double> _Array2Dld;

typedef _Array2D<int>          _Array2Di;
typedef _Array2D<unsigned int> _Array2Dui;
}   // namespace data_struct
}   // namespace mrf
