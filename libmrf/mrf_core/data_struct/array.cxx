
template<typename T>
void _Array<T>::fill(T *ptab, std::vector<T> const &tab, unsigned int n_element)
{
  assert(n_element <= tab.size());

  for (unsigned int i = 0; i < n_element; i++)
  {
    //std::cout << "  element "<< i << "  = "  << tab[i] << std::endl;

    ptab[i] = tab[i];
  }
}


template<typename T>
void _Array<T>::fill(std::vector<T> &tab, T const *ptab, unsigned int n_element)
{
  tab.resize(n_element);
  for (unsigned int i = 0; i < n_element; i++)
  {
    tab[i] = ptab[i];
  }
}
