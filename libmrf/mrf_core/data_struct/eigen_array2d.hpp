/**
 * @file eigen_array2d.hpp
 * @author Arthur Dufay (adufay@inria.fr)
 * @brief
 * @version 0.1
 * @date 2018-11-12
 *
 * @copyright Copyright INRIA (c) 2018
 *
 */
#pragma once

#ifdef MRF_WITH_EIGEN_SUPPORT

#  include <mrf_core_dll.hpp>
#  include <mrf_core/mrf_types.hpp>

#  include <Eigen/Core>

#  include <cstddef>


namespace mrf
{
namespace data_struct
{
template<class T>
class _Array2D: public Eigen::Array<T, -1, -1, Eigen::RowMajor>
{
public:
  // Default constructor enabling std::vector<Array2D>
  _Array2D() {}

  //inverse parameter width ad height because conctructor of eigen is Array2D(rows,cols)
  _Array2D(size_t width, size_t height): _Array2D(static_cast<Eigen::Index>(height), static_cast<Eigen::Index>(width))
  {
    //We force to Zero all coefficients to have the same behavior between the MRF and Eigen implementations
    this->setZero();
  }

  // Add width and heigth features
  inline size_t width() const { return this->cols(); }
  inline size_t height() const { return this->rows(); }

  // Ensure same types
  inline size_t cols() const { return static_cast<size_t>(Eigen::Array<T, -1, -1, Eigen::RowMajor>::cols()); }
  inline size_t rows() const { return static_cast<size_t>(Eigen::Array<T, -1, -1, Eigen::RowMajor>::rows()); }

  //support eigen expression: mandatory
  // This constructor allows you to construct _Array2Df from Eigen expressions
  template<typename OtherDerived>
  _Array2D(const Eigen::ArrayBase<OtherDerived> &other): Eigen::Array<T, -1, -1, Eigen::RowMajor>(other)
  {}
  // This method allows you to assign Eigen expressions to _Array2Df
  template<typename OtherDerived>
  _Array2D &operator=(const Eigen::ArrayBase<OtherDerived> &other)
  {
    this->Eigen::Array<T, -1, -1, Eigen::RowMajor>::operator=(other);
    return *this;
  }

  //remove some constructor to be similar to MRF_Array
private:
  _Array2D(Eigen::Index width, Eigen::Index height): Eigen::Array<T, -1, -1, Eigen::RowMajor>(width, height)
  {
    this->setZero();
  }
  _Array2D(const T * /*data*/) {};
  _Array2D(Eigen::Index /*dim*/) {};
  _Array2D(const T & /*value*/) {};
  _Array2D(const T & /*val0*/, const T & /*val1*/) {};
  _Array2D(const T & /*val0*/, const T & /*val1*/, const T & /*val2*/) {};
  _Array2D(const T & /*val0*/, const T & /*val1*/, const T & /*val2*/, const T & /*val3*/) {};

  // template<typename OtherDerived >
  // _Array2Df(const Eigen::EigenBase< OtherDerived > &other,
  //   typename internal::enable_if< internal::is_convertible< typename OtherDerived::Scalar, Scalar >::value, PrivateType >::type = PrivateType()) {};
};

template<class T>
bool operator!=(_Array2D<T> const &a1, _Array2D<T> const &a2)
{
  if (a1.cols() != a2.cols())
  {
    return true;
  }

  if (a1.rows() != a2.rows())
  {
    return true;
  }


  return (a1.any() != a2.any());
}

template<class T>
bool operator==(_Array2D<T> const &a1, _Array2D<T> const &a2)
{
  if (a1.cols() != a2.cols())
  {
    return false;
  }

  if (a1.rows() != a2.rows())
  {
    return false;
  }

  return (a1.all() == a2.all());
}

}   // namespace data_struct
}   // namespace mrf

#endif
