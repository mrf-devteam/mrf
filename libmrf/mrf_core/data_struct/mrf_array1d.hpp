/**
 * @file mrf_array1d.hpp
 * @author Romain Pacanowski (romain.pacanowski@institutoptique.fr)
 * @brief
 * @version 0.1
 * @date 2018-11-12
 *
 * @copyright Copyright (c) CNRS 2018
 *
 */

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace data_struct
{
template<typename T>
class _Array
{
public:
  _Array(uint size = 0, T initial_value = T());

  inline T *      ptr();
  inline T const *ptr() const;

private:
  T *  _data;
  uint _size;
  uint _reservedSize;   //More allocation because we want aligned DATA

  // STATIC METHODS
public:
  //Fill a pointer array from a std::vector
  static void fill(
      T *                   ptab,   //pointer tab array
      std::vector<T> const &tab,
      unsigned int          n_element);   //number of elements to be copied

  /* Copy to the std::vector */
  static void fill(std::vector<T> &tab, T const *ptab, unsigned int n_element);
};


}   // namespace data_struct
}   // namespace mrf