/**
 * @file eigen_array1d.hpp
 * @author Romain Pacanowski (romain.pacanowski@institutoptique.fr)
 * @brief
 * @version 0.1
 * @date 2018-11-12
 *
 * @copyright Copyright (c) 2018
 *
 */

#pragma once

#include "Eigen/Core"

// Partial typedef Template
//typedef template<typename T> _Array<T> Eigen::VectorXd<T>;
