/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/

#pragma once

#include <mrf_core/data_struct/one_dim_map.hpp>

#include <mrf_core/math/math.hpp>

#include <iostream>
#include <map>


namespace mrf
{
namespace util
{
template<typename T, class DATA_TYPE>
class _TwoDimMap
{
protected:
  std::map<T, _OneDimMap<T, DATA_TYPE>> _map;


public:
  inline unsigned int size() const;

  inline typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator begin() const;
  inline typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator end() const;

  inline typename std::map<T, _OneDimMap<T, DATA_TYPE>>::iterator begin();
  inline typename std::map<T, _OneDimMap<T, DATA_TYPE>>::iterator end();

  inline std::map<T, _OneDimMap<T, DATA_TYPE>> const &map() const;
  inline std::map<T, _OneDimMap<T, DATA_TYPE>> &      map();

  /**
   * Insert an element into the map
   **/
  inline bool insert(T key1, T key2, DATA_TYPE const &data);

  /**
   * Returns true if key1 exists in the map
   **/
  inline bool findkey(T key1) const;

  /**
   * Returns the nearest element
   **/
  DATA_TYPE const &operator()(T key1, T key2) const;
  inline void      nearest(
           T                                                               key1,
           T                                                               key2,
           typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator &n_it,
           typename std::map<T, DATA_TYPE>::const_iterator &               n_it2) const;


  inline void nearestKey(T t_key1, T t_key2, T &key1, T &key2) const;

  inline DATA_TYPE const &nearestValue(T t_key1, T t_key2) const;


  inline DATA_TYPE linear(T t_key1, T t_key2) const;

  /**
   * Return the value by taking the first key that is lesser than t_key1
   * and by performing a linear interpolation for t_key2
   **/
  inline DATA_TYPE infLinear(T t_key1, T t_key2) const;
  inline DATA_TYPE infInf(T t_key1, T t_key2) const;
  inline DATA_TYPE infNearest(T t_key1, T t_key2) const;


  /**
   * Compute the derivative of the 2nd parameter
   * for a given t_key1 taken by lower value.
   **/
  inline DATA_TYPE infDerivative(T t_key1, T t_key2) const;

  inline void infKeysAndValues(T t_key1, T t_key2, T &key2_inf, T &key2_sup, T &value2_inf, T &value2_sup) const;
};

template<typename T, class DATA_TYPE>
inline unsigned int _TwoDimMap<T, DATA_TYPE>::size() const
{
  unsigned int size = 0;

  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator it;

  for (it = begin(); it != end(); ++it)
  {
    size += it->second.size();
  }

  return size;
}



template<typename T, class DATA_TYPE>
inline typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator _TwoDimMap<T, DATA_TYPE>::begin() const
{
  return _map.begin();
}


template<typename T, class DATA_TYPE>
inline typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator _TwoDimMap<T, DATA_TYPE>::end() const
{
  return _map.end();
}



template<typename T, class DATA_TYPE>
inline typename std::map<T, _OneDimMap<T, DATA_TYPE>>::iterator _TwoDimMap<T, DATA_TYPE>::begin()
{
  return _map.begin();
}

template<typename T, class DATA_TYPE>
inline typename std::map<T, _OneDimMap<T, DATA_TYPE>>::iterator _TwoDimMap<T, DATA_TYPE>::end()
{
  return _map.end();
}



template<typename T, class DATA_TYPE>
inline std::map<T, _OneDimMap<T, DATA_TYPE>> const &_TwoDimMap<T, DATA_TYPE>::map() const
{
  return _map;
}

template<typename T, class DATA_TYPE>
inline std::map<T, _OneDimMap<T, DATA_TYPE>> &_TwoDimMap<T, DATA_TYPE>::map()
{
  return _map;
}




template<typename T, class DATA_TYPE>
inline bool _TwoDimMap<T, DATA_TYPE>::insert(T key1, T key2, DATA_TYPE const &data)
{
  if (findkey(key1))
  {
    //std::cout << " TWO DIM Map. A Key is already there " << std::endl;
    return _map[key1].insert(key2, data);
  }
  else
  {
    _OneDimMap<T, DATA_TYPE> a_map;
    a_map.insert(key2, data);
    std::pair<typename std::map<T, _OneDimMap<T, DATA_TYPE>>::iterator, bool> inserted;
    inserted = _map.insert(std::make_pair(key1, a_map));
    return inserted.second;
  }
}

template<typename T, class DATA_TYPE>
inline bool _TwoDimMap<T, DATA_TYPE>::findkey(T key1) const
{
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator a_cit;
  a_cit = _map.find(key1);
  return a_cit != _map.end();
}


template<typename T, class DATA_TYPE>
inline void _TwoDimMap<T, DATA_TYPE>::nearest(
    T                                                               key1,
    T                                                               key2,
    typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator &n_it,
    typename std::map<T, DATA_TYPE>::const_iterator &               n_it2) const
{
  nearestIT(key1, _map, n_it);
  nearestIT(key2, n_it->second.map(), n_it2);
}

template<typename T, class DATA_TYPE>
inline void _TwoDimMap<T, DATA_TYPE>::nearestKey(T t_key1, T t_key2, T &key1, T &key2) const
{
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator an_nearest_it;
  typename std::map<T, DATA_TYPE>::const_iterator                an_nearest_it2;

  nearest(t_key1, t_key2, an_nearest_it, an_nearest_it2);


  key1 = an_nearest_it->first;
  key2 = an_nearest_it2->first;
}



template<typename T, class DATA_TYPE>
inline DATA_TYPE const &_TwoDimMap<T, DATA_TYPE>::nearestValue(T t_key1, T t_key2) const
{
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator an_nearest_it;
  typename std::map<T, DATA_TYPE>::const_iterator                an_nearest_it2;

  nearest(t_key1, t_key2, an_nearest_it, an_nearest_it2);

  //     std::cout << " key1 = " << t_key1 << " key2 = " << t_key2
  //               << " with nearest 1 = " << an_nearest_it->first
  //               << " with nearest 2 = " << an_nearest_it2->first << std::endl;


  return an_nearest_it2->second;
}


template<typename T, class DATA_TYPE>
DATA_TYPE const &_TwoDimMap<T, DATA_TYPE>::operator()(T key1, T key2) const
{
  return nearestValue(key1, key2);
}


template<typename T, class DATA_TYPE>
inline DATA_TYPE _TwoDimMap<T, DATA_TYPE>::linear(T t_key1, T t_key2) const
{
  using namespace mrf::math;

  // element which is >= targeted_key
  //typename std::map< T, _OneDimMap<T, DATA_TYPE> >::const_iterator upper_it = _map.lower_bound( t_key1 );
  //typename std::map< T, _OneDimMap<T, DATA_TYPE> >::const_iterator lower_it = upper_it;

  typedef typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator CONST_MAP_IT;

  CONST_MAP_IT upper_it = _map.lower_bound(t_key1);
  CONST_MAP_IT lower_it = upper_it;

  if (upper_it == _map.begin())   //  < min_map
  {
    //std::cout << " CASE upper_it = begin. Two Dim Map upper_it->first: " << upper_it->first << std::endl;


    if (_map.size() >= 2)   //Line Extrapolation
    {
      CONST_MAP_IT up_upper_it = upper_it;
      up_upper_it++;

      // x1= upper_it->first,  y1 = upper_it->second.linear(t_key2 );
      // x2 = up+upper_it->first, y2 = up_upper_it->second.linear(t_key_2);

      T const         x1 = upper_it->first;
      DATA_TYPE const y1 = upper_it->second.linear(t_key2);

      T const         x2 = up_upper_it->first;
      DATA_TYPE const y2 = up_upper_it->second.linear(t_key2);

      _Line2D<T, DATA_TYPE> l(x1, y1, x2, y2);
      return l.valueAt(t_key1);

      //return upper_it->second.linear( t_key2 );
    }
    else   //Fall back to nearest
    {
      return upper_it->second.linear(t_key2);
    }
  }
  else
  {
    lower_it--;

    if (upper_it == _map.end() && lower_it != _map.end())   // > max_map
    {
      if (_map.size() >= 2)   //Line Interpolation
      {
        CONST_MAP_IT low_lower_it = lower_it;
        low_lower_it--;

        T const         x1 = low_lower_it->first;
        DATA_TYPE const y1 = low_lower_it->second.linear(t_key2);

        T const         x2 = lower_it->first;
        DATA_TYPE const y2 = lower_it->second.linear(t_key2);

        _Line2D<T, DATA_TYPE> l(x1, y1, x2, y2);
        return l.valueAt(t_key1);

        //return lower_it->second.linear( t_key2 );
      }
      else   // Fall-back to nearest
      {
        return lower_it->second.linear(t_key2);
      }
    }
    else if (upper_it != _map.end() && lower_it != _map.end())   // between two keys
    {
      //Upper is > map.begin()

      //Bilinear interpolation !
      // std::cout  << "TWO DIM MAP  CASE Bilinear interpolation " << std::endl;
      // std::cout << " Lower 1D Map is: " << lower_it->second << std::endl;
      // std::cout << " Lower 1D Map is: " << upper_it->second << std::endl;

      DATA_TYPE d1 = upper_it->second.linear(t_key2);
      DATA_TYPE d2 = lower_it->second.linear(t_key2);

      T const diff_upper = _Math<T>::absVal(upper_it->first - t_key1);
      T const diff_lower = _Math<T>::absVal(lower_it->first - t_key1);


      T const il = _Math<T>::absVal(upper_it->first - lower_it->first);

      T const alpha = T(1.0) - (diff_upper / il);
      T const beta  = T(1.0) - (diff_lower / il);

      return d1 * alpha + d2 * beta;
    }
    else   // Map empty?
    {
      assert(0);
    }
  }
}


template<typename T, class DATA_TYPE>
inline DATA_TYPE _TwoDimMap<T, DATA_TYPE>::infLinear(T t_key1, T t_key2) const
{
  using namespace mrf::math;
  assert(_map.size() > 0);

  // element which is >= targeted_key
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator upper_it = _map.upper_bound(t_key1);
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator lower_it = upper_it;
  lower_it--;

  if (lower_it == _map.end())
  {
    std::cout << "  upper_it = " << upper_it->first << std::endl;
    std::cout << "  _map.size() = " << _map.size() << std::endl;
    std::cout << "  t_key1 = " << t_key1 << "  t_key2 = " << t_key2 << std::endl;
    assert(0);
  }
  return lower_it->second.linear(t_key2);
}



template<typename T, class DATA_TYPE>
inline DATA_TYPE _TwoDimMap<T, DATA_TYPE>::infInf(T t_key1, T t_key2) const
{
  using namespace mrf::math;

  assert(_map.size() > 0);


  // element which is >= targeted_key
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator upper_it = _map.upper_bound(t_key1);
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator lower_it = upper_it;
  lower_it--;

  if (lower_it == _map.end())
  {
    std::cout << "  _map.size() = " << _map.size() << std::endl;

    assert(0);
  }
  return lower_it->second.inf(t_key2);
}


template<typename T, class DATA_TYPE>
inline DATA_TYPE _TwoDimMap<T, DATA_TYPE>::infNearest(T t_key1, T t_key2) const
{
  using namespace mrf::math;

  assert(_map.size() > 0);

  // element which is >= targeted_key
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator upper_it = _map.upper_bound(t_key1);
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator lower_it = upper_it;
  lower_it--;

  if (lower_it == _map.end())
  {
    assert(0);
  }

  return lower_it->second.nearestValue(t_key2);
}

template<typename T, class DATA_TYPE>
inline DATA_TYPE _TwoDimMap<T, DATA_TYPE>::infDerivative(T t_key1, T t_key2) const
{
  // element which is >= targeted_key
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator upper_it = _map.upper_bound(t_key1);
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator lower_it = upper_it;
  lower_it--;

  if (lower_it == _map.end())
  {
    assert(0);
  }

  //std::cout << " Key for tV = " << lower_it->first << std::endl;
  return lower_it->second.derivative(t_key2);
}

template<typename T, class DATA_TYPE>
inline void
_TwoDimMap<T, DATA_TYPE>::infKeysAndValues(T t_key1, T t_key2, T &key2_inf, T &key2_sup, T &value2_inf, T &value2_sup)
    const
{
  // element which is >= targeted_key
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator upper_it = _map.upper_bound(t_key1);
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator lower_it = upper_it;
  lower_it--;

  if (lower_it == _map.end())
  {
    assert(0);
  }

  //std::cout << " Key for tV = " << lower_it->first << std::endl;
  lower_it->second.keysAndValues(t_key2, key2_inf, key2_sup, value2_inf, value2_sup);
}
//-------------------------------------------------------------------------
// Extern Operator
//-------------------------------------------------------------------------
template<typename T, class DATA_TYPE>
inline std::ostream &operator<<(std::ostream &os, _TwoDimMap<T, DATA_TYPE> const &two_dim_map)
{
  typename std::map<T, _OneDimMap<T, DATA_TYPE>>::const_iterator it;

  os << " [ ";

  for (it = two_dim_map.begin(); it != two_dim_map.end(); ++it)
  {
    os << " (" << it->first << " , " << it->second << ") " << std::endl;
  }

  os << " ] " << std::endl;

  return os;
}

}   // namespace util
}   // namespace mrf
