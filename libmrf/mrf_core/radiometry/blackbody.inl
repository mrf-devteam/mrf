/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 **/
inline BlackBody::BlackBody(): _temperature(0) {}

inline BlackBody::BlackBody(uint temperature): _temperature(temperature) {}

inline uint BlackBody::temperature() const
{
  return _temperature;
}

inline void BlackBody::setTemperature(uint temperature)
{
  _temperature = temperature;
}

inline Spectrum<float> &BlackBody::spectrum()
{
  return _spectrum;
}

inline Spectrum<float> const &BlackBody::spectrum() const
{
  return _spectrum;
}

inline void BlackBody::generateSpectrum(uint start_wavelength, uint end_wavelength, uint offset_wavelength)
{
  offset_wavelength = std::min(uint(1), offset_wavelength);
  _spectrum.clear();

  double delta_lambda = offset_wavelength * 1E-9;

  for (uint i = start_wavelength; i <= end_wavelength; i += offset_wavelength)
  {
    double lambda = double(i) * 1E-9;   //convert from nanometer to meter

    //planck's formula
    double value = 2. * _PLANCK_TIMES_C_CONSTANT * _SPEED_OF_LIGHT * pow(lambda, -5.) * 1.
                   / exp(_PLANCK_TIMES_C_CONSTANT / (lambda * _temperature * _BOLTZMANN_CONSTANT) - 1);

    value *= delta_lambda;

    _spectrum.add(i, float(value));
  }
}