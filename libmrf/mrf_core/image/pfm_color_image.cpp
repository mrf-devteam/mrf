/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include <mrf_core/image/pfm_color_image.hpp>

#include <vector>
#include <array>

using mrf::color::Color;

namespace mrf
{
namespace image
{
PFMColorImage::PFMColorImage(size_t width, size_t height): ColorImage(width, height) {}

PFMColorImage::PFMColorImage(const std::string &file_path, std::function<void(int)> progress): ColorImage()
{
  progress(0);

  std::ifstream is(file_path, std::ios::in | std::ios::binary);

  if (!is.is_open())
  {
    throw MRF_ERROR_LOADING_FILE;
  }

  is.seekg(0, std::ios::beg);

  std::array<char, 3> format;
  is.read(&format[0], 2);
  format[2] = '\0';

  if (format[0] != 'P')
  {
    throw MRF_ERROR_WRONG_PIXEL_FORMAT;
  }

  // Format: RGB or monochrome
  int n_channels;

  switch (format[1])
  {
    case 'f':
      n_channels = 1;
      break;
    case 'F':
      n_channels = 3;
      break;
    default:
      throw MRF_ERROR_WRONG_PIXEL_FORMAT;
      break;
  }

  std::function<float(std::istream &)> getFloat = [](std::istream &s) {
    char              c = 0;
    std::vector<char> buff;
    // Consume front spaces / newlines
    do
    {
      s.read(&c, 1);
    } while (c == ' ' || c == '\n');

    do
    {
      buff.push_back(c);
      s.read(&c, 1);
    } while (c != ' ' && c != '\n');

    buff.push_back('\0');

    return std::stof(std::string(&buff[0]));
  };

  const size_t width  = static_cast<size_t>(getFloat(is));
  const size_t height = static_cast<size_t>(getFloat(is));

  MultiChannelImage<mrf::color::Color>::init(width, height);

  const float order = getFloat(is);

  // BYTE ORDER
  bool swap = order > 0.f;

  // Read pixels
  std::function<void(float *, bool)> swapBuffer = [](float *n, bool swap) {
    unsigned char *cptr;

    if (swap)
    {
      cptr = reinterpret_cast<unsigned char *>(n);
      std::swap(cptr[0], cptr[3]);
      std::swap(cptr[1], cptr[2]);
    }
  };

  const size_t       n_elem   = n_channels * _width * _height;
  const size_t       dataSize = sizeof(float) * n_elem;
  std::vector<float> bufferedImage(n_elem);

  is.read(reinterpret_cast<char *>(bufferedImage.data()), dataSize);

  if (!is)
  {
    throw MRF_ERROR_LOADING_FILE;
  }

  for (size_t y = 0; y < _height; y++)
  {
    #pragma omp parallel for schedule(static)
    for (int x = 0; x < static_cast<int>(_width); x++)
    {
      const size_t pfm_px   = y * _width + x;
      const size_t local_px = (_height - y - 1) * _width + x;
      for (int c = 0; c < 3; c++)
      {
        // We cap the color offset to n_channel to keep same value for
        // monochromatic PFM
        const size_t offset = n_channels * pfm_px + std::min(c, n_channels - 1);
        assert(offset < n_elem);
        swapBuffer(&bufferedImage[offset], swap);
        (*this)[local_px][c] = bufferedImage[offset];
      }
    }
    const float progress_value = static_cast<float>(100 * y) / static_cast<float>(_height - 1);
    progress(static_cast<int>(progress_value));
  }

  is.close();

  progress(100);
}

PFMColorImage::PFMColorImage(const ColorImage &img): ColorImage(img) {}


PFMColorImage::~PFMColorImage() {}

IMAGE_LOAD_SAVE_FLAGS PFMColorImage::save(
    const std::string &file_path,
    const std::string & /*additional_comments*/,
    std::function<void(int)> progress) const
{
  std::ofstream pfm_file;
  progress(0);

  pfm_file.open(file_path, std::ios::out | std::ios::binary);

  if (!pfm_file.is_open())
  {
    return MRF_ERROR_SAVING_FILE;
  }

  // PF means RGB
  // Pf means monochrome
  // http://www.pauldebevec.com/Research/HDR/PFM/
  pfm_file << "PF" << std::endl;

  pfm_file << _width << " " << _height << std::endl;

  // BYTE ORDER: little endian
  pfm_file << "-1.0" << std::endl;

  for (size_t y = 0; y < _height; y++)
  {
    for (size_t x = 0; x < _width; x++)
    {
      const size_t local_px = (_height - y - 1) * _width + x;
      pfm_file.write(reinterpret_cast<const char *>(_pixels[local_px].ptr()), 3 * sizeof(float));
    }

    const float progress_value = static_cast<float>(100 * y) / static_cast<float>(_height - 1);
    progress(static_cast<int>(progress_value));
  }

  pfm_file.close();

  progress(100);

  return MRF_NO_ERROR;
}

}   // namespace image
}   // namespace mrf
