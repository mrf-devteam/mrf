/**
 * Author: Romain Pacanowski @ institutoptique . fr
 * Copyright CNRS 2020
 **/

/**
 * Convenient header to include all headers of the image module
 **/

#pragma once

#include <mrf_core/image/image.hpp>

//-----------------------------------------------------------------------------
// Tristimulus images
//-----------------------------------------------------------------------------

#include <mrf_core/image/color_image.hpp>
#include <mrf_core/image/exr_color_image.hpp>
#include <mrf_core/image/pfm_color_image.hpp>
#include <mrf_core/image/hdr_color_image.hpp>
#include <mrf_core/image/png_color_image.hpp>

//-----------------------------------------------------------------------------
// Spectral images
//-----------------------------------------------------------------------------

#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/image/artraw_spectral_image.hpp>
#include <mrf_core/image/envi_spectral_image.hpp>
#include <mrf_core/image/exr_spectral_image.hpp>
