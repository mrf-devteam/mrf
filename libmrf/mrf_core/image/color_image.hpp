/**
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *
 * Alban Fichet @ institutoptique.fr
 * April 2020
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/image/image.hpp>
#include <mrf_core/math/vec3.hpp>

#include <string>
#include <fstream>
#include <functional>
#include <vector>

namespace mrf
{
namespace image
{
template<class T>
class MultiChannelImage: public Image
{
public:
  MultiChannelImage(size_t width = 0, size_t height = 0): _pixels(width * height), _width(width), _height(height) {}

  virtual ~MultiChannelImage() {}

  virtual void init(size_t width, size_t height, bool force_reset = false)
  {
    //if already allocated
    if (force_reset)
    {
      for (uint i = 0; i < _width * _height; i++)
      {
        _pixels[i] = T();
      }
    }

    _pixels.resize(width * height);

    _width  = width;
    _height = height;
  }

  virtual T const &getPixel(size_t x, size_t y) const { return _pixels[y * _width + x]; }

  T const &getPixel(size_t i) const { return _pixels[i]; }

  void setPixel(size_t x, size_t y, T const &value) { _pixels[y * _width + x] = value; }

  void setPixel(size_t i, T const &value) { _pixels[i] = value; }

  T const &operator()(size_t x, size_t y) const { return _pixels[y * _width + x]; }

  T &operator()(size_t x, size_t y) { return _pixels[y * _width + x]; }

  T const &operator[](size_t i) const { return _pixels[i]; }

  T &operator[](size_t i) { return _pixels[i]; }

  T const &operator()(size_t i) const { return _pixels[i]; }

  T &operator()(size_t i) { return _pixels[i]; }

  MultiChannelImage<T> &operator*=(float f)
  {
    for (T &v : _pixels)
    {
      v *= f;
    }

    return *this;
  }

  virtual size_t width() const { return _width; }

  virtual size_t height() const { return _height; }

  virtual size_t size() const { return _width * _height; }

  T *      ptr() { return _pixels.data(); }
  const T *ptr() const { return _pixels.data(); }

  //Return the sum of all the pixel,
  //it might be long
  T sum() const
  {
    T t;

    for (T const &v : _pixels)
    {
      t += v;
    }

    return t;
  }

protected:
  std::vector<T> _pixels;
  size_t         _width;
  size_t         _height;
};

template<class T1, class T2>
inline bool operator==(MultiChannelImage<T1> const &img1, MultiChannelImage<T2> const &img2)
{
  //Check Types
  if (!(std::is_same<T1, T2>::value))
  {
    return false;
  }

  //Check Dimensions
  if ((img1.width() != img2.width()) || (img1.height() != img2.height()))
  {
    return false;
  }

  // Now Check All values
  // TODO: Optimize me with OpenMP ?

  for (uint i = 0; i < img1.size(); i++)
  {
    if (img1(i) != img2(i))
    {
      return false;
    }
  }

  return true;
}


class MRF_CORE_EXPORT ColorImage: public MultiChannelImage<mrf::color::Color>
{
public:
  ColorImage(size_t width = 0, size_t height = 0);

  virtual ~ColorImage();

  IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const = 0;

  /*
   * This is the HDR fileformat introduce by Greg Ward see here :
   * https://en.wikipedia.org/wiki/RGBE_image_format
   * http://radsite.lbl.gov/radiance/refer/filefmts.pdf
   **/
  float                          totalPower() const;
  float                          maxValue() const;
  void                           computeStatistics();
  inline mrf::math::Vec3f const &redStatistics() const;
  inline mrf::math::Vec3f const &greenStatistics() const;
  inline mrf::math::Vec3f const &blueStatistics() const;
  inline mrf::math::Vec3f const &lumStatistics() const;
  inline float                   logMeanLuminance() const;

private:
  /*! @brief Red min, max and mean */
  mrf::math::Vec3f _red;
  /*! @brief Green min, max and mean */
  mrf::math::Vec3f _green;
  /*! @brief Blue min, max and mean */
  mrf::math::Vec3f _blue;
  /*! @brief Luminance min, max and mean */
  mrf::math::Vec3f _lum;

  /*! @brief Exponential of the mean of the log of the luminances */
  float _l_exp_log_mean;
  /*! @brief Exponential of the mean of the log of the luminances */
  float _r_exp_log_mean;
  /*! @brief Exponential of the mean of the log of the luminances */
  float _g_exp_log_mean;
  /*! @brief Exponential of the mean of the log of the luminances */
  float _b_exp_log_mean;
  /*! @brief Mean of the log of the luminances */
  float _l_log_mean;
};


#include "color_image.inl"

int  ReadFloat(FILE *fptr, float *n, int swap);
void ReadInt(FILE *fptr, int *n, char buffer[100]);

}   // namespace image
}   // namespace mrf
