/**
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *
 * Romain Pacanowski @ institutoptique.fr
 * March 2020
 * Copyright: CNRS 2020
 *
 * Alban Fichet @ institutoptique.fr
 * April 2020
 * Copyright: CNRS 2020
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/image/image.hpp>
#include <mrf_core/image/color_image.hpp>
#include <mrf_core/data_struct/array2d.hpp>

#include <type_traits>
#include <fstream>
#include <functional>
#include <vector>

namespace mrf
{
namespace image
{
/**
 * @brief A class representing a Spectral image with the same wavelength
 * distribution for each pixel
 *
 * @note : Wavelength are stored as unsigned int number because their units
 * are nanometer
 *
 */
class MRF_CORE_EXPORT UniformSpectralImage: public Image
{
public:
  UniformSpectralImage();

  UniformSpectralImage(
      size_t width,
      size_t height,
      uint   start_wavelength = 360,
      uint   step_wavelength  = 1,
      size_t nb_wavelength    = 471);

  UniformSpectralImage(size_t width, size_t height, const std::vector<uint> &wavelengths);

  UniformSpectralImage(
      size_t                       width,
      size_t                       height,
      std::pair<uint, uint> const &start_end_wavelength = std::pair<uint, uint>(360, 830),
      size_t                       nb_wavelength        = 12);

  virtual ~UniformSpectralImage();

  /**
   * @brief Clear all the data contained in a UniformSpectralImage
   */
  virtual void clear();

  /**
   * @brief Clear all the data contained in a UniformSpectralImage except the
   * std::vector of wavelengths
   */
  virtual void clearConserveWavelengths();

  /**
   * @brief Set the spectral image to zeros.
   */
  virtual void zeros();

  /**
   * @brief Allocate the memory of a UniformSpectralImage
   */
  virtual void allocateMemory(size_t width, size_t height, std::vector<uint> wavelengths);

  /**
   * @brief Add contribution for given wavelength
   *
   * The data pointer shall be at least of size width*heigth and pixel at
   * (x, y) placed at data[y * width + x].
   *
   * The image shall be first initialised with the correct size
   * @see allocateMemory
   *
   * If the wavelength already exists in the image, the values stored in
   * data are added to the wavelength specified.
   *
   * @param data       Pointer to the data to add
   * @param width      With of the image data.
   * @param height     Height of the image data.
   * @param wavelength The wavelength in nanometer corresponding to the
   *                   contribution to add.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  template<typename T = float>
  IMAGE_LOAD_SAVE_FLAGS addValuesFromBuffer(const T *data, size_t width, size_t height, uint wavelength);

  /**
   * @brief Add contribution for given wavelength
   *
   * @param values     std::vector containing the data to add
   * @param width      With of the image data.
   * @param height     Height of the image data.
   * @param wavelength The wavelength in nanometer corresponding to the
   *                   contribution to add.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  template<typename T = float>
  IMAGE_LOAD_SAVE_FLAGS addValuesFromBuffer(std::vector<T> const &values, size_t width, size_t height, uint wavelength)
  {
    return addValuesFromBuffer<T>(values.data(), width, height, wavelength);
  }

  /**
   * @brief Add contribution for given wavelength
   *
   * This method loads an image of supported formats by MRF and uses
   * the red channel for the contribution to add.
   *
   * @param file_path         Path to an image of supported type by MRF.
   * @param wavelength        The wavelength in nanometer corresponding to the
   *                          contribution to add.
   * @param loadLinearWhenLDR Consider the values stored as plain values
   *                          and does not apply inverse sRGB gamma to the
   *                          channels of the image when not an HDR format.
   *                          Default true.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS
  addValuesFromGrayImage(std::string const &file_path, uint wavelength, bool loadLinearWhenLDR = true);

  /**
   * @brief Add contribution for given wavelength
   *
   * This method uses the red channel for the contribution to add.
   *
   * @param image      Image to use for adding contribution.
   * @param wavelength The wavelength in nanometer corresponding to the
   *                   contribution to add.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS addValuesFromGrayImage(ColorImage const &image, uint wavelength);

  /**
   * @brief Add contribution for given wavelengths
   *
   * This method uses the red green and blue channels of the image pointed by
   * file_path for the contribution to add.
   *
   * @param file_path         Path to an image of supported type by MRF.
   * @param wavelengths       The wavelengths in nanometer corresponding to
   *                          the contribution to add for red, gren and blue.
   * @param loadLinearWhenLDR Consider the values stored as plain values
   *                          and does not apply inverse sRGB gamma to the
   *                          channels of the image when not an HDR format.
   *                          Default true.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS addValuesFromColorImage(
      std::string const &                 file_path,
      std::tuple<uint, uint, uint> const &wavelengths,
      bool                                loadLinearWhenLDR = true);

  /**
   * @brief Add contribution for given wavelengths
   *
   * This method uses the red green and blue channels of the image pointed by
   * file_path for the contribution to add.
   *
   * @param file_path  Path to an image of supported type by MRF.
   * @param wavelength The wavelength in nanometer corresponding to the
   *                   contribution to add.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS
  addValuesFromColorImage(ColorImage const &image, std::tuple<uint, uint, uint> const &wavelengths);


  void spectralDownsampling(uint downsampling_factor);

  void generateAccumulativeData();

  /**
   * @brief Direct memory access to the image data
   *
   * To access wavelength at index i, and pixel at position x, y,
   * `image.data()[i](y, x)`
   *
   * @return std::vector<mrf::data_struct::Array2D> const&
   */
  std::vector<mrf::data_struct::Array2D> const &data() const { return _data; }
  std::vector<mrf::data_struct::Array2D> &      data() { return _data; }


  /**
   * @brief Direct memory access to the image cumulative data
   *
   * To access wavelength at index i, and pixel at position x, y,
   * `image.cumulativeData()[i](y, x)`
   *
   * @return std::vector<mrf::data_struct::Array2D> const&
   */
  std::vector<mrf::data_struct::Array2D> const &cumulativeData() const { return _accumulative_data; }
  std::vector<mrf::data_struct::Array2D> &      cumulativeData() { return _accumulative_data; }



  std::vector<uint> const &wavelengths() const { return _wavelengths; }
  std::vector<uint> &      wavelengths() { return _wavelengths; }

  virtual size_t width() const;
  virtual size_t height() const;


  //virtual std::map<std::string, std::string> metadata() const;

  /**
   * Returns width()*height()*nb_wavelengths()
   */
  inline virtual size_t size() const { return width() * height() * wavelengths().size(); }

  /**
   * @brief Get pixel value
   *
   * This method shall be avoided: it is very ineficient.
   * Use data() instead when possible.
   *
   * @param x in 0 .. width - 1
   * @param y in 0 .. height - 1
   * @return value for each wavelength
   */
  virtual std::vector<float> getPixel(size_t x, size_t y) const;

  /**
   * @brief Set pixel value
   *
   * This method shall be avoided: it is very ineficient.
   * Use data() instead when possible.
   *
   * @param x in 0 .. width - 1
   * @param y in 0 .. height - 1
   * @param data value for each wavelength
   */
  virtual void setPixel(size_t x, size_t y, const std::vector<float> &data);

  /**
   * @brief Saves a file
   *
   * @param file_path The path to the file to save.
   * @param progress  A callback to notify the saving progress in
   *                  percents.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const = 0;

  IMAGE_LOAD_SAVE_FLAGS saveAsGrayPngImages(std::string const &folder_path) const;

  /**
   * @brief Saves a given pixel as a spectrum
   *
   * @param x in 0 .. width - 1
   * @param y in 0 .. height - 1
   * @param file_path Path to the spectrum to save
   * @return IMAGE_LOAD_SAVE_FLAGS
   */
  IMAGE_LOAD_SAVE_FLAGS savePixelToFile(size_t x, size_t y, std::string const &file_path) const;

  /**
   * @brief Saves a given pixel as a spectrum
   *
   * @param x in 0 .. width - 1
   * @param y in 0 .. height - 1
   * @param kernel_size average size arround the selected pixel
   * @param file_path Path to the spectrum to save
   * @return IMAGE_LOAD_SAVE_FLAGS
   */
  IMAGE_LOAD_SAVE_FLAGS savePixelToFile(size_t x, size_t y, uint kernel_size, std::string const &file_path) const;

  /**
   * Return memory occupancy in MBytes
   */
  double memoryOccupancy() const;

  /**
   * Return expected memory occupancy of loading an image in MBytes
   */
  static double expectedMemoryOccupancy(int image_width, int image_height, int image_bands);

protected:
  /**
   * store pixels values, one array 2D foreach wavelength
   */
  std::vector<mrf::data_struct::Array2D> _data;
  std::vector<mrf::data_struct::Array2D> _accumulative_data;

  /**
   * store wavelength of each array2D
   */
  std::vector<uint> _wavelengths;
  bool              _accumulative_data_computed;
};


inline bool operator==(UniformSpectralImage const &img1, UniformSpectralImage const &img2)
{
  // Check dimensions
  if ((img1.wavelengths().size() != img2.wavelengths().size()) || (img1.width() != img2.width())
      || (img1.height() != img2.height()))
  {
    // std::cout << " wrong dimensions or wavelengths size in operator== UniformSpectralImage" << std::endl;
    // std::cout << " img1.wavelengths().size() vs. " << img1.wavelengths().size() << "  " << img2.wavelengths().size()
    //           <<  " width vs. " << img1.width() << "  "  << img2.width()
    //           << "  height vs. " << img1.height() << "  "  << img2.height()
    //           << std::endl;
    return false;
  }

  if (!std::equal(img1.wavelengths().begin(), img1.wavelengths().end(), img2.wavelengths().begin()))
  {
    //std::cout << " wrong Wavelengths operator== UniformSpectralImage" << std::endl;
    return false;
  }

  // Check Values
  std::vector<mrf::data_struct::Array2D> const &data1 = img1.data();
  std::vector<mrf::data_struct::Array2D> const &data2 = img2.data();

  for (size_t i = 0; i < img1.data().size(); i++)
  {
    mrf::data_struct::Array2D const &d1 = data1[i];
    mrf::data_struct::Array2D const &d2 = data2[i];

    if ((d1 != d2))
    {
      // std::cout << " i = " << i << std::endl;
      // std::cout << " wrong PIXEL VALUES operator== UniformSpectralImage" << std::endl;
      return false;
    }
  }

  return true;
}


inline bool operator!=(UniformSpectralImage const &img1, UniformSpectralImage const &img2)
{
  return !(img1 == img2);
}


template<typename T>
IMAGE_LOAD_SAVE_FLAGS
UniformSpectralImage::addValuesFromBuffer(const T *data, size_t width, size_t height, uint wavelength)
{
  // Check if contains an array and avoid adding arrays of different size
  if (_data.size() > 0 && (width != this->width() || height != this->height()))
  {
    return MRF_ERROR_WRONG_IMAGE_SIZE;
  }

  // Check if wavelength is already in the image
  auto res = std::lower_bound(_wavelengths.begin(), _wavelengths.end(), wavelength);

  data_struct::Array2D *px;

  if (res != _wavelengths.end() && *res == wavelength)
  {
    // TODO: optim Eigen
    px = &(_data[res - _wavelengths.begin()]);
  }
  else
  {
    // Keep the vector sorted
    auto i   = _wavelengths.insert(res, wavelength);
    auto idx = i - _wavelengths.begin();

    px = &*_data.insert(_data.begin() + idx, data_struct::Array2D(width, height));
  }

  //TODO: RP Split this in three distrincts for-loop to improve optimisations (if outside)
  #pragma omp parallel for schedule(static)
  for (int i = 0; i < static_cast<int>(width * height); i++)
  {
    if (std::is_integral<T>::value)
    {
      if (std::is_signed<T>::value)
      {
        (*px)(i) += static_cast<float>(data[i] + static_cast<float>(std::numeric_limits<T>::max()))
                    / (2.f * static_cast<float>(std::numeric_limits<T>::max()));
      }
      else
      {
        (*px)(i) += static_cast<float>(data[i]) / static_cast<float>(std::numeric_limits<T>::max());
      }
    }
    else
    {
      (*px)(i) += static_cast<float>(data[i]);
    }
  }

  _accumulative_data_computed = false;

  return MRF_NO_ERROR;
}


}   // namespace image
}   // namespace mrf
