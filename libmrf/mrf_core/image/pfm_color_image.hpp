/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core/image/color_image.hpp>

namespace mrf
{
namespace image
{
class MRF_CORE_EXPORT PFMColorImage: public ColorImage
{
public:
  PFMColorImage(size_t width = 0, size_t height = 0);

  PFMColorImage(
      const std::string &      file_path,
      std::function<void(int)> progress = [](int) {
      });

  PFMColorImage(ColorImage const &img);

  virtual ~PFMColorImage();

  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const override;
};
}   // namespace image
}   // namespace mrf
