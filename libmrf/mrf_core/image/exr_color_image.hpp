/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 * Modifications
 *  Romain Pacanowski @ institutoptique.fr
 *   - added support for metadata retrieval
 **/

#pragma once

#include <mrf_core/image/color_image.hpp>
#include <tinyexr/tinyexr.h>

namespace mrf
{
namespace image
{
class MRF_CORE_EXPORT EXRColorImage: public ColorImage
{
public:
  EXRColorImage(size_t width = 0, size_t height = 0);

  EXRColorImage(
      const std::string &      file_path,
      std::function<void(int)> progress = [](int) {
      });

  EXRColorImage(ColorImage const &img);

  virtual ~EXRColorImage();

  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const override;

protected:
  void updateMetadata(EXRHeader const &exr_header);
};
}   // namespace image
}   // namespace mrf
