/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/image/uniform_spectral_image.hpp>

#include <functional>

namespace mrf
{
namespace image
{
/**
 * @class EnviSpectralImage
 *
 */
class MRF_CORE_EXPORT EnviSpectralImage: public UniformSpectralImage
{
public:
  /**
   * Order for saving the data in the .raw file
   */
  enum InterleavedMode
  {
    IM_BIL = 0,
    IM_BIP,
    IM_BSQ,
    IM_UNDEF
  };

  /**
   * Type of data stored in the .raw file
   */
  enum DataType
  {
    DT_UINT_8      = 1,
    DT_INT_16      = 2,
    DT_INT_32      = 3,
    DT_FLOAT       = 4,
    DT_DOUBLE      = 5,
    DT_CPLX_FLOAT  = 6,
    DT_CPLX_DOUBLE = 9,
    DT_UINT_16     = 12,
    DT_UINT_32     = 13,
    DT_INT_64      = 14,
    DT_UINT_64     = 15,
    DT_UNDEF
  };

  /**
   * @class Header
   * Store information about the specific attributes of an ENVI file
   * contained in the header part (.hdr)
   */
  class Header
  {
  public:
    Header(
        size_t             _width            = 0,
        size_t             _height           = 0,
        size_t             _bands            = 0,
        InterleavedMode    _interleaved_mode = IM_BSQ,
        std::string const &_comments         = "",
        DataType           _data_type        = DT_FLOAT)
      : width(_width)
      , height(_height)
      , bands(_bands)
      , interleave_mode(_interleaved_mode)
      , comments(_comments)
      , data_type(_data_type)
    {}

    Header(Header const &cpy)
      : width(cpy.width)
      , height(cpy.height)
      , bands(cpy.bands)
      , interleave_mode(cpy.interleave_mode)
      , comments(cpy.comments)
      , data_type(cpy.data_type)
    {}

    size_t          width;
    size_t          height;
    size_t          bands;
    InterleavedMode interleave_mode;
    std::string     comments;
    DataType        data_type;
  };

  /**
   * @brief Creates a new empty EnviSpectralImage
   *
   * @param width            Image width.
   * @param height           Image height.
   * @param start_wavelength Start wavelength sampled.
   * @param step_wavelength  Sampling step.
   * @param nb_wavelength    Number of wavelength sampled between
   *                         `start_wavelength` and `stop_wavelength`.
   */
  EnviSpectralImage(
      size_t width            = 0,
      size_t height           = 0,
      uint   start_wavelength = 360,
      uint   step_wavelength  = 1,
      size_t nb_wavelength    = 12);

  EnviSpectralImage(size_t width, size_t height, const std::vector<uint> &wavelengths);

  /**
   * @brief Creates a new EnviSpectralImage from a stored file
   *
   * We assume the header file and the image data share the same name
   * exept for the extension: <name>.hdr and <name>.raw
   *
   * @param file_path Either a .raw or a .hdr file, the second file will
   *                  be used using the same filename with the other
   *                  extension.
   * @param progress  A callback to notify the loading progress in
   *                  percents.
   * @throws IMAGE_LOAD_SAVE_FLAGS if unsucessfull
   */
  EnviSpectralImage(
      const std::string &      file_path,
      std::function<void(int)> progress = [](int) {
      });

  /**
   * @brief Creates a new EnviSpectralImage from a pair of stored files
   *
   * Here, the header and the data file names can differ.
   *
   * @param file_path_header The header (.hdr) file.
   * @param file_path_data   The data (.raw) file.
   * @param progress         A callback to notify the loading
   *                         progress in percents.
   *
   * @throws IMAGE_LOAD_SAVE_FLAGS if unsucessfull
   */
  EnviSpectralImage(
      const std::string &      file_path_header,
      const std::string &      file_path_data,
      std::function<void(int)> progress = [](int) {
      });

  /**
   * @brief Creates a new ENVI image from another Uniform Spectral
   * Image
   *
   * @param other Other UniformSpectralImage image object.
   */
  EnviSpectralImage(const UniformSpectralImage &other);

  virtual ~EnviSpectralImage();

  /**
   * @brief Get the pair of filename for a given ENVI file
   *
   * ENVI format is using two files: one header (.hdr) and one file
   * containing the image data (.raw).
   * This method gives the two corresponding filenames given one filename
   *
   * @param file_path  One of the filename (either .hdr or .raw).
   * @param names      Result:
   *                      - name.first is the header (.hdr).
   *                      - name.second is the data (.raw).
   *
   * @returns true if the provided file_path is either ending with .hdr
   *          or .raw. false otherwise.
   */
  static bool getFilenames(std::string const &file_path, std::pair<std::string, std::string> &names);

  //---------------------------------------------------------------------
  // Save routines
  //---------------------------------------------------------------------

  /**
   * @brief Saves in ENVI file format
   *
   * In this method, we assume the header file and the image data have the
   * same name exept for the extension: <name>.hdr and <name>.raw
   *
   * @param file_path Either a .raw or a .hdr file, the second file will
   *                  be used using the same filename with the other
   *                  extension.
   * @param progress  A callback to notify the saving progress in
   *                  percents.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const override;

  /**
   * @brief Saves in ENVI file format
   *
   * @param file_path_header The header (.hdr) file.
   * @param file_path_data   The data (.raw) file.
   * @param progress  A callback to notify the saving progress in
   *                  percents.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &file_path_header,
      const std::string &file_path_data,
      const std::string &additional_comments = "",

      std::function<void(int)> progress = [](int) {
      }) const;

  //---------------------------------------------------------------------
  // Getter / setters metadata
  //---------------------------------------------------------------------

  /**
   * @brief Sets data interleaved
   *
   * Sets the order for saving the data in the .raw file.
   *
   * @param mode Mode to set.
   */
  void setInterleavedMode(InterleavedMode mode);

  /**
   * @brief Gets data interleaved mode
   */
  InterleavedMode interleavedMode() const { return _header.interleave_mode; }

  /**
   * @brief Sets comments
   *
   * Sets comments that will be saved in the header of an ENVI file.
   *
   * @param comments Comments to set.
   */
  void setComments(std::string const &comments);

  /**
   * @brief Gets file comments
   */
  std::string comments() const { return _header.comments; }

  /**
   * @brief Sets data type
   *
   * Sets the data type used for saving the data in the .raw file.
   *
   * @param mode Data type to set.
   */
  void setDataType(DataType dt);

  /**
   * @brief Gets data type
   */
  DataType dataType() const { return _header.data_type; }

  /**
   * @brief Sets data type
   *
   * Sets the data type used for saving the data in the .raw file.
   * This method is based on the C++ types.
   */
  template<typename T>
  void setDataType();

  // /**
  //  * @brief
  //  */
  // virtual std::map<std::string, std::string> metadata() const override;

private:
  //---------------------------------------------------------------------
  // Read routines
  //---------------------------------------------------------------------

  /**
   * @brief Loads in ENVI file format
   *
   * In this method, we assume the header file and the image data have the
   * same name exept for the extension: <name>.hdr and <name>.raw
   *
   * @param file_path Either a .raw or a .hdr file, the second file will
   *                  be used using the same filename with the other
   *                  extension.
   * @param progress  A callback to notify the loading progress in
   *                  percents.
   *
   * @return The load state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS load(
      const std::string &      file_path,
      std::function<void(int)> progress = [](int) {
      });

  /**
   * @brief Loads in ENVI file format
   *
   * @param file_path_header The header (.hdr) file.
   * @param file_path_data   The data (.raw) file.
   * @param progress  A callback to notify the loading progress in
   *                  percents.
   *
   * @return The load state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS load(
      const std::string &      file_path_header,
      const std::string &      file_path_image_data,
      std::function<void(int)> progress = [](int) {
      });

  /**
   * @brief Reads the header from an input stream.
   *
   * @param is Input stream to the ENVI file header.
   * @returns The load state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS readHeader(std::istream &is);

  /**
   * @brief Reads data from an output stream.
   *
   * @param is        Input stream to the ENVI file data.
   * @param progress  A callback to notify the loading progress in
   *                  percents.
   *
   * @returns The load state. MRF_NO_ERROR if successfull.
   */
  template<typename T>
  IMAGE_LOAD_SAVE_FLAGS readImageData(
      std::istream &           is,
      std::function<void(int)> progress = [](int) {
      });

  //---------------------------------------------------------------------
  // Save routines
  //---------------------------------------------------------------------

  /**
   * @brief Saves the header to an output stream.
   *
   * @param os Ouptut stream to the ENVI file header.
   * @returns The save state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS writeHeader(std::ostream &os, std::string additional_comments = "") const;

  /**
   * @brief Saves data to an output stream.
   *
   * @param os        Output stream to the ENVI file data.
   * @param progress  A callback to notify the saving progress in
   *                  percents.
   *
   * @returns The save state. MRF_NO_ERROR if successfull.
   */
  template<typename T>
  IMAGE_LOAD_SAVE_FLAGS writeImageData(
      std::ostream &           os,
      std::function<void(int)> progress = [](int) {
      }) const;

  /**
   * @brief Updates internally the metdata associated with this image
   */
  void updateMetadata();


  // FIELDS
  Header _header;
};
}   // namespace image
}   // namespace mrf
