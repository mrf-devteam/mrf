﻿/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * September 2020
 * Copyright: CNRS 2020
 *
 **/

#pragma once

#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/data_struct/array2d.hpp>
#include <mrf_core/radiometry/spectrum_type.hpp>

namespace mrf
{
namespace image
{
class MRF_CORE_EXPORT PolarisedSpectralImage: public UniformSpectralImage
{
public:
  PolarisedSpectralImage(radiometry::SpectrumType spectrumType = radiometry::SPECTRUM_EMISSIVE);

  PolarisedSpectralImage(
      size_t                   width,
      size_t                   height,
      uint                     start_wavelength = 360,
      uint                     step_wavelength  = 1,
      size_t                   nb_wavelength    = 471,
      radiometry::SpectrumType spectrumType     = radiometry::SPECTRUM_EMISSIVE);

  PolarisedSpectralImage(
      size_t                       width,
      size_t                       height,
      std::pair<uint, uint> const &start_end_wavelength = std::pair<uint, uint>(360, 830),
      size_t                       nb_wavelength        = 12,
      radiometry::SpectrumType     spectrumType         = radiometry::SPECTRUM_EMISSIVE);

  PolarisedSpectralImage(
      size_t                   width,
      size_t                   height,
      const std::vector<uint> &wavelengths,
      radiometry::SpectrumType spectrumType = radiometry::SPECTRUM_EMISSIVE);

  PolarisedSpectralImage(
      const UniformSpectralImage &uniform_spectral_image,
      radiometry::SpectrumType    spectrumType = radiometry::SPECTRUM_EMISSIVE);

  virtual ~PolarisedSpectralImage();

  /**
   * @brief Clear all the data except the std::vector of wavelengths
   */
  virtual void clearConserveWavelengths();

  /**
   * @brief Set the spectral image to zeros.
   */
  virtual void zeros();

  /**
   * @brief Allocate the memory
   */
  virtual void allocateMemory(size_t width, size_t height, std::vector<uint> wavelengths);

  /**
   * Retuns the spectrum type stored in each of the pixels
   */
  radiometry::SpectrumType getSpectrumType() const { return _spectrumType; }

  bool emissive() const { return radiometry::isEmissive(_spectrumType); }
  bool reflective() const { return radiometry::isReflective(_spectrumType); }
  bool polarised() const { return radiometry::isPolarised(_spectrumType); }
  bool bispectral() const { return radiometry::isBispectral(_spectrumType); }

  size_t nSpectralBands() const;
  size_t nStokesComponents() const;

  //    /**
  //     * Return memory occupancy in MBytes
  //     */
  //    double memoryOccupancy() const;

  //    /**
  //     * Return expected memory occupancy of loading an image in MBytes
  //     */
  //    static double expectedMemoryOccupancy(int image_width, int image_height, int image_bands);

  std::vector<mrf::data_struct::Array2D> &      emissiveFramebuffer(size_t stokeComponent = 0);
  const std::vector<mrf::data_struct::Array2D> &emissiveFramebuffer(size_t stokeComponent = 0) const;

  std::vector<mrf::data_struct::Array2D> &      reflectiveFramebuffer();
  const std::vector<mrf::data_struct::Array2D> &reflectiveFramebuffer() const;

  const std::vector<mrf::data_struct::Array2D> &reradiation() const;

  mrf::data_struct::Array2D &reradiationFrameBuffer(size_t wl_idx_from, size_t wl_idx_to);

  const mrf::data_struct::Array2D &reradiationFrameBuffer(size_t wl_idx_from, size_t wl_idx_to) const;

protected:
  size_t reradiationSize() const;

  static size_t idxFromWavelengthIdx(size_t wlFrom_idx, size_t wlTo_idx);

  static void wavelengthsIdxFromIdx(size_t rerad_idx, size_t &wlFrom_idx, size_t &wlTo_idx);

  // Spectrum type stored by the image
  mrf::radiometry::SpectrumType _spectrumType;

private:
  // We can have up to 8 pixel buffers:
  // - 0 for not polarised image
  // - 3 for emissive polarised images (S1, S2, S3)
  // UniformSpectralImage::_data stores the S0 or T.

  // Stores the Stokes (S1-S3) components in case of an emissive polarised image
  std::vector<std::vector<mrf::data_struct::Array2D>> _emissivePolarisedData;

  // Stores reflective part in case of both reflective and emissve image
  std::vector<mrf::data_struct::Array2D> _extraUnpolarisedData;

  // Stores the reradiation
  std::vector<mrf::data_struct::Array2D> _reradiation;
};
}   // namespace image
}   // namespace mrf
