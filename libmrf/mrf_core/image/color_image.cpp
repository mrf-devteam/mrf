/**
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *
 * Alban Fichet @ institutoptique.fr
 * April 2020
 * Copyright CNRS 2020
 *
 **/

#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/color_image.hpp>
#include <mrf_core/io/exr_io.hpp>

#include <lodepng/lodepng.h>
#include <rgbe/rgbe.h>
#include <tinyexr/tinyexr.h>

#include <cstdio>
#include <iostream>
#include <fstream>

using mrf::color::Color;
using mrf::math::Vec3f;
using std::string;


namespace mrf
{
namespace image
{
ColorImage::ColorImage(size_t width, size_t height)
  : MultiChannelImage(width, height)
  , _red(std::numeric_limits<float>::max(), std::numeric_limits<float>::min(), 0.f)
  , _green(std::numeric_limits<float>::max(), std::numeric_limits<float>::min(), 0.f)
  , _blue(std::numeric_limits<float>::max(), std::numeric_limits<float>::min(), 0.f)
  , _lum(std::numeric_limits<float>::max(), std::numeric_limits<float>::min(), 0.f)
  , _l_exp_log_mean(0.f)
  , _r_exp_log_mean(0.f)
  , _g_exp_log_mean(0.f)
  , _b_exp_log_mean(0.f)
  , _l_log_mean(0.f)
{}

ColorImage::~ColorImage() {}

#ifdef _MSC_VER
#  pragma warning(default : 4996)
#endif

float ColorImage::totalPower() const
{
  Color sum_color = sum();
  return sum_color.luminance();
}

float ColorImage::maxValue() const
{
  if (_pixels.empty()) return 0.f;

  float max_value = _pixels[0].r();

  for (uint i = 0; i < _width * _height; i++)
  {
    max_value = std::max(_pixels[i].r(), max_value);
    max_value = std::max(_pixels[i].g(), max_value);
    max_value = std::max(_pixels[i].b(), max_value);
  }
  return max_value;
}

void ColorImage::computeStatistics()
{
  _red   = Vec3f(std::numeric_limits<float>::max(), std::numeric_limits<float>::min(), 0.f);
  _green = Vec3f(std::numeric_limits<float>::max(), std::numeric_limits<float>::min(), 0.f);
  _blue  = Vec3f(std::numeric_limits<float>::max(), std::numeric_limits<float>::min(), 0.f);
  _lum   = Vec3f(std::numeric_limits<float>::max(), std::numeric_limits<float>::min(), 0.f);

  _l_exp_log_mean = 0.f;
  _r_exp_log_mean = 0.f;
  _g_exp_log_mean = 0.f;
  _b_exp_log_mean = 0.f;
  _l_log_mean     = 0.f;

  for (unsigned int pixel_counter = 0; pixel_counter < _height * _width; pixel_counter++)
  {
    auto  pixel = _pixels[pixel_counter];
    float lum   = pixel.luminance();

    if (lum < EPSILON)
    {
      lum = EPSILON;
    }

    _red[0]   = std::min(_red[0], pixel[0]);
    _green[0] = std::min(_green[0], pixel[1]);
    _blue[0]  = std::min(_blue[0], pixel[2]);
    _lum[0]   = std::min(_lum[0], lum);

    _red[1]   = std::max(_red[1], pixel[0]);
    _green[1] = std::max(_green[1], pixel[1]);
    _blue[1]  = std::max(_blue[1], pixel[2]);
    _lum[1]   = std::max(_lum[1], lum);

    _lum[2] += lum;
    _red[2] += pixel[0];
    _green[2] += pixel[1];
    _blue[2] += pixel[2];

    _l_exp_log_mean += log(std::max(EPSILON, lum));
    _r_exp_log_mean += log(std::max(EPSILON, pixel[0]));
    _g_exp_log_mean += log(std::max(EPSILON, pixel[1]));
    _b_exp_log_mean += log(std::max(EPSILON, pixel[2]));
  }

  float n = static_cast<float>(_height * _width);

  _red[2] /= n;
  _green[2] /= n;
  _blue[2] /= n;
  _lum[2] /= n;

  _l_log_mean = _l_exp_log_mean / n;

  _l_exp_log_mean = exp(_l_log_mean);

  _r_exp_log_mean = exp(_r_exp_log_mean / n);
  _g_exp_log_mean = exp(_g_exp_log_mean / n);
  _b_exp_log_mean = exp(_b_exp_log_mean / n);
}


}   // namespace image
}   // namespace mrf
