/*
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/mat3.hpp>
#include <mrf_core/math/vec3.hpp>

#include <functional>

// Other data

namespace mrf
{
namespace color
{
enum MRF_COLOR_SPACE
{
  SRGB = 0,
  ADOBE_RGB,
  WIDE_GAMUT_RGB,
  CIE_RGB,
  CIE_XYZ
};

enum MRF_WHITE_POINT
{
  D50,
  D65,
  E
};

enum MRF_ERROR_COLOR
{
  INCORRECT_COLORSPACE
};


MRF_DEVICE_CONSTANT static float LUMINANCE_COEFF[3] = {0.299f, 0.587f, 0.114f};

inline math::Mat4f const &getRGBtoXYZMat(MRF_COLOR_SPACE colorSpace = SRGB, MRF_WHITE_POINT whitePoint = D65);

inline math::Mat4f const &getXYZtoRGBMat(MRF_COLOR_SPACE colorSpace = SRGB, MRF_WHITE_POINT whitePoint = D65);

// Source http://www.brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html

static math::Mat4f const SRGB_D65_TO_XYZ_MATRIX
    = {0.4124564f,
       0.3575761f,
       0.1804375f,
       0.f,
       0.2126729f,
       0.7151522f,
       0.0721750f,
       0.f,
       0.0193339f,
       0.1191920f,
       0.9503041f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const XYZ_TO_SRGB_D65_MATRIX
    = {3.2404542f,
       -1.5371385f,
       -0.4985314f,
       0.f,
       -0.9692660f,
       1.8760108f,
       0.0415560f,
       0.f,
       0.0556434f,
       -0.2040259f,
       1.0572252f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const SRGB_D50_TO_XYZ_MATRIX
    = {0.4360747f,
       0.3850649f,
       0.1430804f,
       0.f,
       0.2225045f,
       0.7168786f,
       0.0606169f,
       0.f,
       0.0139322f,
       0.0971045f,
       0.7141733f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const XYZ_TO_SRGB_D50_MATRIX
    = {3.1338561f,
       -1.6168667f,
       -0.4906146f,
       0.f,
       -0.9787684f,
       1.9161415f,
       0.0334540f,
       0.f,
       0.0719453f,
       -0.2289914f,
       1.4052427f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const ADOBE_RGB_1998_D65_TO_XYZ_MATRIX
    = {0.5767309f,
       0.1855540f,
       0.1881852f,
       0.f,
       0.2973769f,
       0.6273491f,
       0.0752741f,
       0.f,
       0.0270343f,
       0.0706872f,
       0.9911085f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const XYZ_TO_ADOBE_RGB_1998_D65_MATRIX
    = {2.0413690f,
       -0.5649464f,
       -0.3446944f,
       0.f,
       -0.9692660f,
       1.8760108f,
       0.0415560f,
       0.f,
       0.0134474f,
       -0.1183897f,
       1.0154096f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const ADOBE_RGB_1998_D50_TO_XYZ_MATRIX
    = {0.6097559f,
       0.2052401f,
       0.1492240f,
       0.f,
       0.3111242f,
       0.6256560f,
       0.0632197f,
       0.f,
       0.0194811f,
       0.0608902f,
       0.7448387f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const XYZ_TO_ADOBE_RGB_1998_D50_MATRIX
    = {1.9624274f,
       -0.6105343f,
       -0.3413404f,
       0.f,
       -0.9787684f,
       1.9161415f,
       0.0334540f,
       0.f,
       0.0286869f,
       -0.1406752f,
       1.3487655f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const WIDE_GAMUT_RGB_D50_TO_XYZ_MATRIX
    = {0.7161046f,
       0.1009296f,
       0.1471858f,
       0.f,
       0.2581874f,
       0.7249378f,
       0.0168748f,
       0.f,
       0.0000000f,
       0.0517813f,
       0.7734287f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const XYZ_TO_WIDE_GAMUT_RGB_D50_MATRIX
    = {1.4628067f,
       -0.1840623f,
       -0.2743606f,
       0.f,
       -0.5217933f,
       1.4472381f,
       0.0677227f,
       0.f,
       0.0349342f,
       -0.0968930f,
       1.2884099f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const CIE_RGB_D50_TO_XYZ_MATRIX
    = {0.4868870f,
       0.3062984f,
       0.1710347f,
       0.f,
       0.1746583f,
       0.8247541f,
       0.0005877f,
       0.f,
       -0.0012563f,
       0.0169832f,
       0.8094831f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const XYZ_TO_CIE_RGB_D50_MATRIX
    = {2.3638081f,
       -0.8676030f,
       -0.4988161f,
       0.f,
       -0.5005940f,
       1.3962369f,
       0.1047562f,
       0.f,
       0.0141712f,
       -0.0306400f,
       1.2323842f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const CIE_RGB_E_TO_XYZ_MATRIX
    = {0.4887180f,
       0.3106803f,
       0.2006017f,
       0.f,
       0.1762044f,
       0.8129847f,
       0.0108109f,
       0.f,
       0.0000000f,
       0.0102048f,
       0.9897952f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static math::Mat4f const XYZ_TO_CIE_RGB_E_MATRIX
    = {2.3706743f,
       -0.9000405f,
       -0.4706338f,
       0.f,
       -0.5138850f,
       1.4253036f,
       0.0885814f,
       0.f,
       0.0052982f,
       -0.0146949f,
       1.0093968f,
       0.f,
       0.f,
       0.f,
       0.f,
       1.f};

static const float ADOBE_RGB_GAMMA = 2.19921875f;


mrf::math::Mat4f const &getRGBtoXYZMat(MRF_COLOR_SPACE colorSpace, MRF_WHITE_POINT whitePoint)
{
  switch (whitePoint)
  {
    case D50:
      switch (colorSpace)
      {
        case ADOBE_RGB:
          return ADOBE_RGB_1998_D50_TO_XYZ_MATRIX;
        case SRGB:
          return SRGB_D50_TO_XYZ_MATRIX;
        case CIE_RGB:
          return CIE_RGB_D50_TO_XYZ_MATRIX;
        case WIDE_GAMUT_RGB:
          return WIDE_GAMUT_RGB_D50_TO_XYZ_MATRIX;
        default:
          throw INCORRECT_COLORSPACE;
      }
      break;

    case D65:
      switch (colorSpace)
      {
        case ADOBE_RGB:
          return ADOBE_RGB_1998_D65_TO_XYZ_MATRIX;
        case SRGB:
          return SRGB_D65_TO_XYZ_MATRIX;
        default:
          throw INCORRECT_COLORSPACE;
      }
      break;
    case E:
      switch (colorSpace)
      {
        case CIE_RGB:
          return CIE_RGB_E_TO_XYZ_MATRIX;
        default:
          throw INCORRECT_COLORSPACE;
      }

    default:
      throw INCORRECT_COLORSPACE;
  }
}


math::Mat4f const &getXYZtoRGBMat(MRF_COLOR_SPACE colorSpace, MRF_WHITE_POINT whitePoint)
{
  switch (whitePoint)
  {
    case D50:
      switch (colorSpace)
      {
        case ADOBE_RGB:
          return XYZ_TO_ADOBE_RGB_1998_D50_MATRIX;
        case SRGB:
          return XYZ_TO_SRGB_D50_MATRIX;
        case CIE_RGB:
          return XYZ_TO_CIE_RGB_D50_MATRIX;
        case WIDE_GAMUT_RGB:
          return XYZ_TO_WIDE_GAMUT_RGB_D50_MATRIX;
        default:
          throw INCORRECT_COLORSPACE;
      }
      break;

    case D65:
      switch (colorSpace)
      {
        case ADOBE_RGB:
          return XYZ_TO_ADOBE_RGB_1998_D65_MATRIX;
        case SRGB:
          return XYZ_TO_SRGB_D65_MATRIX;
        default:
          throw INCORRECT_COLORSPACE;
      }
      break;

    case E:
      switch (colorSpace)
      {
        case CIE_RGB:
          return XYZ_TO_CIE_RGB_E_MATRIX;
        default:
          throw INCORRECT_COLORSPACE;
      }

    default:
      throw INCORRECT_COLORSPACE;
  }
}


}   // namespace color
}   // namespace mrf