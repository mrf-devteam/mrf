/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 *
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/materials/umat.hpp>
#include <mrf_core/geometry/local_frame.hpp>

#include <string>
#include <complex>


namespace mrf
{
namespace materials
{
/**
 * @brief  Generic Interface for BSDF. BSDF can either reflect or refract the light and they never emit it
 *
 *
 */
class MRF_CORE_EXPORT BSDF: public UMat
{
public:
  BSDF(std::string const &name);
  BSDF(std::string const &name, std::string const &type);
  virtual ~BSDF();

  //-------------------------------------------------------------------------
  // Partial Implementation of UMat's methods
  //-------------------------------------------------------------------------

  //The default behaviour is to return false and to assert(0)
  virtual bool  diffuseComponent() const;
  virtual float diffuseAlbedo() const;

  virtual float specularAlbedo() const;
  virtual bool  specularComponent() const;

  virtual float nonDiffuseAlbedo() const;
  virtual bool  nonDiffuseComponent() const;


  virtual float
  diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual float
  specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual float
  nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  //There is no spatial dependencies for BRDF
  virtual float diffuseAlbedo(mrf::geom::LocalFrame const &) const;
  virtual float specularAlbedo(mrf::geom::LocalFrame const &) const;

  // Emissivity Part which returns zero for all BSDF
  virtual bool           isEmissive() const;
  virtual RADIANCE_TYPE  radianceEmitted(mrf::math::Vec3f const &emit_in_dir, mrf::geom::LocalFrame const &lf) const;
  virtual RADIOSITY_TYPE radiosityEmitted() const;
  virtual RADIOSITY_TYPE radiosityEmitted(mrf::geom::LocalFrame const &) const;

  virtual RADIOSITY_TYPE
  particleEmission(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2, mrf::math::Vec3f &direction)
      const;


  //-------------------------------------------------------------------------
  // Partial Implementation of UMat's methods Regarding Transmission
  //-------------------------------------------------------------------------

  // By default a BSDF does transmit light but the exact quantities
  // are only known from the extended
  // Default Behaviour : assert(0) and return 1.0f if assertions are disabled
  virtual float transmissionAlbedo() const;
  virtual float transmissionAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  transmissionComponent() const;
  virtual float
  transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;


  //-------------------------------------------------------------------------
  // Partial Implementation of BRDF Methods
  //-------------------------------------------------------------------------
  virtual float
  brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;
};




}   // namespace materials
}   // namespace mrf