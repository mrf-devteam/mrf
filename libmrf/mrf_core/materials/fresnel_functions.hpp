/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/ior.hpp>

#include <complex>

namespace mrf
{
namespace materials
{
/**
 * @brief      Compute the Fresnel coefficient from real indexes of refraction
 *
 * @param      eta          Real IOR
 * @param[in]  cos_theta_d  cosine of difference vector (= view dot half_vector)
 *
 * @return     The Fresnel Coefficient
 */
inline float fresnel_dielectric_dielectric(float const &eta, double const cos_theta_d)
{
#ifdef _MSC_VER
#  pragma warning(disable : 4244)
#endif
  float const n2 = eta * eta;

  float const cos_theta(cos_theta_d);
  float const c2(cos_theta_d * cos_theta_d);

  float const s2 = 1.0 - c2;

  float const n2_sin2_theta = n2 - s2;
  if (n2_sin2_theta <= 0) return 1.0f;   // total internal reflection

  float const n2_cos_theta       = n2 * cos_theta;
  float const sqrt_n2_sin2_theta = sqrt(n2_sin2_theta);

  float const Fs = (cos_theta - sqrt_n2_sin2_theta) / (cos_theta + sqrt_n2_sin2_theta);
  float const Fp = (sqrt_n2_sin2_theta - n2_cos_theta) / (n2_cos_theta + sqrt_n2_sin2_theta);

  return (Fs * Fs + Fp * Fp) * 0.5;

#ifdef _MSC_VER
#  pragma warning(default : 4244)
#endif
}

/**
 * @brief      Compute the Fresnel coefficient from a complex index of refraction
 *
 * @param      a_eta        Real part of the IOR
 * @param      a_k          Imaginary part of the IOR
 * @param[in]  cos_theta_d  cosine of difference vector (= view dot half_vector)
 *
 * @return     The Fresnel Coefficient
 */
inline double fresnel_dielectric_conductor(float const &a_eta, float const &a_k, double const cos_theta_d)
{   // TODO : replace by S Lagarde's function ?
  //Two Real Numbers
  std::complex<double> const c2(cos_theta_d * cos_theta_d, 0.0);
  std::complex<double> const s2 = std::complex<double>(1.0, 0.0) - c2;

  std::complex<double> const eta(a_eta, a_k);
  std::complex<double> const n2 = eta * eta;

  std::complex<double> const n2_sin2_theta = n2 - s2;
  std::complex<double> const n2_cos_theta  = n2 * cos_theta_d;

  std::complex<double> const sqrt_n2_sin2_theta = std::sqrt(n2_sin2_theta);

  std::complex<double> const Fs = (-sqrt_n2_sin2_theta + cos_theta_d) / (sqrt_n2_sin2_theta + cos_theta_d);
  std::complex<double> const Fp = (-sqrt_n2_sin2_theta + n2_cos_theta) / (sqrt_n2_sin2_theta + n2_cos_theta);

  return 0.5 * (std::norm(Fs) + std::norm(Fp));
}

/**
 * @brief      Compute the Fresnel coefficient from two complex indexes of refraction
 *
 * @param      eta_1        Complex IOR of the first medium (up)
 * @param      eta_2           Complex IOR of the second medium (down)
 * @param[in]  cos_theta_d  cosine of difference vector (= view dot half_vector)
 *
 * @return     The Fresnel Coefficient
 */
inline float fresnel_conductor_conductor(
    std::complex<float> const &eta_1,
    std::complex<float> const &eta_2,
    double const               cos_theta_i)
{
  // TODO : optimize the code !
  const float c1 = static_cast<float>(cos_theta_i);
  assert(c1 >= 0.0f && c1 <= 1.0f);

  const float sqr_s1 = 1.f - c1 * c1;
  //assert(sqr_s1 <= 1 && sqr_s1 >= 0);
  const std::complex<float> ct
      = std::sqrt(eta_2 * eta_2 - eta_1 * eta_1 * sqr_s1);   // represents eta2 * cos(theta_transm)

  const std::complex<float> a    = eta_1 * c1;
  const float               Rper = (float)std::pow(std::abs((a - ct) / (a + ct)), 2.f);
  assert(Rper >= 0.0f && Rper <= 1.0f);

  const std::complex<float> c    = eta_2 * eta_2 * c1;
  const std::complex<float> d    = eta_1 * ct;
  const float               Rpar = std::pow(std::abs((c - d) / (c + d)), 2.f);
  assert(Rpar >= 0.0f && Rpar <= 1.0f);

  return (Rpar + Rper) * 0.5f;
}

/**
 * @brief      Compute the Fresnel coefficient from a complex index of refraction
 *
 * @param      a_eta1        Complex IOR of the first medium (incoming)
 * @param      a_eta2        Complex IOR of the second medium  (transmitted)
 * @param[in]  cos_theta_d  cosine of difference vector (= view dot half_vector)
 *
 * @return     The Fresnel Coefficient
 */
inline float fresnel(std::complex<float> const &a_eta1, std::complex<float> const &a_eta2, double const cos_theta_d)
{
  if (cos_theta_d <= 0.0)
  {
    return 1.0;
  }

  //NO imaginary part -> dielectric / dielectric case
  if (a_eta1.imag() < EPSILON && a_eta2.imag() < EPSILON)
  {
    return fresnel_dielectric_dielectric(a_eta2.real() / a_eta1.real(), cos_theta_d);
  }

  // Dielectric / Conductor
  if (a_eta1.imag() < EPSILON)
  {
    return static_cast<float>(
        fresnel_dielectric_conductor(a_eta2.real() / a_eta1.real(), a_eta2.imag() / a_eta1.real(), cos_theta_d));
  }

  // Conductor / conductor
  return fresnel_conductor_conductor(a_eta1, a_eta2, cos_theta_d);
}

/**
 * @brief   Compute the Fresnel coefficient using the best method
 *
 * @param   n1          Real part of first IOR
 * @param   k1          Imaginary part of first IOR
 * @param   n2          Real part of second IOR
 * @param   k2          Imaginary part of second IOR
 * @param   cos_theta_d Cosine of incoming ray
 *
 * @return              Fresnel term for this configuration
 */
inline float fresnel(float n1, float k1, float n2, float k2, double const cos_theta_d)
{
  return fresnel(std::complex<float>(n1, k1), std::complex<float>(n2, k2), cos_theta_d);
}

/**
 * @brief   Compute the Fresnel coefficient using the best method
 *
 * @param   n1          Real part of first IOR
 * @param   k1          Imaginary part of first IOR
 * @param   n2          Real part of second IOR
 * @param   k2          Imaginary part of second IOR
 * @param   cos_theta_d Cosine of incoming ray
 *
 * @return              Fresnel term for this configuration
 */
inline RADIANCE_TYPE
fresnel(RADIANCE_TYPE n1, RADIANCE_TYPE k1, RADIANCE_TYPE n2, RADIANCE_TYPE k2, double const cos_theta_d)
{
  // TODO : computations should be vectorized ?
  RADIANCE_TYPE fresnel_value;

  for (unsigned int channel = 0; channel < fresnel_value.size(); channel++)
  {
    fresnel_value[channel] = fresnel(
        std::complex<float>(n1[channel], k1[channel]),
        std::complex<float>(n2[channel], k2[channel]),
        cos_theta_d);
  }
  return fresnel_value;
}

/**
 * @brief      Compute the Fresnel Coefficient for a Conductor
 *
 * @param      square_eta  The square eta
 * @param      square_k    The square k
 * @param[in]  cos_theta   The cosine theta
 *
 * @return     { description_of_the_return_value }
 */
inline RADIANCE_TYPE
fresnel_for_conductor(RADIANCE_TYPE const &square_eta, RADIANCE_TYPE const &square_k, double cos_theta)
{   // TODO : should replace fresnel_dielectric_conductor function
#ifdef MRF_RENDERING_MODE_SPECTRAL
  (void)square_eta;
  (void)square_k;
  (void)cos_theta;
  return RADIANCE_TYPE();
#else

  // From S. Lagarde
  // Dielectric / Conductor

  float const CosTheta2 = static_cast<float>(cos_theta * cos_theta);
  float const SinTheta2 = static_cast<float>(1.0 - CosTheta2);
  assert(SinTheta2 >= 0.0f);

  RADIANCE_TYPE const Eta2  = square_eta;
  RADIANCE_TYPE const Etak2 = square_k;

  RADIANCE_TYPE const t0       = Eta2 - Etak2 - RADIANCE_TYPE(SinTheta2);
  RADIANCE_TYPE const a2plusb2 = (t0 * t0 + Eta2 * Etak2 * 4.0).sqrt();
  RADIANCE_TYPE const t1       = a2plusb2 + RADIANCE_TYPE(CosTheta2);
  RADIANCE_TYPE const a        = ((a2plusb2 + t0) * 0.5).sqrt();

#  ifdef _MSC_VER
#    pragma warning(disable : 4244)
#  endif

  RADIANCE_TYPE const t2 = a * cos_theta * 2.0;

#  ifdef _MSC_VER
#    pragma warning(default : 4244)
#  endif

  RADIANCE_TYPE const Rs = (t1 - t2) / (t1 + t2);

  RADIANCE_TYPE const t3 = a2plusb2 * CosTheta2 + RADIANCE_TYPE(SinTheta2 * SinTheta2);
  RADIANCE_TYPE const t4 = t2 * SinTheta2;

  RADIANCE_TYPE const Rp = Rs * (t3 - t4) / (t3 + t4);

  RADIANCE_TYPE const final_result = (Rp + Rs) * 0.5;




#  ifdef MRF_FRESNEL_FOR_CONDUCTOR_DEBUG
  if (Rs.sqrLength() <= 0.0)
  {
    std::cout << "cos_theta = " << cos_theta << std::endl;
    assert(0);
  }
  if (Rp.sqrLength() <= 0.0)
  {
    std::cout << " Rs = " << Rs << std::endl;
    std::cout << "cos_theta = " << cos_theta << std::endl;
    assert(0);
  }
  if (final_result.sqrLength() <= 0.0)
  {
    std::cout << " Rs = " << Rs;
    std::cout << " Rp  = " << Rs;
    std::cout << "cos_theta = " << cos_theta << std::endl;
  }
#  endif



  return final_result;
#endif
}


}   // namespace materials
}   // namespace mrf
