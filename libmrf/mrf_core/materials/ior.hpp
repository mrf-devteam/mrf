/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/materials.hpp>

namespace mrf
{
namespace materials
{
/**
 * @brief      This class represents the index of refraction of a medium
 */
template<typename T>
class IOR_template
{
private:
  /* Real-part of the Index of Refraction */
  RADIANCE_TYPE _eta;
  /* Imaginary-part of the Index of Refraction, which represents absorption of the medium. Only valid for Conductor. */
  RADIANCE_TYPE _kappa;

public:
  inline IOR_template(T real_part = T(1.0), T img_part = T(0.0)): _eta(real_part), _kappa(img_part) {}

  inline IOR_template(RADIANCE_TYPE const &eta): _eta(eta), _kappa(T(0.0)) {}


  inline IOR_template(RADIANCE_TYPE const &eta, RADIANCE_TYPE const &kappa): _eta(eta), _kappa(kappa) {}

  inline RADIANCE_TYPE const &real() const { return _eta; }

  inline RADIANCE_TYPE const &img() const { return _kappa; }

  inline RADIANCE_TYPE const &eta() const { return _eta; }

  inline RADIANCE_TYPE const &kappa() const { return _kappa; }


  inline RADIANCE_TYPE sqrNorm() const { return (_eta * _eta + _kappa * _kappa); }
};

typedef MRF_CORE_EXPORT IOR_template<float> IOR;
typedef MRF_CORE_EXPORT IOR_template<double> IOR_D;


IOR const AIR_IOR(RADIANCE_TYPE(1.0f), RADIANCE_TYPE(0.0f));



}   // namespace materials
}   // namespace mrf