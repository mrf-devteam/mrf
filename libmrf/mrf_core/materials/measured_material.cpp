/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#include <mrf_core/materials/measured_material.hpp>
#include <mrf_core/sampling/random_generator.hpp>

namespace mrf
{
namespace materials
{
using namespace mrf::sampling;
using namespace math;


MeasuredMaterial::MeasuredMaterial(std::string const &name): BRDF(name) {}

MeasuredMaterial::MeasuredMaterial(std::string const &name, std::string const &type): BRDF(name, type) {}


MeasuredMaterial::~MeasuredMaterial() {}



float MeasuredMaterial::diffuseAlbedo() const
{
  return 0.f;
}

float MeasuredMaterial::diffuseAlbedo(mrf::geom::LocalFrame const & /*lf*/) const
{
  return diffuseAlbedo();
}

bool MeasuredMaterial::diffuseComponent() const
{
  return true;
}

float MeasuredMaterial::specularAlbedo() const
{
  return 0.0f;
}

float MeasuredMaterial::specularAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0;
}

bool MeasuredMaterial::specularComponent() const
{
  return false;
}

float MeasuredMaterial::transmissionAlbedo() const
{
  return 0.0f;
}

float MeasuredMaterial::transmissionAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0;
}

bool MeasuredMaterial::transmissionComponent() const
{
  return false;
}


float MeasuredMaterial::nonDiffuseAlbedo() const
{
  return 0.0f;
}

bool MeasuredMaterial::nonDiffuseComponent() const
{
  return false;
}


float MeasuredMaterial::totalAlbedo() const
{
  return 0.f;
}

//-------------------------------------------------------------------
// float MeasuredMaterial::diffPDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const

// {
//   return 0.f;
// }

// float MeasuredMaterial::specPDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const
// {
//   return 0.0f;
// }

// float MeasuredMaterial::transPDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const
// {
//   return 0.0f;
// }

// float MeasuredMaterial::nonDiffusePDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const
// {
//   return 0.0f;
// }



//-------------------------------------------------------------------
ScatterEvent::SCATTER_EVT MeasuredMaterial::generateScatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    RADIANCE_TYPE & /*incident_radiance*/,
    Vec3f &outgoing_dir) const
{
  float e1 = RandomGenerator::Instance().getFloat();

  //TODO IMPROVE ME : check whether or not the norma of
  // incident_radiance * _color is > EPSILON
  // if not RETURN IMMEDIATELY ON ABSORPTION EVENT !!!

  if (e1 < diffuseAlbedo())   // TOTAL ALBEDO = DIFFUSE ALBEDO FOR
  // LAMBERTIAN SURFACE
  {
    //#ifdef DEBUG
    // std::cout << " HEere = " << __FILE__ << " "<<  __LINE__  << std::endl;
    // std::cout << "  _color = " << _color << std::endl ;
    //#endif


    //We use a pdf proportional to the cosine
    //incident_radiance *= _color;
    e1 /= diffuseAlbedo();

    float e2     = RandomGenerator::Instance().getFloat();
    outgoing_dir = randomHemiCosDirection(lf, e1, e2);

    //#ifdef DEBUG
    // std::cout << "  outgoing_dir = " << outgoing_dir << std::endl ;
    //#endif

    return ScatterEvent::DIFFUSE_REFL;
  }
  else
  {
    return ScatterEvent::ABSORPTION;
  }
}

ScatterEvent::SCATTER_EVT MeasuredMaterial::scatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f &outgoing_dir,
    float &           proba_outgoing_dir,
    float             e1,
    float             e2) const
{
  if (e1 < diffuseAlbedo())   // TOTAL ALBEDO = DIFFUSE ALBEDO FOR
  // LAMBERTIAN SURFACE
  {
    //We use a pdf proportional to the cosine
    float const e3     = e1 / diffuseAlbedo();
    outgoing_dir       = randomHemiCosDirection(lf, e2, e3);
    proba_outgoing_dir = static_cast<float>(Math::INV_PI) * diffuseAlbedo();

    return ScatterEvent::DIFFUSE_REFL;
  }
  else
  {
    return ScatterEvent::ABSORPTION;
  }
}



ScatterEvent::SCATTER_EVT MeasuredMaterial::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    float             e1,
    float             e2,
    mrf::math::Vec3f &outgoing_dir,
    RADIANCE_TYPE &   brdf_corrected_by_pdf) const
{
  if (e1 < diffuseAlbedo())   // TOTAL ALBEDO = DIFFUSE ALBEDO FOR LAMBERTIAN SURFACE
  {
    //We use a pdf proportional to the cosine
    float const e3 = e1 / diffuseAlbedo();
    outgoing_dir   = randomHemiCosDirection(lf, e2, e3);
    //proba_outgoing_dir = Math::INV_PI * diffuseAlbedo();

    // Since the BRDF / pdf = 1.0, we just return the _color (which is normalized)
    float const dot_NL = outgoing_dir.dot(lf.normal());
    if (dot_NL <= 0.0f)
    {
      brdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
      return ScatterEvent::ABSORPTION;
    }

    brdf_corrected_by_pdf = 0.f;   // _color / dot_NL;

    return ScatterEvent::DIFFUSE_REFL;
  }
  return ScatterEvent::ABSORPTION;
}

}   // namespace materials
}   // namespace mrf
