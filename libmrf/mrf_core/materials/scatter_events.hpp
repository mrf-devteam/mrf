/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace materials
{
/**
 * The different type of possible events when light interacts
 * with a Material
 **/
class MRF_CORE_EXPORT ScatterEvent
{
public:
  enum SCATTER_EVT
  {
    ABSORPTION    = 0,
    TIR           = 1, /* Total Internal Reflection */
    INTERNAL_REFL = 2, /* INTERNAL REFLECTION inside the medium */
    DIFFUSE_REFL  = 3,
    SPECULAR_REFL = 4,
    /* THis means a reflection occured but it can be diffuse or specular */
    ANY_REFL = 5,
    /* Perfect Transmissive Case when the surface is not rough  */
    TRANSMISSION = 6,
    /* This means a transmission has occured but not necessarily the perfect one*/
    TRANSMISSION_FRONT = 7,   // Transmission when view dir is front facing the geometry normal
    TRANSMISSION_BACK  = 8,   // Transmission when view dir is back facing the geometry normal
    ANY_TRANSMISSION   = 9
  };
};

}   // namespace materials
}   // namespace mrf
