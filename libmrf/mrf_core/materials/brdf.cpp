/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#include <mrf_core/materials/brdf.hpp>

namespace mrf
{
namespace materials
{
using namespace mrf::sampling;
using namespace mrf::math;
using namespace mrf::geom;


//----------------       MOTHER CLASS BRDF    ------------------//
//--------------------------------------------------------------//
BRDF::BRDF(std::string const &name): UMat(name) {}
BRDF::BRDF(std::string const &name, std::string const &type): UMat(name, type) {}

BRDF::~BRDF() {}

//-------------------------------------------------------------------------
// Default behaviour is to return wrong or 0.0
// TODO: This shall be an abstract class (af)
//-------------------------------------------------------------------------
bool BRDF::diffuseComponent() const
{
  assert(0);
  return false;
}

float BRDF::diffuseAlbedo() const
{
  assert(0);
  return 0.0f;
}

float BRDF::specularAlbedo() const
{
  assert(0);
  return 0.0f;
}

bool BRDF::specularComponent() const
{
  assert(0);
  return false;
}

float BRDF::nonDiffuseAlbedo() const
{
  assert(0);
  return 0.0f;
}

bool BRDF::nonDiffuseComponent() const
{
  assert(0);
  return false;
}

float BRDF::diffPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  assert(0);
  return 0.0f;
}

float BRDF::specPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  assert(0);
  return 0.0f;
}

float BRDF::nonDiffusePDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  assert(0);
  return 0.0f;
}



//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
// Partial Implementation of UMat's methods
//-------------------------------------------------------------------------
float BRDF::diffuseAlbedo(mrf::geom::LocalFrame const &) const
{
  return diffuseAlbedo();
}

float BRDF::specularAlbedo(mrf::geom::LocalFrame const &) const
{
  return specularAlbedo();
}

bool BRDF::isEmissive() const
{
  return false;
}

RADIANCE_TYPE
BRDF::radianceEmitted(Vec3f const & /*emit_out_dir*/, mrf::geom::LocalFrame const & /*lf*/) const
{
  return RADIANCE_TYPE();
}


RADIOSITY_TYPE
BRDF::radiosityEmitted() const
{
  return RADIOSITY_TYPE();
}

RADIOSITY_TYPE
BRDF::radiosityEmitted(mrf::geom::LocalFrame const &) const
{
  return RADIOSITY_TYPE();
}

RADIOSITY_TYPE
BRDF::particleEmission(
    mrf::geom::LocalFrame const & /*lf*/,
    float /*random_var_1*/,
    float /*random_var_2*/,
    mrf::math::Vec3f & /*direction*/) const
{
  return RADIOSITY_TYPE();
}


float BRDF::brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir)
    const
{
  RADIOSITY_TYPE c_brdf = coloredBRDFValue(lf, in_dir, out_dir);
#ifdef MRF_RENDERING_MODE_SPECTRAL
  return c_brdf.normalizedIntegral();
#else
  //TODO IMPLEMENT THIS IN color::Color
  //return c_brdf.power();
  const float DEFAULT_LUMINANCE_COEFF = 1.0f / 3.0f;
  return (
      DEFAULT_LUMINANCE_COEFF * c_brdf.r() + DEFAULT_LUMINANCE_COEFF * c_brdf.g()
      + DEFAULT_LUMINANCE_COEFF * c_brdf.b());
#endif
}

float BRDF::transmissionAlbedo() const
{
  return 0.0f;
}

float BRDF::transmissionAlbedo(LocalFrame const &) const
{
  return 0.0f;
}

bool BRDF::transmissionComponent() const
{
  return false;
}

float BRDF::transPDF(LocalFrame const & /*lf*/, Vec3f const & /*in_dir*/, Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

mrf::materials::IOR BRDF::ior() const
{
  assert(0);
  return AIR_IOR;
}

ScatterEvent::SCATTER_EVT BRDF::generateScatteringDirection(
    LocalFrame const &lf,
    Vec3f const &     in_dir,
    RADIANCE_TYPE & /*incident_radiance*/,
    Vec3f &outgoing_dir) const
{
  float proba_out;
  return UMat::scatteringDirection(lf, in_dir, outgoing_dir, proba_out);
}


ScatterEvent::SCATTER_EVT BRDF::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    float                        e1,
    float                        e2,
    mrf::math::Vec3f &           outgoing_dir,
    RADIANCE_TYPE &              brdf_corrected_by_pdf) const
{
  using namespace mrf::math;

  // By default if the child class has not implemented anything we sampling over the cosine
  outgoing_dir       = randomHemiCosDirection(lf, e1, e2);
  float const dot_NL = mrf::math::clamp(lf.normal().dot(outgoing_dir), 0.0f, 1.0f);
  if (dot_NL > EPSILON)
  {
    brdf_corrected_by_pdf
        = coloredBRDFValue(lf, in_dir, outgoing_dir) * static_cast<float>(mrf::math::Math::PI) / dot_NL;
    return ScatterEvent::ANY_REFL;
  }
  else
  {
    //Sometimes it does happen...need to investigate why
    //assert(0);
    return ScatterEvent::ABSORPTION;
  }
}


ScatterEvent::SCATTER_EVT BRDF::scatteringDirectionWithBSDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    float                        e1,
    float                        e2,
    mrf::math::Vec3f &           outgoing_dir,
    RADIANCE_TYPE &              bsdf_corrected_by_pdf,
    mrf::materials::IOR const & /*in_ior*/,
    mrf::materials::IOR const & /*out_ior*/) const
{
  //Calling the BRDF version for BRDF
  return scatteringDirectionWithBRDF(lf, in_dir, e1, e2, outgoing_dir, bsdf_corrected_by_pdf);
}


}   // namespace materials
}   // namespace mrf
