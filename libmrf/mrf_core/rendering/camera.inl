/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * camera class for rendering
 * represent a pinhole camera
 *
 **/
/*
inline Camera::Camera(mrf::math::Vec3f const & pos,
                      mrf::math::Vec3f const & lookAt,
                      mrf::math::Vec3f const & up,
                      float fovy,
                      float vpd,
                      unsigned int image_width_in_pixels,
                      unsigned int image_height_in_pixels,
                      unsigned int nb_samples_per_pixels)
  : _position(pos),
    _look_at(lookAt),
    _up(up),
    _fovy(fovy),
    _aspect_ratio(static_cast<float>(image_width_in_pixels) / static_cast<float>(image_height_in_pixels)),
    _view_plane_distance(vpd),
    _far_plane_distance(MAX_ZFAR),
    _top(_view_plane_distance * std::tan(mrf::math::Math::toRadian(_fovy) * 0.5f)),
    _right(_aspect_ratio * _top),
    _normalized_optical_axis((_look_at - _position).normal()),
    _right_axis(_normalized_optical_axis.cross(_up)),
    _sensor(_position + _normalized_optical_axis * _view_plane_distance + _up * _top - (_right_axis * _right),
            _right_axis,
            -_up,
            _normalized_optical_axis,
            mrf::math::Vec2u(image_width_in_pixels, image_height_in_pixels),
            mrf::math::Vec2d(2.f * _right, 2.f * _top)),
    _sensor_sampler(&_sensor, nb_samples_per_pixels)
{
  updateInnerData();
  updateRotMat();
}
*/
/*
inline Camera::Camera(
    mrf::math::Vec3f const &pos,
    mrf::math::Vec3f const &lookAt,
    mrf::math::Vec3f const &up,
    float                   vpd,
    float                   fpd,
    size_t                  image_width_in_pixels,
    size_t                  image_height_in_pixels,
    unsigned int            nb_samples_per_pixels,
    std::string             id_name)
  : _position(pos)
  , _look_at(lookAt)
  , _up(up)
  , _fovy(fovy)
  , _aspect_ratio(static_cast<float>(image_width_in_pixels) / static_cast<float>(image_height_in_pixels))
  , _view_plane_distance(vpd)
  , _far_plane_distance(fpd)
  , _top(_view_plane_distance * std::tan(mrf::math::Math::toRadian(_fovy) * 0.5f))
  , _right(_aspect_ratio * _top)
  , _normalized_optical_axis((_look_at - _position).normal())
  , _right_axis(_normalized_optical_axis.cross(_up))
  , _sensor(
        _position + _normalized_optical_axis * _view_plane_distance + _up * _top - (_right_axis * _right),
        _right_axis,
        -_up,
        _normalized_optical_axis,
        mrf::math::Vec2sz(image_width_in_pixels, image_height_in_pixels),
        mrf::math::Vec2d(2.f * _right, 2.f * _top))
  , _sensor_sampler(&_sensor, nb_samples_per_pixels)
  , _id_name(id_name)

{
  updateInnerData();
  updateRotMat();
}

inline Camera::Camera(
    mrf::geom::AABBox const &bbox,
    size_t                   image_width_in_pixels,
    size_t                   image_height_in_pixels,
    unsigned int             nb_samples_per_pixels,
    std::string              id_name)

  : _position(bbox.center() - mrf::math::Vec3f(0.f, 0.f, bbox.getRadius() / tan(mrf::math::Math::toRadian(fov * 0.5f))))
  , _look_at(bbox.center())
  , _up(mrf::math::Vec3f(0.f, 1.f, 0.f))
  , _fovy(fov)
  , _aspect_ratio((bbox.max().x() - bbox.min().x()) / (bbox.max().y() - bbox.min().y()))
  , _view_plane_distance(std::max(MIN_ZNEAR, (_position - _look_at).length() - (bbox.getRadius() + 10.f * EPSILON)))
  , _far_plane_distance((_position - _look_at).length() + (bbox.getRadius() + 10.f * EPSILON))
  , _top(_view_plane_distance * std::tan(mrf::math::Math::toRadian(_fovy) * 0.5f))
  , _right(_aspect_ratio * _top)
  , _normalized_optical_axis((_look_at - _position).normal())
  , _right_axis(_normalized_optical_axis.cross(_up))
  , _sensor(
        _position + _normalized_optical_axis * _view_plane_distance + _up * _top - (_right_axis * _right),
        _right_axis,
        -_up,
        _normalized_optical_axis,
        mrf::math::Vec2sz(image_width_in_pixels, image_height_in_pixels),
        mrf::math::Vec2d(2.0f * _right, 2.0f * _top))
  , _sensor_sampler(&_sensor, nb_samples_per_pixels)
  , _id_name(id_name)
{
  updateInnerData();
  updateRotMat();
}

inline mrf::math::Vec3f const &Camera::position() const
{
  return _position;
}

inline mrf::math::Vec3f const &Camera::lookAt() const
{
  return _look_at;
}

inline mrf::math::Vec3f const &Camera::up() const
{
  return _up;
}

inline mrf::math::Vec3f &Camera::position()
{
  return _position;
}

inline mrf::math::Vec3f &Camera::lookAt()
{
  return _look_at;
}

inline mrf::math::Vec3f &Camera::up()
{
  return _up;
}

inline float Camera::viewPlaneDistance() const
{
  return _view_plane_distance;
}

inline float Camera::farPlaneDistance() const
{
  return _far_plane_distance;
}

inline float Camera::zNear() const
{
  return _view_plane_distance;
}

inline float Camera::zFar() const
{
  return _far_plane_distance;
}

inline float Camera::fovy() const
{
  return _fovy;
}

inline float Camera::left() const
{
  return _left;
}

inline float Camera::right() const
{
  return _right;
}

inline float Camera::bottom() const
{
  return _bottom;
}

inline float Camera::top() const
{
  return _top;
}

inline float Camera::aspectRatio() const
{
  return _aspect_ratio;
}

inline float &Camera::aspectRatio()
{
  return _aspect_ratio;
}

inline mrf::math::Mat3f const &Camera::rotMat() const
{
  return _rotation_matrix;
}

inline Sensor const &Camera::sensor() const
{
  return _sensor;
}

inline Sensor &Camera::sensor()
{
  return _sensor;
}

inline SensorSampler const &Camera::sensorSampler() const
{
  return _sensor_sampler;
}

inline SensorSampler &Camera::sensorSampler()
{
  return _sensor_sampler;
}

inline void Camera::setSensorResolution(unsigned int width, unsigned int height)
{
  _sensor.setResolution(width, height);
  _sensor_sampler.updateSubPixelVectors();
}

inline void Camera::setSensorResolution(mrf::math::Vec2u const &new_resolution)
{
  setSensorResolution(new_resolution.x(), new_resolution.y());
}

inline mrf::math::Mat4f Camera::viewMat() const
{
  mrf::math::Vec3f translation = _rotation_matrix * _position;
  return mrf::math::Mat4f(
      _rotation_matrix(0, 0),
      _rotation_matrix(0, 1),
      _rotation_matrix(0, 2),
      -translation[0],
      _rotation_matrix(1, 0),
      _rotation_matrix(1, 1),
      _rotation_matrix(1, 2),
      -translation[1],
      _rotation_matrix(2, 0),
      _rotation_matrix(2, 1),
      _rotation_matrix(2, 2),
      -translation[2],
      0,
      0,
      0,
      1);
}

inline mrf::math::Mat4f Camera::projMat() const
{
  float invtan = 1.f / static_cast<float>(tan(_fovy * 0.5 * mrf::math::Math::PI / 180));
  float range  = _far_plane_distance - _view_plane_distance;

  //need to be implemented
  return mrf::math::Mat4f(
      invtan / _aspect_ratio,
      0,
      0,
      0,
      0,
      invtan,
      0,
      0,
      0,
      0,
      -(_view_plane_distance + _far_plane_distance) / range,
      -2 * _view_plane_distance * _far_plane_distance / range,
      0,
      0,
      -1,
      0);
}

inline void Camera::position(mrf::math::Vec3f const &npos)
{
  _position = npos;
  //updateRotMat();
}

inline void Camera::lookAt(mrf::math::Vec3f const &nlookat)
{
  _look_at = nlookat;
  //updateRotMat();
}

inline void Camera::up(mrf::math::Vec3f const &nup)
{
  _up = nup;
  //updateRotMat();
}

inline void Camera::viewPlaneDistance(float nvpd)
{
  _view_plane_distance = nvpd;
  updateInnerData();
}

inline void Camera::farPlaneDistance(float nfpd)
{
  _far_plane_distance = nfpd;
}

inline void Camera::fovy(float nfovy)
{
  _fovy = nfovy;
}

inline void Camera::aspectRatio(float aspectRatio)
{
  _aspect_ratio = aspectRatio;
}

inline void Camera::rotMat(mrf::math::Mat3f nrotMat)
{
  _rotation_matrix = nrotMat;
}

//Useful functions
inline mrf::math::Vec3f Camera::dir2LookAt() const
{
  return _look_at - _position;
}

inline void Camera::translate(mrf::math::Vec3f const &t)
{
  _position += t;
}

inline mrf::math::Vec3f Camera::rightAxis() const
{
  return dir2LookAt().cross(_up).normalize();
}

inline void Camera::rotate(mrf::math::Vec3f const &axis, float angle)
{
  mrf::math::Quatf q;
  q.fromAxisAngle(axis, angle);

  rotate(q);
}

inline void Camera::rotate(mrf::math::Quatf const &q)
{
  mrf::math::Mat4f const m   = q.getMatrix();
  mrf::math::Vec3f       dir = dir2LookAt();

  _up      = m * _up;
  dir      = m * dir;
  _look_at = _position + dir;
}

void Camera::turnRight(float angle, bool useUpVector)
{
  turnLeft(-angle, useUpVector);
}

void Camera::turnLeft(float angle, bool useUpVector)
{
  mrf::math::Vec3f up = useUpVector ? _up : mrf::math::Vec3f(0.f, 1.f, 0.f);
  rotate(up, angle);
}

void Camera::lookUp(float angle)
{
  rotate(rightAxis(), angle);
}

void Camera::lookDown(float angle)
{
  rotate(rightAxis(), -angle);
}

inline float Camera::depthNormalized(float t) const
{
  float const z_range = _far_plane_distance - _view_plane_distance;

  return (t - _view_plane_distance) / z_range;
}

inline void Camera::updateClippingPlanes(mrf::geom::AABBox const &newBBox)
{
  mrf::math::Vec3f const center = newBBox.center();
  mrf::math::Vec3f       v(center - _position);
  float const            size  = newBBox.getRadius() + 10.f * EPSILON;
  float const            zNear = std::max(MIN_ZNEAR, v.length() - size);
  float const            zFar  = v.length() + size;
  mrf::math::Vec3f       dir   = dir2LookAt().normal();
  v.normalEq();
  float const angle = acosf(dir[0] * v[0] + dir[2] * v[2]);
  float const z1    = zNear * sinf(angle);
  float const d     = sqrtf(fabsf(zNear * zNear - z1 * z1));

  _view_plane_distance = std::max(MIN_ZNEAR, d);
  _far_plane_distance  = zFar;
}

inline void Camera::updateClippingPlanes(float radius)
{
  float const z = (_position - _look_at).length();
  radius += 10.f * EPSILON;

  _view_plane_distance = std::max(MIN_ZNEAR, z - radius);
  _far_plane_distance  = z + radius;
  updateInnerData();
}



inline void Camera::updateRotMat()
{
  mrf::math::Vec3f const inv_view = (_position - _look_at).normalize();
  _rotation_matrix.setRow(0, _up.cross(inv_view));
  _rotation_matrix.setRow(1, _up);
  _rotation_matrix.setRow(2, inv_view);
}


inline std::string const &Camera::idName() const
{
  return _id_name;
}
*/
