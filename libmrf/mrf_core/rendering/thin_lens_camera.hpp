/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2015
 *
 * modifications : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 **/

#pragma once


#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/sampling/jittering.hpp>
#include <mrf_core/rendering/camera.hpp>

#include <iostream>
#include <iomanip>

namespace mrf
{
namespace rendering
{
/**
 *
 * This class describes a camera implementing
 * the Thin-Lens Approximation
 *
 *  1 / _focalLength = 1 / _focalDistance + 1 / _sensorDistance
 *
 *  Every units should be specified using the International System of Units
 *  Distances should be specified in meter.
 *
 *  Changing on these distances automically adjust the others
 **/
class MRF_CORE_EXPORT ThinLens: public Camera
{
private:
  /* Vertical FOV*/
  float _fovy;

  /* Aperture */
  float _aperture;

  /* The f number aka f# */
  double _fNumber;

  /* The focal length of the lens */
  double _focalLength;

  /* The distance between the lens and the focal plane
   * (THIS IS NOT THE FOCAL LENGTH OF THE LENS) */
  double _focalDistance;

  /* The distance between the captor and the lens */
  double _sensorDistance;

  /* Jitterer Object to Sample the Lens */
  mrf::sampling::JitteredSample2D _lensJitterer;

  /* Local Frame Object positioned at the center of the lens
   * X axis is the  _right vector
   * Y axis is the _up     vector
   * Z acis is the inverse of the Optical Axis
   */
  mrf::math::Reff _thinLensFrame;

public:
  /**
   * @brief      Construct a Thin-Lens Camera
   *             All units need to be passed in the International System of Units
   *
   * @param      pos                     The position in the 3D scene where the Camera is
   * @param      lookAt                  The point where the camera is looking at
   * @param[in]  focal_length            The focal length of the Camera (in meter)
   * @param[in]  f_number                The F# number
   * @param[in]  focal_distance          The distance in meter where the focal plane lies
   * @param[in]  sensor_size             The size (width, height) of the sensor
   * @param[in]  image_width_in_pixels   The width of the image in pixels
   * @param[in]  image_height_in_pixels  The height of the image in pixels
   * @param[in]  nb_samples_per_pixels   The number of samples/rays (this is pass to the SensorSampler)
   * @param[in]  nb_samples_for_lens     The number of samples used to sample the lens
   */
  inline ThinLens(
      mrf::math::Vec3f const &pos                   = mrf::math::Vec3f(0.0f, 0.0f, -10.0f),
      mrf::math::Vec3f const &lookAt                = mrf::math::Vec3f(0.0f, 0.0f, 0.0f),
      mrf::math::Vec3f const &up                    = mrf::math::Vec3f(0.0f, 1.0f, 0.0f),
      float                   fov                   = 60.,
      double                  focal_length          = 0.035,
      double                  f_number              = 5.0,
      double                  focal_distance        = 1.0,
      float                   aperture              = 1.,
      mrf::math::Vec2d const &sensor_size           = mrf::math::Vec2d(0.01, 0.01),
      mrf::math::Vec2u const &sensor_res            = mrf::math::Vec2u(512, 512),
      unsigned int            nb_samples_per_pixels = 4,
      unsigned int            nb_samples_for_lens   = 8,
      std::string             id_name               = "NO_ID_NAME_SPECIFIED");




  //Accessors / Setters
  inline double fNum() const;
  inline double focalLength() const;

  inline double focalPlaneDistance() const;
  inline void   focalPlaneDistance(double nfd);

  inline double       captorDistance() const;
  inline unsigned int lensNbSamples() const;

  inline float aperture() const;
  inline void  aperture(float naperture);

  //------------------------------------------------------------------------------------------
  // Accessors with overhead computation
  //------------------------------------------------------------------------------------------

  /**
   * @brief      Compute the exact fovy (diagonal one) of the Thin Lens Camera
   *             This computation does not assume focus at infinity
   *
   * @return     The diagonal fovy in degrees
   */
  inline float fovy() const;
  inline void  fovy(float nfovy);


  // Compute Fovy in degrees but for the X (resp. Y) direction
  inline double fovyX() const;
  inline double fovyY() const;

  /**
   * @brief      Returns the diagonal Field of View (FOVY)
   *             The diagonal FOVY = FOVY_IN_X + FOVY_IN_Y
   *
   * @return     A double representing the diagonal FOVY
   */
  inline double fovyAtInfinity() const;

  /**
   * @brief      Returns the Field of View (FOVY) in X (width) and Y (height) dimensions
   *
   * @return    A 2D vector containing the fovy for X and Y dimensions
   */
  inline void fovyAtInfinity(mrf::math::Vec2<double> &fovy_xy) const;


  /**
   * @brief      Returns the effctive aperture of this Camera
   *             The effective apeture is equal to  (focal_length / f#)
   * @return     the effective aperture
   */
  inline double effectiveAperture() const;


  /**
   * @brief      Returns a position on the Lens in Camera space
   *
   * @param[in]  random_e1  the random parameter inside [0,1]
   * @param[in]  random_e2  the random parameter inside [0,1]
   *
   * @return     the sample position on the Lens in Camera space
   */
  mrf::math::Vec3f sampleLens(float random_e1, float random_e2) const;




  //------------------------------------------------------------------------------------------
  // Interface from Camera
  //------------------------------------------------------------------------------------------
  mrf::lighting::Ray generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l) const;

  void generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l, mrf::lighting::Ray &ray) const;

  mrf::lighting::Ray
  generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l, unsigned int lens_sample_nb) const;


  unsigned int numberOfLensSamples() const;

  void generateLensBorderSamples(std::vector<mrf::math::Vec3d> &lens_border_positions) const;

  virtual CameraType getType() const { return CameraType::ThinLens; }
  virtual Camera *   copy() const { return (Camera *)new ThinLens(*this); }

  virtual mrf::math::Mat4f viewMat() const;
  virtual mrf::math::Mat4f projMat() const;

  virtual void updateInnerData();
};

//Constructor(s)
inline ThinLens::ThinLens(
    mrf::math::Vec3f const &pos,
    mrf::math::Vec3f const &lookAt,
    mrf::math::Vec3f const &up,
    float                   fov,
    double                  focal_length,
    double                  f_number,
    double                  focal_distance,
    float                   aperture,
    mrf::math::Vec2d const &sensor_size,
    mrf::math::Vec2u const &sensor_res,
    unsigned int            nb_samples_per_pixels,
    unsigned int            nb_samples_for_lens,
    std::string             id_name)
  : Camera(pos, lookAt, up, MIN_ZNEAR, MAX_ZFAR, 1., 512, 512, 4, id_name)
  , _fovy(fov)
  , _aperture(aperture)
  , _fNumber(f_number)
  , _focalLength(focal_length)
  , _focalDistance(focal_distance)
  , _sensorDistance(_focalLength * _focalDistance / (_focalDistance - _focalLength))
  , _lensJitterer(nb_samples_for_lens)
  , _thinLensFrame(pos)
{
  _aspect_ratio = (static_cast<float>(sensor_res.x()) / static_cast<float>(sensor_res.y()));

  std::cout << " INSIDE " << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << " _sensor Distance = " << _focalLength * _focalDistance / (_focalDistance - _focalLength) << std::endl;
  std::cout << " Right Axis = " << _right_axis << "  Up = " << _up
            << "  Normalized Optical Axis = " << _normalized_optical_axis << std::endl;




  //Update the position of the Sensor according to _sensorDistance

  // The code below will produce an inverted image

  // _sensor.setPosition( pos - _normalizedOA * _sensorDistance +
  //                      _up * sensor_size.y()*0.5 - (_rightAxis * sensor_size.x()*0.5 ) );

  // This seems weird but we inverse completely the system TO NOT get an inverted image
  _sensor.set(
      pos - _normalized_optical_axis * static_cast<float>(_sensorDistance)
          - _up * static_cast<float>(sensor_size.y() * 0.5) + (_right_axis * static_cast<float>(sensor_size.x() * 0.5)),
      -_right_axis,
      -_up,
      _normalized_optical_axis);


  // Update sensor resolution and sensor size and nb_samples_per_pixel for the SensorSampler
  _sensor.setResAndSize(sensor_res, sensor_size);
  _sensor_sampler.setSPP(nb_samples_per_pixels);

  // Update the Referenc Frame of the Thin-Lens
  _thinLensFrame.setOrientation(_right_axis, _up, -_normalized_optical_axis);
}




//----------------------------------------------------------------------------------------------
//Accessors
//----------------------------------------------------------------------------------------------

inline void ThinLens::fovy(float nfovy)
{
  _fovy = nfovy;
}

inline double ThinLens::fNum() const
{
  return _fNumber;
}

inline double ThinLens::focalLength() const
{
  return _focalLength;
}

inline double ThinLens::focalPlaneDistance() const
{
  return _focalDistance;
}

inline void ThinLens::focalPlaneDistance(double nfd)
{
  _focalDistance = nfd;
}

inline double ThinLens::captorDistance() const
{
  return _sensorDistance;
}

inline unsigned int ThinLens::lensNbSamples() const
{
  return _lensJitterer.totalNbSamples();
}

inline float ThinLens::aperture() const
{
  return _aperture;
}

inline void ThinLens::aperture(float naperture)
{
  _aperture = naperture;
}


//----------------------------------------------------------------------------------------------
//Accessors with overhead
//----------------------------------------------------------------------------------------------
inline float ThinLens::fovy() const
{
  return _fovy;
  /*
  double const sensor_diagonal_size = _sensor.size().length();

  double const fovy = 2.0 * std::atan(sensor_diagonal_size / (2.0 * _sensorDistance));

  return static_cast<float>(mrf::math::_Math<double>::toDegree(fovy));
  */
}


inline double ThinLens::fovyX() const
{
  assert(0);
  return 0.0;
}

inline double ThinLens::fovyY() const
{
  assert(0);
  return 0.0;
}


inline double ThinLens::fovyAtInfinity() const
{
  using namespace mrf::math;

  double const sensor_diagonal_size = _sensor.size().length();

  double const fovy = 2.0 * std::atan(sensor_diagonal_size / (2.0 * _focalLength));

  return mrf::math::_Math<double>::toDegree(fovy);
}

inline void ThinLens::fovyAtInfinity(mrf::math::Vec2<double> &fovy_xy) const
{
  fovy_xy[0] = 2.0 * std::atan(_sensor.size().x() / (2.0 * _focalLength));
  fovy_xy[1] = 2.0 * std::atan(_sensor.size().y() / (2.0 * _focalLength));

  fovy_xy[0] = mrf::math::_Math<double>::toDegree(fovy_xy[0]);
  fovy_xy[1] = mrf::math::_Math<double>::toDegree(fovy_xy[1]);
}

inline double ThinLens::effectiveAperture() const
{
  return _focalLength / _fNumber;
}


// EXTERNAL OPERATORS
inline std::ostream &operator<<(std::ostream &os, ThinLens const &cam)
{
  mrf::math::Vec2d fovy_at_inf_xy;
  cam.fovyAtInfinity(fovy_at_inf_xy);


  return os << " Camera [ pos : " << cam.position() << " lookat : " << cam.lookAt() << " up     : " << cam.up()
            << std::endl
            << " X axis : " << cam.rightAxis() << std::endl
            << " Effective Aperture: " << cam.effectiveAperture() << std::endl
            << " F#     : " << cam.fNum() << " Focal Length : " << cam.focalLength()
            << " Sensor Distance : " << cam.captorDistance() << " Focal Plane Distance : " << cam.focalPlaneDistance()
            << std::endl
            << " Fovy At Infinity : " << cam.fovyAtInfinity() << " Fovy At Infiniy in X and Y " << fovy_at_inf_xy
            << " Exact Fovy : " << cam.fovy() << std::endl
            << std::setprecision(10) << " Sensor :  " << cam.sensor() << std::endl
            << " SensorSampler " << cam.sensorSampler() << " Number of Samples for Lens :  " << cam.lensNbSamples()
            << " ] " << std::endl;
}

}   // namespace rendering
}   // namespace mrf
