/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/


inline mrf::geom::MRF_AABBox const &Scene::bbox() const
{
  return _bbox;
}

inline mrf::geom::MRF_AABBox &Scene::bbox()
{
  return _bbox;
}

inline mrf::materials::COLOR Scene::background(mrf::math::Vec3f const &dir) const
{
  return _bckgmng->colorFromDirection(dir);
}

inline void Scene::setBckManager(BckgManager *bck_manager)
{
  _bckgmng = bck_manager;
}

inline bool Scene::hasBackground() const
{
  if (_bckgmng) return true;
  return false;
}

inline BckgManager *Scene::background() const
{
  return _bckgmng;
}

inline uint Scene::addMaterial(mrf::materials::UMat *a_brdf)
{
  _umats.push_back(a_brdf);
  return static_cast<uint>(_umats.size() - 1);
}

inline void Scene::markMaterialUnused(uint index)
{
  if (index < _umats.size())
  {
    delete _umats[index];
    _umats[index] = nullptr;
  }
}

inline mrf::materials::UMat const &Scene::umaterial(uint index) const
{
  return (*_umats[index]);
}

inline mrf::materials::UMat &Scene::umaterial(uint index)
{
  return (*_umats[index]);
}

inline uint Scene::umatSize() const
{
  return static_cast<uint>(_umats.size());
}

inline std::vector<mrf::lighting::Light *> const &Scene::lights() const
{
  return _lights;
}

inline mrf::lighting::Light const &Scene::lightByIndex(unsigned index) const
{
  return *(_lights[index]);
}

template<class LIGHT_TYPE>
inline bool Scene::getLight(LIGHT_TYPE &a_light, unsigned int index) const
{
  unsigned int count = 0;
  for (auto it = _lights.begin(); it != _lights.end(); ++it)
  {
    if (typeid(*it) == typeid(LIGHT_TYPE))
    {
      if (index == count)
      {
        a_light = static_cast<LIGHT_TYPE &>(*it);
        return true;
      }
      count++;
    }
  }
  return false;
}

template<class LIGHT_TYPE>
inline bool Scene::getAllLightsByType(std::vector<LIGHT_TYPE> &all_lights) const
{
  bool found = false;

  for (auto it = _lights.begin(); it != _lights.end(); ++it)
  {
    if (typeid(*it) == typeid(LIGHT_TYPE))
    {
      LIGHT_TYPE &a_light = static_cast<LIGHT_TYPE &>(*it);
      all_lights.push_back(a_light);
      found = true;
    }
  }
  return found;
}

inline void Scene::addPointLight(mrf::lighting::Light *a_light)
{
  _lights.push_back(a_light);

  if (_bbox.getRadius() < 1e-6)   // OK probably first thing in our scene that we add
  {
    _bbox = a_light->bbox();
  }
  else
  {
    _bbox = a_light->bbox().createUnion(_bbox);
  }

  _nbPointLights++;
}

inline void Scene::addDirLight(mrf::lighting::Light *a_light)
{
  _lights.push_back(a_light);
  _nbDirectionalLights++;
}

inline void Scene::addQuadLight(mrf::lighting::Light *a_light)
{
  _lights.push_back(a_light);

  if (_bbox.getRadius() < 1e-6)   // OK probably first thing in our scene that we add
  {
    _bbox = a_light->bbox();
  }
  else
  {
    _bbox = a_light->bbox().createUnion(_bbox);
  }

  _nbQuadLights++;
}

inline void Scene::addSphereLight(mrf::lighting::Light *a_light)
{
  _lights.push_back(a_light);

  if (_bbox.getRadius() < 1e-6)   // OK probably first thing in our scene that we add
  {
    _bbox = a_light->bbox();
  }
  else
  {
    _bbox = a_light->bbox().createUnion(_bbox);
  }


  _nbSphereLights++;
}

inline void Scene::removeAllLights()
{
  for (unsigned int i = 0; i < _lights.size(); i++)
  {
    delete _lights[i];
  }

  _nbPointLights       = 0;
  _nbDirectionalLights = 0;
  _nbQuadLights        = 0;
  _nbSphereLights      = 0;

  _lights.clear();
}


//-------------------------------------------------------------------------
// Accessors for Scene Statistics
//-------------------------------------------------------------------------

inline unsigned int Scene::nbPointLights() const
{
  return _nbPointLights;
}

inline unsigned int Scene::nbDirLights() const
{
  return _nbDirectionalLights;
}

inline unsigned int Scene::nbQuadLights() const
{
  return _nbQuadLights;
}

inline unsigned int Scene::nbSphereLights() const
{
  return _nbSphereLights;
}

inline uint Scene::nbLights() const
{
  return static_cast<uint>(_lights.size());
}

inline unsigned int Scene::totalIntersectionCost() const
{
  unsigned int       sum  = 0;
  unsigned int const size = static_cast<uint>(_meshes.size());
  for (unsigned int i = 0; i < size; i++)
  {
    //sum += _geomObjects[i]->intersectionCost();
    //TODO
  }

  return sum;
}

inline float Scene::totalGeometryCost() const
{
  unsigned int       sum  = 0;
  unsigned int const size = static_cast<uint>(_meshes.size());
  for (unsigned int i = 0; i < size; i++)
  {
    //sum += _geomObjects[i]->memorySize();
    //TODO
  }

  return sum / (1024.f * 1024.f);
}

inline std::string Scene::info() const
{
  std::string info("********************* Scene Info *********************\n");

  info.append("* \t AABBOX: ");
  info.append(_bbox.info());
  info.append("\t            *\n");


  info.append("* \t Number of Meshes: ");
  info.append(mrf::util::StringParsing::numericsToString(_meshes.size()));
  info.append("\t            *\n");

  info.append("* \t Number of Shapes: ");
  info.append(mrf::util::StringParsing::numericsToString(_shapes.size()));
  info.append("\t            *\n");

  info.append("* \t Geometry Cost in Memory in Mb: ");
  info.append(mrf::util::StringParsing::numericsToString(totalGeometryCost()));
  info.append("\t            \n");

  info.append("* \t Total Intersection Cost: ");
  info.append(mrf::util::StringParsing::numericsToString(totalIntersectionCost()));
  info.append("\t            \n");

  info.append("*                                                   *\n");

  info.append("* \t Number of Lights: ");
  info.append(mrf::util::StringParsing::numericsToString(nbLights()));
  info.append("\t                    *\n");

  info.append("* \t Number of Point Lights: ");
  info.append(mrf::util::StringParsing::numericsToString(nbPointLights()));
  info.append("\t            *\n");

  info.append("* \t Number of Quad Lights: ");
  info.append(mrf::util::StringParsing::numericsToString(nbQuadLights()));
  info.append("\t            *\n");

  info.append("* \t Number of Sphere Lights: ");
  info.append(mrf::util::StringParsing::numericsToString(nbSphereLights()));
  info.append("\t            *\n");

  info.append("* \t Number of Materials: ");
  info.append(mrf::util::StringParsing::numericsToString(_umats.size()));
  info.append("\t                    *\n");

  info.append("*****************************************************\n");


  return info;
}


//-------------------------------------------------------------------------
// Intersectable Objects management
//-------------------------------------------------------------------------


inline void Scene::addShape(mrf::geom::Shape *a_shape)
{
  _shapes.push_back(a_shape);

  // TEST ME MORE
  if (_bbox.getRadius() < 1e-6)   // OK probably first thing in our scene that we add
  {
    _bbox = a_shape->bbox();
  }
  else
  {
    _bbox = a_shape->bbox().createUnion(_bbox);
  }

  uint shape_idx = _shapes.size() - 1;
  auto mesh      = dynamic_cast<mrf::geom::Mesh *>(a_shape->mesh().get());
  if (mesh)
  {
    int mesh_idx = 0;
    for (auto &mesh : _meshes_vector)
    {
      if (mesh.get() == a_shape->mesh().get()) break;
      mesh_idx++;
    }
    _meshes_instances.resize(_meshes.size());
    _meshes_instances[mesh_idx].push_back(shape_idx);
  }
}

inline mesh_map &Scene::meshes()
{
  return _meshes;
}

inline mesh_map const &Scene::meshes() const
{
  return _meshes;
}

inline std::shared_ptr<mrf::geom::Intersectable> Scene::mesh(std::string const &name)
{
  auto it = _meshes.find(name);
  if (it != _meshes.end()) return it->second;
  return nullptr;
}

inline std::vector<std::shared_ptr<mrf::geom::Intersectable>> &Scene::meshes_vector()
{
  return _meshes_vector;
}

inline std::vector<std::shared_ptr<mrf::geom::Intersectable>> const &Scene::meshes_vector() const
{
  return _meshes_vector;
}

inline std::vector<mrf::geom::Shape *> &Scene::shapes()
{
  return _shapes;
}

inline std::vector<mrf::geom::Shape *> const &Scene::shapes() const
{
  return _shapes;
}

inline std::vector<std::vector<uint>> &Scene::meshes_instances()
{
  return _meshes_instances;
}

inline std::vector<std::vector<uint>> const &Scene::meshes_instances() const
{
  return _meshes_instances;
}