/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Sensor class for rendering
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace rendering
{
/**
 * @brief      This class represents a rectangular Sensor
 *
 * The position of the sensor represents its upper left corner
 * Note that its default orientation is with the Y axis pointing down
 *
 * A sensor is defined by its position, orientation (stored as a mrf::math::Ref class)
 * its resolution (width and height in pixels) and its size (i.e., area of the sensor)
 *
 */
class MRF_CORE_EXPORT Sensor
{
private:
  /* resolution in pixels in x and y */
  mrf::math::Vec2sz _pixel_resolution;

  /* Real world sensor sizes in meter */
  mrf::math::Vec2d _sensor_size;

  /* The real-world size of one pixel in meters*/
  mrf::math::Vec2d _pixel_size;

  /* Referential i.e,. local frame of the Sensor */
  mrf::math::Ref<float> _ref;


  // Precomputed Values
  //
  /* The X axis, which length is equal to one pixel width  in  LOCAL Coordinates */
  mrf::math::Vec3f _pixel_x;

  /* The Y axis, which length is equal to one pixel height in LOCAL Coordinates */
  mrf::math::Vec3f _pixel_y;

  /* The upper Left Corner of the Sensor in WORLD Coordinates */
  mrf::math::Vec3f _w_upper_left_corner;


public:
  inline Sensor(
      mrf::math::Vec3f const & position         = mrf::math::Vec3f(0.0f, 0.0f, 0.0f),
      mrf::math::Vec3f const & x_axis           = mrf::math::Vec3f(1.0f, 0.0f, 0.0f),
      mrf::math::Vec3f const & y_axis           = mrf::math::Vec3f(0.0f, -1.0f, 0.0f),
      mrf::math::Vec3f const & z_axis           = mrf::math::Vec3f(0.0f, 0.0f, -1.0f),
      mrf::math::Vec2sz const &pixel_resolution = mrf::math::Vec2sz(512, 512),
      mrf::math::Vec2d const & sensor_size      = mrf::math::Vec2d(8.8e-3, 8.8e-3));

  inline ~Sensor();

  //Accessors
  inline mrf::math::Vec2d const & size() const;
  inline mrf::math::Vec2sz const &resolution() const;

  // Setters function
  inline void
  set(mrf::math::Vec3f const &position,
      mrf::math::Vec3f const &x_axis,
      mrf::math::Vec3f const &y_axis,
      mrf::math::Vec3f const &z_axis);

  inline void setPosition(mrf::math::Vec3f const &position);

  inline void setResolution(size_t width_in_pixel, size_t height_in_pixel);
  inline void setSize(double width_of_the_sensor, double height_of_the_sensor);
  inline void setSize(mrf::math::Vec2d const &new_size);

  inline void setResAndSize(
      unsigned int width_in_pixel,
      unsigned int height_in_pixel,
      double       width_of_the_sensor,
      double       height_of_the_sensor);

  inline void setResAndSize(mrf::math::Vec2u const &new_res, mrf::math::Vec2d const &new_size);

  /**
   * @brief      Returns the position of the upper left corner of a given pixel
   *
   * The returned value is in World Coordinates.
   *
   * @see pixelULC( unsigned int i, unsigned int j)
   *       for the upper left corner in local coordinates
   *
   * @param[in]  i     { row index of the pixel }
   * @param[in]  j     { column index of the pixel }
   *
   * @return     { The upper left corner 3D position in World Coordinates }
   */
  inline mrf::math::Vec3f wPixelULC(unsigned int i, unsigned int j) const;


  /**
   * @brief      Returns the position of the upper left corner of a given pixel
   *
   * The returned value is in Local Coordinates.
   *
   * @see wPixelULC( unsigned int i, unsigned int j) for the upper
   *       left corner in Wold coordinates
   *
   * @param[in]  i     { row index of the pixel }
   * @param[in]  j     { column index of the pixel }
   *
   * @return     { The upper left corner 3D position in Local Coordinates  }
   */
  inline mrf::math::Vec3f pixelULC(unsigned int i, unsigned int j) const;



  /**
   * Return the X axis,which length is equal to one pixel width
   */
  inline mrf::math::Vec3f pAxisX() const;

  /**
   * Return the Y-axis which length is equal to one pixel height
   */
  inline mrf::math::Vec3f pAxisY() const;


  /**
   * Returns in LOCAL Coordinates the Upper Left Corner of the Sensor
   * This is the Origin (0,0,0)
   */
  inline mrf::math::Vec3f upperLeftCorner() const;


  /**
   * Returns in World-Coordinates the Upper Left Corner of the Sensor
   */
  inline mrf::math::Vec3f wULC() const;


  /**
   * Equivalent of pAxisX() but the result is in WORLD coordinates
   */
  inline mrf::math::Vec3f wPixelAxisX() const;

  /**
   * Equivalent of pAxisY() but the result is in WORLD coordinates
   */
  inline mrf::math::Vec3f wPixelAxisY() const;


  /**
   * @brief      Return a copy of the 4x4 local to world transformation Matrix
   *
   * @return     The 4 by 4 Matrix representing the local to world transformation
   */
  inline mrf::math::Mat4f localToWorldMatrix() const;
};




Sensor::Sensor(
    mrf::math::Vec3f const & position,
    mrf::math::Vec3f const & x_axis,
    mrf::math::Vec3f const & y_axis,
    mrf::math::Vec3f const & z_axis,
    mrf::math::Vec2sz const &pixel_resolution,
    mrf::math::Vec2d const & sensor_size)
  : _pixel_resolution(pixel_resolution)
  , _sensor_size(sensor_size)
  , _pixel_size(_sensor_size / _pixel_resolution)
  , _ref(position)
{
  using namespace mrf::math;

  // Changing Extrinsic parameters
  _ref.setOrientation(x_axis, y_axis, z_axis);

  _w_upper_left_corner = _ref.getMatrixFrom() * Vec3f(0.0f, 0.0f, 0.0f);

  //Changing Intrinsics parameters
  _pixel_x = Vec3f(1.0f, 0.0f, 0.0f) * static_cast<float>(_pixel_size.x());
  _pixel_y = Vec3f(0.0f, 1.0f, 0.0f) * static_cast<float>(_pixel_size.y());
}

Sensor::~Sensor() {}

mrf::math::Vec2d const &Sensor::size() const
{
  return _sensor_size;
}

mrf::math::Vec2sz const &Sensor::resolution() const
{
  return _pixel_resolution;
}


void Sensor::set(
    mrf::math::Vec3f const &position,
    mrf::math::Vec3f const &x_axis,
    mrf::math::Vec3f const &y_axis,
    mrf::math::Vec3f const &z_axis)
{
  using namespace mrf::math;

  _ref.setPosition(position);
  _ref.setOrientation(x_axis, y_axis, z_axis);

  _w_upper_left_corner = _ref.getMatrixFrom() * Vec3f(0.0f, 0.0f, 0.0f);
}

inline void Sensor::setPosition(mrf::math::Vec3f const &position)
{
  using namespace mrf::math;

  _ref.setPosition(position);
  _w_upper_left_corner = _ref.getMatrixFrom() * Vec3f(0.0f, 0.0f, 0.0f);
}


void Sensor::setResolution(size_t width_in_pixel, size_t height_in_pixel)
{
  using namespace mrf::math;

  _pixel_resolution.set(width_in_pixel, height_in_pixel);

  _pixel_size = _sensor_size / _pixel_resolution;

  _pixel_x = Vec3f(1.0f, 0.0f, 0.0f) * static_cast<float>(_pixel_size.x());
  _pixel_y = Vec3f(0.0f, 1.0f, 0.0f) * static_cast<float>(_pixel_size.y());
}

void Sensor::setSize(double width_of_the_sensor, double height_of_the_sensor)
{
  using namespace mrf::math;

  _sensor_size[0] = width_of_the_sensor;
  _sensor_size[1] = height_of_the_sensor;

  _pixel_size = _sensor_size / _pixel_resolution;

  _pixel_x = Vec3f(1.0f, 0.0f, 0.0f) * static_cast<float>(_pixel_size.x());
  _pixel_y = Vec3f(0.0f, 1.0f, 0.0f) * static_cast<float>(_pixel_size.y());
}

inline void Sensor::setSize(mrf::math::Vec2d const &new_size)
{
  setSize(new_size.x(), new_size.y());
}

inline void Sensor::setResAndSize(
    unsigned int width_in_pixel,
    unsigned int height_in_pixel,
    double       width_of_the_sensor,
    double       height_of_the_sensor)
{
  //Not the fastest way to do it but makes code maintenance easier
  setResolution(width_in_pixel, height_in_pixel);
  setSize(width_of_the_sensor, height_of_the_sensor);
}


inline void Sensor::setResAndSize(mrf::math::Vec2u const &new_res, mrf::math::Vec2d const &new_size)
{
  //Not the fastest way to do it but makes code maintenance easier
  setResolution(new_res.x(), new_res.y());
  setSize(new_size.x(), new_size.y());
}


mrf::math::Vec3f Sensor::pixelULC(unsigned int i, unsigned int j) const
{
  using namespace mrf::math;

  //Local Coordinates of a Pixel
  //Vec3f const position( _pixelSize.x() * i, _pixelSize.y() * j, 0.0 );
  Vec3f const position(static_cast<float>(_pixel_size.y() * j), static_cast<float>(_pixel_size.x() * i), 0.0f);

  return position;
}


mrf::math::Vec3f Sensor::wPixelULC(unsigned int i, unsigned int j) const
{
  using namespace mrf::math;

  //Local Coordinate of a Pixel
  Vec3f const position(static_cast<float>(_pixel_size.x() * i), static_cast<float>(_pixel_size.y() * j), 0.0f);

  return _ref.getMatrixFrom() * position;
}


mrf::math::Vec3f Sensor::pAxisX() const
{
  return _pixel_x;
}

mrf::math::Vec3f Sensor::pAxisY() const
{
  return _pixel_y;
}

mrf::math::Vec3f Sensor::wPixelAxisX() const
{
  return _ref.getMatrixFrom() * _pixel_x;
}

mrf::math::Vec3f Sensor::wPixelAxisY() const
{
  return _ref.getMatrixFrom() * _pixel_y;
}


mrf::math::Vec3f Sensor::wULC() const
{
  return _w_upper_left_corner;
}



mrf::math::Mat4f Sensor::localToWorldMatrix() const
{
  return _ref.getMatrixFrom();
}

//----------------------------------------------------------------------------------------------
inline std::ostream &operator<<(std::ostream &os, Sensor const &sensor)
{
  os << " Sensor {  In World Coordinates:  Upper Left Corner" << sensor.wULC() << std::endl
     << "  Pixel Vector X: " << sensor.wPixelAxisX() << "  Pixel Vector Y: " << sensor.wPixelAxisY()
     << "  Size: " << sensor.size() << "  Resolution: " << sensor.resolution() << "  } " << std::endl;

  return os;
}
}   // namespace rendering
}   // namespace mrf