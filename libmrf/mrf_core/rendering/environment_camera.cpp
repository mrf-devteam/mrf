/*
 *
 * author : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 **/

#include <mrf_core/rendering/environment_camera.hpp>

namespace mrf
{
namespace rendering
{
using namespace mrf::math;

Environment::Environment(
    mrf::math::Vec3f const &pos,
    mrf::math::Vec3f const &lookAt,
    mrf::math::Vec3f const &up,
    float                   theta,
    float                   phi,
    float                   vpd,
    float                   fpd,
    size_t                  image_width_in_pixels,
    size_t                  image_height_in_pixels,
    unsigned int            nb_samples_per_pixels,
    std::string             id_name)
  : Camera(pos, lookAt, up, vpd, fpd, 1., image_width_in_pixels, image_height_in_pixels, nb_samples_per_pixels, id_name)
  , _theta(theta)
  , _phi(phi)
{}

mrf::lighting::Ray Environment::generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l) const
{
  return mrf::lighting::Ray();
}


void Environment::generateCameraRay(
    unsigned int        i,
    unsigned int        j,
    unsigned int        k,
    unsigned int        l,
    mrf::lighting::Ray &ray) const
{}

mrf::math::Mat4f Environment::projMat() const
{
  return mrf::math::Mat4f();
}

mrf::math::Mat4f Environment::viewMat() const
{
  return mrf::math::Mat4f();
}

void Environment::theta(float ntheta)
{
  _theta = ntheta;
}

float Environment::theta() const
{
  return _theta;
}

void Environment::phi(float nphi)
{
  _phi = nphi;
}

float Environment::phi() const
{
  return _phi;
}

void Environment::updateInnerData() {}

}   // namespace rendering

}   // namespace mrf