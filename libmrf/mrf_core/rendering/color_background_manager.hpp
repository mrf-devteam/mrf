/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/color/color.hpp>
#include <mrf_core/radiometry/spectrum.hpp>
#include <mrf_core/materials/materials.hpp>
#include <mrf_core/rendering/background_manager.hpp>



namespace mrf
{
namespace rendering
{
class MRF_CORE_EXPORT ColorBckgManager: public BckgManager
{
protected:
  mrf::materials::COLOR _background_color;

public:
  ColorBckgManager(mrf::materials::COLOR const a_color = mrf::materials::COLOR(0.0f));

  virtual mrf::materials::COLOR colorFromDirection(mrf::math::Vec3f const &dir) const;
};
}   // namespace rendering
}   // namespace mrf
