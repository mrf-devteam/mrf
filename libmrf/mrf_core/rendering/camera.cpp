/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * modifications : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 **/
#include <mrf_core/rendering/camera.hpp>

#include <vector>

namespace mrf
{
namespace rendering
{
Camera::Camera(
    mrf::math::Vec3f const &pos,
    mrf::math::Vec3f const &lookAt,
    mrf::math::Vec3f const &up,
    float                   vpd,
    float                   fpd,
    float                   top,
    size_t                  image_width_in_pixels,
    size_t                  image_height_in_pixels,
    unsigned int            nb_samples_per_pixels,
    std::string             id_name)
  : _position(pos)
  , _look_at(lookAt)
  , _up(up)
  , _aspect_ratio(static_cast<float>(image_width_in_pixels) / static_cast<float>(image_height_in_pixels))
  , _view_plane_distance(vpd)
  , _far_plane_distance(fpd)
  , _top(top)   // _view_plane_distance * std::tan(mrf::math::Math::toRadian(_fovy) * 0.5f)
  , _right(_aspect_ratio * _top)
  , _normalized_optical_axis((_look_at - _position).normal())
  , _right_axis(_normalized_optical_axis.cross(_up))
  , _sensor(
        _position + _normalized_optical_axis * _view_plane_distance + _up * _top - (_right_axis * _right),
        _right_axis,
        -_up,
        _normalized_optical_axis,
        mrf::math::Vec2sz(image_width_in_pixels, image_height_in_pixels),
        mrf::math::Vec2d(2.f * _right, 2.f * _top))
  , _sensor_sampler(&_sensor, nb_samples_per_pixels)
  , _id_name(id_name)
  , _rotation_matrix(mrf::math::Mat3f::identity())
{
  //updateInnerData();
  //updateRotMat();
}

/*
inline Camera::Camera(
    mrf::geom::AABBox const &bbox,
    float                    top,
    size_t                   image_width_in_pixels,
    size_t                   image_height_in_pixels,
    unsigned int             nb_samples_per_pixels,
    std::string              id_name)

  : _position(bbox.center() - mrf::math::Vec3f(0.f, 0.f, bbox.getRadius() / tan(mrf::math::Math::toRadian(fov * 0.5f))))
  , _look_at(bbox.center())
  , _up(mrf::math::Vec3f(0.f, 1.f, 0.f))
  , _aspect_ratio((bbox.max().x() - bbox.min().x()) / (bbox.max().y() - bbox.min().y()))
  , _view_plane_distance(std::max(MIN_ZNEAR, (_position - _look_at).length() - (bbox.getRadius() + 10.f * EPSILON)))
  , _far_plane_distance((_position - _look_at).length() + (bbox.getRadius() + 10.f * EPSILON))
  , _top(top)
  , _right(_aspect_ratio * _top)
  , _normalized_optical_axis((_look_at - _position).normal())
  , _right_axis(_normalized_optical_axis.cross(_up))
  , _sensor(
        _position + _normalized_optical_axis * _view_plane_distance + _up * _top - (_right_axis * _right),
        _right_axis,
        -_up,
        _normalized_optical_axis,
        mrf::math::Vec2sz(image_width_in_pixels, image_height_in_pixels),
        mrf::math::Vec2d(2.0f * _right, 2.0f * _top))
  , _sensor_sampler(&_sensor, nb_samples_per_pixels)
  , _id_name(id_name)
{
  updateInnerData();
  updateRotMat();
}
*/

mrf::math::Vec3f const &Camera::position() const
{
  return _position;
}

mrf::math::Vec3f const &Camera::lookAt() const
{
  return _look_at;
}

mrf::math::Vec3f const &Camera::up() const
{
  return _up;
}

mrf::math::Vec3f &Camera::position()
{
  return _position;
}

mrf::math::Vec3f &Camera::lookAt()
{
  return _look_at;
}

mrf::math::Vec3f &Camera::up()
{
  return _up;
}

float Camera::viewPlaneDistance() const
{
  return _view_plane_distance;
}

float Camera::farPlaneDistance() const
{
  return _far_plane_distance;
}

float Camera::zNear() const
{
  return _view_plane_distance;
}

float Camera::zFar() const
{
  return _far_plane_distance;
}

float Camera::left() const
{
  return _left;
}

float Camera::right() const
{
  return _right;
}

float Camera::bottom() const
{
  return _bottom;
}

float Camera::top() const
{
  return _top;
}

float Camera::aspectRatio() const
{
  return _aspect_ratio;
}

float &Camera::aspectRatio()
{
  return _aspect_ratio;
}

mrf::math::Mat3f const &Camera::rotMat() const
{
  return _rotation_matrix;
}

Sensor const &Camera::sensor() const
{
  return _sensor;
}

Sensor &Camera::sensor()
{
  return _sensor;
}

SensorSampler const &Camera::sensorSampler() const
{
  return _sensor_sampler;
}

SensorSampler &Camera::sensorSampler()
{
  return _sensor_sampler;
}

void Camera::setSensorResolution(unsigned int width, unsigned int height)
{
  _sensor.setResolution(width, height);
  _sensor_sampler.updateSubPixelVectors();
}

void Camera::setSensorResolution(mrf::math::Vec2u const &new_resolution)
{
  setSensorResolution(new_resolution.x(), new_resolution.y());
}


void Camera::position(mrf::math::Vec3f const &npos)
{
  _position = npos;
  //updateRotMat();
}

void Camera::lookAt(mrf::math::Vec3f const &nlookat)
{
  _look_at = nlookat;
  //updateRotMat();
}

void Camera::up(mrf::math::Vec3f const &nup)
{
  _up = nup;
  //updateRotMat();
}

void Camera::viewPlaneDistance(float nvpd)
{
  _view_plane_distance = nvpd;
  updateInnerData();
}

void Camera::farPlaneDistance(float nfpd)
{
  _far_plane_distance = nfpd;
}

void Camera::aspectRatio(float aspectRatio)
{
  _aspect_ratio = aspectRatio;
}

void Camera::rotMat(mrf::math::Mat3f nrotMat)
{
  _rotation_matrix = nrotMat;
}

//Useful functions
mrf::math::Vec3f Camera::dir2LookAt() const
{
  return _look_at - _position;
}

void Camera::translate(mrf::math::Vec3f const &t)
{
  _position += t;
}

mrf::math::Vec3f Camera::rightAxis() const
{
  return dir2LookAt().cross(_up).normalize();
}

void Camera::rotate(mrf::math::Vec3f const &axis, float angle)
{
  mrf::math::Quatf q;
  q.fromAxisAngle(axis, angle);

  rotate(q);
}

void Camera::rotate(mrf::math::Quatf const &q)
{
  mrf::math::Mat4f const m   = q.getMatrix();
  mrf::math::Vec3f       dir = dir2LookAt();

  _up      = m * _up;
  dir      = m * dir;
  _look_at = _position + dir;
}

void Camera::turnRight(float angle, bool useUpVector)
{
  turnLeft(-angle, useUpVector);
}

void Camera::turnLeft(float angle, bool useUpVector)
{
  mrf::math::Vec3f up = useUpVector ? _up : mrf::math::Vec3f(0.f, 1.f, 0.f);
  rotate(up, angle);
}

void Camera::lookUp(float angle)
{
  rotate(rightAxis(), angle);
}

void Camera::lookDown(float angle)
{
  rotate(rightAxis(), -angle);
}

float Camera::depthNormalized(float t) const
{
  float const z_range = _far_plane_distance - _view_plane_distance;

  return (t - _view_plane_distance) / z_range;
}

void Camera::updateClippingPlanes(mrf::geom::AABBox const &newBBox)
{
  mrf::math::Vec3f const center = newBBox.center();
  mrf::math::Vec3f       v(center - _position);
  float const            size  = newBBox.getRadius() + 10.f * EPSILON;
  float const            zNear = std::max(MIN_ZNEAR, v.length() - size);
  float const            zFar  = v.length() + size;
  mrf::math::Vec3f       dir   = dir2LookAt().normal();
  v.normalEq();
  float const angle = acosf(dir[0] * v[0] + dir[2] * v[2]);
  float const z1    = zNear * sinf(angle);
  float const d     = sqrtf(fabsf(zNear * zNear - z1 * z1));

  _view_plane_distance = std::max(MIN_ZNEAR, d);
  _far_plane_distance  = zFar;
}

void Camera::updateClippingPlanes(float radius)
{
  float const z = (_position - _look_at).length();
  radius += 10.f * EPSILON;

  _view_plane_distance = std::max(MIN_ZNEAR, z - radius);
  _far_plane_distance  = z + radius;
  updateInnerData();
}



void Camera::updateRotMat()
{
  mrf::math::Vec3f const inv_view = (_position - _look_at).normalize();
  _rotation_matrix.setRow(0, _up.cross(inv_view));
  _rotation_matrix.setRow(1, _up);
  _rotation_matrix.setRow(2, inv_view);
}


std::string const &Camera::idName() const
{
  return _id_name;
}


}   // namespace rendering
}   // namespace mrf