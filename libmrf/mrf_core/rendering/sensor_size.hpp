/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2015
 *
 **/

#pragma once


#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>

namespace mrf
{
namespace rendering
{
/**
 * @brief      Utility class to store the different sizes in meter of the different sensor type
 */
class MRF_CORE_EXPORT SensorSize
{
public:
  static mrf::math::Vec2d const CANON_APS_C;

  static mrf::math::Vec2d const FOUR_THIRD;
  static mrf::math::Vec2d const APS_C;
  static mrf::math::Vec2d const APS_H;
  static mrf::math::Vec2d const FULL_FRAME_35MM;

  static mrf::math::Vec2d const STD_8MM_FRAME;
  static mrf::math::Vec2d const STD_16MM_FRAME;
  static mrf::math::Vec2d const STD_65MM_FRAME;
};

//Initialization

//Popular Camera Sensor size
mrf::math::Vec2d const SensorSize::FOUR_THIRD = mrf::math::Vec2d(0.01730, 0.013f);

mrf::math::Vec2d const SensorSize::CANON_APS_C = mrf::math::Vec2d(0.02230, 0.01490);
mrf::math::Vec2d const SensorSize::APS_C       = mrf::math::Vec2d(0.0236, 0.01560);
mrf::math::Vec2d const SensorSize::APS_H       = mrf::math::Vec2d(0.02790, 0.01860);


mrf::math::Vec2d const SensorSize::FULL_FRAME_35MM = mrf::math::Vec2d(0.0360, 0.0240);

//Film Frames
mrf::math::Vec2d const SensorSize::STD_8MM_FRAME  = mrf::math::Vec2d(0.0048, 0.0035);
mrf::math::Vec2d const SensorSize::STD_16MM_FRAME = mrf::math::Vec2d(0.01026, 0.00749);
mrf::math::Vec2d const SensorSize::STD_65MM_FRAME = mrf::math::Vec2d(0.05248, 0.02301);


}   // namespace rendering

}   // namespace mrf