/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * modifications : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/vec3.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/rendering/sensor.hpp>
#include <mrf_core/rendering/sensor_sampler.hpp>
#include <mrf_core/geometry/aabbox.hpp>
#include <mrf_core/lighting/ray.hpp>

#include <algorithm>
#include <vector>
namespace mrf
{
namespace rendering
{
enum class CameraType
{
  Pinhole,
  ThinLens,
  Orthographic,
  Gonio,
  Environment,
  Size
};

static std::string CameraType2string(const CameraType &type)
{
  switch (type)
  {
    case CameraType::Pinhole:
      return "pinhole";
    case CameraType::ThinLens:
      return "thinlens";
    case CameraType::Orthographic:
      return "orthographic";
    case CameraType::Gonio:
      return "gonio";
    case CameraType::Environment:
      return "environment";
    default:
      return "";
  }
}

static CameraType string2CameraType(const std::string &str)
{
  if (str == "pinhole") return CameraType::Pinhole;
  if (str == "thinlens") return CameraType::ThinLens;
  if (str == "orthographic") return CameraType::Orthographic;
  if (str == "gonio") return CameraType::Gonio;
  if (str == "environment") return CameraType::Environment;
  return CameraType::Size;
}

static float const MIN_ZNEAR = 0.01f;
static float const MAX_ZFAR  = 3000.0f;

class MRF_CORE_EXPORT Camera
{
public:
  /**
   * @brief      Construct a pinhole Camera from the given parameter
   *             View Plane Distance is to MIN_ZNEAR
   *             Far Plane Distance to to  3000
   *
   * @param      pos                     Position of the Camera
   * @param      lookAt                  Look At of the Camera
   * @param      up                      Up Vector
   * @param[in]  vpd                     The distance between the position of the camera and the sensor
   * @param[in]  image_width_in_pixels   number of pixels for the image width
   * @param[in]  image_height_in_pixels  number of pixels for the image height
   * @param[in]  nb_samples_per_pixels   number of samples per pixel
   */
  /*AMBIGOUS CALL WITH THE DECLARATION BELOW
  inline Camera(mrf::math::Vec3f const & pos,
               mrf::math::Vec3f const & lookAt,
               mrf::math::Vec3f const & up,
               float vpd,
               unsigned int image_width_in_pixels,
               unsigned int image_height_in_pixels,
               unsigned int nb_samples_per_pixels);
               */
  Camera(
      mrf::math::Vec3f const &pos                    = mrf::math::Vec3f(0.0f, 0.0f, -10.0f),
      mrf::math::Vec3f const &lookAt                 = mrf::math::Vec3f(0.0f, 0.0f, 0.0f),
      mrf::math::Vec3f const &up                     = mrf::math::Vec3f(0.0f, 1.0f, 0.0f),
      float                   vpd                    = MIN_ZNEAR,
      float                   fpd                    = MAX_ZFAR,
      float                   top                    = 1.f,
      size_t                  image_width_in_pixels  = 512,
      size_t                  image_height_in_pixels = 512,
      unsigned int            nb_samples_per_pixels  = 4,
      std::string             id_name                = "NO_ID_NAME_SPECIFIED");

  /**
   * @brief Create a camera to see the entire bbox with a field
   *        of view of fov.
   *
   * Warning: If you want to use gluPerspective:
   * In general the aspect ratio should match the aspect ratio of
   * the associated viewport, here it matches the bbox ratio.
   * */
  /*
  Camera(
      mrf::geom::AABBox const &bbox,
      float                    top = 1.f,
      size_t                   image_width_in_pixels  = 512,
      size_t                   image_height_in_pixels = 512,
      unsigned int             nb_samples_per_pixels  = 4,
      std::string              id_name                = "NO_ID_NAME_SPECIFIED");
  */

  virtual ~Camera() {}

  mrf::math::Vec3f const &position() const;
  mrf::math::Vec3f const &lookAt() const;
  mrf::math::Vec3f const &up() const;
  mrf::math::Vec3f &      position();
  mrf::math::Vec3f &      lookAt();
  mrf::math::Vec3f &      up();
  float                   viewPlaneDistance() const;
  float                   farPlaneDistance() const;
  /** Alias for viewPlaneDistance */
  float zNear() const;
  /** Alias for farPlaneDistance */
  float                   zFar() const;
  float                   left() const;
  float                   right() const;
  float                   bottom() const;
  float                   top() const;
  float                   aspectRatio() const;
  float &                 aspectRatio();
  mrf::math::Mat3f const &rotMat() const;
  SensorSampler const &   sensorSampler() const;
  //Be carefull with this one
  SensorSampler &sensorSampler();
  Sensor const & sensor() const;
  Sensor &       sensor();
  /**
   * @brief      Update the resolution of the sensor in pixels
   *             Note: The SensorSampler is also updated
   *
   * @param[in]  width   { the new width in pixel }
   * @param[in]  height  { the new height in pixel }
   */
  void setSensorResolution(unsigned int width, unsigned int height);

  /**
   * @brief      Update the resolution of the sensor in pixels
   *             Note: The SensorSampler is also updated
   *
   * @param      new_sensor_resolution  { a vector where the first (resp. second) component represents the new width (resp. height) in pixels. }
   */
  void setSensorResolution(mrf::math::Vec2u const &new_sensor_resolution);

  //Setters
  void position(mrf::math::Vec3f const &npos);
  void lookAt(mrf::math::Vec3f const &nlookat);
  void up(mrf::math::Vec3f const &nup);
  void viewPlaneDistance(float nvpd);
  void farPlaneDistance(float nfpd);
  void aspectRatio(float aspectRatio);
  void rotMat(mrf::math::Mat3f nrotMat);

  //Useful function
  mrf::math::Vec3f dir2LookAt() const;
  void             translate(mrf::math::Vec3f const &t);
  mrf::math::Vec3f rightAxis() const;
  void             rotate(mrf::math::Quatf const &q);
  void             rotate(mrf::math::Vec3f const &axis, float angle);
  void             turnRight(float angle, bool useUpVector = false);
  void             turnLeft(float angle, bool useUpVector = false);
  void             lookUp(float angle);
  void             lookDown(float angle);

  std::string const &idName() const;


  /**
   * Return the normalized depth
   **/
  float depthNormalized(float t) const;

  /**
   * @brief
   * Make sure _position and _lookAt are corect !
   * */
  void updateClippingPlanes(mrf::geom::AABBox const &newBBox);

  /**
   * Make sure _position and _lookAt are corect !
   * @param radius is The radius of the bounding sphere
   *               (assuming, _lookAt is the center)
   * */
  void updateClippingPlanes(float radius);


  virtual mrf::math::Mat4f viewMat() const = 0;
  virtual mrf::math::Mat4f projMat() const = 0;

  virtual mrf::lighting::Ray
  generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l) const = 0;
  /**
  * @brief      Returns a Ray given the pixel, sub-pixel and lens indexes

  * @param[in]  i               The pixel row index
  * @param[in]  j               The pixel column index
  * @param[in]  k               The sub-pixel row index
  * @param[in]  l               The sub-pixel column index
  * @param[in]  lens_sample_nb  The lens sample index
  *
  * @return     the Ray
  */
  /* Not implemented
  virtual mrf::lighting::Ray
  generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l, unsigned int lens_sample_nb) const;
  */

  /**
   * @brief      Returns the number of samples used to sample the lens
   *             For Pinhole camera this value is always equal to 1
   *
   * @return     The number of samples for the lens
   */
  virtual unsigned int numberOfLensSamples() const { return 1; };

  virtual void
  generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l, mrf::lighting::Ray &ray) const = 0;


  virtual CameraType getType() const = 0;
  virtual Camera *   copy() const    = 0;

  virtual void updateInnerData() {};

private:
  void updateRotMat();

protected:
  mrf::math::Vec3f _position;
  mrf::math::Vec3f _look_at;
  mrf::math::Vec3f _up;   // Y axis of the Camera Frame
  float            _aspect_ratio;
  float            _view_plane_distance;
  float            _far_plane_distance;
  //Left, Right, Top, Bottom as define by gluPerspective
  float            _left, _top, _right, _bottom;
  mrf::math::Mat3f _rotation_matrix;
  // Normalized Optical Axis == (lookAt - _position).normalized()
  mrf::math::Vec3f _normalized_optical_axis;
  mrf::math::Vec3f _right_axis;   // X axis of the Camera Frame

  mrf::rendering::Sensor        _sensor;
  mrf::rendering::SensorSampler _sensor_sampler;

  std::string _id_name;   //the id coming from the .mcf file stored as string
};

//#include <mrf_core/rendering/camera.inl>

}   // namespace rendering
}   // namespace mrf