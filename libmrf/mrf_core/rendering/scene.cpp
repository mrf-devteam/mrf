/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/
#include <mrf_core/rendering/scene.hpp>


namespace mrf
{
namespace rendering
{
Scene::Scene(): _nbPointLights(0), _nbDirectionalLights(0), _nbQuadLights(0), _nbSphereLights(0), _bckgmng(nullptr) {}

Scene::~Scene()
{
  //Materials
  for (unsigned int i = 0; i < _umats.size(); i++)
  {
    delete _umats[i];
  }
  _umats.clear();

  //Lights
  removeAllLights();

  //Meshes
  /* for (auto it = _meshes.begin(); it != _meshes.end(); ++it)
   {
     delete it->second;
   }
   */
  _meshes.clear();

  //Meshes
  for (unsigned int i = 0; i < _shapes.size(); i++)
  {
    delete _shapes[i];
  }
  _shapes.clear();
  _meshes_instances.clear();
  _meshes_vector.clear();

  //Background manager
  if (_bckgmng)
  {
    delete _bckgmng;
    _bckgmng = nullptr;
  }
}

mrf::rendering::mesh_map::iterator
Scene::Scene::addIntersectable(std::shared_ptr<mrf::geom::Intersectable> &an_intersectable, std::string &mesh_name)
{
  mrf::rendering::mesh_map::iterator result_position;

  if (mesh_name.size() == 0)
  {
    mesh_name = "MRF_mesh_" + std::to_string(_meshes.size());
  }

  auto res = _meshes.insert(std::make_pair(mesh_name, an_intersectable));
  if (res.second)
  {
    _meshes_vector.push_back(an_intersectable);

    //we just inserted the first intersectable
    if (_meshes.size() == 1)
    {
      _bbox = an_intersectable->bbox();
    }
    else
    {
      _bbox = an_intersectable->bbox().createUnion(_bbox);
    }

    return res.first;
  }

  return _meshes.end();
}


void Scene::recomputeAABBox()
{
  assert(0);
  //TODO: CODE ME

  //_bbox = mrf::geom::MRF_AABBox();
  //TODO
  //std::vector<mrf::geom::Intersectable*>::iterator it;
  //for (it = _geomObjects.begin(); it != _geomObjects.end(); ++it)
  {
    //_bbox = (*it)->bbox().createUnion( _bbox );
  }
}

void verifyBRDF(
    mrf::rendering::Scene const &a_scene,
    unsigned int                 number_of_view_directions,
    unsigned int                 number_of_samples_to_compute_integral)
{
  using namespace std;
  for (unsigned int i = 0; i < a_scene.umatSize(); i++)
  {
    cout << "[DEBUG] Checking " << a_scene.umaterial(i).name() << endl;

    vector<float>                         view_angles;
    vector<mrf::materials::RADIANCE_TYPE> hemi_refl = a_scene.umaterial(i).hemiReflectivity(
        number_of_view_directions,
        view_angles,
        number_of_samples_to_compute_integral);

    for (unsigned int k = 0; k < hemi_refl.size(); k++)
    {
#ifdef MRF_RENDERING_MODE_SPECTRAL
      cout << "[WARN] Spectral mode does not check conservation of energy ! " << endl;
#else
      if (hemi_refl[k].x() > 1.0 || hemi_refl[k].y() > 1.0 || hemi_refl[k].z() > 1.0)
      {
        cout << "[WARN] This Material does not conserve energy ! " << view_angles[k] << hemi_refl[k] << endl;
      }
#endif
    }
  }
}


}   // namespace rendering
}   // namespace mrf