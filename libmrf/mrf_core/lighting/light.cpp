#include <mrf_core/lighting/light.hpp>

namespace mrf
{
namespace lighting
{
using namespace materials;

Light::Light(ILight *geometry, Emittance *emit, unsigned int emittance_id, LIGHT_TYPE const type)
  : _emittance_id(emittance_id)
  , _geometry(geometry)
  , _emit_mat(emit)
  , _luminancePower(_emit_mat->radiosityEmitted().luminance() * _geometry->surfaceArea())
  , _type(type)
{}

Light::~Light()
{
  /**
   *we do not delete the geometry here, the scene is responsible for that
   *\todo use smart pointer
   **/
  //delete _geometry;
}
}   // namespace lighting
}   // namespace mrf
