/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/


#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//MRF
#include <mrf_core/geometry/aabbox.hpp>
#include <mrf_core/materials/umat.hpp>
#include <mrf_core/materials/emittance.hpp>
#include <mrf_core/lighting/i_light.hpp>
#include <mrf_core/lighting/particle.hpp>
#include <mrf_core/sampling/random_generator.hpp>
#include <mrf_core/geometry/local_frame.hpp>

namespace mrf
{
namespace lighting
{
/**
 *
 * Mother Abstract Class of all lights
 * A light is the combinastion of an ILight Object
 * describing the geometruc surface properties
 * whilie the UMat describes the emissivity properties
 *
 **/
class MRF_CORE_EXPORT Light
{
public:
  enum LIGHT_TYPE
  {
    POINT_LIGHT  = 0,
    DIR_LIGHT    = 1,
    QUAD_LIGHT   = 2,
    ENV_LIGHT    = 3,
    SPHERE_LIGHT = 4
  };

protected:
  //mrf::geom::AABBox _bbox;
  unsigned int _emittance_id;   //The index of the Emitttance Material
  //for this light

  ILight *                   _geometry;
  mrf::materials::Emittance *_emit_mat;

  float                      _luminancePower;   //This is monochromatic power
  mrf::materials::POWER_TYPE _coloredPower;     //This the chromatic or colored power

  LIGHT_TYPE _type;

public:
  Light(ILight *geometry, mrf::materials::Emittance *umat, unsigned int emittance_id, LIGHT_TYPE const type);
  virtual ~Light();

  inline mrf::geom::MRF_AABBox &bbox();

  inline LIGHT_TYPE             type() const;
  inline void                   type(LIGHT_TYPE const a_type);
  inline mrf::math::Vec3f       center() const;
  inline mrf::lighting::ILight *geometry();


  /**
   * Returns the total emissive power of this light in Watt (W)
   **/
  //          virtual float emissivePower( mrf::rendering::Scene const & scene ) const = 0;

  //THE NEW SYSTEM
  inline mrf::materials::Emittance const &emit() const;

  inline mrf::materials::Emittance *emit_pointer();

  inline unsigned int materialIndex() const;


  /**
   * Returns the total power of this light source in  Watt (W)
   *
   **/
  inline float totalPower() const;

  /**
   * Generate a Photon/Particle according to this light source power
   * and direction emissivity
   **/
  inline Particle emitParticle() const;

  /**
   * Returns a position sample of the light
   * given the two independent random variables
   *
   * @see ILight interface
   **/
  inline float surfaceSampling(float e1, float e2, mrf::geom::LocalFrame &light_local_frame) const;

  /**
   * @see ILight interface
   **/
  inline float lightGeometricFactor(mrf::math::Vec3f const &light_vector) const;

  /**
   * @see ILight interface
   **/
  inline mrf::math::Vec3f lightVector(
      mrf::math::Vec3f const &point_shaded,
      mrf::math::Vec3f const &light_position,
      float &                 distance_point_light,
      float &                 square_distance_point_light) const;

  /**
   * Returns the normal of the light at the given position
   **/
  //          inline mrf::math::Vec3f const & surfaceNormal( mrf::math::Vec3f const & position ) const;



protected:
  //          inline void updatePowerValues();
};

inline mrf::geom::MRF_AABBox &Light::bbox()
{
  return _geometry->bbox();
}

inline mrf::math::Vec3f Light::center() const
{
  return _geometry->center();
}

inline mrf::lighting::ILight *Light::geometry()
{
  return _geometry;
}

inline Light::LIGHT_TYPE Light::type() const
{
  return _type;
}

inline void Light::type(Light::LIGHT_TYPE const a_type)
{
  _type = a_type;
}




//-------------------------------------------------------------------------------------------------
inline mrf::materials::Emittance const &Light::emit() const
{
  return *_emit_mat;
}

inline mrf::materials::Emittance *Light::emit_pointer()
{
  return _emit_mat;
}

inline unsigned int Light::materialIndex() const
{
  return _emittance_id;
}



inline float Light::totalPower() const
{
  //FIX ME !!!
  return _luminancePower;
  //return 0.0f; //( _umat->radiosityEmitted() * _geometry->surfaceArea() );
}



inline Particle Light::emitParticle() const
{
  float e1 = mrf::sampling::RandomGenerator::Instance().getFloat();
  float e2 = mrf::sampling::RandomGenerator::Instance().getFloat();

  float e3 = mrf::sampling::RandomGenerator::Instance().getFloat();
  float e4 = mrf::sampling::RandomGenerator::Instance().getFloat();


  //mrf::math::Vec3f particle_position;
  mrf::geom::LocalFrame light_local_frame;
  float                 proba_geom = _geometry->surfaceSampling(e1, e2, light_local_frame);


  //#ifdef DEBUG
  //          std::cout << __FILE__ << " " << __LINE__ << std::endl ;
  //          std::cout << "  proba_geom = " << proba_geom << std::endl ;
  //#endif

  mrf::math::Vec3f               particle_direction;
  mrf::materials::RADIOSITY_TYPE particle_radiosity
      = _emit_mat->particleEmission(light_local_frame, e3, e4, particle_direction);

  mrf::materials::POWER_TYPE particle_power = particle_radiosity / proba_geom;

  //          std::cout << __FILE__ << " " << __LINE__ << std::endl ;
  //          std::cout << "  particle_power = " << particle_power << std::endl ;



  return Particle(
      particle_power,
      light_local_frame.position() + light_local_frame.normal() * EPSILON,
      particle_direction);
}


//       inline void
//       Light::updatePowerValues()
//       {
//          _luminancePower = _geometry->surfaceArea() * _umat->radiosityEmitted().power() ;
//          _coloredPower   =  _umat->radiosityEmitted() * _geometry->surfaceArea();
//       }


inline float Light::surfaceSampling(float e1, float e2, mrf::geom::LocalFrame &light_local_frame) const
{
  float proba_geom = _geometry->surfaceSampling(e1, e2, light_local_frame);
  return proba_geom;
}

//       inline mrf::math::Vec3f const &
//       Light::surfaceNormal( mrf::math::Vec3f const & position ) const
//       {
//          //TODO : FIXE ME. Normal of the surface may vary
//          return _geometry->localFrame().normal();
//       }

inline float Light::lightGeometricFactor(mrf::math::Vec3f const &light_vector) const
{
  return _geometry->lightGeometricFactor(light_vector);
}

inline mrf::math::Vec3f Light::lightVector(
    mrf::math::Vec3f const &point_shaded,
    mrf::math::Vec3f const &light_position,
    float &                 distance_point_light,
    float &                 square_distance_point_light) const
{
  return _geometry->lightVector(point_shaded, light_position, distance_point_light, square_distance_point_light);
}
}   // namespace lighting
}   // namespace mrf
