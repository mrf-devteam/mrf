
/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <iostream>
#include <cmath>

namespace mrf
{
namespace math
{
/**
 * 4D Vector class
 * */
template<class T>
class Vec4
{
public:
  /*----- methods -----*/

  inline static Vec4 zero();
  inline static Vec4 xaxis();
  inline static Vec4 yaxis();
  inline static Vec4 zaxis();

  /*----- methods -----*/

  template<class S>
  Vec4(const Vec4<S> &vec)
  {
    _e[0] = (T)vec(0);
    _e[1] = (T)vec(1);
    _e[2] = (T)vec(2);
    _e[3] = (T)vec(3);
  }


  Vec4()
  // : _e[0]( T(0) ),
  //   _e[1]( T(1) ),
  //   _e[2]( T(2) ),
  //   _e[3]( T(3) )
  {
    _e[0] = T(0);
    _e[1] = T(0);
    _e[2] = T(0);
    _e[3] = T(0);
  }
  Vec4(T init_value) { _e[0] = _e[1] = _e[2] = _e[3] = T(init_value); }

  Vec4(const Vec4<T> &vec);
  Vec4(const Vec3<T> &vec, T e4);

  Vec4(const T &e0, const T &e1, const T &e2, const T &e3);
  Vec4(const T vec[]);

  ~Vec4() {}

  T *      ptr();
  const T *ptr() const;
  T *      getArray();
  const T *getArray() const;

  void setValues(const T _v1, const T _v2, const T _v3, const T _v4);
  void set(const T _v1, const T _v2, const T _v3, const T _v4);


  T length() const;
  T sqrLength() const;
  T norm() const;    //an alias for length
  T norm2() const;   //an alias for sqrLength

  T dot(const Vec4<T> &vec) const;

  /**
   *
   * Returns the mean of the 4 components
   **/
  inline T mean() const;


  /**
   *
   * Returns the sum of the 4 components
   **/
  inline T sum() const;



  T x() const;
  T y() const;
  T z() const;
  T w() const;


  Vec4  cross(const Vec4<T> &vec) const;
  Vec4  normal() const;
  Vec4 &normalEq();
  Vec4 &normalEq(const T length);
  Vec4 &negateEq();
  Vec4 &clampToMaxEq(const T &max);
  Vec4  sqrt() const;

  Vec4 &squareEq();
  Vec4  square() const;

  Vec4 &absVal();
  Vec4 &abs();


  /**
   * Round vector components to zero if they are below the epsilon threshold
   */
  void epsilonRoundZero(T const epsilon);


  /** Given a candidate vector, replace
      elements whose corresponding elements in Candidate are
      smaller.  This function is used for finding objects' bounds,
      among other things.
      @brief change the Vect3 D if we use D.minimize
  */
  inline void minimize(Vec4<T> const &Candidate);

  /** Given a candidate vector, replace
      elements whose corresponding elements in Candidate are
      larger.  This function is used for finding objects' bounds,
      among other things.
      @brief change the Vect3 D if we use D.maximize
  */
  inline void maximize(Vec4<T> const &Candidate);

  /* Apply a std::pow power function on each component:
   */
  inline Vec4<T> &pow(T const &power);


  static inline T dot(Vec4<T> const &v0, Vec4<T> const &v1);

  static inline Vec4<T> interpolate(Vec4<T> const &u, Vec4<T> const &v, T alpha = T(0.5));

  inline void clampToZero(T EPSILON);
  inline bool allPosOrNull(T EPSILON);
  inline bool isInfOrNan() const;
  inline bool isFinite() const;

  void        setX(const T &_v);
  void        setY(const T &_v);
  void        setZ(const T &_v);
  void        setW(const T &_v);
  inline void setConstant(T cte_value);


  Vec4 operator+(const Vec4<T> &rhs) const;
  Vec4 operator-(const Vec4<T> &rhs) const;
  Vec4 operator-() const;
  Vec4 operator*(const T &rhs) const;
  Vec4 operator*(const Vec4<T> &rhs) const;
  Vec4 operator/(const T &rhs) const;
  Vec4 operator/(const Vec4<T> &rhs) const;

  Vec4 &operator+=(const Vec4<T> &rhs);
  Vec4 &operator-=(const Vec4<T> &rhs);
  Vec4 &operator*=(const T &rhs);
  Vec4 &operator*=(const Vec4<T> &rhs);
  Vec4 &operator/=(const T &rhs);
  Vec4 &operator/=(const Vec4<T> &rhs);
  Vec4 &operator=(const Vec4<T> &rsh);

  bool operator==(const Vec4<T> &rhs) const;
  bool operator!=(const Vec4<T> &rhs) const;

  T &      operator()(int idx);
  const T &operator()(int idx) const;

  T &      operator[](int idx);
  const T &operator[](int idx) const;

  bool operator>(const Vec4<T> &_v) const;
  bool operator>=(const Vec4<T> &_v) const;
  bool operator<(const Vec4<T> &_v) const;
  bool operator<=(const Vec4<T> &_v) const;


private:
  /*----- data members -----*/

  T _e[4];
};

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::zero()
{
  return Vec4(0, 0, 0, 0);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::xaxis()
{
  return Vec4(T(1), T(0), T(0), T(0));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::yaxis()
{
  return Vec4(T(0), T(1), T(0), T(0));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::zaxis()
{
  return Vec4(T(0), T(0), T(1), T(0));
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T>::Vec4(const Vec4<T> &vec)
{
  _e[0] = vec._e[0];
  _e[1] = vec._e[1];
  _e[2] = vec._e[2];
  _e[3] = vec._e[3];
}
//-------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T>::Vec4(const Vec3<T> &vec, T e4)
{
  _e[0] = vec.x();
  _e[1] = vec.y();
  _e[2] = vec.z();
  _e[3] = e4;
}



//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T>::Vec4(const T &e0, const T &e1, const T &e2, const T &e3)
{
  _e[0] = e0;
  _e[1] = e1;
  _e[2] = e2;
  _e[3] = e3;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T>::Vec4(const T vec[])
{
  memcpy(_e, vec, 4 * sizeof(T));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T *Vec4<T>::ptr()
{
  return _e;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T *Vec4<T>::getArray()
{
  return this->ptr();
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T *Vec4<T>::getArray() const
{
  return this->ptr();
}


//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec4<T>::set(const T _v1, const T _v2, const T _v3, const T _v4)
{
  _e[0] = _v1;
  _e[1] = _v2;
  _e[2] = _v3;
  _e[3] = _v4;
}


//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec4<T>::setValues(const T _v1, const T _v2, const T _v3, const T _v4)
{
  set(_v1, _v2, _v3, _v4);
}




//------------------------------------------------------------------------------
//!
template<class T>
inline const T *Vec4<T>::ptr() const
{
  return _e;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec4<T>::length() const
{
  return (T)std::sqrt(sqrLength());
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec4<T>::sqrLength() const
{
  return _e[0] * _e[0] + _e[1] * _e[1] + _e[2] * _e[2] + _e[3] * _e[3];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec4<T>::norm() const
{
  return this->length();
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec4<T>::norm2() const
{
  return this->sqrLength();
}


//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec4<T>::dot(const Vec4<T> &vec) const
{
  return _e[0] * vec._e[0] + _e[1] * vec._e[1] + _e[2] * vec._e[2] + _e[3] * vec._e[3];
}


template<class T>
inline T Vec4<T>::mean() const
{
  return sum() / T(4);
}


template<class T>
inline T Vec4<T>::sum() const
{
  return _e[0] + _e[1] + _e[2] + _e[3];
}


//-------------------------------------------------------------------------
//!

template<class T>
inline T Vec4<T>::x() const
{
  return _e[0];
}

template<class T>
inline T Vec4<T>::y() const
{
  return _e[1];
}

template<class T>
inline T Vec4<T>::z() const
{
  return _e[2];
}

template<class T>
inline T Vec4<T>::w() const
{
  return _e[3];
}



//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::cross(const Vec4<T> &vec) const
{
  return Vec4<T>(
      _e[1] * vec._e[2] - _e[2] * vec._e[1],
      _e[2] * vec._e[0] - _e[0] * vec._e[2],
      _e[0] * vec._e[1] - _e[1] * vec._e[0],
      0);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::normal() const
{
  T tmp = (T)1 / this->length();
  return Vec4<T>(_e[0] * tmp, _e[1] * tmp, _e[2] * tmp, _e[3] * tmp);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::normalEq()
{
  T tmp = (T)1 / this->length();
  _e[0] *= tmp;
  _e[1] *= tmp;
  _e[2] *= tmp;
  _e[3] *= tmp;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::normalEq(const T length)
{
  T tmp = length / this->length();
  _e[0] *= tmp;
  _e[1] *= tmp;
  _e[2] *= tmp;
  _e[3] *= tmp;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::negateEq()
{
  _e[0] = -_e[0];
  _e[1] = -_e[1];
  _e[2] = -_e[2];
  _e[3] = -_e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::clampToMaxEq(const T &max)
{
  if (_e[0] > max)
  {
    _e[0] = max;
  }
  if (_e[1] > max)
  {
    _e[1] = max;
  }
  if (_e[2] > max)
  {
    _e[2] = max;
  }
  if (_e[3] > max)
  {
    _e[3] = max;
  }
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::sqrt() const
{
  return Vec4<T>(std::sqrt(_e[0]), std::sqrt(_e[1]), std::sqrt(_e[2]), std::sqrt(_e[3]));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::squareEq()
{
  _e[0] = _e[0] * _e[0];
  _e[1] = _e[1] * _e[1];
  _e[2] = _e[2] * _e[2];
  _e[3] = _e[3] * _e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::square() const
{
  return Vec4<T>(_e[0] * _e[0], _e[1] * _e[1], _e[2] * _e[2], _e[3] * _e[3]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::absVal()
{
  _e[0] = mrf::math::_Math<T>::absVal(_e[0]);
  _e[1] = mrf::math::_Math<T>::absVal(_e[1]);
  _e[2] = mrf::math::_Math<T>::absVal(_e[2]);
  _e[3] = mrf::math::_Math<T>::absVal(_e[3]);

  return *this;
}
template<class T>
inline Vec4<T> &Vec4<T>::abs()
{
  return this->absVal();
}




//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec4<T>::epsilonRoundZero(T const epsilon)
{
  _e[0] = (_Math<T>::absVal(_e[0]) < epsilon) ? T(0) : _e[0];
  _e[1] = (_Math<T>::absVal(_e[1]) < epsilon) ? T(0) : _e[1];
  _e[2] = (_Math<T>::absVal(_e[2]) < epsilon) ? T(0) : _e[2];
  _e[3] = (_Math<T>::absVal(_e[3]) < epsilon) ? T(0) : _e[3];
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::operator+(const Vec4<T> &rhs) const
{
  return Vec4<T>(_e[0] + rhs._e[0], _e[1] + rhs._e[1], _e[2] + rhs._e[2], _e[3] + rhs._e[3]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::operator-(const Vec4<T> &rhs) const
{
  return Vec4<T>(_e[0] - rhs._e[0], _e[1] - rhs._e[1], _e[2] - rhs._e[2], _e[3] - rhs._e[3]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::operator-() const
{
  return Vec4<T>(-_e[0], -_e[1], -_e[2], -_e[3]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::operator*(const T &rhs) const
{
  return Vec4<T>(_e[0] * rhs, _e[1] * rhs, _e[2] * rhs, _e[3] * rhs);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::operator*(const Vec4<T> &rhs) const
{
  return Vec4<T>(_e[0] * rhs._e[0], _e[1] * rhs._e[1], _e[2] * rhs._e[2], _e[3] * rhs._e[3]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::operator/(const T &rhs) const
{
  return Vec4<T>(_e[0] / rhs, _e[1] / rhs, _e[2] / rhs, _e[3] / rhs);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> Vec4<T>::operator/(const Vec4<T> &rhs) const
{
  return Vec4<T>(_e[0] / rhs._e[0], _e[1] / rhs._e[1], _e[2] / rhs._e[2], _e[3] / rhs._e[3]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::operator+=(const Vec4<T> &rhs)
{
  _e[0] += rhs._e[0];
  _e[1] += rhs._e[1];
  _e[2] += rhs._e[2];
  _e[3] += rhs._e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::operator-=(const Vec4<T> &rhs)
{
  _e[0] -= rhs._e[0];
  _e[1] -= rhs._e[1];
  _e[2] -= rhs._e[2];
  _e[3] -= rhs._e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::operator*=(const T &rhs)
{
  _e[0] *= rhs;
  _e[1] *= rhs;
  _e[2] *= rhs;
  _e[3] *= rhs;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::operator*=(const Vec4<T> &rhs)
{
  _e[0] *= rhs._e[0];
  _e[1] *= rhs._e[1];
  _e[2] *= rhs._e[2];
  _e[3] *= rhs._e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::operator/=(const T &rhs)
{
  _e[0] /= rhs;
  _e[1] /= rhs;
  _e[2] /= rhs;
  _e[3] /= rhs;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::operator/=(const Vec4<T> &rhs)
{
  _e[0] /= rhs._e[0];
  _e[1] /= rhs._e[1];
  _e[2] /= rhs._e[2];
  _e[3] /= rhs._e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> &Vec4<T>::operator=(const Vec4<T> &rhs)
{
  //assignment to itself ?
  if ((void *)this == (void *)&rhs)
  {
    return *this;
  }

  _e[0] = rhs._e[0];
  _e[1] = rhs._e[1];
  _e[2] = rhs._e[2];
  _e[3] = rhs._e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec4<T>::operator==(const Vec4<T> &rhs) const
{
  return _e[0] == rhs._e[0] && _e[1] == rhs._e[1] && _e[2] == rhs._e[2] && _e[3] == rhs._e[3];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec4<T>::operator!=(const Vec4<T> &rhs) const
{
  return !(*this == rhs);
  //_e[0] != rhs._e[0] || _e[1] != rhs._e[1] || _e[2] != rhs._e[2] || _e[3] != rhs._e[3];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Vec4<T>::operator()(int idx)
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Vec4<T>::operator()(int idx) const
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Vec4<T>::operator[](int idx)
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Vec4<T>::operator[](int idx) const
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec4<T>::operator>(const Vec4<T> &_v) const
{
  return !(*this <= _v);
  // _e[0] > _v._e[0] && _e[1] > _v._e[1] && _e[2] > _v._e[2] && _e[3] > _v._e[3];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec4<T>::operator>=(const Vec4<T> &_v) const
{
  return _e[0] >= _v._e[0] && _e[1] >= _v._e[1] && _e[2] >= _v._e[2] && _e[3] >= _v._e[3];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec4<T>::operator<(const Vec4<T> &_v) const
{
  return !(*this >= _v);
  // _e[0] < _v._e[0] && _e[1] < _v._e[1] && _e[2] < _v._e[2] &&  _e[3] < _v._e[3];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec4<T>::operator<=(const Vec4<T> &_v) const
{
  return _e[0] <= _v._e[0] && _e[1] <= _v._e[1] && _e[2] <= _v._e[2] && _e[3] <= _v._e[3];
}



//------------------------------------------------------------------------------
//!
template<class T>
inline Vec4<T> operator*(const T &val, const Vec4<T> &vec)
{
  return Vec4<T>(vec(0) * val, vec(1) * val, vec(2) * val, vec(3) * val);
}


//-------------------------------------------------------------------------
//!
template<class T>
inline void Vec4<T>::minimize(Vec4<T> const &Candidate)
{
  if (Candidate[0] < _e[0]) _e[0] = Candidate[0];
  if (Candidate[1] < _e[1]) _e[1] = Candidate[1];
  if (Candidate[2] < _e[2]) _e[2] = Candidate[2];
  if (Candidate[3] < _e[3]) _e[3] = Candidate[3];
}

template<class T>
inline void Vec4<T>::maximize(Vec4<T> const &Candidate)
{
  if (Candidate[0] > _e[0]) _e[0] = Candidate[0];
  if (Candidate[1] > _e[1]) _e[1] = Candidate[1];
  if (Candidate[2] > _e[2]) _e[2] = Candidate[2];
  if (Candidate[3] > _e[3]) _e[3] = Candidate[3];
}

template<class T>
inline Vec4<T> &Vec4<T>::pow(T const &power)
{
  _e[0] = std::pow(_e[0], power);
  _e[1] = std::pow(_e[1], power);
  _e[2] = std::pow(_e[2], power);
  _e[3] = std::pow(_e[3], power);

  return *this;
}

template<class T>
inline T Vec4<T>::dot(Vec4<T> const &v0, Vec4<T> const &v1)
{
  return v0.dot(v1);
}


template<class T>
inline Vec4<T> Vec4<T>::interpolate(const Vec4<T> &u, const Vec4<T> &v, T alpha)
{
  return (u * (T(1.0) - alpha) + v * alpha);
}

template<class T>
inline void Vec4<T>::clampToZero(T EPSILON)
{
  _e[0] = (_Math<T>::absVal(_e[0]) < EPSILON) ? T(0.0) : _e[0];
  _e[1] = (_Math<T>::absVal(_e[1]) < EPSILON) ? T(0.0) : _e[1];
  _e[2] = (_Math<T>::absVal(_e[2]) < EPSILON) ? T(0.0) : _e[2];
  _e[3] = (_Math<T>::absVal(_e[3]) < EPSILON) ? T(0.0) : _e[3];
}

template<class T>
inline bool Vec4<T>::allPosOrNull(T EPSILON)
{
  return (_e[0] > -EPSILON) && (_e[1] > -EPSILON) && (_e[2] > -EPSILON) && (_e[3] > -EPSILON);
}

template<class T>
inline bool Vec4<T>::isInfOrNan() const
{
  // These conversion to double is due to the lack of compliance of MS VS 2017 and 2019
  double const e0 = static_cast<double>(_e[0]);
  double const e1 = static_cast<double>(_e[1]);
  double const e2 = static_cast<double>(_e[2]);
  double const e3 = static_cast<double>(_e[3]);

  return std::isnan(e0) || std::isnan(e1) || std::isnan(e2) || std::isnan(e3) || std::isinf(e0) || std::isinf(e1)
         || std::isinf(e2) || std::isinf(e3);
}


template<class T>
inline bool Vec4<T>::isFinite() const
{
  return !isInfOrNan();
}

template<class T>
inline void Vec4<T>::setX(const T &_v)
{
  _e[0] = _v;
}

template<class T>
inline void Vec4<T>::setY(const T &_v)
{
  _e[1] = _v;
}

template<class T>
inline void Vec4<T>::setZ(const T &_v)
{
  _e[2] = _v;
}

template<class T>
inline void Vec4<T>::setW(const T &_v)
{
  _e[3] = _v;
}

template<class T>
inline void Vec4<T>::setConstant(T cte_value)
{
  setX(cte_value);
  setY(cte_value);
  setZ(cte_value);
  setW(cte_value);
}


/*==============================================================================
  TYPEDEF
  ==============================================================================*/

typedef Vec4<int>          Vec4i;
typedef Vec4<unsigned int> Vec4u;
typedef Vec4<unsigned int> Vec4ui;
typedef Vec4<float>        Vec4f;
typedef Vec4<double>       Vec4d;
typedef Vec4<bool>         Vec4b;

/*
 *  External Operator
 */
template<class T>
inline std::ostream &operator<<(std::ostream &os, Vec4<T> const &v)
{
  return os << " [ " << v[0] << "; " << v[1] << "; " << v[2] << "; " << v[3] << " ] ";
}



}   // namespace math
}   // namespace mrf
