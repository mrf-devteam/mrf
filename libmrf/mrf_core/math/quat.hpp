/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/mat4.hpp>
#include <mrf_core/math/vec3.hpp>

namespace mrf
{
namespace math
{
template<class T>
class Quat
{
public:
  /*----- static methods -----*/

  inline static Quat identity();

  /*----- methods -----*/

  Quat();
  Quat(const T &x, const T &y, const T &z, const T &w);


  /**
   * THIS IS NOT A CONSTRUCTOR WHERE vec represents the
   * rotation axis and scalar the rotation angle.
   *
   *
   * USE fromAxisAngle to construct such Quaternion !!!
   *
   **/
  Quat(const Vec3<T> &vec, const T &scalar);

  Quat(const Quat<T> &quat);
  Quat(const T vec[]);
  ~Quat() {}

  Vec3<T> &      getVec();
  T &            getX();
  T &            getY();
  T &            getZ();
  T &            getW();
  const Vec3<T> &getVec() const;
  const T &      getX() const;
  const T &      getY() const;
  const T &      getZ() const;
  const T &      getW() const;

  void fromAxisAngle(const Vec3<T> &axis, const T &angle);
  void fromAxisCos(const Vec3<T> &axis, const T &cosAngle);
  void fromAxes(const Vec3<T> &x, const Vec3<T> &y, const Vec3<T> &z);
  void fromEulerXYZ(const Vec3<T> &angles);
  void fromEulerXZY(const Vec3<T> &angles);
  void fromEulerYXZ(const Vec3<T> &angles);
  void fromEulerYZX(const Vec3<T> &angles);
  void fromEulerZXY(const Vec3<T> &angles);
  void fromEulerZYX(const Vec3<T> &angles);
  void fromTwoVecs(const Vec3<T> &vFrom, const Vec3<T> &vTo);
  void fromYawPitchRollValues(const T &_yaw, const T &_pitch, const T &_roll);
  void fromMatrix(const Mat4<T> &mat);

  T norm() const;
  T dot(const Quat<T> &quat) const;

  Quat &negateEq();
  Quat  conjugate() const;
  Quat &conjugateEq();
  Quat  inverse() const;
  Quat &inverseEq();

  Mat4<T> getMatrix() const;

  void    getAxes(Vec3<T> &x, Vec3<T> &y, Vec3<T> &z) const;
  Vec3<T> getAxisX() const;
  Vec3<T> getAxisY() const;
  Vec3<T> getAxisZ() const;
  void    getEulerAngles(T &_yaw, T &_pitch, T &_roll) const;

  Quat slerp(const Quat<T> &quat, T a) const;
  Quat squad(const Quat<T> &quat, T a) const;

  Quat operator+(const Quat<T> &quat) const;
  Quat operator-(const Quat<T> &quat) const;
  Quat operator-() const;
  Quat operator*(const Quat<T> &quat) const;
  Quat operator*(const T &val) const;
  Quat operator/(const T &val) const;

  Vec3<T> operator*(const Vec3<T> &vec) const;

  Quat &operator+=(const Quat<T> &quat);
  Quat &operator-=(const Quat<T> &quat);
  Quat &operator*=(const Quat<T> &quat);
  Quat &operator*=(const T &val);
  Quat &operator/=(const T &val);
  Quat &operator=(const Quat &quat);

  T &      operator()(int idx);
  const T &operator()(int idx) const;

  T &      operator[](int idx);
  const T &operator[](int idx) const;

private:
  Vec3<T> _vec;
  T       _scalar;
};

#include "quat.cxx"

}   // namespace math
}   // namespace mrf
