/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//MRF
#include <mrf_core/math/math.hpp>
#include <mrf_core/geometry/computation.hpp>
#include <mrf_core/sampling/random_generator.hpp>
#include <mrf_core/sampling/jittering.hpp>
#include <mrf_core/sampling/halton.hpp>


namespace mrf
{
namespace sampling
{
template<typename T>
class _Sampler
{
public:
  //This is the Pattanaik (PhD Thesis 1995) method
  static void uniformTriangle(
      float                   e1,
      float                   e2,
      mrf::math::Vec3f const &v1,
      mrf::math::Vec3f const &v2,
      mrf::math::Vec3f const &v3,
      mrf::math::Vec3f &      point_generated);

  static mrf::math::Vec3f uniformTriangle(
      float                   e1,
      float                   e2,
      mrf::math::Vec3f const &v1,
      mrf::math::Vec3f const &v2,
      mrf::math::Vec3f const &v3);



  /**
   * Taken From pbrt
   */
  static void concentricSampleDisk(float e1, float e2, float &dx, float &dy);
  // In Polar coordinates
  static void concentricSampleDiskPolar(float e1, float e2, float &dx, float &dy);

  static void uniformSphere(float e1, float e2, mrf::math::Vec3f &direction_generated);


  static void uniformHemiSphere(float e1, float e2, mrf::math::Vec3f const &N, mrf::math::Vec3f &direction_generated);

  static void uniformHemiSphere(float e1, float e2, mrf::math::Vec3f &direction_generated);

  static mrf::math::Vec3f uniformHemiSphere(float e1, float e2);


  static void cosineHemiSphere(float e1, float e2, mrf::math::Vec3f const &N, mrf::math::Vec3f &direction_generated);

  static mrf::math::Vec3f cosineHemiSphere(float e1, float e2, mrf::math::Vec3f const &N);


  //This sample the hemisphere multiplied by the cosine in global frame
  static mrf::math::Vec3f cosineHemiSphere(float e1, float e2);




  static void stratifiedHemiSphere(
      float             e1,
      float             e2,
      unsigned int      num_sample_theta,
      unsigned int      num_sample_phi,
      unsigned int      i,
      unsigned int      j,
      mrf::math::Vec3f &direction_generated);

  static inline void stratifiedHemiSphere(
      float        e1,
      float        e2,
      unsigned int num_sample_theta,
      unsigned int num_sample_phi,
      unsigned int i,
      unsigned int j,
      float &      theta,
      float &      phi);




  /**
   * Generate the direction for a local coordinate system
   * defined by the given normal
   */
  mrf::math::Vec3f stratifiedHemiSphere(
      float                   e1,
      float                   e2,
      unsigned int            ntheta,
      unsigned int            nphi,
      unsigned int            i,
      unsigned int            j,
      mrf::math::Vec3f const &N);



  //          static void phongLobe( float e1, float e2,
  //                                 unsigned int shininess,
  //                                 mrf::math::Vec3f & direction_generated );

  static mrf::math::Vec3f hemiSphereCosExp(float e1, float e2, mrf::math::Vec3f const &N, float shininess);



  static mrf::math::Vec3f
  phongLobe(float e1, float e2, mrf::math::Vec3f const &N, mrf::math::Vec3f const &incident_direction, float shininess);


  //          inline static void orthoBasis( VERTEX_TYPE const & N,
  //                                         VERTEX_TYPE & X, VERTEX_TYPE & Y);
};


typedef _Sampler<float>  Sampler;
typedef _Sampler<double> SamplerD;

template<typename T, class VERTEX_TYPE>
class _UniformQuadSampler
{
private:
  T            _Step[2];
  VERTEX_TYPE  _min;
  unsigned int _dimension;
  unsigned int _nb_samples;

public:
  _UniformQuadSampler(
      VERTEX_TYPE  min,
      VERTEX_TYPE  max,
      unsigned int dimension,   //for which dim the
      //quad is planar?
      unsigned int nb_samples_per_dim = 4);


  VERTEX_TYPE sample(unsigned int num_of_sample);
};

typedef _UniformQuadSampler<float, mrf::math::Vec3f> QuadSampler;

/**
 * Jittered +mrf::rendering::HALTON Quad Sampler
 *
 */
template<class VERTEX_TYPE>
class MRF_CORE_EXPORT _JHQuadSampler
{
private:
  JitteredSample2D _js2d;
  Halton           _halton;

  //          VERTEX_TYPE _min;
  //          float _stepX;
  //          float _stepY;


public:
  //          _JHQuadSampler( VERTEX_TYPE min,
  //                           float stepX, float stepY,
  //                           unsigned int nb_samples_per_dim );

  static _JHQuadSampler &Instance(unsigned int nb_samples)
  {
    static _JHQuadSampler theJHQuadSampler(nb_samples);
    return theJHQuadSampler;
  }


  inline VERTEX_TYPE sample(VERTEX_TYPE const &min, float stepX, float stepY, unsigned int sample)
  {
    //             float e1 = _js2d.getRandomFloat(0, sample, _halton.halton(0,sample));
    //             float e2 = _js2d.getRandomFloat(1, sample, _halton.halton(1,sample));

    float e1 = _js2d.getRandomFloat(0, sample, RandomGenerator::Instance().getFloat());
    float e2 = _js2d.getRandomFloat(1, sample, RandomGenerator::Instance().getFloat());

    return VERTEX_TYPE(min[0] + e1 * stepX, min[1] + e2 * stepY);
  }



private:
  _JHQuadSampler(unsigned int nb_samples_per_dim): _js2d(nb_samples_per_dim) {}

  virtual ~_JHQuadSampler() {}

  _JHQuadSampler(_JHQuadSampler const &) {}

  _JHQuadSampler &operator=(_JHQuadSampler const &) {}
};

typedef _JHQuadSampler<mrf::math::Vec2f> JHQuadSampler;

#include "sampler.cxx"

}   // namespace sampling
}   // namespace mrf
