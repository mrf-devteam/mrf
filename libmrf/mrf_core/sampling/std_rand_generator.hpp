/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#if defined(_OPENMP)
#  include <omp.h>
#endif

//STL
#include <random>
#include <vector>
#include <iostream>
#include <ctime>

namespace mrf
{
namespace sampling
{
/**
 * @brief      Layer to generate random number using the STL library (work only in C++ 2011)
 *             Currently the random generator is set to mt19337_64 choosen for its speed.
 *
 *             This class is thread safe. One random number generator is created for each thread
 */
class MRF_CORE_EXPORT StdRandGen
{
public:
  inline StdRandGen();

  inline void   setSeed(long unsigned int new_seed);
  inline float  getFloat(unsigned int id_thread);
  inline double getDouble(unsigned int id_thread);

private:
  //The different type of Random Generator supported by C++ 2011
  //#ifndef MRF_STD_RANDOM_GENERATOR_TYPE

  typedef std::mt19937_64 GENERATOR_TYPE;

  //typedef std::mt19937 GENERATOR_TYPE;

  //typedef std::minstd_rand GENERATOR_TYPE;
  //typedef std::minstd_rand0 GENERATOR_TYPE;

  //SLOW
  //typedef std::ranlux48 GENERATOR_TYPE;
  //typedef std::knuth_b GENERATOR_TYPE;
  //#endif

  std::uniform_real_distribution<> _distribution;
  std::vector<GENERATOR_TYPE>      _generators;
};

inline StdRandGen::StdRandGen(): _distribution(0.0, 1.0)
{
#ifdef MRF_STD_RANDOM_GENERATOR_DEBUG
  std::cout << "std GENERATOR CONSTRUCTED  <==========================" << std::endl;
#endif

#if defined(_OPENMP)
  //std::cout << "  omp_get_max_threads()*2 = " <<  omp_get_max_threads() * 2 << std::endl;

  for (int i = 0; i < omp_get_max_threads() * 2; i++)
  {
    //Create generators for each CPU Core or Thread to enforce randomness and speed
    _generators.push_back(GENERATOR_TYPE(time(NULL)));
  }
#else
  for (unsigned int i = 0; i < mrf::sampling::NB_MAX_NUM_THREADS; i++)
  {
    //Create generators for each CPU Core or Thread to enforce randomness and speed
    _generators.push_back(GENERATOR_TYPE(time(NULL)));
  }
#endif
}

inline void StdRandGen::setSeed(long unsigned int new_seed)
{
  for (unsigned int i = 0; i < _generators.size(); ++i)
  {
    _generators[i].seed(new_seed);
  }
}

inline float StdRandGen::getFloat(unsigned int id_thread)
{
  // std::cout << " id+thread = " << id_thread << std::endl;
  // std::cout << " _generators.size = " << _generators.size() << std::endl;
  // std::cout << " omp_get_num_threads() = " << omp_get_num_threads() << std::endl;

#if defined(_OPENMP)
  return static_cast<float>(_distribution(_generators[id_thread]));
#else
  // P-Thread MODE  USE THE id_thread.
  return static_cast<float>(_distribution(_generators[id_thread]));
#endif
}

#if defined(_OPENMP)
inline double StdRandGen::getDouble(unsigned int /*id_thread*/)
{
  return _distribution(_generators[omp_get_num_threads()]);
}
#else
inline double StdRandGen::getDouble(unsigned int id_thread)
{
  // P-Thread MODE  USE THE id_thread.
  return _distribution(_generators[id_thread]);
}
#endif

}   // namespace sampling
}   // namespace mrf
