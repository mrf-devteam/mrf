/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace sampling
{
static unsigned int const NB_MAX_NUM_THREADS = 512;
}
}   // namespace mrf



#ifdef MRF_WITH_BOOST_SUPPORT
#  include <mrf_core/sampling/BoostRandGenerator.hpp>
#else
#  include <mrf_core/sampling/std_rand_generator.hpp>
#endif


namespace mrf
{
namespace sampling
{
/**
 * @brief      Singleton class that provides random  number generation
 *
 *             Depending on the compilation configuration boost or the STL are used
 *             as underlying generator
 *
 *             This class is thread-safe.
 *
 */
class MRF_CORE_EXPORT RandomGenerator
{
public:
private:
  //static MTRandom _generator;

#ifdef MRF_WITH_BOOST_SUPPORT
  static BoostRandGen _generator;
#else
  static StdRandGen _generator;
  //static __thread StdRandGen _generator  : A TESTER pour PThread. stockage par thread
#endif

#if defined(_OPENMP)
  //#pragma omp threadprivate(_generator)
#endif

public:
  static RandomGenerator &Instance();


  inline void   setSeed(unsigned long new_seed);
  inline float  getFloat(unsigned int id_thread = 0);
  inline double getDouble(unsigned int id_thread = 0);

  //Generates an unsigned int between 0 and max_ui
  static inline unsigned int getUI(unsigned int max_ui, unsigned int id_thread = 0);



private:
  //Hidden due to Singleton Implementation
  RandomGenerator();
  virtual ~RandomGenerator();
  RandomGenerator(RandomGenerator const &);              // copy ctor is hidden
  RandomGenerator &operator=(RandomGenerator const &);   // assign op is hidden
};



inline void RandomGenerator::setSeed(unsigned long new_seed)
{
  _generator.setSeed(new_seed);
}

inline float RandomGenerator::getFloat(unsigned int id_thread)
{
  return _generator.getFloat(id_thread);
}

inline double RandomGenerator::getDouble(unsigned int id_thread)
{
  return _generator.getDouble(id_thread);
}

inline unsigned int RandomGenerator::getUI(unsigned int max_ui, unsigned int id_thread)
{
  // SURE ??
  return static_cast<unsigned int>(_generator.getFloat(id_thread) * (max_ui + 1));
}




}   // namespace sampling
}   // namespace mrf
