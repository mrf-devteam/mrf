/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <cmath>

namespace mrf
{
namespace sampling
{
/**
 * This class performs jittering sampling in 2D
 *
 *
 * @author     Romain Pacanowski <romain.pacanowski@institutoptique.fr>
 * @author     Xavier Granier    <xavier.granier@inria.fr>
 *
 **/
template<typename INDEX_TYPE>
class _JitteredSample2D
{
private:
  /* The number of samples per dimension */
  INDEX_TYPE _ap;

public:
  _JitteredSample2D(): _ap(1) {}

  /**
   * @brief      Construct a Jitterer 2D Object with the given total number of samples
   *
   *             The total number of samples is rounded to the nearest power of two
   *             The number of samples per dimension is: (int) sqrt(maxnb)
   *
   * @param[in]  maxnb  The desired total number of samples
   *
   */
  _JitteredSample2D(INDEX_TYPE const maxnb): _ap((INDEX_TYPE)std::sqrt(static_cast<double>(maxnb))) {}

  void setMax(INDEX_TYPE const maxnb) { _ap = static_cast<INDEX_TYPE>(std::sqrt(static_cast<double>(maxnb))); }


  template<typename RANDOM_TYPE, typename DIM1_TYPE>
  RANDOM_TYPE getRandomFloat(DIM1_TYPE const d, INDEX_TYPE const nb, RANDOM_TYPE const f) const
  {
    return (RANDOM_TYPE((INDEX_TYPE)(((d == 0) ? (nb) : (nb / _ap))) % _ap) + f) / RANDOM_TYPE(_ap);
  }

  INDEX_TYPE nbOfSamplesPerDimension() const { return _ap; }

  INDEX_TYPE totalNbSamples() const { return _ap * _ap; }


};   // end of _JitteredSampled2D Class


typedef mrf::sampling::_JitteredSample2D<unsigned int> JitteredSample2D;

}   // namespace sampling
}   // namespace mrf
