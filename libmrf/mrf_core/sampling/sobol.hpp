/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Code originaly from Blender
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace sampling
{
#define SOBOL_BITS           32
#define SOBOL_MAX_DIMENSIONS 21201

void MRF_CORE_EXPORT sobol_generate_direction_vectors(uint vectors[][SOBOL_BITS], int dimensions);
}   // namespace sampling
}   // namespace mrf