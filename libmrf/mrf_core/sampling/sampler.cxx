
template<typename T>
void _Sampler<T>::uniformTriangle(
    float                   epsilon1,
    float                   epsilon2,
    mrf::math::Vec3f const &v1,
    mrf::math::Vec3f const &v2,
    mrf::math::Vec3f const &v3,
    mrf::math::Vec3f &      point_generated)
{
  float ne1       = std::sqrt(1 - epsilon1);
  point_generated = v1 + epsilon2 * ne1 * (v2 - v1) + (1 - ne1) * (v3 - v1);
}


template<typename T>
mrf::math::Vec3f _Sampler<T>::uniformTriangle(
    float                   epsilon1,
    float                   epsilon2,
    mrf::math::Vec3f const &v1,
    mrf::math::Vec3f const &v2,
    mrf::math::Vec3f const &v3)
{
  float ne1 = std::sqrt(1 - epsilon1);
  return (v1 + epsilon2 * ne1 * (v2 - v1) + (1 - ne1) * (v3 - v1));
}


/**
 * Taken From pbrt
 */
template<typename T>
void _Sampler<T>::concentricSampleDisk(float u1, float u2, float &dx, float &dy)
{
  float r, theta;

  // Map uniform random numbers to $[-1,1]^2$
  float const sx = 2.0f * u1 - 1.0f;
  float const sy = 2.0f * u2 - 1.0f;
  // Map square to $(r,\theta)$
  // Handle degeneracy at the origin
  if (sx == 0.0f && sy == 0.0f)
  {
    dx = 0.0;
    dy = 0.0;
    return;
  }
  if (sx >= -sy)
  {
    if (sx > sy)
    {
      // Handle first region of disk
      r = sx;
      if (sy > 0.0)
        theta = sy / r;
      else
        theta = 8.0f + sy / r;
    }
    else
    {
      // Handle second region of disk
      r     = sy;
      theta = 2.0f - sx / r;
    }
  }
  else
  {
    if (sx <= sy)
    {
      // Handle third region of disk
      r     = -sx;
      theta = 4.0f - sy / r;
    }
    else
    {
      // Handle fourth region of disk
      r     = -sy;
      theta = 6.0f + sx / r;
    }
  }
  theta *= static_cast<float>(mrf::math::Math::PI_4);
  dx = r * std::cos(theta);
  dy = r * std::sin(theta);
}


template<typename T>
void _Sampler<T>::concentricSampleDiskPolar(float u1, float u2, float &r, float &theta)
{
  float dx, dy;

  // Map uniform random numbers to $[-1,1]^2$
  float const sx = 2.0f * u1 - 1.0f;
  float const sy = 2.0f * u2 - 1.0f;
  // Map square to $(r,\theta)$
  // Handle degeneracy at the origin
  if (sx == 0.0f && sy == 0.0f)
  {
    dx = 0.0;
    dy = 0.0;
    return;
  }
  if (sx >= -sy)
  {
    if (sx > sy)
    {
      // Handle first region of disk
      r = sx;
      if (sy > 0.0)
        theta = sy / r;
      else
        theta = 8.0f + sy / r;
    }
    else
    {
      // Handle second region of disk
      r     = sy;
      theta = 2.0f - sx / r;
    }
  }
  else
  {
    if (sx <= sy)
    {
      // Handle third region of disk
      r     = -sx;
      theta = 4.0f - sy / r;
    }
    else
    {
      // Handle fourth region of disk
      r     = -sy;
      theta = 6.0f + sx / r;
    }
  }
  theta *= static_cast<float>(mrf::math::Math::PI_4);
}

//Inspired from PBRT and LAIR
template<typename T>
void _Sampler<T>::uniformSphere(float e1, float e2, mrf::math::Vec3f &direction_generated)
{
  float z   = 1.0f - 2.0f * e1;
  float r   = std::sqrt(mrf::math::_Math<T>::max(0.0f, 1.0f - z * z));
  float phi = 2.0f * mrf::math::_Math<T>::PI * e2;
  float x   = r * std::cos(phi);
  float y   = r * std::sin(phi);

  direction_generated[0] = x;
  direction_generated[1] = y;
  direction_generated[2] = z;
}

//Inspired from PBRT and LAIR
template<typename T>
void _Sampler<T>::uniformHemiSphere(float e1, float e2, mrf::math::Vec3f &direction_generated)
{
  float z = 2 * mrf::math::_Math<T>::PI * e2;

  float latitude = std::sqrt(e1);

  direction_generated[2] = latitude;

  latitude = sqrt(mrf::math::_Math<T>::max(0.0f, 1.0f - latitude * latitude));

  direction_generated[0] = std::cos(z) * latitude;
  direction_generated[1] = std::sin(z) * latitude;
}

//Inspired from PBRT and LAIR
template<typename T>
mrf::math::Vec3f _Sampler<T>::uniformHemiSphere(float e1, float e2)
{
  float z = 2 * mrf::math::_Math<T>::PI * e2;

  float latitude = std::sqrt(e1);

  mrf::math::Vec3f direction_generated;

  direction_generated[2] = latitude;

  latitude = std::sqrt(mrf::math::_Math<T>::max(0.0f, 1.0f - latitude * latitude));

  direction_generated[0] = std::cos(z) * latitude;
  direction_generated[1] = std::sin(z) * latitude;

  return direction_generated;
}


template<typename T>
void _Sampler<T>::uniformHemiSphere(
    float                   e1,
    float                   e2,
    mrf::math::Vec3f const &N,
    mrf::math::Vec3f &      direction_generated)
{
  float z1       = e1;
  float latitude = std::sqrt(mrf::math::_Math<T>::max(0.0f, 1.0f - z1 * z1));

  float z  = 2 * mrf::math::_Math<T>::PI * e2;
  float x1 = std::cos(z) * latitude;
  float y1 = std::sin(z) * latitude;


  mrf::math::Vec3f X(N.generateOrthogonal());
  X.normalize();

  mrf::math::Vec3f Y(N.cross(X));
  Y.normalize();

  direction_generated = x1 * X + y1 * Y + z1 * N;
}



template<typename T>
void _Sampler<T>::cosineHemiSphere(float e1, float e2, mrf::math::Vec3f const &N, mrf::math::Vec3f &direction_generated)
{
  const float phi = 2. * mrf::math::Math::PI * e1;

  mrf::math::Vec3f X(N.generateOrthogonal());
  mrf::math::Vec3f Y(N.cross(X));


  const float z         = std::sqrt(1.0 - e2);
  const float sin_theta = std::sqrt(e2 / X.norm2());
  const float x         = std::cos(phi) * sin_theta;
  const float y         = std::sin(phi) * sin_theta;

  direction_generated = x * X + y * Y + z * N;

  // PBRT WAY
  //     Vector ret;
  // 00027         ConcentricSampleDisk(u1, u2, &ret.x, &ret.y);
  // 00028         ret.z = std::sqrt(max(0.f,
  // 00029                           1.f - ret.x*ret.x - ret.y*ret.y));
  // 00030         return ret;
}

template<typename T>
mrf::math::Vec3f _Sampler<T>::cosineHemiSphere(float e1, float e2, mrf::math::Vec3f const &N)
{
  const float phi = 2. * mrf::math::Math::PI * e1;

  mrf::math::Vec3f X(N.generateOrthogonal());
  mrf::math::Vec3f Y(N.cross(X));


  const float z         = std::sqrt(1.0 - e2);
  const float sin_theta = std::sqrt(e2 / X.norm2());
  const float x         = std::cos(phi) * sin_theta;
  const float y         = std::sin(phi) * sin_theta;

  return x * X + y * Y + z * N;

  //    float x, y;
  //    concentricSampleDisk( e1, e2, x, y);
  //    float z = std::sqrt( Math::max( 0.0f, 1.0f - x*x - y*y ));
  //    mrf::math::Vec3f X ( N.generateOrthogonal() );
  //    mrf::math::Vec3f Y( N.cross(X));
  //    return ( x*X+y*Y+z*N );
}


template<typename T>
mrf::math::Vec3f _Sampler<T>::cosineHemiSphere(float e1, float e2)
{
  //PBRT Way
  float x, y;
  concentricSampleDisk(e1, e2, x, y);
  float z = std::sqrt(mrf::math::Math::max(0.0f, 1.0f - x * x - y * y));

  return mrf::math::Vec3f(x, y, z);
}



template<typename T>
void _Sampler<T>::stratifiedHemiSphere(
    float             e1,
    float             e2,
    unsigned int      num_sample_theta,
    unsigned int      num_sample_phi,
    unsigned int      i,
    unsigned int      j,
    mrf::math::Vec3f &direction_generated)
{
  float theta = std::asin(std::sqrt((j + 1 - e1) / num_sample_theta));
  float phi   = 2 * mrf::math::_Math<T>::PI * (i + 1 - e2) / num_sample_phi;

  float x, y, z;
  float r = 1;
  mrf::math::_Math<T>::cartesianCoord(theta, phi, r, x, y, z);

  direction_generated[0] = x;
  direction_generated[1] = y;
  direction_generated[2] = z;
}

template<typename T>
void _Sampler<T>::stratifiedHemiSphere(
    float        e1,
    float        e2,
    unsigned int num_sample_theta,
    unsigned int num_sample_phi,
    unsigned int i,
    unsigned int j,
    float &      theta,
    float &      phi)
{
  theta = std::asin(std::sqrt((j - e1) / num_sample_theta));
  phi   = 2 * mrf::math::_Math<T>::PI * (i - e2) / num_sample_phi;
}


template<typename T>
mrf::math::Vec3f _Sampler<T>::stratifiedHemiSphere(
    float                   e1,
    float                   e2,
    unsigned int            ntheta,
    unsigned int            nphi,
    unsigned int            i,
    unsigned int            j,
    mrf::math::Vec3f const &N)
{
  e1 = (j - e1) / ntheta;
  e2 = (i - e2) / nphi;

  float root  = std::sqrt(e1);
  float theta = asinf(root);
  float phi   = 2 * mrf::math::Math::PI * e2;

  mrf::math::Vec3f X, Y;
  geom::Computation::orthoBasis(N, X, Y);

  return X * cos(phi) * sin(theta) + Y * sin(phi) * sin(theta) + N * cos(theta);
}


template<typename T>
mrf::math::Vec3f _Sampler<T>::hemiSphereCosExp(float e1, float e2, mrf::math::Vec3f const &N, float shininess)
{
  const float            phi = 2. * mrf::math::Math::PI * e1;
  const mrf::math::Vec3f X(N.generateOrthogonal());
  const mrf::math::Vec3f Y(N ^ X);

  const float z = powf(e2, 1. / (float)(shininess + 1));
  ;
  const float sin_theta = std::sqrt((1. - z * z) / X.norm2());
  const float x         = sin_theta * std::cos(phi);
  const float y         = sin_theta * std::sin(phi);

  return x * X + y * Y + z * N;
}


template<typename T>
mrf::math::Vec3f
_Sampler<T>::phongLobe(float e1, float e2, mrf::math::Vec3f const &N, mrf::math::Vec3f const &indir, float shininess)
{
  //FROM GIS

  const mrf::math::Vec3f Z = N * (-2. * (mrf::math::Vec3f::dot(indir, N))) + indir;

  return hemiSphereCosExp(e1, e2, Z, shininess);
}


// template<typename T>
// void
// _Sampler<T>::phongLobe( float e1, float e2,
//                         unsigned int shininess,
//                         mrf::math::Vec3f & direction_generated )
// {
//    float frac = 2.0f / (shininess + 1);
//    float e3 = std::sqrt(  _Math<T>::max(0.0f, 1 - powf(e1, frac)) );
//    float z = 2 * _Math<T>::PI * e2;


//    direction_generated = x*X + y*Y + z*N;

//    direction_generated[0] = e3 * std::cos( z );
//    direction_generated[1] = e3 * std::sin( z );
//    direction_generated[2] = powf( e1, 1.0f/(shininess + 1) );




// 	const float phi =  2. * M_PI * _e1;
// 	const Vect3 X(_dir.GeneratePerpendicular());
// 	const Vect3 Y(_dir^X);

// 	const float z = powf(_e2,1./(float)(shininess+1));;
// 	const float sin_theta = std::sqrt((1.-z*z)/X.Norm2());
// 	const float x = sin_theta*std::cos(phi);
// 	const float y = sin_theta*std::sin(phi);

// 	return x*X+y*Y+z*_dir;
// }


template<typename T, class VERTEX_TYPE>
_UniformQuadSampler<T, VERTEX_TYPE>::_UniformQuadSampler(
    VERTEX_TYPE  min,
    VERTEX_TYPE  max,
    unsigned int dimension,
    unsigned int nb_samples)

  : _min(min)
  , _dimension(dimension)
  , _nb_samples(nb_samples)
{
  if (dimension == 0)
  {
    _Step[0] = (max[1] - min[1]) / static_cast<float>(nb_samples - 1);
    _Step[1] = (max[2] - min[2]) / static_cast<float>(nb_samples - 1);
  }
  else if (dimension == 1)
  {
    _Step[0] = (max[0] - min[0]) / static_cast<float>(nb_samples - 1);
    _Step[1] = (max[2] - min[2]) / static_cast<float>(nb_samples - 1);
  }
  else
  {
    _Step[0] = (max[0] - min[0]) / static_cast<float>(nb_samples - 1);
    _Step[1] = (max[1] - min[1]) / static_cast<float>(nb_samples - 1);
  }
}


template<typename T, class VERTEX_TYPE>
VERTEX_TYPE _UniformQuadSampler<T, VERTEX_TYPE>::sample(unsigned int k)
{
  VERTEX_TYPE result;


  if (_dimension == 0)
  {
    result[1] = _min[1] + ((k % _nb_samples) * _Step[0]);
    result[2] = _min[2] + ((k / _nb_samples) * _Step[1]);
  }
  else if (_dimension == 1)
  {
    result[0] = _min[0] + ((k % _nb_samples) * _Step[0]);
    result[2] = _min[2] + ((k / _nb_samples) * _Step[1]);
  }
  else
  {
    result[0] = _min[0] + ((k % _nb_samples) * _Step[0]);
    result[1] = _min[1] + ((k / _nb_samples) * _Step[1]);
  }

  result[_dimension] = _min[_dimension];
  return result;
}


// template< class VERTEX_TYPE>
// _JHQuadSampler<VERTEX_TYPE>::_JHQuadSampler( VERTEX_TYPE min,
//                                              float stepX,
//                                              float stepY,
//                                              unsigned int nb_samples_per_dim )
//       : _js2d(nb_samples_per_dim*nb_samples_per_dim),
//         _min( min ),
//         _stepX( stepX ),
//         _stepY( stepY )
// {

// }


// template<class VERTEX_TYPE>
// inline VERTEX_TYPE
// _JHQuadSampler<VERTEX_TYPE>::sample( unsigned int num_of_sample )
// {
//    float e1 = _js2d.getRandomFloat(0, num_of_sample, _halton.halton(0,num_of_sample));
//    float e2 = _js2d.getRandomFloat(1, num_of_sample, _halton.halton(1,num_of_sample));

//    VERTEX_TYPE result;
//    result[0] = _min[0] + e1 * _stepX;
//    result[1] = _min[1] + e2 * _stepY;

//    return result;
// }
