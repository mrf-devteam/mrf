#include <mrf_core/sampling/random_generator.hpp>

namespace mrf
{
namespace sampling
{
#ifdef MRF_WITH_BOOST_SUPPORT
BoostRandGen RandomGenerator::_generator;
#else
StdRandGen RandomGenerator::_generator;
#endif


RandomGenerator &RandomGenerator::Instance()
{
  static RandomGenerator the_generator;
  return the_generator;
}


//Private methods
//Hidden due to Singleton Implementation
RandomGenerator::RandomGenerator() {}

RandomGenerator::~RandomGenerator() {}

RandomGenerator::RandomGenerator(RandomGenerator const &) {}

RandomGenerator &RandomGenerator::operator=(RandomGenerator const &)
{
  return *this;
}

}   // namespace sampling
}   // namespace mrf
