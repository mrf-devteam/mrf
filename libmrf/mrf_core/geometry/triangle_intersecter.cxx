// #ifndef MRF_BACK_FACE_CULLING
// #define MRF_BACK_FACE_CULLING 1
// #endif


//Operators
template<class VERTEX_TYPE>
_Intersection<VERTEX_TYPE> &_Intersection<VERTEX_TYPE>::operator=(_Intersection const &n_inter)
{
  if ((void *)this == (void *)&n_inter)
  {
    return *this;
  }

  this->point              = n_inter.point;
  this->normal             = n_inter.normal;
  this->t                  = n_inter.t;
  this->material           = n_inter.material;
  this->object             = n_inter.object;
  this->uv                 = n_inter.uv;
  this->lighting_structure = n_inter.lighting_structure;
  this->tangent            = n_inter.tangent;

  return *this;
}



/**
 *  From Fast, minimum storage ray-triangle intersection.
 * Tomas Möller and Ben Trumbore.
 * Journal of Graphics Tools, 2(1):21--28, 1997
 */
template<class VERTEX_TYPE>
bool _TriaIntersecter<VERTEX_TYPE>::intersect(
    mrf::lighting::Ray const &ray,
    VERTEX_TYPE const &       v1,
    VERTEX_TYPE const &       v2,
    VERTEX_TYPE const &       v3,
    VERTEX_TYPE &             hit_point,
    float &                   w1,
    float &                   w2,
    float &                   w3,
    float &                   t)
{
  //  std::cout << __LINE__ << " " << __FILE__ << std::endl;
  // assert(0);


  //-------------------------------------------------------------------------
  // TO enable backface culling the sign of the determinant must be checked
  // if negative  the triangle is pointing away from the ray
  // and can be backface culled
  //
  // When the determinant is close to Zero, this means that the ray is parallel
  // from the triangle
  //-------------------------------------------------------------------------

  VERTEX_TYPE const E1 = v2 - v1;
  VERTEX_TYPE const E2 = v3 - v1;
  VERTEX_TYPE const P  = (ray.dir()).cross(E2);

  float const det = VERTEX_TYPE::dot(P, E1);

#ifdef MRF_BACK_FACE_CULLING
  // This means that  det is close to zero or NEGATIVE
  if (det < RAYTRACING_EPSILON)
  {
    return false;
  }
#else
  // This means that the ray is to parallel to the triangle
  // So no intersection is possible
  if (mrf::math::Math::absVal(det) < RAYTRACING_EPSILON)
  {
#  ifdef INTERSECTION_DEBUG
    std::cout << " NO INTERSECION DUE TO PARALLEL at " << __FILE__ << "  " << __LINE__ << std::endl;
#  endif
    return false;
  }
#endif



  VERTEX_TYPE const T = ray.origin() - v1;
  VERTEX_TYPE const Q = T.cross(E1);

  float const inv_det        = 1.0f / det;
  float const t_intersection = VERTEX_TYPE::dot(Q, E2) * inv_det;

  if ((t_intersection < ray.tMin()) || (t_intersection > ray.tMax()))
  {
    return false;
  }

  float const u = VERTEX_TYPE::dot(P, T) * inv_det;

  //if ( (u < 0.0f) || (u > 1.0f) ){return false;}
  if ((u < 0.0f))
  {
    return false;
  }

  float const v = VERTEX_TYPE::dot(ray.dir(), Q) * inv_det;
  if ((v < 0.0f) || (u + v > 1.0f))
  {
    return false;
  }

  //Intersection occured
  //Fill the results

  t  = t_intersection;
  w1 = (1.0f - u - v);
  w2 = u;
  w3 = v;

  hit_point = w1 * v1 + w2 * v2 + w3 * v3;


  return true;
}


/**
 * Does this ray intersects the Triangle back facelly ?
 **/
template<class VERTEX_TYPE>
bool _TriaIntersecter<VERTEX_TYPE>::intersectBackFace(
    mrf::lighting::Ray const &ray,
    VERTEX_TYPE const &       v1,
    VERTEX_TYPE const &       v2,
    VERTEX_TYPE const &       v3,
    VERTEX_TYPE const &       nv1,
    VERTEX_TYPE const &       nv2,
    VERTEX_TYPE const &       nv3)
{
  std::cout << __LINE__ << " " << __FILE__ << std::endl;
  assert(0);

  VERTEX_TYPE face_normal = nv1 + nv2 + nv3;
  face_normal.normalize();

  //-------------------------------------------------------------------------
  // First find an intersection if any !
  //-------------------------------------------------------------------------

  VERTEX_TYPE E1  = v2 - v1;
  VERTEX_TYPE E2  = v3 - v1;
  VERTEX_TYPE P   = VERTEX_TYPE::cross(ray.dir(), E2);
  float       det = VERTEX_TYPE::dot(P, E1);

  //Ray is parallel to face, no intersection at all therefore no backface intersection
  if (mrf::math::Math::absVal(det) < RAYTRACING_EPSILON)
  {
    return false;
  }

  float inv_det = 1.0f / det;

  VERTEX_TYPE T = ray.origin() - v1;
  VERTEX_TYPE Q = VERTEX_TYPE::cross(T, E1);

  float t_intersection = VERTEX_TYPE::dot(Q, E2) * inv_det;
  if ((t_intersection < ray.tMin()) || (t_intersection > ray.tMax()))
  {
    //No INTERSECTION WITH TRIANGLE
    return false;
  }

  float u = VERTEX_TYPE::dot(P, T) * inv_det;
  if ((u < 0.0f) || (u > 1.0f))
  {
    //No INTERSECTION WITH TRIANGLE
    return false;
  }

  float v = VERTEX_TYPE::dot(ray.dir(), Q) * inv_det;
  if ((v < 0.0f) || (u + v > 1.0f))
  {
    //No INTERSECTION WITH TRIANGLE
    return false;
  }

  // OK WE HAVE AN INTERSECTION
  // Updating the Intersection structure
  //    float t = t_intersection;
  //VERTEX_TYPE point = (1.0f - u - v) * v1 + u*v2 + v*v3;


  //Notice that ray direction is pointing to the face
  float dot_N_ray = VERTEX_TYPE::dot(ray.dir(), face_normal);


  if (dot_N_ray > DBL_EPSILON)
  {
    //WE HAVE A BACKFACE INTERSECTION !!!
    return true;
  }
  else
  {
    //WE DO NOT HAVE A BACKFACE INTERSECTION !!!
    return false;
  }
}



/**
 *  From Fast, minimum storage ray-triangle intersection.
 * Tomas Möller and Ben Trumbore.
 * Journal of Graphics Tools, 2(1):21--28, 1997
 */
template<class VERTEX_TYPE>
bool _TriaIntersecter<VERTEX_TYPE>::intersect(
    mrf::lighting::Ray const &ray,
    VERTEX_TYPE const &       v1,
    VERTEX_TYPE const &       v2,
    VERTEX_TYPE const &       v3)
{
  VERTEX_TYPE const E1 = v2 - v1;
  VERTEX_TYPE const E2 = v3 - v1;
  VERTEX_TYPE const P  = ray.dir().cross(E2);

  float const det = VERTEX_TYPE::dot(P, E1);

#ifdef MRF_BACK_FACE_CULLING
  if (det < EPSILON)
  {
    return false;
  }
#else
  if (mrf::math::Math::absVal(det) < RAYTRACING_EPSILON)
  {
    //std::cout << " NO INTERSECION DUE TO PARALLEL at " << __FILE__ << "  " << __LINE__ << std::endl;
    return false;
  }
#endif


  float const inv_det = 1.0f / det;

  VERTEX_TYPE const T = ray.origin() - v1;
  VERTEX_TYPE const Q = T.cross(E1);

  float const t_intersection = VERTEX_TYPE::dot(Q, E2) * inv_det;
  if ((t_intersection < ray.tMin()) || (t_intersection > ray.tMax()))
  {
    return false;
  }

  float const u = VERTEX_TYPE::dot(P, T) * inv_det;
  if ((u < 0.0f))
  {
    return false;
  }

  float const v = VERTEX_TYPE::dot(ray.dir(), Q) * inv_det;
  if ((v < 0.0f) || (u + v > 1.0f))
  {
    return false;
  }

  return true;
}


/**
 *  From Fast, minimum storage ray-triangle intersection.
 * Tomas Möller and Ben Trumbore.
 * Journal of Graphics Tools, 2(1):21--28, 1997
 */
template<class VERTEX_TYPE>
bool _TriaIntersecter<VERTEX_TYPE>::intersect(
    mrf::lighting::Ray const &  ray,
    VERTEX_TYPE const &         v1,
    VERTEX_TYPE const &         v2,
    VERTEX_TYPE const &         v3,
    _Intersection<VERTEX_TYPE> &hit_info)
{
  VERTEX_TYPE E1 = v2 - v1;
  VERTEX_TYPE E2 = v3 - v1;
  VERTEX_TYPE P  = ray.dir().cross(E2);


  float det = VERTEX_TYPE::dot(P, E1);
#ifdef MRF_BACK_FACE_CULLING
  if (det < EPSILON)
  {
    //std::cout << " BACKFACE CULLING NO INTERSECION DUE TO PARALLEL at " << __FILE__ << "  " << __LINE__ << std::endl;
    return false;
  }
#else
  if (mrf::math::Math::absVal(det) < DBL_EPSILON)
  {
#  ifdef INTERSECTION_DEBUG
    std::cout << " NO INTERSECION DUE TO PARALLEL at " << __FILE__ << "  " << __LINE__ << std::endl;
    assert(0);
#  endif
    return false;
  }
#endif


  float inv_det = 1.0f / det;

  VERTEX_TYPE T = ray.origin() - v1;
  VERTEX_TYPE Q = T.cross(E1);

  float t_intersection = VERTEX_TYPE::dot(Q, E2) * inv_det;
  if ((t_intersection < ray.tMin()) || (t_intersection > ray.tMax()))
  {
    //std::cout << " NO INTERSECION DUE TO parametric distance out of range   at " << __FILE__ << "  " << __LINE__ << std::endl;

    return false;
  }

  float u = VERTEX_TYPE::dot(P, T) * inv_det;
  if ((u < 0.0f) || (u > 1.0f))
  {
    // #ifdef INTERSECTION_DEBUG
    // std::cout << " NO INTERSECION DUE TO OUTSIDE ON u  at " << __FILE__ << "  " << __LINE__ << std::endl;
    // std::cout << " u = " << u << "  inv_det = " << inv_det << std::endl;
    // #endif

    return false;
  }

  float v = VERTEX_TYPE::dot(ray.dir(), Q) * inv_det;
  if ((v < 0.0f) || (u + v > 1.0f))
  {
    // #ifdef INTERSECTION_DEBUG
    // std::cout << " NO INTERSECION DUE TO OUTSIDE ON v  at " << __FILE__ << "  " << __LINE__ << std::endl;
    // std::cout << " v = " << v << "  inv_det = " << inv_det << std::endl;
    // #endif

    return false;
  }

  // OK WE HAVE AN INTERSECTION
  // Updating the Intersection structure
  hit_info.t     = t_intersection;
  hit_info.point = (1.0f - u - v) * v1 + u * v2 + v * v3;
  // hit_info.normal = VERTEX_TYPE::cross( E1, E2 );
  //    hit_info.normal.normalize();

  return true;
}


/**
 *  From Fast, minimum storage ray-triangle intersection.
 * Tomas Möller and Ben Trumbore.
 * Journal of Graphics Tools, 2(1):21--28, 1997
 */
template<class VERTEX_TYPE>
bool _TriaIntersecter<VERTEX_TYPE>::intersect(
    mrf::lighting::Ray const &  ray,
    VERTEX_TYPE const &         v1,
    VERTEX_TYPE const &         v2,
    VERTEX_TYPE const &         v3,
    VERTEX_TYPE const &         nv1,
    VERTEX_TYPE const &         nv2,
    VERTEX_TYPE const &         nv3,
    _Intersection<VERTEX_TYPE> &hit_info)
{
  std::cout << __LINE__ << " " << __FILE__ << std::endl;
  assert(0);


  VERTEX_TYPE E2  = v3 - v1;
  VERTEX_TYPE P   = ray.dir().cross(E2);
  VERTEX_TYPE E1  = v2 - v1;
  float       det = VERTEX_TYPE::dot(P, E1);
#ifdef MRF_BACK_FACE_CULLING
  if (det < EPSILON)
  {
    return false;
  }
#else
  if (mrf::math::Math::absVal(det) < DBL_EPSILON)
  {
    return false;
  }
#endif

  //     if( Math::absVal( det ) < DBL_EPSILON )
  //     {
  //         return false;
  //     }
  float inv_det = 1.0f / det;

  VERTEX_TYPE T = ray.origin() - v1;
  VERTEX_TYPE Q = T.cross(E1);

  float t_intersection = VERTEX_TYPE::dot(Q, E2) * inv_det;
  if ((t_intersection < ray.tMin()) || (t_intersection > ray.tMax()))
  {
    return false;
  }

  float u = VERTEX_TYPE::dot(P, T) * inv_det;
  if ((u < 0.0f) || (u > 1.0f))
  {
    return false;
  }

  float v = VERTEX_TYPE::dot(ray.dir(), Q) * inv_det;
  if ((v < 0.0f) || (u + v > 1.0f))
  {
    return false;
  }

  // OK WE HAVE AN INTERSECTION
  // Updating the Intersection structure
  hit_info.t      = t_intersection;
  hit_info.point  = (1.0f - u - v) * v1 + u * v2 + v * v3;
  hit_info.normal = (1.0f - u - v) * nv1 + u * nv2 + v * nv3;



  if (hit_info.normal.norm() < DBL_EPSILON)
  {
    //        std::cout << " here overhead !!! "  << std::endl;

    hit_info.normal = E1.cross(E2);

    if (hit_info.normal.norm() < DBL_EPSILON)
    {
      std::cout << "NORMAL INTERSECTION PROBLEM in " << __FILE__ << " PROBLEM " << __FUNCTION__
                << " Could not compute normal for face or by interpolation " << std::endl;
      return false;
    }
  }

  hit_info.normal.normalize();

  return true;
}


/**
 *  From Fast, minimum storage ray-triangle intersection.
 * Tomas Möller and Ben Trumbore.
 * Journal of Graphics Tools, 2(1):21--28, 1997
 */
template<class VERTEX_TYPE>
bool _TriaIntersecter<VERTEX_TYPE>::intersectWNBFC(
    mrf::lighting::Ray const &  ray,
    VERTEX_TYPE const &         v1,
    VERTEX_TYPE const &         v2,
    VERTEX_TYPE const &         v3,
    VERTEX_TYPE const &         nv1,
    VERTEX_TYPE const &         nv2,
    VERTEX_TYPE const &         nv3,
    _Intersection<VERTEX_TYPE> &hit_info)
{
  std::cout << __LINE__ << " " << __FILE__ << std::endl;
  assert(0);

  VERTEX_TYPE E2  = v3 - v1;
  VERTEX_TYPE P   = VERTEX_TYPE::cross(ray.dir(), E2);
  VERTEX_TYPE E1  = v2 - v1;
  float       det = VERTEX_TYPE::dot(P, E1);

  if (mrf::math::Math::absVal(det) < RAYTRACING_EPSILON)
  {
    return false;
  }
  float inv_det = 1.0f / det;

  VERTEX_TYPE T = ray.origin() - v1;
  VERTEX_TYPE Q = VERTEX_TYPE::cross(T, E1);

  float t_intersection = VERTEX_TYPE::dot(Q, E2) * inv_det;
  if ((t_intersection < ray.tMin()) || (t_intersection > ray.tMax()))
  {
    return false;
  }

  float u = VERTEX_TYPE::dot(P, T) * inv_det;
  if ((u < 0.0f) || (u > 1.0f))
  {
    return false;
  }

  float v = VERTEX_TYPE::dot(ray.dir(), Q) * inv_det;
  if ((v < 0.0f) || (u + v > 1.0f))
  {
    return false;
  }

  // OK WE HAVE AN INTERSECTION
  // Updating the Intersection structure
  hit_info.t      = t_intersection;
  hit_info.point  = (1.0f - u - v) * v1 + u * v2 + v * v3;
  hit_info.normal = (1.0f - u - v) * nv1 + u * nv2 + v * nv3;


  if (hit_info.normal.norm() < DBL_EPSILON)
  {
    hit_info.normal = VERTEX_TYPE::cross(E1, E2);

    if (hit_info.normal.norm() < DBL_EPSILON)
    {
      std::cout << "NORMAL INTERSECTION PROBLEM in " << __FILE__ << " PROBLEM " << __FUNCTION__
                << " Could not compute normal for face or by interpolation " << std::endl;
      return false;
    }
  }

  hit_info.normal.normalize();

  return true;
}



template<class VERTEX_TYPE>
bool _TriaIntersecter<VERTEX_TYPE>::intersectWNBFC(
    mrf::lighting::Ray const &ray,
    VERTEX_TYPE const &       v1,
    VERTEX_TYPE const &       v2,
    VERTEX_TYPE const &       v3,
    VERTEX_TYPE &             hit_point,
    float &                   w1,
    float &                   w2,
    float &                   w3,
    float &                   t)
{
  std::cout << __LINE__ << " " << __FILE__ << std::endl;
  assert(0);


  VERTEX_TYPE E1 = v2 - v1;
  VERTEX_TYPE E2 = v3 - v1;

  VERTEX_TYPE ne1 = (v2 - v1);
  VERTEX_TYPE ne2 = (v3 - v1);
  VERTEX_TYPE N   = ne1.cross(ne2);
  N.normalize();
  std::cout << __LINE__ << " " << __FILE__ << std::endl;
  assert(0);


  VERTEX_TYPE P   = (ray.dir()).cross(E2);
  float       det = VERTEX_TYPE::dot(P, E1);

#ifdef MRF_BACK_FACE_CULLING
  // This means that  det is close to zero or NEGATIVE
  if (det < RAYTRACING_EPSILON)
  {
    return false;
  }
#else
  // This means that the ray is to parallel to the triangle
  // So no intersection is possible
  if (mrf::math::Math::absVal(det) < RAYTRACING_EPSILON)
  {
    return false;
  }
#endif

  //float detN = VERTEX_TYPE::dot( N, ray.dir() );
  // if( Math::absVal(detN) < EPSILON)  //Grazing ray ?
  // {

  //     VERTEX_TYPE oc = (v1 - ray.origin());
  //     oc.normalize();

  //     float oc_dot_N = VERTEX_TYPE::dot( oc, N );

  //     if(  Math::absVal( (1 - oc_dot_N) ) <   EPSILON  )
  //     {
  //         return false;
  //     }
  //     else
  //     {
  //         return false;
  //     }
  // }


  float       inv_det = 1.0f / det;
  VERTEX_TYPE T       = ray.origin() - v1;
  VERTEX_TYPE Q       = T.cross(E1);

  float t_intersection = VERTEX_TYPE::dot(Q, E2) * inv_det;

  if ((t_intersection < ray.tMin()) || (t_intersection > ray.tMax()))
  {
    return false;
  }

  float u = VERTEX_TYPE::dot(P, T) * inv_det;
  if ((u < 0.0f) || (u > 1.0f))
  {
    return false;
  }

  float v = VERTEX_TYPE::dot(ray.dir(), Q) * inv_det;
  if ((v < 0.0f) || (u + v > 1.0f))
  {
    return false;
  }

  //Intersection occured
  //Fill the results
  t  = t_intersection;
  w1 = (1.0f - u - v);
  w2 = u;
  w3 = v;

  hit_point = w1 * v1 + w2 * v2 + w3 * v3;

  return true;
}




//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
