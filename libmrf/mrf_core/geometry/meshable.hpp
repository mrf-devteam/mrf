/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace geom
{
//Early Declaration
template<class VEC3_TYPE>
class _Mesh;

typedef _Mesh<mrf::math::Vec3f> Mesh;

/**
 * Defines the interface every objects must implement to
 * be transformed into a mrf::geom::_Mesh
 **/

class MRF_CORE_EXPORT Meshable
{
protected:
public:
  Meshable() {}

  virtual ~Meshable() {}

  virtual mrf::geom::Mesh createMesh() const = 0;
};

}   // namespace geom

}   // namespace mrf