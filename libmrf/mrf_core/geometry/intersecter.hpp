/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>


#ifdef DEBUG
#  include <iostream>
#endif


//MRF
#include <mrf_core/math/math.hpp>
#include <mrf_core/lighting/ray.hpp>
#include <mrf_core/lighting/particle.hpp>

#include <mrf_core/geometry/aabbox.hpp>
#include <mrf_core/geometry/computation.hpp>

namespace mrf
{
namespace geom
{
template<class VERTEX_TYPE>
class _Intersection
{
public:
  VERTEX_TYPE point;
  VERTEX_TYPE normal;
  VERTEX_TYPE tangent;

  float        t;
  unsigned int material;
  unsigned int object;

  //Texture coordinates
  mrf::math::Vec2f uv;


  //TODO : Change me so we used LocalFrame instead of point, normal, and uv
  _Intersection()
  {
    t       = INFINITY;
    tangent = VERTEX_TYPE(1.0f, 0.0f, 0.0f);
  }


  //Operators
  _Intersection &operator=(_Intersection const &n_inter);
};

typedef _Intersection<mrf::math::Vec3f> Intersection;
#include "intersecter.cxx"


template<class VERTEX_TYPE>
class _TriaIntersecter
{
public:
  /**
   * Compute the intersection between a given Ray
   * and a triangle defined by the three given vertices
   *
   * Returns whether or not the intersection occured.
   * If the ray does intersect the triangle,
   * the given parameter hit_point will be set to be the intersection
   * and w1, w2 and w3 its barycentric coordinates according to
   * v1, v2 and v3
   *
   * t is the parametric distance along the ray where
   * the intersection occured
   *
   **/
  static bool intersect(
      mrf::lighting::Ray const &ray,
      VERTEX_TYPE const &       v1,
      VERTEX_TYPE const &       v2,
      VERTEX_TYPE const &       v3,
      VERTEX_TYPE &             hit_point,
      float &                   u,
      float &                   v,
      float &                   w,
      float &                   t);



  // v1 v2 and v3 define the triangle
  static bool
  intersect(mrf::lighting::Ray const &ray, VERTEX_TYPE const &v1, VERTEX_TYPE const &v2, VERTEX_TYPE const &v3);


  /**
   * Does this ray intersects the Triangle back facelly ?
   **/
  static bool intersectBackFace(
      mrf::lighting::Ray const &ray,
      VERTEX_TYPE const &       v1,
      VERTEX_TYPE const &       v2,
      VERTEX_TYPE const &       v3,
      VERTEX_TYPE const &       nv1,
      VERTEX_TYPE const &       nv2,
      VERTEX_TYPE const &       nv3);

  static bool intersectWNBFC(
      mrf::lighting::Ray const &  ray,
      VERTEX_TYPE const &         v1,
      VERTEX_TYPE const &         v2,
      VERTEX_TYPE const &         v3,
      VERTEX_TYPE const &         nv1,
      VERTEX_TYPE const &         nv2,
      VERTEX_TYPE const &         nv3,
      _Intersection<VERTEX_TYPE> &hit_info);

  static bool intersectWNBFC(
      mrf::lighting::Ray const &ray,
      VERTEX_TYPE const &       v1,
      VERTEX_TYPE const &       v2,
      VERTEX_TYPE const &       v3,
      VERTEX_TYPE &             hit_point,
      float &                   u,
      float &                   v,
      float &                   w,
      float &                   t);



  //The Normal used will be the face one
  static bool intersect(
      mrf::lighting::Ray const &  ray,
      VERTEX_TYPE const &         v1,
      VERTEX_TYPE const &         v2,
      VERTEX_TYPE const &         v3,
      _Intersection<VERTEX_TYPE> &hit_info);


  static bool intersect(
      mrf::lighting::Ray const &  ray,
      VERTEX_TYPE const &         v1,
      VERTEX_TYPE const &         v2,
      VERTEX_TYPE const &         v3,
      VERTEX_TYPE const &         nv1,   //normal of vertex 1
      VERTEX_TYPE const &         nv2,   //normal of vertex 2
      VERTEX_TYPE const &         nv3,   //normal of vertex 3
      _Intersection<VERTEX_TYPE> &hit_info);
};

typedef _TriaIntersecter<mrf::math::Vec3f> TriaIntersecter;
#include "triangle_intersecter.cxx"


template<class VERTEX_TYPE>
class _AABBoxIntersecter
{
public:
  /**
   *
   * Test wheter or not a ray given by its direction and origin
   * intersects the AABBox defines
   *
   * The t_min and t_max are filled if any intersection occured
   *
   **/
  static bool intersect(
      VERTEX_TYPE const &bbox_min,
      VERTEX_TYPE const &bbox_max,
      VERTEX_TYPE const &ray_origin,
      VERTEX_TYPE const &ray_dir,
      float &            t_min,
      float &            t_max);

  /**
   * Test whether or not the bbox defined by the given min and
   * max  points is intersected by the given ray.
   * The method checks whether the intersection occurs between
   * ray.tmin and ray.tmax parametric distances
   *
   * The normal of the hitInfo structure is not updated
   **/
  static bool intersect(
      VERTEX_TYPE const &       box_min,
      VERTEX_TYPE const &       box_max,
      mrf::lighting::Ray const &ray,
      Intersection &            hitInfo,
      float *                   t_inter_max = NULL);

  static bool intersect(
      VERTEX_TYPE const &            box_min,
      VERTEX_TYPE const &            box_max,
      mrf::lighting::Particle const &particle,
      Intersection &                 hitInfo,
      float *                        t_inter_max = NULL);

  static bool intersectSegment(
      VERTEX_TYPE const &box_min,
      VERTEX_TYPE const &box_max,
      VERTEX_TYPE const &min_seg,
      VERTEX_TYPE const &max_seg);

  /**
   * Returns whether or not the given aabbox intersects
   * the triangle define by the given vertices v1,v2,v3.
   **/
  static bool intersectTriangle(AABBox const &box, VERTEX_TYPE const &v1, VERTEX_TYPE const &v2, VERTEX_TYPE const &v3);
};

typedef _AABBoxIntersecter<mrf::math::Vec3f> AABBoxIntersecter;
#include "aabbox_intersecter.cxx"


//External Operator
template<typename T>
inline std::ostream &operator<<(std::ostream &os, _Intersection<T> const &hit_info)
{
  return os << " Intersection [point = " << hit_info.point << " normal =  " << hit_info.normal
            << " tangent = " << hit_info.tangent << " t = " << hit_info.t << " object id =  " << hit_info.object
            << "  material id = " << hit_info.material << std::endl;
}



}   // namespace geom
}   // namespace mrf
