/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/lighting/i_light.hpp>

namespace mrf
{
namespace geom
{
/**
 * A class which represents a direction which implements the
 * ILight interface to be able to create directional light source
 *
 * The AABBox of a direction is set as infinite
 *
 **/
class MRF_CORE_EXPORT Direction: public mrf::lighting::ILight
{
protected:
  mrf::math::Vec3f _direction;
  MRF_AABBox       _aabbox;

public:
  Direction(float x, float y, float z);
  Direction(mrf::math::Vec3f const &direction);

  inline mrf::math::Vec3f const &value() const;
  inline mrf::math::Vec3f        center() const;

  virtual ~Direction();

  //-------------------------------------------------------------------------
  // ILight interface implementation
  //-------------------------------------------------------------------------
  virtual mrf::geom::MRF_AABBox &bbox();
  virtual float                  surfaceArea() const;

  virtual float surfaceSampling(float random_var_1, float random_var_2, mrf::geom::LocalFrame &light_local_frame) const;

  //          virtual mrf::geom::LocalFrame const & localFrame() const;


  virtual float lightGeometricFactor(mrf::math::Vec3f const &light_vector) const;

  virtual mrf::math::Vec3f lightVector(
      mrf::math::Vec3f const &point_shaded,
      mrf::math::Vec3f const &light_position,
      float &                 distance_point_light,
      float &                 square_distance_point_light) const;
};


inline mrf::math::Vec3f Direction::center() const
{
  return mrf::math::Vec3f();
}


inline mrf::math::Vec3f const &Direction::value() const
{
  return _direction;
}
}   // namespace geom
}   // namespace mrf
