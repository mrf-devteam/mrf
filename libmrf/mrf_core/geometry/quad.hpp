/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/vec3.hpp>
#include <mrf_core/lighting/i_light.hpp>
#include <mrf_core/geometry/shape.hpp>
#include <mrf_core/geometry/meshable.hpp>

namespace mrf
{
namespace geom
{
class MRF_CORE_EXPORT Quad
{
public:
  virtual ~Quad() {}
  virtual mrf::math::Vec3f normal() const = 0;
  virtual mrf::math::Reff &localRef()     = 0;

  virtual float length() const = 0;
  virtual float height() const = 0;

  virtual float height2() const = 0;
  virtual float length2() const = 0;

  virtual std::vector<mrf::math::Vec3f> getVertices() const = 0;
};



/**
 * A Rectangular Quad define by its length and its width
 * Its normal is assumed to be the Z vector of its localFrame
 *
 **/
class MRF_CORE_EXPORT RQuad
  : public Shape
  , public mrf::lighting::ILight
  , public Meshable
  , public Intersectable
{
protected:
  float _length;
  float _height;

  //A Quad defines a Plane which equation is :
  // ax + by + cz + d= 0
  // where the normal n of the plane is defined as (a,b,c)

  //This the value of d  = - dot(n , quad_position)
  float _d;

  //Half of width and height
  float _width_2;
  float _height_2;


public:
  RQuad(
      float                   pos_x,
      float                   pos_y,
      float                   pos_z,
      mrf::math::Vec3f const &normal         = mrf::math::Vec3f(0.0f, 0.0f, 1.0f),
      float                   length         = 1.0f,
      float                   height         = 1.0f,
      unsigned int            material_index = 0);

  RQuad(
      mrf::math::Vec3f const &origin,
      mrf::math::Vec3f const &down_right_corner,
      mrf::math::Vec3f const &up_right_corner,
      unsigned int            material_index);

  RQuad(
      mrf::math::Vec3f const &v1,
      mrf::math::Vec3f const &v2,
      mrf::math::Vec3f const &v3,
      mrf::math::Vec3f const &v4,
      unsigned int            material_index);

  RQuad(
      mrf::math::Reff &ref,
      mrf::math::Vec3f scale,
      float            length         = 1.0f,
      float            height         = 1.0f,
      unsigned int     material_index = 0);


  virtual ~RQuad();


  inline mrf::math::Vec3f normal() const;
  inline float            length() const;
  inline float            height() const;

  /**
   * Returns the height of the RQuad divided by two
   **/
  inline float height2() const;

  /**
   * Returns the width of the RQuad divided by two
   **/
  inline float length2() const;

  //inline mrf::math::Reff const & localRef() const ;

  virtual std::vector<mrf::math::Vec3f> getVertices() const;


  //-------------------------------------------------------------------------
  // Meshable Interface implementation
  //-------------------------------------------------------------------------

  virtual mrf::geom::Mesh createMesh() const;

  //-------------------------------------------------------------------------
  // Intersectable Interface implementation
  //-------------------------------------------------------------------------
  virtual bool isIntersected(mrf::lighting::Ray const &r) const;


  virtual bool isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info) const;

  virtual bool isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int sub_object_id) const;

  virtual bool isIntersected(mrf::lighting::Ray const &r, unsigned int sub_object_id) const;


  virtual bool isIntersectedWNBFC(mrf::lighting::Ray const &r, Intersection &hit_info) const;


  virtual bool
  isIntersectedWNBFC(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int sub_object_id) const;



  virtual bool isIntersected(MRF_AABBox const &box, unsigned index_face) const;
  virtual bool isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max, unsigned index_face) const;

  virtual bool isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max) const;

  virtual mrf::geom::MRF_AABBox const &bbox() const;
  virtual mrf::geom::MRF_AABBox &      bbox();


  /**
   * Since there is no sub objects in the Quad this methods
   * does NOT FILL the objects vector
   *
   **/
  virtual bool
  isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max, std::vector<unsigned int> &objects) const;

  /**
   * Since there is no sub objects in the Quad this methods
   * does NOT FILL the objects vector
   *
   **/
  virtual bool isIntersected(MRF_AABBox const &box, std::vector<unsigned int> &objects) const;



  virtual unsigned int intersectionCost() const;
  virtual unsigned int memorySize() const;
  virtual unsigned int numSubObjects() const;
  virtual bool         hasSubObjects() const;
  virtual MRF_AABBox   subObjectAABBox(unsigned int subobj_index) const;

  //-------------------------------------------------------------------------
  // ILight interface implementation
  //-------------------------------------------------------------------------
  //virtual mrf::geom::AABBox& bbox() ;
  virtual mrf::math::Vec3f center() const;

  virtual float surfaceArea() const;

  virtual float surfaceSampling(float random_var_1, float random_var_2, mrf::geom::LocalFrame &light_local_frame) const;

  //          virtual mrf::geom::LocalFrame const & localFrame() const;


  virtual float lightGeometricFactor(mrf::math::Vec3f const &light_vector) const;

  virtual mrf::math::Vec3f lightVector(
      mrf::math::Vec3f const &point_shaded,
      mrf::math::Vec3f const &light_position,
      float &                 distance_point_light,
      float &                 square_distance_point_light) const;



  //-------------------------------------------------------------------------
  mrf::math::Vec2f uvByPosition(mrf::math::Vec3f const &position) const;


  bool isInside(mrf::math::Vec3f const &point) const;
};

inline float RQuad::length() const
{
  return _length;
}

inline float RQuad::height() const
{
  return _height;
}

inline mrf::math::Vec3f RQuad::normal() const
{
  return _cs.getOrientation().getAxisZ();
}


inline float RQuad::height2() const
{
  return _height_2;
}

inline float RQuad::length2() const
{
  return _width_2;
}
/*
inline mrf::math::Reff const &
RQuad::localRef() const
{
  return Shape::localRef();
}
*/


}   // namespace geom
}   // namespace mrf
