#include <mrf_core/geometry/direction.hpp>

namespace mrf
{
namespace geom
{
using namespace mrf::math;


Direction::Direction(float x, float y, float z)
  : _direction(x, y, z)
  , _aabbox(MRF_AABBox(Vec3f(-INFINITY, -INFINITY, -INFINITY), Vec3f(INFINITY, INFINITY, INFINITY)))
{
  _direction.normalEq();
}

Direction::Direction(Vec3f const &direction): _direction(direction) {}



Direction::~Direction() {}


//-------------------------------------------------------------------------
// ILight interface implementation
//-------------------------------------------------------------------------

mrf::geom::MRF_AABBox &Direction::bbox()
{
  return _aabbox;
}

float Direction::surfaceArea() const
{
  return 1.0f;
}


// For a directional light this has not a real meaning
float Direction::surfaceSampling(
    float /*random_var_1*/,
    float /*random_var_2*/,
    mrf::geom::LocalFrame & /*light_local_frame*/) const
{
  //This has not a real meaning for a directional light source
  //Therefore, We do not do anything with light_local_frame

  // Since the area is 1.0 the probability to choose this point
  // is also always 1.0
  return 1.0f;
}


//       //TODO : FIXE ME !!!
//       LocalFrame const &
//       Direction::localFrame() const
//       {
//          //return new LocalFrame();
//       }


float Direction::lightGeometricFactor(mrf::math::Vec3f const & /*light_vector*/) const
{
  // For a Directional Light source this factor is always equal to 1
  // because a point like a sphere as every possible normals
  return 1.0f;
}



mrf::math::Vec3f Direction::lightVector(
    Vec3f const & /*point_shaded*/,
    Vec3f const & /*light_position*/,
    float &distance_point_light,
    float &square_distance_point_light) const
{
  //Light is supposed to be at infinity ...
  distance_point_light = INFINITY;

  //... BUT since the next value is used to compute the
  // geometric factor we set it to 1.0
  square_distance_point_light = 1.0f;

  return (-_direction);
}



}   // namespace geom
}   // namespace mrf
