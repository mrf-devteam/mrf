


//-----------------------------------------------------------------------
// Constructors
//-----------------------------------------------------------------------
template<class VEC3_TYPE>
_Mesh<VEC3_TYPE>::_Mesh()
{}

template<class VEC3_TYPE>
_Mesh<VEC3_TYPE>::_Mesh(
    std::vector<VEC3_TYPE> const &   vertices,
    std::vector<VEC3_TYPE> const &   normals,
    std::vector<VEC3_TYPE> const &   uvw,
    std::vector<unsigned int> const &face_indexes,
    std::vector<unsigned int> const &material_per_faces)
{
  std::vector<VEC3_TYPE> tangents;
  generateTangentsPerVertex(vertices, normals, uvw, face_indexes, tangents);

  initializeMeshData(vertices, normals, uvw, tangents, face_indexes, material_per_faces);
}

template<class VEC3_TYPE>
_Mesh<VEC3_TYPE>::_Mesh(
    std::vector<VEC3_TYPE> const &   vertices,
    std::vector<VEC3_TYPE> const &   normals,
    std::vector<VEC3_TYPE> const &   uvw,
    std::vector<unsigned int> const &face_indexes,
    std::vector<unsigned int> const &material_per_faces,
    enum TangentMethods const &      tangent_method)
{
  std::vector<VEC3_TYPE> tangents;
  this->_tangent_method = tangent_method;
  generateTangentsPerVertex(vertices, normals, uvw, face_indexes, tangents);


  initializeMeshData(vertices, normals, uvw, tangents, face_indexes, material_per_faces);
}

template<class VEC3_TYPE>
void _Mesh<VEC3_TYPE>::generateTangentsPerVertex(
    std::vector<VEC3_TYPE> const &   vertices,
    std::vector<VEC3_TYPE> const &   normals,
    std::vector<VEC3_TYPE> const &   uvw,
    std::vector<unsigned int> const &face_indexes,
    std::vector<VEC3_TYPE> &         tangents)
{
  tangents.clear();

//TODO use logging system
#ifdef MRF_GEOM_MESH_DEBUG
  std::cout << " [DEBUG] COMPUTING INTIAL TANGENTS FROM NORMALS " << std::endl;
#endif

  enum TangentMethods tangent_method = this->getTangentMethod();
  tangent_method                     = withProjection;

  if (tangent_method == withUV)
  {
    computeTangentsFromUV(vertices, normals, uvw, face_indexes, tangents);
  }
  else
  {
    for (unsigned int i = 0; i < vertices.size(); i++)
    {
      //Variant 1: A more robust strategy than just assigning a fixed value
      //tangents.push_back( vertices[i].generateOrthogonal() );

      //Variant 2:
      // TODO: Test it
      VEC3_TYPE const Y(0.0, 1.0, 0.0);

      //Variant 3: TODO    http://www.imm.dtu.dk/~jerf/papers/abstracts/onb.html

      //If the normal is colinear to Y then the following code will not work
      double dot_Y_N = Y.dot(normals[i]);

      if (dot_Y_N >= 1.0 || dot_Y_N <= -1.0)
      {
        VEC3_TYPE const new_tangent(1.0, 0.0, 0.0);

        tangents.push_back(new_tangent);
      }
      else
      {
        // VEC3_TYPE new_tangent = (Y -  Y.dot(normals[i])*normals[i]).normalize(); --> This cannot work unless you are in a local Frame
        VEC3_TYPE new_tangent;
        VEC3_TYPE new_bitangent;

        switch (tangent_method)
        {
          case withUV:
            mrf::math::Vec3<float>::generateOrthogonalWithUV(normals[i], new_tangent, new_bitangent);
            break;
          case withProjection:
            mrf::math::Vec3<float>::generateOrthogonalWithProjection(normals[i], new_tangent, new_bitangent);
            break;
          case withJCGT:
            mrf::math::Vec3<float>::generateOrthogonalWithJCGT(normals[i], new_tangent, new_bitangent);
            break;
          case asBRDFExplorer:
            mrf::math::Vec3<float>::generateOrthogonalAsBRDFExplorer(normals[i], new_tangent, new_bitangent);
            break;
          default:
            mrf::math::Vec3<float>::generateOrthogonalWithUV(normals[i], new_tangent, new_bitangent);
            break;
        }

        new_tangent.normalize();


        //assert(mrf::math::equals(new_tangent.dot(normals[i]), 0.0f, 0.00001f));
        if (!mrf::math::equals(new_tangent.dot(normals[i]), 0.0f, 0.00001f))
        {
          mrf::gui::fb::Loger::getInstance()->debug(
              " [ " + std::to_string(new_tangent.x()) + "; " + std::to_string(new_tangent.y()) + "; "
              + std::to_string(new_tangent.z()) + " ] ");
        }
        //assert(!new_tangent.isInfOrNan());

        tangents.push_back(new_tangent);
      }
    }
  }
  assert(tangents.size() == vertices.size());

#ifdef MRF_GEOM_MESH_DEBUG
  std::cout << " [DEBUG] ... DONE " << std::endl;
#endif
}

template<class VEC3_TYPE>
_Mesh<VEC3_TYPE>::_Mesh(
    std::vector<VEC3_TYPE> const &   vertices,
    std::vector<VEC3_TYPE> const &   normals,
    std::vector<VEC3_TYPE> const &   uvw,
    std::vector<VEC3_TYPE> const &   tangents,
    std::vector<unsigned int> const &face_indexes,
    std::vector<unsigned int> const &material_per_faces)
{
  initializeMeshData(vertices, normals, uvw, tangents, face_indexes, material_per_faces);
}

template<class VEC3_TYPE>
_Mesh<VEC3_TYPE>::_Mesh(
    std::vector<VEC3_TYPE> const &   vertices,
    std::vector<VEC3_TYPE> const &   normals,
    std::vector<VEC3_TYPE> const &   uvw,
    std::vector<VEC3_TYPE> const &   tangents,
    std::vector<unsigned int> const &face_indexes,
    std::vector<unsigned int> const &material_per_faces,
    enum TangentMethods const &      tangent_method)
{
  this->_tangent_method = tangent_method;
  initializeMeshData(vertices, normals, uvw, tangents, face_indexes, material_per_faces);
}

template<class VEC3_TYPE>
_Mesh<VEC3_TYPE>::_Mesh(
    std::vector<VEC3_TYPE> &&   vertices,
    std::vector<VEC3_TYPE> &&   normals,
    std::vector<VEC3_TYPE> &&   uvw,
    std::vector<VEC3_TYPE> &&   tangents,
    std::vector<unsigned int> &&face_indexes,
    std::vector<unsigned int> &&material_per_faces)
{
  initializeMeshData(vertices, normals, uvw, tangents, face_indexes, material_per_faces);
}

template<class VEC3_TYPE>
_Mesh<VEC3_TYPE>::_Mesh(_Mesh<VEC3_TYPE> const &cpyMesh)
{
  initializeMeshData(
      cpyMesh._vertices,
      cpyMesh._npv,
      cpyMesh._uvs,
      cpyMesh._tpv,
      cpyMesh._faces,
      cpyMesh._materialPerFaces);
}

//-----------------------------------------------------------------------
// Accessors
//-----------------------------------------------------------------------
template<class VEC3_TYPE>
std::vector<VEC3_TYPE> const &_Mesh<VEC3_TYPE>::vertices() const
{
  return _vertices;
}

template<class VEC3_TYPE>
std::vector<VEC3_TYPE> const &_Mesh<VEC3_TYPE>::normals() const
{
  return _npv;
}

template<class VEC3_TYPE>
std::vector<VEC3_TYPE> const &_Mesh<VEC3_TYPE>::tangents() const
{
  return _tpv;
}

template<class VEC3_TYPE>
std::vector<VEC3_TYPE> const &_Mesh<VEC3_TYPE>::uvs() const
{
  return _uvs;
}


template<class VEC3_TYPE>
std::vector<unsigned int> const &_Mesh<VEC3_TYPE>::faces() const
{
  return _faces;
}

template<class VEC3_TYPE>
std::vector<unsigned int> const &_Mesh<VEC3_TYPE>::materialPerFaces() const
{
  return _materialPerFaces;
}

template<class VEC3_TYPE>
mrf::geom::MRF_AABBox const &_Mesh<VEC3_TYPE>::bbox() const
{
  return _bbox;
}

template<class VEC3_TYPE>
std::string const &_Mesh<VEC3_TYPE>::absoluteFilepath() const
{
  return _mesh_absolute_filepath;
}

template<class VEC3_TYPE>
std::string &_Mesh<VEC3_TYPE>::absoluteFilepath()
{
  return _mesh_absolute_filepath;
}



//-----------------------------------------------------------------------
//Operator
//-----------------------------------------------------------------------
// template< class VEC3_TYPE >
// _Mesh<VEC3_TYPE> &
// _Mesh<VEC3_TYPE>::operator<< ( _Mesh<VEC3_TYPE> const & a_mesh) //Merge two _Mesh
// {
//   /*
//    int ver_size = _vertices.size();
//    int face_size = _faces.size();

//    //Merge
//    _vertices.insert(         _vertices.end(),          a_mesh._vertices.begin(),            a_mesh._vertices.end() );
//    _npv.insert(              _npv.end(),               a_mesh._npv.begin(),                 a_mesh._npv.end());
//    _tpv.insert(              _tpv.end(),               a_mesh._tpv.begin(),                 a_mesh._tpv.end());
//    _uvs.insert(              _uvs.end(),               a_mesh._uvs.begin(),                 a_mesh._uvs.end());
//    _faces.insert(            _faces.end(),             a_mesh._faces.begin(),               a_mesh._faces.end());
//    _materialPerFaces.insert( _materialPerFaces.end(),  a_mesh._materialPerFaces.begin(),    a_mesh._materialPerFaces.end());

//    //Fixing faces
//    for(unsigned int i = face_size; i<_faces.size(); ++i)
//    {
//      //_faces[i] = std::make_tuple( std::get<0>(_faces[i]) + ver_size, std::get<1>(_faces[i]) + ver_size, std::get<2>(_faces[i]) + ver_size);
//      _faces[i] = std::make_tuple(std::get<0>(_faces[i]) + ver_size, std::get<1>(_faces[i]) + ver_size, std::get<2>(_faces[i]) + ver_size);
//    }

//    updateAABBox();
//    */
//   return (*this);
// }


template<class VEC3_TYPE>
template<class OTHER_VEC3_TYPE>
_Mesh<VEC3_TYPE> &_Mesh<VEC3_TYPE>::operator=(_Mesh<OTHER_VEC3_TYPE> const &a_mesh)
{
  assert(a_mesh._vertices.size() == a_mesh._npv.size());
  assert(a_mesh._vertices.size() == a_mesh._tpv.size());
  assert(a_mesh._tpv.size() == a_mesh._uvs.size());

  for (unsigned int i = 0; i < a_mesh._vertices.size(); ++i)
  {
    for (int j = 0; j < 3; ++j)
    {
      _vertices[i][j] = a_mesh._vertices[i][j];
      _npv[i][j]      = a_mesh._npv[i][j];
      _tpv[i][j]      = a_mesh._tpv[i][j];
      _uvs[i][j]      = a_mesh._uvs[i][j];
    }
  }

  _faces.resize(a_mesh._faces.size());
  std::copy(a_mesh._faces.begin(), a_mesh._faces.end(), _faces.begin());

  _materialPerFaces.resize(a_mesh._materialPerFaces.size());
  std::copy(a_mesh._materialPerFaces.begin(), a_mesh._materialPerFaces.end(), _materialPerFaces.begin());

  updateAABBox();

  _mesh_absolute_filepath = a_mesh._mesh_absolute_filepath;

  return (*this);
}

//-----------------------------------------------------------------------
//Private Function to initialize Mesh Data
//-----------------------------------------------------------------------
template<class VEC3_TYPE>
void _Mesh<VEC3_TYPE>::initializeMeshData(
    std::vector<VEC3_TYPE> const &   vertices,
    std::vector<VEC3_TYPE> const &   normals,
    std::vector<VEC3_TYPE> const &   uvw,
    std::vector<VEC3_TYPE> const &   tangents,
    std::vector<unsigned int> const &face_indexes,
    std::vector<unsigned int> const &material_per_faces)
{
  assert(vertices.size() == normals.size());
  assert(vertices.size() == tangents.size());
  assert(tangents.size() == uvw.size());
  assert(face_indexes.size() == 3 * material_per_faces.size());

  //Copy everything
  _vertices.resize(vertices.size());
  std::copy(vertices.begin(), vertices.end(), _vertices.begin());

  _npv.resize(normals.size());
  std::copy(normals.begin(), normals.end(), _npv.begin());

  _uvs.resize(uvw.size());
  std::copy(uvw.begin(), uvw.end(), _uvs.begin());

  _tpv.resize(tangents.size());
  std::copy(tangents.begin(), tangents.end(), _tpv.begin());

  checkNormalTangentOrthogonality();

  //Material per faces
  _materialPerFaces.resize(material_per_faces.size());
  std::copy(material_per_faces.begin(), material_per_faces.end(), _materialPerFaces.begin());

  //Faces
  _faces.resize(face_indexes.size());
  std::copy(face_indexes.begin(), face_indexes.end(), _faces.begin());


  //Now Update th Axis-Aligned Bounding Box
  updateAABBox();
}

//-----------------------------------------------------------------------
//Private Function to initialize Mesh Data
//-----------------------------------------------------------------------
template<class VEC3_TYPE>
void _Mesh<VEC3_TYPE>::initializeMeshData(
    std::vector<VEC3_TYPE> &&   vertices,
    std::vector<VEC3_TYPE> &&   normals,
    std::vector<VEC3_TYPE> &&   uvw,
    std::vector<VEC3_TYPE> &&   tangents,
    std::vector<unsigned int> &&face_indexes,
    std::vector<unsigned int> &&material_per_faces)
{
  assert(vertices.size() == normals.size());
  assert(vertices.size() == tangents.size());
  assert(tangents.size() == uvw.size());
  assert(face_indexes.size() == 3 * material_per_faces.size());

  //Move everything
  _vertices = std::move(vertices);

  _npv = std::move(normals);

  _uvs = std::move(uvw);

  _tpv = std::move(tangents);

  checkNormalTangentOrthogonality();

  //Material per faces
  _materialPerFaces = std::move(material_per_faces);


#ifdef MRF_GEOM_MESH_DEBUG
  for (unsigned int i = 0; i < 45; i++)
  {
    //std::cout << " _npv[" << i << "] = " << _npv[i] << std::endl;
    std::cout << " _tpv[" << i << "] = " << _tpv[i] << std::endl;
  }
#endif

  //Faces
  _faces = std::move(face_indexes);

  //Now Update th Axis-Aligned Bounding Box
  updateAABBox();
}


template<class VEC3_TYPE>
void _Mesh<VEC3_TYPE>::checkNormalTangentOrthogonality()
{
#ifdef MRF_GEOM_MESH_DEBUG
  std::cout << " ENTERING checkNormalTangentOrthogonality" << std::endl;
#endif

  using namespace std;
  unsigned int nb_invalid_tangents = 0;
  unsigned int nb_invalid_normals  = 0;

  //TODO use logging system
  //std::cout << "[INFO] Checking Normal and Tangent Orthogonality in Mesh.cxx " << std::endl;

  //This loop could be parallelized
  for (unsigned int i = 0; i < _npv.size(); i++)
  {
#ifdef _MSC_VER
#  pragma warning(disable : 4244)
#endif
    double const normal_norm = _npv[i].norm();
    if (!mrf::math::equals(normal_norm, 1.0, 0.00001))
    {
      _npv[i] /= normal_norm;
      nb_invalid_normals++;
    }


    double const tangent_norm = _tpv[i].norm();
    if (!mrf::math::equals(tangent_norm, 1.0, 0.00001))
    {
      _tpv[i] /= tangent_norm;
    }
#ifdef _MSC_VER
#  pragma warning(default : 4244)
#endif

    double const dot_NT = _npv[i].dot(_tpv[i]);

    if (!mrf::math::equals(dot_NT, 0.0, 0.00001))
    {
#ifdef MRF_GEOM_MESH_DEBUG

      std::cout << "  Index of Vertex = " << i << std::endl;
      cout << " Current Normal = " << _npv[i] << " current normal norm = " << _npv[i].norm() << std::endl;
      cout << " Current Tangent = " << _tpv[i] << endl;
      cout << " dot_NT = " << dot_NT << endl;
#endif

      VEC3_TYPE const bi_tangent  = _tpv[i].cross(_npv[i]).normalize();
      VEC3_TYPE const new_tangent = _npv[i].cross(bi_tangent);

#ifdef MRF_GEOM_MESH_DEBUG
      cout << " New tangent = " << new_tangent << " new_tangent.norm = " << new_tangent.norm();
      cout << " Normal = " << _npv[i] << " normal.norm = " << _npv[i].norm() << std::endl;
      cout << "  New dot_NT = " << _npv[i].dot(new_tangent) << std::endl;
#endif

      _tpv[i] = new_tangent;
      nb_invalid_tangents++;
    }

    // std::cout << "_tpv[" << i << "] = " << _tpv[i] << " _npv[i] = " << _npv[i] << std::endl;
    // std::cout << " Value tested = " << _tpv[i].dot(_npv[i]) << std::endl;

    //assert(mrf::math::equals(_tpv[i].dot(_npv[i]), 0.0f, 0.0001f));
  }

#ifdef MRF_GEOM_MESH_DEBUG
  if (nb_invalid_normals > 0)
  {
    //TODO use logging system
    cout << " [WARN] " << nb_invalid_normals << "/" << _npv.size() << " invalid normals detected and corrected "
         << endl;
  }

  if (nb_invalid_tangents > 0)
  {
    //TODO use logging system
    cout << " [WARN] " << nb_invalid_tangents << "/" << _tpv.size() << " invalid tangents detected and corrected "
         << endl;
  }
#endif
}


template<class VEC3_TYPE>
MRF_AABBox _Mesh<VEC3_TYPE>::aabboxFromFace(unsigned int index_face) const
{
  /*
  VEC3_TYPE const & v1  = _vertices[std::get<0>(_faces[ index_face ]) ];
  VEC3_TYPE const & v2  = _vertices[std::get<1>(_faces[ index_face ]) ];
  VEC3_TYPE const & v3  = _vertices[std::get<2>(_faces[ index_face ]) ];
  */

  VEC3_TYPE const &v1 = _vertices[(_faces[3 * index_face])];
  VEC3_TYPE const &v2 = _vertices[(_faces[3 * index_face + 1])];
  VEC3_TYPE const &v3 = _vertices[(_faces[3 * index_face + 2])];


  VEC3_TYPE min, max;
  min[0] = mrf::math::Math::min(v1[0], v2[0], v3[0]);
  min[1] = mrf::math::Math::min(v1[1], v2[1], v3[1]);
  min[2] = mrf::math::Math::min(v1[2], v2[2], v3[2]);

  max[0] = mrf::math::Math::max(v1[0], v2[0], v3[0]);
  max[1] = mrf::math::Math::max(v1[1], v2[1], v3[1]);
  max[2] = mrf::math::Math::max(v1[2], v2[2], v3[2]);

  return MRF_AABBox(min, max);
}


template<class VEC3_TYPE>
void _Mesh<VEC3_TYPE>::updateAABBox()
{
  //Compute Axis Aligned Bounding Box
  //Reset The AABBox
  _bbox              = AABBox();
  AABBox testing_box = AABBox();
  for (unsigned int i = 0; i < numSubObjects(); i++)
  {
    MRF_AABBox face_bbox = aabboxFromFace(i);
    _bbox                = _bbox.createUnion(face_bbox);
  }
}

template<class VEC3_TYPE>
template<typename T>
VEC3_TYPE _Mesh<VEC3_TYPE>::interpolateNormal(unsigned int face_index, T u, T v, T w) const
{
  /*
  return u*_npv[std::get<0>(_faces[face_index]) ] +
         v*_npv[std::get<1>(_faces[face_index ]) ] +
         w*_npv[std::get<2>(_faces[face_index]) ];

  */

  return u * _npv[_faces[3 * face_index]] + v * _npv[_faces[3 * face_index + 1]] + w * _npv[_faces[3 * face_index + 2]];
}



template<class VEC3_TYPE>
template<typename T>
VEC3_TYPE _Mesh<VEC3_TYPE>::interpolateTangent(unsigned int face_index, T u, T v, T w) const
{
  /*
  return u*_tpv[std::get<0>(_faces[face_index]) ] +
         v*_tpv[std::get<1>(_faces[face_index ]) ] +
         w*_tpv[std::get<2>(_faces[face_index]) ];
  */

  return u * _tpv[_faces[3 * face_index]] + v * _tpv[_faces[3 * face_index + 1]] + w * _tpv[_faces[3 * face_index + 2]];
}


#ifdef MRF_MESH_TENSOR_TANGENT_INTERPOLATOR
template<class VEC3_TYPE>
template<typename T>
VEC3_TYPE
_Mesh<VEC3_TYPE>::computeTangentFromTensor(mrf::math::Mat3<T> const &tenseur, VEC3_TYPE const &init_tangent) const
{
  VEC3_TYPE tangent = init_tangent;
  VEC3_TYPE v(0.0f, 0.0f, 0.0f);

  int   iters = 0;
  float cit   = 1.0f;
  float theta = 0.0f;


  while (iters < 100 && cit > 0.000001 * theta * theta)
  {
    //v = init_tangent.normalize();
    v = tangent;
    v.normalize();

    tangent = tenseur * v;
    theta   = v.dot(tangent);
    iters++;

    cit = (tangent - v * theta).dot(tangent - v * theta);
  }

  VEC3_TYPE const default_v = VEC3_TYPE(1.0, 1.0, 0.0);

  tangent = (tangent.norm() <= 0.00000001) ? default_v : tangent;
  //tangent.normalize();

  return tangent;
}


template<class VEC3_TYPE>
template<typename T>
VEC3_TYPE _Mesh<VEC3_TYPE>::interpolateTangent(unsigned int face_index, T u, T v, T w) const
{
  mrf::math::Mat3<T> tenseur1;

  Vec3f const t1 = _tpv[_faces[face_index][0]];

  tenseur1.setColumn(0, t1 * t1.x());
  tenseur1.setColumn(1, t1 * t1.y());
  tenseur1.setColumn(2, t1 * t1.z());

  mrf::math::Mat3<T> tenseur2;
  Vec3f const        t2 = _tpv[_faces[face_index][1]];

  tenseur2.setColumn(0, t2 * t2.x());
  tenseur2.setColumn(1, t2 * t2.y());
  tenseur2.setColumn(2, t2 * t2.z());

  mrf::math::Mat3<T> tenseur3;
  Vec3f const        t3 = _tpv[_faces[face_index][2]];

  tenseur3.setColumn(0, t3 * t3.x());
  tenseur3.setColumn(1, t3 * t3.y());
  tenseur3.setColumn(2, t3 * t3.z());

  mrf::math::Mat3<T> tenseur = tenseur1 * u + tenseur2 * v + tenseur3 * w;

  Vec3f const &tangent_inter
      = u * _tpv[_faces[face_index][0]] + v * _tpv[_faces[face_index][1]] + w * _tpv[_faces[face_index][2]];

  return computeTangentFromTensor(tenseur, tangent_inter);
  //  return tangent_inter;
}
#endif


template<class VEC3_TYPE>
template<typename T>
VEC3_TYPE _Mesh<VEC3_TYPE>::interpolateUVS(unsigned int face_index, T u, T v, T w) const
{
  /*
  return u*_uvs[std::get<0>(_faces[face_index]) ] +
         v*_uvs[std::get<1>(_faces[face_index ]) ] +
         w*_uvs[std::get<2>(_faces[face_index]) ];
  */

  return u * _uvs[_faces[3 * face_index]] + v * _uvs[_faces[3 * face_index + 1]] + w * _uvs[_faces[3 * face_index + 2]];
}

template<class VEC3_TYPE>
template<typename T>
void _Mesh<VEC3_TYPE>::updateHitInfoStructure(
    unsigned int     sub_object_id,
    VEC3_TYPE const &point,
    T                u,
    T                v,
    T                w,
    T                intersection_parametric_distance,
    Intersection &   hit_info) const
{
  hit_info.normal  = interpolateNormal(sub_object_id, u, v, w);
  hit_info.tangent = interpolateTangent(sub_object_id, u, v, w);


  assert(hit_info.normal.isFinite());
  assert(hit_info.tangent.isFinite());

  hit_info.normal.normalize();
  hit_info.tangent.normalize();

  assert(hit_info.normal.isFinite());
  assert(hit_info.tangent.isFinite());

  //Do we have an orthogonal frame? low but how can we speed this up
  if (!mrf::math::equals(hit_info.tangent.dot(hit_info.normal), 0.0f, 0.001f))
  {
    //If not re-orthormalize it
    VEC3_TYPE bi_tangent = hit_info.tangent.cross(hit_info.normal);
    hit_info.tangent     = hit_info.normal.cross(bi_tangent);

    // std::cout << "hit_info.normal= " << hit_info.normal.length() << std::endl;
    // std::cout << "hit_info.tangent= " << hit_info.tangent.length() << std::endl;
    // std::cout << "hit_info.tangent dot normal = " << hit_info.tangent.dot(hit_info.normal) << std::endl;

    // // std::cout << " Tangent 1:  " << _tpv[ _faces[sub_object_id][0] ] << " Tangent 2 = " << _tpv[ _faces[sub_object_id][1] ] << " Tangent 3 = " << _tpv[ _faces[sub_object_id][2] ] << std::endl;
    // std::cout << " Normal 1:  "  << _npv[ _faces[sub_object_id][0] ] << " Normal 2 = " << _npv[ _faces[sub_object_id][1] ] << " Normal 3 = " << _npv[ _faces[sub_object_id][2] ] << std::endl;
    // assert(0);
  }


  VEC3_TYPE c_uv = interpolateUVS(sub_object_id, u, v, w);
  hit_info.uv[0] = c_uv[0];
  hit_info.uv[1] = c_uv[1];


  hit_info.point    = point;
  hit_info.t        = intersection_parametric_distance;
  hit_info.material = _materialPerFaces[sub_object_id];


  // Not updated Since we don't use it
  // hit_info.lighting_structure = _lighting_structure_index;
  // hit_info.object = _id;
}

//-------------------------------------------------------------------------
// Intersectable Interface
//-------------------------------------------------------------------------
template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersectedWNBFC(mrf::lighting::Ray const &r, Intersection &hit_info) const
{
  hit_info.t          = INFINITY;
  bool is_intersected = false;

  //Slow because iterating on each face
  for (unsigned int i = 0; i < _faces.size() / 3; i++)
  {
    /*VEC3_TYPE const & v1  = _vertices[ std::get<0>(_faces[ i ]) ];
    VEC3_TYPE const & v2  = _vertices[std::get<1>(_faces[ i ]) ];
    VEC3_TYPE const & v3  = _vertices[std::get<2>(_faces[ i ]) ];*/
    VEC3_TYPE const &v1 = _vertices[(_faces[3 * i])];
    VEC3_TYPE const &v2 = _vertices[(_faces[3 * i + 1])];
    VEC3_TYPE const &v3 = _vertices[(_faces[3 * i + 2])];

    VEC3_TYPE point;
    float     u, v, w, t;
    if ((TriaIntersecter::intersectWNBFC(r, v1, v2, v3, point, u, v, w, t)) && (t < hit_info.t))
    {
      updateHitInfoStructure(i, point, u, v, w, t, hit_info);

      is_intersected = true;
    }
  }

  return is_intersected;
}


template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersectedWNBFC(
    mrf::lighting::Ray const &r,
    Intersection &            hit_info,
    unsigned int              sub_object_id) const
{
  //Get the vertices of the face and test the ray against intersection
  /*VEC3_TYPE const & v1  = _vertices[std::get<0>(_faces[ sub_object_id ]) ];
  VEC3_TYPE const & v2  = _vertices[std::get<1>(_faces[ sub_object_id ]) ];
  VEC3_TYPE const & v3  = _vertices[std::get<2>(_faces[ sub_object_id ]) ];*/
  VEC3_TYPE const &v1 = _vertices[_faces[3 * sub_object_id]];
  VEC3_TYPE const &v2 = _vertices[_faces[3 * sub_object_id + 1]];
  VEC3_TYPE const &v3 = _vertices[_faces[3 * sub_object_id + 2]];

  VEC3_TYPE point;
  float     u, v, w, t;

  if (TriaIntersecter::intersectWNBFC(r, v1, v2, v3, point, u, v, w, t))
  {
    updateHitInfoStructure(sub_object_id, point, u, v, w, t, hit_info);
    return true;
  }

  return false;
}

template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersected(mrf::lighting::Ray const &r) const
{
  //Beware that, this method is slow
  for (unsigned int i = 0; i < _faces.size() / 3; i++)
  {
    /*
     VEC3_TYPE const & v1  = _vertices[std::get<0>(_faces[ i ]) ];
     VEC3_TYPE const & v2  = _vertices[std::get<1>(_faces[ i ]) ];
     VEC3_TYPE const & v3  = _vertices[std::get<2>(_faces[ i ]) ];
     */
    VEC3_TYPE const &v1 = _vertices[_faces[3 * i]];
    VEC3_TYPE const &v2 = _vertices[_faces[3 * i + 1]];
    VEC3_TYPE const &v3 = _vertices[_faces[3 * i + 2]];

    if (TriaIntersecter::intersect(r, v1, v2, v3))
    {
      return true;
    }
  }

  return false;
}

template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info) const
{
  hit_info.t          = INFINITY;
  bool is_intersected = false;

  for (unsigned int i = 0; i < _faces.size() / 3; i++)
  {
    /*
    VEC3_TYPE const & v1  = _vertices[std::get<0>(_faces[ i ]) ];
    VEC3_TYPE const & v2  = _vertices[std::get<1>(_faces[ i ])];
    VEC3_TYPE const & v3  = _vertices[std::get<2>(_faces[ i ]) ];
    */
    VEC3_TYPE const &v1 = _vertices[_faces[3 * i]];
    VEC3_TYPE const &v2 = _vertices[_faces[3 * i + 1]];
    VEC3_TYPE const &v3 = _vertices[_faces[3 * i + 2]];

    VEC3_TYPE point;
    float     u, v, w, t;
    if ((TriaIntersecter::intersect(r, v1, v2, v3, point, u, v, w, t)) && (t < hit_info.t))
    {
      updateHitInfoStructure(i, point, u, v, w, t, hit_info);
      is_intersected = true;
    }   //end of if intersection

  }   //end of for-loop

  return is_intersected;
}


template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int sub_object_id)
    const
{
  //Get the vertices of the face and test the ray against intersection
  /*
  VEC3_TYPE const & v1  = _vertices[std::get<0>(_faces[ sub_object_id ]) ];
  VEC3_TYPE const & v2  = _vertices[std::get<1>(_faces[ sub_object_id ])];
  VEC3_TYPE const & v3  = _vertices[std::get<2>(_faces[ sub_object_id ]) ];
  */
  VEC3_TYPE const &v1 = _vertices[_faces[3 * sub_object_id]];
  VEC3_TYPE const &v2 = _vertices[_faces[3 * sub_object_id + 1]];
  VEC3_TYPE const &v3 = _vertices[_faces[3 * sub_object_id + 2]];

#ifdef INTERSECTION_DEBUG
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << " Testing  with Triangle  v1: " << v1 << " v2:" << v2 << " v3: " << v3 << std::endl;
  std::cout << " Triangle Area: " << triangleArea(v1, v2, v3) << std::endl;
#endif


  VEC3_TYPE point;
  float     u, v, w, t;

  if (TriaIntersecter::intersect(r, v1, v2, v3, point, u, v, w, t))
  {
#ifdef INTERSECTION_DEBUG
    std::cout << __FILE__ << " " << __LINE__ << std::endl;
    std::cout << " Intersection with Triangle " << std::endl;
#endif

    updateHitInfoStructure(sub_object_id, point, u, v, w, t, hit_info);

    return true;
  }

  return false;
}



template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersected(mrf::lighting::Ray const &r, unsigned int sub_object_id) const
{
  //Get the vertices of the face and test the ray against intersection
  /*
  VEC3_TYPE const & v1  = _vertices[std::get<0>(_faces[ sub_object_id ]) ];
  VEC3_TYPE const & v2  = _vertices[std::get<1>(_faces[ sub_object_id ])];
  VEC3_TYPE const & v3  = _vertices[std::get<2>(_faces[ sub_object_id ]) ];
  */
  VEC3_TYPE const &v1 = _vertices[_faces[3 * sub_object_id]];
  VEC3_TYPE const &v2 = _vertices[_faces[3 * sub_object_id + 1]];
  VEC3_TYPE const &v3 = _vertices[_faces[3 * sub_object_id + 2]];

  if (TriaIntersecter::intersect(r, v1, v2, v3))
  {
    return true;
  }

  return false;
}

template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max, unsigned index_face)
    const
{
  MRF_AABBox box(min, max);
  return isIntersected(box, index_face);
}


template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max) const
{
  for (unsigned int i = 0; i < numSubObjects(); i++)
  {
    if (isIntersected(min, max, i))
    {
      return true;
    }
  }
  return false;
}


template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersected(MRF_AABBox const &box, unsigned index_face) const
{
  //Get the vertices of the face and test the ray against intersection
  /*
  VEC3_TYPE const & v1  = _vertices[std::get<0>(_faces[ index_face ]) ];
  VEC3_TYPE const & v2  = _vertices[std::get<1>(_faces[ index_face ])];
  VEC3_TYPE const & v3  = _vertices[std::get<2>(_faces[ index_face ]) ];
  */
  VEC3_TYPE const &v1 = _vertices[_faces[3 * index_face]];
  VEC3_TYPE const &v2 = _vertices[_faces[3 * index_face + 1]];
  VEC3_TYPE const &v3 = _vertices[_faces[3 * index_face + 2]];

  return _AABBoxIntersecter<VEC3_TYPE>::intersectTriangle(box, v1, v2, v3);
}

template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersected(
    mrf::math::Vec3f const &   min,
    mrf::math::Vec3f const &   max,
    std::vector<unsigned int> &face_indexes) const
{
  bool is_intersected = false;

  for (unsigned int i = 0; i < numSubObjects(); i++)
  {
    if (isIntersected(min, max, i))
    {
      face_indexes.push_back(i);
      is_intersected = true;
    }
  }

  return is_intersected;
}


template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::isIntersected(MRF_AABBox const &box, std::vector<unsigned int> &face_indices) const
{
  // TODO: This was roughly implemented to avoid warning.
  // This shall be tested
  bool is_intersected = false;

  for (unsigned int i = 0; i < numSubObjects(); i++)
  {
    if (isIntersected(box, i))
    {
      face_indices.push_back(i);
      is_intersected = true;
    }
  }

  return is_intersected;
}



template<class VEC3_TYPE>
mrf::geom::MRF_AABBox &_Mesh<VEC3_TYPE>::bbox()
{
  return _bbox;
}


template<class VEC3_TYPE>
unsigned int _Mesh<VEC3_TYPE>::numSubObjects() const
{
  return static_cast<uint>(_faces.size() / 3);
}


template<class VEC3_TYPE>
bool _Mesh<VEC3_TYPE>::hasSubObjects() const
{
  return true;
}

template<class VEC3_TYPE>
MRF_AABBox _Mesh<VEC3_TYPE>::subObjectAABBox(unsigned int subobj_index) const
{
  /*
  VEC3_TYPE min( _vertices[std::get<0>(_faces[subobj_index]) ] );
  min.minimize( _vertices[std::get<1>(_faces[subobj_index]) ] );
  min.minimize( _vertices[std::get<2>(_faces[subobj_index]) ] );
  */
  VEC3_TYPE min(_vertices[_faces[3 * subobj_index]]);
  min.minimize(_vertices[_faces[3 * subobj_index + 1]]);
  min.minimize(_vertices[_faces[3 * subobj_index + 2]]);

  /*
  VEC3_TYPE max( _vertices[std::get<0>(_faces[subobj_index]) ] );
  max.maximize( _vertices[std::get<1>(_faces[subobj_index]) ] );
  max.maximize( _vertices[std::get<2>(_faces[subobj_index]) ] );
  */
  VEC3_TYPE max(_vertices[_faces[3 * subobj_index]]);
  max.maximize(_vertices[_faces[3 * subobj_index + 1]]);
  max.maximize(_vertices[_faces[3 * subobj_index + 2]]);


  MRF_AABBox a_box(min, max);

  return a_box;
}




template<class VEC3_TYPE>
unsigned int _Mesh<VEC3_TYPE>::intersectionCost() const
{
  return static_cast<uint>(_faces.size() / 3);
}

template<class VEC3_TYPE>
unsigned int _Mesh<VEC3_TYPE>::memorySize() const
{
  return (static_cast<uint>(_faces.size()) * sizeof(unsigned int))
         + (static_cast<uint>(_vertices.size()) * 4 * 3 * sizeof(float))
         + (static_cast<uint>(_materialPerFaces.size()) * sizeof(unsigned int));
}
