/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/


#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//STL LIB
#include <iostream>
#include <memory>

//MRF
#include <mrf_core/geometry/object.hpp>
#include <mrf_core/geometry/intersectable.hpp>
#include <mrf_core/geometry/mesh.hpp>
#include <mrf_core/math/math.hpp>

namespace mrf
{
namespace geom
{
class MRF_CORE_EXPORT Shape: public Object   //, public Intersectable
{
protected:
  mrf::math::Reff  _cs;
  mrf::math::Mat4f _scaleTransform;

  //FROM OBJECT COORDINATES TO WORLD COORDINATES
  mrf::math::Mat4f _objectToWorldTransform;

  //From WORLD COORDINATES to OBJECT COORDINARES
  mrf::math::Mat4f _worldToObjectTransform;

  //Geometry of the shape
  std::shared_ptr<mrf::geom::Intersectable> _mesh;

public:
  /**
   * Construct a Shape and place it at the given position
   * By default the shape is place at the origin of the world
   *
   */
  Shape(float pos_x = 0.0f, float pos_y = 0.0f, float pos_z = 0.0f, unsigned int index_material = 0);

  Shape(float pos_x, float pos_y, float pos_z, MRF_AABBox const &box, unsigned int index_material = 0);

  Shape(mrf::math::Reff &ref, mrf::math::Vec3f &scale, unsigned int index_material = 0);

  Shape(Shape const &ashape);

  virtual ~Shape();

  inline void setMesh(std::shared_ptr<mrf::geom::Intersectable> &mesh);

  inline std::shared_ptr<mrf::geom::Intersectable> mesh();

  //FROM OBJECT COORDINATES TO WORLD COORDINATES
  inline mrf::math::Mat4f objectToWorldTransform();

  //From WORLD COORDINATES to OBJECT COORDINARES
  inline mrf::math::Mat4f worldToObjectTransform();


  /**
   * Returns a reference to the local Coordinate System of this Shape
   */
  //inline mrf::math::Reff &localRef();

  /**
   * Update the local Reference Frame
   */
  inline void setRef(mrf::math::Reff const &a_ref);


  inline mrf::math::Reff const &localRef() const;

  /**
   * Returns the position of the Shape in World coordinate
   */
  inline mrf::math::Vec3f const &position() const;

  inline void setPosition(mrf::math::Vec3f const &new_position);


  /**
   * Returns a Quaternion Representing the Orientation of the Shape
   **/
  inline mrf::math::Quatf const &orientation() const;



  /**
   * Returns the center of the AABBox in world coordinates
   */
  inline mrf::math::Vec3f bboxCenter() const;


  inline mrf::math::Mat4f scaleTransform() const;


  /**
   * Set the scale of the object i.e.
   * Change the scale Matrix of it
   */
  inline void setScale(float sx, float sy, float sz);


  /**
   * Scale the object by a scale Matrix defined by sx sy and sz
   * This will multiply the current scale Matrix of the object
   * by the given one
   */
  inline void scale(float sx, float sy, float sz);



  /**
   * Transforms the Ray defined in World Space to the Object Space
   **/
  virtual void transformRayToObjectSpace(mrf::lighting::Ray const &r, mrf::lighting::Ray &transformed_ray) const;


  /**
   * hit_info.point and hit_info.normal will be updated
   * overload this methods if you want to do specific treatment.
   **/
  virtual void intersectionToWorldSpace(mrf::lighting::Ray const &world_ray, Intersection &hit_info) const;



  //          //-------------------------------------------------------------------------
  //          // Partial Implementation of ILight interface
  //          //-------------------------------------------------------------------------
  //          virtual mrf::geom::AABBox& bbox();

  virtual mrf::geom::MRF_AABBox const &bbox() const;
  virtual mrf::geom::MRF_AABBox &      bbox();


  //-------------------------------------------------------------------------
  // Partial Implementation of Intersectable interface
  //-------------------------------------------------------------------------
  virtual unsigned int memorySize() const;


  //-------------------------------------------------------------------------
  // None Implemented methods of Intersectable interface
  //-------------------------------------------------------------------------
  // virtual bool isIntersectedWNBFC( mrf::lighting::Ray const & r, Intersection & hit_info  ) const = 0;


  // virtual bool isIntersectedWNBFC( mrf::lighting::Ray const & r,
  //                                  Intersection & hit_info,
  //                                  unsigned int sub_object_id ) const = 0;

  // virtual bool isIntersected( mrf::lighting::Ray const & r ) const = 0;

  // virtual bool isIntersected( mrf::lighting::Ray const & r,
  //                             Intersection & hit_info ) const = 0;


  // virtual bool isIntersected( mrf::lighting::Ray const & r,
  //                             Intersection & hit_info,
  //                             unsigned int sub_object_id ) const = 0;

  // virtual bool isIntersected( mrf::lighting::Ray const & r, unsigned int sub_object_id ) const = 0;
  // virtual unsigned int numSubObjects() const = 0;
  // virtual bool hasSubObjects() const = 0;
  // virtual MRF_AABBox subObjectAABBox( unsigned int subobj_index ) const = 0;

  // virtual bool isIntersected( MRF_AABBox const & box, unsigned index_face ) const = 0;

  // virtual bool isIntersected( mrf::math::Vec3f const & min, mrf::math::Vec3f const & max , unsigned index_face ) const = 0;

  // virtual bool isIntersected( mrf::math::Vec3f const & min, mrf::math::Vec3f const & max ) const = 0;
  // virtual bool isIntersected( mrf::math::Vec3f const & min,
  //                             mrf::math::Vec3f const & max,
  //                             std::vector<unsigned int> & sub_objects ) const = 0;

  // virtual bool isIntersected( MRF_AABBox const & box,
  //                             std::vector<unsigned int> & objects ) const = 0;

  // virtual unsigned int intersectionCost() const = 0;

private:
  inline void updateWorld2ObjTransformations();
};


inline void Shape::setMesh(std::shared_ptr<mrf::geom::Intersectable> &mesh)
{
  _mesh = mesh;
}

inline std::shared_ptr<mrf::geom::Intersectable> Shape::mesh()
{
  return _mesh;
}

//FROM OBJECT COORDINATES TO WORLD COORDINATES
inline mrf::math::Mat4f Shape::objectToWorldTransform()
{
  return _objectToWorldTransform;
}

//From WORLD COORDINATES to OBJECT COORDINARES
inline mrf::math::Mat4f Shape::worldToObjectTransform()
{
  return _worldToObjectTransform;
}

// inline mrf::math::Reff &Shape::localRef()
// {
//   return _cs;
// }

inline void Shape::setRef(mrf::math::Reff const &a_ref)
{
  _cs = a_ref;
  updateWorld2ObjTransformations();
}


inline mrf::math::Reff const &Shape::localRef() const
{
  return _cs;
}



inline mrf::math::Vec3f Shape::bboxCenter() const
{
  return (_cs.getMatrixFrom() * _bbox.center());
  //mrf::math::Vec3f center = mrf::math::TO_MRF( centerAABBox( _bbox ));
  //return ( _cs.getMatrixFrom()*center);
}


inline mrf::math::Vec3f const &Shape::position() const
{
  return _cs.getPosition();
}

inline void Shape::setPosition(mrf::math::Vec3f const &new_position)
{
  _cs.setPosition(new_position);

  updateWorld2ObjTransformations();
}



inline mrf::math::Quatf const &Shape::orientation() const
{
  return _cs.getOrientation();
}


inline void Shape::setScale(float x, float y, float z)
{
  _scaleTransform = mrf::math::Mat4f::scale(x, y, z);

  float sx = _scaleTransform(0);
  float sy = _scaleTransform(5);
  float sz = _scaleTransform(10);

  MRF_AABBox realBox = mrf::geom::scale(_bbox, sx, sy, sz);
  _bbox              = realBox;

  updateWorld2ObjTransformations();
}

inline void Shape::scale(float x, float y, float z)
{
  _scaleTransform *= mrf::math::Mat4f::scale(x, y, z);
  updateWorld2ObjTransformations();
}

inline mrf::math::Mat4f Shape::scaleTransform() const
{
  return _scaleTransform;
}


inline void Shape::updateWorld2ObjTransformations()
{
  _objectToWorldTransform = _cs.getMatrixFrom() * _scaleTransform;
  _worldToObjectTransform = _scaleTransform.inverse() * _cs.getMatrixTo();
}


}   // namespace geom

}   // namespace mrf
