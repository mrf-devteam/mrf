#include <mrf_core/geometry/computation.hpp>
#include <mrf_core/math/math.hpp>

#include <vector>
#include <iostream>

using namespace mrf::math;

namespace mrf
{
namespace geom
{
void Computation::computeNormals(
    float *       vertices,
    unsigned int  num_of_vertices,
    unsigned int *index_faces,
    unsigned int  num_of_faces,
    float **      normals)
{
  Vec3f        current_normal;
  unsigned int normal_counter;

  //For each vertex
  for (unsigned int i = 0; i < num_of_vertices; i++)
  {
    current_normal.set(0.0f, 0.0f, 0.0f);
    normal_counter = 0;

    for (unsigned int j = 0; j < num_of_faces * 3; j += 3)
    {
      if ((index_faces[j] == i) || (index_faces[j + 1] == i) || (index_faces[j + 2] == i))
      {
        current_normal += computeTriangleNormal(vertices, index_faces[j], index_faces[j + 1], index_faces[j + 2]);

        normal_counter++;
      }

    }   //end of for loop for each face

#ifdef DEBUG
    std::cout << "  normal_counter = " << normal_counter << std::endl;
#endif

    if (normal_counter != 0)
    {
      //current_normal = current_normal / normal_counter;

      if (current_normal.norm() > EPSILON)
      {
        current_normal.normalize();
      }
      else
      {
        std::cout << " WARNING : Could not compute a normal for vertex " << i << std::endl;
      }


      (*normals)[i * 3]     = current_normal[0];
      (*normals)[i * 3 + 1] = current_normal[1];
      (*normals)[i * 3 + 2] = current_normal[2];
    }
    else
    {
      std::cout << "Vertex " << i << " is not used by any face !!" << std::endl;
    }
  }   //end of for loop for each vertex


}   //end of computeNormals method


void Computation::computeNormals(
    std::vector<float> const &       vertices,
    std::vector<unsigned int> const &index_faces,
    std::vector<float> &             normals)
{
  normals.resize(vertices.size());
  Vec3f        current_normal;
  unsigned int normal_counter;

  //For each vertex
  for (unsigned int i = 0; i < vertices.size() / 3; i++)
  {
    current_normal.set(0.0f, 0.0f, 0.0f);
    normal_counter = 0;

    Vec3f current_vertex;
    vertex(vertices, i, current_vertex);

    for (unsigned int j = 0; j < index_faces.size(); j += 3)
    {
      if ((index_faces[j] == i) || (index_faces[j + 1] == i) || (index_faces[j + 2] == i))
      {
        Vec3f tria_normal;

        tria_normal = computeTriangleNormal(vertices, index_faces[j], index_faces[j + 1], index_faces[j + 2]);

        current_normal += tria_normal;
        normal_counter++;
      }

    }   //end of for loop for each face

    if (normal_counter > 0)
    {
      if (current_normal.norm() > EPSILON)
      {
        current_normal.normalize();
      }
      else
      {
        std::cout << " WARNING : Could not compute a normal for vertex " << i << std::endl;
      }

      normals[i * 3]     = current_normal[0];
      normals[i * 3 + 1] = current_normal[1];
      normals[i * 3 + 2] = current_normal[2];
    }
    else
    {
      std::cout << "Vertex " << i << " is not used by any face !!" << std::endl;
    }
  }   //end of for loop for each vertex
}




Vec3f Computation::computeTriangleNormal(
    float *      vertices,
    unsigned int index_a,
    unsigned int index_b,
    unsigned int index_c)
{
  Vec3f a = vertex(vertices, index_a);
  Vec3f b = vertex(vertices, index_b);
  Vec3f c = vertex(vertices, index_c);

  Vec3f ab = (b - a);
  Vec3f ac = (c - a);

  Vec3f normal = ab.cross(ac);
  //normal.normalize();
  return normal;
}


Vec3f Computation::computeTriangleNormal(
    std::vector<float> const &vertices,
    unsigned int              index_a,
    unsigned int              index_b,
    unsigned int              index_c)
{
  Vec3f a = vertex(vertices, index_a);
  Vec3f b = vertex(vertices, index_b);
  Vec3f c = vertex(vertices, index_c);

  Vec3f ab = (b - a);
  Vec3f ac = (c - a);

  Vec3f normal = ab.cross(ac);
  //normal.normalize();
  return normal;
}


Vec3f Computation::vertex(float const *vertices, unsigned int index_v)
{
  return Vec3f(vertices[index_v * 3], vertices[index_v * 3 + 1], vertices[index_v * 3 + 2]);
}


Vec3f Computation::vertex(std::vector<float> const &vertices, unsigned int index_v)
{
  return Vec3f(vertices[index_v * 3], vertices[index_v * 3 + 1], vertices[index_v * 3 + 2]);
}




void Computation::vertex(float const *vertices, unsigned int index_v, Vec3f &v)
{
  v.set(vertices[index_v * 3], vertices[index_v * 3 + 1], vertices[index_v * 3 + 2]);
}


void Computation::vertex(std::vector<float> const &vertices, unsigned int index_v, Vec3f &v)
{
  v.set(vertices[index_v * 3], vertices[index_v * 3 + 1], vertices[index_v * 3 + 2]);
}


void Computation::vertexByFace(
    float const *       vertices,
    unsigned int        index_face,
    unsigned int const *faces,
    Vec3f &             v1,
    Vec3f &             v2,
    Vec3f &             v3)
{
#ifdef DEBUG
  std::cout << __FILE__ << " " << __FUNCTION__ << std::endl;
#endif



  unsigned int i1 = faces[index_face * 3];
  unsigned int i2 = faces[index_face * 3 + 1];
  unsigned int i3 = faces[index_face * 3 + 2];

  v1 = vertex(vertices, i1);
  v2 = vertex(vertices, i2);
  v3 = vertex(vertices, i3);
}


void Computation::indicesByFace(
    unsigned int        index_face,
    unsigned int const *faces,
    unsigned int &      i1,
    unsigned int &      i2,
    unsigned int &      i3)
{
  i1 = faces[index_face * 3];
  i2 = faces[index_face * 3 + 1];
  i3 = faces[index_face * 3 + 2];
}


void Computation::indicesByFace(
    unsigned int                     index_face,
    std::vector<unsigned int> const &faces,
    unsigned int &                   i1,
    unsigned int &                   i2,
    unsigned int &                   i3)
{
  i1 = faces[index_face * 3];
  i2 = faces[index_face * 3 + 1];
  i3 = faces[index_face * 3 + 2];
}


void Computation::saveOneVertex(Vec3f const &v, std::vector<float> &vertices, unsigned int &index_saved)
{
  vertices.push_back(v[0]);
  index_saved = static_cast<uint>((vertices.size() - 1) / 3);
  vertices.push_back(v[1]);
  vertices.push_back(v[2]);
}

void Computation::saveOneVertex(Vec3f const &v, std::vector<float> &vertices)
{
  vertices.push_back(v[0]);
  vertices.push_back(v[1]);
  vertices.push_back(v[2]);
}


void Computation::computeBoxGreatDiagonalV(
    Vec3f const &min,
    Vec3f const &max,
    Vec3f &      d1,
    Vec3f &      d2,
    Vec3f &      d3,
    Vec3f &      d4)
{
  Vec3f p1, p2, p3, p4, p5, p6, p7, p8;

  p1[0] = min[0];
  p1[1] = min[1];
  p1[2] = min[2];
  p7[0] = max[0];
  p7[1] = max[1];
  p7[2] = max[2];

  p2[0] = max.x();
  p2[1] = min.y();
  p2[2] = min.z();
  p3[0] = max.x();
  p3[1] = min.y();
  p3[2] = max.z();
  p4[0] = min.x();
  p4[1] = min.y();
  p4[2] = max.z();

  p5[0] = min.x();
  p5[1] = max.y();
  p5[2] = min.z();
  p6[0] = max.x();
  p6[1] = max.y();
  p6[2] = min.z();
  p8[0] = min.x();
  p8[1] = max.y();
  p8[2] = max.z();

  d1 = p7 - p1;
  d2 = p8 - p2;
  d3 = p5 - p3;
  d4 = p6 - p4;
}


void Computation::computeBoxGreatDiagonalV(Vec3f const &min, Vec3f const &max, std::vector<Vec3f> &d)
{
  Vec3f d1, d2, d3, d4;
  computeBoxGreatDiagonalV(min, max, d1, d2, d3, d4);

  d.push_back(d1);
  d.push_back(d2);
  d.push_back(d3);
  d.push_back(d4);
}



/**
 * Computes the length of the four greatest diagonals of a box
 *
 */
void Computation::computeBoxGreatDiagonals(
    Vec3f const &min,
    Vec3f const &max,
    float &      d1,
    float &      d2,
    float &      d3,
    float &      d4)
{
  Vec3f vd1, vd2, vd3, vd4;
  Computation::computeBoxGreatDiagonalV(min, max, vd1, vd2, vd3, vd4);


  d1 = vd1.norm();
  d2 = vd2.norm();
  d3 = vd3.norm();
  d4 = vd4.norm();
}


}   // namespace geom
}   // namespace mrf
