/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//MRF
#include <mrf_core/lighting/ray.hpp>
#include <mrf_core/math/vec3.hpp>
#include <mrf_core/geometry/intersecter.hpp>
#include <mrf_core/geometry/aabbox.hpp>


namespace mrf
{
namespace geom
{
/**
 * Defines the interface every objects must implement to
 * be used in a Ray/Path tracing algorithm and with any
 * accelerating structure
 *
 **/
class MRF_CORE_EXPORT Intersectable
{
public:
  virtual MRF_AABBox &bbox() = 0;
  inline virtual ~Intersectable() {}

  virtual bool isIntersectedWNBFC(mrf::lighting::Ray const &r, Intersection &hit_info) const = 0;


  virtual bool
  isIntersectedWNBFC(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int sub_object_id) const = 0;



  /**
   * Returns whether or not a given ray intersects the object
   *
   *
   *@param the Ray tested against intersection
   */
  virtual bool isIntersected(mrf::lighting::Ray const &r) const = 0;

  /**
   * Returns whether or not a given ray intersects the object
   * If the ray does intersect the Object the given Intersection
   * structure will be correctly fill
   *
   * @param the Ray tested against intersection
   * @param the intersection structure
   */
  virtual bool isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info) const = 0;


  /**
   * Returns whether or not a given ray intersects the sub_object_id of the object
   *
   * This is usefull for Meshes or other data which are composed of many entities
   * each of them intersectable by a ray.
   *
   * The given Intersection structure which contains
   * information about the intersection is fill by the Object
   *
   * For implicit surfaces this method should call the previous one.
   *
   **/
  virtual bool isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int sub_object_id) const = 0;

  /**
   * Same as previous except that no Intersection Structure is fill.
   * This is usefull for shadow Rays where only the visibility information
   * is required
   *
   **/
  virtual bool isIntersected(mrf::lighting::Ray const &r, unsigned int sub_object_id) const = 0;

  /**
   * Return the number of subobjects of this Intersectable
   **/
  virtual unsigned int numSubObjects() const = 0;


  /**
   * Returns whether or not this Intersectable has subobjects
   **/
  virtual bool hasSubObjects() const = 0;

  /**
   * Returns the Axis Aligned Bounding Box of subobject which
   * has the given subobj_index
   *
   **/
  virtual MRF_AABBox subObjectAABBox(unsigned int subobj_index) const = 0;


  //TODO :  are the next two methods really usefull. What about IMPLICIT surfaces ???

  /**
   * Returns whether or not the given AABBox box intersects the
   * face numbered index_face.
   *
   **/
  virtual bool isIntersected(MRF_AABBox const &box, unsigned index_face) const = 0;


  /**
   * Returns whether or not the given AABBox defined by the
   * two given extremal points min and max intersects the
   * face numbered index_face.
   *
   **/
  virtual bool isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max, unsigned index_face) const = 0;

  /**
   * Returns wheter or not this objects is intersected by the
   * AABBox defined by the given two extremal points
   *
   * This methods is necessary for Accelerating Intersection structures
   * when building the structure
   * It must be implemented in a very efficient way to minize
   * the construction time of the structure
   **/
  virtual bool isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max) const = 0;


  //TODO :  SHALL WE KEEP THIS METHODS ?
  /**
   * Returns whether or not the AABBox defined by min and max
   * intersects the Object.
   *
   * If it does and if the Object contains many Intersectable,
   * the given objects vector will be fill with every Intersectable
   * intersected by the AABBox.
   *
   * @param min the lower corner of the  given AABBox
   * @param max the higher corner of the given AABBox
   **/
  virtual bool isIntersected(
      mrf::math::Vec3f const &   min,
      mrf::math::Vec3f const &   max,
      std::vector<unsigned int> &sub_objects) const = 0;

  virtual bool isIntersected(MRF_AABBox const &box, std::vector<unsigned int> &objects) const = 0;



  /**
   * Returns the Ray Intersection Cost of this Intersectable
   * It is an empirical value that represents how costly is
   * the ray - object intersection computation
   *
   **/
  virtual unsigned int intersectionCost() const = 0;

  /**
   * Returns the total (in bytes) amount  of memory
   * used by this Object
   **/
  virtual unsigned int memorySize() const = 0;
};

}   // namespace geom

}   // namespace mrf
