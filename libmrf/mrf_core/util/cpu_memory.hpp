/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace util
{
class MRF_CORE_EXPORT CPUMemory
{
public:
  // return available memory in MBytes
  static unsigned long long int availableMemory();

  // return used memory in MBytes
  static unsigned long long int usedMemory();

  // return total memory installed on system in MBytes
  static unsigned long long int totalMemory();
};
}   // namespace util
}   // namespace mrf
