/*
 *
 * author : David Murray @inria.fr
 * Copyright INRIA 2019
 *
 **/
#pragma once

#include <mrf_core/util/string_parsing.hpp>
#include <mrf_core/util/command_line.hpp>

#include <string>
#include <fstream>
#include <vector>
#include <cctype>
#include <string.h>
#include <cassert>

#ifdef DEBUG
#  include <iostream>
#endif

namespace mrf
{
namespace util
{
enum ARG_LIST
{
  HELP,
  LOGGING,
  LOGFILE,
  SCENE,
  CAMERA,
  FIRST_CAMERA,
  NUM_CAMERAS,
  MAX_TIME,
  DEBUG_PIXELS,
  NEXT_EVENT,
  ENVMAP_IS,
  MPL,
  RNG_METHOD,
  RNG_SEED,
  WAVELENGTH,
  WPP,
  TEMP,
  HEIGHT,
  WIDTH,
  SAMPLES,
  SPF,
  OUTPUT,
  CUDA_FILE,
  CONTINUOUS_SAMPLING,
  INTERACTIVE,
  NO_GUI,
  ENV_ROT,
  LOG_VARIANCE,
  RUNTIME_PTX_COMPIL,
  OVERWRITE_PTX,
  LAST   // Always add you additional arguments before
};

/*! \class ArgsParsing
 * \brief Class handling the parsing of command-line option.
 *
 *  This class provides a simple way to check if an argument is in the command-line option and/or to get its value. It also affects default value if necessary.
 */

class MRF_CORE_EXPORT ArgsParsing
{
public:
  /*!
   *  \brief Default constructor.
   */
  ArgsParsing();

  /*!
   *  \brief Constructor based on command-line arguments.
   *  \param argc : the number of arguments.
   *  \param argv : the list of arguments.
   */
  ArgsParsing(int argc, char **argv);

  /*!
   *  \brief Default destructor.
   */
  ~ArgsParsing();

  /*!
   *  \brief Checks if the argument is used in the options.
   *  \param arg : the argument to check.
   *  \return true if argument is initialized, false otherwise.
   */
  bool hasArgument(ARG_LIST arg);

  /*!
   *  \brief Accessor to a single valued argument. hasArgument() must be called beforehand.
   *  \param arg : the argument to acess.
   *  \param i : the index of the value to get from the argument. Set to 0 by default.
   *  \return A string containing the value of the argument.
   */
  std::string getSingleValue(ARG_LIST arg, int i = 0);

  /*!
   *  \brief Accessor to a mulitple valued argument. hasArgument() must be called beforehand.
   *  \param arg : the argument to access.
   *  \return A vector of string containing the values of the argument.
   */
  std::vector<std::string> getMultipleValue(ARG_LIST arg);

  /*!
   *  \brief Updates an argument value with a single input.
   *  \param arg : the argument to update.
   *  \param value : the new value to set.
   */
  void updateArg(ARG_LIST arg, std::string value);

  /*!
   *  \brief Updates an argument value with multiple input.
   *  \param arg : the argument to update.
   *  \param values : the new values to set.
   */
  void updateArg(ARG_LIST arg, std::vector<std::string> values);

private:
  //Utility method to convert a range [first:last:step] into a vector.
  std::vector<std::string> wavelengthRange(std::string input);

  bool *                    _has_argument;
  std::vector<std::string> *_value;
};

}   //end of namespace util
}   //end of namespace mrf