/*
 *
 * Imported from HDRSee Software.
 * Removed unimplemented methods/functions
 *
 * Author: Romain PACANOWSKI.
 * Copyright ©2019 CNRS
 *
 */


#pragma once

#include <cassert>
#include <string>
#include <iostream>

#ifdef MRF_ON_MACOS_X
#  include <mach-o/dyld.h>
#elif defined(_WIN32)
#  include <windows.h>
#else   //LINUX
#  include <unistd.h>
#  include <errno.h>
#  include <sys/types.h>
#  include <sys/stat.h>
#endif

#include <mrf_core_dll.hpp>
#include <mrf_core/util/string_parsing.hpp>


namespace mrf
{
namespace util
{
/**
 * This class represents a Path to a specific directory
 * of the underlying Operating System.
 * The Path can be absolute or can contain symbolic link or either
 * relative path (e.g., "./" "../")
 *
 * The syntax of the Path uses slash // Changes all BackSlash to Slash
    ("/") as separator.
  * NB: Win* Operating Systems do not require ("\")
  *
  **/
class MRF_CORE_EXPORT Path
{
public:
  static unsigned int const MAX_PATH_SIZE = 4096;

private:
  std::string _name;
  bool        _isAbsolute;

public:
  inline Path(std::string const &a_path);
  inline ~Path();
  inline Path(Path const &another_path);

  /**
   * Converts the ./ or ../ of the Path's name
   * to transform it intro an absolute Path
   **/
  inline void convertToAbsolutePath();

  //-------------------------------------------------------------------------
  inline bool               isAbsolute() const;
  inline std::string const &name() const;
  //-------------------------------------------------------------------------

private:
  /**
   * Changes all BackSlashes to Slashes
   **/
  inline void backSlashToSlash();

  //-------------------------------------------------------------------------
public:
  /**
   * Returns the Absolute Path of the current executable
   **/
  static inline Path getExecutionPath();
};

//-------------------------------------------------------------------------
inline Path::Path(std::string const &a_name): _name(a_name), _isAbsolute(false)
{
  backSlashToSlash();
}

inline Path::~Path() {}


inline Path::Path(Path const &another_path): _name(another_path._name), _isAbsolute(another_path._isAbsolute) {}

inline void Path::convertToAbsolutePath()
{
  std::cerr << " [TODO] Implement me at " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0);
}

inline bool Path::isAbsolute() const
{
  return _isAbsolute;
}

inline std::string const &Path::name() const
{
  return _name;
}

//-------------------------------------------------------------------------
inline void Path::backSlashToSlash()
{
  for (unsigned int i = 0; i < _name.size(); i++)
  {
    if (_name[i] == BACK_SLASH[0])
    {
      _name[i] = SLASH[0];
    }
  }
}

//-------------------------------------------------------------------------
inline Path Path::getExecutionPath()
{
#ifdef MRF_DEV_BUILD
  return Path(std::string(MRF_DIR));
#endif

  using namespace std;

// The MacOS Way:
#ifdef MRF_ON_MACOS_X
  char     execution_path[Path::MAX_PATH_SIZE];
  uint32_t size = sizeof(execution_path);
  if (_NSGetExecutablePath(execution_path, &size) == 0)
  {
#  ifdef MRF_UTIL_PATH_DEBUG
    cout << "Executable path is: " << execution_path << endl;
#  endif

    char  real_exec_path[Path::MAX_PATH_SIZE];
    char *test = NULL;
    test       = realpath(execution_path, real_exec_path);
    if (test == NULL)
    {
      cerr << " Could not resolve real_path of " << execution_path << endl;
      assert(0);
    }

    string full_path;
    StringParsing::getDirectory(real_exec_path, full_path);

    return Path(full_path);
  }
  else
  {
    cerr << "Path size exceeds " << Path::MAX_PATH_SIZE << endl;
  }

  //Should  never happend
  assert(0);
  return string("");
#elif defined(_WIN32)
  //The Windows Way:
  char execution_path[Path::MAX_PATH_SIZE];
  GetModuleFileName(GetModuleHandle(NULL), execution_path, Path::MAX_PATH_SIZE);

  string full_path;
  StringParsing::getDirectory(execution_path, full_path);

  return Path(full_path);

//The Solaris Way:
//getexecname()
// The FREEBSD Way:
// int mib[4];
// mib[0] = CTL_KERN;
// mib[1] = KERN_PROC;
// mib[2] = KERN_PROC_PATHNAME;
// mib[3] = -1;
// char buf[1024];
// size_t cb = sizeof(buf);
// sysctl(mib, 4, buf, &cb, NULL, 0);
#else

  // The Linux Way:
  // readlink on /proc/self/exe

  char        execution_path[Path::MAX_PATH_SIZE];
  int const   ep_size       = sizeof(execution_path);
  char const *SELF_EXE_PATH = "/proc/self/exe";

  ssize_t return_code = readlink(SELF_EXE_PATH, execution_path, ep_size);
  int     errsv       = errno;

  if (return_code != -1)
  {
#  ifdef DEBUG
    std::cout << " SELF_EXE_READ IS " << execution_path << std::endl;
#  endif

    if (return_code > MAX_PATH_SIZE)
    {
      throw std::runtime_error(
          std::string("Problem while retrieving a too loong execution PATH of: ") + std::to_string(return_code));
    }

    execution_path[return_code] = '\0';

    string full_path;
    StringParsing::getDirectory(execution_path, full_path);

    return Path(full_path);
  }
  else
  {
    cerr << "Problem while trying to read " << SELF_EXE_PATH << "with errno code equal to: " << errsv << endl;
    assert(0);
    throw std::runtime_error(std::string("Problem while trying to read ") + std::string(SELF_EXE_PATH));
  }

  //Should  never happen
  assert(0);
  return string("");
#endif
}


}   // namespace util
}   // namespace mrf