/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/

#include <mrf_core/util/cpu_memory.hpp>

#if defined(_WIN32) || defined(_WIN64)

#  include <windows.h>

#elif defined(linux) || defined(__linux)

#  include <sys/sysinfo.h>
#  include <unistd.h>

#elif defined(__APPLE__)

#  include <sys/types.h>
#  include <sys/sysctl.h>
#  include <mach/mach.h>
#  include <mach/vm_statistics.h>
#  include <mach/mach_types.h>
#  include <mach/mach_init.h>
#  include <mach/mach_host.h>

#endif

#include <iostream>

namespace mrf
{
namespace util
{
unsigned long long int CPUMemory::availableMemory()
{
#if defined(_WIN32) || defined(_WIN64)

  MEMORYSTATUSEX memory_status;
  ZeroMemory(&memory_status, sizeof(MEMORYSTATUSEX));
  memory_status.dwLength = sizeof(MEMORYSTATUSEX);

  if (GlobalMemoryStatusEx(&memory_status))
  {
    return unsigned long long int(memory_status.ullAvailPhys) / unsigned long long int(1000 * 1000);
  }
  else
  {
    return 0;
  }

#elif defined(linux) || defined(__linux)
  unsigned long long int page_size       = sysconf(_SC_PAGE_SIZE);
  unsigned long long int available_pages = get_phys_pages();

  return available_pages * page_size / (1000 * 1000);
#elif defined(__APPLE__)
  // RP:  The problem with the call below is that it counts virtual memory as well on MacOS
  //
  // xsw_usage vmusage = { 0 };
  // size_t size = sizeof(vmusage);
  // if (sysctlbyname("vm.swapusage", &vmusage, &size, NULL, 0) != 0)
  // {
  //   return 0;
  // }

  // unsigned long long int available_memory;
  // mach_msg_type_number_t count = HOST_VM_INFO_COUNT;
  // vm_statistics_data_t vmstat;
  // if (KERN_SUCCESS != host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmstat, &count))
  // {
  //   return 0;
  // }
  // available_memory = (unsigned long long int)vmstat.free_count * (unsigned long long int)vmusage.xsu_pagesize;

  // return available_memory;

  return totalMemory() - usedMemory();
#endif
}


unsigned long long int CPUMemory::usedMemory()
{
#if defined(_WIN32) || defined(_WIN64)

  MEMORYSTATUSEX memory_status;
  ZeroMemory(&memory_status, sizeof(MEMORYSTATUSEX));
  memory_status.dwLength = sizeof(MEMORYSTATUSEX);
  if (GlobalMemoryStatusEx(&memory_status))
  {
    return unsigned long long int(memory_status.ullTotalPhys - memory_status.ullAvailPhys)
           / unsigned long long int(1000 * 1000);
  }
  else
  {
    return 0;
  }

#elif defined(linux) || defined(__linux)
  return totalMemory() - availableMemory();

#elif defined(__APPLE__)
  vm_size_t              page_size;
  mach_port_t            mach_port;
  mach_msg_type_number_t count;
  vm_statistics64_data_t vm_stats;

  mach_port = mach_host_self();
  count     = sizeof(vm_stats) / sizeof(natural_t);
  if (KERN_SUCCESS == host_page_size(mach_port, &page_size)
      && KERN_SUCCESS == host_statistics64(mach_port, HOST_VM_INFO, (host_info64_t)&vm_stats, &count))
  {
    long long used_memory
        = ((int64_t)vm_stats.active_count + (int64_t)vm_stats.inactive_count + (int64_t)vm_stats.wire_count)
          * (int64_t)page_size;

    // long long free_memory = (int64_t)vm_stats.free_count * (int64_t)page_size;
    //printf("free memory: %lld\nused memory: %lld\n", free_memory, used_memory);

    return used_memory / 1024 / 1024;
  }

  return 0;
#endif
}


unsigned long long int CPUMemory::totalMemory()
{
#if defined(linux) || defined(__linux)
  unsigned long long int pages     = sysconf(_SC_PHYS_PAGES);
  unsigned long long int page_size = sysconf(_SC_PAGE_SIZE);

  //unsigned long long int available_pages = get_avphys_pages();

  return pages * page_size / (1000 * 1000);
#elif defined(__APPLE__)
  int64_t bytes;
  size_t  len = sizeof(bytes);
  sysctlbyname("hw.memsize", &bytes, &len, NULL, 0);
  unsigned long long int megs = bytes / 1024 / 1024;
  //std::cout << "AMT MEM: MiB " <<  megs << std::endl;
  return megs;
#else
  return usedMemory() + availableMemory();
#endif
}

}   // namespace util
}   // namespace mrf
