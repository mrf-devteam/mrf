/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#include <mrf_core/io/material_scene_parser.hpp>

#include <mrf_core/util/string_parsing.hpp>
#include <mrf_core/io/extensions.hpp>
#include <mrf_core/io/alta_reader.h>
#include <mrf_core/io/file_io.hpp>

//STL
#include <iostream>
#include <vector>
#include <typeinfo>
#include <algorithm>


#include <mrf_core/util/precision_timer.hpp>

namespace mrf
{
namespace io
{
MaterialSceneParser::MaterialSceneParser(): _doc_handle(nullptr) {}

MaterialSceneParser::MaterialSceneParser(tinyxml2::XMLHandle doc_handle): _doc_handle(doc_handle) {}

MaterialSceneParser::MaterialSceneParser(
    tinyxml2::XMLHandle                                             doc_handle,
    std::vector<std::string>                                        id_fct_map,
    std::map<std::string, BasePluginParser<mrf::materials::UMat> *> parse_fct_map,
    ParserHelper &                                                  parser)
  : _doc_handle(doc_handle)
  , _id_map(id_fct_map)
  , _parse_map(parse_fct_map)
  , _parser(parser)
{}

bool MaterialSceneParser::parsingScene(mrf::rendering::Scene &scene)
{
  mrf::gui::fb::Loger::getInstance()->trace("Starting to parse the scene file ");

  //Clear All Maps and BRDF counter
  _brdf_map.clear();

  bool correctly_parsed_global_material = true;
  if (!parseGlobalMaterials(scene))
  {
    mrf::gui::fb::Loger::getInstance()->warn(" Problem while parsing materials ");
    correctly_parsed_global_material = false;
  }

  //we do not return the error due to bad parsing
  //of ext resources to allow the user to still render an
  //image even though some envmaps or other ext resources
  //can't be loaded
  return correctly_parsed_global_material;
}

bool MaterialSceneParser::parseGlobalMaterials(mrf::rendering::Scene &scene)
{
  mrf::gui::fb::Loger::getInstance()->trace("Looking for materials ");

  tinyxml2::XMLElement *material = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK)
                                       .FirstChildElement(SceneLexer::MATERIALS_MK)
                                       .FirstChildElement()
                                       .ToElement();

  if (material)
  {
    mrf::gui::fb::Loger::getInstance()->trace("Materials Found ! Parsing them....");

    for (; material; material = material->NextSiblingElement())
    {
      std::string element_name = material->Name();

      if (element_name.compare(SceneLexer::MATERIAL_MK) == 0)
      {
        if (!addMaterial(material, scene))
        {
          mrf::gui::fb::Loger::getInstance()->warn("Problem while tying to add one material");
        }
      }
      else if (element_name.compare(SceneLexer::EMITTANCE_MK) == 0)
      {
        if (!addEmittance(material, scene))
        {
          mrf::gui::fb::Loger::getInstance()->warn("Problem while trying to add an emittance in parse materials");
        }
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->warn("Unrecognized element in materials ");
      }
    }

    if (_multi_mat_list.size() >= 1)
    {
      for (int i = 0; i < _multi_mat_list.size(); ++i)
      {
        auto multi_mat = _multi_mat_list[i];
        bool rOK       = true;
        for (int j = 0; j < multi_mat->size(); ++j)
        {
          std::string name = multi_mat->getAllNames()[j];
          rOK &= _brdf_map.count(name) == 1;
          if (rOK)
            multi_mat->addMRFIdEntry(_brdf_map[name]);
          else
            break;
        }
        if (!rOK) multi_mat = nullptr;
      }
    }
  }
  else
  {
    // THIS IS NOT AN ERROR because A scene can contain only light sources
    mrf::gui::fb::Loger::getInstance()->warn(ParsingErrors::NO_MATERIALS);
  }
  return true;
}

//MATERIAL MANAGEMENT
bool MaterialSceneParser::addMaterial(tinyxml2::XMLElement *a_mat_element, mrf::rendering::Scene &scene)
{
  if (!a_mat_element->Attribute(SceneLexer::NAME_AT))
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_MATERIAL_NAME);
    return false;
  }

  std::string name(a_mat_element->Attribute(SceneLexer::NAME_AT));
  if (_brdf_map.count(name) == 1)
  {
    mrf::gui::fb::Loger::getInstance()->info("Material " + name + " is already in the list.");
    return true;
  }

  std::string type;

  if (!a_mat_element->Attribute(SceneLexer::TYPE_AT))
  {
    mrf::gui::fb::Loger::getInstance()->warn(" MATERIAL HAS NOT TYPE, aborting");
    return false;
  }
  else
  {
    type = std::string(a_mat_element->Attribute(SceneLexer::TYPE_AT));
  }

  mrf::gui::fb::Loger::getInstance()->trace(" Material name is ", name);
  mrf::gui::fb::Loger::getInstance()->trace(" Material type is ", type);

  if (type.compare(mrf::materials::BRDFTypes::MULTI_MAT) == 0)
  {
    mrf::gui::fb::Loger::getInstance()->trace("Multi-Material Detected, parsing them");
    mrf::materials::MultiMaterial *multimat = new mrf::materials::MultiMaterial(name);
    bool                           rOK      = parseMultiMaterial(a_mat_element, multimat);
    if (rOK)
    {
      _brdf_map[name] = scene.addMaterial(multimat);
      _multi_mat_list.push_back(multimat);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->warn(
          "Something went wrong with a mutli-material. Please check that its material are in the scene graph.");
      delete multimat;
    }
  }
  else
  {
    for (auto registered_mat : _id_map)
    {
      if (type.compare(registered_mat) == 0)
      {
        mrf::gui::fb::Loger::getInstance()->trace(std::string(registered_mat) + " Detected");
        _brdf_map[name]
            = scene.addMaterial(_parse_map[std::string(registered_mat)]->parse(a_mat_element, name, _parser));
        return true;
      }
    }

    mrf::gui::fb::Loger::getInstance()->trace("Material : " + name + " is not among implemented material.");
    return false;
  }

  return true;
}


//void MaterialSceneParser::parsePhong(tinyxml2::XMLElement *a_mat_element, mrf::materials::LPhysicalPhong *phong_mat)
//{
//  if (a_mat_element->FirstChildElement(SceneLexer::DIFFUSE_CMP_MK))
//  {
//    tinyxml2::XMLElement *diffuse_elem = a_mat_element->FirstChildElement(SceneLexer::DIFFUSE_CMP_MK);
//
//    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse component detected ");
//
//    mrf::materials::COLOR diffuse_color        = ParserHelper::retrieveColor(a_mat_element);
//    float diffuse_reflectivity = ParserHelper::retrieveReflectivity(diffuse_elem);
//
//    //if (retrieveReflectivity(diffuse_elem, diffuse_reflectivity))
//    {
//      mrf::gui::fb::Loger::getInstance()->trace("Diffuse reflectivity retrieved");
//      phong_mat->setDiffuseReflectivity(diffuse_reflectivity);
//    }
//    //else
//    //{
//    //  mrf::gui::fb::Loger::getInstance()->warn("  default value=" + std::to_string(phong_mat->diffuseAlbedo()));
//    //}
//
//    //if (retrieveColor(diffuse_elem, diffuse_color))
//    {
//      mrf::gui::fb::Loger::getInstance()->trace("Diffuse Color retrieved");
//      phong_mat->setDiffuseColor(diffuse_color);
//    }
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::SPECULAR_CMP_MK))
//  {
//    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");
//
//    tinyxml2::XMLElement *specular_elem = a_mat_element->FirstChildElement(SceneLexer::SPECULAR_CMP_MK);
//
//
//    int r_exponent = -1;
//    int valueOK    = specular_elem->QueryIntAttribute(SceneLexer::EXPONENT_AT, &r_exponent);
//
//    if (valueOK == tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->trace("Exponent retrieved", r_exponent);
//
//      if (r_exponent < 0)
//      {
//        mrf::gui::fb::Loger::getInstance()->trace("Exponent is negative, reversing its value");
//      }
//
//
//      phong_mat->setExponent(math::MathI::absVal(r_exponent));
//    }
//    else
//    {
//      mrf::gui::fb::Loger::getInstance()->trace(" COuld not retrieve exponent attribute. Assigning default value");
//    }
//
//    mrf::materials::COLOR specular_color        = ParserHelper::retrieveColor(specular_elem);
//    float specular_reflectivity = ParserHelper::retrieveReflectivity(specular_elem);
//
//    //if (retrieveReflectivity(specular_elem, specular_reflectivity))
//    {
//      mrf::gui::fb::Loger::getInstance()->trace("Specular reflectivity retrieved");
//      phong_mat->setSpecularReflectivity(specular_reflectivity);
//    }
//    //else
//    //{
//    //  mrf::gui::fb::Loger::getInstance()->warn("  default value=" + std::to_string(phong_mat->specularAlbedo()));
//    //}
//
//    //if (retrieveColor(specular_elem, specular_color))
//    {
//      mrf::gui::fb::Loger::getInstance()->trace("Specular Color retrieved");
//      phong_mat->setSpecularColor(specular_color);
//    }
//  }
//}
//
//void MaterialSceneParser::parsePrincipled(
//    tinyxml2::XMLElement *            a_mat_element,
//    mrf::materials::PrincipledDisney *principled_mat)
//{
//  if (a_mat_element->FirstChildElement(SceneLexer::BASE_COLOR_MK))
//  {
//    tinyxml2::XMLElement *base_color_elem = a_mat_element->FirstChildElement(SceneLexer::BASE_COLOR_MK);
//
//    mrf::gui::fb::Loger::getInstance()->trace(" BaseColor component detected ");
//
//    mrf::materials::COLOR baseColor = ParserHelper::retrieveColor(base_color_elem);
//
//    //if (retrieveColor(base_color_elem, baseColor))
//    {
//      mrf::gui::fb::Loger::getInstance()->trace("  BaseColor retrieved");
//      principled_mat->setBaseColor(baseColor);
//    }
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::ROUGHNESS_MK))
//  {
//    tinyxml2::XMLElement *roughness_elem = a_mat_element->FirstChildElement(SceneLexer::ROUGHNESS_MK);
//    float       roughness      = 0.5f;
//    int         roughnessOK    = roughness_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &roughness);
//    if (roughnessOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->warn("Roughness not found, using default value (0.5f).");
//      roughness = 0.5f;
//    }
//    principled_mat->setRoughness(roughness);
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::CLEARCOAT_MK))
//  {
//    tinyxml2::XMLElement *clearcoat_elem = a_mat_element->FirstChildElement(SceneLexer::CLEARCOAT_MK);
//    float       clearcoat      = 0.f;
//    int         clearcoatOK    = clearcoat_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &clearcoat);
//    if (clearcoatOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->warn("Clearcoat not found, using default value (0.f).");
//      clearcoat = 0.f;
//    }
//    principled_mat->setClearcoat(clearcoat);
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::CLEARCOATGLOSS_MK))
//  {
//    tinyxml2::XMLElement *clearcoatGloss_elem = a_mat_element->FirstChildElement(SceneLexer::CLEARCOATGLOSS_MK);
//    float       clearcoatGloss      = 0.f;
//    int         clearcoatGlossOK    = clearcoatGloss_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &clearcoatGloss);
//    if (clearcoatGlossOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->warn("Clearcoat Gloss not found, using default value (0.f).");
//      clearcoatGloss = 0.f;
//    }
//    principled_mat->setClearcoatGloss(clearcoatGloss);
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::SUBSURFACE_MK))
//  {
//    tinyxml2::XMLElement *subsurface_elem = a_mat_element->FirstChildElement(SceneLexer::SUBSURFACE_MK);
//    float       subsurface      = 0.f;
//    int         subsurfaceOK    = subsurface_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &subsurface);
//    if (subsurfaceOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->warn("Subsurface not found, using default value (0.f).");
//      subsurface = 0.f;
//    }
//    principled_mat->setSubsurface(subsurface);
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::SHEEN_MK))
//  {
//    tinyxml2::XMLElement *sheen_elem = a_mat_element->FirstChildElement(SceneLexer::SHEEN_MK);
//    float       sheen      = 0.f;
//    int         sheenOK    = sheen_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &sheen);
//    if (sheenOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->warn("Sheen not found, using default value (0.f).");
//      sheen = 0.f;
//    }
//    principled_mat->setSheen(sheen);
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::SHEENTINT_MK))
//  {
//    tinyxml2::XMLElement *sheenTint_elem = a_mat_element->FirstChildElement(SceneLexer::SHEENTINT_MK);
//    float       sheenTint      = 0.f;
//    int         sheenTintOK    = sheenTint_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &sheenTint);
//    if (sheenTintOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->warn("Sheen tint not found, using default value (0.f).");
//      sheenTint = 0.f;
//    }
//    principled_mat->setSheenTint(sheenTint);
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::SPECULAR_MK))
//  {
//    tinyxml2::XMLElement *specular_elem = a_mat_element->FirstChildElement(SceneLexer::SPECULAR_MK);
//    float       specular      = 0.f;
//    int         specularOK    = specular_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &specular);
//    if (specularOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->warn("Specular not found, using default value (0.f).");
//      specular = 0.f;
//    }
//    principled_mat->setSpecular(specular);
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::SPECULARTINT_MK))
//  {
//    tinyxml2::XMLElement *specularTint_elem = a_mat_element->FirstChildElement(SceneLexer::SPECULARTINT_MK);
//    float       specularTint      = 0.f;
//    int         specularTintOK    = specularTint_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &specularTint);
//    if (specularTintOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->warn("Specular tint not found, using default value (0.f).");
//      specularTint = 0.f;
//    }
//    principled_mat->setSpecularTint(specularTint);
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::ANISOTROPIC_MK))
//  {
//    tinyxml2::XMLElement *anisotropic_elem = a_mat_element->FirstChildElement(SceneLexer::ANISOTROPIC_MK);
//    float       anisotropic      = 0.f;
//    int         anisotropicOK    = anisotropic_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &anisotropic);
//    if (anisotropicOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->warn("Anisotropic not found, using default value (0.f).");
//      anisotropic = 0.f;
//    }
//    principled_mat->setAnisotropic(anisotropic);
//  }
//
//  if (a_mat_element->FirstChildElement(SceneLexer::METALLIC_MK))
//  {
//    tinyxml2::XMLElement *metallic_elem = a_mat_element->FirstChildElement(SceneLexer::METALLIC_MK);
//    float       metallic      = 0.f;
//    int         metallicOK    = metallic_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &metallic);
//    if (metallicOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->warn("Metallic not found, using default value (0.f).");
//      metallic = 0.f;
//    }
//    principled_mat->setMetallic(metallic);
//  }
//}
//
//bool MaterialSceneParser::parseUTIATexture(tinyxml2::XMLElement *a_tex_element, mrf::materials::UTIA *utia_mat)
//{
//  bool        texture_parsed = false;
//  std::string texture_path;
//  if (a_tex_element)
//  {
//    std::string file;
//    if (!a_tex_element->Attribute(SceneLexer::FILE_PATH_AT))
//    {
//      mrf::gui::fb::Loger::getInstance()->fatal(" NO FILE PATH SPECIFIED for this texture. ABORTING !!!");
//    }
//    else
//    {
//      std::string texture_file = a_tex_element->Attribute(SceneLexer::FILE_PATH_AT);
//
//      mrf::gui::fb::Loger::getInstance()->trace(" Found : TEXTURE FILE: ", texture_file);
//      texture_path = _current_directory + texture_file;
//      mrf::gui::fb::Loger::getInstance()->trace(" Full Path of file is: ", texture_path);
//
//      texture_parsed = true;
//    }
//  }
//
//  if (texture_parsed)
//  {
//    if (!checkErrorOnTextureLoading(utia_mat->loadUTIAData(texture_path), texture_path))
//    {
//      delete utia_mat;
//      return false;
//    }
//
//
//    unsigned int interpolate = 0;
//    int          interpolOK  = a_tex_element->QueryUnsignedAttribute(SceneLexer::UTIA_INTERPOLATION_AT, &interpolate);
//    utia_mat->setInterpolate(interpolate);
//
//    //int param;
//    //int paramOK = a_tex_element->QueryIntAttribute(SceneLexer::UTIA_PARAMETERIZATION_AT, &param);
//    //if (paramOK != XML_SUCCESS)
//    //{
//    //  if (utia_mat->getPhiVDiv() * utia_mat->getPhiIDiv() == 1)
//    //    utia_mat->setParameterization(UTIA::ISOTROPIC_DH);
//    //  else
//    //    utia_mat->setParameterization(UTIA::STANDARD);
//    //}
//    //else
//    //{
//    //  //TODO use string ?
//    //  utia_mat->setParameterization(param);
//    //}
//
//    const char *param;
//    int         paramOK = a_tex_element->QueryStringAttribute(SceneLexer::UTIA_PARAMETERIZATION_AT, &param);
//    if (paramOK != tinyxml2::XML_SUCCESS)
//    {
//      if (utia_mat->getPhiVDiv() * utia_mat->getPhiIDiv() == 1)
//        utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_2D);
//      else
//        utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D);
//    }
//    else
//    {
//      std::string param_str = std::string(param);
//      if (param_str.compare(SceneLexer::UTIA_2D_VAL) == 0)
//      {
//        utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_2D);
//      }
//      else if (param_str.compare(SceneLexer::RUSINKIEWICZ_2D_VAL) == 0)
//      {
//        utia_mat->setParameterization(mrf::materials::MeasuredMaterial::RUSINKIEWICZ_2D);
//      }
//      else if (param_str.compare(SceneLexer::RUSINKIEWICZ_2D_COS_VAL) == 0)
//      {
//        utia_mat->setParameterization(mrf::materials::MeasuredMaterial::RUSINKIEWICZ_2D_COS);
//      }
//      else if (param_str.compare(SceneLexer::RUSINKIEWICZ_4D_VAL) == 0)
//      {
//        utia_mat->setParameterization(mrf::materials::MeasuredMaterial::RUSINKIEWICZ_4D);
//      }
//      else if (param_str.compare(SceneLexer::UTIA_4D_H_VAL) == 0)
//      {
//        utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D_H);
//      }
//      else if (param_str.compare(SceneLexer::UTIA_4D_H_NL_VAL) == 0)
//      {
//        utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D_H_NL);
//      }
//      else if (param_str.compare(SceneLexer::HEMISPHERICAL_VAL) == 0)
//      {
//        utia_mat->setParameterization(mrf::materials::MeasuredMaterial::HEMISPHERICAL_ALBEDO);
//      }
//      else
//      {
//        utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D);
//      }
//    }
//
//    tinyxml2::XMLElement *div_elem = a_tex_element->FirstChildElement(SceneLexer::UTIA_PARAM_MK);
//
//    unsigned int theta_v = utia_mat->width();
//    unsigned int phi_v   = 1;
//    unsigned int theta_i = utia_mat->height();
//    unsigned int phi_i   = 1;
//    if (div_elem)
//    {
//      //values are not modified if attribute does not exist.
//      int divOK = div_elem->QueryUnsignedAttribute(SceneLexer::UTIA_THETA_V_AT, &theta_v);
//      divOK &= div_elem->QueryUnsignedAttribute(SceneLexer::UTIA_PHI_V_AT, &phi_v);
//      divOK &= div_elem->QueryUnsignedAttribute(SceneLexer::UTIA_THETA_I_AT, &theta_i);
//      divOK &= div_elem->QueryUnsignedAttribute(SceneLexer::UTIA_PHI_I_AT, &phi_i);
//    }
//    else
//    {
//      if (utia_mat->getParameterization() == mrf::materials::UTIA::UTIA_2D
//          || utia_mat->getParameterization() == mrf::materials::UTIA::RUSINKIEWICZ_2D
//          || utia_mat->getParameterization() == mrf::materials::UTIA::RUSINKIEWICZ_2D_COS)
//      {
//        //If isotropic DH requested, only warn that image dimension are used, do not force the V/I parameterization.
//        mrf::gui::fb::Loger::getInstance()->trace("No parameter markup detected, using image resolution as theta (D/H) resolution.");
//      }
//      else
//      {
//        mrf::gui::fb::Loger::getInstance()->trace(
//            "No parameter markup detected, using isotropic parameterization with image resolution as theta (V/I) resolution.");
//        //Force isotropic parameterization.
//        utia_mat->setParameterization(mrf::materials::UTIA::UTIA_2D);
//      }
//    }
//
//    utia_mat->setThetaVDiv(theta_v);
//    utia_mat->setPhiVDiv(phi_v);
//    utia_mat->setThetaIDiv(theta_i);
//    utia_mat->setPhiIDiv(phi_i);
//
//    mrf::gui::fb::Loger::getInstance()->trace("Parsed and loaded single UTIA texture with param:");
//    mrf::gui::fb::Loger::getInstance()->trace("Theta V: " + std::to_string(theta_v) + "| Phi V : " + std::to_string(phi_v));
//    mrf::gui::fb::Loger::getInstance()->trace("Theta I: " + std::to_string(theta_i) + "| Phi I : " + std::to_string(phi_i));
//  }
//  return true;
//}
//
//bool MaterialSceneParser::parseIndexedUTIATexture(
//    tinyxml2::XMLElement *       a_tex_element,
//    mrf::materials::IndexedUTIA *idx_utia_mat,
//    int                          tex_index)
//{
//  bool        texture_parsed = false;
//  std::string texture_path;
//  if (a_tex_element)
//  {
//    std::string file;
//    if (!a_tex_element->Attribute(SceneLexer::FILE_PATH_AT))
//    {
//      mrf::gui::fb::Loger::getInstance()->fatal(" NO FILE PATH SPECIFIED for this texture. ABORTING !!!");
//    }
//    else
//    {
//      std::string texture_file = a_tex_element->Attribute(SceneLexer::FILE_PATH_AT);
//
//      mrf::gui::fb::Loger::getInstance()->trace(" Found : TEXTURE FILE: ", texture_file);
//      texture_path = _current_directory + texture_file;
//      mrf::gui::fb::Loger::getInstance()->trace(" Full Path of file is: ", texture_path);
//
//      texture_parsed = true;
//    }
//  }
//
//  if (texture_parsed)
//  {
//    unsigned int mat_index;
//    int          rOK = a_tex_element->QueryUnsignedAttribute(SceneLexer::UTIA_INDEX_AT, &mat_index);
//    if (rOK != tinyxml2::XML_SUCCESS) mat_index = tex_index;
//    idx_utia_mat->setMatIndex(mat_index, tex_index);
//
//    if (!checkErrorOnTextureLoading(idx_utia_mat->loadUTIAData(texture_path, tex_index), texture_path))
//    {
//      delete idx_utia_mat;
//      return false;
//    }
//
//    unsigned int theta_v, phi_v, theta_i, phi_i;
//    rOK &= a_tex_element->QueryUnsignedAttribute(SceneLexer::UTIA_THETA_V_AT, &theta_v);
//    idx_utia_mat->setThetaVDiv((rOK != tinyxml2::XML_SUCCESS) ? idx_utia_mat->width(tex_index) : theta_v, tex_index);
//
//    rOK &= a_tex_element->QueryUnsignedAttribute(SceneLexer::UTIA_PHI_V_AT, &phi_v);
//    idx_utia_mat->setPhiVDiv((rOK != tinyxml2::XML_SUCCESS) ? 1 : phi_v, tex_index);
//
//    rOK &= a_tex_element->QueryUnsignedAttribute(SceneLexer::UTIA_THETA_I_AT, &theta_i);
//    idx_utia_mat->setThetaIDiv((rOK != tinyxml2::XML_SUCCESS) ? idx_utia_mat->height(tex_index) : theta_i, tex_index);
//
//    rOK &= a_tex_element->QueryUnsignedAttribute(SceneLexer::UTIA_PHI_I_AT, &phi_i);
//    idx_utia_mat->setPhiIDiv((rOK != tinyxml2::XML_SUCCESS) ? 1 : phi_i, tex_index);
//
//
//    const char *param;
//    int         paramOK = a_tex_element->QueryStringAttribute(SceneLexer::UTIA_PARAMETERIZATION_AT, &param);
//    if (paramOK != tinyxml2::XML_SUCCESS)
//    {
//      if (phi_v * phi_i == 1)
//        idx_utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_2D, tex_index);
//      else
//        idx_utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D, tex_index);
//    }
//    else
//    {
//      std::string param_str = std::string(param);
//      if (param_str.compare(SceneLexer::UTIA_2D_VAL) == 0)
//      {
//        idx_utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_2D, tex_index);
//      }
//      else if (param_str.compare(SceneLexer::RUSINKIEWICZ_2D_VAL) == 0)
//      {
//        idx_utia_mat->setParameterization(mrf::materials::MeasuredMaterial::RUSINKIEWICZ_2D, tex_index);
//      }
//      else if (param_str.compare(SceneLexer::RUSINKIEWICZ_2D_COS_VAL) == 0)
//      {
//        idx_utia_mat->setParameterization(mrf::materials::MeasuredMaterial::RUSINKIEWICZ_2D_COS, tex_index);
//      }
//      else if (param_str.compare(SceneLexer::RUSINKIEWICZ_4D_VAL) == 0)
//      {
//        idx_utia_mat->setParameterization(mrf::materials::MeasuredMaterial::RUSINKIEWICZ_4D, tex_index);
//      }
//      else if (param_str.compare(SceneLexer::UTIA_4D_H_VAL) == 0)
//      {
//        idx_utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D_H, tex_index);
//      }
//      else if (param_str.compare(SceneLexer::UTIA_4D_H_NL_VAL) == 0)
//      {
//        idx_utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D_H_NL, tex_index);
//      }
//      else if (param_str.compare(SceneLexer::HEMISPHERICAL_VAL) == 0)
//      {
//        idx_utia_mat->setParameterization(mrf::materials::MeasuredMaterial::HEMISPHERICAL_ALBEDO, tex_index);
//      }
//      else
//      {
//        idx_utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D, tex_index);
//      }
//    }
//
//    //idx_utia_mat->setParameterization(MeasuredMaterial::UTIA_4D, tex_index);
//
//    mrf::gui::fb::Loger::getInstance()->trace("Parsed and loaded UTIA texture " + std::to_string(tex_index) + " with param:");
//    mrf::gui::fb::Loger::getInstance()->trace("Theta V: " + std::to_string(theta_v) + "| Phi V : " + std::to_string(phi_v));
//    mrf::gui::fb::Loger::getInstance()->trace("Theta I: " + std::to_string(theta_i) + "| Phi I : " + std::to_string(phi_i));
//  }
//  return true;
//}
//
//bool MaterialSceneParser::parseUTIA(tinyxml2::XMLElement *a_mat_element, mrf::materials::UTIA *utia_mat)
//{
//  std::string texture_path;
//
//  tinyxml2::XMLElement *texture_elem = a_mat_element->FirstChildElement(SceneLexer::UTIA_TEXTURE_MK);
//  if (parseUTIATexture(texture_elem, utia_mat)) return true;
//
//  return false;
//}
//
//bool MaterialSceneParser::parseIndexedUTIA(
//    tinyxml2::XMLElement *       a_mat_element,
//    mrf::materials::IndexedUTIA *idx_utia_mat)
//{
//  std::string texture_path;
//
//  unsigned int interpolate;
//
//  int rOK = a_mat_element->QueryUnsignedAttribute(SceneLexer::UTIA_INTERPOLATION_AT, &interpolate);
//  idx_utia_mat->setInterpolate(interpolate);
//
//  tinyxml2::XMLElement *all_tex_mk = a_mat_element->FirstChildElement(SceneLexer::UTIA_LIST_MK);
//  if (all_tex_mk)
//  {
//    mrf::gui::fb::Loger::getInstance()->info("Found multiple UTIA, parsing them.");
//    // For each UTIA NOW
//    tinyxml2::XMLElement *texture_elem = all_tex_mk->FirstChildElement(SceneLexer::UTIA_TEXTURE_MK);
//    mrf::gui::fb::Loger::getInstance()->info("Found multiple UTIA, parsing them.");
//
//    int i = 0;
//    for (; texture_elem; texture_elem = texture_elem->NextSiblingElement())
//    {
//      if (parseIndexedUTIATexture(texture_elem, idx_utia_mat, i)) ++i;
//    }
//    mrf::gui::fb::Loger::getInstance()->info("Parsed and loaded " + std::to_string(i) + " UTIA textures.");
//
//    if (parseTexture(a_mat_element, SceneLexer::UTIA_INDEX_MAP_MK, texture_path))
//    {
//      if (!checkErrorOnTextureLoading(idx_utia_mat->loadUTIAMap(texture_path), texture_path))
//      {
//        //delete lphong_mat;
//        return false;
//      }
//      mrf::gui::fb::Loger::getInstance()->info("Normalizing indices");
//      idx_utia_mat->normalizeMapIndices();
//    }
//
//    return i > 0 ? true : false;
//  }
//
//  return false;
//}

bool MaterialSceneParser::parseMultiMaterial(
    tinyxml2::XMLElement *         a_mat_element,
    mrf::materials::MultiMaterial *multi_mat)
{
  if (a_mat_element->FirstChildElement(SceneLexer::TEXTURE_INDEX_MK))
  {
    //const char *path = a_mat_element->FirstChildElement(SceneLexer::TEXTURE_INDEX_MK)->Attribute(SceneLexer::FILE_PATH_AT);
    //if(path) multi_mat->setTexturePath(path);
    std::string path;
    if (parseTexture(a_mat_element, SceneLexer::TEXTURE_INDEX_MK, path))
    {
      if (!_parser.checkErrorOnTextureLoading(multi_mat->loadIndexTexture(path), path))
      {
        mrf::gui::fb::Loger::getInstance()->fatal("In multi-material: Specified a texture index without path.");
        return false;
      }
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->fatal("In multi-material: Specified a texture index without path.");
      return false;
    }
  }

  tinyxml2::XMLElement *material = a_mat_element->FirstChildElement(SceneLexer::MATERIAL_MK);

  mrf::gui::fb::Loger::getInstance()->trace("Materials Found ! Parsing it....");

  for (; material; material = material->NextSiblingElement())
  {
    std::string element_name = material->Name();

    if (element_name.compare(SceneLexer::MATERIAL_MK) == 0)
    {
      std::string name(material->Attribute(SceneLexer::NAME_AT));

      /*      if (_brdf_map.count(name) != 1)
              rOK &= addMaterial(material, scene); */
      //multi_mat->addEntry(_brdf_map[name], name);

      multi_mat->addNameEntry(name);


      unsigned int idx;
      int          rOK = material->QueryUnsignedAttribute(SceneLexer::MAT_INDEX_AT, &idx);
      if (rOK == tinyxml2::XML_SUCCESS)
      {
        multi_mat->addLocalIdEntry(idx);
      }
    }
  }

  return true;
}

bool MaterialSceneParser::parseTexture(
    tinyxml2::XMLElement *a_mat_element,
    char const *          texture_markup,
    std::string &         path)
{
  tinyxml2::XMLElement *texture_elem = a_mat_element->FirstChildElement(texture_markup);
  if (texture_elem)
  {
    std::string file;

    if (!texture_elem->Attribute(SceneLexer::FILE_PATH_AT))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" NO FILE PATH SPECIFIED for this texture. ABORTING !!!");
      return false;
    }

    std::string texture_file = texture_elem->Attribute(SceneLexer::FILE_PATH_AT);

    mrf::gui::fb::Loger::getInstance()->trace(" Found : TEXTURE FILE: ", texture_file);
    path = _current_directory + texture_file;
    mrf::gui::fb::Loger::getInstance()->trace(" Full Path of file is: ", path);

    return true;
  }
  return false;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

/*
bool MaterialSceneParser::addAnisotropicNGAN(XMLElement *elem, Scene &scene, std::string const &material_name)
{
  mrf::gui::fb::Loger::getInstance()->debug( " Retrieving AnisotopicNGAN material " );


  //-------------------------------------------------------------------------
  // Diffuse Component
  //-------------------------------------------------------------------------
  Color3F diffuse_color;
  if ( elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK ) )
  {
    XMLElement* diffuse_elem = elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse component detected ");


    if ( !retrieveRGBColor( diffuse_elem, diffuse_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the diffuse color ");
      return false;
    }

    mrf::gui::fb::Loger::getInstance()->trace("Diffuse Color retrieved");

  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No Diffuse component For this anisotropic material ");
  }


  //-------------------------------------------------------------------------
  // Specular Component
  //-------------------------------------------------------------------------
  Color3F specular_color;
  float fresnel_coeff = 1.0f;
  float specular_exponent = 1.0f;

  if ( elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK ))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");

    XMLElement* specular_elem = elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK );

    int valueOK  = specular_elem->QueryFloatAttribute( SceneLexer::FRESNEL_AT, &fresnel_coeff );

    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Fresnel coefficient retrieved ", fresnel_coeff);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while trying to retrieve the fresnel coefficient ");
      return false;
    }

    valueOK = specular_elem->QueryFloatAttribute( SceneLexer::EXPONENT_AT, &specular_exponent );
    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Specular Exponent retrieved ", specular_exponent);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while trying to retrieve the specular exponent ");
      return false;
    }



    //-------------------------------------------------------------------------
    // Retrieve Specular Color
    //-------------------------------------------------------------------------
    if ( !retrieveRGBColor( specular_elem, specular_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the specular color ");
      return false;
    }

    mrf::gui::fb::Loger::getInstance()->trace("Specular Color retrieved");

  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace( " No specular component for this Anisotropic material ");
  }


  ASNgan* ngan_mat = new ASNgan( material_name, diffuse_color, specular_color,
                                 specular_exponent, fresnel_coeff );

  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( ngan_mat );
  //_umat_counter++;

  return true;
}
*/


//-------------------------------------------------------------------------
// Anisoptropic Phong Material by A and S.
//-------------------------------------------------------------------------
/*
bool MaterialSceneParser::addAnisotropicPhong(XMLElement *elem, mrf::rendering::Scene &scene, std::string const &material_name)
{
  //-------------------------------------------------------------------------
  // Parse Diffuse component if any
  //-------------------------------------------------------------------------

  Color3F diffuse_color;
  float   diffuse_reflectivity = 0.0f;

  if ( elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK ) )
  {
    XMLElement* diffuse_elem = elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse component detected ");


    if ( !retrieveRGBColor( diffuse_elem, diffuse_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the diffuse color ");
      return false;
    }

    mrf::gui::fb::Loger::getInstance()->trace("Diffuse Color retrieved");


    if ( !retrieveReflectivity( diffuse_elem, diffuse_reflectivity) )
    {
      mrf::gui::fb::Loger::getInstance()->warn(" Problem while trying to retreive the diffuse reflectivity ");

      diffuse_reflectivity = diffuse_color.avgCmp();
      mrf::gui::fb::Loger::getInstance()->warn( " Assigning Average color value to it ", diffuse_reflectivity );
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->trace("Diffuse reflectivity retrieved", diffuse_reflectivity);
    }
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No Diffuse component For this anisotropic material ");
  }


  //-------------------------------------------------------------------------
  // Parse Specular component if any
  //-------------------------------------------------------------------------
  Color3F specular_color;
  float specular_reflectivity = 0.0f;
  float nu = 0.0f;
  float nv = 0.0f;

  if ( elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK ))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");

    // Parse both exponents

    XMLElement* specular_elem = elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK );
    float r_nu_exponent = -1;
    float r_nv_exponent = -1;

    int valueOK  = specular_elem->QueryFloatAttribute( SceneLexer::NU_AT, &r_nu_exponent );
    int valueOK2 = specular_elem->QueryFloatAttribute( SceneLexer::NV_AT, &r_nv_exponent );


    if (  (valueOK == TIXML_SUCCESS) && (valueOK2 == TIXML_SUCCESS) )
    {
      mrf::gui::fb::Loger::getInstance()->trace("nu retrieved ", r_nu_exponent);
      mrf::gui::fb::Loger::getInstance()->trace("nv retrieved ", r_nv_exponent);

      if ( r_nu_exponent < 0 )
      {
        mrf::gui::fb::Loger::getInstance()->trace("Exponent is nu negative, reversing its value");
      }
      if ( r_nv_exponent < 0 )
      {
        mrf::gui::fb::Loger::getInstance()->trace("Exponent is nv negative, reversing its value");
      }

      nu = Math::absVal(r_nu_exponent);
      nv = Math::absVal(r_nv_exponent);

    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->trace(" Could not retrieve exponent attribute. Assigning default values");
    }



    if ( !retrieveRGBColor( specular_elem, specular_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the specular color ");
      return false;
    }

    mrf::gui::fb::Loger::getInstance()->trace("Specular Color retrieved");

    //-------------------------------------------------------------------------
    // Retrieve Specular Color and Reflectivities
    //-------------------------------------------------------------------------
    if ( !retrieveReflectivity( specular_elem, specular_reflectivity) )
    {
      mrf::gui::fb::Loger::getInstance()->warn(" Problem while trying to retrive the specular reflectivity ");

      specular_reflectivity = specular_color.avgCmp();

      mrf::gui::fb::Loger::getInstance()->warn( " Assigning Average color value to it : ", specular_reflectivity );
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->trace("Specular reflectivity retrieved", specular_reflectivity );
    }



  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace( " No specular component for this Anisotropic material ");
  }

  AnisoPhong* aniso_phong_mat = new AnisoPhong( material_name,
      diffuse_color,
      diffuse_reflectivity,
      specular_color,
      specular_reflectivity,
      nu, nv );


  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( aniso_phong_mat );
  return true;
}
*/


/*
bool MaterialSceneParser::addAshikhminShirleyIsotropic(
    XMLElement *           elem,
    mrf::rendering::Scene &scene,
    std::string const &    material_name)
{
  //-------------------------------------------------------------------------
  // Parse Diffuse component if any
  //-------------------------------------------------------------------------

  Color3F diffuse_color( 0.0f, 0.0f, 0.0f);

  if ( elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK ) )
  {
    XMLElement* diffuse_elem = elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse component detected ");

    if ( !retrieveRGBColor( diffuse_elem, diffuse_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the diffuse color ");
      return false;
    }

    mrf::gui::fb::Loger::getInstance()->trace("Diffuse Color retrieved");

  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No Diffuse component For this AS Isotropic material ");
  }


  //-------------------------------------------------------------------------
  // Parse Specular component if any
  //-------------------------------------------------------------------------
  Color3F specular_color(0.0f, 0.0f, 0.0f);
  float specular_exponent = 0.0f;

  if ( elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK ))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");

    XMLElement* specular_elem = elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK );


    int valueOK = specular_elem->QueryFloatAttribute( SceneLexer::EXPONENT_AT, &specular_exponent );
    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Specular Exponent retrieved ", specular_exponent);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while trying to retrieve the specular exponent ");
      return false;
    }

    //-------------------------------------------------------------------------
    // Retrieve Specular Color
    //-------------------------------------------------------------------------
    if ( !retrieveRGBColor( specular_elem, specular_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the specular color ");
      return false;
    }

    mrf::gui::fb::Loger::getInstance()->trace("Specular Color retrieved");
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace( " No specular component for this Anisotropic material ");
  }


  ASIso* asiso_mat = new ASIso( material_name, diffuse_color, specular_color, specular_exponent );


  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( asiso_mat );

  return true;
}
*/


/*
bool MaterialSceneParser::addDBRDF(XMLElement *elem, mrf::rendering::Scene &scene, std::string const &material_name)
{
  Color3F specular_color(0.0f, 0.0f, 0.0f);
  if ( elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK ))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");

    XMLElement* specular_elem = elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK );
    if ( !retrieveRGBColor( specular_elem, specular_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the specular color ");
      return false;
    }

    mrf::gui::fb::Loger::getInstance()->trace("Specular Color retrieved", specular_color);
  }


  float sigma_x = 1.0f;
  float sigma_y = 1.0f;
  float fresnel_at_incidence = 1.0f;


  DBRDF* dbrdf_mat = new DBRDF( material_name,
                                specular_color,
                                sigma_x, sigma_y,
                                fresnel_at_incidence );

  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( dbrdf_mat );

  return true;
}
*/


/*
bool MaterialSceneParser::addFBTDF(XMLElement *elem, Scene &scene, std::string const &material_name)
{
  Color3F color( 1.0f, 1.0f, 1.0f);
  retrieveRGBColor( elem, color );



  //-------------------------------------------------------------------------
  // Specular Component
  //-------------------------------------------------------------------------
  Color3F specular_cmp;
  if ( elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK ) )
  {
    XMLElement* spec_elem = elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");

    if ( !retrieveRGBColor( spec_elem, specular_cmp))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the specular component ");
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Specular Component retrieved");
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No Specular component For this refractive material ");
  }

  //-------------------------------------------------------------------------
  // Transmittance Component
  //-------------------------------------------------------------------------
  Color3F trans_cmp;
  float ior = 1.0f;

  if ( elem->FirstChildElement( SceneLexer::TRANS_CMP_MK ) )
  {
    XMLElement* trans_elem = elem->FirstChildElement( SceneLexer::TRANS_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Trans component detected ");

    if ( !retrieveRGBColor( trans_elem, trans_cmp))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the transmittance component ");
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Transmittance component retrieved");


    int valueOK  = trans_elem->QueryFloatAttribute( SceneLexer::IOR_AT, &ior );


    if (  valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace( " Index of Refraction retrieved", ior );
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->trace( " Could not retrieve Index of Refraction. Assigning default (1.0) value." );
    }

  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No transmittance component for this refractive material ");
  }



  FBTDF* fbtdf_mat = new FBTDF( material_name,
                                color,
                                trans_cmp,
                                specular_cmp,
                                ior );

  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( fbtdf_mat );

  return true;
}
*/

/*
bool MaterialSceneParser::addSGD(XMLElement *elem, mrf::rendering::Scene &scene, std::string const &material_name)
{
  mrf::gui::fb::Loger::getInstance()->debug("ADDING SGD MATERIAL ");

  Color3D diffuse_color;
  if ( elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK ) )
  {
    XMLElement* diffuse_elem = elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse component detected ");

    if ( !retrieveRGBColor( diffuse_elem, diffuse_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retreieve the diffuse color ");
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Diffuse Color retrieved");
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No Diffuse component For this  material ");
  }

  Color3D specular_color;

  Color3D alpha;
  Color3D power;
  Color3D F0;
  Color3D F1;
  Color3D K_alpha_p;
  Color3D lambda;
  Color3D c;
  Color3D k;
  Color3D theta_o;

  if ( elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK ))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");

    XMLElement* specular_elem =  elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK );

    if ( !retrieveRGBColor( specular_elem, specular_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the specular color ");
      return false;
    }

    XMLElement* sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::ALPHA_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got Alpha Element");
      if( !retrieveRGBColor(sub_spec_elem, alpha))
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Problem while trying to retrieve the alpha color");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::POWER_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got Power Element");
      if( !retrieveRGBColor(sub_spec_elem, power))
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Problem while trying to retrieve the Power color");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::F0_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got F0 Element");
      if( !retrieveRGBColor(sub_spec_elem, F0))
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Problem while trying to retrieve the F0 color");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::F1_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got F1 Element");
      if( !retrieveRGBColor(sub_spec_elem, F1))
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Problem while trying to retrieve the F1 color");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::K_ALPHA_P_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got K_alpha_p Element");
      if( !retrieveRGBColor(sub_spec_elem, K_alpha_p))
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Problem while trying to retrieve the K_alpha_p color");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::LAMBDA_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got Lambda Element");
      if( !retrieveRGBColor(sub_spec_elem, lambda))
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Problem while trying to retrieve the Lambda color");
        return false;
      }
    }
    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::C_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got c Element");
      if( !retrieveRGBColor(sub_spec_elem, c))
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Problem while trying to retrieve the c color");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::K_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got k Element");
      if( !retrieveRGBColor(sub_spec_elem, k))
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Problem while trying to retrieve the k color");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::THETA_O_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got THETA_O Element");
      if( !retrieveRGBColor(sub_spec_elem, theta_o))
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Problem while trying to retrieve the THETA_O color");
        return false;
      }
    }

  }


  mrf::materials::SGD*  sgd_mat = new mrf::materials::SGD(material_name,
      diffuse_color,
      specular_color,
      alpha, power, F0,F1,
      K_alpha_p, lambda,
      c, k, theta_o );

  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( sgd_mat );

  return true;
}
*/


/*
bool MaterialSceneParser::addGGXD(XMLElement *elem, mrf::rendering::Scene &scene, std::string const &material_name)
{
  Color3F diffuse_color;
  if ( elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK ) )
  {
    XMLElement* diffuse_elem = elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse component detected ");

    if ( !retrieveRGBColor( diffuse_elem, diffuse_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the diffuse color ");
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Diffuse Color retrieved");
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No Diffuse component For this  material ");
  }


  Color3F specular_color(1.0f, 1.0f, 1.0f);
  Color3F fresnel_color(1.0f, 1.0f, 1.0f);
  Color3F  ggxd_a_color(1.0f, 1.0f, 1.0f);

  float alpha = 0.0f;
  //float ggxd_a = 0.0f;
  float ggxd_b = 0.0f;
  float ggxd_c = 0.0f;


  if ( elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK ))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");

    XMLElement* specular_elem =  elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK );

    if ( !retrieveRGBColor( specular_elem, specular_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the specular color ");
      return false;
    }

    XMLElement* sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::GGXD_FRESNEL_MK );

    if ( !retrieveRGBColor( sub_spec_elem, fresnel_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the specular color ");
      return false;
    }


    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::ALPHA_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got Alpha Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &alpha );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("Alpha retrieved ", alpha);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving the alpha component ");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::GGXD_A_MK );

    if ( !retrieveRGBColor( sub_spec_elem, ggxd_a_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the A COLOR ");
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->trace(" A retrieved ", ggxd_a_color);

    // if( sub_spec_elem )
    // {
    //     mrf::gui::fb::Loger::getInstance()->trace("Got  GGX_Diffract_A Element");

    //     int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &ggxd_a );
    //     if ( valueOK == TIXML_SUCCESS )
    //     {
    //         mrf::gui::fb::Loger::getInstance()->trace("GGX Diffraction A value retrieved ", ggxd_a);
    //     }
    //     else
    //     {
    //         mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving ggx diffract value");
    //         return false;
    //     }
    // }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::GGXD_B_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got  GGX_Diffract_B Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &ggxd_b );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("GGX Diffraction B value retrieved ", ggxd_b);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving B ggx diffract value");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::GGXD_C_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got  GGX_Diffract_C Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &ggxd_c );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("GGX Diffraction C value retrieved ", ggxd_c);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving ggx C diffract value");
        return false;
      }
    }


  }



  GGXD* ggxd_mat = new GGXD(material_name,
                            diffuse_color,  specular_color, fresnel_color,
                            alpha,
                            ggxd_a_color,
                            ggxd_b,
                            ggxd_c);

  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( ggxd_mat );
  return true;
}
*/


/*
bool MaterialSceneParser::addLowSmooth(XMLElement *elem, mrf::rendering::Scene &scene, std::string const &material_name)
{
  Color3F diffuse_color;
  if ( elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK ) )
  {
    XMLElement* diffuse_elem = elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse component detected ");

    if ( !retrieveRGBColor( diffuse_elem, diffuse_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the diffuse color ");
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Diffuse Color retrieved");
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No Diffuse component For this  material ");
  }

  XMLElement*  sub_spec_elem = elem->FirstChildElement( SceneLexer::LOW_SMOOTH_A_MK );

  Color3F A_color;
  if ( !retrieveRGBColor( sub_spec_elem, A_color))
  {
    mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the A COLOR ");
    return false;
  }
  mrf::gui::fb::Loger::getInstance()->trace(" A retrieved ", A_color);


  float B=0.0f;

  sub_spec_elem =  elem->FirstChildElement( SceneLexer::LOW_SMOOTH_B_MK );
  if( sub_spec_elem )
  {
    mrf::gui::fb::Loger::getInstance()->trace("Got  LOW_SMOOTH B Element");

    int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &B );
    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Low Smooth BRDF Model, B value retrieved ", B);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving B for Low Smooth BRDF Model");
      return false;
    }
  }

  float C=0.0f;
  sub_spec_elem =  elem->FirstChildElement( SceneLexer::LOW_SMOOTH_C_MK );
  if( sub_spec_elem )
  {
    mrf::gui::fb::Loger::getInstance()->trace("Got  LOW_SMOOTH C Element");

    int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &C );
    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Low Smooth BRDF Model, C value retrieved ", C);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving C for Low Smooth BRDF Model");
      return false;
    }
  }


  float fresnel=0.0f;
  sub_spec_elem =  elem->FirstChildElement( SceneLexer::LOW_SMOOTH_ETA_MK );
  if( sub_spec_elem )
  {
    mrf::gui::fb::Loger::getInstance()->trace("Got Low Smooth Fresnel Element");

    int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &fresnel );
    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Low Smooth BRDF Model, B value retrieved ", fresnel);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving ETA for Low Smooth BRDF Model");
      return false;
    }
  }


  LowSmooth* low_smooth_mat = new LowSmooth( material_name,
      diffuse_color,
      A_color,
      B,
      C,
      fresnel );



  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( low_smooth_mat );

  return true;
}
*/


/*
bool MaterialSceneParser::addHe(XMLElement *elem, mrf::rendering::Scene &scene, std::string const &material_name)
{
  double sigma_0 = 1.0;
  if( elem->Attribute( SceneLexer::SIGMA_0_AT ) )
  {
    int valueOK = elem->QueryDoubleAttribute( SceneLexer::SIGMA_0_AT, &sigma_0 );
    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace(" Retrieved sigma_0: ", sigma_0);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->trace(" Warning could not retrieced value for sigma_0. Assigning default value: ", sigma_0);
    }
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Warning missing Sigma_0 attribute. Assigning default value: ", sigma_0);
  }

  double tau = 1.0;
  if( elem->Attribute( SceneLexer::TAU_AT ) )
  {
    int valueOK = elem->QueryDoubleAttribute( SceneLexer::TAU_AT, &tau );
    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace(" Retrieved tau: ", tau);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->trace(" Warning could not retrieced value for Tau. Assigning default value: ", tau);
    }
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Warning missing Tau attribute. Assigning default value: ", tau);
  }

  // Fresnel definition: Air by default
  Color3F eta(1.0f, 1.0f, 0.0f);
  Color3F k(0.0f, 0.0f, 0.0f);

  //Fresnel
  XMLElement* fresnel_elem =  elem->FirstChildElement( SceneLexer::CTD_FRESNEL_MK );
  if( fresnel_elem )
  {
    XMLElement* eta_element = fresnel_elem->FirstChildElement( SceneLexer::ETA_MK);
    if( eta_element )
    {
      rgbColor_from_Markup( eta_element, eta);

    }

    XMLElement* k_element = fresnel_elem->FirstChildElement( SceneLexer::K_MK);
    if( k_element )
    {
      rgbColor_from_Markup( k_element, k);
    }
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->warn(" HE BRDF Model: No Fresnel Markup found. Assigning default one");
  }//end if-else fresnel element


  //Diffuse Part now
  //TODO: Factorise this code which seems redundant
  //-------------------------------------------------------------------------
  // Diffuse Component
  //-------------------------------------------------------------------------
  Color3F diffuse_color;
  if ( elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK ) )
  {
    XMLElement* diffuse_elem = elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse component detected ");

    if ( !retrieveRGBColor( diffuse_elem, diffuse_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the diffuse color ");
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Diffuse Color retrieved");
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No Diffuse component HE material ");
  }

  //TODO: NB Terms for the infinite loop
  //TODO: NB_TERMS_AT


  //FINAL PART: Creating the Material
  mrf::materials::He*  he_mat = new He( material_name, diffuse_color, eta, k, sigma_0, tau, 10 );
  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( he_mat );

  return true;
}
*/


/*
bool MaterialSceneParser::addGenBeckmann(XMLElement *elem, mrf::rendering::Scene &scene, std::string const &material_name)
{
  using namespace mrf::materials;

  //Specular PART
  float b = 0.0f;
  float p = 0.0f;
  // Fresnel definition: Air by default
  Color3F eta(1.0f, 1.0f, 0.0f);
  Color3F k(0.0f, 0.0f, 0.0f);

  if ( elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK ))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");

    XMLElement* specular_elem =  elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK );

    //Fresnel
    XMLElement* sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::CTD_FRESNEL_MK );
    if( sub_spec_elem )
    {

      XMLElement* eta_element = sub_spec_elem->FirstChildElement( SceneLexer::ETA_MK);
      if( eta_element )
      {
        rgbColor_from_Markup( eta_element, eta);

      }

      XMLElement* k_element = sub_spec_elem->FirstChildElement( SceneLexer::K_MK);
      if( k_element )
      {
        rgbColor_from_Markup( k_element, k);
      }
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->warn(" MicroFacet GHS: No Fresnel Markup found. Assigning default one");
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::CTD_B_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got  b Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &b );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("MicroFacetGHS BRDF Model, B value retrieved ", b);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving B MicroFacetGHS  BRDF Model");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::CTD_P_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got  p Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &p );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("MicroFacetGHS BRDF Model, p value retrieved ", p);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving p MicroFacetGHS  BRDF Model");
        return false;
      }
    }
  }
  //---------------------------------------------------------------------------------------
  // End of Specular PART
  //---------------------------------------------------------------------------------------

  mrf::gui::fb::Loger::getInstance()->trace( " Eta parsed ", eta);
  mrf::gui::fb::Loger::getInstance()->trace( " k parsed ", k);
  mrf::gui::fb::Loger::getInstance()->trace( " b", b );
  mrf::gui::fb::Loger::getInstance()->trace( " p", p );

  //Diffuse Part now
  //TODO: Factorise this code which seems redundant
  //-------------------------------------------------------------------------
  // Diffuse Component
  //-------------------------------------------------------------------------
  Color3F diffuse_color;
  if ( elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK ) )
  {
    XMLElement* diffuse_elem = elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse component detected ");

    if ( !retrieveRGBColor( diffuse_elem, diffuse_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrive the diffuse color ");
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Diffuse Color retrieved");
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No Diffuse component HE material ");
  }

  GenBeckmann* a_gen_beck = new GenBeckmann(material_name, diffuse_color, eta, k, b, p);

  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( a_gen_beck );

  return true;
}
*/


/*
bool MaterialSceneParser::addMicroFacetGHS(XMLElement *elem, mrf::rendering::Scene &scene, std::string const &material_name)
{
  if( !elem->Attribute( SceneLexer::VARIANT_AT ) )
  {
    mrf::gui::fb::Loger::getInstance()->fatal(" VARIANT Attribute Missing for MicroFacet GHS BRDF");
    return false;
  }

  string variante_attr ( elem->Attribute( SceneLexer::VARIANT_AT ) );

  mrf::gui::fb::Loger::getInstance()->trace(" Variant detected is ", variante_attr );


  //Specular PART
  float b = 0.0f;
  float p = 0.0f;
  // Fresnel definition: Air by default
  Color3F eta(1.0f, 1.0f, 0.0f);
  Color3F k(0.0f, 0.0f, 0.0f);

  if ( elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK ))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");

    XMLElement* specular_elem =  elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK );

    //Fresnel
    XMLElement* sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::CTD_FRESNEL_MK );
    if( sub_spec_elem )
    {

      XMLElement* eta_element = sub_spec_elem->FirstChildElement( SceneLexer::ETA_MK);
      if( eta_element )
      {
        rgbColor_from_Markup( eta_element, eta);

      }

      XMLElement* k_element = sub_spec_elem->FirstChildElement( SceneLexer::K_MK);
      if( k_element )
      {
        rgbColor_from_Markup( k_element, k);
      }
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->warn(" MicroFacet GHS: No Fresnel Markup found. Assigning default one");
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::CTD_B_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got  b Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &b );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("MicroFacetGHS BRDF Model, B value retrieved ", b);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving B MicroFacetGHS  BRDF Model");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::CTD_P_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got  p Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &p );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("MicroFacetGHS BRDF Model, p value retrieved ", p);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving p MicroFacetGHS  BRDF Model");
        return false;
      }
    }
  }
  //---------------------------------------------------------------------------------------
  // End of Specular PART
  //---------------------------------------------------------------------------------------

  mrf::gui::fb::Loger::getInstance()->trace( " Eta parsed ", eta);
  mrf::gui::fb::Loger::getInstance()->trace( " k parsed ", k);
  mrf::gui::fb::Loger::getInstance()->trace( " b", b );
  mrf::gui::fb::Loger::getInstance()->trace( " p", p );



  //Diffraction PART
  float alpha   = 0.0f;
  float sigma_s = 0.0f;
  float c       = 0.0;

  XMLElement* diffrac_elem =  elem->FirstChildElement( SceneLexer::CTD_DIFFRACTION_MK );

  if ( diffrac_elem )
  {
    XMLElement* sub_spec_elem =  diffrac_elem->FirstChildElement( SceneLexer::ALPHA_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got  alpha Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &alpha );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("MicroFacetGHS BRDF Model, alpha value retrieved ", alpha);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving alpha MicroFacetGHS  BRDF Model");
        return false;
      }
    }

    sub_spec_elem =  diffrac_elem->FirstChildElement( SceneLexer::C_MK );
    if( sub_spec_elem )
    {
      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &c );
      if( valueOK != TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->warn(" WARNING C Attribute could not be retrieved. Assigning default value to it: ", c);
      }
    }

    sub_spec_elem =  diffrac_elem->FirstChildElement( SceneLexer::SIGMA_S_MK );
    if( sub_spec_elem )
    {
      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &sigma_s );
      if( valueOK != TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->warn(" WARNING Sigma_S Attribute could not be retrieved. Assigning default value to it: ", sigma_s);
      }
    }

  }//end of if Diffraction Markup detected



  //---------------------------------------------------------------------------------------
  // End of Diffraction PART
  //---------------------------------------------------------------------------------------
  mrf::gui::fb::Loger::getInstance()->trace( " Alpha: ", alpha );
  mrf::gui::fb::Loger::getInstance()->trace( " sigma_s parsed:", sigma_s );
  mrf::gui::fb::Loger::getInstance()->trace( " c parsed: ", c );

  int qint_enable_diffuse_lobe = 1;
  int value_OK =  elem->QueryIntAttribute( SceneLexer::ENABLE_DIFFUSE_LOBE_AT, &qint_enable_diffuse_lobe );
  bool const enable_diffuse_lobe = (qint_enable_diffuse_lobe == 1) ? true : false;
  mrf::gui::fb::Loger::getInstance()->trace(" Enable Diffuse Lobe:", enable_diffuse_lobe );

  int qint_enable_specular_lobe = 1;
  value_OK =  elem->QueryIntAttribute( SceneLexer::ENABLE_SPECULAR_LOBE_AT, &qint_enable_specular_lobe );
  bool const enable_specular_lobe = (qint_enable_specular_lobe == 1) ? true : false;
  mrf::gui::fb::Loger::getInstance()->trace(" Enable Specular Lobe:", enable_specular_lobe );

  int qint_enable_diffraction_lobe = 1;
  value_OK =  elem->QueryIntAttribute( SceneLexer::ENABLE_DIFFRACTION_LOBE_AT, &qint_enable_diffraction_lobe );
  bool const enable_diffraction_lobe = (qint_enable_diffraction_lobe == 1) ? true : false;
  mrf::gui::fb::Loger::getInstance()->trace(" Enable Diffraction Lobe:", enable_diffraction_lobe );


  //Finalizing
  if( variante_attr == "conductor" )
  {
    ConductorGHS* microghs_mat = new ConductorGHS( material_name,
        eta, k,
        alpha, b, p, sigma_s, c,
        enable_specular_lobe,
        enable_diffraction_lobe);
    _brdfMap[material_name] = scene.umatSize();
    scene.addMaterial( microghs_mat );
  }
  else if ( variante_attr == "plastic" )
  {
    //mrf::gui::fb::Loger::getInstance()->fatal(" The Variant of the Microfacet GHS model is not supported YET");

    PlasticGHS* microghs_mat = new PlasticGHS( material_name,
        eta, k,
        alpha, b, p, sigma_s, c,
        enable_specular_lobe,
        enable_diffraction_lobe,
        enable_diffuse_lobe );

    _brdfMap[material_name] = scene.umatSize();
    scene.addMaterial( microghs_mat );

  }
  else if ( variante_attr == "subsurface" )
  {

    SubsurfaceGHS* microghs_mat = new SubsurfaceGHS( material_name,
        eta, k,
        alpha, b, p, sigma_s, c,
        enable_specular_lobe,
        enable_diffraction_lobe,
        enable_diffuse_lobe );

    _brdfMap[material_name] = scene.umatSize();
    scene.addMaterial( microghs_mat );
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->fatal(" The Variant of the Microfacet GHS model is not supported ");
    return false;
  }

  return true;
}
*/


/*
bool MaterialSceneParser::addCTD(XMLElement *elem, mrf::rendering::Scene &scene, std::string const &material_name)
{
  Color3F diffuse_color;
  if ( elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK ) )
  {
    XMLElement* diffuse_elem = elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK );
    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse component detected ");

    if ( !retrieveRGBColor( diffuse_elem, diffuse_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the diffuse color ");
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Diffuse Color retrieved");
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(" No Diffuse component For this  material ");
  }


  Color3F specular_color(0.0f, 0.0f, 0.0f);
  Color3F fresnel_term(0.0f, 0.0f, 0.0f);
  float b = 0.0f;
  float p = 0.0f;
  if ( elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK ))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Specular component detected ");

    XMLElement* specular_elem =  elem->FirstChildElement( SceneLexer::SPECULAR_CMP_MK );

    if ( !retrieveRGBColor( specular_elem, specular_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the specular color ");
      return false;
    }

    XMLElement* sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::CTD_FRESNEL_MK );
    if ( !retrieveRGBColor( sub_spec_elem, fresnel_term))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the specular color ");
      return false;
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->trace("Fresnel Color retrieved");
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::CTD_B_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got  b Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &b );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("CTD BRDF Model, B value retrieved ", b);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving B CTD  BRDF Model");
        return false;
      }
    }

    sub_spec_elem =  specular_elem->FirstChildElement( SceneLexer::CTD_P_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got  p Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &p );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("CTD BRDF Model, p value retrieved ", p);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving p CTD  BRDF Model");
        return false;
      }
    }
  }//end of if Specular Markup detected


  Color3F diffraction_color(0.0f, 0.0f, 0.0f);
  float alpha = 0.0f;
  if ( elem->FirstChildElement( SceneLexer::CTD_DIFFRACTION_MK ))
  {
    XMLElement* diffrac_elem =  elem->FirstChildElement( SceneLexer::CTD_DIFFRACTION_MK );

    if ( !retrieveRGBColor( diffrac_elem, diffraction_color))
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" Problem while trying to retrieve the diffraction color ");
      return false;
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->trace("Diffraction Color retrieved");
    }

    XMLElement* sub_spec_elem =  diffrac_elem->FirstChildElement( SceneLexer::ALPHA_MK );
    if( sub_spec_elem )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Got  alpha Element");

      int valueOK = sub_spec_elem->QueryFloatAttribute( SceneLexer::VALUE_AT, &alpha );
      if ( valueOK == TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->trace("CTD BRDF Model, alpha value retrieved ", alpha);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" PROBLEM while retrieving alpha CTD  BRDF Model");
        return false;
      }
    }
  }//end of if Diffraction Markup detected


  // Now construct and add the material
  CTD* ctd_mat = new CTD( material_name,
                          diffuse_color,
                          specular_color,
                          diffraction_color,
                          fresnel_term,
                          alpha,
                          b,
                          p );


  _brdfMap[material_name] = scene.umatSize();
  scene.addMaterial( ctd_mat );

  return true;
}
*/


/*
bool MaterialSceneParser::addHazyGloss(XMLElement *elem, mrf::rendering::Scene &scene, std::string const &material_name)
{
  mrf::gui::fb::Loger::getInstance()->trace("PARSING A Hazy Gloss ELEMENT ");


  int valueOK = 0;

  float hazyness = 0.5f;
  valueOK = elem->QueryFloatAttribute(SceneLexer::HAZINESS_AT, &hazyness);

  if ( valueOK == TIXML_SUCCESS )
  {
    mrf::gui::fb::Loger::getInstance()->trace("Hazy Gloss MOdel  haziness value retrieved ", hazyness);
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->warn(" PROBLEM while retrieving haziness value for Hazy Gloss");
  }


  float smooth = 0.5f;
  valueOK = elem->QueryFloatAttribute(SceneLexer::SMOOTH_AT, &smooth);

  if ( valueOK == TIXML_SUCCESS )
  {
    mrf::gui::fb::Loger::getInstance()->trace("Hazy Gloss MOdel  smooth value retrieved ", smooth);
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->warn(" PROBLEM while retrieving smooth value for Hazy Gloss");
  }


  string const distribution_type = elem->Attribute(SceneLexer::DISTRIB_AT);

  mrf::gui::fb::Loger::getInstance()->trace(" Distribution type: ", distribution_type);


  //TODO : FOR THE FRESNEL
  // string const fresnel_type      =
  if( ! elem->Attribute(SceneLexer::FRESNEL_AT) )
  {
    mrf::gui::fb::Loger::getInstance()->trace("  No Fresnel type Attribute Set " );
  }
  //loger->trace(" Fresnel type: ", fresnel_type);

  // Fresnel definition: Air by default
  Color3F reflectivity(1.0f, 1.0f, 1.0f);
  Color3F edgetint(0.0f, 0.0f, 0.0f);

  //Color Part
  XMLElement* refl_element = elem->FirstChildElement( SceneLexer::REFL_MK);
  if( refl_element )
  {
    rgbColor_from_Markup( refl_element, reflectivity);

  }

  XMLElement* edgetint_element = elem->FirstChildElement( SceneLexer::EDGETINT_MK);
  if( edgetint_element )
  {
    rgbColor_from_Markup( edgetint_element, edgetint);
  }

  mrf::gui::fb::Loger::getInstance()->trace(" reflectivity = ", reflectivity);
  mrf::gui::fb::Loger::getInstance()->trace(" edgetint = ", edgetint);



  //DIffuse Part ?
  RADIANCE_TYPE diffuse_term;
  XMLElement* diffuse_element = elem->FirstChildElement( SceneLexer::DIFFUSE_CMP_MK);
  float albedo = 0.0f;
  if( diffuse_element )
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Diffuse Markup found. Parsing it ");
    rgbColor_from_Markup( diffuse_element, diffuse_term);

    if( diffuse_element->Attribute(SceneLexer::ALBEDO_AT) )
    {
      mrf::gui::fb::Loger::getInstance()->trace(" ALbedo found while parsing ");

      int valueOK = diffuse_element->QueryFloatAttribute(SceneLexer::ALBEDO_AT, &albedo);
      if( valueOK != TIXML_SUCCESS )
      {
        mrf::gui::fb::Loger::getInstance()->warn("Problem while parsing albedo ");
      }
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->trace(" no albedo found");
    }
  }
  mrf::gui::fb::Loger::getInstance()->trace(" Diffuse color for Hazy Gloss set to: ", diffuse_term);
  mrf::gui::fb::Loger::getInstance()->trace(" Diffuse ALbedo set to : ", albedo );


  if( distribution_type == "ggx"  or distribution_type == "walter_bsdf")
  {
    float roughness = 0.2f;
    int valueOK = elem->QueryFloatAttribute(SceneLexer::ROUGHNESS_AT, &roughness);

    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Hazy Gloss MOdel  Roughness value retrieved ", roughness);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->warn(" PROBLEM while retrieving Roughness value for Hazy Gloss");
      return false;
    }

    float haze_extent = 0.5f;
    valueOK = elem->QueryFloatAttribute(SceneLexer::HAZE_EXTENT_AT, &haze_extent);

    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Hazy Gloss MOdel  haze_extent value retrieved ", haze_extent);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->warn(" PROBLEM while retrieving haze_extent value for Hazy Gloss");
    }


    mrf::gui::fb::Loger::getInstance()->trace("ADDING A Hazy Gloss BRDF WITH GGX Disbribution ");


    mrf::gui::fb::Loger::getInstance()->trace(" THERE ");

    if( distribution_type == "ggx" )
    {
      mrf::gui::fb::Loger::getInstance()->trace(" Constructing an HAZY GGX ");

      _HazyGloss<GGX,FresnelConductor>* a_hazy_gloss_mat =
        new _HazyGloss<GGX,FresnelConductor>( material_name,
                                              reflectivity,
                                              edgetint,
                                              roughness,
                                              hazyness,
                                              haze_extent,
                                              smooth,
                                              diffuse_term,
                                              albedo );

      _brdfMap[material_name] = scene.umatSize();
      scene.addMaterial( a_hazy_gloss_mat );
      mrf::gui::fb::Loger::getInstance()->trace("ADDED one Hazy GLoss Material with GGX Distribution ");
    }
    else
    {
      _HazyGloss<_WalterBSDF<GGX>,FresnelConductor>* a_hazy_gloss_mat =
        new _HazyGloss<_WalterBSDF<GGX>,FresnelConductor>( material_name,
            reflectivity,
            edgetint,
            roughness,
            hazyness,
            haze_extent,
            smooth,
            diffuse_term,
            albedo );

      _brdfMap[material_name] = scene.umatSize();
      scene.addMaterial( a_hazy_gloss_mat );
      mrf::gui::fb::Loger::getInstance()->trace("ADDED one Hazy GLoss Material with GGX Distribution ");
    }

    return true;

  }
  else if ( distribution_type == "anisoGGX" )
  {
    float haze_extent_x = 2.0f;
    valueOK = elem->QueryFloatAttribute(SceneLexer::HAZE_EXTENT_X_AT, &haze_extent_x);

    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Hazy Gloss MOdel  haze_extent X value retrieved ", haze_extent_x);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->warn(" PROBLEM while retrieving haze_extent X  value for Hazy Gloss");
      return false;
    }

    float haze_extent_y = 2.0f;
    valueOK = elem->QueryFloatAttribute(SceneLexer::HAZE_EXTENT_Y_AT, &haze_extent_y);

    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Hazy Gloss MOdel  haze_extent Y value retrieved ", haze_extent_y);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->warn(" PROBLEM while retrieving haze_extent Y  value for Hazy Gloss");
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->trace(" Haze extent X retrieved ", haze_extent_x);
    mrf::gui::fb::Loger::getInstance()->trace(" Haze extent Y retrieved ", haze_extent_y);


    float roughness_x = 0.2f;
    valueOK = elem->QueryFloatAttribute(SceneLexer::ROUGHNESS_X_AT, &roughness_x);

    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Hazy Gloss MOdel  Roughness X value retrieved ", roughness_x);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->warn(" PROBLEM while retrieving Roughness X value for Hazy Gloss");
      return false;
    }

    float roughness_y = 0.2f;
    valueOK = elem->QueryFloatAttribute(SceneLexer::ROUGHNESS_Y_AT, &roughness_y);

    if ( valueOK == TIXML_SUCCESS )
    {
      mrf::gui::fb::Loger::getInstance()->trace("Hazy Gloss MOdel  Roughness Y value retrieved ", roughness_y);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->warn(" PROBLEM while retrieving Roughness Y value for Hazy Gloss");
      return false;
    }

    mrf::gui::fb::Loger::getInstance()->trace("ADDING A Hazy Gloss BRDF WITH  ANISOTROPIC ggx Disbribution ");

    _HazyGloss<AnisoGGX,FresnelConductor>* a_hazy_gloss_mat =
      new _HazyGloss<AnisoGGX,FresnelConductor>( material_name,
          reflectivity,
          edgetint,
          roughness_x, roughness_y,
          hazyness,
          haze_extent_x, haze_extent_y,
          smooth,
          diffuse_term,
          albedo );

    _brdfMap[material_name] = scene.umatSize();
    scene.addMaterial( a_hazy_gloss_mat );
    mrf::gui::fb::Loger::getInstance()->trace("ADDED one Hazy GLoss Material with GGX Distribution ");
    return true;

  }
  else if ( distribution_type == "ward" )
  {
    mrf::gui::fb::Loger::getInstance()->trace("NOT YET IMPLEMENTED ");
    return false;

  }
  else if ( distribution_type == "beckman" )
  {
    mrf::gui::fb::Loger::getInstance()->trace("NOT YET IMPLEMENTED ");
    return false;

  }
  else if ( distribution_type == "walter_phong" )
  {
    mrf::gui::fb::Loger::getInstance()->trace("NOT YET IMPLEMENTED ");
    return false;
  }

  return true;
}
*/

mrf::materials::UMat *
MaterialSceneParser::addEmittance(tinyxml2::XMLElement *an_emitt_elem, mrf::rendering::Scene &scene)
{
  if (!an_emitt_elem->Attribute(SceneLexer::NAME_AT))
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_LIGHT_MATERIAL_NAME);
    return nullptr;
  }
  std::string emittance_name(an_emitt_elem->Attribute(SceneLexer::NAME_AT));

  mrf::gui::fb::Loger::getInstance()->trace("emittance_name", emittance_name);

  std::string emittance_type;
  retrieveEmittanceType(an_emitt_elem, emittance_type);

  mrf::gui::fb::Loger::getInstance()->trace("emittance_type ", emittance_type);

  for (auto registered_mat : _id_map)
  {
    if (emittance_type.compare(registered_mat) == 0)
    {
      mrf::gui::fb::Loger::getInstance()->trace(std::string(registered_mat) + " Detected");
      _brdf_map[emittance_name]
          = scene.addMaterial(_parse_map[std::string(registered_mat)]->parse(an_emitt_elem, emittance_name, _parser));
      return scene.getAllMaterials()[_brdf_map[emittance_name]];
    }
  }

  return nullptr;
}


bool MaterialSceneParser::retrieveEmittanceType(tinyxml2::XMLElement *elem, std::string &emittance_type)
{
  if (!elem->Attribute(SceneLexer::TYPE_AT))
  {
    mrf::gui::fb::Loger::getInstance()->fatal(" Emittance HAS NOT TYPE : Assigning default one (Diffuse)");
    return false;
  }

  emittance_type = elem->Attribute(SceneLexer::TYPE_AT);
  return true;
}

MaterialSceneParser::~MaterialSceneParser() {}

bool MaterialSceneParser::retrieveMaterialIndex(tinyxml2::XMLElement *elem, unsigned int &material_index) const
{
  if (!elem->Attribute(SceneLexer::REF_MATERIAL_AT))
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_MATERIALS);
    return false;
  }

  std::string ref_material = elem->Attribute(SceneLexer::REF_MATERIAL_AT);
  mrf::gui::fb::Loger::getInstance()->trace("REFERENCE MATERIAL ");
  mrf::gui::fb::Loger::getInstance()->trace(ref_material);

  //NOW Check if the material exists
  auto current = _brdf_map.find(ref_material);
  auto end     = _brdf_map.end();
  if (current == end)   //  THIS Key does not exists !
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::UNDEFINED_MATERIAL);
    return false;
  }
  //This returns a TYPE& and not a TYPE const & brdfMap[ref_material];
  material_index = current->second;
  return true;
}

}   // namespace io
}   // namespace mrf
