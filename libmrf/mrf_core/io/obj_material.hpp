/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/vec3.hpp>
#include <mrf_core/color/color.hpp>
#include <mrf_core/materials/materials.hpp>
#include <mrf_core/radiometry/spectrum.hpp>

#include <vector>
#include <string>
#include <map>
#include <iostream>

// namespace mrf
// {
//     namespace io
//     {

class MRF_CORE_EXPORT OBJMaterial
{
private:
  //mrf::rendering::Color3F ka;
  //kd, ks, tf, Ns, Ni;

  std::string _name;

  mrf::materials::COLOR _ka;   // Ambiant Component (can be used to construct a light)
  mrf::materials::COLOR _kd;   // Diffuse Component
  mrf::materials::COLOR _ks;   // Specular Component
  mrf::materials::COLOR _tf;   // Transmission Filter (change the color of the light )
  mrf::materials::COLOR _ns;   // Specular Exponent
  mrf::materials::COLOR _ni;   // Index of Refraction


  std::string _map_ka;     // Texture filename for the ambiant  ka term
  std::string _map_kd;     // Texture filename for the diffuse  kd term
  std::string _map_ks;     // Texture filename for the specular ks term
  std::string _map_ns;     // Texture filename for the shininess exponent term
  std::string _bumpMap;    // The filename of the bump map texture.
  std::string _map_d;      // The filename of the alpha map that spatially modulates the dissolve coefficient
  std::string _map_refl;   // The filename of the reflection texture

  float _dissolve;   // A scalar value in [0,1] that represents the transparency (dissolve component)
  // 1.0 is opaque. 0.0 is fully transparent. Default is 1.0

  unsigned int _illModel;   // A value between [0,10]
                            // 0    Color on and Ambient off
                            // 1    Color on and Ambient on
                            // 2    Highlight on
                            // 3    Reflection on and Ray trace on
                            // 4    Transparency: Glass on
                            //      Reflection: Ray trace on
                            // 5    Reflection: Fresnel on and Ray trace on
                            // 6    Transparency: Refraction on
                            //      Reflection: Fresnel off and Ray trace on
                            // 7    Transparency: Refraction on
                            //      Reflection: Fresnel on and Ray trace on
                            // 8    Reflection on and Ray trace off
                            // 9    Transparency: Glass on
                            //      Reflection: Ray trace off
                            // 10   Casts shadows onto invisible surfaces


public:
  inline OBJMaterial();
  inline OBJMaterial(std::string const &material_name);
  ~OBJMaterial() {};
  inline OBJMaterial(OBJMaterial const &);
  inline OBJMaterial &operator=(OBJMaterial const &);

  inline void setMaterialName(std::string const &name);

  inline void setKa(float r, float g, float b);
  inline void setKd(float r, float g, float b);
  inline void setKs(float r, float g, float b);
  inline void setTf(float r, float g, float b);

  inline void setNs(float exponent);
  inline void setNs(float exponent_red, float exponent_green, float exponent_blue);

  inline void setNi(float optical_density);
  inline void setNi(float optical_density_red, float optical_density_green, float optical_density_blue);


  inline void setMapKa(std::string const &map_ka);
  inline void setMapKd(std::string const &map_kd);
  inline void setMapKs(std::string const &map_ks);
  inline void setMapNs(std::string const &map_ns);
  inline void setBumpmap(std::string const &bump_map);
  inline void setMapD(std::string const &map_d);
  inline void setReflectionMap(std::string const &map_refl);


  inline void setDissolveCoeff(float a_coef);
  inline void setIllModel(unsigned int model_type);


  //-----------------------------------------------------
  inline std::string const &name() const;

  inline mrf::materials::COLOR const &ka() const;
  inline mrf::materials::COLOR const &kd() const;
  inline mrf::materials::COLOR const &ks() const;
  inline mrf::materials::COLOR const &tf() const;
  inline mrf::materials::COLOR const &ns() const;
  inline mrf::materials::COLOR const &ni() const;


  inline std::string const &mapKa() const;
  inline std::string const &mapKd() const;
  inline std::string const &mapKs() const;
  inline std::string const &mapNs() const;
  inline std::string const &bumpmap() const;
  inline std::string const &mapD() const;
  inline std::string const &mapRefl() const;


  inline bool hasKaTexture() const;
  inline bool hasKdTexture() const;
  inline bool hasKsTexture() const;
  inline bool hasNsTexture() const;
  inline bool hasBumpmapTexture() const;
  inline bool hasAlphaTexture() const;
  inline bool hasReflectionMap() const;
  inline bool hasTextures() const;



  inline float        dissolveCoeff() const;
  inline unsigned int illModel() const;
};


inline OBJMaterial::OBJMaterial(): _dissolve(1.0) {}


inline OBJMaterial::OBJMaterial(std::string const &material_name): _name(material_name) {}

inline OBJMaterial::OBJMaterial(OBJMaterial const &a_mat)
  : _name(a_mat._name)
  , _ka(a_mat._ka)
  , _kd(a_mat._kd)
  , _ks(a_mat._ks)
  , _tf(a_mat._tf)
  , _ns(a_mat._ns)
  , _ni(a_mat._ni)
  , _map_ka(a_mat._map_ka)
  , _map_kd(a_mat._map_kd)
  , _map_ks(a_mat._map_ks)
  , _map_ns(a_mat._map_ns)
  , _bumpMap(a_mat._bumpMap)
  , _map_d(a_mat._map_d)
  , _map_refl(a_mat._map_refl)
  , _dissolve(a_mat._dissolve)
  , _illModel(a_mat._illModel)
{}


inline OBJMaterial &OBJMaterial::operator=(OBJMaterial const &a_mat)
{
  if ((void *)this == (void *)&a_mat)   //assignment to itself?
  {
    return *this;
  }

  _name = a_mat._name;
  _ka   = a_mat._ka;
  _kd   = a_mat._kd;
  _ks   = a_mat._ks;
  _tf   = a_mat._tf;
  _ns   = a_mat._ns;
  _ni   = a_mat._ni;

  _map_ka = a_mat._map_ka;
  _map_kd = a_mat._map_kd;
  _map_ks = a_mat._map_ks;
  _map_ns = a_mat._map_ns;

  return *this;
}




// Set the ambient reflectivity
inline void OBJMaterial::setKa(float r, float g, float b)
{
  _ka = mrf::materials::COLOR(r, g, b);
}

// Set the diffuse reflectivity
inline void OBJMaterial::setKd(float r, float g, float b)
{
  _kd = mrf::materials::COLOR(r, g, b);
}

// Set the specular reflectivity.
inline void OBJMaterial::setKs(float r, float g, float b)
{
  _ks = mrf::materials::COLOR(r, g, b);
}

// Set the transmission filter using RGB values.
inline void OBJMaterial::setTf(float r, float g, float b)
{
  _tf = mrf::materials::COLOR(r, g, b);
}

// Set the specular exponent for the current material.
// This defines the focus of the specular highlight (shininess).
inline void OBJMaterial::setNs(float exponent)
{
  _ns = mrf::materials::COLOR(exponent, exponent, exponent);
}

inline void OBJMaterial::setNs(float exponent_red, float exponent_green, float exponent_blue)
{
  _ns = mrf::materials::COLOR(exponent_red, exponent_green, exponent_blue);
}

// Set the optical density for the surface.
// This is also known as index of refraction.
inline void OBJMaterial::setNi(float optical_density)
{
  _ni = mrf::materials::COLOR(optical_density, optical_density, optical_density);
}

inline void OBJMaterial::setNi(float optical_density_red, float optical_density_green, float optical_density_blue)
{
  _ni = mrf::materials::COLOR(optical_density_red, optical_density_green, optical_density_blue);
}


inline void OBJMaterial::setMapKa(std::string const &map_ka)
{
  _map_ka = map_ka;
}

inline void OBJMaterial::setMapKd(std::string const &map_kd)
{
  _map_kd = map_kd;
}

inline void OBJMaterial::setMapKs(std::string const &map_ks)
{
  _map_ks = map_ks;
}

inline void OBJMaterial::setMapNs(std::string const &map_ns)
{
  _map_ns = map_ns;
}

inline void OBJMaterial::setBumpmap(std::string const &bump_map)
{
  _bumpMap = bump_map;
}

inline void OBJMaterial::setMapD(std::string const &map_d)
{
  _map_d = map_d;
}

inline void OBJMaterial::setReflectionMap(std::string const &map_refl)
{
  _map_refl = map_refl;
}


inline void OBJMaterial::setDissolveCoeff(float a_coef)
{
  _dissolve = a_coef;
}

inline void OBJMaterial::setIllModel(unsigned int model_type)
{
  _illModel = model_type;
}


inline std::string const &OBJMaterial::name() const
{
  return _name;
}

inline void OBJMaterial::setMaterialName(std::string const &name)
{
  _name = name;
}

inline mrf::materials::COLOR const &OBJMaterial::ka() const
{
  return _ka;
}

inline mrf::materials::COLOR const &OBJMaterial::kd() const
{
  return _kd;
}

inline mrf::materials::COLOR const &OBJMaterial::ks() const
{
  return _ks;
}

inline mrf::materials::COLOR const &OBJMaterial::tf() const
{
  return _tf;
}

inline mrf::materials::COLOR const &OBJMaterial::ns() const
{
  return _ns;
}

inline mrf::materials::COLOR const &OBJMaterial::ni() const
{
  return _ni;
}

inline std::string const &OBJMaterial::mapKa() const
{
  return _map_ka;
}

inline std::string const &OBJMaterial::mapKd() const
{
  return _map_kd;
}

inline std::string const &OBJMaterial::mapKs() const
{
  return _map_ks;
}

inline std::string const &OBJMaterial::mapNs() const
{
  return _map_ns;
}

inline std::string const &OBJMaterial::bumpmap() const
{
  return _bumpMap;
}

inline std::string const &OBJMaterial::mapD() const
{
  return _map_d;
}

inline std::string const &OBJMaterial::mapRefl() const
{
  return _map_refl;
}


inline float OBJMaterial::dissolveCoeff() const
{
  return _dissolve;
}

inline unsigned int OBJMaterial::illModel() const
{
  return _illModel;
}


inline bool OBJMaterial::hasKaTexture() const
{
  return !_map_ka.empty();
}

inline bool OBJMaterial::hasKdTexture() const
{
  return !_map_kd.empty();
}

inline bool OBJMaterial::hasKsTexture() const
{
  return !_map_ks.empty();
}

inline bool OBJMaterial::hasNsTexture() const
{
  return !_map_ns.empty();
}

inline bool OBJMaterial::hasBumpmapTexture() const
{
  return !_bumpMap.empty();
}

inline bool OBJMaterial::hasAlphaTexture() const
{
  return !_map_d.empty();
}

inline bool OBJMaterial::hasReflectionMap() const
{
  return !_map_refl.empty();
}

inline bool OBJMaterial::hasTextures() const
{
  return hasReflectionMap() || hasAlphaTexture() || hasBumpmapTexture() || hasNsTexture() || hasKsTexture()
         || hasKdTexture() || hasKaTexture();
}


inline std::ostream &operator<<(std::ostream &os, OBJMaterial const &the_material)
{
  os << " [ Material " << the_material.name() << " dissolve factor d: " << the_material.dissolveCoeff()
     << " Illumination Model " << the_material.illModel() << std::endl
     << "   Tf: " << the_material.tf() << std::endl
     << "   Ka: " << the_material.ka() << std::endl
     << "   Kd: " << the_material.kd() << std::endl
     << "   Ks: " << the_material.ks() << std::endl
     << "   Ns: " << the_material.ns() << std::endl
     << "   Ni: " << the_material.ni() << std::endl;


  if (the_material.hasKaTexture())
  {
    os << "   Map_Ka: " << the_material.mapKa() << std::endl;
  }
  if (the_material.hasKdTexture())
  {
    os << "   Map_Kd: " << the_material.mapKd() << std::endl;
  }
  if (the_material.hasKsTexture())
  {
    os << "   Map_Ks: " << the_material.mapKs() << std::endl;
  }
  if (the_material.hasNsTexture())
  {
    os << "   Map_Ns: " << the_material.mapNs() << std::endl;
  }

  if (the_material.hasBumpmapTexture())
  {
    os << "   Bumpmap: " << the_material.bumpmap() << std::endl;
  }

  if (the_material.hasAlphaTexture())
  {
    os << "   Alpha Texture (map_d):" << the_material.mapD() << std::endl;
  }

  if (the_material.hasReflectionMap())
  {
    os << "   Alpha Texture (map_d):" << the_material.mapRefl() << std::endl;
  }

  os << " ] " << std::endl;
  return os;
}



//   } // end of io namespace

//} // end of mrf namespace
