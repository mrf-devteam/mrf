/**
 *
 * Copyright(C) CNRS. 2015
 * Author: Romain Pacnowski: romain.pacanowski@institutoptique.fr
 *
 */

#include <mrf_core/io/parser.hpp>
#include <mrf_core/rendering/thin_lens_camera.hpp>
#include <mrf_core/rendering/pinhole_camera.hpp>
#include <mrf_core/rendering/orthographic_camera.hpp>
#include <mrf_core/rendering/environment_camera.hpp>
#include <mrf_core/io/camera_parser.hpp>
#include <mrf_core/io/camera_lexer.hpp>

#include <mrf_core/rendering/sensor.hpp>
#include <mrf_core/rendering/sensor_size.hpp>
#include <mrf_core/util/precision_timer.hpp>

#include <exception>
#include <string>

using tinyxml2::XML_SUCCESS;
using tinyxml2::XMLElement;
using tinyxml2::XMLNode;

namespace mrf
{
namespace io
{
//------------------------------------------------------------------------------------------
//public STATIC
//------------------------------------------------------------------------------------------
void CameraParser::loadCameras(std::string const &filename, std::map<std::string, mrf::rendering::Camera *> &cameras)
{
  using namespace mrf::util;

  CameraParser &cam_parser = CameraParser::Instance();

  try
  {
    PrecisionTimer timer;

    timer.start();
    cam_parser.parseFile(filename, cameras);
    timer.stop();

    std::string message(" parsing took " + std::to_string(timer.elapsed()) + " second ");
    mrf::gui::fb::Loger::getInstance()->info(message);

    mrf::gui::fb::Loger::getInstance()->info(" Number of cameras parsed: ", cameras.size());
  }
  catch (std::exception const &e)
  {
    //Propagate exception
    mrf::gui::fb::Loger::getInstance()->fatal(" Parsing of the camera file " + filename + " FAILED. ");
    mrf::gui::fb::Loger::getInstance()->fatal(e.what());


    throw "ERROR WHILE PARSING CAMERA";
  }

  if (cameras.size() == 0)
  {
    throw "NO CAMERA PARSED";
  }
}



//------------------------------------------------------------------------------------------
//public
//------------------------------------------------------------------------------------------
void CameraParser::parseFile(std::string const &filename, std::map<std::string, mrf::rendering::Camera *> &cameras)
{
  using namespace std;
  using namespace mrf::rendering;
  using namespace mrf::math;

  std::string ext;
  mrf::util::StringParsing::getFileExtension(filename.c_str(), ext);
  if (!(ext == "mcf"))
  {
    throw std::runtime_error("The provided camera file is not a MCF file, aborting.");
  }
  else
  {
    std::ifstream f(filename.c_str());
    if (!f.good())
    {
      throw std::runtime_error("Could not open the camera file, please check that it exits.");
    }
  }

  // FIRST TRY TO LOAD THE XML FILE
  mrf::gui::fb::Loger::getInstance()->trace("Starting to parse the camera file ");

  tinyxml2::XMLDocument tinyxml_doc;

  if (tinyxml_doc.LoadFile(filename.c_str()) != XML_SUCCESS)
  {
    mrf::gui::fb::Loger::getInstance()->warn(ParsingErrors::XML_LOAD_FILE);
    mrf::gui::fb::Loger::getInstance()->trace("At line", tinyxml_doc.ErrorLineNum());
    mrf::gui::fb::Loger::getInstance()->trace("Error = ", tinyxml_doc.ErrorStr());
    mrf::gui::fb::Loger::getInstance()->warn(" CAMERA FILE COULD NOT BE LOADED ");

    throw std::logic_error(std::string(" Failed to Load XML File. Check your Syntax!"));
  }

  tinyxml2::XMLHandle tinyxml_docHandle(&tinyxml_doc);

  //AT THIS POINT THE XML File IS Loaded and can be parsed.


  XMLElement *all_cams_element = tinyxml_docHandle.FirstChildElement(CameraLexer::CAMERAS_MK).ToElement();

  XMLElement *camera_element = nullptr;

  //Handle the case where <cameras> is not there. Then only one camera is going to be parsed
  if (!all_cams_element)
  {
    mrf::gui::fb::Loger::getInstance()->warn("The <cameras> markup is missing. Trying to parse all cameras...");
    camera_element = tinyxml_docHandle.FirstChildElement(CameraLexer::SINGLE_CAM_MK).ToElement();
  }
  else
  {
    if (all_cams_element->FirstChildElement(CameraLexer::SINGLE_CAM_MK))
      camera_element = all_cams_element->FirstChildElement(CameraLexer::SINGLE_CAM_MK)->ToElement();
    else
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Can't find anny camera in the camera file !");
      throw std::logic_error(std::string("Failed to Load the camera file"));
    }
  }

  mrf::gui::fb::Loger::getInstance()->trace(" STARTING TO PARSE XML Camera MARKUPS ");

  // Clear the Hash-table
  cameras.clear();


  for (; camera_element != 0; camera_element = camera_element->NextSiblingElement())
  {
    char const *type_of_camera = camera_element->Attribute(CameraLexer::TYPE_AT);
    if (!type_of_camera)
    {
      mrf::gui::fb::Loger::getInstance()->warn(" No type attribute ignoring this camera ");
      continue;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Type of camera parsed is: ", type_of_camera);

    //auto camera_type = _string2CameraType_Map.find(string(type_of_camera));
    CameraType type = string2CameraType(string(type_of_camera));

    if (type == CameraType::Size)
    {
      mrf::gui::fb::Loger::getInstance()->warn(" Unknown type attribute ignoring this camera ");
      continue;
    }

    //----------------------------------------------------------------------------------
    // ID of the camera which needs to be put in the Hash-table = std::map
    char const *id_camera = camera_element->Attribute(CameraLexer::ID_AT);
    if (!id_camera)
    {
      mrf::gui::fb::Loger::getInstance()->warn("No id for this camera. Ignoring it");
      continue;
    }

    std::string id_camera_str(id_camera);
    auto        cam_ids_iterator = cameras.find(id_camera_str);
    if (cam_ids_iterator != cameras.end())
    {
      mrf::gui::fb::Loger::getInstance()->warn("A camera with the following ID exists already ", id_camera_str);
      mrf::gui::fb::Loger::getInstance()->warn(" Ignoring this camera ");
      continue;
    }
    //----------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------
    // Parsing the content of the <camera> markup
    //----------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------
    mrf::math::Vec3f position;

    try
    {
      position = Parser::vec3_from_markup<mrf::math::Vec3f>(*camera_element, CameraLexer::POSITION_MK);
    }
    catch (std::exception const &e)
    {
      mrf::gui::fb::Loger::getInstance()->warn("A Position Element could not be retrieved. Ignoring this camera");
      mrf::gui::fb::Loger::getInstance()->warn(e.what());
      continue;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Camera Position retrieved is ", position);

    //----------------------------------------------------------------------------------
    // Look-At By default
    mrf::math::Vec3f look_at(0.0f, 0.0f, 0.0f);

    try
    {
      look_at = Parser::vec3_from_markup<mrf::math::Vec3f>(*camera_element, CameraLexer::CAM_LOOKAT_MK);
    }
    catch (std::exception const &e)
    {
      mrf::gui::fb::Loger::getInstance()->warn("A LookAt Element could not be retrieved. Using default one");
      mrf::gui::fb::Loger::getInstance()->warn(e.what());
    }
    mrf::gui::fb::Loger::getInstance()->trace("Camera look_at is set to ", look_at);

    //----------------------------------------------------------------------------------
    //Up Vector By default
    mrf::math::Vec3f up(0.0f, 1.0f, 0.0f);
    try
    {
      up = Parser::vec3_from_markup<mrf::math::Vec3f>(*camera_element, CameraLexer::CAM_UP_MK);
    }
    catch (std::exception const &e)
    {
      mrf::gui::fb::Loger::getInstance()->warn("A  Up Element could not be retrieved. Using default one");
      mrf::gui::fb::Loger::getInstance()->warn(e.what());

      mrf::math::Vec3f previous_up = up;
      mrf::math::Vec3f view        = (look_at - position).normalize();
      mrf::math::Vec3f right       = view.cross(previous_up).normalize();

      mrf::math::Vec3f new_up = right.cross(view);
      new_up.normalize();

      up = new_up;
    }
    mrf::gui::fb::Loger::getInstance()->trace("Camera up is set to ", up);

    //----------------------------------------------------------------------------------
    //  Sensor Resolution (i.e., pixels of the image). If not present 512x512 is going
    //  to be used
    //
    //  Samples per Pixel = SPP = 1 by default
    //----------------------------------------------------------------------------------
    mrf::math::Vec2u resolution(512, 512);
    unsigned int     spp             = 1;
    double           sensor_distance = 0.015;

    XMLNode *   sensor_node    = camera_element->FirstChildElement(CameraLexer::SENSOR_MK);
    XMLElement *sensor_element = nullptr;
    if (sensor_node)
    {
      sensor_element = sensor_node->ToElement();
      try
      {
        resolution = Parser::vec2_for_width_and_height<unsigned int, mrf::math::Vec2>(
            *sensor_element,
            CameraLexer::RESOLUTION_MK);
      }
      catch (...)
      {
        mrf::gui::fb::Loger::getInstance()->warn(" No <resolution> markup. Assigning default values. ");
      }

      // Samples per pixel
      try
      {
        spp = Parser::scalar_from_attribute_name<unsigned int>(
            *camera_element,
            CameraLexer::SENSOR_MK,
            CameraLexer::SAMPLES_PER_PIXEL_AT);
      }
      catch (...)
      {
        mrf::gui::fb::Loger::getInstance()->warn(
            " Error happened while paring the spp attribute Assigning default value");
      }

      try
      {
        sensor_distance = Parser::scalar_from_value<double>(*sensor_element, CameraLexer::SENSOR_DISTANCE_MK);
      }
      catch (...)
      {
        mrf::gui::fb::Loger::getInstance()->warn(
            " Could not retrieve sensor distance (view plane distance). Assigning default one.");
      }
      mrf::gui::fb::Loger::getInstance()->trace(" VPD. Sensor distance is set to: ", sensor_distance);


      // int int_spp;
      // int sppOK = sensor_element->QueryIntAttribute( CameraLexer::SAMPLES_PER_PIXEL_AT,  &int_spp);

      // if( sppOK != TIXML_SUCCESS )
      // {
      //   mrf::gui::fb::Loger::getInstance()->warn(" Error happened while paring the spp attribute Assigning default value");
      //   spp = 1;
      // }
      // else
      // {
      //   spp = static_cast<unsigned int>( int_spp );
      // }
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->warn(" No <sensor> markup. Assigning default values. ");
    }

    //mrf::gui::fb::Loger::getInstance()->trace(" Sensor Resolution (i.e. Image size) is set  to ", resolution);
    mrf::gui::fb::Loger::getInstance()->trace(" Samples per Pixel set To ", spp);
    //----------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------
    //  Instancier le bon type de Camera / ThinLensCam en fontion de l'attribut type
    //----------------------------------------------------------------------------------
    switch (type)
    {
      case CameraType::Pinhole:
      {
        // We ignore the size of the sensor because it is controled by FOVY
        // And View plane distance for now
        // Another strategy would be to read the sensor size and the fovy
        // and deduce the view plane distance (i.e. the distance between the pinhole and the sensor)


        //Fovy NOW
        double fovy = 60.0;
        try
        {
          fovy = Parser::scalar_from_value<double>(*camera_element, CameraLexer::CAM_FOVY_MK);
        }
        catch (...)
        {
          mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve fovy value. Assigning default one");
        }


        mrf::gui::fb::Loger::getInstance()->trace(" Fovy set to : ", fovy);

        // Now Creating the Classical Pinhole Camera
        Camera *a_new_pinhole_cam = new Pinhole(
            position,
            look_at,
            up,
            static_cast<float>(fovy),
            static_cast<float>(sensor_distance),
            3000.0f,
            resolution.x(),
            resolution.y(),
            spp,
            id_camera_str);
        //Assign ViewPlane distance
        //a_new_pinhole_cam->viewPlaneDistance( sensor_distance );

        cameras[id_camera_str] = a_new_pinhole_cam;
        break;
      }
      case CameraType::ThinLens:
      {
        //----------------------------------------------------------------------------
        // Vertical FOV.
        //----------------------------------------------------------------------------
        double fovy = 60.0;
        try
        {
          fovy = Parser::scalar_from_value<double>(*camera_element, CameraLexer::CAM_FOVY_MK);
        }
        catch (...)
        {
          mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve fovy value. Assigning default one");
        }

        mrf::gui::fb::Loger::getInstance()->trace(" Fovy set to : ", fovy);

        //----------------------------------------------------------------------------
        // Focus distance.
        //----------------------------------------------------------------------------
        double focus_distance = 100.0;
        try
        {
          focus_distance = Parser::scalar_from_value<double>(*camera_element, CameraLexer::FOCUS_DISTANCE_MK);
        }
        catch (...)
        {
          mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve focus distance. Assigning a default value ");
        }
        mrf::gui::fb::Loger::getInstance()->trace(" Focus Distance set to ", focus_distance);

        //----------------------------------------------------------------------------
        // Aperture.
        //----------------------------------------------------------------------------
        double aperture = 1.0;
        try
        {
          aperture = Parser::scalar_from_value<double>(*camera_element, CameraLexer::CAM_APERTURE_MK);
        }
        catch (...)
        {
          mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve aperture. Assigning a default value ");
        }
        mrf::gui::fb::Loger::getInstance()->trace(" Aperture set to ", aperture);

        //----------------------------------------------------------------------------
        // Sensor Physical Size.
        //----------------------------------------------------------------------------
        Vec2d sensor_size = mrf::rendering::SensorSize::APS_C;

        if (sensor_element)
        {
          try
          {
            sensor_size = Parser::vec2_for_width_and_height<double, Vec2>(*sensor_element, CameraLexer::SIZE_MK);
          }
          catch (...)
          {
            mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve size of the sensor. Assigning default one");
          }
        }
        else
        {
          std::cout << " PROUT " << std::endl;
        }
        mrf::gui::fb::Loger::getInstance()->trace(" Sensor Size set to ", sensor_size);



        //----------------------------------------------------------------------------
        // Stop Number of F-Number
        //----------------------------------------------------------------------------
        double f_number = 10.0;
        try
        {
          f_number = Parser::scalar_from_attribute_name<double>(
              *camera_element,
              CameraLexer::LENS_MK,
              CameraLexer::FSTOP_AT);
        }
        catch (...)
        {
          mrf::gui::fb::Loger::getInstance()->warn(" COuld not retrieve f_number. Assigning default one");
        }
        mrf::gui::fb::Loger::getInstance()->trace(" F-Number set to: ", f_number);

        //----------------------------------------------------------------------------
        // Samples per Lens
        //----------------------------------------------------------------------------
        unsigned int spl = 50;
        try
        {
          spl = Parser::scalar_from_attribute_name<unsigned int>(
              *camera_element,
              CameraLexer::LENS_MK,
              CameraLexer::SAMPLES_PER_LENS_AT);
        }
        catch (...)
        {
          mrf::gui::fb::Loger::getInstance()->warn(
              " Could not retrieve Samples Number for the lens. Assigning default one");
        }
        mrf::gui::fb::Loger::getInstance()->trace(" #Samples for Lens is set to: ", spl);

        //----------------------------------------------------------------------------
        // Focal Length
        //----------------------------------------------------------------------------
        double focal_length = 0.035;
        try
        {
          focal_length = Parser::scalar_from_attribute_name<double>(
              *camera_element,
              CameraLexer::LENS_MK,
              CameraLexer::FOCAL_LENGTH_AT);
        }
        catch (...)
        {
          mrf::gui::fb::Loger::getInstance()->warn(
              " Could not retrieve focal length for the lens. Assigning default one");
        }
        mrf::gui::fb::Loger::getInstance()->trace(" #Focal Length is set to: ", focal_length);

        Camera *a_thin_lens_cam = new ThinLens(
            position,
            look_at,
            up,
            (float)fovy,
            focal_length,
            f_number,
            focus_distance,
            static_cast<float>(aperture),
            sensor_size,
            resolution,
            spp * spp,
            spl,
            id_camera_str);

        cameras[id_camera_str] = a_thin_lens_cam;
        break;
      }
      case CameraType::Orthographic:
      {
        //----------------------------------------------------------------------------
        // Sensor Physical Size.
        //----------------------------------------------------------------------------
        Vec2d sensor_size = mrf::rendering::SensorSize::APS_C;

        try
        {
          sensor_size = Parser::vec2_for_width_and_height<double, Vec2>(*sensor_element, CameraLexer::SIZE_MK);
        }
        catch (...)
        {
          mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve size of the sensor. Assigning default one");
        }

        mrf::gui::fb::Loger::getInstance()->trace(" Sensor Size set to ", sensor_size);


        // Now Creating the Classical Pinhole Camera
        Camera *a_new_orthographic_cam = new Orthographic(
            position,
            look_at,
            up,
            sensor_size,
            static_cast<float>(sensor_distance),
            3000.0f,
            resolution.x(),
            resolution.y(),
            spp,
            id_camera_str);

        cameras[id_camera_str] = a_new_orthographic_cam;
        break;
      }
      case CameraType::Environment:
      {
        //----------------------------------------------------------------------------
        // Theta
        //----------------------------------------------------------------------------
        float theta = static_cast<float>(mrf::math::_Math<float>::PI_2);
        try
        {
          theta = Parser::scalar_from_value<float>(*camera_element, "theta");
        }
        catch (...)
        {
          mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve theta value. Assigning default one");
        }
        mrf::gui::fb::Loger::getInstance()->trace(" theta set to : ", theta);

        //----------------------------------------------------------------------------
        // Phi
        //----------------------------------------------------------------------------
        float phi = 0.;
        try
        {
          phi = Parser::scalar_from_value<float>(*camera_element, "phi");
        }
        catch (...)
        {
          mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve phi value. Assigning default one");
        }
        mrf::gui::fb::Loger::getInstance()->trace(" phi set to : ", phi);


        // Now Creating the Classical Pinhole Camera
        Camera *a_new_environment_cam = new Environment(
            position,
            look_at,
            up,
            theta,
            phi,
            static_cast<float>(sensor_distance),
            3000.0f,
            resolution.x(),
            resolution.y(),
            spp,
            id_camera_str);

        cameras[id_camera_str] = a_new_environment_cam;
        break;
      }
      default:
        continue;
    }   //end of switch


  }   // end of for-loop
}

CameraParser &CameraParser::Instance()
{
  static CameraParser s_cam_parser = CameraParser();
  return s_cam_parser;
}

//------------------------------------------------------------------------------------------
// Singleton Implementation
//------------------------------------------------------------------------------------------
CameraParser::CameraParser() {}


CameraParser::~CameraParser() {}

CameraParser::CameraParser(CameraParser const &other) {}

CameraParser &CameraParser::operator=(CameraParser const &)
{
  return *this;
}

}   // end of namespace io
}   // end of namespace mrf
//
