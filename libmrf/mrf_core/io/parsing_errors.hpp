#pragma once


/**
 * Copyright(C) CNRS. 2015, 2016.
 *
 * Author: Romain Pacnowski: romain.pacanowski@institutoptique.fr
 *
 *
 */

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace io
{
class MRF_CORE_EXPORT ParsingErrors
{
public:
  static constexpr char const *const XML_LOAD_FILE          = " FAILED TO LOAD XML FILE ";
  static constexpr char const *const NO_MATERIALS           = " NO MATERIALS FOUND ";
  static constexpr char const *const NO_MATERIAL_NAME       = "MATERIAL HAS NO NAME";
  static constexpr char const *const UNDEFINED_MATERIAL     = "NO MATERIAL FOUND WITH PROVIDED NAME";
  static constexpr char const *const NO_LIGHTS              = " NO LIGHTS FOUND ";
  static constexpr char const *const NO_SHAPES              = " NO SHAPES FOUND ";
  static constexpr char const *const NO_MESHES              = " NO MESHES FOUND ";
  static constexpr char const *const NO_LIGHT_MATERIAL_NAME = " NAME ATTRIBUTE IS MISSING FOR LIGHT MATERIAL ";
  static constexpr char const *const NO_LIGHT_MATERIAL      = " NO LIGHT MATERIAL FOUND";
  static constexpr char const *const NO_VERTICES            = " NO VERTICES MARKUP FOUND ";
  static constexpr char const *const NO_FACES               = "NO FACES MARKUP  FOUND  ";
  static constexpr char const *const NO_TEXTURES            = "NO TEXTURES MARKUP  FOUND  ";
};



}   // namespace io

}   // namespace mrf
