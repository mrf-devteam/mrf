/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/io/obj_lexer.hpp>
#include <mrf_core/util/string_parsing.hpp>
#include <mrf_core/io/file_io.hpp>
#include <mrf_core/io/obj_scene.hpp>
#include <mrf_core/io/obj_material_parser.hpp>
#include <mrf_core/io/obj_material.hpp>
#include <mrf_core/materials/material_builder.hpp>
#include <mrf_core/rendering/scene.hpp>
#include <mrf_core/geometry/computation.hpp>

// STL
#include <vector>
#include <string>
#include <map>
#include <list>
#include <iostream>
#include <fstream>
#include <sstream>


namespace mrf
{
namespace io
{
template<class FileIOPolicy>
class _OBJParser
{
private:
  static FileIOPolicy _fileIO;


  //New representation
  OBJScene _objscene;

  std::string _cOBJFilename;        // The current obj file (full path included) being processed
  std::string _cAbsPathToOBJFile;   // The current path (and only the path) to the current obj file

  std::string              _cOBJMaterial;   // The name of the current material
  bool                     _cSmoothMode;    // The current value of the smooth mode (true enables smoothing)
  std::vector<std::string> _cGroups;        // The current active groups
  std::string              _cObjectName;    //The current name for the current object


  //Public Methods
public:
  static _OBJParser<FileIOPolicy> &Instance();


  /**
   * Load the given file and construct the object OBJScene
   *
   * @param abs_path_obj_filename: the  name of the file (with absolute path included) to the obj file to be loaded
   * @param abs_path_to_objfile: the absolute path to the obj file
   *
   * @return bool: true if the file was successfully loaded false otherwise
   *
   **/
  bool load(
      std::string const &        abs_path_obj_filename,
      std::string const &        abs_path_to_objfile,
      mrf::gui::fb::Loger::LEVEL logging_level);

  OBJScene &objscene();

  /**
   * Analyze the tokens of a line to construct an OBJFace that will be stored in the OBJScene
   * @param tokens : the list of tokens WITHOUT the one representing the face token.
   * @return bool: true if the face was correctly parsed false otherwise
   **/
  bool analyzeLineForFace(std::vector<std::string> const &tokens);


  bool analyzeOneLine(std::string const &line, unsigned int line_number);

  /**
   * Reset all the strings _c to "" and _cSmoothMode to false
   **/
  void resetCurrentStates();


  // Loading methods
  bool load(
      char const *               filename,
      std::vector<float> &       vertices,
      std::vector<float> &       v_normals,   // normal for the vertex
      std::vector<float> &       v_texture,   // texture coordinates for the vertex
      std::vector<unsigned int> &index_faces,
      bool                       parse_material_lib = false);

  bool load(
      std::string const &        filename,
      std::vector<float> &       vertices,
      std::vector<float> &       v_normals,   // normal for the vertex
      std::vector<float> &       v_texture,   // texture coordinates for the vertex
      std::vector<unsigned int> &index_faces,
      bool                       parse_material_lib = false);

  bool load(
      std::string const &    filename,
      std::string const &    obj_file_directory,
      mrf::rendering::Scene &scene,
      std::string const &    material_type,
      bool                   store_normal_per_face = false);


  //Returns true if parsing went good
  static inline bool
  convertToFloat(std::string const &s_x, std::string const &s_y, std::string const &s_z, float &x, float &y, float &z);

private:
  unsigned int current_material;
  unsigned int current_group;

  //Helper for parsing
  static bool analyzeOneLine(
      std::string &              line,
      std::vector<float> &       vertices,
      std::vector<float> &       v_normals,
      std::vector<float> &       v_texture,
      std::vector<unsigned int> &materials,
      std::vector<unsigned int> &index_faces,
      bool                       parse_material_lib = false);

  //Returns true if parsing went good
  static inline bool getVertex(
      std::string const &s_x,
      std::string const &s_y,
      std::string const &s_z,
      std::string const &s_w,
      float &            x,
      float &            y,
      float &            z);

  static inline void countData(
      char const *  filename,
      unsigned int &nb_f,
      unsigned int &nb_usemtl,
      std::string & mtl_filename,
      unsigned int &nb_group);

  static bool processMaterialFile(
      std::string const &                  directory_path,
      std::string const &                  mtl_file_name,
      std::string const &                  material_type_mode,
      mrf::rendering::Scene &              scene,
      std::map<std::string, unsigned int> &materials_map);

  static bool parseVertexAndAttributes(
      std::string const & file_name_path,
      std::vector<float> &vertices,
      std::vector<float> &v_normals, /* normal for the vertex*/
      std::vector<float> &v_texture /* texture coordinates */);


  //-------------------------------------------------------------------------
  // Parse the indices of the faces and associate
  // for each face a material id
  //-------------------------------------------------------------------------
  static bool parseFacesAndMaterial(
      std::string const &                        file_name_path,
      std::map<std::string, unsigned int> const &materials_map,
      std::vector<unsigned int> &                index_faces,
      std::vector<unsigned int> &                index_normals,
      std::vector<unsigned int> &                index_uvs,
      std::vector<unsigned int> &                materials);


  static bool analyzeTokensForGeometry(
      std::vector<std::string> const &tokens,
      std::vector<float> &            vertices,
      std::vector<float> &            v_normals,
      std::vector<float> &            v_texture);

  static bool analyzeTokensForFacesAndMaterials(
      std::vector<std::string> const &tokens,
      unsigned int                    current_material_id,
      std::vector<unsigned int> &     index_faces,
      std::vector<unsigned int> &     index_normals,
      std::vector<unsigned int> &     index_uvs,
      std::vector<unsigned int> &     materials);

  //Hidden due to Singleton Implementation
  _OBJParser();
  virtual ~_OBJParser();
  _OBJParser(_OBJParser<FileIOPolicy> const &other);                            // copy ctor is hidden
  _OBJParser<FileIOPolicy> &operator=(_OBJParser<FileIOPolicy> const &other);   // assign op is hidden
};

#include "obj_parser.cxx"

typedef _OBJParser<StreamFileIO> OBJParser;

}   // namespace io
}   // namespace mrf
