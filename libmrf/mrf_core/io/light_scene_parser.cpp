///*
// *
// * author : Romain Pacanowski @ institutoptique.fr
// * Copyright CNRS 2016
// * Copyright CNRS 2017
// *
// **/
//
//#include <mrf_core/io/light_scene_parser.hpp>
//
//#include <mrf_core/util/string_parsing.hpp>
//#include <mrf_core/io/extensions.hpp>
//#include <mrf_core/io/alta_reader.h>
//#include <mrf_core/io/file_io.hpp>
//
////STL
//#include <iostream>
//#include <vector>
//#include <typeinfo>
//#include <algorithm>
//
//
//#include <mrf_core/util/precision_timer.hpp>
//
//namespace mrf
//{
//namespace io
//{
//
//bool LightSceneParser::parsingScene(tinyxml2::XMLElement *all_mat_elem, mrf::rendering::Scene &scene)
//{
//  mrf::gui::fb::Loger::getInstance()->trace("Starting to parse the scene file ");
//
//  //Clear All Maps and BRDF counter
//  _brdf_map.clear();
//
//  bool correctly_parsed_global_material = true;
//  if (!parseGlobalMaterials(scene))
//  {
//    mrf::gui::fb::Loger::getInstance()->warn(" Problem while parsing materials ");
//    correctly_parsed_global_material = false;
//  }
//
//  //we do not return the error due to bad parsing
//  //of ext resources to allow the user to still render an
//  //image even though some envmaps or other ext resources
//  //can't be loaded
//  return correctly_parsed_global_material;
//}
//
//mrf::materials::UMat *LightSceneParser::addEmittance(tinyxml2::XMLElement *an_emitt_elem, mrf::rendering::Scene &scene)
//{
//  if (!an_emitt_elem->Attribute(SceneLexer::NAME_AT))
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_LIGHT_MATERIAL_NAME);
//    return nullptr;
//  }
//  std::string emittance_name(an_emitt_elem->Attribute(SceneLexer::NAME_AT));
//
//  mrf::gui::fb::Loger::getInstance()->trace("emittance_name", emittance_name);
//
//  std::string emittance_type;
//  retrieveEmittanceType(an_emitt_elem, emittance_type);
//
//  mrf::gui::fb::Loger::getInstance()->trace("emittance_type ", emittance_type);
//
//  if (emittance_type.compare(mrf::materials::EmittanceTypes::DIFFUSE) == 0)
//  {
//    mrf::materials::DiffuseEmittance *diffuse_emit = new mrf::materials::DiffuseEmittance(emittance_name);
//    assert(diffuse_emit);
//
//    retrieveDiffuseEmittance(an_emitt_elem, *diffuse_emit);
//
//    _brdf_map[emittance_name] = scene.addMaterial(diffuse_emit);
//    return diffuse_emit;
//  }
//  else if (emittance_type.compare(mrf::materials::EmittanceTypes::UNIFORM) == 0)
//  {
//    mrf::materials::UniformEmittance *emit = new mrf::materials::UniformEmittance(emittance_name);
//    assert(emit);
//
//    retrieveDiffuseEmittance(an_emitt_elem, *emit);
//
//    _brdf_map[emittance_name] = scene.addMaterial(emit);
//    return emit;
//  }
//  else if (emittance_type.compare(mrf::materials::EmittanceTypes::DIRAC) == 0)
//  {
//    mrf::math::Vec3f direction = ParserHelper::retrieveDirection(an_emitt_elem);
//    //if (!retrieveDirection(an_emitt_elem, direction))
//    //{
//    //  mrf::gui::fb::Loger::getInstance()->fatal(" ABORTING while trying to retrieve the direction of the light ");
//    //  return nullptr;
//    //}
//
//    mrf::materials::COLOR color = ParserHelper::retrieveColor(an_emitt_elem);
//    //if (!retrieveColor(an_emitt_elem, color))
//    //{
//    //  mrf::gui::fb::Loger::getInstance()->fatal("Emittance : Color value could not be retrieve ");
//    //  return nullptr;
//    //}
//
//    tinyxml2::XMLElement *radiance_elem = an_emitt_elem->FirstChildElement(SceneLexer::RADIANCE_MK);
//    float       radiance      = ParserHelper::scalar_from_value<float>(radiance_elem);
//
//    //if (!retrieveValue(radiance_elem, radiance))
//    //{
//    //  mrf::gui::fb::Loger::getInstance()->fatal("Emittance : Radiance value could not be retrieve ");
//    //  return nullptr;
//    //}
//
//
//    mrf::gui::fb::Loger::getInstance()->debug(" direction retrieved ", direction);
//    mrf::gui::fb::Loger::getInstance()->debug(" color retrieved ", color);
//    mrf::gui::fb::Loger::getInstance()->debug(" radiance retrieved ", radiance);
//
//    //Direction* geom_dir = new Direction( direction );
//    mrf::materials::DiracEmittance *emit
//        = new mrf::materials::DiracEmittance(emittance_name, direction, radiance, color);
//
//    _brdf_map[emittance_name] = scene.addMaterial(emit);
//    return emit;
//  }
//  else if (emittance_type.compare(mrf::materials::EmittanceTypes::CONIC) == 0)
//  {
//    float aperture;
//    int   apertureOK = an_emitt_elem->QueryFloatAttribute(SceneLexer::APERTURE_AT, &aperture);
//
//    if (apertureOK != tinyxml2::XML_SUCCESS)
//    {
//      mrf::gui::fb::Loger::getInstance()->info(" Conic Emittance. PROBLEM WHILE RETRIEVING THE APERTURE ");
//      return nullptr;
//    }
//
//
//    mrf::materials::COLOR color = ParserHelper::retrieveColor(an_emitt_elem);
//    //if (!retrieveColor(an_emitt_elem, color))
//    //{
//    //  std::cout << __FILE__ << " " << __LINE__ << std::endl;
//    //  mrf::gui::fb::Loger::getInstance()->warn(" Conic Emittance. Color value could not be retrieve ");
//    //  return nullptr;
//    //}
//
//    tinyxml2::XMLElement *radiance_elem = an_emitt_elem->FirstChildElement(SceneLexer::RADIANCE_MK);
//    float radiance = ParserHelper::scalar_from_value<float>(radiance_elem);
//    //if (!retrieveValue(radiance_elem, radiance))
//    //{
//    //  mrf::gui::fb::Loger::getInstance()->warn(" Conic Emittance. Radiance value could not be retrieve ");
//    //  return nullptr;
//    //}
//
//    mrf::math::Vec3f direction = ParserHelper::retrieveDirection(an_emitt_elem);
//    //if (!retrieveDirection(an_emitt_elem, direction))
//    //{
//    //  mrf::gui::fb::Loger::getInstance()->warn("Direction values could not be retrieved could not be retrieved\n. Assigning default one (0,1,0)");
//    //  direction = Vec3f(0, 1, 0);
//    //}
//
//    mrf::gui::fb::Loger::getInstance()->trace(" Color ", color);
//    mrf::gui::fb::Loger::getInstance()->trace(" Radiance ", radiance);
//    mrf::gui::fb::Loger::getInstance()->trace(" Aperture ", aperture);
//    mrf::gui::fb::Loger::getInstance()->trace(" Direction ", direction);
//
//    //convert aperture in radians
//    aperture = aperture / 180.f * static_cast<float>(mrf::math::Math::PI);
//
//    mrf::materials::ConicalEmittance *conemit_mat
//        = new mrf::materials::ConicalEmittance(emittance_name, aperture, direction, radiance, color);
//
//    _brdf_map[emittance_name] = scene.addMaterial(conemit_mat);
//    return conemit_mat;
//  }
//  else
//  {
//    mrf::gui::fb::Loger::getInstance()->warn("Unsupported emittance type");
//  }
//  return nullptr;
//}
//
//
//bool LightSceneParser::retrieveEmittanceType(tinyxml2::XMLElement *elem, std::string &emittance_type)
//{
//  if (!elem->Attribute(SceneLexer::TYPE_AT))
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal(" Emittance HAS NOT TYPE : Assigning default one (Diffuse)");
//    return false;
//  }
//
//  emittance_type = elem->Attribute(SceneLexer::TYPE_AT);
//  return true;
//}
//
//
//bool LightSceneParser::retrieveDiffuseEmittance(tinyxml2::XMLElement *elem, mrf::materials::DiffuseEmittance &emittance)
//{
//  assert(elem);
//  mrf::materials::COLOR color = ParserHelper::retrieveColor(elem);
//
//  mrf::gui::fb::Loger::getInstance()->debug(" Retrieving Diffuse Emittance  ");
//
//  //if (!retrieveColor(elem, color))
//  //{
//  //  mrf::gui::fb::Loger::getInstance()->warn(" DiffuseEmittance. Color value could not be retrieve ");
//  //  return false;
//  //}
//  emittance.setColor(color);
//
//  tinyxml2::XMLElement *radiance_elem = elem->FirstChildElement(SceneLexer::RADIANCE_MK);
//  if (!radiance_elem)
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal("COULD NOT RETRIEVE Radiance  Element for light source");
//    return false;
//  }
//
//
//  float radiance = ParserHelper::scalar_from_value<float>(radiance_elem);
//  //if (!retrieveValue(radiance_elem, radiance))
//  {
//    mrf::gui::fb::Loger::getInstance()->warn(" DiffuseEmittance. Radiance value could not be retrieve ");
//    return false;
//  }
//
//  emittance.setRadiance(radiance);
//
//  return true;
//}
//
//bool LightSceneParser::checkErrorOnTextureLoading(mrf::image::IMAGE_LOAD_SAVE_FLAGS flag, std::string const &name)
//{
//  using namespace mrf::image;
//  if (flag == MRF_NO_ERROR)
//  {
//    mrf::gui::fb::Loger::getInstance()->info("Successfully loaded ", name);
//    return true;
//  }
//  if (flag == MRF_ERROR_WRONG_EXTENSION)
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error WRONG EXTENSION file=", name);
//    return false;
//  }
//  if (flag == MRF_ERROR_WRONG_FILE_PATH)
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error WRONG FILE PATH file=", name);
//    return false;
//  }
//  if (flag == MRF_ERROR_LOADING_FILE)
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error LOADING FILE file=", name);
//    return false;
//  }
//  if (flag == MRF_ERROR_DECODING_FILE)
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error DECODING FILE file=", name);
//    return false;
//  }
//  if (flag == MRF_ERROR_WRONG_PIXEL_FORMAT)
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error WRONG PIXEL FORMAT file=", name);
//    return false;
//  }
//  if (flag == MRF_ERROR_WRONG_IMAGE_SIZE)
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error WRONG IMAGE SIZE file=", name);
//    return false;
//  }
//  mrf::gui::fb::Loger::getInstance()->fatal("Unrecognized error flag on load image flag=", flag);
//  return false;
//}
//
//LightSceneParser::~MaterialSceneParser()
//{
//}
//
//bool LightSceneParser::retrieveMaterialIndex(tinyxml2::XMLElement *elem, unsigned int &material_index) const
//{
//  if (!elem->Attribute(SceneLexer::REF_MATERIAL_AT))
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_MATERIALS);
//    return false;
//  }
//
//  std::string ref_material = elem->Attribute(SceneLexer::REF_MATERIAL_AT);
//  mrf::gui::fb::Loger::getInstance()->trace("REFERENCE MATERIAL ");
//  mrf::gui::fb::Loger::getInstance()->trace(ref_material);
//
//  //NOW Check if the material exists
//  auto current = _brdf_map.find(ref_material);
//  auto end     = _brdf_map.end();
//  if (current == end)   //  THIS Key does not exists !
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::UNDEFINED_MATERIAL);
//    return false;
//  }
//  //This returns a TYPE& and not a TYPE const & brdfMap[ref_material];
//  material_index = current->second;
//  return true;
//}
//
//
//
//void LightSceneParser::cleanString(std::string &str)
//{
//  std::string space(mrf::util::SPACE);
//  std::string double_space(mrf::util::DOUBLE_SPACE);
//  std::string tab(mrf::util::TAB);
//  std::string new_unix_line(mrf::util::NEW_LINE_UNIX);
//  std::string new_win_line(mrf::util::NEW_LINE_WIN);
//  std::string new_mac_line(mrf::util::NEW_LINE_MAC);
//
//  mrf::util::StringParsing::replaceAllOccurences(str, tab, space);
//  mrf::util::StringParsing::replaceAllOccurences(str, new_unix_line, space);
//  mrf::util::StringParsing::replaceAllOccurences(str, new_mac_line, space);
//  mrf::util::StringParsing::replaceAllOccurences(str, new_win_line, double_space);
//}
//
//}   // namespace io
//}   // namespace mrf
