/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/feedback/loger.hpp>
#include <mrf_core/util/string_parsing.hpp>

#include <cassert>
#include <cstdio>
#include <ios>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>

//disable warning for fopen and fscanf on visual studio
#ifdef _MSC_VER
#  pragma warning(disable : 4996)
#endif

namespace mrf
{
namespace io
{
/**
 * Returns true if file exists
 **/
inline bool file_exists(std::string const &filename)
{
  std::ifstream ifile(filename.c_str());
  return (bool)ifile;
}

/**
 * try to find a file by concataning current_directory and relative_file_path
 * If not found try each folder in folders (concataning folder and relative_file_path)
 * If not found try each folder in folders (concataning folder and file_path extracted from relative_file_path)
 *
 * Store result in found_path (first one found)
 * Return true if a file is found
 **/
inline bool find_file_from_folders(
    std::string const &             current_directory,
    std::vector<std::string> const &directories,
    std::string const &             relative_file_path,
    std::string &                   found_path)
{
  std::string filename;
  mrf::util::StringParsing::fileNameFromAbsPath(relative_file_path.c_str(), filename);

  bool file_found = false;

  //Try from current folder
  std::string path = current_directory + relative_file_path;
  if (mrf::io::file_exists(path))
  {
    file_found = true;
    mrf::gui::fb::Loger::getInstance()->trace("FOUND AT: ", path);
  }

  //try from one of ext resources paths
  if (!file_found)
  {
    for (const auto &folder : directories)
    {
      path = current_directory + folder + relative_file_path;
      mrf::gui::fb::Loger::getInstance()->trace(" Try: ", path);

      if (mrf::io::file_exists(path))
      {
        file_found = true;
        mrf::gui::fb::Loger::getInstance()->trace("FOUND AT: ", path);
        break;
      }
    }
  }

  //try from one of ext resources paths,
  //without the relative path from the xml attrib
  if (!file_found)
  {
    for (const auto &folder : directories)
    {
      path = current_directory + folder + filename;
      mrf::gui::fb::Loger::getInstance()->trace(" Try: ", path);

      if (mrf::io::file_exists(path))
      {
        file_found = true;
        mrf::gui::fb::Loger::getInstance()->trace("FOUND AT: ", path);
        break;
      }
    }
  }

  if (!file_found)
  {
    mrf::gui::fb::Loger::getInstance()->warn(
        "File does not exists in any externals paths, Path of file is: ",
        relative_file_path);
    return false;
  }

  found_path = path;
  return true;
}

class MRF_CORE_EXPORT _FileIO
{};


class MRF_CORE_EXPORT CFileIO: public _FileIO
{
public:
  //openmode = app,ate,binary,in,out,trunc
  static void open(char const *filename, std::ios_base::openmode mode);

  static inline void close() { fclose(_fileStream); }


  static void seekp(long int offset, std::ios::seekdir origin);

  static inline void seekg(long int offset, std::ios::seekdir origin) { seekp(offset, origin); }

  //Write Raw bytes
  static inline void write(char const *buffer, std::streamsize num) { fwrite(buffer, sizeof(char), num, _fileStream); }

  //Read Raw bytes
  static inline void read(char *buffer, std::streamsize num) { fread(buffer, sizeof(char), num, _fileStream); }

  static inline bool eof()
  {
    int isEOF = feof(_fileStream);

    if (isEOF != 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Does not check if end of file has been reached i.e.
   * eof is not an error */
  static inline bool error()
  {
    if (_fileStream == NULL)
    {
      return true;
    }

    int errorOccured = ferror(_fileStream);
    if (errorOccured != 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  static inline std::streampos tellg() { return static_cast<std::streampos>(ftell(_fileStream)); }

  static inline std::streampos tellp() { return static_cast<std::streampos>(ftell(_fileStream)); }


  static void readLine(std::string &line);

  static inline void write(std::string const &line) { fputs(line.c_str(), _fileStream); }

  static inline void write(char const *data) { fputs(data, _fileStream); }

  static void inline write(unsigned int const n)
  {
    assert(_fileStream);
    fprintf(_fileStream, "%u", n);
  }

  static void inline write(float const a)
  {
    assert(_fileStream);
    fprintf(_fileStream, "%f", a);
  }


  static void inline read(float &ax1, float &ax2, float &ax3)
  {
    assert(_fileStream);
    fscanf(_fileStream, "%f %f %f", &ax1, &ax2, &ax3);
  }

  static void inline read(double &x1, double &x2, double &x3)
  {
    assert(_fileStream);
    fscanf(_fileStream, "%lf %lf %lf", &x1, &x2, &x3);
  }

  static void inline read(int &x1, int &x2, int &x3)
  {
    assert(_fileStream);
    fscanf(_fileStream, "%d %d %d", &x1, &x2, &x3);
  }

  static void inline read(unsigned int &x1, unsigned int &x2, unsigned int &x3)
  {
    assert(_fileStream);
    fscanf(_fileStream, "%u %u %u", &x1, &x2, &x3);
  }

  static inline FILE *fileStream() { return _fileStream; }



private:
  static FILE *_fileStream;

  //TEXT MODES
  static char const *READ_MODE;
  static char const *WRITE_MODE;
  static char const *APPEND_MODE;   //stream is positioned at the end

  static char const *READ_WRITE_MODE;

  //TC = Truncate + Create
  // IF the file exist it truncates it to zero length
  // if not, it create a new file
  static char const *READ_WRITE_TC_MODE;
  static char const *READ_APPEND_MODE;   //stream is positioned at the end


  //BINARY MODES
  static char const *BINARY_READ_MODE;
  static char const *BINARY_WRITE_MODE;
  static char const *BINARY_APPEND_MODE;   //stream is positioned at the end

  static char const *BINARY_READ_WRITE_MODE;
  static char const *BINARY_READ_WRITE_TC_MODE;
  static char const *BINARY_READ_APPEND_MODE;   //stream is placed at the end

};   //end of _CFileIO Class

class MRF_CORE_EXPORT StreamFileIO: public _FileIO
{
public:
  static inline void open(char const *filename, std::ios_base::openmode mode)
  {
    _fileStream.clear();
    _fileStream.open(filename, mode);

#ifdef DEBUG
    if (!_fileStream)
    {
      std::cout << "Filename = " << filename << std::endl;
      std::cout << "PROBLEM" << std::endl;
    }
#endif
  }


  static inline void close() { _fileStream.close(); }


  //Origin = ios_base::cur or ios_base::beg or ios_base::end
  static inline void seekp(long int offset, std::ios::seekdir origin) { _fileStream.seekp(offset, origin); }

  static inline void seekg(long int offset, std::ios::seekdir origin) { _fileStream.seekg(offset, origin); }


  static inline void seekg(std::ios::seekdir position) { _fileStream.seekg(position); }

  //Write Raw bytes
  static inline void write(char const *buffer, std::streamsize num) { _fileStream.write(buffer, num); }


  //Read Raw bytes
  static inline void read(char *buffer, std::streamsize num) { _fileStream.read(buffer, num); }


  //Text input
  static void inline readLine(std::string &line)
  {
    line.clear();

    char c = static_cast<char>(_fileStream.get());

    while (_fileStream.good() && c != '\n')
    {
      if (c != '\r')
      {
        line.append(1, c);
      }

      c = static_cast<char>(_fileStream.get());
    }
  }

  //Text output
  static void inline write(std::string const &str) { _fileStream << str; }

  static void inline write(unsigned int const n) { _fileStream << n; }

  static void inline write(unsigned char const cn) { _fileStream << cn; }

  static void inline write(float const fn) { _fileStream << fn; }


  static void inline write(double const dn) { _fileStream << dn; }

  static inline bool error() { return _fileStream.fail(); }

  static inline bool eof() { return _fileStream.eof(); }

  static inline std::streampos tellg() { return _fileStream.tellg(); }


  static inline std::streampos tellp() { return _fileStream.tellp(); }


  static void inline read(float &x1, float &x2, float &x3) { _fileStream >> x1 >> x2 >> x3; }

  static void inline read(double &x1, double &x2, double &x3) { _fileStream >> x1 >> x2 >> x3; }

  static void inline read(int &x1, int &x2, int &x3) { _fileStream >> x1 >> x2 >> x3; }

  static void inline read(unsigned int &x1, unsigned int &x2, unsigned int &x3) { _fileStream >> x1 >> x2 >> x3; }

  static int inline get() { return _fileStream.get(); }

  static bool inline good() { return _fileStream.good(); }



private:
  static std::fstream _fileStream;


};   // end of _StreamFileIO Class


}   //end of namespace io
}   //end of namespace mrf

//disable warning for fopen and fscanf on visual studio
#ifdef _MSC_VER
#  pragma warning(default : 4996)
#endif