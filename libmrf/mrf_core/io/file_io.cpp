#include <mrf_core/io/file_io.hpp>

//disable warning for fopen and fscanf on visual studio
#ifdef _MSC_VER
#  pragma warning(disable : 4996)
#endif

namespace mrf
{
namespace io
{
/*************************************************************************/
//CFileIO Class

//STATIC PRIVATE VARIABLES
FILE *CFileIO::_fileStream = NULL;

//TEXT MODES
char const *CFileIO::READ_MODE   = "r";
char const *CFileIO::WRITE_MODE  = "w";
char const *CFileIO::APPEND_MODE = "a";   //stream is positioned at the end

char const *CFileIO::READ_WRITE_MODE = "r+";

//TC = Truncate + Create
// IF the file exist it truncates it to zero length
// if not, it create a new file
char const *CFileIO::READ_WRITE_TC_MODE = "w+";
char const *CFileIO::READ_APPEND_MODE   = "a+";   // stream is positioned
// at the end


//BINARY MODES
char const *CFileIO::BINARY_READ_MODE   = "rb";
char const *CFileIO::BINARY_WRITE_MODE  = "wb";
char const *CFileIO::BINARY_APPEND_MODE = "ab";   // stream is positioned
// at the end

char const *CFileIO::BINARY_READ_WRITE_MODE    = "rb+";
char const *CFileIO::BINARY_READ_WRITE_TC_MODE = "wb+";
char const *CFileIO::BINARY_READ_APPEND_MODE   = "ab+";   // stream is placed
// at the end

//STATIC FUNCTIONS
void CFileIO::open(char const *filename, std::ios_base::openmode mode)
{
  if (mode & std::ios::binary)
  {
    std::cout << __FILE__ << __LINE__ << std::endl;
    if ((mode & std::ios::app) || (mode & std::ios::ate))
    {
      if (mode & std::ios::in)
      {
        // std::cout << __FILE__ << __LINE__ << std::endl;
        _fileStream = fopen(filename, BINARY_READ_APPEND_MODE);
      }
      else
      {
        std::cout << __FILE__ << __LINE__ << std::endl;
        _fileStream = fopen(filename, BINARY_APPEND_MODE);
      }
    }   //end of appending case
    else if (mode & std::ios::in & std::ios::out)
    {
      if (mode & std::ios::trunc)
      {
        // std::cout << __FILE__ << __LINE__ << std::endl;
        _fileStream = fopen(filename, BINARY_READ_WRITE_TC_MODE);
      }
      else
      {
        // std::cout << __FILE__ << __LINE__ << std::endl;
        _fileStream = fopen(filename, BINARY_READ_WRITE_MODE);
      }
    }
    else if (mode & std::ios::in)
    {
      // std::cout << __FILE__ << __LINE__ << std::endl;
      _fileStream = fopen(filename, BINARY_READ_MODE);
    }
    else if (mode & std::ios::out)
    {
      // std::cout << __FILE__ << __LINE__ << std::endl;
      _fileStream = fopen(filename, BINARY_WRITE_MODE);
    }
  }   // end of binary case
  else
  {
    if ((mode & std::ios::app) || (mode & std::ios::ate))
    {
      if (mode & std::ios::in)
      {
        // std::cout << __FILE__ << __LINE__ << std::endl;
        _fileStream = fopen(filename, READ_APPEND_MODE);
      }
      else
      {
        // std::cout << __FILE__ << __LINE__ << std::endl;
        _fileStream = fopen(filename, APPEND_MODE);
      }
    }   //end of appending case
    else if (mode & std::ios::in & std::ios::out)
    {
      if (mode & std::ios::trunc)
      {
        // std::cout << __FILE__ << __LINE__ << std::endl;
        _fileStream = fopen(filename, READ_WRITE_TC_MODE);
      }
      else
      {
        // std::cout << __FILE__ << __LINE__ << std::endl;
        _fileStream = fopen(filename, READ_WRITE_MODE);
      }
    }
    else if (mode & std::ios::in)
    {
#ifdef DEBUG
      std::cout << "  OPEN MODE C READ MODE " << std::endl;
#endif

      _fileStream = fopen(filename, READ_MODE);
    }
    else if (mode & std::ios::out)
    {
      if (mode & std::ios::trunc)
      {
        // std::cout << __FILE__ << __LINE__ << std::endl;
        _fileStream = fopen(filename, WRITE_MODE);
      }
      else
      {
        // std::cout << __FILE__ << __LINE__ << std::endl;
        _fileStream = fopen(filename, APPEND_MODE);
      }
    }
  }
}   // end of open function


void CFileIO::seekp(long int offset, std::ios::seekdir origin)
{
  if (origin == std::ios_base::cur)
  {
    fseek(_fileStream, offset, SEEK_CUR);
  }
  else if (origin == std::ios_base::beg)
  {
    fseek(_fileStream, offset, SEEK_SET);
  }
  else if (origin == std::ios_base::end)
  {
    fseek(_fileStream, offset, SEEK_END);
  }
}

//Text line input
void CFileIO::readLine(std::string &line)
{
  char  buffer[512];
  char *s = fgets(buffer, 512 * sizeof(char), _fileStream);

  if (s != NULL)
  {
    std::string lineRetrieved(buffer);
#ifdef DEBUG
    std::cout << "  lineRetrieved = " << lineRetrieved;
#endif

    line = lineRetrieved.substr(0, lineRetrieved.size() - 1);
#ifdef DEBUG
    std::cout << "  line = " << line;
#endif
  }
  else
  {
#ifdef DEBUG
    std::cout << "POSSIBLE Problem while readLine C version" << std::endl;
#endif
  }
}


/************************************************************************/
//StreamFileIO Class
std::fstream StreamFileIO::_fileStream;


}   // namespace io

}   // namespace mrf

#ifdef _MSC_VER
#  pragma warning(default : 4996)
#endif