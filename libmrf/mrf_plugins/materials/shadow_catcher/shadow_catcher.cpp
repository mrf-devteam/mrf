#include "shadow_catcher.hpp"

namespace mrf
{
namespace materials
{
ShadowCatcher::ShadowCatcher(std::string const &name): BRDF(name, BRDFTypes::SHADOW_CATCHER) {}



ShadowCatcher::~ShadowCatcher() {}


//-------------------------------------------------------------------------
// UMat Interface Methods Implementation
//-------------------------------------------------------------------------
float ShadowCatcher::brdfValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.f;
}


RADIANCE_TYPE
ShadowCatcher::coloredBRDFValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return RADIANCE_TYPE();
}



float ShadowCatcher::diffuseAlbedo() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}

float ShadowCatcher::diffuseAlbedo(mrf::geom::LocalFrame const &) const
{
  return diffuseAlbedo();
}


bool ShadowCatcher::diffuseComponent() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return false;
}


float ShadowCatcher::specularAlbedo() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}


float ShadowCatcher::specularAlbedo(mrf::geom::LocalFrame const &) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}

bool ShadowCatcher::specularComponent() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return false;
}


float ShadowCatcher::transmissionAlbedo() const
{
  return 0.0f;
}

float ShadowCatcher::transmissionAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0f;
}

bool ShadowCatcher::transmissionComponent() const
{
  return false;
}


float ShadowCatcher::nonDiffuseAlbedo() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}


bool ShadowCatcher::nonDiffuseComponent() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return false;
}

float ShadowCatcher::totalAlbedo() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}


//-------------------------------------------------------------------------
float ShadowCatcher::diffPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}

float ShadowCatcher::specPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}

float ShadowCatcher::transPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}

float ShadowCatcher::nonDiffusePDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}


ScatterEvent::SCATTER_EVT ShadowCatcher::generateScatteringDirection(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    RADIANCE_TYPE & /*incident_radiance*/,
    mrf::math::Vec3f & /*outgoing_dir*/) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return ScatterEvent::ABSORPTION;
}




}   // namespace materials
}   // namespace mrf
