
#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> bg_color;
#  else
rtDeclareVariable(float, bg_color, , );
#  endif
#else
rtDeclareVariable(float3, bg_color, , );
#endif

bool bsdf_is_dirac()
{
  return true;
}

float bsdf_pdf(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  return 0.f;   //envmap_pdf(light);
}

// TODO: remove? exactly the same a below
COLOR bsdf_eval_optim(float3 h, float3 ffnormal, float3 eye, float3 light, int lobe, float external_pdf)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  COLOR tmp(0.f);

  if (use_envmap)
  {
    COLOR_B envmap_color = envmap_lookup(-eye);
    tmp                  = colorFromAsset(envmap_color, current_prd.wavelength_offset, current_prd.id_wavelength);
  }
  else
  {
    tmp = colorFromAsset(bg_color, current_prd.wavelength_offset, current_prd.id_wavelength);
  }

  return tmp;
#else
  if (use_envmap)
  {
    return envmap_lookup(-eye);
  }
  return bg_color;
#endif
}

COLOR bsdf_eval(float3 ffnormal, float3 eye, float3 light, int lobe)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  COLOR tmp(0.f);

  if (use_envmap)
  {
    COLOR_B envmap_color = envmap_lookup(-eye);
    tmp                  = colorFromAsset(envmap_color, current_prd.wavelength_offset, current_prd.id_wavelength);
  }
  else
  {
    tmp = colorFromAsset(bg_color, current_prd.wavelength_offset, current_prd.id_wavelength);
  }

  return tmp;
#else
  if (use_envmap)
  {
    return envmap_lookup(-eye);
  }
  return bg_color;
#endif
}

float3 generate_half_vector(float3 n, float3 in_dir)
{
  return n;
}

float4 generate_reflective_sample(float3 n, float3 in_dir)
{
  float2 rnd2d = sampling2D(
      current_prd.num_sample,
      current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
      current_prd.offset_sampling,
      current_prd.seed);
  float z1 = rnd2d.x;
  float z2 = rnd2d.y;

  //restrict first random number in sub interval of cdf
  //so it will only sample the half upper part of the envmap -> everything over horizon
  //(if we consider that envamp is oriented correctly )
  z1       = z1 * envmap_cdf_rows[envmap_cdf_rows.size() / 2];
  float4 L = sampleEnvmap(z1, z2, n);

  current_prd.shadow_catcher = 1;

  return make_float4(make_float3(L), -1.f);
}
