/*
 *
 * author : Arthur Dufay @inria.fr
 * Copyright CNRS 2018
 * Copyright INRIA 2018
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/measured_material.hpp>



namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *MEASURED_ISOTROPIC = "MeasuredIsotropic";
};   // namespace BRDFTypes

//-------------------------------------------------------------------------
// Measured Isotropic brdf from a alta file, only float values are supported
// and parametrization must be TV_TL_DPHI
//-------------------------------------------------------------------------

class MRF_PLUGIN_EXPORT MeasuredIsotropic: public MeasuredMaterial
{
protected:
#ifdef MRF_RENDERING_MODE_SPECTRAL
  //could be just a std::vector<float> to avoid duplication of wavelengths
  //which are the same for each triplet [theta_in,theta_out_phi] and the dirac
  std::vector<mrf::radiometry::Spectrum_T<float, float>> _data;
#else
  //TODO use samrt pointer
  float *       _data;
#endif
  mrf::Triplet_uint _data_size;

  std::vector<COLOR> _dirac;

public:
  MeasuredIsotropic(std::string const &name);

  virtual ~MeasuredIsotropic();


  //-------------------------------------------------------------------------
  // Some Accessors
  //-------------------------------------------------------------------------

#ifdef MRF_RENDERING_MODE_SPECTRAL
  inline std::vector<mrf::radiometry::Spectrum_T<float, float>> &getColor();
  inline void setColor(std::vector<mrf::radiometry::Spectrum_T<float, float>> &color);
#else
  inline float *getColor();
  inline void   setColor(float *data);
#endif

  inline mrf::Triplet_uint getDataSize() const;
  inline void              setDataSize(mrf::Triplet_uint const &size);
  inline std::size_t       getDiracSize() const;
  inline void              setDiracSize(uint size);
  inline void              setDiracColor(uint num_color, COLOR const &c);




  inline COLOR const &getDiracColor(uint num_color);


  //-------------------------------------------------------------------------
  // UMat Interface Methods Implementation
  //-------------------------------------------------------------------------

  // virtual float
  // brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  // virtual RADIANCE_TYPE coloredBRDFValue(
  //     mrf::geom::LocalFrame const &lf,
  //     mrf::math::Vec3f const &     in_dir,
  //     mrf::math::Vec3f const &     out_dir) const;


  virtual float diffuseAlbedo() const;
  virtual float diffuseAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  diffuseComponent() const;

  virtual float specularAlbedo() const;
  virtual float specularAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  specularComponent() const;

  virtual float transmissionAlbedo() const;
  virtual float transmissionAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  transmissionComponent() const;

  virtual float nonDiffuseAlbedo() const;
  virtual bool  nonDiffuseComponent() const;


  virtual float totalAlbedo() const;

  //-------------------------------------------------------------------------
  // virtual float
  // diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  // virtual float
  // specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  // virtual float
  // transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  // virtual float
  // nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;

  virtual ScatterEvent::SCATTER_EVT scatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f &           outgoing_dir,
      float &                      proba_outgoing_dir,
      float                        e1,
      float                        e2) const;

  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;
};


#ifdef MRF_RENDERING_MODE_SPECTRAL
inline std::vector<mrf::radiometry::Spectrum_T<float, float>> &MeasuredIsotropic::getColor()
{
  return _data;
}
inline void MeasuredIsotropic::setColor(std::vector<mrf::radiometry::Spectrum_T<float, float>> &color)
{
  _data = color;
}
#else
inline float *MeasuredIsotropic::getColor()
{
  return _data;
}


inline void MeasuredIsotropic::setColor(float *data)
{
  _data = data;
}
#endif


inline mrf::Triplet_uint MeasuredIsotropic::getDataSize() const
{
  return _data_size;
}
inline void MeasuredIsotropic::setDataSize(mrf::Triplet_uint const &size)
{
  _data_size = size;
}

inline std::size_t MeasuredIsotropic::getDiracSize() const
{
  return _dirac.size();
}

inline void MeasuredIsotropic::setDiracSize(uint size)
{
  _dirac.resize(size);
}

inline void MeasuredIsotropic::setDiracColor(uint num_color, COLOR const &c)
{
  _dirac[num_color] = c;
}



inline COLOR const &MeasuredIsotropic::getDiracColor(uint num_color)
{
  return _dirac[num_color];
}

}   // namespace materials
}   // namespace mrf
