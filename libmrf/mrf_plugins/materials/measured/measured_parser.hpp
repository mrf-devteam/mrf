#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>
#include <mrf_core/util/precision_timer.hpp>
#include <mrf_core/util/string_parsing.hpp>
#include <mrf_core/io/file_io.hpp>

#include <mrf_core/rendering/scene.hpp>


#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>
#include <mrf_core/io/alta_reader.h>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <mrf_core/materials/materials.hpp>

#include <mrf_plugins/materials/measured/measured_isotropic.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
class MeasuredIsotropicParser: public BaseMaterialParser<mrf::materials::MeasuredIsotropic>
{
public:
  MeasuredIsotropicParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new MeasuredIsotropicParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::MEASURED_ISOTROPIC; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::MeasuredIsotropic *measured_mat = dynamic_cast<mrf::materials::MeasuredIsotropic *>(a_mat);

    std::string brdf_path;

#ifdef MRF_RENDERING_MODE_SPECTRAL
    if (a_mat_element->Attribute(SceneLexer::SPECTRAL_FILE_PATH_AT))
    {
      brdf_path = a_mat_element->Attribute(SceneLexer::SPECTRAL_FILE_PATH_AT);
    }
    else if (a_mat_element->Attribute(SceneLexer::FILE_PATH_AT))
    {
      // This is for backward compatibility
      brdf_path = a_mat_element->Attribute(SceneLexer::FILE_PATH_AT);
      mrf::gui::fb::Loger::getInstance()->warn(
          " Syntax error. In XML markup  <materials type=\"MeasuredIsotropic\" /> you are using attribute file instead of "
          + std::string(SceneLexer::SPECTRAL_FILE_PATH_AT));
    }
    else
    {
      throw std::runtime_error("file or file_spectral Attribute is missing, Cannot load Measured Isotropic BRDF ");
      return false;
    }
#else
    if (a_mat_element->Attribute(SceneLexer::RGB_FILE_PATH_AT))
    {
      brdf_path = a_mat_element->Attribute(SceneLexer::RGB_FILE_PATH_AT);
    }
    else if (a_mat_element->Attribute(SceneLexer::FILE_PATH_AT))   // This is for backward compatibility
    {
      brdf_path = a_mat_element->Attribute(SceneLexer::FILE_PATH_AT);
      mrf::gui::fb::Loger::getInstance()->warn(
          " Syntax error. In XML markup  <materials type=\"MeasuredIsotropic\" /> you are using attribute file instead of "
          + std::string(SceneLexer::RGB_FILE_PATH_AT));
    }
    else
    {
      throw std::runtime_error("file or file_rgb Attribute is missing. Cannot load Measured Isotropic BRDF ");
      return false;
    }
#endif

    std::string     path;
    mrf::AltaHEADER alta_header;
    float *         data = nullptr;

    std::string brdf_directory;
    mrf::util::StringParsing::getDirectory(brdf_path.c_str(), brdf_directory);

    mrf::gui::fb::Loger::getInstance()->debug("Measured Isotropic BRDF Directory :" + brdf_directory);

    bool load_only_dirac = false;
    if (a_mat_element->Attribute(SceneLexer::ONLY_DIRAC_AT))
    {
      mrf::gui::fb::Loger::getInstance()->trace(
          "LOADING ONLY THE DIRAC PART FROM A MEASURED MATERIAL",
          load_only_dirac);
      load_only_dirac = true;
    }

    bool load_only_scattering_part = false;
    if (a_mat_element->Attribute(SceneLexer::ONLY_SCATTER_AT))
    {
      mrf::gui::fb::Loger::getInstance()->trace(" LOADING ONLY THE SCATTERING PART FROM A MEASURED MATERIAL");
      ;
      load_only_scattering_part = true;
    }

    if (load_only_dirac && load_only_scattering_part)
    {
      mrf::gui::fb::Loger::getInstance()->fatal(
          "YOU CHOOSE TWO CONTRADICTORY attributes (only_dirac and only_scatter)\n");
      mrf::gui::fb::Loger::getInstance()->fatal(
          "Either You specify to load only dirac part or to load only scattering");
      mrf::gui::fb::Loger::getInstance()->fatal("part of your measurement file !!!");

      //GAME OVER !
      throw std::runtime_error(
          "Specify to load only dirac part or to load only scattering part of your measurement file ");
      return false;
    }

    measured_mat->setInterpolate(false);
    if (a_mat_element->Attribute(SceneLexer::INTERPOLATE_AT, "1"))
    {
      mrf::gui::fb::Loger::getInstance()->trace("Interpolate angular sampling");
      measured_mat->setInterpolate(true);
    }

    unsigned int header_size = 0;


    if (mrf::util::StringParsing::pathIsAbsolute(brdf_path))   //Full path to resource has been provided
    {
      mrf::util::PrecisionTimer timer;

      timer.start();
      mrf::AltaReader::readData<float>(brdf_path, alta_header, data, mrf::gui::fb::Loger::getInstance());
      timer.stop();

      mrf::gui::fb::Loger::getInstance()->trace("Measured BRDF read in: ", timer.elapsed());
    }
    else   //Trying to locate the file in different directories
    {
      if (mrf::io::find_file_from_folders(parser._scene_path, parser._ext_paths, brdf_path, path))
      {
        mrf::util::PrecisionTimer timer;

        timer.start();
        mrf::AltaReader::readData<float>(path, alta_header, data, mrf::gui::fb::Loger::getInstance());
        timer.stop();

        mrf::gui::fb::Loger::getInstance()->trace("Measured BRDF read in: ", timer.elapsed());
      }
    }

    struct wrong_data_func
    {
      bool operator()(const std::string &value)
      {
        std::string temp_str = value;
        temp_str.erase(std::remove(temp_str.begin(), temp_str.end(), '\r'), temp_str.end());
        temp_str.erase(std::remove(temp_str.begin(), temp_str.end(), '\n'), temp_str.end());
        if (temp_str.size() == 0)
        {
          return true;
        }
        return false;
      }
    };

    // Impossible situation. no scattering term but the user asked for only that !
    // ===> Game over
    if (!alta_header.hasScatteringTerm() && load_only_scattering_part)
    {
      mrf::gui::fb::Loger::getInstance()->fatal(
          " Impossible situation !!! No scattering term inside .alta file but you asked for only the scattering term ");
      throw std::runtime_error(
          "Impossible situation !!! No scattering term inside .alta file but you asked for only the scattering term");
    }

// Impossible situation. no scattering term but the user asked for only that !
// ===> Game over
#ifdef MRF_RENDERING_MODE_SPECTRAL
    if (!alta_header.hasSpectralDirac() && load_only_dirac)
    {
      mrf::gui::fb::Loger::getInstance()->fatal(
          "Impossible situation !!! No DIRAC term inside .alta file but you asked for only the DIRAC term");
      throw std::runtime_error(
          "Impossible situation !!! No DIRAC term inside .alta file but you asked for only the DIRAC term");
    }
#else
    if (!alta_header.hasRGBDirac() && load_only_dirac)
    {
      mrf::gui::fb::Loger::getInstance()->fatal(
          "Impossible situation !!! No RGB DIRAC term inside .alta file but you asked for only the DIRAC term");
      throw std::runtime_error(
          "Impossible situation !!! No RGB DIRAC term inside .alta file but you asked for only the DIRAC term");
    }
#endif

    mrf::gui::fb::Loger::getInstance()->trace(" DATA from ALTA file loaded ");
    //check data or if parseHEader returned a size superior at 0 !
    if (data || header_size > 0)
    {
      // Parse the brdf data if scattering part needs to be loaded
      // Preparing data sizes to scattering part
      mrf::Triplet_uint data_size;

      if (!load_only_dirac && alta_header.hasScatteringTerm())
      {
        mrf::gui::fb::Loger::getInstance()->trace("Retrieving Grid Dimensions from ALTA header for Scattering Part");
        std::vector<int> grid_dimensions;
        for (auto it_list = alta_header.header()["#GRID"].cbegin(); it_list != alta_header.header()["#GRID"].cend();
             ++it_list)
        {
          grid_dimensions.push_back(std::stoi(*it_list));
        }

        if (grid_dimensions.size() == 3)
        {
          uint resolution_theta_in  = grid_dimensions[0];
          uint resolution_theta_out = grid_dimensions[1];
          uint resolution_phi       = grid_dimensions[2];
          /* uint data_counter = 0; */
          data_size = mrf::Triplet_uint(resolution_theta_in, resolution_theta_out, resolution_phi);
          measured_mat->setDataSize(data_size);
        }
        else
        {
          mrf::gui::fb::Loger::getInstance()->fatal(
              " GRID SIZE is different from 3.... errors will occur. ONLY ISOTROPIC BRDF is supported now. Aborting ");
          measured_mat = nullptr;
          throw std::runtime_error("BAD FILE IN Measured Isotropic Material.");
          return false;
        }
      }

#ifdef MRF_RENDERING_MODE_SPECTRAL

      //First parse the wavelengths required for DIRAC AND Scattering PART
      std::vector<float> waves;
      if (alta_header.header().find("#WAVELENGTH") != alta_header.header().end())
      {
        std::list<std::string> alta_waves = alta_header.header().find("#WAVELENGTH")->second;
        alta_waves.remove_if(wrong_data_func());

        waves.resize(alta_waves.size());

        uint i = 0;
        for (auto waves_it = alta_waves.cbegin(); waves_it != alta_waves.cend(); ++waves_it)
        {
          waves[i] = std::stof(*waves_it);
          i++;
        }
      }
      mrf::gui::fb::Loger::getInstance()->trace(" PArsed wavelengths. Wavelengths Size = ", waves.size());


      // if no scattering term is present or if data == nullptr we wont do that
      if ((data != nullptr) && (!load_only_dirac && alta_header.hasScatteringTerm()))
      {
        // COPY DATA from ALTA file to MeasuredIsotropic object if required
        auto &material_colors = measured_mat->getColor();
        material_colors.resize(std::get<0>(data_size) * std::get<1>(data_size) * std::get<2>(data_size));

        uint data_counter = 0;

        for (uint spectrum_counter = 0; spectrum_counter < material_colors.size(); spectrum_counter++)
        {
          mrf::radiometry::Spectrum_T<float, float> &spectrum = material_colors[spectrum_counter];
          spectrum.wavelengths()                              = waves;
          spectrum.values().resize(waves.size());

          for (uint wave_num = 0; wave_num < waves.size(); wave_num++)
          {
            spectrum[wave_num] = data[data_counter];
            data_counter++;
          }
        }
      }   // end of if !only-load-dirac


      //Parse the dirac data if required !
      if (alta_header.hasSpectralDirac() && (!load_only_scattering_part))
      {
        mrf::gui::fb::Loger::getInstance()->trace(" LOADING DIRAC PART OF MATERIAL ");

        //TODO: The if below seems useless now sicne we now that we have a DIRAC SPECTRAL
        if (alta_header.header().find("#DIRAC") != alta_header.header().end())
        {
          std::list<std::string> temp_dirac = alta_header.header().find("#DIRAC")->second;
          temp_dirac.remove_if(wrong_data_func());

          uint nb_colors = uint(temp_dirac.size() / waves.size());

          measured_mat->setDiracSize(nb_colors);

          uint num_color = 0;
          for (auto it_color = temp_dirac.cbegin(); it_color != temp_dirac.cend();)
          {
            mrf::radiometry::Spectrum_T<float, float> spectrum;

            spectrum.wavelengths() = waves;
            spectrum.values().resize(waves.size());

            for (uint i = 0; i < waves.size(); i++)
            {
              spectrum[i] = std::stof(*it_color++);
            }

            measured_mat->setDiracColor(num_color, spectrum);

            num_color++;
          }
        }   // end of if THERE is a DIRAC PART in the HEADER
      }     // end of if load-only-scattering_part

#else   // SPECTRAL VS RGB

      measured_mat->setColor(data);
      if (alta_header.hasRGBDirac() && (!load_only_scattering_part))
      {
        //TODO: The if below seems useless now
        if (alta_header.header().find("#DIRAC_RGB") != alta_header.header().end())
        {
          std::list<std::string> temp_dirac = alta_header.header().find("#DIRAC_RGB")->second;
          temp_dirac.remove_if(wrong_data_func());

          uint nb_colors = uint(temp_dirac.size()) / 3;

          measured_mat->setDiracSize(nb_colors);

          uint num_color = 0;
          for (auto it_color = temp_dirac.cbegin(); it_color != temp_dirac.cend();)
          {
            float dirac_r = std::stof(*it_color++);
            float dirac_g = std::stof(*it_color++);
            float dirac_b = std::stof(*it_color++);

            measured_mat->setDiracColor(num_color, mrf::materials::COLOR(dirac_r, dirac_g, dirac_b));

            num_color++;
          }
        }
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->warn(
            "NO RGB DIRAC FOUND in ALTA Measured Isotropic File. CHECK YOUR FILE. THE MATERIAL IS CREATED BUT BLACK !!!");
      }

#endif
    }
    return true;
  }

protected:
private:
};
}   // namespace io
}   // namespace mrf
