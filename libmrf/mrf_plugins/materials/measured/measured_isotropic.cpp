/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#include "measured_isotropic.hpp"
#include <mrf_core/sampling/random_generator.hpp>

namespace mrf
{
namespace materials
{
using namespace mrf::sampling;
using namespace math;


MeasuredIsotropic::MeasuredIsotropic(std::string const &name): MeasuredMaterial(name, BRDFTypes::MEASURED_ISOTROPIC)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL

#else
  //TODO use samrt pointer
  _data = nullptr;
#endif
}




MeasuredIsotropic::~MeasuredIsotropic()
{
#ifdef MRF_RENDERING_MODE_SPECTRAL

#else
  //TODO use samrt pointer
  delete[] _data;
#endif
}


//-------------------------------------------------------------------
// float MeasuredIsotropic::brdfValue(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const
// {
//   return 0.f;
// }


// RADIANCE_TYPE
// MeasuredIsotropic::coloredBRDFValue(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const
// {
//   return 0.f;
// }




float MeasuredIsotropic::diffuseAlbedo() const
{
  return 0.f;
}

float MeasuredIsotropic::diffuseAlbedo(mrf::geom::LocalFrame const & /*lf*/) const
{
  return diffuseAlbedo();
}

bool MeasuredIsotropic::diffuseComponent() const
{
  return true;
}

float MeasuredIsotropic::specularAlbedo() const
{
  return 0.0f;
}

float MeasuredIsotropic::specularAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0;
}

bool MeasuredIsotropic::specularComponent() const
{
  return false;
}

float MeasuredIsotropic::transmissionAlbedo() const
{
  return 0.0f;
}

float MeasuredIsotropic::transmissionAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0;
}

bool MeasuredIsotropic::transmissionComponent() const
{
  return false;
}


float MeasuredIsotropic::nonDiffuseAlbedo() const
{
  return 0.0f;
}

bool MeasuredIsotropic::nonDiffuseComponent() const
{
  return false;
}


float MeasuredIsotropic::totalAlbedo() const
{
  return 0.f;
}

//-------------------------------------------------------------------
// float MeasuredIsotropic::diffPDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const

// {
//   return 0.f;
// }

// float MeasuredIsotropic::specPDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const
// {
//   return 0.0f;
// }

// float MeasuredIsotropic::transPDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const
// {
//   return 0.0f;
// }

// float MeasuredIsotropic::nonDiffusePDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const
// {
//   return 0.0f;
// }



//-------------------------------------------------------------------
ScatterEvent::SCATTER_EVT MeasuredIsotropic::generateScatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    RADIANCE_TYPE & /*incident_radiance*/,
    Vec3f &outgoing_dir) const
{
  float e1 = RandomGenerator::Instance().getFloat();

  //TODO IMPROVE ME : check whether or not the norma of
  // incident_radiance * _color is > EPSILON
  // if not RETURN IMMEDIATELY ON ABSORPTION EVENT !!!

  if (e1 < diffuseAlbedo())   // TOTAL ALBEDO = DIFFUSE ALBEDO FOR
  // LAMBERTIAN SURFACE
  {
    //#ifdef DEBUG
    // std::cout << " HEere = " << __FILE__ << " "<<  __LINE__  << std::endl;
    // std::cout << "  _color = " << _color << std::endl ;
    //#endif


    //We use a pdf proportional to the cosine
    //incident_radiance *= _color;
    e1 /= diffuseAlbedo();

    float e2     = RandomGenerator::Instance().getFloat();
    outgoing_dir = randomHemiCosDirection(lf, e1, e2);

    //#ifdef DEBUG
    // std::cout << "  outgoing_dir = " << outgoing_dir << std::endl ;
    //#endif

    return ScatterEvent::DIFFUSE_REFL;
  }
  else
  {
    return ScatterEvent::ABSORPTION;
  }
}

ScatterEvent::SCATTER_EVT MeasuredIsotropic::scatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f &outgoing_dir,
    float &           proba_outgoing_dir,
    float             e1,
    float             e2) const
{
  if (e1 < diffuseAlbedo())   // TOTAL ALBEDO = DIFFUSE ALBEDO FOR
  // LAMBERTIAN SURFACE
  {
    //We use a pdf proportional to the cosine
    float const e3     = e1 / diffuseAlbedo();
    outgoing_dir       = randomHemiCosDirection(lf, e2, e3);
    proba_outgoing_dir = static_cast<float>(Math::INV_PI) * diffuseAlbedo();

    return ScatterEvent::DIFFUSE_REFL;
  }
  else
  {
    return ScatterEvent::ABSORPTION;
  }
}



ScatterEvent::SCATTER_EVT MeasuredIsotropic::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    float             e1,
    float             e2,
    mrf::math::Vec3f &outgoing_dir,
    RADIANCE_TYPE &   brdf_corrected_by_pdf) const
{
  if (e1 < diffuseAlbedo())   // TOTAL ALBEDO = DIFFUSE ALBEDO FOR LAMBERTIAN SURFACE
  {
    //We use a pdf proportional to the cosine
    float const e3 = e1 / diffuseAlbedo();
    outgoing_dir   = randomHemiCosDirection(lf, e2, e3);
    //proba_outgoing_dir = Math::INV_PI * diffuseAlbedo();

    // Since the BRDF / pdf = 1.0, we just return the _color (which is normalized)
    float const dot_NL = outgoing_dir.dot(lf.normal());
    if (dot_NL <= 0.0f)
    {
      brdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
      return ScatterEvent::ABSORPTION;
    }

    brdf_corrected_by_pdf = 0.f;   // _color / dot_NL;

    return ScatterEvent::DIFFUSE_REFL;
  }
  return ScatterEvent::ABSORPTION;
}

}   // namespace materials
}   // namespace mrf
