#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <mrf_core/materials/materials.hpp>


#include <mrf_plugins/materials/microfacet/conductor/perfect_mirror.hpp>
#include <mrf_plugins/materials/microfacet/conductor/fresnel_mirror.hpp>
#include <mrf_plugins/materials/microfacet/conductor/ggx.hpp>
#include <mrf_plugins/materials/microfacet/conductor/aniso_ggx.hpp>
#include <mrf_plugins/materials/microfacet/dielectric/fresnel_glass.hpp>
#include <mrf_plugins/materials/microfacet/dielectric/walter_bsdf.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
class GGXParser: public BaseMaterialParser<mrf::materials::GGX>
{
public:
  GGXParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new GGXParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::GGX; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::GGX *ggx_mat = dynamic_cast<mrf::materials::GGX *>(a_mat);

    // Fresnel definition: Air by default
    mrf::materials::COLOR eta(1.0f);
    mrf::materials::COLOR k(0.0f);

    //Fresnel
    parser.retrieveFresnel(a_mat_element, eta, k);

    bool  isotropicGGX = true;
    float alpha_g      = 0.35f;
    int   valueOK      = a_mat_element->QueryFloatAttribute(SceneLexer::ALPHA_G_AT, &alpha_g);

    float alpha_x = alpha_g, alpha_y = alpha_g;
    if (valueOK != tinyxml2::XML_SUCCESS)
    {
      valueOK      = a_mat_element->QueryFloatAttribute(SceneLexer::ALPHA_X_AT, &alpha_x);
      int valueOK2 = a_mat_element->QueryFloatAttribute(SceneLexer::ALPHA_Y_AT, &alpha_y);

      if ((valueOK == tinyxml2::XML_SUCCESS) && (valueOK2 == tinyxml2::XML_SUCCESS))
      {
        isotropicGGX = false;
      }
    }

    if (isotropicGGX)
    {
      ggx_mat->setAlpha(alpha_g);
      ggx_mat->setFresnel(eta, k);
    }
    else
    {
      ggx_mat = nullptr;
      return false;
    }
    return true;
  }

protected:
private:
};

class WalterBSDFParser: public BaseMaterialParser<mrf::materials::WalterBSDF>
{
public:
  WalterBSDFParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new WalterBSDFParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::WALTER_BSDF; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::WalterBSDF *walter_mat = dynamic_cast<mrf::materials::WalterBSDF *>(a_mat);

    // Fresnel definition: Air by default
    mrf::materials::COLOR eta(1.0f);
    mrf::materials::COLOR k(0.0f);

    //Fresnel
    parser.retrieveFresnel(a_mat_element, eta, k);

    bool  isotropicGGX = true;
    float alpha_g      = 0.35f;
    int   valueOK      = a_mat_element->QueryFloatAttribute(SceneLexer::ALPHA_G_AT, &alpha_g);

    float alpha_x = alpha_g, alpha_y = alpha_g;
    if (valueOK != tinyxml2::XML_SUCCESS)
    {
      valueOK      = a_mat_element->QueryFloatAttribute(SceneLexer::ALPHA_X_AT, &alpha_x);
      int valueOK2 = a_mat_element->QueryFloatAttribute(SceneLexer::ALPHA_Y_AT, &alpha_y);

      if ((valueOK == tinyxml2::XML_SUCCESS) && (valueOK2 == tinyxml2::XML_SUCCESS))
      {
        isotropicGGX = false;
      }
    }

    tinyxml2::XMLElement *extinction_elem = a_mat_element->FirstChildElement(SceneLexer::EXTINCTION_CMP_MK);
    mrf::materials::COLOR extinction(0.f);
    if (extinction_elem) extinction = parser.retrieveColor(extinction_elem);
    walter_mat->setExtinction(extinction);

    if (isotropicGGX)
    {
      walter_mat->microfacet()->setAlpha(alpha_g);
      walter_mat->microfacet()->setFresnel(eta, k);
    }
    else
    {
      walter_mat = nullptr;
      return false;
    }
    return true;
  }

protected:
private:
};

class PerfectMirrorParser: public BaseMaterialParser<mrf::materials::PerfectMirror>
{
public:
  PerfectMirrorParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new PerfectMirrorParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::PERFECT_MIRROR; }

  virtual bool parse(tinyxml2::XMLElement *, mrf::materials::UMat *a_mat, ParserHelper &)
  {
    mrf::materials::PerfectMirror *perfect_mirror_mat = dynamic_cast<mrf::materials::PerfectMirror *>(a_mat);

    return perfect_mirror_mat != nullptr;
  }

protected:
private:
};

class FresnelMirrorParser: public BaseMaterialParser<mrf::materials::FresnelMirror>
{
public:
  FresnelMirrorParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new FresnelMirrorParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::FRESNEL_MIRROR; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::FresnelMirror *fresnel_mirror_mat = dynamic_cast<mrf::materials::FresnelMirror *>(a_mat);

    if (!fresnel_mirror_mat) return false;

    mrf::materials::COLOR eta(1.0f);
    mrf::materials::COLOR k(0.0f);

    parser.retrieveFresnel(a_mat_element, eta, k);
    fresnel_mirror_mat->setFresnel(eta, k);
    return true;
  }

protected:
private:
};

class FresnelGlassParser: public BaseMaterialParser<mrf::materials::FresnelGlass>
{
public:
  FresnelGlassParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new FresnelGlassParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::FRESNEL_GLASS; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::FresnelGlass *fresnel_glass_mat = dynamic_cast<mrf::materials::FresnelGlass *>(a_mat);

    if (!fresnel_glass_mat) return false;

    mrf::materials::COLOR eta(1.0f);
    mrf::materials::COLOR k(0.0f);

    parser.retrieveFresnel(a_mat_element, eta, k);
    fresnel_glass_mat->setFresnel(eta, k);

    tinyxml2::XMLElement *extinction_elem = a_mat_element->FirstChildElement(SceneLexer::EXTINCTION_CMP_MK);
    mrf::materials::COLOR extinction(0.f);
    if (extinction_elem) extinction = parser.retrieveColor(extinction_elem);
    fresnel_glass_mat->setExtinction(extinction);

    bool thin_glass = false;
    a_mat_element->QueryBoolAttribute(SceneLexer::THIN_GLASS_AT, &thin_glass);
    if (thin_glass)
      fresnel_glass_mat->defineAsThin();
    else
      fresnel_glass_mat->defineAsThick();

    return true;
  }

protected:
private:
};

}   // namespace io
}   // namespace mrf
