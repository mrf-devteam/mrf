/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/microfacet/dielectric/backend/optix/dielectric_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *MicrofacetDielectricPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *MicrofacetDielectricPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  optix::Material optix_mat       = context->createMaterial();
  auto            compile_options = cuda_compile_options;

  auto fresnel_glass = dynamic_cast<mrf::materials::FresnelGlass *>(material);
  if (fresnel_glass)
  {
    if (fresnel_glass->thinGlass())   //Thickness of 1 for now
    {
      optix_mat["_thickness"]->setFloat(1.f);
    }
    else
    {
      optix_mat["_thickness"]->setFloat(0.f);
    }

    optix_mat["_alpha_g"]->setFloat(0.f);   //Fresnel glass is smooth glass
    optix_mat["_sqr_alpha_g"]->setFloat(0.f);
    optix_mat["_distribution"]->setInt(0);   //HARDCODED FOR NOW: 0 = GGX distribution.
    optix_mat["_is_conductor"]->setInt(0);   //HARDCODED FOR NOW: 0 = false.

    compile_options.push_back("-DMICROFACET_DIELECTRIC");


    OptixMicrofacetDielectric *ret_mat
        = new OptixMicrofacetDielectric(fresnel_glass, optix_mat, compile_options, ptx_cfg);

    std::vector<std::string> variable_names = {"_eta", "_kappa", "_extinction"};

    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(fresnel_glass->eta());
    variable_values.push_back(fresnel_glass->kappa());
    variable_values.push_back(fresnel_glass->extinction());

#ifdef MRF_RENDERING_MODE_SPECTRAL
    variable_names.push_back("_inv_transmittance_PDF");
    variable_values.push_back(fresnel_glass->getInvTransPDF(wavelengths));
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->updateTextures(context, wavelengths);

    if (ret_mat->hasTexture("cdf"))
    {
      int id = ret_mat->getTexture("cdf")->getId();
      ret_mat->getMat()["_trans_cdf_tex_id"]->setInt(id);
    }
    else
    {
      ret_mat->getMat()["_trans_cdf_tex_id"]->setInt(-1);
    }
#endif

    return ret_mat;
  }

  auto walter_glass = dynamic_cast<mrf::materials::WalterBSDF *>(material);
  if (walter_glass)
  {
    float alpha_g = walter_glass->alpha_g();
    optix_mat["_alpha_g"]->setFloat(alpha_g);   //Fresnel glass is smooth glass
    optix_mat["_sqr_alpha_g"]->setFloat(alpha_g * alpha_g);
    optix_mat["_distribution"]->setInt(0);   //HARDCODED FOR NOW: 0 = GGX distribution.
    optix_mat["_thickness"]->setFloat(0.f);

    compile_options.push_back("-DMICROFACET_DIELECTRIC");


    OptixMicrofacetDielectric *ret_mat
        = new OptixMicrofacetDielectric(walter_glass, optix_mat, compile_options, ptx_cfg);

    std::vector<std::string> variable_names = {"_eta", "_kappa", "_extinction"};

    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(walter_glass->eta());
    variable_values.push_back(walter_glass->k());
    variable_values.push_back(walter_glass->extinction());

#ifdef MRF_RENDERING_MODE_SPECTRAL
    variable_names.push_back("_inv_transmittance_PDF");
    variable_values.push_back(walter_glass->getInvTransPDF(wavelengths));
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->updateTextures(context, wavelengths);

    if (ret_mat->hasTexture("cdf"))
    {
      int id = ret_mat->getTexture("cdf")->getId();
      optix_mat["_trans_cdf_tex_id"]->setInt(id);
    }
    else
    {
      optix_mat["_trans_cdf_tex_id"]->setInt(-1);
    }
#endif

    return ret_mat;
  }

  return nullptr;
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixMicrofacetDielectric::update(optix::Context &context, std::vector<uint> wavelengths)
#else
void OptixMicrofacetDielectric::update(optix::Context &context)
#endif
{
  //_loger.trace("UPDATE MATERIALS");
  //mrf::util::PrecisionTimer all_mat_timer;
  //all_mat_timer.start();

#ifdef MRF_RENDERING_MODE_SPECTRAL
  //This if optimize the update function, no update is needed when wavelengths.size==1 because it means
  //that muiltiplexing was not activated
  if (wavelengths.size() > 1)
  {
    auto walter = dynamic_cast<mrf::materials::WalterBSDF *>(_mrf_material);
    if (walter)
    {
#  ifdef MRF_RENDERING_MODE_SPECTRAL
      _variable_values[3] = walter->getInvTransPDF(wavelengths);
#  endif

      updateRadianceGPUBuffer(context, _variable_values, _variable_names, wavelengths);
    }

    auto fresnel_glass = dynamic_cast<mrf::materials::FresnelGlass *>(_mrf_material);
    if (fresnel_glass)
    {
#  ifdef MRF_RENDERING_MODE_SPECTRAL
      _variable_values[3] = fresnel_glass->getInvTransPDF(wavelengths);
#  endif

      updateRadianceGPUBuffer(context, _variable_values, _variable_names, wavelengths);
    }
  }
  else
  {
    OptixBSDFMaterial::update(context, wavelengths);
  }
#else
  OptixBSDFMaterial::update(context);
#endif
  //float elapsed = all_mat_timer.elapsed();
  //_loger.trace("    Update Materials took: " + std::to_string(elapsed) + "s");
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixMicrofacetDielectric::updateTextures(optix::Context &context, std::vector<uint> wavelengths)
{
  //mrf::util::PrecisionTimer timer;
  //timer.start();

  optix::TextureSampler tex_sampler;


  auto fresnel_glass = dynamic_cast<mrf::materials::FresnelGlass *>(_mrf_material);
  if (fresnel_glass)
  {
    if (!fresnel_glass->initTransCDF(wavelengths))
    {
      //timer.stop();
      //_loger.trace("Failed to init CDF: either monochromatic or uniform (CDF useless) or unknown issue.");
      return;
    }

    if (_textures.find("cdf") == _textures.end())
    {
      tex_sampler = mrf::optix_backend::createClampedOptixTexture(
          context,
          RT_FORMAT_FLOAT,
          fresnel_glass->getTransCDF(wavelengths).size());
      _textures["cdf"] = tex_sampler;
    }
    else
    {
      tex_sampler = _textures["cdf"];
    }

    mrf::optix_backend::update1DTextureFromData(fresnel_glass->getTransCDF(wavelengths), tex_sampler);
  }

  auto walter_glass = dynamic_cast<mrf::materials::WalterBSDF *>(_mrf_material);
  if (walter_glass)
  {
    if (!walter_glass->initTransCDF(wavelengths))
    {
      //timer.stop();
      //_loger.trace("Failed to init CDF: either monochromatic or uniform (CDF useless) or unknown issue.");
      return;
    }

    if (_textures.find("cdf") == _textures.end())
    {
      tex_sampler = mrf::optix_backend::createClampedOptixTexture(
          context,
          RT_FORMAT_FLOAT,
          walter_glass->getTransCDF(wavelengths).size());
      _textures["cdf"] = tex_sampler;
    }
    else
    {
      tex_sampler = _textures["cdf"];
    }

    mrf::optix_backend::update1DTextureFromData(walter_glass->getTransCDF(wavelengths), tex_sampler);
  }



  //float elapsed = timer.elapsed();
  //_loger.info("Updated Glass CDF in: " + std::to_string(elapsed) + "s");
}
#endif


}   // namespace optix_backend
}   // namespace mrf
