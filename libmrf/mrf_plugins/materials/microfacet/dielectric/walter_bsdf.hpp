/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 * Walter Microfacet BSDF
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//MRF
#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/materials/brdf.hpp>
#include <mrf_core/materials/bsdf.hpp>
#include <mrf_core/materials/fresnel.hpp>
#include <mrf_core/geometry/local_frame.hpp>

#include <mrf_plugins/materials/microfacet/conductor/ggx.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *WALTER_BSDF = "walter_bsdf";   // Walter BSDF material
};                                                // namespace BRDFTypes

/**
 * @brief      This class implements the BSDF material as defined by Walter 2007.
 *             This type of materials are rough materials that reflects (like a BRDF)
 *             and refracts light.
 *
 *             Note that Walter derives the model for Dielectrics but this model should work
 *             for conductor as well as long as the Fresnel evaluation is correct
 *
 *             Also Note that this type of materials do not absorb light unless the generated light
 *             direction is too grazing.
 *
 *
 * @tparam     MicroBRDF_TYPE  The type of underlying BRDF that represents
 *             the reflective part of the material. This class should implement
 *             the Microfacet methods (distribution, fresnel and geometric terms evaluation)
 */
template<class MicroBRDF_TYPE>
class _WalterBSDF: public BSDF
{
private:
  MicroBRDF_TYPE *_microfacet;

#ifdef MRF_RENDERING_MODE_SPECTRAL
  std::vector<float>    _trans_CDF;
  mrf::materials::COLOR _inv_trans_PDF;
#endif

  mrf::materials::COLOR _extinction;

public:
  /**
   * @brief      Constructor for Isotropic BSDF
   *
   * @param      name  The name of the material
   */
  _WalterBSDF(std::string const &name);

  /**
   * @brief      Constructor for Isotropic BSDF
   *
   * @param      name  The name of the material
   */
  _WalterBSDF(
      std::string const &  name,
      float                roughness,
      RADIANCE_TYPE const &eta,
      RADIANCE_TYPE const &kappa,
      RADIANCE_TYPE const &extinction);

  /**
   * @brief      Constructor for Anisotropic BSDF
   *
   * @param      name         The name of the material
   * @param[in]  roughness_x  The roughness along the tangent direction
   * @param[in]  roughness_y  The roughness along the bitangent direction
   * @param      eta          The real part of the Index of Refraction
   * @param      kappa        The imaginary part of the Index of Refraction
   */
  _WalterBSDF(
      std::string const &  name,
      float                roughness_x,
      float                roughness_y,
      RADIANCE_TYPE const &eta,
      RADIANCE_TYPE const &kappa,
      RADIANCE_TYPE const &extinction);

  virtual ~_WalterBSDF();

  virtual mrf::materials::IOR ior() const;
  virtual RADIANCE_TYPE       eta() const;
  virtual RADIANCE_TYPE       k() const;
  virtual RADIANCE_TYPE       extinction() const;
  virtual float               alpha_g() const;

  void setExtinction(RADIANCE_TYPE ext) { _extinction = ext; }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual std::vector<float>  getTransCDF(std::vector<uint> wavelengths);
  mrf::materials::COLOR const getInvTransPDF(std::vector<uint> wavelengths);
  virtual bool                initTransCDF(std::vector<uint> wavelengths);
#endif

  // The missing methods of the UMat interface are implemented below
  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;


  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;


  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBSDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              bsdf_corrected_by_pdf,
      mrf::materials::IOR const &  in_ior,
      mrf::materials::IOR const &  out_ior) const;

  //Weak Microfacet Interface implementation
  inline float distribution(float cos_theta_h, float sqr_sin_phi_h, float sqr_cos_phi_h) const;

  inline float geometricTerm(
      mrf::geom::LocalFrame const &lf,
      float                        dot_HV,
      mrf::math::Vec3f const &     view_dir,
      mrf::math::Vec3f const &     light_dir,
      float                        dot_NL,
      float                        dot_NV) const;

  inline mrf::math::Vec3f sampleHalfDir(mrf::geom::LocalFrame const &lf, float e1, float e2) const;

  MicroBRDF_TYPE *microfacet() { return _microfacet; }

private:
  // True if transmission and false if TOTAL Internal Reflection
  bool tDir(
      mrf::math::Vec3f const &   V,   // The View Vector pointing away from the Macro-Geometry
      mrf::math::Vec3f const &   H,   // Half vector pointing toward the lenss dense medium
      float                      dot_VH,
      mrf::materials::IOR const &in_ior,
      mrf::materials::IOR const &out_ior,
      float const                dot_NV,
      mrf::math::Vec3f &         transmitted_dir) const;
};


template<class MicroBRDF_TYPE>
_WalterBSDF<MicroBRDF_TYPE>::_WalterBSDF(std::string const &name)
  : BSDF(name, BRDFTypes::WALTER_BSDF)
  , _microfacet(new MicroBRDF_TYPE(name + "_walter_isotropic_bsdf"))
  , _extinction(RADIANCE_TYPE(0.f))
{}

template<class MicroBRDF_TYPE>
_WalterBSDF<MicroBRDF_TYPE>::_WalterBSDF(
    std::string const &  name,
    float                roughness,
    RADIANCE_TYPE const &eta,
    RADIANCE_TYPE const &kappa,
    RADIANCE_TYPE const &extinction)
  : BSDF(name, BRDFTypes::WALTER_BSDF)
  , _microfacet(new MicroBRDF_TYPE(name + "_walter_isotropic_bsdf", roughness, eta, kappa))
  , _extinction(extinction)
{}


template<class MicroBRDF_TYPE>
_WalterBSDF<MicroBRDF_TYPE>::_WalterBSDF(
    std::string const &  name,
    float                roughness_x,
    float                roughness_y,
    RADIANCE_TYPE const &eta,
    RADIANCE_TYPE const &kappa,
    RADIANCE_TYPE const &extinction)
  : BSDF(name, BRDFTypes::WALTER_BSDF)
  , _microfacet(new MicroBRDF_TYPE(name + "_walter_anisotropic_bsdf", roughness_x, roughness_y, eta, kappa))
  , _extinction(extinction)
{}

template<class MicroBRDF_TYPE>
_WalterBSDF<MicroBRDF_TYPE>::~_WalterBSDF()
{
  delete _microfacet;
}

template<class MicroBRDF_TYPE>
mrf::materials::IOR _WalterBSDF<MicroBRDF_TYPE>::ior() const
{
  return _microfacet->ior();
}

template<class MicroBRDF_TYPE>
RADIANCE_TYPE _WalterBSDF<MicroBRDF_TYPE>::eta() const
{
  return _microfacet->eta();
}

template<class MicroBRDF_TYPE>
RADIANCE_TYPE _WalterBSDF<MicroBRDF_TYPE>::k() const
{
  return _microfacet->k();
}

template<class MicroBRDF_TYPE>
RADIANCE_TYPE _WalterBSDF<MicroBRDF_TYPE>::extinction() const
{
  return _extinction;
}

template<class MicroBRDF_TYPE>
float _WalterBSDF<MicroBRDF_TYPE>::alpha_g() const
{
  return _microfacet->alpha_g();
}



//------------------------------------------------------------------------------------------------
// Implementation of the UMat interface
//------------------------------------------------------------------------------------------------
template<class MicroBRDF_TYPE>
RADIANCE_TYPE _WalterBSDF<MicroBRDF_TYPE>::coloredBRDFValue(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const &     out_dir) const
{
  // THE BRDF part of the BSDF is completely managed by MicroBRDF_TYPE
  return _microfacet->coloredBRDFValue(lf, in_dir, out_dir);
}


template<class MicroBRDF_TYPE>
ScatterEvent::SCATTER_EVT _WalterBSDF<MicroBRDF_TYPE>::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    float                        e1,
    float                        e2,
    mrf::math::Vec3f &           outgoing_dir,
    RADIANCE_TYPE &              brdf_corrected_by_pdf) const
{
  // THE BRDF part of the BSDF is completely managed by MicroBRDF_TYPE
  return _microfacet->scatteringDirectionWithBRDF(lf, in_dir, e1, e2, outgoing_dir, brdf_corrected_by_pdf);
}


template<class MicroBRDF_TYPE>
bool _WalterBSDF<MicroBRDF_TYPE>::tDir(
    mrf::math::Vec3f const &   V,   // The View Vector pointing away from the Macro-Geometry
    mrf::math::Vec3f const &   H,   // Half vector pointing toward the lenss dense medium
    float                      dot_VH,
    mrf::materials::IOR const &in_ior,
    mrf::materials::IOR const &out_ior,
    float                      dot_NV,   // NO CLAMPING
    mrf::math::Vec3f &         transmitted_dir) const
{
  using namespace mrf::math;
  using namespace mrf::geom;

  float const eta_ratio     = in_ior.eta().avgCmp() / out_ior.eta().avgCmp();
  float const sqr_eta_ratio = eta_ratio * eta_ratio;
  //dot_VH = V.dot(H);
  float const sqr_dot_VH = dot_VH * dot_VH;


  float const TIR_TEST = 1.f + (sqr_eta_ratio * (sqr_dot_VH - 1.f));

  if (TIR_TEST < 0.0)
  {
    //std::cout << "TIR_TEST = " << TIR_TEST << std::endl;
    return false;   // NO TRANSMISSION
  }
  float const sign_dot_NV = Math::sign(dot_NV);

  transmitted_dir = (eta_ratio * dot_VH - sign_dot_NV * std::sqrt(TIR_TEST)) * H - eta_ratio * V;

  //Transmission if possible
  return true;
}


// TODO:  Generalize this for conductor as well
// TODO: Refactor this to improve speed?

// 1: Sample   the Half direction for Reflection
// 2: Evaluate the Fresnel Term for dot_HV = cos_theta_diff
// 3: Choose according to Fresnel Term if the material reflects or refracts
// 4: Compute outgoing_dir according to scattering event (reflection or refraction)
// 5: Evaluate bsdf_corrected_by_pdf accordingly
// 6: Return the scattering event

template<class MicroBRDF_TYPE>
ScatterEvent::SCATTER_EVT _WalterBSDF<MicroBRDF_TYPE>::scatteringDirectionWithBSDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    float                        e1,
    float                        e2,
    mrf::math::Vec3f &           outgoing_dir,
    RADIANCE_TYPE &              bsdf_corrected_by_pdf,
    mrf::materials::IOR const &  in_ior,
    mrf::materials::IOR const &  out_ior) const
{
  using namespace mrf::math;
  using namespace mrf::sampling;
  using namespace mrf::geom;


  Vec3f const V      = -in_dir;   // View direction in global space
  float const dot_NV = V.dot(lf.normal());

  if (dot_NV >= -EPSILON && dot_NV <= EPSILON)   // TOO GRAZING. doest not matter if are inside or outside
  {
    return ScatterEvent::ABSORPTION;
  }

  Vec3f const H_in_global_space = _microfacet->sampleHalfDir(
      lf,
      e1,
      e2);   // H is sampled according to the local Frame. Normals are always outside in MRF
  float const dot_NH = clamp(H_in_global_space.dot(lf.normal()), 0.0f, 1.0f);

#ifdef WALTERBSDF_DEBUG
  if (dot_NH < -EPSILON)
  {
    std::cout << " EPSILON " << EPSILON << std::endl;
    std::cout << " lf = " << lf << std::endl;
    std::cout << " V = " << V << ";  dot_NH =  " << dot_NH << " ;  e1 = " << e1 << " e2 = " << e2 << std::endl;
  }
#endif

  assert(dot_NH >= -EPSILON);

  if (dot_NH >= -EPSILON && dot_NH <= EPSILON)   // H is too grazing. Avoiding division by zero
  {
    bsdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
    return ScatterEvent::ABSORPTION;
  }

  float const dot_HV = H_in_global_space.dot(V);
  if (dot_HV >= -EPSILON
      && dot_HV <= EPSILON)   //  TOO grazing as well and BRDF estimator is going to be equal to zero anyway
  {
    bsdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
    return ScatterEvent::ABSORPTION;
  }

  //float const abs_dot_HV = std::abs( dot_HV );


  if (dot_NV > EPSILON)   // FRONT-FACING we are outside the medium
  {
#ifdef WALTERBSDF_DEBUG
    std::cout << __FILE__ << " " << __LINE__ << " FRONT FACING " << std::endl;
#endif


    if (dot_HV < -EPSILON)   // This Microfacet is not seen frm current direction Geometric Term equals Zero
    // kill the Ray now
    {
      bsdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
      return ScatterEvent::ABSORPTION;
    }

    assert(dot_HV >= EPSILON);

    RADIANCE_TYPE fresnel_term = _microfacet->fresnelTerm(dot_HV);

    // Russian Roulette to choose between Reflection and Refraction
    float const e3 = RandomGenerator::Instance().getFloat();

    if (e3 < fresnel_term.avgCmp())   // REFLECTION without penetrating the medium
    {
      //NEW OUTGOING DIR [REFLECTION]:  in World coordinates
      outgoing_dir = in_dir + 2.0f * dot_HV * H_in_global_space;

      float const dot_NL = clamp(outgoing_dir.dot(lf.normal()), 0.0f, 1.0f);

      if (dot_NL < EPSILON)   // TOO GRAZING. Avoid division by zero. BRDF*cosine is going to be equal to zero anyway
      {
        bsdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
        return ScatterEvent::ABSORPTION;
      }

      bsdf_corrected_by_pdf = dot_HV * _microfacet->geometricTerm(lf, dot_HV, V, outgoing_dir, dot_NL, dot_NV)
                              / (dot_NV * dot_NL * dot_NH);

#ifdef MRF_RENDERING_MODE_SPECTRAL
#else
      assert(bsdf_corrected_by_pdf.isPositive());
      assert(bsdf_corrected_by_pdf.isFinite());
#endif

      return ScatterEvent::ANY_REFL;
    }
    else   //TRANSMISSION inside the medium: GOING INSIDE THE MEDIUM
    {
      Vec3f trans_dir;
      bool  transmission_is_possible = tDir(V, H_in_global_space, dot_HV, in_ior, out_ior, dot_NV, trans_dir);
      if (transmission_is_possible == false)
      {
        std::cout << " lf = " << lf << std::endl;
        std::cout << " V = " << V << " H in globla space" << H_in_global_space << " dot-HV = " << dot_HV
                  << " in_ior = " << in_ior.eta() << " out-ior= " << out_ior.eta() << " dot_NV = " << dot_NV
                  << " trans_dir = " << trans_dir << std::endl;
        assert(0);
      }
      assert(transmission_is_possible);   // NO TIR CAN HAPPEN IN THIS CASE

      // dot_NL in the correct direction
      float const inv_dot_NL = trans_dir.dot(-lf.normal());
      assert(inv_dot_NL >= -EPSILON);

      if (inv_dot_NL <= EPSILON)   // TOO GRAZING.
      {
        bsdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
        return ScatterEvent::ABSORPTION;
      }



      bsdf_corrected_by_pdf = dot_HV * _microfacet->geometricTerm(lf, dot_HV, V, trans_dir, inv_dot_NL, dot_NV)
                              / (dot_NV * inv_dot_NL * dot_NH);
      outgoing_dir = trans_dir;

#ifdef MRF_RENDERING_MODE_SPECTRAL
#else
      assert(bsdf_corrected_by_pdf.isPositive());
      assert(bsdf_corrected_by_pdf.isFinite());
#endif

#ifdef WALTERBSDF_DEBUG
      std::cout << "WALTERBSDF returning Front Tranmission event " << std::endl;
#endif

      return ScatterEvent::TRANSMISSION_FRONT;
    }
  }
  else   // BACK-FACING we are inside the medium  dot_NV is negative
  {
#ifdef WALTERBSDF_DEBUG
    std::cout << __FILE__ << " " << __LINE__ << " BACK  BAKC BACK  " << std::endl;
#endif

    float const inv_dot_HV = -dot_HV;   //inv_dot_HV should be positive
    if (inv_dot_HV < -EPSILON)          // This Microfacet is not seen frm current direction Geometric Term equals Zero
    // kill the Ray now
    {
      bsdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
      return ScatterEvent::ABSORPTION;
    }

    float const inv_dot_NV = -dot_NV;
    assert(inv_dot_NV >= EPSILON);   // should be positive. Back-facing

    Vec3f trans_dir;
    bool  transmission_is_possible = tDir(V, H_in_global_space, inv_dot_HV, in_ior, out_ior, inv_dot_NV, trans_dir);


    RADIANCE_TYPE fresnel_term = _microfacet->fresnelTerm(inv_dot_HV);

    float const e3 = RandomGenerator::Instance().getFloat();
    if (e3 < fresnel_term.avgCmp() || !transmission_is_possible)   // REFLECTION OR TIR
    {
      outgoing_dir
          = in_dir + 2.0f * inv_dot_HV * (-H_in_global_space);   // We should inverse shince H  is on the other side

#ifdef WALTERBSDF_DEBUG
      std::cout << __FILE__ << " " << __LINE__ << " REFLECTION  " << std::endl;
      std::cout << " outgoing_dir.length() = " << outgoing_dir.length() << std::endl;
#endif

      assert(outgoing_dir.dot(-H_in_global_space) > -EPSILON);


      float const dot_NL = outgoing_dir.dot(-lf.normal());
      if (dot_NL < EPSILON)   // TOO GRAZING
      {
        bsdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
        return ScatterEvent::ABSORPTION;
      }

      bsdf_corrected_by_pdf = inv_dot_HV
                              * _microfacet->geometricTerm(lf, inv_dot_HV, V, outgoing_dir, dot_NL, inv_dot_NV)
                              / (inv_dot_NV * dot_NL * dot_NH);

#ifdef MRF_RENDERING_MODE_SPECTRAL
#else
      assert(bsdf_corrected_by_pdf.isPositive());
      assert(bsdf_corrected_by_pdf.isFinite());
#endif

      if (!transmission_is_possible)
      {
#ifdef WALTERBSDF_DEBUG
        std::cout << "WALTERBSDF returning TIR " << std::endl;
#endif
        return ScatterEvent::TIR;
      }
      else
      {
#ifdef WALTERBSDF_DEBUG
        std::cout << "WALTERBSDF returning Internal Reflection " << std::endl;
#endif

        return ScatterEvent::INTERNAL_REFL;
      }
    }
    else   // TRANSMISSION BACK LEAVING THE MEIDUM
    {
#ifdef WALTERBSDF_DEBUG
      std::cout << __FILE__ << " " << __LINE__ << " ENTERING BACK TRANSMISSION  " << std::endl;
      std::cout << " trans_dir.length() = " << trans_dir.length() << std::endl;
#endif

      //Normalize the transmitted direction.
      trans_dir.normalize();

      assert(
          trans_dir.dot(H_in_global_space) >= -EPSILON);   //This should be positive because we are leaving the meidum

      float const dot_NL = trans_dir.dot(lf.normal());
      assert(dot_NL >= -EPSILON);

      if (dot_NL >= -EPSILON && dot_NL <= EPSILON)   // Transmitted direction is too grazing
      {
        bsdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
        return ScatterEvent::ABSORPTION;
      }


      bsdf_corrected_by_pdf = inv_dot_HV * _microfacet->geometricTerm(lf, inv_dot_HV, V, trans_dir, dot_NL, inv_dot_NV)
                              / (inv_dot_NV * dot_NL * dot_NH);


#ifdef WALTERBSDF_DEBUG
      if (!bsdf_corrected_by_pdf.isPositive())
      {
        std::cout << " bsdf_corrected_by_pdf =  " << bsdf_corrected_by_pdf << std::endl;
      }
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
#else
      assert(bsdf_corrected_by_pdf.isPositive());
      assert(bsdf_corrected_by_pdf.isFinite());
#endif

      outgoing_dir = trans_dir;
      return ScatterEvent::TRANSMISSION_BACK;
    }

  }   //end of else back-facing case

}   //end of scatteringDirectionWithBSDF(...) method


//
// Weak Microfacet Interface
template<class MicroBRDF_TYPE>
inline float
_WalterBSDF<MicroBRDF_TYPE>::distribution(float cos_theta_h, float sqr_sin_phi_h, float sqr_cos_phi_h) const
{
  return _microfacet->distribution(cos_theta_h, sqr_sin_phi_h, sqr_cos_phi_h);
}


template<class MicroBRDF_TYPE>
inline float _WalterBSDF<MicroBRDF_TYPE>::geometricTerm(
    mrf::geom::LocalFrame const &lf,
    float                        dot_HV,
    mrf::math::Vec3f const &     view_dir,
    mrf::math::Vec3f const &     light_dir,
    float                        dot_NL,
    float                        dot_NV) const
{
  return _microfacet->geometricTerm(lf, dot_HV, view_dir, light_dir, dot_NL, dot_NV);
}

template<class MicroBRDF_TYPE>
inline mrf::math::Vec3f
_WalterBSDF<MicroBRDF_TYPE>::sampleHalfDir(mrf::geom::LocalFrame const &lf, float e1, float e2) const
{
  return _microfacet->sampleHalfDir(lf, e1, e2);
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
template<class MicroBRDF_TYPE>
std::vector<float> _WalterBSDF<MicroBRDF_TYPE>::getTransCDF(std::vector<uint> wavelengths)
{
  if (_trans_CDF.size() == 0) initTransCDF(wavelengths);
  return _trans_CDF;
}

template<class MicroBRDF_TYPE>
mrf::materials::RADIANCE_TYPE const _WalterBSDF<MicroBRDF_TYPE>::getInvTransPDF(std::vector<uint> wavelengths)
{
  if (_inv_trans_PDF.size() == 0) initTransCDF(wavelengths);
  return _inv_trans_PDF;
}

template<class MicroBRDF_TYPE>
bool _WalterBSDF<MicroBRDF_TYPE>::initTransCDF(std::vector<uint> wavelengths)
{
  if (_trans_CDF.size() > 0) return true;

  uint first = wavelengths[0];
  uint last  = wavelengths[wavelengths.size() - 1];
  if (last == first) return false;   //Monochromatic, no need for a CDF.
  uint nb_bin                = (uint)wavelengths.size();
  uint nb_wavelength_per_bin = (last - first) / ((uint)wavelengths.size() - 1);
  uint step                  = 1;   //1 nm resolution for the CDF

  //First get extinction on a 1nm basis.
  std::vector<float> extinction;
  float              variations   = 0.f;
  float              tmp_sigma    = k().findLinearyInterpolatedValue((float)first) * 4.f * 3.141592f / (float)first;
  float              tmp_user_ext = _extinction.findLinearyInterpolatedValue((float)first);
  float              tmp_ext      = exp(-tmp_sigma) * (1.f - tmp_user_ext);
  extinction.push_back(tmp_ext);
  int count = 1;
  while (first < last)
  {
    tmp_sigma    = k().findLinearyInterpolatedValue((float)first) * 4.f * 3.141592f / (float)first;
    tmp_user_ext = _extinction.findLinearyInterpolatedValue((float)first);
    tmp_ext      = exp(-tmp_sigma) * (1.f - tmp_user_ext);
    extinction.push_back(tmp_ext);
    variations += abs(extinction[count] - extinction[count - 1]);
    first += step;
    ++count;
  }

  if (variations < 0.1f) return false;   //Function is nearly uniform, no need to importance sample.

  //Compute CDF on a per bin basis
  float int_trans_PDF = 0.f;
  for (uint i = 0; i < nb_bin - 1; ++i)
  {
    float bin_transmittance = 0.f;
    for (uint j = 0; j < nb_wavelength_per_bin; ++j)
    {
      bin_transmittance += extinction[i * nb_wavelength_per_bin + j];
      _trans_CDF.push_back(int_trans_PDF + bin_transmittance);
    }
    int_trans_PDF += bin_transmittance;

    _inv_trans_PDF.add((float)wavelengths[i], bin_transmittance);
  }

  //Normalize CDF
  for (auto it = _trans_CDF.begin(); it < _trans_CDF.end(); ++it)
  {
    *it /= int_trans_PDF;
  }

  //Normalize and inverse PDF
  for (uint i = 0; i < nb_bin - 1; ++i)
  {
    if (_inv_trans_PDF[i] <= 0.00001f)
      _inv_trans_PDF[i] = 0.f;
    else
    {
      _inv_trans_PDF[i] = int_trans_PDF / _inv_trans_PDF[i];
    }
  }

  return (_trans_CDF.size() > 0 && _inv_trans_PDF.size() > 0);
}
#endif

typedef MRF_PLUGIN_EXPORT _WalterBSDF<GGX> WalterBSDF;

}   // namespace materials
}   // namespace mrf