/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2018
 *
 * Fresnel Glass BSDF
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//MRF
#include <mrf_core/math/math.hpp>
#include <mrf_core/materials/materials.hpp>
#include <mrf_core/materials/brdf.hpp>
#include <mrf_core/materials/bsdf.hpp>
#include <mrf_core/materials/fresnel.hpp>
#include <mrf_core/geometry/local_frame.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *FRESNEL_GLASS = "FresnelGlass";   // Fresnel Glass (no roughness)  Material
};                                                   // namespace BRDFTypes

/**
 * @brief      This class implements a perfect (no roughness) glass.
 *
 *
 */
class MRF_PLUGIN_EXPORT FresnelGlass: public BSDF
{
public:
  /**
   * @brief      Constructor for FresnelGlass
   *
   * @param      eta and kappa (a complex IOR)
   */
  FresnelGlass(std::string const &name);

  /**
   * @brief      Constructor for FresnelGlass
   *
   * @param      eta and kappa (a complex IOR)
   */
  FresnelGlass(
      std::string const &name,
      COLOR const &      eta,
      COLOR const &      kappa,
      COLOR const &      extinction,
      bool               thin_glass = false);

  virtual ~FresnelGlass();

  mrf::materials::COLOR const &eta() const;
  mrf::materials::COLOR const &kappa() const;
  mrf::materials::COLOR const &extinction() const;
  bool const &                 thinGlass() const;
  virtual mrf::materials::IOR  ior() const;

  void setFresnel(mrf::materials::COLOR eta, mrf::materials::COLOR kappa)
  {
    _eta   = eta;
    _kappa = kappa;
  }
  void setEta(mrf::materials::COLOR eta) { _eta = eta; }
  void setKappa(mrf::materials::COLOR kappa) { _kappa = kappa; }

  void setExtinction(mrf::materials::COLOR ext) { _extinction = ext; }

  void defineAsThin() { _thin_glass = true; }
  void defineAsThick() { _thin_glass = false; }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual std::vector<float>  getTransCDF(std::vector<uint> wavelengths);
  mrf::materials::COLOR const getInvTransPDF(std::vector<uint> wavelengths);
  virtual bool                initTransCDF(std::vector<uint> wavelengths);
#endif

  // The missing methods of the UMat interface are implemented below

  //   inline float geometricTerm(
  //       mrf::geom::LocalFrame const &lf,
  //       float                        dot_HV,
  //       mrf::math::Vec3f const &     view_dir,
  //       mrf::math::Vec3f const &     light_dir,
  //       float                        dot_NL,
  //       float                        dot_NV) const;

  //   virtual RADIANCE_TYPE coloredBRDFValue(
  //       mrf::geom::LocalFrame const &lf,
  //       mrf::math::Vec3f const &     in_dir,
  //       mrf::math::Vec3f const &     out_dir) const;

  //   virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBSDF(
  //       mrf::geom::LocalFrame const &lf,
  //       mrf::math::Vec3f const &     in_dir,
  //       float                        e1,
  //       float                        e2,
  //       mrf::math::Vec3f &           outgoing_dir,
  //       RADIANCE_TYPE &              bsdf_corrected_by_pdf,
  //       mrf::materials::IOR const &  in_ior,
  //       mrf::materials::IOR const &  out_ior) const;


  //   virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
  //       mrf::geom::LocalFrame const &lf,
  //       mrf::math::Vec3f const &     in_dir,
  //       float                        e1,
  //       float                        e2,
  //       mrf::math::Vec3f &           outgoing_dir,
  //       RADIANCE_TYPE &              brdf_corrected_by_pdf) const;

  //   inline mrf::math::Vec3f sampleHalfDir(mrf::geom::LocalFrame const &lf, float e1, float e2) const;

private:
  // True if transmission and false if TOTAL Internal Reflection
  //   bool tDir(
  //       mrf::math::Vec3f const &   V,   // The View Vector pointing away from the Macro-Geometry
  //       mrf::math::Vec3f const &   H,   // Half vector pointing toward the lenss dense medium
  //       float                      dot_VH,
  //       mrf::materials::IOR const &in_ior,
  //       mrf::materials::IOR const &out_ior,
  //       float const                dot_NV,
  //       mrf::math::Vec3f &         transmitted_dir) const;

protected:
  mrf::materials::COLOR _eta;
  mrf::materials::COLOR _kappa;
  mrf::materials::COLOR _extinction;
  bool                  _thin_glass;

#ifdef MRF_RENDERING_MODE_SPECTRAL
  std::vector<float>    _trans_CDF;
  mrf::materials::COLOR _inv_trans_PDF;
#endif
};



}   // namespace materials
}   // namespace mrf