//#include "random.h"
#include "include/fresnel.h"

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> _eta;
rtBuffer<float, 1> _kappa;
#  else
rtDeclareVariable(float, _eta, , );
rtDeclareVariable(float, _kappa, , );
#  endif
#else
rtDeclareVariable(float3, _eta, , );
rtDeclareVariable(float3, _kappa, , );
#endif

rtDeclareVariable(float, _alpha_g, , );
rtDeclareVariable(float, _sqr_alpha_g, , );   // K is either View or Light directions


#include "include/microfacet.h"


/**
 * @brief      Compute the BSDF pdf for either a reflection or refraction.
 *
 * @param      ffnormal        the face-forward normal vector.
 * @param      eye             the incoming direction, pointing away from the hitpoint.
 * @param      light           the outgoing direction, pointing away from the hitpoint.
 * @param      lobe            the previously sampled lobe. To use full BRDF, lobe = -1.
 *
 * @return     The value of the pdf.
 */
float bsdf_pdf(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  if (_alpha_g == 0.f)   //BRDF is a dirac
  {
    //We should check if we are aligned on the dirac or otherwise return 0.
    //However, this function should also never be called in such a case (no next event on dirac).
    float dot_NL = dot(light, ffnormal);
    return (dot_NL < 0.f) ? 0.f : 1.f;
  }
  else
  {
    float dot_NL = dot(light, ffnormal);

    float3 h = halfVector(eye, light, ffnormal);
    // h = normalize(h);
    float dot_NH = dot(h, ffnormal);
    if (dot_NH <= 0.f) return 0.f;
    float distribution_value = distribution(_sqr_alpha_g, dot_NH);

    //Compute the adapted jacobien term dw_h / dw_o
    return distribution_value * abs(dot_NH) / (4.f * abs(dot(eye, h)));   // 1 / 4 |o dot h| -> jacobien term
  }
}

/**
 * @brief      Compute the BSDF for either a reflection or refraction.
 *
 * @param      ffnormal        the face-forward normal vector.
 * @param      eye             the incoming direction, pointing away from the hitpoint.
 * @param      light           the outgoing direction, pointing away from the hitpoint.
 * @param      lobe            the previously sampled lobe. To use full BRDF, lobe = -1.
 *
 * @return     The value of the BSDF.
 */
COLOR bsdf_eval(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  COLOR res;

  float dot_NV = dot(ffnormal, eye);
  float dot_NL = dot(ffnormal, light);

  if (dot_NV <= 0.f || dot_NL <= 0.f)
  {
    return COLOR(0.f);
  }

  float  dot_HV = dot_NV;
  float  dot_NH = 1.f;
  float  dot_HL = dot_NL;
  float3 h      = halfVector(eye, light, ffnormal);
  if (_alpha_g > 0.f)
  {
    // h = normalize(h);
    dot_HV = dot(h, eye);
    dot_NH = dot(ffnormal, h);
    dot_HL = dot(h, light);
    if (dot_HV == 0.f || dot_NH <= 0.f)
    {
      return COLOR(0.f);
    }
  }

  COLOR F(0.f);
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  int id_wavelength = current_prd.id_wavelength;
  if (id_wavelength > -1)
  {
    // If ray has been refracted at some point, only one wavelength is still active.
    const float eta = lerp(_eta[id_wavelength], _eta[id_wavelength + 1], current_prd.wavelength_offset);
    const float ior
        = lerp(current_prd.ior[id_wavelength], current_prd.ior[id_wavelength + 1], current_prd.wavelength_offset);
    const float kappa = lerp(_kappa[id_wavelength], _kappa[id_wavelength + 1], current_prd.wavelength_offset);

    F[id_wavelength] = fresnelDieletricConductor(eta / ior, kappa / ior, dot_HV);
  }
  else   // No refraction has occured, compute for all wavelength.
  {
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    {
      const float eta   = lerp(_eta[i], _eta[i + 1], current_prd.wavelength_offset);
      const float ior   = lerp(current_prd.ior[i], current_prd.ior[i + 1], current_prd.wavelength_offset);
      const float kappa = lerp(_kappa[i], _kappa[i + 1], current_prd.wavelength_offset);

      F[i] = fresnelDieletricConductor(eta / ior, kappa / ior, dot_HV);
    }
  }
#else
  //TEMP use of COLOR constructor -> _eta and _kappa should be passed as color.
  F = fresnelDieletricConductor(COLOR(_eta) / current_prd.ior, COLOR(_kappa) / current_prd.ior, dot_HV);
#endif

  if (_alpha_g > 0.f)
  {
    float D = distribution(_sqr_alpha_g, dot_NH);
    // float G          = geometricTerm(dot_HV, dot_NV, dot_HL, dot_NL, _sqr_alpha_g);
    float G = geometricTerm(h, ffnormal, eye, light, make_float2(_alpha_g));

    F *= D * G / (4.f * dot_NL * dot_NV);
  }
  else   //Smooth -> ideal reflection, only fresnel + corrective terms
  {
    //No need to check alignment on the dirac.
    //We assume it is handled beforehand and current call is valid.
    F *= 1.f / dot_NV;
  }

  return F;
}

/**
 * @brief      Compute the optimized term BSDF * n dot l / pdf, for either a reflection or refraction.
 *
 * @param      ffnormal        the face-forward normal vector.
 * @param      eye             the incoming direction, pointing away from the hitpoint.
 * @param      light           the outgoing direction, pointing away from the hitpoint.
 * @param      lobe            the previously sampled lobe. To use full BSDF, lobe = -1.
 * @param      external_pdf    an pdf previously computed if needed.
 *
 * @return     The value of BSDF * n dot l / pdf.
 */
COLOR bsdf_eval_optim(float3 h, float3 ffnormal, float3 eye, float3 light, int lobe, float external_pdf)
{
  //Lobe is unused, only one lobe in pure microfacet

  COLOR res;

  float dot_NV = dot(ffnormal, eye);
  float dot_NL = dot(ffnormal, light);

  if (dot_NV <= 0.f || dot_NL <= 0.f)
  {
    return COLOR(0.f);
  }

  float dot_HV = dot_NV;
  float dot_NH = 1.f;
  float dot_HL = dot_NL;
  if (_alpha_g > 0.f)
  {
    dot_HV = dot(eye, h);
    dot_NH = dot(ffnormal, h);
    dot_HL = dot(light, h);
    if (dot_HV == 0.f || dot_NH <= 0.f)
    {
      return COLOR(0.f);
    }
  }

  COLOR F(0.f);
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  const int id_wavelength = current_prd.id_wavelength;

  if (id_wavelength > -1)
  {
    // If ray has been refracted at some point, only one wavelength is still active.
    const float eta = lerp(_eta[id_wavelength], _eta[id_wavelength + 1], current_prd.wavelength_offset);
    const float ior
        = lerp(current_prd.ior[id_wavelength], current_prd.ior[id_wavelength + 1], current_prd.wavelength_offset);
    const float kappa = lerp(_kappa[id_wavelength], _kappa[id_wavelength + 1], current_prd.wavelength_offset);

    F[id_wavelength] = fresnelDieletricConductor(eta / ior, kappa / ior, dot_HV);
  }
  else   // No refraction has occured, compute for all wavelength.
  {
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    {
      const float eta   = lerp(_eta[i], _eta[i + 1], current_prd.wavelength_offset);
      const float ior   = lerp(current_prd.ior[i], current_prd.ior[i + 1], current_prd.wavelength_offset);
      const float kappa = lerp(_kappa[i], _kappa[i + 1], current_prd.wavelength_offset);

      F[i] = fresnelDieletricConductor(eta / ior, kappa / ior, dot_HV);
    }
  }
#else
  //TEMP use of COLOR constructor -> _eta and _kappa should be passed as color.
  F = fresnelDieletricConductor(COLOR(_eta) / current_prd.ior, COLOR(_kappa) / current_prd.ior, dot_HV);
#endif

  if (_alpha_g > 0.f)
  {
    // float G  = geometricTerm(dot_HV, dot_NV, dot_HL, dot_NL, _sqr_alpha_g);
    float G = geometricTerm(h, ffnormal, eye, light, make_float2(_alpha_g));
    // float G1 = v_geometricTerm(dot_HV, dot_NV, dot_HL, dot_NL, _sqr_alpha_g);
    //D is cancelled out by pdf.
    float temp_float = G * abs(dot_HV / (dot_NV * dot_NH));
    F *= temp_float;
    // float temp_float = G / G1;
    // F *= (G1 > 0.f) ? temp_float : 0.f;
  }

  return F;
}
