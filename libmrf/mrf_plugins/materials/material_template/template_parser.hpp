#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <mrf_core/materials/materials.hpp>

/////////TODO
//Replace template name by your material information.
//#include <mrf_plugins/materials/material_template/material_template.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
/////////TODO
//Replace template name by your material information.
class TemplateMatParser: public BaseMaterialParser<mrf::materials::TemplateMat>
{
public:
  TemplateMatParser() {}

  /////////TODO
  //Replace template name by your material information.
  static std::string getID() { return mrf::materials::BRDFTypes::TEMPLATE_MAT_XML_NAME; }

  /////////TODO
  //Replace template name by your material information.
  static BasePluginParser<mrf::materials::UMat> *create() { return new TemplateMatParser; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::TemplateMat *template_mat = dynamic_cast<mrf::materials::TemplateMat *>(a_mat);

    if (!template_mat) return false;

    /////////TODO
    /*
     * Implement here how an XML element containing your material should be parse.
     * Some generic parsing function are available in the ParserHelper class.
     * Please see other plugin for example.
     */

    return true;
  }

protected:
private:
};
}   // namespace io
}   // namespace mrf
