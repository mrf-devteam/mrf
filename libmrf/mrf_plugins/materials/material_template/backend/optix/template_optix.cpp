/*
 * Template author: David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 * author : Your Name @ your.institute
 * Copyright XXXX 20XX
 *
 **/

#include <mrf_plugins/materials/material_template/backend/optix/template_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>


#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *TemplateMatOptixPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *TemplateMatOptixPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options)
#endif
{
  auto a_mat = dynamic_cast<mrf::materials::TemplateMat *>(material);
  if (a_mat)
  {
    optix::Material optix_mat = context->createMaterial();
    optix_mat["a_variable"]->setFloat(1.f);
    //TODO
    //Uncomment and use accessors to fetch the value and replace the lines above to pass a variable to the GPU kernel.
    // optix_mat["a_variable"]->setFloat(a_mat->getSomeValue());

    auto compile_options = cuda_compile_options;
    compile_options.push_back("-DSOME_DEFINE");

    OptixTemplateMat *ret_mat = new OptixTemplateMat(a_mat, optix_mat, compile_options);

    std::vector<std::string> variable_names = {"a_color", "another_color"};

    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(mrf::materials::RADIANCE_TYPE(0.8f));
    variable_values.push_back(mrf::materials::RADIANCE_TYPE(0.1f));

    //TODO
    //Uncomment and use accessors to fetch the colors and replace the lines above. Colors here are RADIANCE_TYPE: (r,g,b) or a spectrum depending on wether the app is RGB of spectral.
    // variable_values.push_back(a_mat->getSomeColor());
    // variable_values.push_back(a_mat->getSomeOtherColor());
    //

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif

    return ret_mat;
  }

  return nullptr;
}

//Uncomment if necessary
// #ifdef MRF_RENDERING_MODE_SPECTRAL
// void OptixLambert::update(optix::Context &context, std::vector<uint> wavelengths)
// #else
// void OptixLambert::update(optix::Context &context)
// #endif
// {
// //TODO
// //If the material uses some buffers, they must be update when:
// // - in spectral, when changing the pass (if not fully multiplexed).
// // - in rgb or spectral if necessary ?

// //MANDATORY if method is overridden.
// //Colors already set by the createFromMRF method, e.g. "a_color", "another_color" (above in createFromMRF) are automatically updated by calling the parent method (above).
// #ifdef MRF_RENDERING_MODE_SPECTRAL
// OptixBaseMaterial::update(context, wavelengths);
// #else
// OptixBaseMaterial::update(context);
// #endif
// }


}   // namespace optix_backend
}   // namespace mrf
