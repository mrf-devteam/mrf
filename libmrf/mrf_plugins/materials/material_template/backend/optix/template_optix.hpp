/*
 * Template author: David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 * author : Your Name @ your.institute
 * Copyright XXXX 20XX
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/rendering/renderer.hpp>
#include <mrf_core/lighting/light.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/materials/all_materials.hpp>

#include <mrf_optix/optix_material_plugin.hpp>
#include <mrf_optix/optix_light_plugin.hpp>

#include <mrf_plugins/materials/material_template/material_template.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <vector>
#include <map>


namespace mrf
{
namespace optix_backend
{
class MRF_PLUGIN_EXPORT OptixTemplateMat: public OptixBRDFMaterial
{
public:
  OptixTemplateMat(optix::Material mat): OptixBRDFMaterial(mat) { _kernel_path = "template_kernel.cu"; }
  OptixTemplateMat(std::vector<std::string> const &cuda_compile_options): OptixBRDFMaterial(cuda_compile_options)
  {
    _kernel_path = "template_kernel.cu";
  }
  OptixTemplateMat(optix::Material mat, std::vector<std::string> const &cuda_compile_options)
    : OptixBRDFMaterial(mat, cuda_compile_options)
  {
    _kernel_path = "template_kernel.cu";
  }

  OptixTemplateMat(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options)
    : OptixBRDFMaterial(mrf_mat, mat, cuda_compile_options)
  {
    _kernel_path = "template_kernel.cu";
  }


  //TODO override theses methods if specific buffers must be updated.
  // #ifdef MRF_RENDERING_MODE_SPECTRAL
  // virtual void update(optix::Context &context, std::vector<uint> wavelengths);
  // #else
  // virtual void               update(optix::Context &context);
// #endif

//TODO implement theses methods if textures must be updated
#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateTextures(optix::Context &context, std::vector<uint> wavelengths) {};
#else
  virtual void               updateTextures(optix::Context &context) {};
#endif
};

///////////////////////////////////////////////////////////////////////////////////
////////////////PLUGINS////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

class MRF_PLUGIN_EXPORT TemplateMatOptixPlugin: public OptixMaterialPlugin
{
public:
  TemplateMatOptixPlugin() {}
  ~TemplateMatOptixPlugin() {}

  static OptixMaterialPlugin *create() { return new TemplateMatOptixPlugin; }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      std::vector<uint>               wavelengths);
#else
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options);
#endif

private:
};

}   // namespace optix_backend
}   // namespace mrf
