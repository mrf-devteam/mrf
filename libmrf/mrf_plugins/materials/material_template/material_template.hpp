/*
 * Template author: David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 * author : Your Name @ your.institute
 * Copyright XXXX 20XX
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/brdf.hpp>

namespace mrf
{
namespace materials
{
//If plugin is a BRDF:
namespace BRDFTypes
{
static char const *TEMPLATE_MAT_XML_NAME = "TemplateMatName";
};   // namespace BRDFTypes

//If plugin is a BSDF:
//namespace BSDFTypes
//{
//static char const *TEMPLATE_MAT_XML_NAME = "TemplateMatName";
//};                                        // namespace BSDFTypes

//If plugin is emissive:
//namespace EmittanceTypes
//{
//static char const *TEMPLATE_MAT_XML_NAME = "TemplateMatName";
//};                                        // namespace EmittanceTypes

//-------------------------------------------------------------------------
// Lambert Reflector
//-------------------------------------------------------------------------

class MRF_PLUGIN_EXPORT TemplateMat: public BRDF   //BSDF // Emittance
{
protected:
  /////////TODO
  //Add the material attributes here.

public:
  TemplateMat(std::string const &name);
  /////////TODO
  //Add any constructor here.

  virtual ~TemplateMat();


  //-------------------------------------------------------------------------
  // Some Accessors if needed
  //-------------------------------------------------------------------------

  //-------------------------------------------------------------------------
  // UMat Interface Methods Implementation - NOT MANDATORY WITH OPTIX BACKEND
  //-------------------------------------------------------------------------

  virtual float
  brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;

  //-------------------------------------------------------------------------
  virtual float
  diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;

  virtual ScatterEvent::SCATTER_EVT scatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f &           outgoing_dir,
      float &                      proba_outgoing_dir,
      float                        e1,
      float                        e2) const;

  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;
};

}   // namespace materials
}   // namespace mrf
