#include "uv_shader.hpp"

namespace mrf
{
namespace materials
{
UVShader::UVShader(std::string const &name): BRDF(name, BRDFTypes::UV) {}

UVShader::~UVShader() {}


RADIANCE_TYPE
UVShader::coloredBRDFValue(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return RADIANCE_TYPE(lf.uv().x(), lf.uv().y(), 0.f);
}



}   // namespace materials
}   // namespace mrf
