/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 *  Tangent Shader
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//MRF
#include <mrf_core/math/math.hpp>
#include <mrf_core/materials/brdf.hpp>
#include <mrf_core/materials/bsdf.hpp>

#include <mrf_core/geometry/local_frame.hpp>

#include <mrf_plugins/materials/debug/flat.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *TANGENT = "Tangent";
};   // namespace BRDFTypes


class MRF_PLUGIN_EXPORT TangentShader: public BRDF
{
public:
  TangentShader(std::string const &name);
  virtual ~TangentShader();

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;
};
}   // namespace materials
}   // namespace mrf
