rtDeclareVariable(int, _debug_type, , );

#define FLAT    0
#define NORMAL  1
#define TANGENT 2
#define UV      3

bool bsdf_is_dirac()
{
  return true;
}


float bsdf_pdf(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  return 1.f;
}

float3 toLocalFrame(float3 dir, float3 normal, float3 tangent, float3 bitangent)
{
  float3 res;
  res.x = clamp(dot(dir, tangent), -1.f, 1.f);
  res.z = clamp(dot(dir, normal), 0.f, 1.f);
  res.y = clamp(dot(dir, bitangent), -1.f, 1.f);

  return res;
}

float azimuthAngle(float3 local_dir)
{
  float phi = atan2f(local_dir.y, local_dir.x);

  if (phi < 0.0)
  {
    phi += 2.f * M_PIf;
  }

  return phi;
}

COLOR bsdf_eval(float3 ffnormal, float3 eye, float3 light, int lobe)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
#else
  if (current_prd.depth == 0)
  {
    if (_debug_type == FLAT)
    {
      float2 rnd2d = sampling2D(
          current_prd.num_sample,
          current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
          current_prd.offset_sampling,
          current_prd.seed);
      float z1 = rnd2d.x;
      float z2 = rnd2d.y;

      float3 p;
      cosine_sample_hemisphere(z1, z2, p);
      optix::Onb onb(ffnormal);
      onb.inverse_transform(p);

      float nDotL = dot(p, ffnormal);
      float nDotV = dot(eye, ffnormal);

      //compute d_phi without tangent nor bitangent help
      //works for isotropic brdf
      float d_phi   = M_PIf;
      float EPSILON = 0.0001f;

      float3 tangent   = onb.m_tangent;
      float3 bitangent = -onb.m_binormal;   /// cross(tangent,ffnormal);

      float phi_out  = atan2f(clamp(-dot(eye, bitangent), -1.f, 1.f), clamp(-dot(eye, tangent), -1.f, 1.f));
      float phi_in   = atan2f(clamp(-dot(p, bitangent), -1.f, 1.f), clamp(-dot(p, tangent), -1.f, 1.f));
      float phi_diff = phi_out - phi_in;

      d_phi = 0.5f * phi_diff / M_PIf;
      return COLOR(d_phi);
    }
    else if (_debug_type == NORMAL)
    {
      return ffnormal;
    }
    else if (_debug_type == TANGENT)
    {
      optix::Onb onb(ffnormal);
      float3     tangent   = generateOrthogonal(ffnormal);
      float3     Bitangent = cross(ffnormal, tangent);
      tangent              = cross(ffnormal, Bitangent);

      return tangent;
    }
    else if (_debug_type == UV)
    {
      return make_float3(texcoord.x, texcoord.y, 0.f);
    }
  }
  else
#endif
  {
    return COLOR(0.f);
  }
}

COLOR bsdf_eval_optim(float3 h, float3 ffnormal, float3 eye, float3 light, int lobe, float external_pdf)
{
  return bsdf_eval(ffnormal, eye, light, lobe);
}

float3 generate_half_vector(float3 n, float3 in_dir)
{
  return n;
}

float4 generate_reflective_sample(float3 n, float3 in_dir)
{
  //Ugly to do here, but need a hack for debug due to early escape.
  current_prd.result = bsdf_eval(n, -in_dir, n, -1);

  //Identify as absorption to force an early escape of the path-tracing iteration.
  return make_float4(n, ABSORPTION);
}
