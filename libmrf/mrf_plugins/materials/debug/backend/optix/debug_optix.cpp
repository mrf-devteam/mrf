/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/debug/backend/optix/debug_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>


#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *DebugPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *DebugPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  optix::Material optix_mat       = context->createMaterial();
  auto            compile_options = cuda_compile_options;

  auto flat_mat = dynamic_cast<mrf::materials::FlatBRDF *>(material);
  if (flat_mat)
  {
    optix_mat["_debug_type"]->setInt(0);
    compile_options.push_back("-DFLAT");   //Necessary to avoid misuse of ptx.
    return new OptixDebug(flat_mat, optix_mat, compile_options);
  }

  auto normal_mat = dynamic_cast<mrf::materials::NormalShader *>(material);
  if (normal_mat)
  {
    optix_mat["_debug_type"]->setInt(1);
    compile_options.push_back("-DNORMAL");
    return new OptixDebug(normal_mat, optix_mat, compile_options, ptx_cfg);
  }

  auto tangent_mat = dynamic_cast<mrf::materials::TangentShader *>(material);
  if (tangent_mat)
  {
    optix_mat["_debug_type"]->setInt(2);
    compile_options.push_back("-DTANGENT");
    return new OptixDebug(tangent_mat, optix_mat, compile_options, ptx_cfg);
  }

  auto uv_mat = dynamic_cast<mrf::materials::UVShader *>(material);
  if (uv_mat)
  {
    optix_mat["_debug_type"]->setInt(3);
    compile_options.push_back("-DUV");
    return new OptixDebug(uv_mat, optix_mat, compile_options, ptx_cfg);
  }

  optix_mat->destroy();
  return nullptr;
}

}   // namespace optix_backend
}   // namespace mrf
