
if(${USE_OPTIX})
  set(BCK_SOURCES 
    ${BCK_SOURCES}
      ${CMAKE_CURRENT_LIST_DIR}/optix/debug_optix.cpp
    PARENT_SCOPE)
    
  set(BCK_HEADERS 
    ${BCK_HEADERS}
      ${CMAKE_CURRENT_LIST_DIR}/optix/debug_optix.hpp
    PARENT_SCOPE
    )
    
  set(BCK_CU_SOURCES 
    ${BCK_CU_SOURCES}
      ${CMAKE_CURRENT_LIST_DIR}/optix/debug_shader.cu
    PARENT_SCOPE)
endif()