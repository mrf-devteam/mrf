#include <mrf_plugins/materials/checkerboard/checkerboard.hpp>

#include <mrf_core/math/math.hpp>


namespace mrf
{
namespace materials
{
CheckerBoard::CheckerBoard(
    std::string const &  name,
    uint                 repetition,
    RADIANCE_TYPE const &color_1,
    RADIANCE_TYPE const &color_2)
  : BRDF(name, BRDFTypes::CHECKERBOARD)
  , _repetition(repetition)
  , _color1(color_1)
  , _color2(color_2)
{}

CheckerBoard::~CheckerBoard() {}


RADIANCE_TYPE
CheckerBoard::coloredBRDFValue(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  unsigned int sum = static_cast<unsigned int>(std::floor(_repetition * lf.uv().x()));
  sum += static_cast<unsigned int>(std::floor(_repetition * lf.uv().y()));

  if (sum % 2 == 0)
  {
    return _color1;
  }
  else
  {
    return _color2;
  }
}

ScatterEvent::SCATTER_EVT CheckerBoard::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    float                        e1,
    float                        e2,
    mrf::math::Vec3f &           outgoing_dir,
    RADIANCE_TYPE &              brdf_corrected_by_pdf) const
{
  using namespace mrf::math;

  //Cosine Sampling. This is a CheckerBoard
  outgoing_dir = mrf::materials::UMat::randomHemiCosDirection(lf, e1, e2);

  float const dot_NL = mrf::math::clamp(lf.normal().dot(outgoing_dir), 0.0f, 1.0f);
  if (dot_NL <= 0.0f)
  {
    brdf_corrected_by_pdf = RADIANCE_TYPE(0.0f);
    return ScatterEvent::ABSORPTION;
  }

  brdf_corrected_by_pdf = coloredBRDFValue(lf, in_dir, outgoing_dir) * static_cast<float>(Math::PI);
  return ScatterEvent::DIFFUSE_REFL;
}




}   // namespace materials
}   // namespace mrf
