/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/


#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/emittance.hpp>


namespace mrf
{
namespace materials
{
namespace EmittanceTypes
{
static char const *CONIC = "conic";
};   // namespace EmittanceTypes

/**
 * Conical Emittance along a given direction and a given aperture
 * The direction is given in world coordinates
 *
 * This class is to be used with PointLight
 *
 * Use ConeEmitt Class if you want a surfacic light with conical emittance
 **/
class MRF_PLUGIN_EXPORT ConicalEmittance: public Emittance
{
protected:
  float _cos_aperture;   //cosine of the cone aperture of the goniometric function
  // mrf::math::Vec3f _Zorientation;// Z = DIRECTION Cone orientation in local frame
  mrf::math::Vec3f _Xorientation;   // Local orientation used to generate particles
  mrf::math::Vec3f _Yorientation;   // Local orientation used to generate particles

public:
  ConicalEmittance(
      std::string const &     name,
      float                   aperture  = mrf::math::Math::PI / 2.f,            //Has to be in [0,Pi/2]
      mrf::math::Vec3f const &direction = mrf::math::Vec3f(0.9f, -1.0f, 0.f),   //Cone orientation in global frame
      float                   radiance  = 1.0f,                                 //Has to be >0
      COLOR const &           color     = COLOR(1.0f));

  inline float            cosAperture() const;
  inline float            aperture() const;
  inline float            apertureRad() const;
  inline mrf::math::Vec3f direction() const;

  void setAperture(float ap) { _cos_aperture = mrf::math::Math::absVal(std::cos(ap)); }

  //-------------------------------------------------------------------
  // UMat's interface Implementation of
  //-------------------------------------------------------------------
  virtual RADIANCE_TYPE radianceEmitted(mrf::math::Vec3f const &emit_out_dir, mrf::geom::LocalFrame const &lf) const;


  virtual RADIOSITY_TYPE radiosityEmitted() const;


  virtual RADIOSITY_TYPE
  particleEmission(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2, mrf::math::Vec3f &direction)
      const;


protected:
  virtual void computeInternalValues();
};

inline float ConicalEmittance::cosAperture() const
{
  return _cos_aperture;
}
inline float ConicalEmittance::aperture() const
{
  return acosf(_cos_aperture) * 180.f / static_cast<float>(mrf::math::Math::PI);
}
inline float ConicalEmittance::apertureRad() const
{
  return acosf(_cos_aperture);
}
inline mrf::math::Vec3f ConicalEmittance::direction() const
{
  return _direction;
}

}   // namespace materials
}   // namespace mrf
