#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>

#include <mrf_core/materials/materials.hpp>
#include <mrf_plugins/materials/emittance/conical_emittance.hpp>
#include <mrf_plugins/materials/emittance/dirac_emittance.hpp>
#include <mrf_plugins/materials/emittance/diffuse_emittance.hpp>
#include <mrf_plugins/materials/emittance/uniform_emittance.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>


namespace mrf
{
namespace io
{
class ConicalEmittanceParser: public BaseMaterialParser<mrf::materials::ConicalEmittance>
{
public:
  ConicalEmittanceParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new ConicalEmittanceParser; }

  static std::string getID() { return mrf::materials::EmittanceTypes::CONIC; }

  virtual bool parse(tinyxml2::XMLElement *an_emitt_elem, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::ConicalEmittance *conic = dynamic_cast<mrf::materials::ConicalEmittance *>(a_mat);

    float aperture;
    int   apertureOK = an_emitt_elem->QueryFloatAttribute(SceneLexer::APERTURE_AT, &aperture);

    if (apertureOK != tinyxml2::XML_SUCCESS)
    {
      return false;
    }

    mrf::materials::COLOR color = parser.retrieveColor(an_emitt_elem, false);

    tinyxml2::XMLElement *radiance_elem = an_emitt_elem->FirstChildElement(SceneLexer::RADIANCE_MK);

    float radiance = parser.scalar_from_value(radiance_elem);

    mrf::math::Vec3f direction;
    if (!parser.retrieveDirection(an_emitt_elem, direction))
    {
      a_mat = nullptr;
      return false;
    }

    //convert aperture in radians
    aperture = aperture / 180.f * static_cast<float>(mrf::math::Math::PI);

    conic->setAperture(aperture);
    conic->setDirection(direction);
    conic->setColor(color);
    conic->setRadiance(radiance);

    return true;
  }

protected:
private:
};

class DiffuseEmittanceParser: public BaseMaterialParser<mrf::materials::DiffuseEmittance>
{
public:
  DiffuseEmittanceParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new DiffuseEmittanceParser; }

  static std::string getID() { return mrf::materials::EmittanceTypes::DIFFUSE; }

  virtual bool parse(tinyxml2::XMLElement *an_emitt_elem, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::DiffuseEmittance *diffuse = dynamic_cast<mrf::materials::DiffuseEmittance *>(a_mat);

    mrf::materials::COLOR color = parser.retrieveColor(an_emitt_elem, false);
    diffuse->setColor(color);

    tinyxml2::XMLElement *radiance_elem = an_emitt_elem->FirstChildElement(SceneLexer::RADIANCE_MK);

    float radiance = parser.scalar_from_value(radiance_elem);

    diffuse->setRadiance(radiance);

    return true;
  }

protected:
private:
};

class DiracEmittanceParser: public BaseMaterialParser<mrf::materials::DiracEmittance>
{
public:
  DiracEmittanceParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new DiracEmittanceParser; }

  static std::string getID() { return mrf::materials::EmittanceTypes::DIRAC; }

  virtual bool parse(tinyxml2::XMLElement *an_emitt_elem, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::DiracEmittance *dirac = dynamic_cast<mrf::materials::DiracEmittance *>(a_mat);

    mrf::materials::COLOR color = parser.retrieveColor(an_emitt_elem, false);

    tinyxml2::XMLElement *radiance_elem = an_emitt_elem->FirstChildElement(SceneLexer::RADIANCE_MK);

    float radiance = parser.scalar_from_value(radiance_elem);

    mrf::math::Vec3f direction;
    if (!parser.retrieveDirection(an_emitt_elem, direction))
    {
      a_mat = nullptr;
      return false;
    }

    dirac->setDirection(direction);
    dirac->setColor(color);
    dirac->setRadiance(radiance);

    return true;
  }

protected:
private:
};


class UniformEmittanceParser: public DiffuseEmittanceParser
{
public:
  UniformEmittanceParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new UniformEmittanceParser; }

  static std::string getID() { return mrf::materials::EmittanceTypes::UNIFORM; }

protected:
private:
};

}   // namespace io
}   // namespace mrf
