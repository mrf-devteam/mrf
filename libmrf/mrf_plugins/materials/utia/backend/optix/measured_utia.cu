rtDeclareVariable(int, _material_tex_id, , "Material texture id");
// rtDeclareVariable(int, _diffuse_tex_id, , "Diffuse texture id");
rtBuffer<int, 1> _utia_tex_ids;


//NOTE: in tex2d: (0,0) = top left; (1,1) = bottom right
//y axis is 0 up, 1 down

struct UtiaParam
{
  int4 div;
  int  param;
  int  interpolate;
};

rtBuffer<UtiaParam, 1> _utia_parameters;   //for each mat: (theta_v, phi_v, theta_i, phi_i)

// rtDeclareVariable(unsigned int, _parameterization, , );
// rtDeclareVariable(unsigned int, _interpolate, , );

#include "include/distribution/cosine.h"
#include "include/parameterization.h"

bool bsdf_is_dirac()
{
  return false;
}

//Index is used in 4D anisotropic, use theta and phi
uint directionToIndex4D_ui(float3 normal, float3 direction, uint theta_div, uint phi_div)
{
  float2 spherical = directionToSpherical(normal, direction);

  uint theta_index = floor(spherical.x * float(theta_div));
  uint phi_index   = uint(spherical.y * phi_div) % phi_div;   //Wrap phi

  return theta_index * phi_div + phi_index;
}

//Index is used in 2D isotropic, use only theta
uint directionToIndex2D_ui(float3 normal, float3 direction, uint theta_div)
{
  return floor(directionToTheta(normal, direction) * float(theta_div));
}

//Index is used in 2D isotropic, use only theta
float directionToIndex2D_f(float3 normal, float3 direction)
{
  return directionToTheta(normal, direction);
}

//Returns index as float (0;1)
float2 index_f(float3 normal, float3 eye, float3 light, UtiaParam utia_param, uint3 data_size)
{
  float2 index;
  int    theta_v_div = utia_param.div.x;
  int    phi_v_div   = utia_param.div.y;
  int    theta_i_div = utia_param.div.z;
  int    phi_i_div   = utia_param.div.w;
  if (utia_param.param == UTIA_4D)
  {
    index.x = directionToIndex4D_ui(normal, eye, theta_v_div, phi_v_div) / (float)data_size.x;     // v in [0;1]
    index.y = directionToIndex4D_ui(normal, light, theta_i_div, phi_i_div) / (float)data_size.y;   // i in [0;1]
  }
  else if (utia_param.param == UTIA_4D_H)
  {
    index.x = directionToIndex4D_ui(normal, eye, theta_v_div, phi_v_div) / (float)data_size.x;   // v in [0;1]
    index.y = directionToIndex4D_ui(normal, normalize(eye + light), theta_i_div, phi_i_div)
              / (float)data_size.y;   // i in [0;1]
  }
  else if (utia_param.param == UTIA_4D_H_NL)
  {
    //TODO
    index.x = directionToIndex4D_ui(normal, eye, theta_v_div, phi_v_div) / (float)data_size.x;   // v in [0;1]
    index.y = directionToIndex4D_ui(normal, normalize(eye + light), theta_i_div, phi_i_div)
              / (float)data_size.y;   // i in [0;1]
  }
  else if (utia_param.param == UTIA_2D)
  {
    index.x = directionToIndex2D_f(normal, eye);     // v in [0;1]
    index.y = directionToIndex2D_f(normal, light);   // i in [0;1]
  }
  else if (utia_param.param == RUSINKIEWICZ_2D)
  {
    float3 h = normalize(eye + light);
    index.x  = acos(dot(normal, h)) * 2.f / M_PIf;        //theta_h in [0;1]
    index.y  = 1.f - (acos(dot(h, eye)) * 2.f / M_PIf);   //theta_d in [0;1] //revert y for image consistency
  }
  else if (utia_param.param == RUSINKIEWICZ_2D_COS)
  {
    float3 h = normalize(eye + light);
    index.x  = dot(normal, h);      //cos_theta_h in [0;1]
    index.y  = 1.f - dot(h, eye);   //cos_theta_d in [0;1] //revert y for image consistency
  }
  return index;
}

//Returns index as uint (0;data_size x/y)
uint2 index_ui(float3 normal, float3 eye, float3 light, UtiaParam utia_param, uint3 data_size)
{
  uint2 index;
  int   theta_v_div = utia_param.div.x;
  int   phi_v_div   = utia_param.div.y;
  int   theta_i_div = utia_param.div.z;
  int   phi_i_div   = utia_param.div.w;
  if (utia_param.param == UTIA_4D)
  {
    index.x = directionToIndex4D_ui(normal, eye, theta_v_div, phi_v_div);     // v in [0;dim]
    index.y = directionToIndex4D_ui(normal, light, theta_i_div, phi_i_div);   //i in [0;dim]
  }
  else if (utia_param.param == UTIA_4D_H)
  {
    index.x = directionToIndex4D_ui(normal, eye, theta_v_div, phi_v_div);                      // v in [0;dim]
    index.y = directionToIndex4D_ui(normal, normalize(eye + light), theta_i_div, phi_i_div);   //i in [0;dim]
  }
  else if (utia_param.param == UTIA_4D_H_NL)
  {
    index.x = directionToIndex4D_ui(normal, eye, theta_v_div, phi_v_div);                      // v in [0;dim]
    index.y = directionToIndex4D_ui(normal, normalize(eye + light), theta_i_div, phi_i_div);   //i in [0;dim]
  }
  else if (utia_param.param == UTIA_2D)
  {
    index.x = directionToIndex2D_ui(normal, eye, data_size.x);     // v in [0;dim]
    index.y = directionToIndex2D_ui(normal, light, data_size.y);   //i in [0;dim]
  }
  else if (utia_param.param == RUSINKIEWICZ_2D)
  {
    float3 h = normalize(eye + light);
    index.x  = 0.5;   //(acos(dot(normal, h)) * 2.f / M_PIf) * theta_v_div; // theta_h in [0;dim]
    index.y
        = 0.5;   //(1.f - (acos(dot(h, eye)) * 2.f / M_PIf)) * theta_i_div; // theta_d in [0;dim] //revert y for image consistency
  }
  else if (utia_param.param == RUSINKIEWICZ_2D_COS)
  {
    float3 h = normalize(eye + light);
    index.x  = dot(normal, h) * theta_v_div;        // cos_theta_h in [0;dim]
    index.y  = (1.f - dot(h, eye)) * theta_i_div;   // cos_theta_d in [0;dim] //revert y for image consistency
  }
  return index;
}

float bsdf_pdf(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  return cosine_pdf(ffnormal, eye, light);
}


COLOR fetchColorIsotropic(float3 normal, float3 eye, float3 light, int utia_id, uint3 data_size)
{
  uint selected_tex = _utia_tex_ids[utia_id];

  //Always use built-in interpolation in isotropic.
  float2 index = index_f(normal, eye, light, _utia_parameters[utia_id], data_size);
  return COLOR(make_float3(optix::rtTex2D<float4>(selected_tex, index.x, index.y)));
}

COLOR fetchColorAnisotropic(float3 normal, float3 eye, float3 light, int utia_id, uint3 data_size)
{
  uint selected_tex = _utia_tex_ids[utia_id];

  if (!_utia_parameters[utia_id].interpolate)
  {
    uint2 index = index_ui(normal, eye, light, _utia_parameters[utia_id], data_size);   //x -> v -> eye, y -> i -> light

    return COLOR(make_float3(optix::rtTex2DFetch<float4>(selected_tex, index.x, index.y)));
  }
  else
  {
    int theta_v_div = _utia_parameters[utia_id].div.x;
    int phi_v_div   = _utia_parameters[utia_id].div.y;
    int theta_i_div = _utia_parameters[utia_id].div.z;
    int phi_i_div   = _utia_parameters[utia_id].div.w;

    float theta_v = directionToTheta(normal, eye);
    float theta_i = directionToTheta(normal, light);
    float phi_v   = directionToPhi(normal, eye);
    float phi_i   = directionToPhi(normal, light);

    float step_theta_v = 0.5f * M_PIf / theta_v_div;
    float step_theta_i = 0.5f * M_PIf / theta_i_div;
    float step_phi_v   = 2.f * M_PIf / phi_i_div;
    float step_phi_i   = 2.f * M_PIf / phi_v_div;

    uint iti[2], itv[2], ipi[2], ipv[2];
    iti[0] = floor(theta_i / step_theta_v);
    iti[1] = iti[0] + 1;
    if (iti[0] > theta_i_div - 2)
    {
      iti[0] = theta_i_div - 2;
      iti[1] = theta_i_div - 1;
    }
    itv[0] = floor(theta_v / step_theta_v);
    itv[1] = itv[0] + 1;
    if (itv[0] > theta_v_div - 2)
    {
      itv[0] = theta_v_div - 2;
      itv[1] = theta_v_div - 1;
    }

    ipi[0] = floor(phi_i / step_phi_i);
    ipi[1] = ipi[0] + 1;
    ipv[0] = floor(phi_v / step_phi_v);
    ipv[1] = ipv[0] + 1;

    float sum;
    float wti[2], wtv[2], wpi[2], wpv[2];
    wti[1] = theta_i - (float)(step_theta_i * iti[0]);
    wti[0] = (float)(step_theta_i * iti[1]) - theta_i;
    sum    = wti[0] + wti[1];
    wti[0] /= sum;
    wti[1] /= sum;

    wtv[1] = theta_v - (float)(step_theta_v * itv[0]);
    wtv[0] = (float)(step_theta_v * itv[1]) - theta_v;
    sum    = wtv[0] + wtv[1];
    wtv[0] /= sum;
    wtv[1] /= sum;

    wpi[1] = phi_i - (float)(step_phi_i * ipi[0]);
    wpi[0] = (float)(step_phi_i * ipi[1]) - phi_i;
    sum    = wpi[0] + wpi[1];
    wpi[0] /= sum;
    wpi[1] /= sum;

    wpv[1] = phi_v - (float)(step_phi_v * ipv[0]);
    wpv[0] = (float)(step_phi_v * ipv[1]) - phi_v;
    sum    = wpv[0] + wpv[1];
    wpv[0] /= sum;
    wpv[1] /= sum;

    if (ipi[1] == phi_v_div) ipi[1] = 0;
    if (ipv[1] == phi_v_div) ipv[1] = 0;


    int    nc  = phi_v_div * theta_v_div;
    int    nr  = phi_i_div * theta_i_div;
    float3 RGB = make_float3(0.f);

    int i, j, k, l;

    for (i = 0; i < 2; ++i)
      for (j = 0; j < 2; ++j)
        for (k = 0; k < 2; ++k)
          for (l = 0; l < 2; ++l)
          {
            float w = wtv[i] * wti[j] * wpv[k] * wpi[l];

            uint index_v = itv[i] * phi_v_div + ipv[k];
            uint index_i = iti[j] * phi_i_div + ipi[l];

            float3 brdf_data = make_float3(optix::rtTex2DFetch<float4>(selected_tex, index_v, index_i));

            RGB += w * brdf_data;
          }
    return COLOR(RGB);
  }
}

COLOR fetchColor(float3 normal, float3 eye, float3 light, int utia_id)
{
  uint utia_nb = _utia_tex_ids.size();
  // if(utia_id == -1)
  // return COLOR(0.f);

  if (utia_id == 0 && _material_tex_id > -1)
  {
    float2 uv = make_float2(texcoord.x - floorf(texcoord.x), texcoord.y - floorf(texcoord.y));

    return COLOR(make_float3(optix::rtTex2D<float4>(_utia_tex_ids[0], uv.x, uv.y))) / M_PIf;
  }

  uint selected_tex = _utia_tex_ids[utia_id];


  uint3 data_size = optix::rtTexSize(selected_tex);

  if (data_size.x * data_size.y == 1) return COLOR(make_float3(optix::rtTex2DFetch<float4>(selected_tex, 0, 0)));


  //Isotropic UTIA -> use built-in interpolation
  bool isotropic = _utia_parameters[utia_id].param == UTIA_2D || _utia_parameters[utia_id].param == RUSINKIEWICZ_2D
                   || _utia_parameters[utia_id].param == RUSINKIEWICZ_2D_COS;
  if (isotropic)
  {
    return fetchColorIsotropic(normal, eye, light, utia_id, data_size);
  }
  else
  {
    return fetchColorAnisotropic(normal, eye, light, utia_id, data_size);
  }
}


COLOR bsdf_eval(float3 normal, float3 eye, float3 light, int lobe)
{
  if (_utia_tex_ids.size() <= 0) return COLOR(0.f);

  float nDotL = dot(light, normal);
  float nDotV = dot(eye, normal);

  if (nDotL <= 0.f || nDotV <= 0.f)
  {
    return COLOR(0.f);
  }

  float2 uv       = make_float2(texcoord.x - floorf(texcoord.x), texcoord.y - floorf(texcoord.y));
  uint2  tex_size = make_uint2(optix::rtTexSize(_material_tex_id));
  uint2  uvi      = make_uint2(uv * make_float2(tex_size));

  // float utia_id_float = (_utia_tex_ids.size() == 1 || _material_tex_id < 0)? 0.f : optix::rtTex2D<float4>(_material_tex_id, uv.x, uv.y).x;
  float utia_id_float = (_utia_tex_ids.size() == 1 || _material_tex_id < 0)
                            ? 0.f
                            : optix::rtTex2DFetch<float4>(_material_tex_id, uvi.x, uvi.y).x;
  int utia_id = utia_id_float * 255;

  COLOR tmp_color = fetchColor(normal, eye, light, utia_id);
  // COLOR ret_color = (utia_id == -1)? tmp_color : tmp_color * tmp_color;
  COLOR ret_color = tmp_color;

  // if(_interpolate && _utia_tex_ids.size() > 1 && _material_tex_id > 0)
  if (_utia_tex_ids.size() > 1 && _material_tex_id > 0)
  {
    float weight = 1.f;

    int tmp_id;
    utia_id_float = optix::rtTex2DFetch<float4>(_material_tex_id, uvi.x + 1u, uvi.y).x;
    tmp_id        = utia_id_float * 255;
    if (tmp_id != utia_id)
    {
      weight += exp(-1.f);
      tmp_color = fetchColor(normal, eye, light, utia_id);
      // ret_color += ((utia_id == -1)? tmp_color : tmp_color * tmp_color) * exp(-1.f);
      ret_color += tmp_color * exp(-1.f);
    }

    utia_id_float = optix::rtTex2DFetch<float4>(_material_tex_id, uvi.x, uvi.y + 1u).x;
    tmp_id        = utia_id_float * 255;
    if (tmp_id != utia_id)
    {
      weight += exp(-1.f);
      tmp_color = fetchColor(normal, eye, light, utia_id);
      // ret_color += ((utia_id == -1)? tmp_color : tmp_color * tmp_color) * exp(-1.f);
      ret_color += tmp_color * exp(-1.f);
    }

    utia_id_float = optix::rtTex2DFetch<float4>(_material_tex_id, uvi.x + 1u, uvi.y + 1u).x;
    tmp_id        = utia_id_float * 255;
    if (tmp_id != utia_id)
    {
      weight += exp(-sqrtf(2.f));
      tmp_color = fetchColor(normal, eye, light, utia_id) * exp(-sqrtf(2.f));
      // ret_color += ((utia_id == -1)? tmp_color : tmp_color * tmp_color) * -sqrtf(2.f);
      ret_color += tmp_color * -sqrtf(2.f);
    }
    ret_color /= weight;
  }

  return ret_color;
}

COLOR bsdf_eval_optim(float3 h, float3 ffnormal, float3 eye, float3 light, int lobe, float external_pdf)
{
  float pdf = bsdf_pdf(ffnormal, eye, light, lobe);
  if (pdf <= 0.f) return COLOR(0.f);

  float nDotL = abs(dot(light, ffnormal));
  return bsdf_eval(ffnormal, eye, light, lobe) * nDotL / pdf;
}

float3 generate_half_vector(float3 n, float3 in_dir)
{
  return n;
}

float4 generate_reflective_sample(float3 n, float3 in_dir)
{
  float2 rnd2d = sampling2D(
      current_prd.num_sample,
      current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
      current_prd.offset_sampling,
      current_prd.seed);
  float z1 = rnd2d.x;
  float z2 = rnd2d.y;

  float3 p = cosine_sample(z1, z2, in_dir, n);
  return make_float4(normalize(p), ANY_REFL);
}
