/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/utia/backend/optix/utia_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *UTIAPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *UTIAPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  //if (!material) return;
  optix::Material    optix_mat = context->createMaterial();
  OptixBaseMaterial *ret_mat;
  auto               compile_options = cuda_compile_options;

  auto utia = dynamic_cast<mrf::materials::UTIA *>(material);
  if (utia)
  {
    compile_options.push_back("-DUTIA");
    optix_mat["_material_tex_id"]->setInt(-1);
    optix_mat["_parameterization"]->setUint(utia->getParameterization());

    ret_mat = new OptixUTIA(utia, optix_mat, compile_options, ptx_cfg);

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->updateTextures(context, wavelengths);
#else
    ret_mat->updateTextures(context);
#endif

    if (ret_mat->hasBuffer("indices")) ret_mat->getMat()["_utia_tex_ids"]->set(ret_mat->getBuffer("indices"));

    if (ret_mat->hasBuffer("params")) ret_mat->getMat()["_utia_parameters"]->set(ret_mat->getBuffer("params"));

    return ret_mat;
  }

  return nullptr;
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixUTIA::updateTextures(optix::Context &context, std::vector<uint> wavelengths)
#else
void OptixUTIA::updateTextures(optix::Context &context)
#endif
{
  RTformat tex_format;
#ifdef MRF_RENDERING_MODE_SPECTRAL
  tex_format = RT_FORMAT_FLOAT;
#else
  tex_format = RT_FORMAT_FLOAT4;
#endif

  auto utia     = dynamic_cast<mrf::materials::UTIA *>(_mrf_material);
  auto idx_utia = dynamic_cast<mrf::materials::IndexedUTIA *>(_mrf_material);

  if (utia)
  {
    optix::TextureSampler tex_sampler;
    if (utia->hasTexture())
    {
      if (utia->getUTIAData().width() > 0 && utia->getUTIAData().height() > 0)
      {
        if (_textures.find("base") == _textures.end())
        {
          tex_sampler = mrf::optix_backend::createClampedOptixTexture(
              context,
              tex_format,
              utia->getUTIAData().width(),
              utia->getUTIAData().height());
#ifdef MRF_RENDERING_MODE_SPECTRAL
          //TODO ASAP
          //mrf::optix_backend::updateTextureFromSpectralImage(utia->getUTIAData(), tex_sampler, 0);
#else
          mrf::optix_backend::updateTextureFromImage(utia->getUTIAData(), tex_sampler);
#endif
          //_loger.info("Loaded UTIA on GPU");
          _textures["base"] = tex_sampler;
        }
        else
        {
          tex_sampler = _textures["base"];
        }
      }
    }
    else
    {
      //_loger.fatal("UTIA has no data !");
    }

    {
      if (_buffers.find("indices") == _buffers.end())
      {
        auto index_buffer = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_INT);
        index_buffer->setSize(1);
        void *data     = index_buffer->map();
        int * int_data = (int *)(data);
        int_data[0]    = tex_sampler->getId();
        index_buffer->unmap();

        _buffers["indices"] = index_buffer;
      }
    }

    {
      if (_buffers.find("params") == _buffers.end())
      {
        auto param_buffer = context->createBuffer(RT_BUFFER_INPUT);
        param_buffer->setFormat(RT_FORMAT_USER);
        param_buffer->setElementSize(sizeof(int) * 8);
        param_buffer->setSize(1);
        void *data     = param_buffer->map();
        int * int_data = (int *)(data);
        int_data[0]    = utia->getThetaVDiv();
        int_data[1]    = utia->getPhiVDiv();
        int_data[2]    = utia->getThetaIDiv();
        int_data[3]    = utia->getPhiIDiv();
        int_data[4]    = utia->getParameterization();
        int_data[5]    = utia->getInterpolate();
        //_loger.trace(
        //    "T_v:" + std::to_string(int_data[0]) + " | P_v:" + std::to_string(int_data[1])
        //    + " | T_i:" + std::to_string(int_data[2]) + " | P_i:" + std::to_string(int_data[3]));
        param_buffer->unmap();
        _buffers["params"] = param_buffer;
      }
    }
  }
  else if (idx_utia)
  {
    std::vector<unsigned int>          indices;
    std::vector<optix::TextureSampler> textures;
    optix::TextureSampler              tex_sampler;
    for (int i = 0; i < static_cast<int>(idx_utia->depth()); ++i)
    {
      if (idx_utia->hasTexture(i))
      {
        if (idx_utia->getUTIAData(i).width() > 0 && idx_utia->getUTIAData(i).height() > 0)
        {
          if (_textures.find("base" + std::to_string(i)) == _textures.end())
          {
            tex_sampler = mrf::optix_backend::createClampedOptixTexture(
                context,
                tex_format,
                idx_utia->getUTIAData(i).width(),
                idx_utia->getUTIAData(i).height());
            _textures["base" + std::to_string(i)] = tex_sampler;
            indices.push_back(tex_sampler->getId());
#ifdef MRF_RENDERING_MODE_SPECTRAL
            //TODO ASAP
            //mrf::optix_backend::updateTextureFromSpectralImage(utia->getUTIAData(i), tex_sampler, 0);

#else
            mrf::optix_backend::updateTextureFromImage(idx_utia->getUTIAData(i), tex_sampler);
#endif
            //_loger.info("Loaded UTIA on GPU");
          }
          else
          {
            tex_sampler = _textures["base" + std::to_string(i)];
          }
        }
      }
    }

    if (idx_utia->hasMap())
    {
      if (idx_utia->getUTIAMap().width() > 0 && idx_utia->getUTIAMap().height() > 0)
      {
        if (_textures.find("map") == _textures.end())
        {
          tex_sampler = mrf::optix_backend::createClampedOptixTexture(
              context,
              tex_format,
              idx_utia->getUTIAMap().width(),
              idx_utia->getUTIAMap().height());
          _textures["map"] = tex_sampler;
          mrf::optix_backend::updateTextureFromImage(idx_utia->getUTIAMap(), tex_sampler);
          //_loger.info("Loaded material map on GPU");
        }
        else
        {
          tex_sampler = _textures["map"];
        }
      }
    }

    {
      if (_buffers.find("indices") == _buffers.end())
      {
        auto index_buffer = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_INT);
        index_buffer->setSize(indices.size());
        void *data     = index_buffer->map();
        int * int_data = (int *)(data);
        for (int i = 0; i < indices.size(); ++i)
          int_data[i] = indices[i];
        index_buffer->unmap();

        _buffers["indices"] = index_buffer;
      }
    }

    {
      if (_buffers.find("params") == _buffers.end())
      {
        auto param_buffer = context->createBuffer(RT_BUFFER_INPUT);
        param_buffer->setFormat(RT_FORMAT_USER);
        //_loger.info(std::to_string(sizeof(int) * 6));
        param_buffer->setElementSize(sizeof(int) * 8);   //Padding to align on 32bit
        param_buffer->setSize(indices.size());
        void *data     = param_buffer->map();
        int * int_data = (int *)(data);
        for (int i = 0; i < indices.size(); ++i)
        {
          int_data[i * 6]     = idx_utia->getThetaVDiv(i);
          int_data[i * 6 + 1] = idx_utia->getPhiVDiv(i);
          int_data[i * 6 + 2] = idx_utia->getThetaIDiv(i);
          int_data[i * 6 + 3] = idx_utia->getPhiIDiv(i);
          int_data[i * 6 + 4] = idx_utia->getParameterization(i);
          int_data[i * 6 + 5] = idx_utia->getInterpolate(i);
          //_loger.trace(
          //    "Index: " + std::to_string(i) + " | T_v:" + std::to_string(int_data[i * 4])
          //    + " | P_v:" + std::to_string(int_data[i * 4 + 1]) + " | T_i:" + std::to_string(int_data[i * 4 + 2])
          //    + " | P_i:" + std::to_string(int_data[i * 4 + 3]));
        }
        param_buffer->unmap();
        _buffers["params"] = param_buffer;
      }
    }
  }
}


}   // namespace optix_backend
}   // namespace mrf
