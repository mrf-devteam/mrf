/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 * Modifications: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_plugins/materials/phong/l_phong.hpp>
#include <mrf_core/image/color_image.hpp>

#ifdef MRF_RENDERING_MODE_SPECTRAL
#include <mrf_core/image/uniform_spectral_image.hpp>
#endif

#include <memory>

namespace mrf
{
namespace materials
{
    
namespace BRDFTypes
{
static char const *PHONG_NORMALIZED_TEXTURED = "PhongNormalizedTextured";
};   // namespace BRDFTypes

//-------------------------------------------------------------------//
//---------------        Physical Phong Model by Lafortune CLASS   --//
//-------------------------------------------------------------------//
class MRF_PLUGIN_EXPORT LPhysicalPhongTextured: public LPhysicalPhong
{
 protected:

#ifdef MRF_RENDERING_MODE_SPECTRAL
  //diffuse texture
  std::unique_ptr<mrf::image::UniformSpectralImage> _d_texture;
  //specular texture
  std::unique_ptr<mrf::image::UniformSpectralImage> _s_texture;
#else
  //diffuse texture
  std::unique_ptr<mrf::image::ColorImage> _d_texture;
  //specular texture
  std::unique_ptr<mrf::image::ColorImage> _s_texture;
#endif
  //normal map
  std::unique_ptr<mrf::image::ColorImage> _normal_map;
  //phong exponent texture
  std::unique_ptr<mrf::image::ColorImage> _exp_map;
  //texture repeat
  float _texture_repeat;

 public:
   LPhysicalPhongTextured(std::string const & name,
     COLOR const & /*dcolor*/ = COLOR(0.0f),
     float const /* dalbedo */ = 0.1f,
     COLOR const & /*scolor*/ = COLOR(0.5f),
     float const /* salbedo*/ = 0.7f,
     unsigned int const /*exp*/ = 10,
     float texture_repeat = 1.f);
  virtual ~LPhysicalPhongTextured();

  //-------------------------------------------------------------------------
  // Some Setters
  //-------------------------------------------------------------------------
  mrf::image::IMAGE_LOAD_SAVE_FLAGS loadDiffuseTexture(std::string const & path);
  mrf::image::IMAGE_LOAD_SAVE_FLAGS loadSpecularTexture(std::string const & path);
  mrf::image::IMAGE_LOAD_SAVE_FLAGS loadNormalMap(std::string const & path);
  mrf::image::IMAGE_LOAD_SAVE_FLAGS loadExponentMap(std::string const & path);

  bool hasDiffuseTexture() const { return _d_texture.get() != nullptr; }
  bool hasSpecularTexture() const { return _s_texture.get() != nullptr; }
  bool hasNormalTexture() const { return _normal_map.get() != nullptr; }
  bool hasExponentTexture() const { return _exp_map.get() != nullptr; }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  mrf::image::UniformSpectralImage const & getDiffuseTexture()const
#else
  mrf::image::ColorImage const & getDiffuseTexture()const
#endif
  {
    return *_d_texture.get();
  };

#ifdef MRF_RENDERING_MODE_SPECTRAL
  mrf::image::UniformSpectralImage const & getSpecularTexture()const
#else
  mrf::image::ColorImage const & getSpecularTexture()const
#endif
  {
    return *_s_texture.get();
  };

  mrf::image::ColorImage const & getNormalMap()const
  {
    return *_normal_map.get();
  };
  mrf::image::ColorImage const & getExponentMap()const
  {
    return *_exp_map.get();
  };
  float textureRepeat()const
  {
    return _texture_repeat;
  }
  void setTextureRepeat(float texture_repeat)
  {
    _texture_repeat = texture_repeat;
  }


  //-------------------------------------------------------------------------
  // UMat Interface Methods Implementation
  //-------------------------------------------------------------------------
  virtual float brdfValue( mrf::geom::LocalFrame const & lf,
                           mrf::math::Vec3f const & in_dir,
                           mrf::math::Vec3f const & out_dir ) const ;

  virtual RADIANCE_TYPE coloredBRDFValue( mrf::geom::LocalFrame const & lf,
                                          mrf::math::Vec3f const & in_dir,
                                          mrf::math::Vec3f const & out_dir ) const;
};


}
}
