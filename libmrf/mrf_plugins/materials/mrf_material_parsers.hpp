/*
 *
 * author : Arthur Dufay
 * Copyright INRIA 2018
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>


#include <mrf_core/io/base_plugin_parser.hpp>

#include <mrf_plugins/materials/mrf_materials.hpp>

#include <mrf_plugins/materials/checkerboard/checkerboard_parser.hpp>
#include <mrf_plugins/materials/debug/debug_parser.hpp>
#include <mrf_plugins/materials/lambert/lambert_parser.hpp>
#include <mrf_plugins/materials/measured/measured_parser.hpp>
#include <mrf_plugins/materials/microfacet/microfacet_parser.hpp>
#include <mrf_plugins/materials/phong/phong_parser.hpp>
#include <mrf_plugins/materials/principled/principled_parser.hpp>
#include <mrf_plugins/materials/shadow_catcher/shadow_catcher_parser.hpp>
#include <mrf_plugins/materials/utia/utia_parser.hpp>
#include <mrf_plugins/materials/emittance/emittance_parser.hpp>

//TODO: uncomment and adjust template according to your material.
// #include <mrf_plugins/materials/material_template/template_parser.hpp>