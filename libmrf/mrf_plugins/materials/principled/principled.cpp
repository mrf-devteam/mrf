#include "principled.hpp"
#include <mrf_core/sampling/random_generator.hpp>

namespace mrf
{
namespace materials
{
using namespace mrf::sampling;
using namespace math;


//-------------------------------------------------------------------//
//---------------        Principled BRDF by Disney                 --//
//-------------------------------------------------------------------//
PrincipledDisney::PrincipledDisney(
    std::string const &name,

    RADIANCE_TYPE const &baseColor,
    float const          roughness,
    float const          subsurface,
    float const          sheen,
    float const          sheenTint,
    float const          clearcoat,
    float const          clearcoatGloss,
    float const          metallic,
    float const          specular,
    float const          specularTint,
    float const          anisotropic)
  : BRDF(name, BRDFTypes::PRINCIPLED)
  , _baseColor(baseColor)
  , _roughness(roughness)
  , _subsurface(subsurface)
  , _sheen(sheen)
  , _sheenTint(sheenTint)
  , _clearcoat(clearcoat)
  , _clearcoatGloss(clearcoatGloss)
  , _metallic(metallic)
  , _specular(specular)
  , _specularTint(specularTint)
  , _anisotropic(anisotropic)
{}


PrincipledDisney::~PrincipledDisney() {}


//-------------------------------------------------------------------
float PrincipledDisney::brdfValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  //mrf::math::Vec3f const reflectedDir = 2.f * (in_dir.dot(lf.normal())) * lf.normal() - in_dir;
  //TODO: CODE ME
  return 0.f;
}


RADIANCE_TYPE
PrincipledDisney::coloredBRDFValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  //TODO CODE ME
  //mrf::math::Vec3f const reflectedDir = 2.f * (in_dir.dot(lf.normal())) * lf.normal() - in_dir;
  return RADIANCE_TYPE(0.f);
}

//-------------------------------------------------------------------
// Some PDFs
//-------------------------------------------------------------------
float PrincipledDisney::diffPDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return gneg(lf.normal().dot(in_dir)) / static_cast<float>(Math::PI);
}

float PrincipledDisney::specPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  //TODO CODE ME
  //mrf::math::Vec3f const reflectedDir = (-2.f*(in_dir.dot(lf.normal())))*lf.normal()+in_dir;
  //mrf::math::Vec3f const reflectedDir = 2.f * (in_dir.dot(lf.normal())) * lf.normal() - in_dir;
  //return static_cast<float>(
  //    powf(gpos(reflectedDir.dot(out_dir)), static_cast<float>(_exp)) * static_cast<float>(_exp + 2)
  //    / (2.f * Math::PI));
  return 0.f;
}

float PrincipledDisney::transPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

float PrincipledDisney::nonDiffusePDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  //TODO CODE ME
  //mrf::math::Vec3f const reflectedDir = (-2.f*(in_dir.dot(lf.normal())))*lf.normal()+in_dir;
  //mrf::math::Vec3f const reflectedDir = 2.f * (in_dir.dot(lf.normal())) * lf.normal() - in_dir;
  //return static_cast<float>(
  //    powf(gpos(reflectedDir.dot(out_dir)), static_cast<float>(_exp)) * static_cast<float>(_exp + 2)
  //    / (2.f * Math::PI));
  return 0.f;
}


//-------------------------------------------------------------------
// Scatter Event
//-------------------------------------------------------------------
ScatterEvent::SCATTER_EVT PrincipledDisney::generateScatteringDirection(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    RADIANCE_TYPE & /*incident_radiance*/,
    Vec3f & /*outgoing_dir*/) const
{
  //TODO CODE ME
  return ScatterEvent::ANY_REFL;
}
}   // namespace materials
}   // namespace mrf
