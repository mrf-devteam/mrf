/*
 *
 * author : Leo Ackerman (TODO add mail)
 * Copyright INRIA 2020
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/brdf.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
//Principled BRDF, blending physical results and artistic purposes
static char const *PRINCIPLED = "principled";
};   // namespace BRDFTypes

//-------------------------------------------------------------------//
//---------------        Principled BRDF by Disney                 --//
//-------------------------------------------------------------------//
class MRF_PLUGIN_EXPORT PrincipledDisney: public BRDF
{
protected:
  RADIANCE_TYPE _baseColor;
  float         _roughness;
  float         _subsurface;
  float         _sheen;
  float         _sheenTint;
  float         _clearcoat;
  float         _clearcoatGloss;
  float         _metallic;
  float         _specular;
  float         _specularTint;
  float         _anisotropic;


public:
  PrincipledDisney(
      std::string const &name,
      // Terms from Disney
      RADIANCE_TYPE const &baseColor      = RADIANCE_TYPE(1.f),
      float const          roughness      = 0.0f,
      float const          subsurface     = 0.0f,
      float const          sheen          = 0.0f,
      float const          sheenTint      = 0.0f,
      float const          clearcoat      = 0.0f,
      float const          clearcoatGloss = 0.0f,
      float const          metallic       = 0.0f,
      float const          specular       = 0.0f,
      float const          specularTint   = 0.0f,
      float const          anisotropic    = 0.0f);

  virtual ~PrincipledDisney();

  //-------------------------------------------------------------------------
  // Some Setters
  //-------------------------------------------------------------------------
  inline void setBaseColor(RADIANCE_TYPE const &a_color);
  inline void setRoughness(float a_roughness);
  inline void setSubsurface(float a_subsurface);
  inline void setSheen(float a_sheen);
  inline void setSheenTint(float a_sheenTint);
  inline void setClearcoat(float a_clearcoat);
  inline void setClearcoatGloss(float a_clearcoatGloss);
  inline void setMetallic(float a_metallic);
  inline void setSpecular(float a_specular);
  inline void setSpecularTint(float a_specularTint);
  inline void setAnisotropic(float a_anisotropic);

  //-------------------------------------------------------------------------
  // Some Getters
  //-------------------------------------------------------------------------

  inline RADIANCE_TYPE getBaseColor() const;
  inline float         getBaseColorTint() const;
  inline float         getRoughness() const;
  inline float         getSubsurface() const;
  inline float         getSheen() const;
  inline float         getSheenTint() const;
  inline float         getClearcoat() const;
  inline float         getClearcoatGloss() const;
  inline float         getMetallic() const;
  inline float         getSpecular() const;
  inline float         getSpecularTint() const;
  inline float         getAnisotropic() const;


  //-------------------------------------------------------------------------
  // UMat Interface Methods Implementation
  //-------------------------------------------------------------------------
  virtual float
  brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;

  //-------------------------------------------------------------------------
  // Some PDFs and Scatter Event
  //-------------------------------------------------------------------------
  virtual float
  diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;
};


//-------------------------------------------------------------------------
// Implements Setters and Getters
//-------------------------------------------------------------------------
inline void PrincipledDisney::setBaseColor(RADIANCE_TYPE const &a_color)
{
  _baseColor = a_color;
}

inline void PrincipledDisney::setRoughness(float a_roughness)
{
  _roughness = a_roughness;
}

inline void PrincipledDisney::setSubsurface(float a_subsurface)
{
  _subsurface = a_subsurface;
}

inline void PrincipledDisney::setSheen(float a_sheen)
{
  _sheen = a_sheen;
}

inline void PrincipledDisney::setSheenTint(float a_sheenTint)
{
  _sheenTint = a_sheenTint;
}

inline void PrincipledDisney::setClearcoat(float a_clearcoat)
{
  _clearcoat = a_clearcoat;
}

inline void PrincipledDisney::setClearcoatGloss(float a_clearcoatGloss)
{
  _clearcoatGloss = a_clearcoatGloss;
}

inline void PrincipledDisney::setMetallic(float a_metallic)
{
  _metallic = a_metallic;
}

inline void PrincipledDisney::setSpecular(float a_specular)
{
  _specular = a_specular;
}

inline void PrincipledDisney::setSpecularTint(float a_specularTint)
{
  _specularTint = a_specularTint;
}

inline void PrincipledDisney::setAnisotropic(float a_anisotropic)
{
  _anisotropic = a_anisotropic;
}


//-------------------------------------------------------------------------
inline RADIANCE_TYPE PrincipledDisney::getBaseColor() const
{
  return _baseColor;
}

inline float PrincipledDisney::getBaseColorTint() const
{
  return _baseColor.luminance();
}

inline float PrincipledDisney::getRoughness() const
{
  return _roughness;
}

inline float PrincipledDisney::getSubsurface() const
{
  return _subsurface;
}

inline float PrincipledDisney::getSheen() const
{
  return _sheen;
}

inline float PrincipledDisney::getSheenTint() const
{
  return _sheenTint;
}

inline float PrincipledDisney::getClearcoat() const
{
  return _clearcoat;
}

inline float PrincipledDisney::getClearcoatGloss() const
{
  return _clearcoatGloss;
}

inline float PrincipledDisney::getMetallic() const
{
  return _metallic;
}

inline float PrincipledDisney::getSpecular() const
{
  return _specular;
}

inline float PrincipledDisney::getSpecularTint() const
{
  return _specularTint;
}

inline float PrincipledDisney::getAnisotropic() const
{
  return _anisotropic;
}



}   // namespace materials
}   // namespace mrf
