#pragma once

#include <mrf_core_dll.hpp>

#ifndef __APPLE__
#  include <externals/GL/glew.h>
#endif

namespace mrf
{
namespace gui
{
class GLBuffer
{
public:
  GLBuffer();
  GLBuffer(size_t width, size_t height, size_t depth, GLenum format = GL_RGBA32F_ARB);

  GLuint getID() const { return _GLBufferID; }
  GLenum getGLBufferType() const { return _GLBufferType; }
  size_t getElementSize() const { return _elmt_size; }

  void resizeBuffer(size_t width, size_t height, size_t depth);

private:
  GLenum _GLBufferType;
  GLuint _GLBufferID;
  size_t _elmt_size;
};
}   // namespace gui
}   // namespace mrf
