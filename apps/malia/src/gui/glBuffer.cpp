#include "glBuffer.h"
#include "gl_helper.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>

namespace mrf
{
namespace gui
{
GLBuffer::GLBuffer(): _GLBufferType(GL_PIXEL_UNPACK_BUFFER), _GLBufferID(GL_INVALID_INDEX), _elmt_size(4) {}

GLBuffer::GLBuffer(size_t width, size_t height, size_t depth, GLenum format): GLBuffer()
{
  switch (format)
  {
    case GL_R32F:
    {
      _elmt_size = sizeof(float);
      break;
    }
    case GL_RG32F:
    {
      _elmt_size = 2 * sizeof(float);
      break;
    }
    case GL_RGB32F_ARB:
    {
      _elmt_size = 3 * sizeof(float);
      break;
    }
    case GL_RGBA32F_ARB:
    default:
    {
      _elmt_size = 4 * sizeof(float);
      break;
    }
  }

  glGenBuffers(1, &_GLBufferID);
  glBindBuffer(_GLBufferType, _GLBufferID);
  glBufferData(_GLBufferType, _elmt_size * width * height * depth, 0, GL_DYNAMIC_DRAW);
  glBindBuffer(_GLBufferType, 0);

  checkErrorGL();
}

void GLBuffer::resizeBuffer(size_t width, size_t height, size_t depth)
{
  glBindBuffer(_GLBufferType, _GLBufferID);
  glBufferData(_GLBufferType, _elmt_size * width * height * depth, 0, GL_DYNAMIC_DRAW);
  glBindBuffer(_GLBufferType, 0);

  checkErrorGL();
}
}   // namespace gui
}   // namespace mrf