#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/image/color_image.hpp>

#ifndef __APPLE__
#  include <externals/GL/glew.h>
#endif

#include "shader.h"
#include "glBuffer.h"

namespace mrf
{
namespace gui
{
class Texture
{
public:
  /*!
   * Default constructor.
   */
  Texture();

  /*!
   * 1D texture constructor.
   *
   * \param width
   * \param format
   * \param name
   * \param use_buffer
   * \param data
   */
  Texture(size_t width, GLenum format, const char *name, bool use_buffer, void *data = nullptr);

  /*!
   * 2D texture constructor.
   *
   * \param width
   * \param format
   * \param name
   * \param use_buffer
   * \param data
   */
  Texture(size_t width, size_t height, GLenum format, const char *name, bool use_buffer, void *data = nullptr);

  /*!
   * 3D texture constructor.
   *
   * \param width
   * \param format
   * \param name
   * \param use_buffer
   * \param data
   */
  Texture(
      size_t      width,
      size_t      height,
      size_t      depth,
      GLenum      format,
      const char *name,
      bool        use_buffer,
      void *      data = nullptr);

  ~Texture();

  /*!
   * Resizes the texture.
   *
   * \param width
   * \param height
   * \param depth
   */
  void resize(size_t width, size_t height, size_t depth);

  bool initTextureFromImage(image::ColorImage const &image, const char *name);

  /*!
   * Activates the texture unit, binds the texture and sends the uniform.
   */
  void displayTextureDirectly();

  /*!
   * Activates the texture unit, binds the texture and sends the uniform.
   * /param prg : the shader program currently used and to which the uniform must be set.
   * /param uniform_name : the uniform name that must be set IF NOT USING THE STORED ONE.
   */
  void activate(Shader *prg, std::string uniform_name = "");

  void activateImage();
  void deactivateImage();

  /*!
   * Deactivates the texture unit and unbinds the texture.
   */
  void deactivate();

  /*!
   * 1D texture updater.
   *
   * \param width
   * \param format
   * \param use_buffer
   * \param data
   */
  void updateTexture(size_t width, GLenum format, bool use_buffer, void *data = nullptr);

  /*!
   * 2D texture updater.
   *
   * \param width
   * \param format
   * \param use_buffer
   * \param data
   */
  void updateTexture(size_t width, size_t height, GLenum format, bool use_buffer, void *data = nullptr);

  /*!
   * 3D texture updater.
   *
   * \param width
   * \param format
   * \param use_buffer
   * \param data
   */
  void updateTexture(size_t width, size_t height, size_t depth, GLenum format, bool use_buffer, void *data = nullptr);


  /*!
   * Update the texture using its internal buffer object, if any. The update is done on device memory.
   */
  void updateTextureFromInternalBuffer();

  /*!
   * Simple getter.
   * \return texture ID.
   */
  GLuint getID() const { return _textureID; }

  /*!
   * Getter for the internal buffer any, if texture is based on an OpenGL buffer for interoperability. If there is none, return GL_INVALID_INDEX.
   * \return internal buffer ID.
   */
  GLuint getInternalBufferID() const { return _internal_buffer.getID(); }

  /*!
   * Simple getter.
   * \return texture unit.
   */
  GLuint getTexUnit() const { return _textureUnit; }

  /*!
   * Simple getter.
   * \return texture type.
   */
  GLenum getTextureType() const { return _textureType; }

  /*!
   * Simple getter.
   * \return texture format.
   */
  GLenum getTextureFormat() const { return _textureFormat; }

  /*!
   * Simple getter.
   * \return texture pixel format.
   */
  GLenum getPixelFormat() const { return _pixelFormat; }

  /*!
   * Simple getter.
   * \return texture data type.
   */
  GLenum getDataType() const { return _dataType; }

  /*!
   * Getter for the name used as uniform for the texture.
   * \return name.
   */
  const char *getName() const { return _uniformName; }

  /*!
   * Simple getter.
   * \return width.
   */
  size_t getWidth() const { return _width; }

  /*!
   * Simple getter.
   * \return width.
   */
  size_t getHeight() const { return _height; }

  /*!
   * Simple getter.
   * \return width.
   */
  size_t getDepth() const { return _depth; }

private:
  /*!
   * Helper to fill in texture format, pixel format and datatype based on requested GL format.
   * /param format
   */
  void glFormat(GLenum format);

private:
  GLBuffer _internal_buffer;

  GLenum      _textureType;
  GLenum      _textureFormat;
  GLenum      _pixelFormat;
  GLenum      _dataType;
  GLuint      _textureID;
  GLuint      _textureUnit;
  const char *_uniformName;
  bool        _hasData;

  size_t _width;
  size_t _height;
  size_t _depth;
};
}   // namespace gui
}   // namespace mrf
