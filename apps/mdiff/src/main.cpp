/**
 * Author : Romain Pacanowski @ institutoptique . fr
 * Copyright : CNRS 2020
 **/



#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <vector>

#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/image_module.hpp>
#include <mrf_core/image/image_diff.hpp>

#include <mrf_core/util/prog_options.hpp>
#include <mrf_core/util/string_parsing.hpp>

using namespace std;
using namespace mrf::util;
using namespace mrf::image;

//------------------------------------------------------------------------------------------------------------

// Supported Metric when completed
std::vector<std::string> valid_metric_names = {"MSE", "RMSE", "SMAPE"};

// Current one
//std::vector<std::string> valid_metric_names = {"MSE", "RMSE"};
//------------------------------------------------------------------------------------------------------------



//------------------------------------------------------------------------------------------------------------
bool valid_image_formats(std::string const &ext1, std::string const &ext2)
{
  using namespace mrf::image;


  if (isLDRExtension(ext1.c_str()) && isLDRExtension(ext2.c_str()))
  {
    return true;
  }

  if (isSpectralExtension(ext1.c_str()) && isSpectralExtension(ext2.c_str()))
  {
    return true;
  }

  if (isHDRExtension(ext1.c_str()) && isHDRExtension(ext2.c_str()))
  {
    return true;
  }


  return false;
}

void construct_options(mrf::util::ProgOptions &mdiff_options)
{
  mdiff_options.addOption("i", ProgArg(true, "{string} Input Files ", 2));
  mdiff_options.addOption(
      "o",
      ProgArg(
          false,
          "{string} Output File for the difference Image. This option forces mdiff to write the difference image regardless of the difference value.",
          1));
  mdiff_options.addOption("metric", ProgArg(false, "{string: MSE | RMSE | SMAPE} Type of metric to compare images", 1));
  mdiff_options.addOption(
      "diff_on_error",
      ProgArg(
          false,
          "{Save the difference image *ONLY* if the difference computed with the given metric is above precision threshold}",
          0));
  mdiff_options.addOption(
      "precision",
      ProgArg(
          false,
          "{float : number representing a precision value to compare both images. The larger the number the more tolerant on the difference  ",
          1));
}

void process_command_line_options(int argc, char **argv, mrf::util::ProgOptions &mdiff_options)
{
  construct_options(mdiff_options);

  if (!mrf::util::validateProgramOptions(argc, argv, mdiff_options))
  {
    throw runtime_error("command line options : bad options error");
  }
}


struct MDiffOptions
{
  // Default Values
  MDiffOptions()
  {
    image_filenames.clear();
    output_filename.clear();
    metric_name = "MSE";
    precision   = 1e-4f;
    diff_img    = false;
  }

  std::vector<std::string> image_filenames;
  std::string              output_filename;
  std::string              metric_name;
  float                    precision;
  bool                     diff_img;
};

void verify_command_line_arguments(mrf::util::ProgOptions const &mdiff_options, MDiffOptions &option_values)
{
  //CLEAR STUFF
  option_values.image_filenames.clear();


  if (mdiff_options.option("i").wasProvided())
  {
    assert(mdiff_options.option("i").nbParameter() == 2);

    for (unsigned int k = 0; k < mdiff_options.option("i").nbParameter(); k++)
    {
      string input_filename;
      mdiff_options.option("i").paramValue(k, input_filename);

      option_values.image_filenames.push_back(input_filename);
    }
    assert(option_values.image_filenames.size() == 2);


    string ext1, ext2;
    StringParsing::getFileExtension(option_values.image_filenames[0], ext1);
    StringParsing::getFileExtension(option_values.image_filenames[1], ext2);

    // CHECK That extensions are consistent
    if (!valid_image_formats(ext1, ext2))
    {
      throw std::runtime_error("Images must be of the same Format Type (LDR or HDR or Spectral)");
    }
  }


  // IF an output was requested check it
  if (mdiff_options.option("o").wasProvided())
  {
    string output_filename;
    mdiff_options.option("o").paramValue(0, output_filename);


    string ext;
    StringParsing::getFileExtension(output_filename, ext);
    if (ext.size() == 0) StringParsing::getFileExtension(option_values.image_filenames[0], ext);

    string output_filename_wo_extension;
    StringParsing::getFileNameWithoutExtension(output_filename, output_filename_wo_extension);

    option_values.output_filename = output_filename_wo_extension + "." + ext;
  }


  if (mdiff_options.option("metric").wasProvided())
  {
    string parsed_metric_name;
    mdiff_options.option("metric").paramValue(0, parsed_metric_name);

    vector<string>::iterator it = std::find(valid_metric_names.begin(), valid_metric_names.end(), parsed_metric_name);


    if (it == valid_metric_names.end())
    {
      throw std::runtime_error("Invalid Metric Names !!!  See mdiff -h for help ");
    }

    option_values.metric_name = parsed_metric_name;
    std::cout << " Metric is " << option_values.metric_name << std::endl;
  }


  if (mdiff_options.option("precision").wasProvided())
  {
    string epsilon_str;
    mdiff_options.option("precision").paramValue(0, epsilon_str);

    float epsilon;
    try
    {
      epsilon = std::stof(epsilon_str);
    }
    catch (const std::exception &)
    {
      throw std::runtime_error("Precision must be a number !!! See mdiff -h for help");
    }


    if (epsilon < 0.0f)
    {
      throw std::runtime_error("Precision must be positive!!! See mdiff -h for help");
    }

    option_values.precision = epsilon;
  }

  if (mdiff_options.option("diff_on_error").wasProvided())
  {
    option_values.diff_img = true;
  }
}


template<class IMAGE_TYPE>
int diff_two_images(IMAGE_TYPE const &img1, IMAGE_TYPE const &img2, MDiffOptions const &option_values)
{
  float difference_value_from_metric;

  if (option_values.metric_name == "MSE")
  {
    difference_value_from_metric = mrf::image::mse_diff<IMAGE_TYPE>(img1, img2);
  }
  else if (option_values.metric_name == "RMSE")
  {
    difference_value_from_metric = mrf::image::rmse_diff<IMAGE_TYPE>(img1, img2);
  }
  else if (option_values.metric_name == "SMAPE")
  {
    difference_value_from_metric = mrf::image::mean_smape(img1, img2);
  }

  else
  {
    //CANNOT HAPPEN because invalid metric are catch before !
    //throw runtime_error("METRIC NOT SUPPORTED...SORRY :(( ");
  }

  std::cout << "[INFO] Difference Value from metric: " << difference_value_from_metric << std::endl;

  if (difference_value_from_metric < option_values.precision)
  {
    std::cout << "[INFO] Images are the same !!! For the specified Metric and the given precision " << std::endl;

    return EXIT_SUCCESS;
  }
  else
  {
    return EXIT_FAILURE;
  }
}

template<class IMAGE_TYPE>
int diff_two_images(
    IMAGE_TYPE const &  img1,
    IMAGE_TYPE const &  img2,
    IMAGE_TYPE &        diffImg,
    MDiffOptions const &option_values)
{
  float difference_value_from_metric;

  if (option_values.metric_name == "MSE")
  {
    difference_value_from_metric = mrf::image::mse_diff<IMAGE_TYPE>(img1, img2, diffImg);
  }
  else if (option_values.metric_name == "RMSE")
  {
    difference_value_from_metric = mrf::image::rmse_diff<IMAGE_TYPE>(img1, img2, diffImg);
  }
  else if (option_values.metric_name == "SMAPE")
  {
    difference_value_from_metric = mrf::image::mean_smape(img1, img2, diffImg);
  }
  else
  {
    //CANNOT HAPPEN because invalid metric are catch before !
    //throw runtime_error("METRIC NOT SUPPORTED...SORRY :(( ");
  }

  std::cout << "[INFO] Difference Value from metric: " << difference_value_from_metric << std::endl;

  if (difference_value_from_metric < option_values.precision)
  {
    std::cout << "[INFO] Images are the same !!! For the specified Metric and the given precision " << std::endl;

    std::string diff_file;
    if (option_values.output_filename.size() > 0)
    {
      std::cout << "[INFO] Difference Image will be saved to: " << option_values.output_filename << std::endl;
      diff_file = option_values.output_filename;

      diffImg.save(diff_file);
    }

    return EXIT_SUCCESS;
  }
  else
  {
    if (option_values.diff_img
        || option_values.output_filename.size() > 0)   //diff_on_error requested OR -o was provided
    {
      std::string diff_file;
      if (option_values.output_filename.size() > 0)
        diff_file = option_values.output_filename;
      else   //Find a filename to save the difference image
      {
        std::string cur_dir;
        mrf::util::StringParsing::getDirectory(option_values.image_filenames[0].c_str(), cur_dir);

        std::string in_name;
        mrf::util::StringParsing::fileNameFromAbsPathWithoutExtension(
            option_values.image_filenames[0].c_str(),
            in_name);

        std::string out_ext;
        mrf::util::StringParsing::getFileExtension(option_values.image_filenames[0], out_ext);

        diff_file = cur_dir + in_name + "_diffError." + out_ext;
      }
      std::cout << "[INFO] Difference Image will be saved to: " << diff_file << std::endl;
      diffImg.save(diff_file);
    }
    return EXIT_FAILURE;
  }
}

int main(int argc, char **argv)
{
  ProgOptions mdiff_options("mdiff");   //String values for the command line options


  if (argc == 1)
  {
    construct_options(mdiff_options);
    mdiff_options.displayHelp();

    return EXIT_FAILURE;
  }

  try
  {
    std::vector<std::string> image_filenames;


    process_command_line_options(argc, argv, mdiff_options);

    if ((mdiff_options.hasOption("h") && mdiff_options.option("h").wasProvided())
        || (mdiff_options.hasOption("help") && mdiff_options.option("help").wasProvided()))
    {
      mdiff_options.displayHelp();
      return EXIT_SUCCESS;
    }



    MDiffOptions option_values;   // Type of the options
    verify_command_line_arguments(mdiff_options, option_values);

    string ext1, ext2;
    StringParsing::getFileExtension(option_values.image_filenames[0], ext1);
    StringParsing::getFileExtension(option_values.image_filenames[1], ext2);

#ifdef MDIFF_DEBUG
    std::cout << __FILE__ << " " << __LINE__ << option_values.image_filenames[0] << " "
              << option_values.image_filenames[1] << std::endl;
    std::cout << " ext1 = " << ext1 << " " << ext2 << std::endl;
#endif

    if (isLDRExtension(ext1.c_str()))
    {
      std::cout << __FILE__ << " " << __LINE__ << std::endl;

      std::unique_ptr<ColorImage> img1 = loadColorImage(option_values.image_filenames[0]);
      std::unique_ptr<ColorImage> img2 = loadColorImage(option_values.image_filenames[1]);
      if (option_values.diff_img || mdiff_options.option("o").wasProvided())
      {
        std::unique_ptr<ColorImage> diffImg = loadColorImage(option_values.image_filenames[0]);

        return diff_two_images<ColorImage>(*img1, *img2, *diffImg, option_values);
      }
      else
        return diff_two_images<ColorImage>(*img1, *img2, option_values);
    }
    else if (isHDRExtension(ext1.c_str()))
    {
      if (ext1 == WARD_EXTENSION && !isRadianceHDR(option_values.image_filenames[0]))
      {
        // This is a Spectral Case ! Will be processed after !!!
      }
      else if (ext2 == WARD_EXTENSION && !isRadianceHDR(option_values.image_filenames[1]))
      {
        // This is a Spectral Case ! Will be processed after !!!
      }
      else
      {
        // Processing HDR (.hdr Ward or .exr format) Images
        std::unique_ptr<ColorImage> img1 = loadColorImage(option_values.image_filenames[0]);
        std::unique_ptr<ColorImage> img2 = loadColorImage(option_values.image_filenames[1]);
        if (option_values.diff_img || mdiff_options.option("o").wasProvided())
        {
          std::unique_ptr<ColorImage> diffImg = loadColorImage(option_values.image_filenames[0]);

          return diff_two_images<ColorImage>(*img1, *img2, *diffImg, option_values);
        }
        else
          return diff_two_images<ColorImage>(*img1, *img2, option_values);
      }
    }

    if (isSpectralExtension(ext1.c_str()))
    {
      std::unique_ptr<UniformSpectralImage> img1 = loadUniformSpectralImage(option_values.image_filenames[0]);
      std::unique_ptr<UniformSpectralImage> img2 = loadUniformSpectralImage(option_values.image_filenames[1]);
      if (option_values.diff_img || mdiff_options.option("o").wasProvided())
      {
        std::unique_ptr<UniformSpectralImage> diffImg = loadUniformSpectralImage(option_values.image_filenames[0]);

        return diff_two_images<UniformSpectralImage>(*img1, *img2, *diffImg, option_values);
      }
      else
        return diff_two_images<UniformSpectralImage>(*img1, *img2, option_values);
    }
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << '\n';
    return EXIT_FAILURE;
  }

  // NEVER REACHed
  //return EXIT_SUCCESS;
}
