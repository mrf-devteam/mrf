set(PROGRAM_NAME mdiff)

if(APPLE)
  set(CMAKE_INSTALL_RPATH "@loader_path/../lib;@loader_path")
elseif(UNIX)
  set(CMAKE_INSTALL_RPATH "$ORIGIN/../lib:$ORIGIN/")
endif()

# Define debug. TODO: shall be done by configure in file
if(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
  string(TOUPPER ${PROGRAM_NAME} PROGRAM_NAME_UPPER)
  add_definitions(-D${PROGRAM_NAME_UPPER}_DEBUG)
endif()

# External Libraries coming from rgb2spec application
include_directories(externals/)
include_directories(externals/QuadProg)

# Sources
add_executable(${PROGRAM_NAME} src/main.cpp)

set_target_properties( ${PROGRAM_NAME} 
  PROPERTIES     
  ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/$<CONFIG>"
  LIBRARY_OUTPUT_DIRECTORY "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/$<CONFIG>"
  RUNTIME_OUTPUT_DIRECTORY "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/$<CONFIG>")

# We link with mrf directly
target_link_libraries(${PROGRAM_NAME} PUBLIC mrf_core_spectral)

install(TARGETS ${PROGRAM_NAME}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  COMPONENT mdiff
)
