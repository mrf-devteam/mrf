import bpy
import os,sys
import subprocess
import time
import platform
import importlib
import xml.etree.ElementTree as ET
import os.path
import logging
import shutil
import math
import importlib
import inspect

from bpy.types import (
  Panel,
  PropertyGroup,
  AddonPreferences,
  PointerProperty,
  )

from bpy.props import (
  StringProperty,
  )

bl_info = {
  "name": "MRF Blender Bridge",
  "description": "Blender bridge for MRF and Malia renderer",
  "blender": (2, 80, 0),
  "category": "Render",
  "support" : "TESTING",
  "wiki_url": "https://pacanows.gitlabpages.inria.fr/MRF/blender_integration.md.html",
}

GLOBAL_LOGGING_LEVEL_VALUE = logging.DEBUG
logger = logging.getLogger("BRIDGE_BLENDER_MRF")
logger.setLevel(GLOBAL_LOGGING_LEVEL_VALUE)
logging.basicConfig(level=GLOBAL_LOGGING_LEVEL_VALUE)

CURRENT_DIRECTORY = os.path.dirname(__file__) + "/lib/"
print(CURRENT_DIRECTORY)

if not CURRENT_DIRECTORY in sys.path:
  sys.path.append( CURRENT_DIRECTORY )


if bpy.app.version < (2,80,0):
  import libblender_legacy as libblender2msf
else:
  import libblender2_8 as libblender2msf


import libmrftools as mrf

def get_mrf_tools():
  CURRENT_DIRECTORY = os.path.dirname(__file__)
  list_of_paths = [CURRENT_DIRECTORY]

  # AF: this is irrelevant for now, can be added later on to avoid having two
  # installations at different location
  # if platform.system() == "Windows":
  #   list_of_paths += ["C:\\Program Files\\"]

  logger.debug(" List of paths to look for " + str(list_of_paths) )
  logger.debug(" Path Directories will also be checked ")

  mrf_tools = mrf.find_mrf_tools( list_of_paths )

  return mrf_tools


# ----------------------------------------------------------------------------
# Utility Functions
# ----------------------------------------------------------------------------

def openImageFromCurrentCameraWithSpectralViewer():
  current_output_dir  = os.path.dirname( bpy.data.scenes[0].output_dir ) + "/"
  logger.debug("Output dir for scene export is: %s" , current_output_dir )
  logger.debug( " Number of cameras in Scene %i" , len(bpy.data.cameras.items() ))

  # Get the name of the Scene
  blender_file_name = os.path.basename(bpy.data.filepath)
  blender_file_name = os.path.splitext(blender_file_name)[0]

  number_of_cameras_in_scene = len(bpy.data.cameras.items())

  # If we have more than 2 cameras. need to open the active one
  if number_of_cameras_in_scene > 1 :
    # From Context
    # This means that the view is active to a camera
    obj_camera = bpy.context.scene.camera

    # Trying to identify which camera is selected
    if obj_camera is None:
      logger.info("No camera active")
    elif obj_camera.type == 'CAMERA':
      logger.info("Regular scene cam")
    else:
      logger.info("{} object as camera".format(obj_camera.type))

    ob = bpy.context.object
    if ob is not None and ob.type == 'CAMERA':
      logger.debug("Active camera object")
      obj_camera = ob

    logger.debug(obj_camera.name)

    a_dict = bpy.data.cameras.items()
    logger.debug(a_dict)
    
    index_cam = bpy.data.cameras.find( obj_camera.name )
    if index_cam == -1:
      logger.warn("Camera not Found. VIEWER not launched ")
      return
    else:
      active_camera_number_str = str(index_cam).zfill( round(math.log10(number_of_cameras_in_scene)+1) + 1 )

      logger.debug( "index %s ", index_cam )
      logger.debug( "active_camera_number_str %s ", active_camera_number_str )

      image_file = current_output_dir + active_camera_number_str + "_" + obj_camera.name + "_" + blender_file_name ;
  else: # Open the image rendered from default camera
    image_file = current_output_dir + blender_file_name ;

  # Adjust extension according to rendering mode
  if bpy.data.scenes[0].spectral_rendering:
    image_file = image_file + ".hdr"
  else:
    image_file = image_file + ".exr"

  # Checking that the image_file exists!
  if os.path.isfile(image_file):
    viewer = "SpectralViewer"

    if shutil.which(viewer) is None :
      logger.error("Cannot find SpectralViewer in your PATH")
      return
    else:
      logger.debug(" SpectralViewer about to open "+ image_file )
      subprocess.Popen((viewer,image_file))
  else:
    logger.warning(" Could not find " + image_file + " . Viewer could not be launched ")
  #endif


def mrf_files_fullpath():
  """ Returns Scene and Camera absolute path """

  current_output_dir  = os.path.dirname( bpy.data.scenes[0].output_dir ) + "/"

  logger.debug("Output dir for scene export is: %s" , current_output_dir )

  # The two strings below are absolute PATH
  mrf_filename    = libblender2msf.getMrfFilenameFromBlender()
  camera_filename = mrf_filename[:-3] + "mcf"

  mrf_filename_wo_abs_path  = os.path.basename( mrf_filename )

  mrf_filename    = current_output_dir + mrf_filename_wo_abs_path
  camera_filename = current_output_dir + mrf_filename_wo_abs_path[:-3] + "mcf"

  return (mrf_filename, camera_filename)


# ----------------------------------------------------------------------------
# Necessary Add-on Methods start here
# ----------------------------------------------------------------------------

def export():
  no_mesh_export = False
  if bpy.data.scenes[0].fast_export:
    no_mesh_export = True

  use_ply = False
  if bpy.data.scenes[0].use_ply:
    use_ply = True

  export_cam = False
  if bpy.data.scenes[0].export_camera:
    export_cam = True

  # current_output_dir  = os.path.dirname( bpy.data.scenes[0].output_dir ) + "/"

  # print("[Debug] Output dir for scene export is:" , current_output_dir )

  # # The two strings below are absolute PATH
  # mrf_filename = libblender2msf.getMrfFilenameFromBlender()
  # camera_filename = mrf_filename[:-3]+"mcf"

  # mrf_filename_wo_abs_path  = os.path.basename( mrf_filename );

  # ABSOLUTE PATH TO Scene and Camera files
  mrf_filename, camera_filename = mrf_files_fullpath()


  blender_file_name = os.path.basename(bpy.data.filepath)
  blender_file_name = os.path.splitext(blender_file_name)[0]

  # Relative Path for this subdirectory containing assets
  obj_sub_directory_name = blender_file_name+"_assets"

  logger.debug(" Object sub directory name = %s ", obj_sub_directory_name )
  logger.debug(" mrf_filename  = %s ", mrf_filename )
  logger.debug(" camera_filename name = %s ", camera_filename )

  #mrf_tools = {}

  mrf_tools = get_mrf_tools()
  libblender2msf.export(mrf_filename,camera_filename,obj_sub_directory_name,export_cam,use_ply,mrf_tools,no_mesh_export)

  if bpy.data.scenes[0].export_animation_camera:
    libblender2msf.export_camera_animation(camera_filename)


def render():
  mrf_filename, _ = mrf_files_fullpath()
  mrf_tools       = get_mrf_tools()

  if (bpy.data.scenes[0].spectral_rendering):
    renderer_exec = mrf_tools['MALIA_EXE']
  else:
    renderer_exec = mrf_tools['MALIA_RGB_EXE']

  logger.info("Rendering with %s", renderer_exec)

  renderer_args = [
    renderer_exec,
    "-scene", mrf_filename,
    "-samples", str(bpy.data.scenes[0].nb_samples),
    "-mpl", str(bpy.data.scenes[0].mrf_max_path_length)
  ]

  if bpy.data.scenes[0].spectral_rendering:
    logger.info("Spectral rendering mode")

    renderer_args.append("-wr")
    renderer_args.append("{min_wl}:{max_wl}:{step_wl}".format(
      min_wl  = str(bpy.data.scenes[0].min_wavelength),
      max_wl  = str(bpy.data.scenes[0].max_wavelength),
      step_wl = str(bpy.data.scenes[0].step_wavelength)
    ))

    renderer_args.append("-wpp")
    renderer_args.append(str(bpy.data.scenes[0].wavelength_per_pass))
  else:
    logger.info("RGB rendering mode")

  if bpy.data.scenes[0].interactive_rendering:
    preview_res_x = bpy.context.area.width*0.5
    preview_res_y = bpy.context.area.height*0.5
    logger.info("INTERACTIVE ON. Preview resolution = " + str(preview_res_x) + " , " + str(preview_res_y))

    renderer_args.append("-width")
    renderer_args.append(str(preview_res_x))
    renderer_args.append("-height")
    renderer_args.append(str(preview_res_y))

    renderer_args.append("-i")

  else:
    res_x, res_y = libblender2msf.getSceneResolution()
    renderer_args.append("-width")
    renderer_args.append(str(res_x))
    renderer_args.append("-height")
    renderer_args.append(str(res_y))

    # renderer_args.append("-o")
    # output_image_path = mrf_filename[:-3] + "hdr"
    # logger.info("Image will be saved in: %s", output_image_path)
    # renderer_args.append(output_image_path)

  #end_if_rendering_interactive_is_enabled
  logger.debug( " cwd = " + os.path.abspath(bpy.data.scenes[0].output_dir) )

  process_renderer = subprocess.Popen(renderer_args, cwd=os.path.abspath(bpy.data.scenes[0].output_dir) )

  while process_renderer.poll() is None:
    #print('Still rendering')
    time.sleep(5)
  logger.info("RENDERER Exited with returncode %d" % process_renderer.returncode)

  if not bpy.data.scenes[0].interactive_rendering and bpy.data.scenes[0].open_viewer_after_render:
    openImageFromCurrentCameraWithSpectralViewer()
    #directory = libblender2msf.getDirectoryFromBlender()
    #libblender2msf.openSpectralViewerAfterRender(directory,mrf_filename,str(bpy.data.scenes[0].nb_samples),bpy.data.scenes[0].spectral_rendering)

# ----------------------------------------------------------------------------
# Classes
# ----------------------------------------------------------------------------

class MrfProperties(PropertyGroup):
  malia_exe: StringProperty(
    name="Malia Spectral",
    description="Path to Malia spectral renderer",
    default="/usr/bin/malia")

  malia_rgb_exe: StringProperty(
    name="Malia RGB",
    description="Path to Malia RGB renderer",
    default="/usr/bin/malia_rgb")

  spectral_viewer_exe: StringProperty(
    name="Spectral Viewer",
    description="Path to Spectral Viewer",
    default="/usr/bin/SpectralViewer")

  def draw(self, context):
    layout = self.layout
    layout.label(text="UI Options:")

    row = layout.row(align=True)
    row.prop(self, "expand_enum", text="UI Options", expand=True)


# ----------------------------------------------------------------------------
# Control Panel
# ----------------------------------------------------------------------------

class MrfControlPannel(bpy.types.Panel):
  """ MrfControlPannel """

  bl_label = 'MRF Control Panel'
  bl_idname = 'RENDER_PT_mrf_control_panel'
  bl_space_type = 'VIEW_3D'
  bl_region_type = 'UI'
  bl_category = 'MRF'

  if bpy.app.version < (2,80,0):
    bl_category = 'MRF'
    # TOOLS is better for blender 2.7 in my opinion
    bl_region_type = 'TOOLS'

  bl_context = 'objectmode'

  mrf_tools = get_mrf_tools()
  
  def draw(self, context):
    layout = self.layout

    box3 = layout.box()
    box3.label(text='Import Options')
    box3.prop(context.scene, 'additional_asset_dir')


    box = layout.box()
    box.label(text='Export Options')
    box.prop(context.scene, 'output_dir')
    box.prop(context.scene, 'export_camera')
    box.prop(context.scene, 'export_animation_camera')
    box.prop(context.scene, 'fast_export')
    box.prop(context.scene, 'use_ply')
    #box.prop(context.scene, 'use_stl')
    box.operator('mrf.export')

    if self.mrf_tools['MALIA_IS_AVAILABLE'] or self.mrf_tools['MALIA_RGB_IS_AVAILABLE']:
      box2 = layout.box()
      box2.label(text='Rendering  Opions')
      box2.prop(context.scene, 'mrf_max_path_length')
      box2.prop(context.scene, 'interactive_rendering')
      box2.prop(context.scene, 'nb_samples')
      box2.operator('mrf.export_and_render')
      box2.operator('mrf.render')

      if self.mrf_tools['SPECTRALVIEWER_IS_AVAILABLE']:
        box2.prop(context.scene, 'open_viewer_after_render')
      #end_if

      if self.mrf_tools['MALIA_IS_AVAILABLE']:
        box2.prop(context.scene, 'spectral_rendering')
        #a_row = box2.row();
        #box2.prop(context.scene, )
        #if bpy.types.Scene.spectral_rendering:
        if bpy.data.scenes[0].spectral_rendering:
          box2.prop(context.scene, 'min_wavelength')
          box2.prop(context.scene, 'max_wavelength')
          box2.prop(context.scene, 'step_wavelength')
          box2.prop(context.scene, 'wavelength_per_pass')

# ----------------------------------------------------------------------------
# Operator definitions
# ----------------------------------------------------------------------------

class Export(bpy.types.Operator):
  """ Exports the current scene for MRF """

  bl_idname = 'mrf.export'
  bl_label = 'Export Scene'
  bl_options = {'REGISTER', 'UNDO'}

  def execute(self, context):
    logger.debug("MRF EXPORT")
    export()
    #bpy.data.scenes[0].render.resolution_x = bpy.data.scenes[0].resolution_x
    #bpy.data.scenes[0].render.resolution_y =1.3* bpy.data.scenes[0].render.resolution_x
    return{'FINISHED'}


# ----------------------------------------------------------------------------
# Render
# ----------------------------------------------------------------------------
class Render(bpy.types.Operator):
  """ Launches the Malia Rendering Engine """

  bl_idname = 'mrf.render'
  bl_label = 'Render'
  bl_options = {'REGISTER', 'UNDO'}

  def execute(self, context):
    logger.debug("MRF RENDER")
    render()
    return{'FINISHED'}

class ExportAndRender(bpy.types.Operator):
  """ Exports the current scene and launches the Malia Rendering Engine """

  bl_idname = 'mrf.export_and_render'
  bl_label = 'Export and render'
  bl_options = {'REGISTER', 'UNDO'}

  def execute(self, context):
    logger.debug("MRF EXPORT AND RENDER")

    export()
    render()
    return{'FINISHED'}


classes = {
  MrfProperties,
  MrfControlPannel,
  Export,
  ExportAndRender,
  Render,
}


# ----------------------------------------------------------------------------
# Plugin register / unregister
# ----------------------------------------------------------------------------

def register():
  #bpy.utils.load_scripts(True)

  mrf_tools = get_mrf_tools()

  bpy.types.Scene.additional_asset_dir  = bpy.props.StringProperty(
    name ="Spectral Assets",
    default = "./assets/",
    description = "Additional Directory where The exporter will look for IOR or Light Spectrum files (.spd) ",
    maxlen = 2048,
    subtype="DIR_PATH",
  )

  bpy.types.Scene.output_dir  = bpy.props.StringProperty(
    name ="Output",
    default = ".",
    description = "Current output directory where the scene, assets and camera files are saved",
    maxlen = 2048,
    subtype="DIR_PATH",
  )

  bpy.types.Scene.export_camera = bpy.props.BoolProperty(
    name= 'Export camera',
    default=True,
  )

  bpy.types.Scene.export_animation_camera = bpy.props.BoolProperty(
    name= 'Export camera anim.',
    default=False
  )

  bpy.types.Scene.fast_export = bpy.props.BoolProperty(
    name= 'Fast Export (no geom.)',
    default=False
  )

  bpy.types.Scene.use_ply = bpy.props.BoolProperty(
    name= 'Use ply',
    default=False
  )

  # bpy.types.Scene.use_stl = bpy.props.BoolProperty(
  #   name= 'Use binary STL for geometries',
  #   default=False
  # )

  if mrf_tools['MALIA_IS_AVAILABLE'] or mrf_tools['MALIA_RGB_IS_AVAILABLE']:
    bpy.types.Scene.interactive_rendering = bpy.props.BoolProperty(
      name= 'Interactive rendering',
      default=True
    )

    bpy.types.Scene.nb_samples = bpy.props.IntProperty(
      name= 'Samples',
      default=10,
      min=1,
      max=100000000
    )

    bpy.types.Scene.mrf_max_path_length = bpy.props.IntProperty(
      name= 'Max Path Length',
      default=10,
      min=1,
      max=100
    )

  if mrf_tools['SPECTRALVIEWER_IS_AVAILABLE']:
    bpy.types.Scene.open_viewer_after_render = bpy.props.BoolProperty(
      name= 'Open viewer after render',
      default=False
    )

  if mrf_tools['MALIA_IS_AVAILABLE']:
    bpy.types.Scene.spectral_rendering = bpy.props.BoolProperty(
      name= 'Spectral rendering',
      default=False,
      #update=spectral_rendering_update,
    )

    print(bpy.types.Scene.spectral_rendering)

    bpy.types.Scene.min_wavelength = bpy.props.IntProperty(
      name= 'First Wavelength',
      default=380,
      min=360,
      max=830
    )

    bpy.types.Scene.max_wavelength = bpy.props.IntProperty(
      name= 'Last Wavelength',
      default=830,
      min=360,
      max=830
    )

    bpy.types.Scene.step_wavelength = bpy.props.IntProperty(
      name= 'Step Wavelengths',
      default=50,
      min=1,
      max=400
    )

    bpy.types.Scene.wavelength_per_pass = bpy.props.IntProperty(
      name= 'Wavelengths per pass',
      default=16,
      min=1,
      max=500
    )
  #endif_if_malia_is_available


  for cls in classes:
    if cls.__name__ == "MrfControlPannel":
      bpy.utils.register_class(cls)
      #cls.setMrfTools(cls,mrf_tools)
    else:
      bpy.utils.register_class(cls)

  logger.info('MRF Add-on registered\n')


def unregister():
  for cls in classes:
    bpy.utils.unregister_class(cls)

  logger.info('MRF Add-on UNregistered\n')



# # Necessary to launch this script from command line with Python
# # blender --python blender_mrf_ui.py
# if __name__ == "__main__":
#   register()