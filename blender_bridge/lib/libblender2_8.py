import os,sys
import xml.etree.ElementTree as ET
import bpy
import bmesh
import math
import mathutils
from shutil import copy
from shutil import copyfile
from shutil import which
import platform

from libpatchmrf import patch_mrf_scene_after_export
from libmrfxml import *
from libmrftools import *


def getMrfFilenameFromBlender():
	blend_file_path = bpy.data.filepath
	directory = os.path.dirname(blend_file_path)
	blender_file_name = os.path.basename(bpy.data.filepath)
	blender_file_name = os.path.splitext(blender_file_name)[0]

	mrf_filename = os.path.join(directory,"./"+blender_file_name+".msf")
	return mrf_filename

def getDirectoryFromBlender():
	blend_file_path = bpy.data.filepath
	directory = os.path.dirname(blend_file_path)
	return directory

def getBlenderSceneObjectName():
	blender_scene_obj_name = "Scene"
	return bpy.data.scenes[0].name


def getSceneResolution():
	x = bpy.data.scenes[getBlenderSceneObjectName()].render.resolution_x
	y = bpy.data.scenes[getBlenderSceneObjectName()].render.resolution_y
	return x,y

def blender_vec_to_mrf_vec(vec):
	position = [0,0,0]
	position[0] = vec[0]
	position[1] = vec[2]
	position[2] = -vec[1]

	#temp_rot = mathutils.Matrix.Rotation(math.radians(90.0), 3, 'X')
	#vec_ = mathutils.Vector((vec[0],vec[1],vec[2]))
	#vec_ = vec_ * temp_rot

	#print(position)
	#print(vec_)

	return position


def unselect_all_objects():
	bpy.ops.object.select_all(action='DESELECT')

def export_material(root_node, material, directory_full_path, directory_relative_path, mrf_tools):
	materials = root_node.find('materials')
	material_name = material.name
	print("obj material is")
	print(material_name)
	#print(material)
	
	if(material_exists(materials,material_name)):
		print('This material already exists it will not be added')
	else:
		if(material_name.startswith('MRF_measured_isotropic')):
			add_material_measured_isotropic(root_node,material_name,directory_full_path, directory_relative_path)
		elif(material_name.startswith('MRF_Tangent')):
			add_typed_material(root_node,material_name,'Tangent')
		elif(material_name.startswith('MRF_Normal')):
			add_typed_material(root_node,material_name,'Normal')
		elif(material_name.startswith('MRF_UV')):
			add_typed_material(root_node,material_name,'UV')
		elif(material_name.startswith('MRF_Flat')):
			add_typed_material(root_node,material_name,'Flat')
		elif(material_name.startswith('MRF_ShadowCatcher')):
			add_typed_material(root_node,material_name,'ShadowCatcher')
	
		elif(material.node_tree and "Mix Shader" in material.node_tree.nodes):
			print("ADD the material (mix) "+material_name)
			ratio = material.node_tree.nodes["Mix Shader"].inputs[0].default_value
			# print("Mix shader ratio: "+str(ratio))
	
			first_node = material.node_tree.nodes["Mix Shader"].inputs[1].links[0].from_socket.node
			second_node = material.node_tree.nodes["Mix Shader"].inputs[2].links[0].from_socket.node
	
			mix_diffuse = False
			if(first_node.name == "Diffuse BSDF"):
				diffuse_node = first_node
				specular_node = second_node
				kd = 1.0 - ratio
				ks = ratio
				if(second_node.name == "Diffuse BSDF"):
					print("Mixing the two diffuse component")
					mix_diffuse = True
			elif(first_node.name == "Glossy BSDF"):
				diffuse_node = second_node
				specular_node = first_node
				ks = 1.0 - ratio
				kd = ratio
	
			r_dif = str( diffuse_node.inputs[0].default_value[0] )
			g_dif = str( diffuse_node.inputs[0].default_value[1] )
			b_dif = str( diffuse_node.inputs[0].default_value[2] )
	
			if(mix_diffuse):
				r_dif2 = str( specular_node.inputs[0].default_value[0] )
				g_dif2 = str( specular_node.inputs[0].default_value[1] )
				b_dif2 = str( specular_node.inputs[0].default_value[2] )
				r = kd*r_dif + ks*r_dif2
				g = kd*g_dif + ks*g_dif2
				b = kd*b_dif + ks*b_dif2
				add_material_lambert(root_node,material_name,r,g,b,directory_full_path, directory_relative_path, mrf_tools)
			else:
				glossy_distribution = specular_node.distribution
				sharp_bsdf = False
				if(glossy_distribution=="SHARP"):
					sharp_bsdf = True
	
				roughness_to_phong = 2.0 * pow(specular_node.inputs[1].default_value, -2) - 2.0
				roughness = (roughness_to_phong,kd,ks)
				r_spec = specular_node.inputs[0].default_value[0]
				g_spec = specular_node.inputs[0].default_value[1]
				b_spec = specular_node.inputs[0].default_value[2]
				rgb = (r_dif,g_dif,b_dif,r_spec,g_spec,b_spec)
				add_material_phong(root_node,material_name,rgb,roughness,directory_full_path,directory_relative_path)
	
		elif(material.node_tree and "Checker Texture" in material.node_tree.nodes):
			print("ADD the material "+material_name)
			color1 = material.node_tree.nodes["Checker Texture"].inputs[1].default_value
			color2 = material.node_tree.nodes["Checker Texture"].inputs[2].default_value
			repetition = material.node_tree.nodes["Checker Texture"].inputs[3].default_value
			add_material_checkerboard(root_node,material_name,color1,color2,repetition,directory_full_path, directory_relative_path, mrf_tools)
	
		elif(material.node_tree and "Diffuse BSDF" in material.node_tree.nodes):
			#print(material.node_tree.nodes["Diffuse BSDF"])
			#print(material.node_tree.nodes["Diffuse BSDF"].inputs[0].default_value[0])
			print("ADD the material (diffuse) "+material_name)
			r = str( material.node_tree.nodes["Diffuse BSDF"].inputs[0].default_value[0] )
			g = str( material.node_tree.nodes["Diffuse BSDF"].inputs[0].default_value[1] )
			b = str( material.node_tree.nodes["Diffuse BSDF"].inputs[0].default_value[2] )
			a = str( material.node_tree.nodes["Diffuse BSDF"].inputs[0].default_value[3] )
			add_material_lambert(root_node,material_name,r,g,b,directory_full_path, directory_relative_path, mrf_tools)
	
		elif(material.node_tree and "Glossy BSDF" in material.node_tree.nodes):
			glossy_distribution = material.node_tree.nodes["Glossy BSDF"].distribution
			print("ADD the material (glossy) "+material_name)
			print("Distribution is "+glossy_distribution)
			sharp_bsdf = False
			if(glossy_distribution=="SHARP"):
				#add_material_perfect_mirror(root_node,material_name)
				sharp_bsdf = True
	
			roughness = material.node_tree.nodes["Glossy BSDF"].inputs[1].default_value
			r = material.node_tree.nodes["Glossy BSDF"].inputs[0].default_value[0]
			g = material.node_tree.nodes["Glossy BSDF"].inputs[0].default_value[1]
			b = material.node_tree.nodes["Glossy BSDF"].inputs[0].default_value[2]
			rgb = (r,g,b)
			add_material_ggx(root_node,material_name,rgb,roughness, sharp_bsdf, directory_full_path, directory_relative_path, mrf_tools)
	
		elif(material.node_tree and "Emission" in material.node_tree.nodes):
			print("ADD the material (emission) "+material_name)
			r = material.node_tree.nodes["Emission"].inputs[0].default_value[0]
			g = material.node_tree.nodes["Emission"].inputs[0].default_value[1]
			b = material.node_tree.nodes["Emission"].inputs[0].default_value[2]
			rgb = (r,g,b)
			radiance = material.node_tree.nodes["Emission"].inputs[1].default_value
			add_material_emission(root_node,material_name,rgb,radiance,directory_full_path, directory_relative_path, mrf_tools)
	
		elif(material.node_tree and "Glass BSDF" in material.node_tree.nodes):
			print("ADD the material (glass) "+material_name)
			roughness = material.node_tree.nodes["Glass BSDF"].inputs[1].default_value
			ior = material.node_tree.nodes["Glass BSDF"].inputs[2].default_value
			print("IOR " + str(ior))
			sharp_glass = False
			print("Glass distribution="+material.node_tree.nodes["Glass BSDF"].distribution)
			if material.node_tree.nodes["Glass BSDF"].distribution=="SHARP":
				sharp_glass = True
	
			thin_glass = False
			r = 1.0 - material.node_tree.nodes["Glass BSDF"].inputs[0].default_value[0]
			g = 1.0 - material.node_tree.nodes["Glass BSDF"].inputs[0].default_value[1]
			b = 1.0 - material.node_tree.nodes["Glass BSDF"].inputs[0].default_value[2]
			extinction = (r,g,b)
			print("Extinction "+ str(extinction))
			if "thin_glass" in bpy.data.materials[material_name]:
				if bpy.data.materials[material_name]["thin_glass"] > 0:
					thin_glass = True
			add_material_glass(root_node, material_name, ior, extinction, roughness, sharp_glass, thin_glass,directory_full_path, directory_relative_path)
	
		elif(material.node_tree and "Principled BSDF" in material.node_tree.nodes):
			r = str( material.node_tree.nodes["Principled BSDF"].inputs[0].default_value[0] )
			g = str( material.node_tree.nodes["Principled BSDF"].inputs[0].default_value[1] )
			b = str( material.node_tree.nodes["Principled BSDF"].inputs[0].default_value[2] )
			base_color = (r,g,b)
			
			params = {'roughness':str(0),'sheen':str(0),'sheenTint':str(0),'subsurface':str(0),'specular':str(0),'specularTint':str(0),'anisotropic':str(0),'clearcoat':str(0), 'clearcoatGloss':str(0) }
			params['roughness'] = str( material.node_tree.nodes["Principled BSDF"].inputs["Roughness"].default_value )
			params['sheen'] = str( material.node_tree.nodes["Principled BSDF"].inputs["Sheen"].default_value )
			params['sheenTint'] = str( material.node_tree.nodes["Principled BSDF"].inputs["Sheen Tint"].default_value )
			params['subsurface'] = str( material.node_tree.nodes["Principled BSDF"].inputs["Subsurface"].default_value )
			params['metallic'] = str( material.node_tree.nodes["Principled BSDF"].inputs["Metallic"].default_value )
			params['specular'] = str( material.node_tree.nodes["Principled BSDF"].inputs["Specular"].default_value )
			params['specularTint'] = str( material.node_tree.nodes["Principled BSDF"].inputs["Specular Tint"].default_value )
			params['anisotropic'] = str( material.node_tree.nodes["Principled BSDF"].inputs["Anisotropic"].default_value )
			params['clearcoat'] = str( material.node_tree.nodes["Principled BSDF"].inputs["Clearcoat"].default_value )
			params['clearcoatGloss'] = str( material.node_tree.nodes["Principled BSDF"].inputs["Clearcoat Roughness"].default_value )
			
			# print(params)
			
			add_material_principled(root_node,material_name,base_color,params,directory_full_path, directory_relative_path,mrf_tools)
	
		else:
			print("Unrecogized material type or empty material, assigning default lambert to it")
			material_name = "default_lambert"
			add_material_lambert(root_node,material_name,'0.8','0.8','0.8',directory_full_path, directory_relative_path, mrf_tools)



def export_meshes(root_node, directory_full_path, directory_relative_path, use_ply, mrf_tools, no_mesh_export=False):
	materials_xml_node =  root_node.find('materials')

	for obj in bpy.data.objects:
		print(obj.name)

		if obj.type != "MESH":
			continue

		if(obj.hide_render):
			print("A mesh is hide_render")
			continue

		if use_ply:
			file_name = obj.name + ".ply"
		else:
			file_name = obj.name + ".obj"

		target_file = os.path.join(directory_full_path, file_name)

		# if use_ply:
			# # bpy.context.scene.objects.active = obj
			# bpy.context.view_layer.objects.active = obj
		# else:
			# obj.select_set(True)
		bpy.context.view_layer.objects.active = obj
		obj.select_set(True)


		if no_mesh_export == False:
			if use_ply:
				try:
					# Note from RP.
					# Need to use Blender BMesh Module

					# First Get the Mesh from obj
					# SHOUD WORK because at this point obj is of type Mesh
					index_current_mesh = bpy.data.meshes.find(obj.name)
					if index_current_mesh == -1:
						print(index_current_mesh)
						print("WTF MESH NOT FOUND ")
						continue
					#endif_if

					# The Mesh from obj
					current_mesh = bpy.data.meshes[obj.name]

					# Consrruct BMesh from current_mesh
					bm = bmesh.new( )
					bm.from_mesh(current_mesh )

					# FORCING triangulation
					bmesh.ops.triangulate( bm, faces=bm.faces[:], quad_method='BEAUTY', ngon_method='BEAUTY')

					bm.to_mesh(current_mesh)

					# Now do the export
					bpy.ops.export_mesh.ply(filepath=target_file, check_existing=True, axis_up='Y', axis_forward='-Z',\
					filter_glob="*.ply", use_selection=True, use_mesh_modifiers=True,\
					use_normals=True, use_uv_coords=True, use_colors=False, global_scale=1)

					# Release resources
					bm.free()

				except RuntimeError:
					print("Runtimeerror export ply on "+obj.name)
					continue
			else:
				#export obj geometry
				bpy.ops.export_scene.obj(filepath=target_file, check_existing=True, axis_forward='-Z', axis_up='Y',\
				filter_glob="*.obj;*.mtl", use_selection=True, use_animation=False, use_mesh_modifiers=True,\
				use_edges=True, use_smooth_groups=False, use_smooth_groups_bitflags=False,\
				use_normals=True, use_uvs=True, use_materials=True, use_triangles=True, use_nurbs=False,\
				use_vertex_groups=False, use_blen_objects=True, group_by_object=False, group_by_material=False,\
				keep_vertex_order=False, global_scale=1, path_mode='AUTO')

		obj.select_set(False)


		material_name = ""
		#parse material
		if len(obj.material_slots) == 1:
			#bpy.context.active_object.active_material = obj.material_slots[0].material
			material = obj.material_slots[0].material
			export_material(root_node, material, directory_full_path, directory_relative_path, mrf_tools)

		elif len(obj.material_slots) > 1:
			print("Adding a multi material")
			multi_mat_names = []
			multi_mat_indices = []
			
			for i in range(0, len(obj.material_slots)):
				export_material(root_node, obj.material_slots[i].material, directory_full_path, directory_relative_path, mrf_tools)
				multi_mat_names.append(obj.material_slots[i].material.name)
				multi_mat_indices.append(i)
			
			material_name = "multi_material"
			tmp_name = material_name
			i = 1
			while(material_exists(materials_xml_node,tmp_name)):
				i += 1
				tmp_name = material_name + str(i)
			material_name = tmp_name
			add_multi_material(root_node,material_name,multi_mat_names, multi_mat_indices,directory_full_path, directory_relative_path,mrf_tools)

		else:
			print("obj has no material, assigning default lambert to it")
			material_name = "default_lambert"


		relative_path = directory_relative_path + "/" + file_name
		if use_ply:
			add_mesh(root_node,relative_path,material_name,"ply")
		else:
			add_mesh(root_node,relative_path,material_name,"obj")



def lib_export_lights(root_node,directory_full_path, directory_relative_path):
	#light_num = 0
	for obj in bpy.data.objects:
		print(obj.name)

		if obj.type != "LIGHT" :
			continue

		if(obj.hide_render):
			print("A light is hide_render")
			continue

		#blender doesn't use the same coordinate system
		position = blender_vec_to_mrf_vec(obj.location)
		#light = bpy.data.lights[obj.name]
		light = obj.data

		print("LIGHT ",light.name)

		power = 100
		rgb = [1,1,1]
		aperture = 45

		# if(light.node_tree):# and ("Emission" in light.node_tree.nodes or "Power" in light.node_tree.nodes)):
			#TODO FIND WHY WE NEED TO DIVIDE BY 4 ALL LIGHT SOURCES POWER
			# power = light.node_tree.nodes["Emission"].inputs[1].default_value / 4
			# rgb[0] = light.node_tree.nodes["Emission"].inputs[0].default_value[0]
			# rgb[1] = light.node_tree.nodes["Emission"].inputs[0].default_value[1]
			# rgb[2] = light.node_tree.nodes["Emission"].inputs[0].default_value[2]
		# else:
			# print("light has no emission, assigning default 100 white emittance to it")
		power = bpy.data.lights[light.name].energy
		rgb = bpy.data.lights[light.name].color

		if(light.type=="POINT"):
			#old version exports point light
			#print("add point light")
			#add_point_light(root_node,power,rgb,position,light.name,directory_full_path, directory_relative_path)

			#now we use sphere lights
			radius = bpy.data.lights[light.name].shadow_soft_size
			print("add sphere light")
			add_sphere_light(root_node,power,rgb,position,radius,light.name,directory_full_path, directory_relative_path)

		elif(light.type=="SPOT"):
			print("add spot light")

			#convert into degrees
			aperture = math.degrees( light.spot_size )

			inv_mat = obj.matrix_world.copy()
			inv_mat.invert()
			inv_mat3 = inv_mat.to_3x3()

			#default spot light direction in blender
			default_direction = mathutils.Vector((0.0, 0.0, -1.0))

			direction = default_direction @ inv_mat3
			direction = blender_vec_to_mrf_vec(direction)

			add_spot_light(root_node,power,rgb,position,direction,aperture,light.name,directory_full_path, directory_relative_path)

		elif(light.type=="AREA"):
			scale = [1,1,1]
			rotation = [0,0,0,0]

			scale[0] = light.size * obj.scale[0]
			if(light.shape=="RECTANGLE"):
				scale[1] = light.size_y * obj.scale[1]
			else:
				scale[1] = light.size * obj.scale[1]


			obj.rotation_mode = "XYZ"

			temp_euler_rot = mathutils.Euler(obj.rotation_euler)
			temp_rot_mat_1 = temp_euler_rot.to_matrix().copy()
			temp_rot_mat_2 = mathutils.Matrix.Rotation(math.radians(-90.0), 3, 'X')
			temp_rot_mat_3 =  temp_rot_mat_2 @ temp_rot_mat_1

			(axis_rot,angle_rot) = temp_rot_mat_3.to_quaternion().to_axis_angle()
			rotation[0] = math.degrees(angle_rot)
			rotation[1] = axis_rot[0]
			rotation[2] = axis_rot[1]
			rotation[3] = axis_rot[2]
			#print(rotation)

			print("add area light")
			add_area_light(root_node,power,rgb,position,scale,rotation,light.name,directory_full_path, directory_relative_path)

		elif(light.type=="SUN"):
			inv_mat = obj.matrix_world.copy()
			inv_mat.invert()
			inv_mat3 = inv_mat.to_3x3()

			#default spot light direction in blender
			default_direction = mathutils.Vector((0.0, 0.0, -1.0))
			direction = default_direction @ inv_mat3
			direction = blender_vec_to_mrf_vec(direction)

			power = light.node_tree.nodes["Emission"].inputs[1].default_value

			print("add sun light")
			add_dir_light(root_node,power,rgb,direction,light.name,directory_full_path, directory_relative_path)

		else:
			print("Unrecognized light type: "+light.type+" light not added")


def count_cameras():
	nb_cameras = 0
	for obj in bpy.data.objects:
		if obj.type == "CAMERA":
			nb_cameras = nb_cameras + 1
	return nb_cameras




# Add a blender camera to a .mcf xml file
# blender_camera_obj: the blender object camera
# camera_xml_tree: the cml tree, returned by ET.parse(camera_filename.mcf)
# blender_scene_obj_name, return by getBlenderSceneObjectName()
# camera_id: the id in the xml file (<camera id="id" ... />)
def blenderCameraToXmlCameraTree(blender_camera_obj, camera_xml_tree, blender_scene_obj_name, camera_id) :

	camera_name = blender_camera_obj.data.name

	#position = blender_vec_to_mrf_vec(blender_camera_obj.location)

	#inv_mat = blender_camera_obj.matrix_world.copy()
	inv_mat = blender_camera_obj.matrix_world.copy()

	position_blender = inv_mat.to_translation().copy()
	position = blender_vec_to_mrf_vec(inv_mat.to_translation())
	inv_mat.invert()
	inv_mat3 = inv_mat.to_3x3()

	default_view = mathutils.Vector((0.0, 0.0, -1.0))
	default_up = mathutils.Vector((0.0, 1.0, 0.0))

	view = default_view @ inv_mat3
	up = default_up @ inv_mat3

	up = blender_vec_to_mrf_vec(up)
	look_at = position_blender + 10.0 * view
	look_at = blender_vec_to_mrf_vec(look_at)

	#camera_tree = ET.parse(camera_filename)
	camera_root = camera_xml_tree.getroot()

	camera_xml = ET.Element('camera')
	camera_xml.set('id',camera_id)

	camera_xml.set('type','pinhole')

	position_xml = ET.Element('position')
	position_xml.set('x',str(position[0]))
	position_xml.set('y',str(position[1]))
	position_xml.set('z',str(position[2]))
	camera_xml.append(position_xml)

	look_at_xml = ET.Element('lookat')
	look_at_xml.set('x',str(look_at[0]))
	look_at_xml.set('y',str(look_at[1]))
	look_at_xml.set('z',str(look_at[2]))
	camera_xml.append(look_at_xml)

	up_xml = ET.Element('up')
	up_xml.set('x',str(up[0]))
	up_xml.set('y',str(up[1]))
	up_xml.set('z',str(up[2]))
	camera_xml.append(up_xml)

	fovy_xml = ET.Element('fovy')
	fovy_xml.set('value',str(math.degrees(bpy.data.cameras[camera_name].angle)))
	camera_xml.append(fovy_xml)

	lens = ET.Element('lens')
	lens.set('focal_length',str(bpy.data.cameras[camera_name].lens/1000.0))
	camera_xml.append(lens)

	sensor_xml = ET.Element('sensor')
	resolution = ET.Element('resolution')

	res_x, res_y = getSceneResolution()
	resolution.set('width',str(res_x))
	resolution.set('height',str(res_y))
	sensor_xml.append(resolution)

	size_sensor = ET.Element('size')
	size_sensor.set('width',str(bpy.data.cameras[camera_name].sensor_width))
	size_sensor.set('height',str(bpy.data.cameras[camera_name].sensor_height))
	sensor_xml.append(size_sensor)

	distance_sensor_xml = ET.Element('distance')
	distance_sensor_xml.set('value','0.1590')
	sensor_xml.append(distance_sensor_xml)

	sensor_xml.set('spp',str(bpy.data.scenes[blender_scene_obj_name].cycles.samples))

	camera_xml.append(sensor_xml)
	camera_root.append(camera_xml)

	indent_xml(camera_root)


def export_camera(camera_filename):

	create_camera_file(camera_filename)

	blender_scene_obj_name = getBlenderSceneObjectName()

	for obj in bpy.data.objects:
		if obj.type != "CAMERA":
			continue
		if(obj.hide_render):
			print("A camera is hide_render")
			continue

		camera_xml_tree = ET.parse(camera_filename)
		camera_name = obj.data.name
		camera_id = str(camera_name)
		blenderCameraToXmlCameraTree(obj, camera_xml_tree, blender_scene_obj_name,camera_id)
		camera_xml_tree.write(camera_filename)


#ERASE the file camera_filename and export all the frames of the first
#camera found (and not hide_render) in the scene graph
def export_camera_animation(camera_filename):
	#Find first and last frame
	blender_scene_obj_name = getBlenderSceneObjectName()
	blender_scene_obj = bpy.data.scenes[blender_scene_obj_name]
	first_frame = blender_scene_obj.frame_start
	last_frame = blender_scene_obj.frame_end
	print("Export camera animation, first frame="+str(first_frame)+" last frame="+str(last_frame))

	#erase previous camera file and get xml tree
	create_camera_file(camera_filename)
	camera_xml_tree = ET.parse(camera_filename)

	nb_digits = len(str(last_frame+1))

	camera_name = ""
	blender_camera_obj = None

	#find the first camera in scene graph which is not hide_render
	for obj in bpy.data.objects:
		if obj.type != "CAMERA":
			continue

		if(obj.hide_render):
			continue

		camera_name = obj.data.name
		blender_camera_obj = obj

	#check camera has been found
	if blender_camera_obj is None:
		print("ERROR CANNOT FIND A CAMERA TO EXPORT ANIMATION")
		return

	#Export all the cameras
	for num_frame in range(first_frame,last_frame+1):
		#change frame number to evaluate camera position and look at num_frame
		bpy.context.scene.frame_set(num_frame)
		num_frame_str = str(num_frame).zfill(nb_digits)
		#add the camera to the xml tree
		camera_id = num_frame_str+"_"+str(camera_name)
		blenderCameraToXmlCameraTree(blender_camera_obj, camera_xml_tree, blender_scene_obj_name, camera_id)

	#write xml to file
	camera_xml_tree.write(camera_filename)


def export_background(root_node,directory_full_path, directory_relative_path):

	print("START BACKGROUND")
	for obj in bpy.data.worlds:
		print (obj.name)
		#print (obj.type)

		if(bpy.data.worlds[obj.name].node_tree and "Background" in bpy.data.worlds[obj.name].node_tree.nodes):
			background_node = bpy.data.worlds[obj.name].node_tree.nodes['Background']
		else:
			continue

		#background_node.inputs[0].default_value[:3] = (0.6,0,1)
		#background_node.inputs[1].default_value = 1.0
		#print(dir(background_node))
		#print(dir(background_node.inputs))
		#bpy.data.images[""].name

		scene_node = root_node.find("scene")
		background = ET.Element('background')


		if background_node.inputs[0].links and len(background_node.inputs[0].links)> 0:
			envmap_node = background_node.inputs[0].links[0].from_node
			if(envmap_node.type=="TEX_ENVIRONMENT" and envmap_node.image and envmap_node.image.name!=""):
				print(envmap_node.name)
				print(envmap_node.image.name)

				envmap_file_path_src = bpy.path.abspath(envmap_node.image.filepath)
				print(envmap_file_path_src)
				envmap_file_path_dst = directory_full_path +"/"+ envmap_node.image.name
				#copy envmap to asset dir
				copy(envmap_file_path_src, envmap_file_path_dst)

				ref_envmap_path = directory_relative_path +"/"+  envmap_node.image.name

				ext_resources = ET.Element('ext_resources')
				envmaps = ET.Element('envmaps')
				panoramic = ET.Element('panoramic')
				panoramic.set("file",ref_envmap_path)
				panoramic.set("name","scene_envmap")
				panoramic.set("theta","0")
				panoramic.set("phi","-90")

				envmaps.append(panoramic)
				ext_resources.append(envmaps)
				root_node.append(ext_resources)

				background.set("type","envmap")
				background.set("ref_envmap","scene_envmap")

				root_node.append(background)
				return


		#no envmap found, use color value
		r = background_node.inputs[0].default_value[0] * background_node.inputs[1].default_value
		g = background_node.inputs[0].default_value[1] * background_node.inputs[1].default_value
		b = background_node.inputs[0].default_value[2] * background_node.inputs[1].default_value

		rgb_color = ET.Element('rgb_color')
		rgb_color.set("r",str(r))
		rgb_color.set("g",str(g))
		rgb_color.set("b",str(b))

		spectrum = ET.Element('spectrum')
		temp_string = "0:"+str(r)+",1000:"+str(r)
		spectrum.set('value',temp_string)

		background.append(spectrum)
		background.append(rgb_color)
		background.set("type","uniform")

		root_node.append(background)
		return

		#print(bpy.data.worlds['World'].texture_slots)

		#for x in bpy.data.worlds['World'].texture_slots:
		#	print(x)

		# print(dir(bpy.data.worlds['World'].texture_slots[0]))

		# print(background_node.inputs[0])
		# print(dir(background_node.inputs[0]))
		# print(background_node.inputs[0].name)
		# print(background_node.inputs[0].node)
		# print(background_node.inputs[0].bl_idname)
		# print(background_node.inputs[0].bl_rna)
		# print(background_node.inputs[0].rna_type)

		#Stop on fisrt world found
		break

	print("END BACKGROUND")


def export_lights(mrf_filename, obj_sub_directory_name):
	blend_file_path = bpy.data.filepath
	directory = os.path.dirname(mrf_filename)

	print("Exporting lights: " + blend_file_path)

	sub_directory = os.path.join(directory,obj_sub_directory_name)

	remove_all_lights_in_xml_file(mrf_filename)

	tree = ET.parse(mrf_filename)
	root = tree.getroot()

	#add_material_lambert(root,'default_lambert','0.8','0.8','0.8')
	unselect_all_objects()
	#export_meshes(root,sub_directory,"./"+obj_sub_directory_name,use_ply)
	lib_export_lights(root,sub_directory,"./"+obj_sub_directory_name)

	indent_xml(root)

	tree.write(mrf_filename)

	print("Done exporting lights: " + blend_file_path)


def export(
	mrf_filename,
	camera_filename,
	obj_sub_directory_name,
	export_cam,use_ply,
	mrf_tools,
	no_mesh_export=False):
	blend_file_path = bpy.data.filepath
	directory = os.path.dirname(mrf_filename)

	print("Exporting scene: " + blend_file_path)

	sub_directory = os.path.join(directory,obj_sub_directory_name)

	if not os.path.exists(sub_directory):
		os.makedirs(sub_directory)

	#the mrf file does not exists
	#we need to create it and fast export (no geometries)
	#cannot be used
	if not os.path.exists(mrf_filename):
		print("The mrf scene doesn't exist: it's a first export, fast export disabled.")
		no_mesh_export = False

	mrf_file  = open(mrf_filename, 'w')
	write_mrf_scene(mrf_file)
	mrf_file.close()

	tree = ET.parse(mrf_filename)
	root = tree.getroot()

	print("[DEBUG] sub_directory:", sub_directory)
	print("[DEBUG] obj_sub_directory_name:" ,obj_sub_directory_name)

	export_background(root,sub_directory,"./"+obj_sub_directory_name)
	unselect_all_objects()
	export_meshes(root,sub_directory,"./"+obj_sub_directory_name,use_ply,mrf_tools,no_mesh_export)
	lib_export_lights(root,sub_directory,"./"+obj_sub_directory_name)

	#try to apply a patch
	patch_mrf_scene_after_export(root, mrf_filename[:-4]+".mpf")

	#indent and save scene
	indent_xml(root)
	tree.write(mrf_filename)
	if(export_cam):
		export_camera(camera_filename)

	print("Done exporting scene: " + blend_file_path)
