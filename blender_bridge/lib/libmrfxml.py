import xml.etree.ElementTree as ET
import os
import sys
from shutil import copy
import subprocess

from libspd import *
from color_to_ior import colorToIor
from color_to_ior import spectralColorToIor



def openSpectralViewerAfterRender(directory,mrf_filename,nb_samples,spectral_rendering):
	camera_names = cameraNamesFromMCF( mrf_filename[:-4]+".mcf" )
	nb_cameras = len(camera_names)

	for camera_name in camera_names:
		image_file = os.path.basename(mrf_filename)
		image_file = os.path.splitext(image_file)[0]
		if(nb_cameras>1):
			image_file = camera_name + "_" + image_file +"_"+ nb_samples +"spp.exr"
		else:
			image_file = image_file +"_"+ nb_samples +"spp.exr"
		if spectral_rendering:
			image_file = image_file[:-4] +".hdr"

		image_file = os.path.join(directory,image_file)

	if os.path.isfile(image_file):
		viewer = ""
		if platform.system()=="Windows":
			viewer = "SpectralViewer"
		else:
			viewer = "SpectralViewer"

		if which(viewer) is None :
			print("ERROR Cannot find SpectralViewer in your PATH")
		else:
			process_viewer = subprocess.Popen((viewer,image_file))
	else:
		print("Cannot open image file:"+image_file)


def indent_xml(elem, level=0):
	i = "\n" + level*"  "
	if len(elem):
		if not elem.text or not elem.text.strip():
			elem.text = i + "  "
		if not elem.tail or not elem.tail.strip():
			elem.tail = i
		for elem in elem:
			indent_xml(elem, level+1)
		if not elem.tail or not elem.tail.strip():
			elem.tail = i
	else:
		if level and (not elem.tail or not elem.tail.strip()):
			elem.tail = i

def write_mrf_scene(mrf_file):
	mrf_file.write("\
<scene>\n\
	<lights>\n\
	</lights>\n\
	<materials>\n\
	</materials>\n\
	<shapes>\n\
	</shapes>\n\
</scene>")

def create_camera_file(camera_filename):
	camera_file  = open(camera_filename, 'w')
	camera_file.write("\
<cameras>\n\
</cameras>")
	camera_file.close()


def material_exists(materials_node, material_name):
	materials = list(materials_node.iter("material"))
	for m in materials:
		if(m.attrib["name"]==material_name):
			return True
	return False


def remove_all_lights_in_xml_file(mrf_filename):
	print(mrf_filename)

	#camera_file  = open(mrf_filename, 'r')
	#for line in camera_file:
	#	print(line)
	#camera_file.close()


	tree = ET.parse(mrf_filename)
	root = tree.getroot()
	lights = root.find('lights')
	root.remove(lights)
	lights = ET.Element('lights')
	root.append(lights)
	indent_xml(root)
	tree.write(mrf_filename)


def cameraNamesFromMCF( camera_filename ) :
	tree = ET.parse(camera_filename )
	root_node = tree.getroot()

	camera_names = []
	for camera in root_node:
		camera_names.append( camera.get("id") )

	return camera_names



def add_material_lambert(root_node,name,r,g,b,directory_full_path, directory_relative_path, mrf_tools):
	materials = root_node.find('materials')
	material = ET.Element('material')
	material.set('name',name)
	material.set('type','Lambert')
	#material.set('alpha_g','1.0')
	color = ET.Element('rgb_color')
	color.set('r',r)
	color.set('g',g)
	color.set('b',b)
	#color.set('a',a)
	material.append(color)
	albedo = ET.Element('albedo')
	albedo.set('value',"1.0")
	material.append(albedo)

	spectrum_exported = False

	spectrum = ET.Element('spectrum')

	# #try to import something
	# if(name.startswith("MRF_")):
	# 	 = os.environ.get('MRF_DIR')
	# 	spectrum_file_path_src = MRF_DIR + "/data/lambert/" + name[len("MRF_"):] + ".spd"
	# 	spectrum_file_path_dest = name[len("MRF_"):] + ".spd"
	# 	spectrum_file_path_dest = directory_full_path + "/" + spectrum_file_path_dest.replace("/","_");

	# 	if os.path.isfile(spectrum_file_path_src):
	# 		copy(spectrum_file_path_src, spectrum_file_path_dest)
	# 		print("Copy "+spectrum_file_path_src+" into ",spectrum_file_path_dest)
	# 		spectrum_exported = True
	# 	else:
	# 		print("ERROR Can't find "+spectrum_file_path_src)

	# 	spectrum.set('file',directory_relative_path+"/"+os.path.basename(spectrum_file_path_dest))


	#if not exported convert
	if not spectrum_exported:
		#convert rgb to spectrum
		spd_path = directory_full_path + "/" + name.replace("/","_") +".spd"
		rgb_to_spd(r,g,b,	spd_path,  mrf_tools, True)
		spectrum.set('file',directory_relative_path+"/"+os.path.basename(name.replace("/","_") +".spd"))

	#default constant spectrum
	#temp_string = "0:"+str(r)+",1000:"+str(r)
	#spectrum.set('value','300:1.0,800:1.0')
	#spectrum.set('value',temp_string)
	material.append(spectrum)
	materials.append(material)



def add_material_perfect_mirror(root_node,name):
	materials = root_node.find('materials')
	material = ET.Element('material')
	material.set('name',name)
	material.set('type','PerfectMirror')
	materials.append(material)



def add_material_checkerboard(root_node,material_name,color1,color2,repetition,directory_full_path, directory_relative_path, mrf_tools):
	materials = root_node.find('materials')
	material = ET.Element('material')
	material.set('name',material_name)
	material.set('type','Checkerboard')
	material.set('repetition',str(repetition))

	xml_color = ET.Element('rgb_color')
	xml_color.set('r',str(color1[0]))
	xml_color.set('g',str(color1[1]))
	xml_color.set('b',str(color1[2]))

	#convert rgb to spectrum
	spectrum = ET.Element('spectrum')
	spd_path = directory_full_path + "/" + material_name.replace("/","_") +"color1.spd"
	rgb_to_spd(str(color1[0]),str(color1[1]),str(color1[2]),spd_path, mrf_tools, True )
	spectrum.set('file',directory_relative_path+"/"+os.path.basename(material_name.replace("/","_") +"color1.spd"))

	xml_color_2 = ET.Element('rgb_color')
	xml_color_2.set('r',str(color2[0]))
	xml_color_2.set('g',str(color2[1]))
	xml_color_2.set('b',str(color2[2]))

	#convert rgb to spectrum
	spectrum_2 = ET.Element('spectrum')
	spd_path = directory_full_path + "/" + material_name.replace("/","_") +"color2.spd"
	rgb_to_spd(str(color2[0]),str(color2[1]),str(color2[2]),spd_path, mrf_tools, True)
	spectrum_2.set('file',directory_relative_path+"/"+os.path.basename(material_name.replace("/","_") +"color2.spd"))

	material.append(xml_color)
	material.append(xml_color_2)
	material.append(spectrum)
	material.append(spectrum_2)
	materials.append(material)



def add_material_ggx(root_node,material_name,rgb,roughness,sharp,directory_full_path, directory_relative_path,mrf_tools):
	eta,kappa = colorToIor(rgb)

	materials = root_node.find('materials')
	material = ET.Element('material')
	material.set('name',material_name)
	if sharp:
		material.set('type','FresnelMirror')
	else:
		material.set('type','ggx')
		material.set('alpha_g',str(roughness))
	fresnel = ET.Element('fresnel')
	eta_xml = ET.Element('eta')
	kappa_xml = ET.Element('kappa')


	rgb_eta_xml = ET.Element('rgb_color')
	rgb_eta_xml.set('r',str(eta[0]))
	rgb_eta_xml.set('g',str(eta[1]))
	rgb_eta_xml.set('b',str(eta[2]))
	rgb_kappa_xml = ET.Element('rgb_color')
	rgb_kappa_xml.set('r',str(kappa[0]))
	rgb_kappa_xml.set('g',str(kappa[1]))
	rgb_kappa_xml.set('b',str(kappa[2]))
	eta_xml.append(rgb_eta_xml)
	kappa_xml.append(rgb_kappa_xml)

	spectrum_eta = ET.Element("spectrum")
	spectrum_kappa = ET.Element("spectrum")


	spectrum_exported = False

	# if(material_name.startswith("MRF_")):
	# 	spectrum_exported = True

	# 	MRF_DIR = os.environ.get('MRF_DIR')
	# 	eta_file_path_src = MRF_DIR + "/assets/refractive_index/data/" + material_name[len("MRF_"):] + "_eta.spd"
	# 	kappa_file_path_src = MRF_DIR + "/assets/refractive_index/data/" + material_name[len("MRF_"):] + "_kappa.spd"

	# 	eta_file_path_dest = material_name[len("MRF_"):] + "_eta.spd"
	# 	kappa_file_path_dest = material_name[len("MRF_"):] + "_kappa.spd"

	# 	eta_file_path_dest = directory_full_path + "/" + eta_file_path_dest.replace("/","_");
	# 	kappa_file_path_dest = directory_full_path + "/" + kappa_file_path_dest.replace("/","_");

	# 	if os.path.isfile(eta_file_path_src):
	# 		copy(eta_file_path_src, eta_file_path_dest)
	# 		print("Copy "+eta_file_path_src+" into ",eta_file_path_dest)
	# 	else:
	# 		spectrum_exported = False
	# 		print("ERROR Can't find "+eta_file_path_src)

	# 	if os.path.isfile(kappa_file_path_src):
	# 		copy(kappa_file_path_src, kappa_file_path_dest)
	# 		print("Copy "+kappa_file_path_src+" into ",kappa_file_path_dest)
	# 	else:
	# 		spectrum_exported = False
	# 		print("ERROR Can't find "+kappa_file_path_src)

	# 	spectrum_eta.set('file',directory_relative_path+"/"+os.path.basename(eta_file_path_dest))
	# 	spectrum_kappa.set('file',directory_relative_path+"/"+os.path.basename(kappa_file_path_dest))

	if not spectrum_exported:
		spd_path = directory_full_path + "/" + material_name.replace("/","_") +".spd"
		# TODO: CHECK THAT RGB2SPEC is available
		rgb_to_spd(str(rgb[0]),str(rgb[1]),str(rgb[2]),spd_path, mrf_tools, True)

		if os.path.exists(spd_path):
			spectralColorToIor(spd_path)

		spd_path = directory_relative_path + "/" + material_name.replace("/","_") +".spd"

		spectrum_eta.set('file',spd_path[:-4]+"_eta.spd")
		spectrum_kappa.set('file',spd_path[:-4]+"_kappa.spd")

	eta_xml.append(spectrum_eta)
	kappa_xml.append(spectrum_kappa)
	fresnel.append(eta_xml)
	fresnel.append(kappa_xml)
	material.append(fresnel)
	materials.append(material)



	#convert eta rgb to spectrum
	#spd_path = directory_full_path + "/" + name.replace("/","_") +"_eta.spd"
	#r = str(eta[0])
	#g = str(eta[1])
	#b = str(eta[2])
	#rgb_to_spd(r,g,b,spd_path)
	#spectrum = ET.Element('spectrum')
	#spectrum.set('file',directory_relative_path+"/"+os.path.basename(name.replace("/","_") +"_eta.spd"))
	#eta_xml.append(spectrum)

	#convert kappa rgb to spectrum
	#spd_path_kappa = directory_full_path + "/" + name.replace("/","_") +"_kappa.spd"
	#r = str(kappa[0])
	#g = str(kappa[1])
	#b = str(kappa[2])
	#rgb_to_spd(r,g,b,spd_path_kappa)
	#spectrum_kappa = ET.Element('spectrum')
	#spectrum_kappa.set('file',directory_relative_path+"/"+os.path.basename(name.replace("/","_") +"_kappa.spd"))
	#kappa_xml.append(spectrum_kappa)


def add_material_principled(root_node,material_name,rgb,params,directory_full_path, directory_relative_path,mrf_tools):
#params = [roughness, sheen, sheenTint, subsurface, metallic, specular, specularTint, anisotropic, clearcoat, clearcoatGloss]
	base_color = rgb

	materials = root_node.find('materials')
	material = ET.Element('material')
	material.set('name',material_name)
	material.set('type','principled')

#Base color
	base_color_xml = ET.Element('base_color')

	rgb_base_color_xml = ET.Element('rgb_color')
	rgb_base_color_xml.set('r',str(rgb[0]))
	rgb_base_color_xml.set('g',str(rgb[1]))
	rgb_base_color_xml.set('b',str(rgb[2]))
	base_color_xml.append(rgb_base_color_xml)

	# spectrum_base_color = ET.Element("spectrum")
	# base_color_xml.append(spectrum_base_color)

	material.append(base_color_xml)

#Params - roughness
	roughness_xml = ET.Element('roughness')
	roughness_xml.set('value', params['roughness'])
	material.append(roughness_xml)

#Params - subsurface
	subsurface_xml = ET.Element('subsurface')
	subsurface_xml.set('value', params['subsurface'])
	material.append(subsurface_xml)

#Params - sheen
	sheen_xml = ET.Element('sheen')
	sheen_xml.set('value', params['sheen'])
	material.append(sheen_xml)

#Params - sheenTint
	sheenTint_xml = ET.Element('sheenTint')
	sheenTint_xml.set('value', params['sheenTint'])
	material.append(sheenTint_xml)

#Params - metallic
	metallic_xml = ET.Element('metallic')
	metallic_xml.set('value', params['metallic'])
	material.append(metallic_xml)
	
#Params - specular
	specular_xml = ET.Element('specular')
	specular_xml.set('value', params['specular'])
	material.append(specular_xml)
	
#Params - specularTint
	specularTint_xml = ET.Element('specularTint')
	specularTint_xml.set('value', params['specularTint'])
	material.append(specularTint_xml)
	
#Params - anisotropic
	anisotropic_xml = ET.Element('anisotropic')
	anisotropic_xml.set('value', params['anisotropic'])
	material.append(anisotropic_xml)
	
#Params - clearcoat
	clearcoat_xml = ET.Element('clearcoat')
	clearcoat_xml.set('value', params['clearcoat'])
	material.append(clearcoat_xml)
	
#Params - clearcoatGloss
	clearcoatGloss_xml = ET.Element('clearcoatGloss')
	clearcoatGloss_xml.set('value', params['clearcoatGloss'])
	material.append(clearcoatGloss_xml)

	materials.append(material)


def add_material_emission(root_node,material_name,rgb,radiance, directory_full_path, directory_relative_path,mrf_tools):
	materials = root_node.find('materials')
	emittance = ET.Element('emittance')
	emittance.set('type','diffuse')
	emittance.set('name',material_name)

	radiance_xml = ET.Element('radiance')
	radiance_xml.set('value',str(radiance))
	rgb_xml = ET.Element('rgb_color')
	rgb_xml.set('r',str(rgb[0]))
	rgb_xml.set('g',str(rgb[1]))
	rgb_xml.set('b',str(rgb[2]))
	emittance.append(radiance_xml)
	emittance.append(rgb_xml)

	spectrum_exported = False
	spectrum = ET.Element('spectrum')

	# #try to import something
	# if(material_name.startswith("MRF_")):
	# 	MRF_DIR = os.environ.get('MRF_DIR')
	# 	spectrum_file_path_src = MRF_DIR + "/assets/light_sources/" + material_name[len("MRF_"):] + ".spd"
	# 	spectrum_file_path_dest = material_name[len("MRF_"):] + ".spd"
	# 	spectrum_file_path_dest = directory_full_path + "/" + spectrum_file_path_dest.replace("/","_");

	# 	if os.path.isfile(spectrum_file_path_src):
	# 		copy(spectrum_file_path_src, spectrum_file_path_dest)
	# 		print("Copy "+spectrum_file_path_src+" into ",spectrum_file_path_dest)
	# 		spectrum_exported = True
	# 		spectrum.set('file',directory_relative_path+"/"+os.path.basename(spectrum_file_path_dest))
	# 	else:
	# 		print("ERROR Can't find "+spectrum_file_path_src)

	#if not exported convert
	if not spectrum_exported:
		#convert rgb to spectrum
		spd_path = directory_full_path + "/" + material_name.replace("/","_") +".spd"
		rgb_to_spd(str(rgb[0]),str(rgb[1]),str(rgb[2]),spd_path, mrf_tools,  False)
		spectrum.set('file',directory_relative_path+"/"+os.path.basename(material_name.replace("/","_") +".spd"))

	emittance.append(spectrum)
	materials.append(emittance)



def add_material_glass(root_node,material_name,ior,extinction,roughness,sharp,thin, directory_full_path, directory_relative_path):
	print("ADD GLASS ")
	materials = root_node.find('materials')
	material = ET.Element('material')
	#walter_bsdf not yet supported, using fresnel by default
	# if sharp:
		#material.set('type','FresnelGlass')
	# else:
		# material.set('type','walter_bsdf')
		# material.set('alpha_g',str(roughness))
	material.set('type','FresnelGlass')

	if thin:
		material.set('thin_glass',str("true"))

	material.set('name',material_name)
	fresnel = ET.Element('fresnel')
	eta_xml = ET.Element('eta')
	kappa_xml = ET.Element('kappa')

	rgb_eta_xml = ET.Element('rgb_color')
	rgb_eta_xml.set('r',str(ior))
	rgb_eta_xml.set('g',str(ior))
	rgb_eta_xml.set('b',str(ior))
	eta_xml.append(rgb_eta_xml)

	rgb_kappa_xml = ET.Element('rgb_color')
	rgb_kappa_xml.set('r',"0")
	rgb_kappa_xml.set('g',"0")
	rgb_kappa_xml.set('b',"0")
	kappa_xml.append(rgb_kappa_xml)


	spectrum_eta = ET.Element("spectrum")
	spectrum_kappa = ET.Element("spectrum")


	spectrum_exported = False

	# if(material_name.startswith("MRF_")):
	# 	spectrum_exported = True

	# 	MRF_DIR = os.environ.get('MRF_DIR')
	# 	eta_file_path_src = MRF_DIR + "/assets/refractive_index/data/" + material_name[len("MRF_"):] + "_eta.spd"
	# 	kappa_file_path_src = MRF_DIR + "/assets/refractive_index/data/" + material_name[len("MRF_"):] + "_kappa.spd"

	# 	eta_file_path_dest = material_name[len("MRF_"):] + "_eta.spd"
	# 	kappa_file_path_dest = material_name[len("MRF_"):] + "_kappa.spd"

	# 	eta_file_path_dest = directory_full_path + "/" + eta_file_path_dest.replace("/","_");
	# 	kappa_file_path_dest = directory_full_path + "/" + kappa_file_path_dest.replace("/","_");

	# 	if os.path.isfile(eta_file_path_src):
	# 		copy(eta_file_path_src, eta_file_path_dest)
	# 		print("Copy "+eta_file_path_src+" into ",eta_file_path_dest)
	# 	else:
	# 		spectrum_exported = False
	# 		print("ERROR Can't find "+eta_file_path_src)

	# 	if os.path.isfile(kappa_file_path_src):
	# 		copy(kappa_file_path_src, kappa_file_path_dest)
	# 		print("Copy "+kappa_file_path_src+" into ",kappa_file_path_dest)
	# 	else:
	# 		spectrum_exported = False
	# 		print("ERROR Can't find "+kappa_file_path_src)

	# 	spectrum_eta.set('file',directory_relative_path+"/"+os.path.basename(eta_file_path_dest))
	# 	spectrum_kappa.set('file',directory_relative_path+"/"+os.path.basename(kappa_file_path_dest))

	if not spectrum_exported:
		value_eta = "0:"+str(ior)+",1000:"+str(ior)
		spectrum_eta.set('value',value_eta)
		value_kappa = "0:0,1000:0"
		spectrum_kappa.set('value',value_kappa)



	extinction_xml = ET.Element('extinction')
	rgb_extinction_xml = ET.Element('rgb_color')
	rgb_extinction_xml.set('r',str(extinction[0]))
	rgb_extinction_xml.set('g',str(extinction[1]))
	rgb_extinction_xml.set('b',str(extinction[2]))
	extinction_xml.append(rgb_extinction_xml)

	spectrum_ext = ET.Element('spectrum')
	value_ext = "0:"+str(extinction[0])+",1000:"+str(extinction[0])
	spectrum_ext.set('value',value_ext)
	extinction_xml.append(spectrum_ext)
	material.append(extinction_xml)

	eta_xml.append(spectrum_eta)
	kappa_xml.append(spectrum_kappa)
	fresnel.append(eta_xml)
	fresnel.append(kappa_xml)
	material.append(fresnel)
	materials.append(material)



# def add_material_measured_isotropic(root_node,material_name,directory_full_path, directory_relative_path):
# 	print("ADD Measured isotropic Material")
# 	materials = root_node.find('materials')
# 	material = ET.Element('material')
# 	material.set('name',material_name)
# 	material.set('type','MeasuredIsotropic')

# 	MRF_DIR = os.environ.get('MRF_DIR')

# 	file_path_src = MRF_DIR + "/assets/measured_materials/" + material_name[len("MRF_measured_isotropic_"):] + ".bin"

# 	file_path_dest = material_name[len("MRF_measured_isotropic_"):] + ".bin"
# 	file_path_dest = directory_full_path + "/" + file_path_dest.replace("/","_");

# 	if os.path.isfile(file_path_src):
# 		copy(file_path_src, file_path_dest)
# 		print("Copy "+file_path_src+" into ",file_path_dest)
# 	else:
# 		print("ERROR Can't find "+file_path_src)

# 	material.set('file',directory_relative_path+"/"+os.path.basename(file_path_dest))
# 	materials.append(material)

def add_typed_material(root_node,material_name,material_type):
	print("ADD "+material_type+" Material")
	materials = root_node.find('materials')
	material = ET.Element('material')
	material.set('name',material_name)
	material.set('type',material_type)
	materials.append(material)

def add_material_phong(root_node,material_name,rgb,roughness,directory_full_path, directory_relative_path):

	materials = root_node.find('materials')
	material = ET.Element('material')
	material.set('name',material_name)

	material.set('type','PhongNormalized')
	material.set('exponent',str(roughness[0]))

	diffuse_xml = ET.Element('diffuse')

	rgb_diffuse_xml = ET.Element('rgb_color')
	rgb_diffuse_xml.set('r',str(rgb[0]))
	rgb_diffuse_xml.set('g',str(rgb[1]))
	rgb_diffuse_xml.set('b',str(rgb[2]))
	albedo_diffuse_xml = ET.Element('albedo')
	albedo_diffuse_xml.set('value',str(roughness[1]))
	diffuse_xml.append(albedo_diffuse_xml)
	diffuse_xml.append(rgb_diffuse_xml)

	specular_xml = ET.Element('specular')
	rgb_specular_xml = ET.Element('rgb_color')
	rgb_specular_xml.set('r',str(rgb[3]))
	rgb_specular_xml.set('g',str(rgb[4]))
	rgb_specular_xml.set('b',str(rgb[5]))
	albedo_specular_xml = ET.Element('albedo')
	albedo_specular_xml.set('value',str(roughness[2]))
	specular_xml.append(albedo_specular_xml)
	specular_xml.append(rgb_specular_xml)

	material.append(diffuse_xml)
	material.append(specular_xml)
	materials.append(material)

def add_multi_material(root_node,material_name, multi_mat_names, multi_mat_indices, directory_full_path, directory_relative_path,mrf_tools):
	print("ADD Multi Material")
	materials = root_node.find('materials')
	material = ET.Element('material')
	material.set('name',material_name)
	material.set('type','multi_material')
	
	nb_mat = len(multi_mat_names)
	for i in range(0, nb_mat):
		sub_mat = ET.Element('material')
		sub_mat.set('index',str(multi_mat_indices[i]))
		sub_mat.set('name',str(multi_mat_names[i]))
		material.append(sub_mat)
	
	materials.append(material)

def add_point_light(root_node,power,rgb,pos,light_name,directory_full_path, directory_relative_path):
	lights = root_node.find('lights')
	point_light = ET.Element('point_light')
	emittance = ET.Element('emittance')
	emittance.set('type','uniform')
	emittance.set('name',light_name)

	radiance = ET.Element('radiance')
	radiance.set('value',str(power))
	rgb_xml = ET.Element('rgb_color')
	rgb_xml.set('r',str(rgb[0]))
	rgb_xml.set('g',str(rgb[1]))
	rgb_xml.set('b',str(rgb[2]))
	emittance.append(radiance)
	emittance.append(rgb_xml)

	spectrum = ET.Element('spectrum')
	spectrum.set('value','0:1.0,1000:1.0')
	
	emittance.append(spectrum)

	point_light.append(emittance)
	position = ET.Element('position')
	position.set('x',str(pos[0]))
	position.set('y',str(pos[1]))
	position.set('z',str(pos[2]))
	point_light.append(position)
	lights.append(point_light)
	return point_light

def add_sphere_light(root_node,power,rgb,pos,radius,light_name,directory_full_path, directory_relative_path):
	lights = root_node.find('lights')
	sphere_light = ET.Element('sphere_light')
	sphere_light.set('radius',str(radius))
	emittance = ET.Element('emittance')
	emittance.set('type','uniform')
	emittance.set('name',light_name)

	radiance = ET.Element('radiance')
	radiance.set('value',str(power))
	rgb_xml = ET.Element('rgb_color')
	rgb_xml.set('r',str(rgb[0]))
	rgb_xml.set('g',str(rgb[1]))
	rgb_xml.set('b',str(rgb[2]))
	emittance.append(radiance)
	emittance.append(rgb_xml)

	spectrum = ET.Element('spectrum')
	spectrum.set('value','0:1.0,1000:1.0')

	# if light_name.startswith("MRF_"):
	# 	copy_light_spd(spectrum,directory_full_path,directory_relative_path,light_name)
	# else:#default constant spectrum
	# 	spectrum.set('value','0:1.0,1000:1.0')

	emittance.append(spectrum)

	sphere_light.append(emittance)
	position = ET.Element('position')
	position.set('x',str(pos[0]))
	position.set('y',str(pos[1]))
	position.set('z',str(pos[2]))
	sphere_light.append(position)
	lights.append(sphere_light)
	return sphere_light


def add_spot_light(root_node,power,rgb,pos,direction,aperture,light_name,directory_full_path, directory_relative_path):
	light = add_point_light(root_node,power,rgb,pos,light_name,directory_full_path, directory_relative_path)
	emittance = light.find('emittance')
	emittance.set('type','conic')
	emittance.set('aperture',str(aperture))
	direction_xml = ET.Element('direction')
	direction_xml.set('x',str(direction[0]))
	direction_xml.set('y',str(direction[1]))
	direction_xml.set('z',str(direction[2]))
	emittance.append(direction_xml)
	return light


# def copy_light_spd(spectrum_xml,directory_full_path,directory_relative_path,ligth_name):
# 	MRF_DIR = os.environ.get('MRF_DIR')

# 	spectrum_file_path_src = MRF_DIR + "/assets/light_sources/" + ligth_name[len("MRF_"):] + ".spd"

# 	spectrum_file_path_dest = ligth_name[len("MRF_"):] + ".spd"
# 	spectrum_file_path_dest = directory_full_path + "/" + spectrum_file_path_dest.replace("/","_");

# 	if os.path.isfile(spectrum_file_path_src):
# 		copy(spectrum_file_path_src, spectrum_file_path_dest)
# 		print("Copy "+spectrum_file_path_src+" into ",spectrum_file_path_dest)
# 	else:
# 		print("ERROR Can't find "+spectrum_file_path_src)

# 	spectrum_xml.set('file',directory_relative_path+"/"+os.path.basename(spectrum_file_path_dest))

def add_area_light(root_node,power,rgb,pos,scale,rotation,light_name,directory_full_path, directory_relative_path):
	lights = root_node.find('lights')
	area_light = ET.Element('area_light')
	emittance = ET.Element('emittance')
	emittance.set('type','diffuse')
	emittance.set('name',light_name)

	area_light_area = scale[0]*scale[1]
	#convert from watt to watt.m-2.sr-1
	area_light_radiance = power / area_light_area

	radiance = ET.Element('radiance')
	radiance.set('value',str(area_light_radiance))
	rgb_xml = ET.Element('rgb_color')
	rgb_xml.set('r',str(rgb[0]))
	rgb_xml.set('g',str(rgb[1]))
	rgb_xml.set('b',str(rgb[2]))
	emittance.append(radiance)
	emittance.append(rgb_xml)

	#default constant spectrum
	spectrum = ET.Element('spectrum')
	spectrum.set('value','0:1.0,1000:1.0')

	# No MRF_DIR. Need some rework on the plugin build potentially
	# if light_name.startswith("MRF_"):
	# 	copy_light_spd(spectrum,directory_full_path,directory_relative_path,light_name)

	emittance.append(spectrum)
	area_light.append(emittance)

	transformations = ET.Element('transformations')
	transform = ET.Element('transform')
	scale_xml = ET.Element('scale')
	scale_xml.set('x',str(scale[0]))
	scale_xml.set('y',str(scale[1]))
	scale_xml.set('z','1.0')
	translation_xml = ET.Element('translation')
	translation_xml.set('x',str(pos[0]))
	translation_xml.set('y',str(pos[1]))
	translation_xml.set('z',str(pos[2]))
	rotation_xml = ET.Element('rotation')
	angle = ET.Element('angle')
	angle.set('value',str(rotation[0]))
	axis = ET.Element('axis')
	axis.set('x',str(rotation[1]))
	axis.set('y',str(rotation[2]))
	axis.set('z',str(rotation[3]))
	rotation_xml.append(angle)
	rotation_xml.append(axis)
	transform.append(scale_xml)
	transform.append(translation_xml)
	transform.append(rotation_xml)
	transformations.append(transform)
	area_light.append(transformations)

	lights.append(area_light)
	return area_light

def add_dir_light(root_node,power,rgb,direction,light_name,directory_full_path, directory_relative_path):
	lights = root_node.find('lights')
	dir_light = ET.Element('dir_light')
	emittance = ET.Element('emittance')
	emittance.set('type','dirac')
	emittance.set('name',light_name)

	radiance = ET.Element('radiance')
	radiance.set('value',str(power))
	rgb_xml = ET.Element('rgb_color')
	rgb_xml.set('r',str(rgb[0]))
	rgb_xml.set('g',str(rgb[1]))
	rgb_xml.set('b',str(rgb[2]))

	spectrum = ET.Element('spectrum')
	spectrum.set('value','0:1.0,1000:1.0')

	# if light_name.startswith("MRF_"):
	# 	copy_light_spd(spectrum,directory_full_path,directory_relative_path,light_name)
	# else:#default constant spectrum

	emittance.append(spectrum)


	emittance.append(radiance)
	emittance.append(rgb_xml)
	direction_xml = ET.Element('direction')
	direction_xml.set('x',str(direction[0]))
	direction_xml.set('y',str(direction[1]))
	direction_xml.set('z',str(direction[2]))
	emittance.append(direction_xml)
	dir_light.append(emittance)

	lights.append(dir_light)
	return dir_light


def add_mesh(root_node,filepath,material_name,mesh_type):
	shapes = root_node.find('shapes')
	shape = ET.Element('shape')
	mesh = ET.Element('mesh')
	shape.set('ref_material', material_name)
	mesh_obj = ET.Element(mesh_type)
	mesh_obj.set('file',filepath)
	mesh.append(mesh_obj)
	shape.append(mesh)
	shapes.append(shape)