################################################
## Author # Arthur Dufay
##
## Create an archive to deploy MRF
################################################

#!bin/sh

tar czvf MRF.tar.gz \
assets/scenes/* \
assets/light_sources/* \
assets/envmaps/* \
cmake/* \
externals/* \
mdl/* \
mrf/* \
tools/* \
apps/iray_renderer/CMakeLists.txt apps/iray_renderer/main.cpp \
apps/spectral_image_converter/CMakeLists.txt apps/spectral_image_converter/main.cpp
