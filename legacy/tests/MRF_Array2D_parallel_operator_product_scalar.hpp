template< class T>
_Array2D<T>
_Array2D<T>::operator*(T scalar)
{
  _Array2D<T> result(_width, _height);

  /*
  // number of threads
  uint nthreads = 8;// std::thread::hardware_concurrency();
  std::vector<std::thread> threads(nthreads);

  //bind lambda expression to threads
  uint const size = _width * _height;
  for (uint th = 0; th<nthreads; th++)
  {
    threads[th] = std::thread(std::bind(
      [&](const int begin_loop, const int end_loop, const int num_thread, const int nthreads)
    {
      // loop over all items
      for (int i = begin_loop; i < end_loop; i++)
      {
        //result[i*nthreads + num_thread] = scalar * _data[i*nthreads + num_thread];
        for (int j = 0; j < 10; j++)
        {
          result[i*nthreads + num_thread] = scalar * exp(j/1.22) * _data[i*nthreads + num_thread];
        }
      }
    }, 0, size/ nthreads, th, nthreads));
  }
  std::for_each(threads.begin(), threads.end(), [](std::thread& x) {x.join(); });

  //residual
  for (unsigned int i = size / nthreads; i < size; i++)
  {
    result[i] = scalar * _data[i];
  }
  */

  unsigned int const size = _width * _height;
  for (unsigned int i = 0; i < size; i++)
  {
    result[i] = scalar * _data[i];
  }

  return result;
}
