/*
*
* author : Arthur Dufay @ inria.fr
* Copyright INRIA 2017
*
*
**/



//test tiny png
#include <string>
#include <iostream>
#include <chrono>

#include "mrf/image/colorimage.hpp"
#include "externals/tinypng/lodepng.h"
#include "mrf/radiometry/d65.hpp"
#include "mrf/image/uniformspectralimage.hpp"

#include "externals/tinypng/lodepng.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace mrf;
using namespace mrf::image;
using namespace mrf::color;
using namespace mrf::radiometry;


typedef std::chrono::high_resolution_clock Clock;

typedef mrf::image::UniformSpectralImage USI;

int main(int argc, char *argv[])
{
  string file_path = "D:\\SpectralData\\hyperspectral_icvl\\BGU_0403-1419-1";
  string header_path = file_path + ".hdr";
  string data_path = file_path + ".raw";

  USI image;
  int dummy_progress_int;
  image.loadEnviFile(header_path, data_path, dummy_progress_int);

  SpectrumConverter spectrum_converter;
  spectrum_converter.init(mrf::color::CIE_1931_2DEG);
  ColorImage rgb_image;

  //t1 = Clock::now();
  image.convertToColorImage(spectrum_converter, rgb_image, dummy_progress_int);
  //t2 = Clock::now();




  //save image as png
  std::vector<unsigned char> png_data_for_output;
  png_data_for_output.resize(rgb_image.width() * rgb_image.height() * 3);//3 channels image
  for (uint i = 0; i < rgb_image.width(); i++)
  {
    for (uint j = 0; j < rgb_image.height(); j++)
    {
      Color color = rgb_image(i, j).xyzToSrgbD65();
      //float norm = 50.f;// 1.f / std::max(std::max(color.r(), color.g()), color.b());
      //color *= norm;
      color = color.clamped(0.0, 1.0);

      png_data_for_output[3 * (j*rgb_image.width() + i)] = unsigned char(color.r() * 255);
      png_data_for_output[3 * (j*rgb_image.width() + i) + 1] = unsigned char(color.g() * 255);
      png_data_for_output[3 * (j*rgb_image.width() + i) + 2] = unsigned char(color.b() * 255);
    }
  }
  uint error = lodepng::encode("./test.png", png_data_for_output,
                               rgb_image.width(), rgb_image.height(), LCT_RGB);


  //save image as envi + hdr spectral file
  image.saveAsEnviFile<unsigned char>("./test_envi_save.hdr", "./test_envi_save.raw", dummy_progress_int);


  //reopen saved image
  USI image2;
  image2.loadEnviFile("./test_envi_save.hdr", "./test_envi_save.raw", dummy_progress_int);

  //convert it
  image2.convertToColorImage(spectrum_converter, rgb_image, dummy_progress_int);

  //save image as png
  for (uint i = 0; i < rgb_image.width(); i++)
  {
    for (uint j = 0; j < rgb_image.height(); j++)
    {
      Color color = rgb_image(i, j).xyzToSrgbD65();
      //float norm = 1.f / std::max(std::max(color.r(), color.g()), color.b());
      //color *= norm;
      color = color.clamped(0.0, 1.0);

      png_data_for_output[3 * (j*rgb_image.width() + i)] = unsigned char(color.r() * 255);
      png_data_for_output[3 * (j*rgb_image.width() + i) + 1] = unsigned char(color.g() * 255);
      png_data_for_output[3 * (j*rgb_image.width() + i) + 2] = unsigned char(color.b() * 255);
    }
  }
  error = lodepng::encode("./test2.png", png_data_for_output,
                          rgb_image.width(), rgb_image.height(), LCT_RGB);
}