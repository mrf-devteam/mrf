// CRT.cpp : Defines the entry point for the console application.
//

#include <string>
#include <iostream>

#include "mrf/util/precision_timer.hpp"

using namespace mrf::util;

class BRDF
{
 public:
  virtual std::string method()
  {
    //std::cout << "BRDF";
    return "BRDF";
  }
  virtual ~BRDF() {}
};

class Lambert : public BRDF
{
 public:
  virtual std::string method()
  {
    //std::cout << "Lambert";
    return "Lambert";
  }
};

class Test
{
 public:
  std::string method()
  {
    //std::cout << "Lambert";
    return "Lambert";
  }
};

int main(int argc, char** argv)
{
  if (argc < 2)
    return -1;

  PrecisionTimer timer;
  size_t nb_call = std::stoul(argv[1]);

  BRDF *brdf = new Lambert;

  timer.start();
  for(size_t i=0; i<nb_call; i++)
    brdf->method(); //outputs "Derived"

  timer.stop();

  std::cout << "Time polymorphism = " << timer.elapsed() << std::endl;

  Test other_lambert;

  timer.reset();
  timer.start();
  for (size_t i = 0; i<nb_call; i++)
    other_lambert.method(); //outputs "Derived"

  timer.stop();
  std::cout << "Time non polymorphism = " << timer.elapsed() << std::endl;

  Test* other_p_lambert = new Test;

  timer.reset();
  timer.start();
  for (size_t i = 0; i<nb_call; i++)
    other_p_lambert->method(); //outputs "Derived"

  timer.stop();
  std::cout << "Time non polymorphism pointer = " << timer.elapsed() << std::endl;

  delete brdf;

  int x;
  std::cin >> x;

  return 0;
}

