/*
*
* author : Arthur Dufay @ inria.fr
* Copyright INRIA 2017
*
*
**/

#include "mrf/radiometry/blackbody.hpp"


//test tiny png
#include <string>
#include <iostream>
#include "mrf/image/colorimage.hpp"
#include "MRF/externals/tinypng/lodepng.h"
#include "MRF/radiometry/d65.hpp"
#include "mrf/image/uniformspectralimage.hpp"

using std::string;
using std::cout;
using std::endl;
using namespace mrf;
using namespace mrf::image;
using namespace mrf::color;
using namespace mrf::radiometry;


int main(int argc, char *argv[])
{
  /*
  float value;
  //test illuminant A
  for (uint i = 0; i < _spectrum.size(); i++)
  {
    value = 100.f*_spectrum[i].second / _spectrum[560 - start_wavelength].second;
  }
  */



  //test blackbody image
  uint start_temperature = 500;
  uint end_temperature = 12000;
  uint image_height = 100;
  uint image_width = end_temperature - start_temperature + 1;
  uint start_wavelength = 300;
  uint end_wavelength = 780;

  std::vector<unsigned char> png_data_for_output;
  png_data_for_output.resize(image_width*image_height * 3);//3 channels image

  UniformSpectralImage u_spectral_image;
  u_spectral_image.init(image_width, image_height);
  
  auto &wavelengths = u_spectral_image.wavelengths();
  wavelengths.resize(end_wavelength - start_wavelength + 1);
  for (uint i = start_wavelength; i <= end_wavelength; i++)
  {
    wavelengths[i - start_wavelength] = i;
  }

  for (uint i = 0; i < image_width; i++)
  {
    mrf::radiometry::BlackBody b;
    b.setTemperature(start_temperature + i);
    b.generateSpectrum(start_wavelength, end_wavelength);
    
    //b.setTemperature(6500);
    //b.generateSpectrum(300, 780);

    //convert spectrum from radiance to irradiance (W*m-2)
    Spectrum<float> & spectrum = b.spectrum();
    float max_intensity = 0.0;
    for (auto it = spectrum.valuesBegin(); it != spectrum.valuesEnd(); ++it)
    {
      *it *= float(M_PI);
      if (*it > max_intensity)
      {
        max_intensity = *it;
      }
    }
    for (auto it = spectrum.valuesBegin(); it != spectrum.valuesEnd(); ++it)
    {
      *it /= max_intensity;
    }

    //instantiate spectrum converter
    SpectrumConverter converter;
    converter.init(mrf::color::CIE_XYZ_2006_2DEG);
    //Color color = converter.emissiveSpectrumToXYZ(spectrum);
    Color color = converter.reflectiveSpectrumToXYZ(spectrum, mrf::radiometry::D65_SPD,
      mrf::radiometry::D_65_FIRST_WAVELENGTH,
      mrf::radiometry::D_65_PRECISION,
      mrf::radiometry::D_65_ARRAY_SIZE);
    
    
    
    color = color.xyzToSrgbD65();
    float norm = 1.f/std::max(std::max(color.r(), color.g()), color.b());
    color *= norm;
    color = color.clamped(0.0, 1.0);

    //color.r() = float(i) / float(image_width);
    //color.g() = float(i) / float(image_width);
    //color.b() = float(i) / float(image_width);

    for (uint j = 0; j < image_height; j++)
    {
      png_data_for_output[3 * (j*image_width + i)    ] = unsigned char(color.r() * 255);
      png_data_for_output[3 * (j*image_width + i) + 1] = unsigned char(color.g() * 255);
      png_data_for_output[3 * (j*image_width + i) + 2] = unsigned char(color.b() * 255);

      u_spectral_image(i, j) = spectrum.values();
    }

  }
  
  //save rgb image
  uint error = lodepng::encode("./blackbody.png", png_data_for_output,
    image_width, image_height, LCT_RGB);

  //save spectral image
  //u_spectral_image.saveAsGrayPngImages("./blackbody_spectral");


  /*
  mrf::image::ColorImage image;
  image.load(string(argv[1]));
  */


}