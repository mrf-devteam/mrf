/*
*
* author : Arthur Dufay @ inria.fr
* Copyright INRIA 2017
*
*
**/



#include <string>
#include <iostream>

#include "mrf/util/cpu_memory.hpp"

using namespace std;
using namespace mrf;
using namespace mrf::util;

int main(int argc, char *argv[])
{
  cout << "Available memory: "<<CPUMemory::availableMemory()<<" MBytes"<<endl;
  cout << "Total memory: " << CPUMemory::totalMemory() << " MBytes" << endl;
  cout << "Used memory: " << CPUMemory::usedMemory() << " MBytes" << endl;


#if defined (_WIN32) || defined (_WIN64)
  system("pause");
#endif
}