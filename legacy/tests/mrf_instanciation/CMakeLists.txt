project(test_instanciation)

cmake_minimum_required(VERSION 2.8)

# IF THE TYPE OF BUILD NOT DEFINED
#
if( NOT CMAKE_BUILD_TYPE )
  set(CMAKE_BUILD_TYPE "Release")
endif()



#DEFINITIONS REGARDING THE BUILD MODE
message(" Building in " ${CMAKE_BUILD_TYPE} " Mode ")


if( ${CMAKE_BUILD_TYPE} STREQUAL "Debug" )
  string(TOUPPER ${PROJECT_NAME} PROJECT_NAME_UPPER)
  add_definitions(-D${PROJECT_NAME_UPPER}_DEBUG)
endif ()

#COMPILER DEFINITIONS
include(CheckCXXCompilerFlag)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# CHECK for Eigen. Necessary for QuadProg++
find_package(Eigen3 3.0 REQUIRED)
message("EIGEN 3 found in ${EIGEN3_INCLUDE_DIR}")
include_directories(${EIGEN3_INCLUDE_DIR})
#---------------------------------------------------------------------------------------------------
# SOURCES MANAGEMENT
#---------------------------------------------------------------------------------------------------

set(MRF_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../..)

# Include the files from the MRF hierarchy
include_directories( ${MRF_DIR} )
include_directories( ${MRF_DIR}/mrf/)

# External Libraries coming from MRF
include_directories( ${MRF_DIR}/externals/)

# External Libraries coming from rgb2spec APplication
include_directories( ${CMAKE_SOURCE_DIR}/externals/)

# Include MRF library as static dependency
#use mrf as static library
add_definitions(-DMRF_WITH_EIGEN_SUPPORT)
add_definitions(-DMRF_STATIC)

add_executable(${PROJECT_NAME} main.cpp)

# We link with mrf directly
# TODO:  USe a FindMRF to have more flexibility ?
# TODO:  Call the MRF CMakeLists  from here?
if (WIN32)
	target_link_libraries(${PROJECT_NAME} debug ${MRF_DIR}/build/Debug/mrf_static.lib )
	target_link_libraries(${PROJECT_NAME} general ${MRF_DIR}/build/Release/mrf_static.lib )
	#target_link_libraries(${PROJECT_NAME} debug ${MRF_DIR}/build/Debug/mrf.lib )
	#target_link_libraries(${PROJECT_NAME} general ${MRF_DIR}/build/Release/mrf.lib )
else()
	target_link_libraries(${PROJECT_NAME} ${MRF_DIR}/build/libmrf.a )
endif()

install(TARGETS ${PROJECT_NAME} DESTINATION ${MRF_DIR}/apps)
