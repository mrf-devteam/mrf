/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2021
 *
 * Author:  Romain Pacanowski @ inria DOT fr
 * Copyright INRIA : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_core/materials/materials.hpp>

#include <mrf_plugins/register_mrf_plugins.hpp>


int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::MeasuredIsotropic a_mat("a_default_measured");
  assert(a_mat.getType() == mrf::materials::BRDFTypes::MEASURED_ISOTROPIC);
  assert(a_mat.name() == "a_default_measured");

  //Test setter/getter

  mrf::Triplet_uint data_size = mrf::Triplet_uint(3, 3, 3);
  a_mat.setDataSize(data_size);
  assert(a_mat.getDataSize() == data_size);

  mrf::uint dirac_size = 4;
  a_mat.setDiracSize(dirac_size);
  assert(a_mat.getDiracSize() == dirac_size);


  //TODO: add assertion for CPU backend function when functionnal

  //More tests on  different accessors

  mrf::geom::LocalFrame lf;
  assert(a_mat.diffuseComponent());
  assert(!a_mat.specularComponent());
  assert(!a_mat.transmissionComponent());
  assert(!a_mat.nonDiffuseComponent());

  assert(a_mat.diffuseAlbedo() == 0.0f);
  assert(a_mat.diffuseAlbedo(lf) == 0.0f);

  assert(a_mat.specularAlbedo() == 0.0f);
  assert(a_mat.specularAlbedo(lf) == 0.0f);
  assert(a_mat.nonDiffuseAlbedo() == 0.0f);

  assert(a_mat.totalAlbedo() == 0.0f);


  assert(a_mat.transmissionAlbedo(lf) == 0.0f);
  assert(a_mat.transmissionAlbedo() == 0.0f);


  assert(a_mat.totalAlbedo() == (a_mat.diffuseAlbedo() + a_mat.specularAlbedo() + a_mat.transmissionAlbedo()));

  assert(a_mat.name() == "a_default_measured");

  // WIP Still a lot of testing to do...

  return EXIT_SUCCESS;
}
