/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_plugins/register_mrf_plugins.hpp>

int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::PrincipledDisney a_mat("a_default_principled");
  assert(a_mat.getType() == mrf::materials::BRDFTypes::PRINCIPLED);
  assert(a_mat.name() == "a_default_principled");

  //Test setter/getter
  assert(a_mat.getBaseColor() == mrf::materials::COLOR(1.f));
  assert(a_mat.getRoughness() == 0.f);
  assert(a_mat.getSubsurface() == 0.f);
  assert(a_mat.getAnisotropic() == 0.f);
  assert(a_mat.getSheen() == 0.f);
  assert(a_mat.getSheenTint() == 0.f);
  assert(a_mat.getClearcoat() == 0.f);
  assert(a_mat.getClearcoatGloss() == 0.f);
  assert(a_mat.getMetallic() == 0.f);
  assert(a_mat.getSpecular() == 0.f);
  assert(a_mat.getSpecularTint() == 0.f);


  mrf::materials::COLOR color(0.7f);
  a_mat.setBaseColor(color);
  assert(a_mat.getBaseColor() == color);

  float roughness = 0.3f;
  a_mat.setRoughness(roughness);
  assert(a_mat.getRoughness() == roughness);

  float subsurface = 0.1f;
  a_mat.setSubsurface(subsurface);
  assert(a_mat.getSubsurface() == subsurface);

  float sheen = 0.2f;
  a_mat.setSheen(sheen);
  assert(a_mat.getSheen() == sheen);

  float sheenTint = 0.5f;
  a_mat.setSheenTint(sheenTint);
  assert(a_mat.getSheenTint() == sheenTint);

  float clearcoat = 0.15f;
  a_mat.setClearcoat(clearcoat);
  assert(a_mat.getClearcoat() == clearcoat);

  float clearcoatGloss = 0.25f;
  a_mat.setClearcoatGloss(clearcoatGloss);
  assert(a_mat.getClearcoatGloss() == clearcoatGloss);

  float specular = 0.35f;
  a_mat.setSpecular(specular);
  assert(a_mat.getSpecular() == specular);

  float specularTint = 0.55f;
  a_mat.setSpecularTint(specularTint);
  assert(a_mat.getSpecularTint() == specularTint);

  float metallic = 0.5f;
  a_mat.setMetallic(metallic);
  assert(a_mat.getMetallic() == metallic);

  float anisotropic = 0.05f;
  a_mat.setAnisotropic(anisotropic);
  assert(a_mat.getAnisotropic() == anisotropic);


  //Test basic constructor
  mrf::materials::PrincipledDisney a_mat2(
      "a_default_principled",
      color,
      roughness,
      subsurface,
      sheen,
      sheenTint,
      clearcoat,
      clearcoatGloss,
      metallic,
      specular,
      specularTint,
      anisotropic);

  assert(a_mat2.getType() == mrf::materials::BRDFTypes::PRINCIPLED);
  assert(a_mat2.name() == "a_default_principled");
  assert(a_mat2.getBaseColor() == color);
  assert(a_mat2.getRoughness() == roughness);
  assert(a_mat2.getSubsurface() == subsurface);
  assert(a_mat2.getSheen() == sheen);
  assert(a_mat2.getSheenTint() == sheenTint);
  assert(a_mat2.getClearcoat() == clearcoat);
  assert(a_mat2.getClearcoatGloss() == clearcoatGloss);
  assert(a_mat2.getSpecular() == specular);
  assert(a_mat2.getSpecularTint() == specularTint);
  assert(a_mat2.getMetallic() == metallic);
  assert(a_mat2.getAnisotropic() == anisotropic);



  //TODO: add assertion for CPU backend function when functionnal


  return EXIT_SUCCESS;
}
