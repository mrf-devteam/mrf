/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_plugins/register_mrf_plugins.hpp>

int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::WalterBSDF a_mat("a_walter_bsdf");
  assert(a_mat.getType() == mrf::materials::BRDFTypes::WALTER_BSDF);
  assert(a_mat.name() == "a_walter_bsdf");

  //Test setter/getter
  assert(a_mat.ior().eta() == mrf::materials::COLOR(1.f));
  assert(a_mat.ior().eta() == a_mat.microfacet()->ior().eta());
  assert(a_mat.eta() == mrf::materials::COLOR(1.f));
  assert(a_mat.eta() == a_mat.microfacet()->ior().eta());

  assert(a_mat.ior().kappa() == mrf::materials::COLOR(0.f));
  assert(a_mat.ior().kappa() == a_mat.microfacet()->ior().kappa());
  assert(a_mat.k() == mrf::materials::COLOR(0.f));
  assert(a_mat.k() == a_mat.microfacet()->ior().kappa());

  assert(a_mat.alpha_g() == 0.f);
  assert(a_mat.alpha_g() == a_mat.microfacet()->alpha_g());

  assert(a_mat.extinction() == mrf::materials::COLOR(0.f));


  mrf::materials::COLOR eta(1.5f);
  mrf::materials::COLOR kappa(3.5f);
  a_mat.microfacet()->setFresnel(eta, kappa);
  assert(a_mat.microfacet()->ior().eta() == eta);
  assert(a_mat.ior().eta() == eta);
  assert(a_mat.eta() == eta);
  assert(a_mat.microfacet()->ior().kappa() == kappa);
  assert(a_mat.ior().kappa() == kappa);
  assert(a_mat.k() == kappa);

  mrf::materials::COLOR extinction(0.2f);
  a_mat.setExtinction(extinction);
  assert(a_mat.extinction() == extinction);

  float alpha_g = 0.1f;
  a_mat.microfacet()->setAlpha(alpha_g);
  assert(a_mat.alpha_g() == alpha_g);

  //Test other constructors
  mrf::materials::WalterBSDF a_mat2("a_walter_bsdf", alpha_g, eta, kappa, extinction);
  assert(a_mat2.getType() == mrf::materials::BRDFTypes::WALTER_BSDF);
  assert(a_mat2.name() == "a_walter_bsdf");
  assert(a_mat2.microfacet()->ior().eta() == eta);
  assert(a_mat2.ior().eta() == eta);
  assert(a_mat2.eta() == eta);
  assert(a_mat2.microfacet()->ior().kappa() == kappa);
  assert(a_mat2.ior().kappa() == kappa);
  assert(a_mat2.k() == kappa);
  assert(a_mat2.extinction() == extinction);
  assert(a_mat2.alpha_g() == alpha_g);

  //TODO : reactivate later when GGX and AnisoGGX are merged
  //mrf::materials::WalterBSDF a_mat3("a_walter_bsdf", alpha_g, alpha_g, eta, kappa, extinction);
  //assert(a_mat3.getType() == mrf::materials::BRDFTypes::WALTER_BSDF);
  //assert(a_mat3.name() == "a_walter_bsdf");
  //assert(a_mat3.microfacet()->ior().eta() == eta);
  //assert(a_mat3.ior().eta() == eta);
  //assert(a_mat3.eta() == eta);
  //assert(a_mat3.microfacet()->ior().kappa() == kappa);
  //assert(a_mat3.ior().kappa() == kappa);
  //assert(a_mat3.k() == kappa);
  //assert(a_mat3.extinction() == extinction);
  //assert(a_mat3.alpha_g() == alpha_g);


  //TODO: add assertion for CPU backend function when functionnal


  return EXIT_SUCCESS;
}
