/**
 * Author: Romain Pacanowski @ institutoptique DOT fr
 * Copyright CNRS : 2020
 **/


#include <cstdlib>

#include <mrf_core/data_struct/array2d.hpp>

std::size_t operator"" _sz(unsigned long long int x)
{
  return x;
}

int main(int /*argc*/, char ** /*argv*/)
{
  using namespace mrf::data_struct;


  Array2D a1(10_sz, 10_sz);
  Array2D a2(10_sz, 10_sz);

  assert(a1 == a2);
  assert(a1.cols() == a2.width());
  assert(a1.rows() == a2.height());

  Array2D a3(1_sz, 10_sz);
  assert(a1 != a3);


  Array2D sum_a1_a2 = a1 + a2;
  assert(a1 == sum_a1_a2);

  Array2D diff_a1_a2 = a1 - a2;
  assert(a2 == diff_a1_a2);


  Array2D const square_diff = (a1 - a1).square();

  float const sum = square_diff.sum();
  assert(sum == 0.0f);

  Array2D complex_computation = a1.square().sqrt().pow(4.0f);

  Array2D a10(2_sz, 2_sz);

  Array2D a30(2_sz, 2_sz);
  a30(0) = 2.0f;
  a30(1) = 2.0f;
  a30(2) = 2.0f;
  a30(3) = 2.0f;

  Array2D diff_a30_a30 = a30 - a30;
  assert(diff_a30_a30 == a10);

  Array2D a4(2_sz, 2_sz);
  a4(0) = 4.0f;
  a4(1) = 4.0f;
  a4(2) = 4.0f;
  a4(3) = 4.0f;

  Array2D a4_sqrt = a4.sqrt();
  assert(a4_sqrt == a30);

  //Back to its original value
  a4.square();

  Array2D a30_sqr = a30.square();
  assert(a30_sqr == a4);

  //Back to its orinal value
  a30.sqrt();

  Array2D a30_pow2 = a30.pow(2.0f);
  assert(a30_pow2 == a4);

  // Back to its orinal value
  a30.sqrt();

  Array2D a5(2_sz, 2_sz);
  a5(0) = -4.0;
  a5(1) = -4.0;
  a5(2) = -4.0;
  a5(3) = -4.0;

  assert(a5.sum() == -16.0f);

  Array2D a5_abs = a5.abs();
  assert(a5_abs == a4);

  assert(a5_abs.sum() == 16.0f);

  Array2D a6(2_sz, 2_sz);

  a6.setConstant(2.0f);
  assert(a6 == a30);

  a6.setZero();
  assert(a6(0) == 0.0f);
  assert(a6(1) == 0.0f);
  assert(a6(2) == 0.0f);
  assert(a6(3) == 0.0f);


  Array2D a7(2_sz, 2_sz);

  assert(a6.width() == a7.width());
  assert(a6.height() == a7.height());
  assert(a7(0) == 0.0f);
  assert(a7(1) == 0.0f);
  assert(a7(2) == 0.0f);
  assert(a7(3) == 0.0f);
  assert(a6 == a7);


  a7(0) = 1.0f;
  a7(1) = 1.0f;
  a7(2) = 1.0f;
  a7(3) = 1.0f;

  Array2D a71(2_sz, 2_sz);
  a71.setOnes();

  assert(a71 == a7);

  a6.setOnes();
  assert(a6(0) == 1.0f);

  assert(a6 == a7);
  assert(a6.sum() == 4.0f);

  // Testing Copy Constructors and assignment operator=

  Array2D a8(10_sz, 100_sz);

  // This type of assignment with Size different
  a7 = a8;

  assert(a7.width() == a8.width());
  assert(a7.height() == a8.height());
  assert(a7 == a8);

  //Copy Constructor
  Array2D a9 = a7;
  assert(a7.width() == a9.width());
  assert(a7.height() == a9.height());
  assert(a7 == a9);

  // Other Test
  Array2D a20(a8);
  assert(a20 == a8);

  // Resize Test
  a1.resize(a8.height(), a8.width());
  assert(a1.width() == a8.width());
  assert(a1.height() == a8.height());
  a1.setZero();
  assert(a1 == a20);

  // More tests
  a1 = a7;
  assert(a1 == a7);

  a1.setZero();
  Array2D a11(a1.width(), a1.height());

  assert(a1 == a11);

  assert(a11 == a1);

  return EXIT_SUCCESS;
}
