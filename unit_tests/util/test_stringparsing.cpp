
/**
 * Author: Romain Pacanowski @ institutoptique DOT fr
 * Copyright CNRS : 2020
 **/

#include <mrf_core/util/string_parsing.hpp>

#include <cassert>
#include <cstdlib>
#include <string>


int main(int /* argc */, char ** /* argv */)
{
  using namespace std;
  using namespace mrf::util;

  std::string filename("foo.bar");


  // Some test regarding directory and extensions
  std::string extension;
  StringParsing::getFileExtension(filename, extension);

  assert(extension == "bar");

  std::string filename_wo_extension;
  StringParsing::getFileNameWithoutExtension(filename, filename_wo_extension);

  assert(filename_wo_extension == "foo");

  std::string current_dir;
  StringParsing::getDirectory(filename.c_str(), current_dir);

  assert(current_dir == "./");

  filename = "/home/foo_user/foo.bar";
  StringParsing::getDirectory(filename.c_str(), current_dir);

  assert(current_dir == "/home/foo_user/");


  // Testing Methods to tokenize
  string const DELIMITER          = "#";
  std::string  a_tokenizer_tester = "FOO#TOTO#TUTU#TITI#TATA";

  std::vector<std::string> tokens;
  unsigned int             nb_of_delimiters;

  StringParsing::tokenize(a_tokenizer_tester, tokens, nb_of_delimiters, DELIMITER);

  assert(nb_of_delimiters == 4);
  assert(tokens.size() == 5);
  assert(tokens[2] == "TUTU");

  tokens.clear();
  StringParsing::tokenize(a_tokenizer_tester.c_str(), tokens, nb_of_delimiters, DELIMITER);
  assert(nb_of_delimiters == 4);
  assert(tokens.size() == 5);
  assert(tokens[2] == "TUTU");


  //Replacing occurences
  StringParsing::replaceAllOccurences(a_tokenizer_tester, "#", " ");

  std::string const TEST_REPLACE_ALL_OCCURENCES = "FOO TOTO TUTU TITI TATA";

  assert(TEST_REPLACE_ALL_OCCURENCES == a_tokenizer_tester);

  //Paths absolute relative and co

  std::string a_path = "./foo/foo.exr";

  assert(StringParsing::pathIsRelative(a_path));
  assert(!StringParsing::pathIsAbsolute(a_path));

  // Next test
  // Windows case
  std::string output;

#ifdef _WIN32
  a_path = "F://foo//foo.hdr";
  assert(!StringParsing::pathIsRelative(a_path));
  assert(StringParsing::pathIsAbsolute(a_path));

  std::string ext;
  assert(StringParsing::extensionFromAbsPath(a_path.c_str(), ext));

  assert(ext == "hdr");

#else

  a_path = "/home/foo/foo.hdr";
  assert(!StringParsing::pathIsRelative(a_path));
  assert(StringParsing::pathIsAbsolute(a_path));

  std::string ext;
  assert(StringParsing::extensionFromAbsPath(a_path.c_str(), ext));

  assert(ext == "hdr");
#endif


  assert(StringParsing::fileNameFromAbsPathWithoutExtension(a_path.c_str(), output));
  assert(output == "foo");


  a_path = "foobar.html";
  assert(StringParsing::fileNameFromAbsPath(a_path.c_str(), output));
  assert(a_path == output);


  // NEW TEST : incrementFilename
  string exr_file = "foo000.exr";
  string new_name;

  StringParsing::incrementFilename(exr_file.c_str(), new_name);
  assert(new_name == "foo001.exr");

  exr_file = "foo999.exr";
  StringParsing::incrementFilename(exr_file.c_str(), new_name);
  assert(new_name == "foo1000.exr");

  //
  string file_wo_extension = "fooBAR";
  string output_file_wo_extension;
  // This SHould return false since there is not "." in file_wo_extension
  assert(!StringParsing::getFileNameWithoutExtension(file_wo_extension.c_str(), output_file_wo_extension));


  //NEW TEST : string to numerics conversion

  //--- Float
  string a_dbl_string = "3.14159";
  float  a_pi;
  assert(StringParsing::convertStringToFloat(a_dbl_string.c_str(), a_pi));
  assert(a_pi == 3.14159f);

  //--- Double
  double a_double_pi;
  assert(StringParsing::convertStringToDouble(a_dbl_string.c_str(), a_double_pi));
  assert(a_double_pi == 3.14159);


  //---Uint Case
  string       a_uint_string = "3";
  unsigned int a_uint_pi;
  assert(StringParsing::convertStringToUInt(a_uint_string.c_str(), a_uint_pi));
  assert(a_uint_pi == 3);


  string a_int_string = "-122";
  int    a_int;
  assert(StringParsing::convertStringToInt(a_int_string.c_str(), a_int));
  assert(a_int == -122);

  string an_int_big_numerical_string = "2e4500";
  assert(!StringParsing::convertStringToInt(an_int_big_numerical_string.c_str(), a_int));

  string an_int_small_numerical_string = "-245000000000000000000000000000000000000000000000";
  assert(!StringParsing::convertStringToInt(an_int_small_numerical_string.c_str(), a_int));


  //--- Failure cases
  string an_impossible_numerical_string = "FOOBAR";
  assert(!StringParsing::convertStringToDouble(an_impossible_numerical_string.c_str(), a_double_pi));
  assert(!StringParsing::convertStringToFloat(an_impossible_numerical_string.c_str(), a_pi));
  assert(!StringParsing::convertStringToUInt(an_impossible_numerical_string.c_str(), a_uint_pi));
  assert(!StringParsing::convertStringToInt(an_impossible_numerical_string.c_str(), a_int));

  //std::cout << " a uint pi = " << a_uint_pi << std::endl;


  string       large_numerical_string = "10000000000000000000000000000000000000000";
  unsigned int a_uint;
  assert(!StringParsing::convertStringToUInt(large_numerical_string.c_str(), a_uint));
  assert(!StringParsing::convertStringToUInt(large_numerical_string, a_uint));

  //  string large_numerical_string_for_float = "10000000000000000000000000000000000000000.0";
  float a_float;
  assert(!StringParsing::convertStringToFloat(large_numerical_string.c_str(), a_float));

  string a_double_large_numerical_string = "235e4500";
  double a_double;
  assert(!StringParsing::convertStringToDouble(a_double_large_numerical_string.c_str(), a_double));

  string a_double_small_numerical_string = "2e-4500";
  assert(!StringParsing::convertStringToDouble(a_double_large_numerical_string.c_str(), a_double));


  string test_weird_numerical_string = "FOOBAR100";
  assert(!StringParsing::convertStringToUInt(test_weird_numerical_string, a_uint));




  //NEW TEST:  string empty
  string empty = " ";
  assert(StringParsing::isStringEmpty(empty));

  empty = "\t";
  assert(StringParsing::isStringEmpty(empty));

  empty = "\n";
  assert(StringParsing::isStringEmpty(empty));

  empty = "\r";
  assert(StringParsing::isStringEmpty(empty));

  empty = "\r\n";
  assert(StringParsing::isStringEmpty(empty));


  empty = "FOOOOOOO";
  assert(!StringParsing::isStringEmpty(empty));




  return EXIT_SUCCESS;
}