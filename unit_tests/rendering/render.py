#!python3

#########################################################################################
#
# Author:  David Murray       Copyright CNRS  : 2021
#
#########################################################################################

######################NON-REGRESSION RENDERING TESTS#####################################
#
#
#########################################################################################

import sys
import argparse
import time
import glob


def  launch_mdiff(path_mdiff_executable, full_path_to_rendered_image, full_path_to_reference_image):

  mdiff_args = []
  mdiff_args.append(path_mdiff_executable)
  mdiff_args.append("-i")
  mdiff_args.append(full_path_to_rendered_image)
  mdiff_args.append(full_path_to_reference_image)
  mdiff_args.append("-diff_on_error")

  print("[DEBUG]  mdiff args are " + str(mdiff_args) )

  process_mdiff = subprocess.Popen(mdiff_args)

  while process_mdiff.poll() is None:
    #print('Still rendering')
      time.sleep(5)

  if(process_mdiff.returncode != 0):
    sys.exit(1)
  else:
    print("mdiff exited with returncode %d" % process_mdiff.returncode)

#end_launch_mdiff


parser = argparse.ArgumentParser()
parser.add_argument("--app_path", help="Path to the malia executable", default="/bin/")
parser.add_argument("--scene_path", help="Scene input folder", default=".")
parser.add_argument("--output", help="Image output folder", default=".")
parser.add_argument("--references", help="Image references folder", default=".")
parser.add_argument("--spectral", help="Spectral rendering or RGB", action='store_true')
parser.add_argument("--compare", help="Spectral rendering or RGB", action='store_true')
parser.add_argument("--multicam", help="Malia will render one image per camera defined in the .mcf", action='store_true')
args = parser.parse_args()

start = time.time()

RENDERER_NAME = "malia"

nb_samples = "50"
wavelength = "400:750:50"
multiplexing = "8"
scene_file = []

import os
import re
import subprocess
import platform
import xml.etree.ElementTree as ET
from shutil import copyfile
from shutil import move

output_img_folder = os.path.abspath(args.output)
ref_img_folder = os.path.abspath(args.references)

RENDERER_PATH = os.path.abspath(args.app_path)
current_directory = os.getcwd()

#First found the scene files in the current directory
for dirname, dirnames, filenames in os.walk(args.scene_path):
    for filename in filenames:
        if(filename.endswith(".msf")):
            #print(os.path.join(dirname, filename))
            #brdf_file.append(os.path.join(dirname, filename))

            temp_msf_file_name = filename
            if(dirname!="."):
                temp_msf_file_name = dirname+"/"+temp_msf_file_name
            else:
                temp_msf_file_name = os.path.join(current_directory,"./"+temp_msf_file_name)

            scene_file.append(temp_msf_file_name)

####render using subprocess.Popen
if args.spectral:
    renderer = os.path.join(RENDERER_PATH, RENDERER_NAME)
else:
    renderer = os.path.join(RENDERER_PATH, RENDERER_NAME +"_rgb")

if platform.system()=="Windows":
    renderer = renderer + ".exe"

#loop on measured material
for scene in scene_file:

    abs_scene = os.path.abspath(scene)
    print("Render the scene " + abs_scene)
    src = abs_scene

    modes = []
    if args.spectral:
      modes.append("spectral_single")
      modes.append("spectral_multi")
    else:
      modes.append("rgb")

    for mode in modes:

      #Render the scene
      output_image = os.path.join(output_img_folder, os.path.basename(src)[:-4]) + "_" + mode + ".exr"

      renderer_args = []

      renderer_args.append(renderer)
      renderer_args.append("-scene")
      renderer_args.append(src)
      renderer_args.append("-rng_seed")
      renderer_args.append("0")
      if mode == "spectral_single":
          renderer_args.append("-wr")
          renderer_args.append(wavelength)
          renderer_args.append("-wpp")
          renderer_args.append("1")
      elif mode == "spectral_multi":
          renderer_args.append("-wr")
          renderer_args.append(wavelength)
          renderer_args.append("-wpp")
          renderer_args.append(multiplexing)

      renderer_args.append("-samples")
      renderer_args.append(nb_samples)
      renderer_args.append("-o")
      renderer_args.append(output_image)
      renderer_args.append("-logging")
      renderer_args.append("3")

      process_renderer = subprocess.Popen(renderer_args)

      while process_renderer.poll() is None:
        #print('Still rendering')
          time.sleep(5)

      if(process_renderer.returncode != 0):
        sys.exit(1)
      else:
        print("Malia exited with returncode %d" % process_renderer.returncode)

        if args.compare:
          #Scene is rendered, now make the diff with the ref image
          mdiff = os.path.join(RENDERER_PATH, "mdiff")
          if platform.system()=="Windows":
            mdiff = mdiff + ".exe"

          if not(args.multicam):

            print("[DEBUG] Starting mdiff. NO MULTICAM")

            ref_image = os.path.join(ref_img_folder, os.path.basename(src)[:-4]) + "_" + mode + ".exr"

            launch_mdiff(mdiff,output_image, ref_image)

          else:
            print("[DEBUG] Starting mdiff. MULTICAM mode")

            ref_images_paths = glob.glob(ref_img_folder + "/*_"+ mode +"*.exr" )
            print(str(ref_img_folder))
            print(str(ref_images_paths))

            for a_path_to_ref_image in ref_images_paths:
              reference_image_name = os.path.basename(a_path_to_ref_image)
              output_image_path = os.path.join(output_img_folder, reference_image_name)

              print("[DEBUG] Full path to reference image" + str(a_path_to_ref_image))
              print("[DEBUG] Reference image to be compared: " + str(reference_image_name) )
              print("[DEBUG] RENDERED output image path is: " + str(output_image_path) )

              launch_mdiff(mdiff, output_image_path, a_path_to_ref_image)

            #end_for


          #end_else multicam case for mdiff comparisons

end = time.time()
print("Rendering script executed in " + str(end - start) + " s.")
