/**
 * Author: Romain Pacanowski @ INRIA DOT fr
 * Copyright INRIA : 2022
 **/


#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/color_image.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/image_diff.hpp>

#include <cassert>
#include <cstdlib>
#include <string>
#include <memory>

using namespace std;
using namespace mrf::image;


float const EPSILON_TEST = 1e-4;

void testPNGImages(char **argv)
{
  //---------------------------------------------------------------------------
  // PNG
  //---------------------------------------------------------------------------
  std::string                 png_filename(argv[1]);
  std::unique_ptr<ColorImage> c_img1;

  try
  {
    c_img1 = loadColorImage(png_filename);

    std::unique_ptr<ColorImage> copy_cimg1 = std::unique_ptr<ColorImage>(new PNGColorImage(*c_img1));

    std::unique_ptr<ColorImage> diff_image = std::unique_ptr<ColorImage>(new PNGColorImage());

    diff_image->init(c_img1->width(), c_img1->height(), true);

    float diff_img1 = mse_diff(*c_img1, *copy_cimg1, *diff_image);

    assert(diff_img1 < EPSILON_TEST);
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &f)
  {
    assert(f == MRF_NO_ERROR);
  }
}


void testPFMImages(char *argv)
{
  //---------------------------------------------------------------------------
  // PFM
  //---------------------------------------------------------------------------
  std::string                 pfm_filename(argv);
  std::unique_ptr<ColorImage> c_img1;

  try
  {
    c_img1 = loadColorImage(pfm_filename);

    std::unique_ptr<ColorImage> copy_cimg1 = std::unique_ptr<ColorImage>(new PFMColorImage(*c_img1));

    std::unique_ptr<ColorImage> diff_image = std::unique_ptr<ColorImage>(new PFMColorImage());

    diff_image->init(c_img1->width(), c_img1->height(), true);

    float diff_img1 = mse_diff(*c_img1, *copy_cimg1, *diff_image);

    assert(diff_img1 < EPSILON_TEST);
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &f)
  {
    std::cout << "  Error = " << f << std::endl;

    assert(f == MRF_NO_ERROR);
  }
}


void testEXRImages(char *argv)
{
#ifndef MRF_RENDERING_MODE_SPECTRAL

  //---------------------------------------------------------------------------
  // EXR
  //---------------------------------------------------------------------------
  std::string                 exr_filename(argv);
  std::unique_ptr<ColorImage> c_img1;

  try
  {
    c_img1 = load(exr_filename);

    std::unique_ptr<ColorImage> copy_cimg1 = std::unique_ptr<ColorImage>(new EXRColorImage(*c_img1));

    std::unique_ptr<ColorImage> diff_image = std::unique_ptr<ColorImage>(new EXRColorImage());

    diff_image->init(c_img1->width(), c_img1->height(), true);

    float diff_img1 = rmse_diff(*c_img1, *copy_cimg1, *diff_image);

    assert(diff_img1 < EPSILON_TEST);

    float diff_img2 = rmse_diff(*c_img1, *copy_cimg1);

    assert(diff_img2 < EPSILON_TEST);
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &f)
  {
    assert(f == MRF_NO_ERROR);
  }
#endif
}

//void testSpectralEXRImages with load function



int main(int argc, char **argv)
{
  if (argc != 4)
  {
    std::cout << " This tests requires 3 input images (png, pfm and exr) " << std::endl;
    return EXIT_FAILURE;
  }

  testPNGImages(argv);
  testPFMImages(argv[2]);
  testEXRImages(argv[3]);


  return EXIT_SUCCESS;
}