/**
 * Author: Romain Pacanowski @ institutoptique DOT fr
 * Copyright CNRS : 2020
 **/

#include <mrf_core/image/artraw_spectral_image.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/color_image.hpp>


#include <cassert>
#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;
using namespace mrf::image;


void my_progress_function(int progress)
{
  std::cout << " Progress aT: " << progress << std::endl;
}


//---------------------------------------------------------------------------
// TESTING saving with ofstream
//---------------------------------------------------------------------------
void testingSaveWithOfStream(ArtRawSpectralImage &img4)
{
  try
  {
    std::ofstream file_writer("test-of-save2.artraw", std::ios::binary | std::ios::out);
    string const  foo_comments = "FOO_COMMENTS";
    img4.save(file_writer, foo_comments, my_progress_function);
    file_writer.close();


    ArtRawSpectralImage img5("test-of-save2.artraw");
    assert(img5.width() == img4.width());
    assert(img5 == img4);
  }
  catch (...)
  {
    std::cerr << " ERROR for test testingSaveWithOfStream " << std::endl;
    assert(false);
  }
}

int main(int argc, char **argv)
{
  if (argc < 2)
  {
    return EXIT_FAILURE;
  }

  assert(isSpectralExtension("artraw"));


  //---------------------------------------------------------------------------
  // ART RAW
  //---------------------------------------------------------------------------
  std::string filename(argv[1]);

  std::cout << "[INFO] Testing if img1 (" << filename << " ) is correctly loaded" << std::endl;
  try
  {
    ArtRawSpectralImage img1(filename);

    // TO DEBUG
    std::cout << "img1.width() = " << img1.width() << std::endl;
    std::cout << "img1.height() = " << img1.height() << std::endl;


    std::cout << "[INFO] Testing if img1  is correctly saved to test_save.artraw" << std::endl;
    string const outsave_filename = "test_save.artraw";

    mrf::image::IMAGE_LOAD_SAVE_FLAGS ret = img1.save(outsave_filename);
    assert(ret == MRF_NO_ERROR);

    std::cout << "[INFO] Testing if img2 is correctly loaded from test_save.artraw" << std::endl;
    ArtRawSpectralImage img2(outsave_filename);

    // Compare img1 and img2. They should be the same
    std::cout << "[INFO] Testing if img2 is equal to img1" << std::endl;
    assert(img1.width() == img2.width());
    assert(img1 == img2);

    // More tests to be done
    //Checking if meta data are read correctly
    auto metadata = img2.metadata();

    for (auto const &a_meta_data : metadata)
    {
      std::cout << "KEY=" << a_meta_data.first << "#VALUE=" << a_meta_data.second << "#" << std::endl;
    }

    auto key_value_it = metadata.find("DPI:");
    assert(key_value_it != metadata.end());
    assert(key_value_it->second == "72.0 x 72.0");

    //---------------------------------------------------------------------------
    // Now testing more Functions
    //---------------------------------------------------------------------------
    assert(!mrf::image::isRadianceHDR("test_save.artraw"));

    //---------------------------------------------------------------------------
    // TESTING Another CONSTRUCTOR with width,height and wavelengths.
    //---------------------------------------------------------------------------
    std::vector<mrf::uint> const &wavelengths_from_img2 = img2.wavelengths();
    ArtRawSpectralImage           img3(img2.width(), img2.height(), wavelengths_from_img2);
    std::vector<mrf::uint> const &wavelengths_from_img3 = img3.wavelengths();


    assert(wavelengths_from_img3.size() == wavelengths_from_img2.size());
    assert(wavelengths_from_img3[0] == wavelengths_from_img2[0]);

    //---------------------------------------------------------------------------
    //  TESTING construction with ifstream
    //---------------------------------------------------------------------------
    std::ifstream file_reader(filename);

    ArtRawSpectralImage img4(file_reader, my_progress_function);
    assert(img1 == img4);

    //---------------------------------------------------------------------------
    testingSaveWithOfStream(img4);
    //---------------------------------------------------------------------------

    // We copy img4 to img41 and modify the metadata of img41
    ArtRawSpectralImage img41 = img4;
    img41.setPlatformName("A FOO PLATFORM");
    img41.setCreationProgram("test_load_and_save_unit_test");
    img41.setCreationCommandLine(argc, argv);
    // saving it to disk
    img41.save("test-of-save2-with-metadata-modified.artraw");

    // NOW the test reading img6 from disk
    ArtRawSpectralImage                img6("test-of-save2-with-metadata-modified.artraw");
    std::map<std::string, std::string> metadata_img6 = img6.metadata();

    //TOKEN_PLATFORM is private within ArtRawSpectralImage ...
    // Hacking with hard-coded string
    std::string const  PLATFORM_STRING("Platform:");
    std::string const &value = metadata_img6[PLATFORM_STRING];

    assert(value == std::string("A FOO PLATFORM"));

    //---------------------------------------------------------------------------
    // TEST ANOTHER CONSTRUCTOR
    //---------------------------------------------------------------------------
    std::vector<mrf::uint> const &wavelengths = img6.wavelengths();

    mrf::uint const start_w = wavelengths[0];
    mrf::uint const stop_w  = wavelengths[wavelengths.size() - 1];
    mrf::uint const nb_w    = wavelengths.size();

    ArtRawSpectralImage img7(img6.width(), img6.height(), start_w, stop_w, nb_w);

    assert(img7 != img6);

    //TEST A FAILURE CASE where the file does not exist

    try
    {
      ArtRawSpectralImage img8("foo.artraw");
      return EXIT_FAILURE;
    }
    catch (...)
    {
      assert(true);
    }

    try
    {
      std::ifstream       in_reader("foot.artraw");
      ArtRawSpectralImage img9(in_reader, my_progress_function);

      return EXIT_FAILURE;
    }
    catch (...)
    {
      assert(true);
    }
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << '\n';
    assert(0);
  }




  //TODO test a real .hdr WARD image




  return EXIT_SUCCESS;
}
