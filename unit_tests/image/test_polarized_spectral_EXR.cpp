
/**
 * Author: Romain Pacanowski @ inria DOT fr
 * Copyright INRIA : 2022
 **/

#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/polarised_spectral_image.hpp>
#include <mrf_core/image/exr_spectral_image.hpp>
#include <mrf_core/image/image_diff.hpp>

#include <cassert>
#include <cstdlib>
#include <string>

int main(int argc, char **argv)
{
  using namespace std;
  using namespace mrf;
  using namespace mrf::image;

  assert(isSpectralExtension("exr"));



  if (argc != 2)
  {
    std::cout << "This tests requires an input image " << std::endl;
    return EXIT_FAILURE;
  }

  try
  {
    //SOME DEFAULT CONSTRUCTORS
    EXRSpectralImage default_img;

    assert(default_img.getSpectrumType() == radiometry::SPECTRUM_EMISSIVE);

    size_t const DEFAULT_WIDTH  = 50;
    size_t const DEFAULT_HEIGHT = 200;

    EXRSpectralImage img1(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    assert(img1.width() == DEFAULT_WIDTH);
    assert(img1.height() == DEFAULT_HEIGHT);


    // LOAD AND SAVE
    std::string const filename(argv[1]);
    std::cout << "[INFO] filename = " << filename << std::endl;

    EXRSpectralImage pola_img1(filename);
    assert(pola_img1.polarised());


    std::string const TEST_FILENAME_1 = "test_save_polarized_spectral_exr.exr";
    pola_img1.save(TEST_FILENAME_1);


    EXRSpectralImage pola_img2(TEST_FILENAME_1);

    assert(pola_img1 == pola_img2);
    assert(pola_img1.polarised());

    assert(pola_img1.nSpectralBands() == 8);
    assert(pola_img1.nStokesComponents() == 4);

    std::vector<mrf::uint> const &wavelengths_before = pola_img2.wavelengths();


    // TESTING setting to zeros the image
    pola_img2.zeros();
    pola_img1.zeros();
    assert(pola_img1 == pola_img2);


    // TESTING clearConserveWavelenths()
    pola_img2.clearConserveWavelengths();
    std::vector<mrf::uint> const &wavelengths_after = pola_img2.wavelengths();

    assert(wavelengths_after.size() == wavelengths_before.size());


    //TESTING isSpectral
    bool is_spectral = EXRSpectralImage::isSpectral(filename);
    assert(is_spectral);


    //TESTING Manual creation of polarized image
    EXRSpectralImage a_polarized_img1(
        radiometry::SPECTRUM_REFLECTIVE | radiometry::SPECTRUM_EMISSIVE | radiometry::SPECTRUM_POLARISED);

    mrf::uint const FIRST_WAVELENGTH = 300;
    mrf::uint const LAST_WAVELENGTH  = 1000;
    mrf::uint const NB_WAVELENGTH    = 50;


    EXRSpectralImage a_polarized_img2(
        100,
        30,
        FIRST_WAVELENGTH,
        LAST_WAVELENGTH,
        NB_WAVELENGTH,
        radiometry::SPECTRUM_REFLECTIVE | radiometry::SPECTRUM_EMISSIVE | radiometry::SPECTRUM_POLARISED);
    assert(a_polarized_img1 != a_polarized_img2);
    assert(a_polarized_img2.polarised());
    assert(a_polarized_img2.emissive());
    assert(a_polarized_img2.reflective());

    // TEST Reflective Buffer
    std::vector<data_struct::Array2D> &refl_buffer = a_polarized_img2.reflectiveFramebuffer();
    assert(refl_buffer[0].width() == 100);
    assert(refl_buffer[0].height() == 30);

    std::vector<data_struct::Array2D> const &refl_buffer2 = a_polarized_img2.reflectiveFramebuffer();

    assert(refl_buffer2[refl_buffer2.size() - 1] == refl_buffer[refl_buffer2.size() - 1]);
    assert(refl_buffer[0].width() == 100);
    assert(refl_buffer[0].height() == 30);



    EXRSpectralImage a_polarized_img3(
        30,
        100,
        FIRST_WAVELENGTH,
        LAST_WAVELENGTH,
        NB_WAVELENGTH,
        radiometry::SPECTRUM_EMISSIVE | radiometry::SPECTRUM_POLARISED);
    assert(a_polarized_img3.emissive());
    assert(a_polarized_img3.nStokesComponents() == 4);
    assert(a_polarized_img3.polarised());


    std::vector<mrf::uint> const &wavelengths = a_polarized_img3.wavelengths();
    std::cout << "last recovered wavelength = " << wavelengths[wavelengths.size() - 1] << std::endl;

    assert(a_polarized_img3.nSpectralBands() == wavelengths.size());

    assert(wavelengths[0] == FIRST_WAVELENGTH);
    assert(wavelengths[wavelengths.size() - 1] == LAST_WAVELENGTH);


    EXRSpectralImage
        a_polarized_img50(30, 100, FIRST_WAVELENGTH, LAST_WAVELENGTH, NB_WAVELENGTH, radiometry::SPECTRUM_BISPECTRAL);
    assert(a_polarized_img50.bispectral());

    // TEST RE-RADIATION
    std::vector<data_struct::Array2D> const &reradiation = a_polarized_img2.reradiation();
    // NO data at this point
    assert(reradiation.size() == 0);

    std::vector<mrf::uint> const &new_wavelengths = a_polarized_img3.wavelengths();
    assert(a_polarized_img50.nSpectralBands() == new_wavelengths.size());


    //TODO: FIX THIS CHECKING IMAGE TYPE ?
    // assert(a_polarized_img1.polarised());


    return EXIT_SUCCESS;
  }
  catch (const mrf::image::IMAGE_LOAD_SAVE_FLAGS &f)
  {
    std::cerr << "Error code: " << f << std::endl;
    return EXIT_FAILURE;
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << '\n';
    return EXIT_FAILURE;
  }

  std::cout << __FILE__ << " " << __LINE__ << std::endl;

  return EXIT_FAILURE;
}