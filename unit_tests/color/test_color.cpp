

/**
 * Author: Romain Pacanowski @ institutoptique DOT fr
 * Copyright CNRS : 2020
 **/

#include <cstdlib>
#include <cassert>

#include <mrf_core/color/color.hpp>
#include <mrf_core/math/mat4.hpp>


float const TESTING_EPSILON = 1e-4f;

bool epsilonColorEquality(mrf::color::Color const &c1, mrf::color::Color const &c2, float epsilon)
{
  bool test1 = mrf::math::_Math<float>::absVal(c1(0) - c2(0)) < epsilon;
  bool test2 = mrf::math::_Math<float>::absVal(c1(1) - c2(1)) < epsilon;
  bool test3 = mrf::math::_Math<float>::absVal(c1(2) - c2(2)) < epsilon;
  bool test4 = mrf::math::_Math<float>::absVal(c1(3) - c2(3)) < epsilon;

  return test1 && test2 && test3 && test4;
}


bool epsilonMatrixEquality(mrf::math::Mat4f const &m1, mrf::math::Mat4f const &m2, float epsilon)
{
  mrf::math::Mat4f diff = m1 - m2;

  float sum = 0.0f;
  for (size_t i = 0; i < 16; i++)
  {
    sum += mrf::math::_Math<float>::absVal(diff(static_cast<int>(i)));
  }


  return (sum <= TESTING_EPSILON);
}

//------------------------------------------------------------------------------------
// TESTS Gamma Function
//------------------------------------------------------------------------------------
void testingGammaAndInverseGammaFunctions()
{
  using namespace mrf::color;
  using namespace mrf::math;


  //---
  // ADOBE GAMMA Function
  std::function<Color(const Color &)> a_gamma_function = mrf::color::getGammaFunction(ADOBE_RGB);
  Color                               c7;
  c7.set(1.f, 1.f, 1.f, 1.f);

  c7 = a_gamma_function(c7);

  //---
  // Default Gamma Function
  std::function<Color(const Color &)> a_default_gamma_function = mrf::color::getGammaFunction(CIE_XYZ);

  Color c8(0.5f, 0.5f, 0.2f);
  std::cout << " c8 = " << c8 << std::endl;

  Color cpy_c8 = c8;

  c8 = a_default_gamma_function(c8);
  std::cout << " c8 = " << c8 << std::endl;

  //The default Gamma function should be a pow(1 / 2.2f) !
  cpy_c8.pow(1.f / 2.2f);
  std::cout << " cpy_c8 = " << cpy_c8 << std::endl;

  assert(epsilonColorEquality(cpy_c8, c8, TESTING_EPSILON));


  //---
  // sRGB Inverse Gamma Function
  std::function<Color(const Color &)> a_srgb_inverse_gamma_function = mrf::color::getInverseGammaFunction(SRGB);
  c8.set(0.0f, 0.01f, 0.0f, 0.0f);
  std::cout << " c8 = " << c8 << std::endl;

  c8 = a_srgb_inverse_gamma_function(c8);
  std::cout << " c8 = " << c8 << std::endl;
  std::cout << " c8.a = " << c8.a() << std::endl;

  assert(c8.r() < TESTING_EPSILON);
  assert(c8.b() < TESTING_EPSILON);

  // ALPHA channel should NOT have changed
  // TODO: FIXE ME
  // gamma functions and also inverse gamma functions are asymetric
  // some are modifying the alpha channel other not.

  // assert( c8.a() ==  0.0f );

  std::cout << " c8 = " << c8 << std::endl;

  assert(mrf::math::_Math<float>::absVal(c8.g() - (0.01f / 12.92f)) < TESTING_EPSILON);

  //---
  // Default inverse gamma function
  std::function<Color(const Color &)> a_default_inverse_gamma_function = mrf::color::getInverseGammaFunction(CIE_XYZ);

  c8.set(0.5f, 0.5f, 0.2f, 1.f);
  std::cout << " c8 = " << c8 << std::endl;

  cpy_c8 = c8;

  c8 = a_default_inverse_gamma_function(c8);
  std::cout << " c8 = " << c8 << std::endl;

  //The default INVERSE Gamma function should be a pow(2.2f) !
  cpy_c8.pow(2.2f);
  std::cout << " cpy_c8 = " << cpy_c8 << std::endl;

  assert(epsilonColorEquality(cpy_c8, c8, TESTING_EPSILON));


  //---
  std::function<Color(const Color &)> adobe_rgb_inverse_gamma_function = mrf::color::getInverseGammaFunction(ADOBE_RGB);
  c8.set(0.5f, 0.5f, 0.2f, 1.f);
  cpy_c8 = c8;

  c8 = adobe_rgb_inverse_gamma_function(c8);

  // The abobe RGB inverse gamma is almost like the default gamma except that the Gamma
  // has specific ADOBE value
  cpy_c8.pow(ADOBE_RGB_GAMMA);

  assert(epsilonColorEquality(cpy_c8, c8, TESTING_EPSILON));
}


void testingConversionColorMatrix()
{
  using namespace mrf::color;
  using namespace mrf::math;


  //------------------------------------------------------------------------------------
  // XYZ to RGB Matrix for color_data.hpp
  //------------------------------------------------------------------------------------

  Mat4f const identity = Mat4f::identity();

  //--------------------------------------------------------------------
  // With D50 Illuminant
  //--------------------------------------------------------------------

  //---
  Mat4f const &d50_ADOBE_RGB_matrix = mrf::color::getXYZtoRGBMat(ADOBE_RGB, D50);


  assert(mrf::math::_Math<float>::absVal(d50_ADOBE_RGB_matrix(2, 1) + 0.1406752) <= TESTING_EPSILON);

  //CHECK THAT THE PRODUCT OF THE TWO MATRIX is the identity
  Mat4f const &d50_ADOBE_RGB_2_XYZ_matrix = mrf::color::getRGBtoXYZMat(ADOBE_RGB, D50);
  Mat4f        check                      = d50_ADOBE_RGB_matrix * d50_ADOBE_RGB_2_XYZ_matrix;
  assert(epsilonMatrixEquality(check, identity, TESTING_EPSILON));


  //---
  Mat4f const &d50_SRGB_matrix = mrf::color::getXYZtoRGBMat(SRGB, D50);
  assert(mrf::math::_Math<float>::absVal(d50_SRGB_matrix(0, 1) + 1.6168667) <= TESTING_EPSILON);

  //CHECK THAT THE PRODUCT OF THE TWO MATRIX is the identity
  Mat4f const &d50_SRGB_2_XYZ_matrix = mrf::color::getRGBtoXYZMat(SRGB, D50);
  check                              = d50_SRGB_matrix * d50_SRGB_2_XYZ_matrix;
  assert(epsilonMatrixEquality(check, identity, TESTING_EPSILON));

  //---
  Mat4f const &d50_CIE_RGB_matrix = mrf::color::getXYZtoRGBMat(CIE_RGB, D50);
  assert(mrf::math::_Math<float>::absVal(d50_CIE_RGB_matrix(1, 1) - 1.3962369) <= TESTING_EPSILON);

  //CHECK THAT THE PRODUCT OF THE TWO MATRIX is the identity
  Mat4f const &d50_CIE_RGB_2_XYZ_matrix = mrf::color::getRGBtoXYZMat(CIE_RGB, D50);
  check                                 = d50_CIE_RGB_matrix * d50_CIE_RGB_2_XYZ_matrix;

  assert(epsilonMatrixEquality(check, identity, TESTING_EPSILON));

  //---
  Mat4f const &d50_WIDE_GAMUT_RGB_matrix = mrf::color::getXYZtoRGBMat(WIDE_GAMUT_RGB, D50);
  assert(mrf::math::_Math<float>::absVal(d50_WIDE_GAMUT_RGB_matrix(2, 2) - 1.2884099) <= TESTING_EPSILON);

  //CHECK THAT THE PRODUCT OF THE TWO MATRIX is the identity
  Mat4f const &d50_WIDE_GAMUT_RGB_2_XYZ_matrix = mrf::color::getRGBtoXYZMat(WIDE_GAMUT_RGB, D50);
  check                                        = d50_WIDE_GAMUT_RGB_matrix * d50_WIDE_GAMUT_RGB_2_XYZ_matrix;

  assert(epsilonMatrixEquality(check, identity, TESTING_EPSILON));

  //--------------------------------------------------------------------
  // With D65 Illuminant
  //--------------------------------------------------------------------
  //--
  Mat4f const &d65_ADOBE_RGB_matrix_xyz_to_rgb = mrf::color::getXYZtoRGBMat(ADOBE_RGB, D65);
  assert(mrf::math::_Math<float>::absVal(d65_ADOBE_RGB_matrix_xyz_to_rgb(2, 2) - 1.0154096) <= TESTING_EPSILON);


  Mat4f const &d65_ADOBE_RGB_matrix_rgb_to_xyz = mrf::color::getRGBtoXYZMat(ADOBE_RGB, D65);

  check = d65_ADOBE_RGB_matrix_xyz_to_rgb * d65_ADOBE_RGB_matrix_rgb_to_xyz;
  assert(epsilonMatrixEquality(check, identity, TESTING_EPSILON));

  //--
  Mat4f const &d65_SRGB_matrix_xyz_to_rgb = mrf::color::getXYZtoRGBMat(SRGB, D65);
  assert(mrf::math::_Math<float>::absVal(d65_SRGB_matrix_xyz_to_rgb(2, 2) - 1.0572252) <= TESTING_EPSILON);

  Mat4f const &d65_SRGB_matrix_rgb_to_xyz = mrf::color::getRGBtoXYZMat(SRGB, D65);

  check = d65_SRGB_matrix_xyz_to_rgb * d65_SRGB_matrix_rgb_to_xyz;
  assert(epsilonMatrixEquality(check, identity, TESTING_EPSILON));


  //--------------------------------------------------------------------
  // With E Illuminant
  //--------------------------------------------------------------------

  //--
  Mat4f const &e_CIE_RGB_matrix_xyz_to_rgb = mrf::color::getXYZtoRGBMat(CIE_RGB, E);
  assert(mrf::math::_Math<float>::absVal(e_CIE_RGB_matrix_xyz_to_rgb(2, 2) - 1.0093968) <= TESTING_EPSILON);

  Mat4f const &e_CIE_RGB_matrix_rgb_to_xyz = mrf::color::getRGBtoXYZMat(CIE_RGB, E);
  assert(mrf::math::_Math<float>::absVal(e_CIE_RGB_matrix_rgb_to_xyz(0, 2) - 0.2006017) <= TESTING_EPSILON);


  check = e_CIE_RGB_matrix_xyz_to_rgb * e_CIE_RGB_matrix_rgb_to_xyz;
  assert(epsilonMatrixEquality(check, identity, TESTING_EPSILON));




  //NOW TESTING INCORRECT USAGE wit inappropriate combining of color space and illuminants
  {
    try
    {
      Mat4f const &d50_failure = mrf::color::getXYZtoRGBMat(CIE_XYZ, D50);

      assert(false);
    }
    catch (...)
    {
      assert(true);
    }

    try
    {
      Mat4f const &d65_failure = mrf::color::getXYZtoRGBMat(CIE_XYZ, D65);
      assert(false);
    }
    catch (...)
    {
      assert(true);
    }

    try
    {
      Mat4f const &e_failure = mrf::color::getXYZtoRGBMat(CIE_XYZ, E);
      assert(false);
    }
    catch (...)
    {
      assert(true);
    }

    try
    {
      Mat4f const &e_failure = mrf::color::getXYZtoRGBMat(CIE_XYZ, MRF_WHITE_POINT(4));
      assert(false);
    }
    catch (...)
    {
      assert(true);
    }


    //---
    try
    {
      Mat4f const &d50_failure = mrf::color::getRGBtoXYZMat(CIE_XYZ, D50);

      assert(false);
    }
    catch (...)
    {
      assert(true);
    }

    try
    {
      Mat4f const &d65_failure = mrf::color::getRGBtoXYZMat(CIE_XYZ, D65);

      assert(false);
    }
    catch (...)
    {
      assert(true);
    }

    try
    {
      Mat4f const &e_failure = mrf::color::getRGBtoXYZMat(CIE_XYZ, E);

      assert(false);
    }
    catch (...)
    {
      assert(true);
    }

    try
    {
      Mat4f const &e_failure = mrf::color::getRGBtoXYZMat(CIE_XYZ, MRF_WHITE_POINT(4));

      assert(false);
    }
    catch (...)
    {
      assert(true);
    }
  }
}



int main(int /*argc*/, char ** /*argv*/)
{
  using namespace mrf::color;
  using namespace mrf::math;

  //Constructors
  Color c1(1.0f, 2.0f, 3.0f);

  Color c2(1.0f, 2.0f, 3.0f);

  assert(c1 == c2);

  for (unsigned int i = 0; i < 3; i++)
  {
    assert(c1[0] == c1(0));
    assert(c1[1] == c1(1));
    assert(c1[2] == c1(2));

    assert(c2[0] == c2(0));
    assert(c2[1] == c2(1));
    assert(c2[2] == c2(2));

    assert(c1(0) == c2(0));
    assert(c1(1) == c2(1));
    assert(c1(2) == c2(2));
  }

  // Methods  related to construction
  assert(c1 == c2);

  assert(c1.size() == c2.size());

  Color c3(c1);
  Color c4 = c3;

  assert(c1 == c3);
  assert(c4 == c3);


  // Operator +
  Color c5 = c1 + c2;
  for (unsigned int i = 0; i < 3; i++)
  {
    assert(c5(i) == c1(i) + c2(i));
  }

  //Assignment Operator
  c5 = c1;
  assert(c5 == c2);


  Color c6(mrf::math::Vec4f(1.f));
  assert(c6.a() == 1.f);

  Color c7(mrf::math::Vec3f(0.2f));
  assert(c7.a() == 1.f);
  assert(c7.g() != c6.g());

  assert(c7.isPositive());
  assert(!c6.isNegative());

  Color copy_c6 = c6;
  c6.pow(10.f);
  assert(copy_c6 == c6);

  Color neg_c6 = -c6;
  neg_c6.clampNegativeToZero();
  assert(neg_c6.r() == neg_c6.g());
  assert(neg_c6.g() == neg_c6.b());
  assert(neg_c6.b() == 0.0f);

  assert(c7.avgCmp() == 0.2f);

  Color c8 = static_cast<Color const &>(c7).pow(10.0f);

  assert(neg_c6 < 1.f);
  assert(!(neg_c6 < -1.f));


  assert(c7 > 0.001f);
  assert(!(c7 > 100.001f));


  testingGammaAndInverseGammaFunctions();

  testingConversionColorMatrix();

  //------------------------------------------------------------------------------------
  // Testing Reinhard 2005 Tone-Operator function
  //------------------------------------------------------------------------------------

  //TODO : FIND A BETTER CASE TO TEST
  // See original paper here : https://ieeexplore.ieee.org/document/1359728
  // Color test_rein_color(1.f);
  // Color means(1.f );

  // float const chromatic_adaptation = 1.f;
  // float const lum_mean  = 1.f;
  // float const light_adaptation = 1.f;
  // float const contrast  = 1.f;
  // float const intensity = 1.f;

  // REVIEWING THE CODE. IT LOOKS GOOD.
  // Color tm_rein = test_rein_color.reinhard05Tonemapping( chromatic_adaptation, means, lum_mean, light_adaptation, contrast, intensity) ;




  return EXIT_SUCCESS;
}