#!/bin/sh

###############################################################################
# PUT YOUR TOKEN HERE !
# !!! DO NOT PUSH YOUR FILE WITH YOUR TOKEN !!!
###############################################################################
token=YOUR_TOKEN_HERE
project_id=28
server=gitlab.inria.fr

###############################################################################
# ONE WAY TO REMOVE PIPELINE IS THROUGH ID
###############################################################################
start_pipeline=128808
end_pipeline=129000
for pipeline_id in $(seq $start_pipeline $end_pipeline)
do
  echo "deleting $pipeline_id"
  curl --header "PRIVATE-TOKEN: ${token}" --request "DELETE" "https://${server}/api/v4/projects/${project_id}/pipelines/${pipeline_id}"
done

################################################################################
###############################################################################
PROJECT=$project_id
MAX_REPEAT=10

###############################################################################
# ANOTHER WAY is to LAUNCH THIS MANY TIMES
# NOT THAT THIS LOOP REMOVES ONLY FAILED OR SKIPPED OR CANCELED PIPELINES
###############################################################################
for pipeline_id in $(seq 1 $MAX_REPEAT)
do
  for PIPELINE in $(curl --header "PRIVATE-TOKEN: $token" "https://${server}/api/v4/projects/$PROJECT/pipelines?status=skipped&sort=desc" | jq '.[].id') ; do
    echo "deleting $PIPELINE"
    curl --header "PRIVATE-TOKEN: ${token}" --request "DELETE" "https://${server}/api/v4/projects/$PROJECT/pipelines/${PIPELINE}"
  done

  for PIPELINE in $(curl --header "PRIVATE-TOKEN: $token" "https://${server}/api/v4/projects/$PROJECT/pipelines?status=failed&sort=desc" | jq '.[].id') ; do
    echo "deleting $PIPELINE"
    curl --header "PRIVATE-TOKEN: ${token}" --request "DELETE" "https://${server}/api/v4/projects/$PROJECT/pipelines/${PIPELINE}"
  done

  for PIPELINE in $(curl --header "PRIVATE-TOKEN: $token" "https://${server}/api/v4/projects/$PROJECT/pipelines?status=canceled&sort=desc" | jq '.[].id') ; do
    echo "deleting $PIPELINE"
    curl --header "PRIVATE-TOKEN: ${token}" --request "DELETE" "https://${server}/api/v4/projects/$PROJECT/pipelines/${PIPELINE}"
  done

done

###############################################################################
# STUFF TO TESTS OR DEBUG
###############################################################################


# PER_PAGE=10000
# for PIPELINE in $(curl --header "PRIVATE-TOKEN: $token" "https://${server}/api/v4/projects/$PROJECT/jobs?per_page=$PER_PAGE&sort=asc" | jq '.[].pipeline.id') ; do
# TOTO=$(curl --header "PRIVATE-TOKEN: $token" "https://${server}/api/v4/projects/$PROJECT/pipelines?status=failed&sort=asc" | jq '.[].id')
# echo $TOTO
# TUTU=$(curl --header "PRIVATE-TOKEN: $token" "https://${server}/api/v4/projects/$PROJECT/pipelines?sort=asc" )
# echo $TUTU