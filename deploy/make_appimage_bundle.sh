#!/bin/bash

echo "Deploying AppImages for " $1

wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
chmod a+x ./linuxdeploy-x86_64.AppImage

# ----------------------------------------------------------------------------
# Malia Spectral
# ----------------------------------------------------------------------------

./linuxdeploy-x86_64.AppImage                                                \
  --executable=./malia                                                       \
  --desktop-file=./Malia.desktop                                             \
  --icon-file=./icon.png                                                     \
  --appdir AppDir
  
cp -r                                                                        \
  scenes/                                                                    \
  optix_include/                                                             \
  cuda                                                                       \
  cuda_sdk/                                                                  \
  shaders/                                                                   \
  AppDir/usr/bin/

cp *.so* AppDir/usr/lib/

OUTPUT=malia.AppImage                                                        \
  ./linuxdeploy-x86_64.AppImage                                              \
    --executable=./malia                                                     \
    --desktop-file=./Malia.desktop                                           \
    --icon-file=./icon.png                                                   \
    --appdir AppDir                                                          \
    --output appimage

# ----------------------------------------------------------------------------
# Malia RGB
# ----------------------------------------------------------------------------

# MALIA RGB
./linuxdeploy-x86_64.AppImage                                                \
  --executable=./malia_rgb                                                   \
  --desktop-file=./Malia_RGB.desktop                                         \
  --icon-file=./icon.png                                                     \
  --appdir AppDirMaliaRGB

cp -r                                                                        \
  scenes/                                                                    \
  optix_include/                                                             \
  cuda                                                                       \
  cuda_sdk/                                                                  \
  shaders/                                                                   \
  AppDirMaliaRGB/usr/bin/

cp *.so* AppDirMaliaRGB/usr/lib/

OUTPUT=malia_rgb.AppImage                                                    \
  ./linuxdeploy-x86_64.AppImage                                              \
    --executable=./malia_rgb                                                 \
    --desktop-file=./Malia_RGB.desktop                                       \
    --icon-file=./icon.png                                                   \
    --appdir AppDirMaliaRGB                                                  \
    --output appimage