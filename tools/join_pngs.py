import sys
import glob
import os
import subprocess
import time

if len(sys.argv) <4 :
	print("Not enough arguments: join_pngs input_folder_1 input_folder_2 output_folder")
    exit(-1)

input_folder_1 = sys.argv[1]
input_folder_2 = sys.argv[2]
output_folder = sys.argv[3]

no_sleep = True

parent_dir = os.getcwd()
#print(os.getcwd())

os.chdir(input_folder_1)
#print(os.getcwd())
input_images_1 = glob.glob("*.png")
#print(input_images_1)

os.chdir(parent_dir)
#print(os.getcwd())

os.chdir(input_folder_2)
#print(os.getcwd())
input_images_2 = glob.glob("*.png")
#print(input_images_2)

os.chdir(parent_dir)


if(len(input_images_1)!=len(input_images_2)):
	print("Error not same number of pngs in input folders")


for x in range(0,len(input_images_1)):
	merge_args = []
	merge_args.append("magick")
	merge_args.append("convert")
	merge_args.append("+append")
	merge_args.append(os.path.join(input_folder_1,input_images_1[x]))
	merge_args.append(os.path.join(input_folder_2,input_images_2[x]))
	merge_args.append(os.path.join(output_folder,input_images_1[x]))

	print(merge_args)
	
	process_merge_png = subprocess.Popen(merge_args)
    
    if no_sleep:
    else:
		while process_merge_png.poll() is None:
			time.sleep(1)