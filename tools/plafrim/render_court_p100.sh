#!/bin/bash
#SBATCH -J TEST
#SBATCH -p court_sirocco
#SBATCH --exclusive
#SBATCH --gres=gpu:2
#SBATCH --constraint=p100
#SBATCH -o log_lux%j.out
#SBATCH -e log_lux%j.err
#SBATCH -N 1
#SBATCH --time=0-4:00:00
module purge
module load slurm/17.11
export IRAY_ROOT=/home/adufay/iray
export LD_LIBRARY_PATH=/cm/shared/apps/gcc/7.3.0/lib64:/home/adufay/iray/linux-x86-64/lib
#module load compiler/cuda/6.5/toolkit/6.5.14
echo "IRAY_ROOT = " $IRAY_ROOT
echo "LD = " $LD_LIBRARY_PATH
#echo "gpu visible devices are:"$CUDA_VISIBLE_DEVICES
#srun hostname
#srun nvidia-smi

renderer="/home/adufay/apps/iray\ renderer/IRAY_RENDERER_SPECTRAL"
samples=" -samples 2 " 
waves=" -wr $1:$2:5 "
out=" -o /home/adufay/lux_$1_$2.exr "
in=" -scene /home/adufay/assets/scenes/luxrender_prism.msf "
mono=" -mono_wavelength "
caustic=" -caustic_sampler "

echo $in $out $waves $samples $mono $caustic -firefly_filter

srun /home/adufay/apps/iray\ renderer/IRAY_RENDERER_SPECTRAL $in $out $waves $samples $mono $caustic -firefly_filter
