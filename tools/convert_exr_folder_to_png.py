import shutil
import os,sys
import re
import subprocess
import time
from shutil import copyfile
from shutil import move
import glob
import platform

SPECTRAL_CONVERTER = "mic"
if platform.system()=="Windows":
    SPECTRAL_CONVERTER = SPECTRAL_CONVERTER + ".exe"


exposition_value   = "1.0"
gamma_value        = "1.0"

if len(sys.argv) < 2:
	print("[ERROR] You must specified a path")
	exit(-1)

folder_path = sys.argv[1]

if len(sys.argv) >= 3:
    exposition_value = sys.argv[2]

print("\n[INFO] Exposition value : " + exposition_value +"\n")

if len(sys.argv) >= 4:
    gamma_value = sys.argv[3]

print("\n[INFO] Gamma value : " + gamma_value +"\n")

if "MRF_DIR" in os.environ:
    current_path = os.environ["PATH"]

    each_path = current_path.split(":")

    MRF_BIN_DIR = os.environ["MRF_DIR"] + "bin/"

    if MRF_BIN_DIR not in each_path :
        os.environ["PATH"] += ":"+MRF_BIN_DIR
        print("[WARNING] MRF_DIR is set but MRF_DIR/bin/ is not in PATH. Adding it temporarily!")
    #endif
else:
    print("[WARNING] MRF_DIR environment variable is not set. Checking if "  + SPECTRAL_CONVERTER + " is in your current PATH ")
#end_if_else


print("[DEBUG] Checking if the spectral converter is in Path")

# CHecking with which
if shutil.which(SPECTRAL_CONVERTER) is None :
    print("[ERROR] The application to convert from Spectral to EXR or PNG was not found. CHECK YOUR PATH")
    exit(-1)


files = glob.glob(folder_path+"/*.exr")


pid_of_suprocesses = []
for file in files:
    process_conv_png = subprocess.Popen( (SPECTRAL_CONVERTER, "-in", file , "-out", file[:-4] + ".png", "-exposition", exposition_value, "-gamma", gamma_value) )
    pid_of_suprocesses.append( process_conv_png )
#end_for

# Always a good idea to wait for the end of the subprocesses
exit_code = [ p.wait() for p in  pid_of_suprocesses ]
print("[INFO] Done!")
