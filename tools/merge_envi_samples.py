###########################################MERGE ENVI SAMPLES#############################
#
#
#  Merge samples from two ENVI files
#  
#
#  Script input parameters are: file_1.hdr #samples_1 file_2.hdr #samples_2 file_out.hdr
#
#  
#  The script merges the files sampl (pixel data).
#
#  The ENVI files should have the same pixel resolution.
#    
#  The wavelengths of each file must be the same.
#
#########################################################################################

import sys
import os

import envi_parsing
import numpy as np


def mergeEnviSamples(path_1,nb_samples_1,path_2,nb_samples_2,path_3):
  header_1 = ev.parse_envi_header(path_1)
  header_2 = ev.parse_envi_header(path_2)

  header = envi_parsing.copy(header_1)
  header.path = path_3
  header.desc = header_1.desc+"\n"+header_2.desc

  print("Interleave mode = "+header_1.interleave+".")

  if(header_1.interleave.startswith("bsq") and
    header_2.interleave.startswith("bsq") and
    header_1.waves==header_2.waves and
    header_1.data_type==4 and
    header_2.data_type==4):
    
    header.write_file()

    path_1 = os.path.splitext(header_1.path)[0] + ".raw"
    path_2 = os.path.splitext(header_2.path)[0] + ".raw"
    path_3 = os.path.splitext(header.path)[0] + ".raw"

    data_1 = np.fromfile(path_1, '<f4')
    data_1.shape = (header.width, header.height, len(header.waves))

    data_2 = np.fromfile(path_2, '<f4')
    data_2.shape = (header.width, header.height, len(header.waves))

    data_out = np.empty(data_1.shape, '>f4')
    
    coeff_1 = float(nb_samples_1) / float(nb_samples_1+nb_samples_2)
    coeff_2 = float(nb_samples_2) / float(nb_samples_1+nb_samples_2)

    print(coeff_1)
    print(coeff_2)

    data_out = coeff_1*data_1 + coeff_2*data_2

    data_out.tofile(path_3)

def mergeEnviSamplesList(path_list):

  data_out = np.empty([1,1,1], '>f4')

  header_out = envi_parsing.Header()

  path_out = path_list[len(path_list)-1]
  print(path_out)

  counter = 0
  

  for path in path_list[:-1]:
    header = ev.parse_envi_header(path)
    header_out = envi_parsing.copy(header)

    print("Interleave mode = "+header.interleave+".")

    if(header.interleave.startswith("bsq") and
      header.data_type==4):
      
      

      path = os.path.splitext(header.path)[0] + ".raw"

      data = np.fromfile(path, '<f4')
      data.shape = (header.width, header.height, len(header.waves))

      if(counter==1):
        data_out = np.empty(data.shape, '>f4')
        data_out = data
      else:
        data_out = data_out + data
      
      counter = counter + 1

  if(counter>0):
    print("Average "+str(counter)+" files")
    
    if(counter>1):
      data_out = data_out / float(counter)
    
    header_out.path = path_out
    header_out.write_file()
    
    path_data_out = os.path.splitext(header_out.path)[0] + ".raw"
    data_out.tofile(path_data_out)




def help():
  print("Usage: "+sys.argv[0]+" file_1.hdr #samples_1 file_2.hdr #samples_2 file_out.hdr")

if(len(sys.argv)<2):
  help()
else:
  if(sys.argv[1].startswith("--list")):
    mergeEnviSamplesList(sys.argv[2:])
  elif(len(sys.argv)==6):
    mergeEnviSamples(sys.argv[1],int(sys.argv[2]),sys.argv[3],int(sys.argv[4]),sys.argv[5])
    

