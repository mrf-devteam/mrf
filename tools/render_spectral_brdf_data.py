#########################################################################################
#
# Author:  Romain Pacanowski       Copyright CNRS  : 2018
#
#########################################################################################



#########################################################################################
#
# This script render all materials (assumed to be spectral tabulated BRDF data)
# for a given scene
#
# This script should be launched from the directory where all zip files are
#
#########################################################################################


import zipfile
import shutil
import os,sys,glob
import re
import subprocess
import time
import platform
import errno
import xml.etree.ElementTree as ET
from shutil import copyfile
from shutil import move

#########################################################################################
# GLOBAL VARIABLES
#########################################################################################
# CHECK THIS IF YOUR ARE ON CLUSTER with SLURM SUPPORT
RENDER_ON_PLAFRIM = "False"


RENDERING_SCRIPT = "render_split_spectral_brdf.py"
EXPORT_TO_EXR    = "True"
EXPORT_TO_PNG    = "True"

# FOR TONE-MAPPING
exposition_value   = "1.0"

# NUMBER OF SAMPLES PER PIXEL
NB_SPP = "1"
#Material Name that is split
SPLIT_MATERIAL_NAME = "measured_material"
#########################################################################################


#########################################################################################
# Global Utility Functions
#########################################################################################
def force_symlink(file1, file2):
    try:
        os.symlink(file1, file2)
    except OSError as e:
        if e.errno == errno.EEXIST:
            os.remove(file2)
            os.symlink(file1, file2)

def safe_symlink(file1, file2):
    try:
        os.symlink(file1,file2)
    except OSError as e:
        if e.errno == errno.EEXIST:
            print("[WARNING] Symlink already exists on :" + file1 + ". Symbolic Link not updated. ")




#########################################################################################
# CHECK that everything is there to process the directory
if os.environ.get("MRF_DIR") is None:
    print("[ERROR] THE MRF environment variable is not set. FIX THAT!!!")
    exit(-1)

MRF_DIR = os.environ.get("MRF_DIR")

scene_files = glob.glob("*.msf")

if len(scene_files) == 0 :
    print("[ERROR] There isn't any .msf file. No scene file, no Rendering. PUT A SCENE FILE here !")
    exit(-1)
if len(scene_files)> 1 :
    print("[WARNING] Multiple scene files are present in the current directory. Only one will be rendered")


PATH_TO_RENDERING_SCRIPT = MRF_DIR + "/tools/" + RENDERING_SCRIPT


scene_filename = scene_files[0]
print("[INFO] Scene file rendered is: " + scene_filename)

scene_name = scene_filename[0:scene_filename.rfind(".")]
camera_filename = scene_name  + ".mcf"
asset_directory  = scene_name +"_assets"

print("scene_name = " + scene_name )

target_directory = "renderings_spp_" + NB_SPP
os.makedirs(target_directory,0o777, True)
target_directory = "../" + target_directory +"/"
print("FINAL TARGET DIRECTORY " + target_directory)
#########################################################################################


#########################################################################################
#   LET'S GO MAESTRO
#########################################################################################

# Listing all zip of the current directory
for brdf_zip_file in glob.glob("*.zip"):
    print("[INFO] Processing " + brdf_zip_file)

    ext_index = brdf_zip_file.rfind(".")
    tmp_material_filename = brdf_zip_file[0:ext_index]
    tmp_material_filename = tmp_material_filename.replace(".","_")

    os.makedirs(tmp_material_filename,0o777, True)

    #shutil.move( brdf_zip_file, tmp_material_filename )

    os.chdir(tmp_material_filename)

    #Other Strategy symbolic link to the zip file as well
    safe_symlink("../" + brdf_zip_file, brdf_zip_file)

    safe_symlink( "../" + scene_filename , scene_filename )
    safe_symlink( "../" + camera_filename, camera_filename )
    safe_symlink( "../" + asset_directory, asset_directory )

    shutil.copy( PATH_TO_RENDERING_SCRIPT, ".")


    zip = zipfile.ZipFile(brdf_zip_file)
    zip.extractall(".")
    # NO MOVE WITH THIS STRATEGY
    #shutil.move(brdf_zip_file, "..")

    print("[INFO] Launching Rendering ")
    #cmd = "python "+ RENDERING_SCRIPT +  " " + SPLIT_MATERIAL_NAME + " " + NB_SPP + " " + EXPORT_TO_EXR + " " + EXPORT_TO_PNG
    #os.system( cmd )

    # WARNING Python3 is required here
    process_launch_renderer = subprocess.Popen( ["python3", RENDERING_SCRIPT, SPLIT_MATERIAL_NAME, NB_SPP, EXPORT_TO_EXR, EXPORT_TO_PNG, RENDER_ON_PLAFRIM, exposition_value] )

    while process_launch_renderer.poll() is None:
      #print('Still rendering')
        time.sleep(5)

    print("RENDERing Python script Exited with returncode %d" % process_launch_renderer.returncode)
    if ( process_launch_renderer.returncode < 0 ):
        print("[ERROR] Rendering failed. ABORTING")
        exit(-1)
    #end_if

    all_bin_files = glob.glob("*.bin")
    for a_bin_file in all_bin_files :
        os.remove(a_bin_file)



    all_png_files = glob.glob("*.png")
    for a_png_file in all_png_files:
        os.replace(a_png_file,target_directory+a_png_file)

    all_exr_files = glob.glob("*.exr")
    for a_exr_file in all_exr_files :
        os.replace(a_exr_file,target_directory+a_exr_file)

    all_hdr_files = glob.glob("*.hdr")
    for a_hdr_file in all_hdr_files :
        os.replace(a_hdr_file, target_directory+a_hdr_file)

    all_raw_files = glob.glob("*.raw")
    for a_raw_file in all_raw_files :
        os.replace(a_raw_file, target_directory+a_raw_file)

    os.chdir("..")


#end_for on all zip FILES



print("[INFO]  ************************** DONE ***********************")

