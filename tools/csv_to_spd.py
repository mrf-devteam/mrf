import csv
import sys
import os

import write_spd


if(len(sys.argv)<2):
  print("Usage: csvToSPd.py input.csv")
  exit(-1)


inputFileName = sys.argv[1]

outputFileName, file_extension = os.path.splitext(inputFileName)
outputFileName_eta = outputFileName + "_eta.spd"
outputFileName_kappa = outputFileName + "_kappa.spd"

print("Converting "+inputFileName+" to \n")
print(outputFileName_eta+" and " + outputFileName_kappa)

spectrum_eta = []
spectrum_kappa = []

parsing_eta = True

with open(inputFileName, 'r', newline='') as fileIn:
  for line in fileIn.readlines():
    #print(line)
    if(line.startswith('wl,n')):
      parsing_eta = True
      continue
    elif(line.startswith('wl,k')):
      parsing_eta = False
      continue
    elif(line.startswith('\r')):
      continue
    elif(line.startswith('\n')):
      continue

    wavelength = float(line.split(",")[0])*1000
    value = float(line.split(",")[1])
    #print(wavelength)
    #print(value)

    if(parsing_eta):
      spectrum_eta.append([wavelength,value])
    else:
      spectrum_kappa.append([wavelength,value])


#print(spectrum_eta)
#print(spectrum_kappa)

write_spd.writeSpd(outputFileName_eta, spectrum_eta)
write_spd.writeSpd(outputFileName_kappa, spectrum_kappa)

print("Wrote "+str(len(spectrum_eta))+" eta values")
print("Wrote "+str(len(spectrum_kappa))+" kappa values")
