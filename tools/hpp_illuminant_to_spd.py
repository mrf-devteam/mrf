import os,sys

waves_values = []

file = open(sys.argv[1],"r")
for line in file:
	#print(line)
	line_splitted = line.split(",")
	for token in line_splitted:
		#print(token)
		token = token.strip()
		token = token.replace("f","")
		if(len(token)>0):
			waves_values.append(token)


_lambda = 300

file_out = open(sys.argv[2],"w")

for wave_val in waves_values:
	print(str(_lambda))
	print(wave_val)
	file_out.write(str(_lambda)+":"+wave_val+", ")
	_lambda = _lambda + 1

file_out.close()
file.close()