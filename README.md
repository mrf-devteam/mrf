Malia Rendering Framework (MRF)
================================

MRF is a spectral rendering framework.

To install, compile and use MRF refers to its documentation available
here: https://mrf-devteam.gitlab.io/mrf/main.md.html

Downloads
=========

We provide continuous builds on master branch.

### Malia and Malia RGB

Malia renderer (in both its spectral and RGB versions) can be download
as a 7zip archive on Windows and in AppImage format for Linux:

| NVIDIA Card  | Windows                                                                                                           | Linux                                                                                                                                                                                                                                                    |
| ------------ | ----------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| >= Maxwell   | [Programs](https://gitlab.inria.fr/pacanows/MRF/-/jobs/artifacts/master/raw/Malia-1.0.0-win64.7z?job=malia_win10) | [AppImage spectral](https://gitlab.inria.fr/pacanows/MRF/-/jobs/artifacts/master/raw/malia.AppImage?job=malia_ubuntu_18_04) / [AppImage RGB](https://gitlab.inria.fr/pacanows/MRF/-/jobs/artifacts/master/raw/malia_rgb.AppImage?job=malia_ubuntu_18_04) |

We also provide a snap package compatible with various Linux
distributions:

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/malia)

### Blender integration

You can pick the relevant Blender plugin depending on your
platform. (Kepler builds coming soon).

| NVIDIA Card  | Windows                                                                                                            | Linux                                                                                                                     |
| ------------ | ------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------- |
| >= Maxwell   | [Blender bridge](https://gitlab.inria.fr/pacanows/MRF/-/jobs/artifacts/master/raw/blender_mrf.zip?job=malia_win10) | [Blender bridge](https://gitlab.inria.fr/pacanows/MRF/-/jobs/artifacts/master/raw/blender_mrf.zip?job=malia_ubuntu_18_04) |


Code format
===========

We use `clang-format` utility to ease merge requests
inspection. Ensure your source code is correctly formatted before
submitting a merge request.

A CMake rule `clang-format` is available for formatting the code on
Linux. Windows users can use the Linux subsystem tool.

```bash
mkdir build
cd build
cmake ..
make clang-format
```
